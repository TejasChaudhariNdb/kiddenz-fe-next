import React from 'react';
import PropTypes from 'prop-types';
import Head from 'next/head';
import AuthHoc from 'hoc/authCheck';
import dynamic from 'next/dynamic';
import { SEO_TAGS } from 'utils/constants';
import { getData } from 'shared/utils';
import { toast } from 'react-toastify';
import MascotLoader from 'components/common/MascotLoader';
import { GApageView } from 'utils/google';

// import VerifyEmail from 'components/EmptyStates/verifyEmail';

const HomePage = dynamic(() => import('components/EmptyStates/verifyEmail'), {
  loading: () => <MascotLoader />,
});

const successToast = msg => toast.success(msg);

const errorToast = msg => toast.error(msg);

const Home = props => {
  const {
    router: { asPath },
  } = props;

  React.useEffect(() => {
    GApageView(asPath, 'Email Verification Success Page');
  }, []);

  return (
    <div>
      <Head>
        <title>{SEO_TAGS.index.title}</title>
        <meta name="description" content={SEO_TAGS.index.description} />
        <meta name="keyword" content={SEO_TAGS.index.keyword} />
        <meta name="og:title" content={SEO_TAGS.index.title} />
        <meta name="og:description" content={SEO_TAGS.index.description} />
      </Head>

      <HomePage
        {...props}
        getData={getData}
        successToast={successToast}
        errorToast={errorToast}
      />
    </div>
  );
};

Home.propTypes = {
  router: PropTypes.object,
};

Home.getInitialProps = async props => {
  const { query: { activation_key: key } = {}, VERIFY_EMAIL_API_CALL } = props;

  VERIFY_EMAIL_API_CALL({
    params: { key },
  });

  const isLoggedIn = false;
  // props.DASHBOARD_GET_CATEGORY_API_CALL({});
  if (isLoggedIn) return {};

  return {};
};

export default AuthHoc(Home, false);
