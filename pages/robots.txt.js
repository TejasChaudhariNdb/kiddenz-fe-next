// eslint-disable-next-line arrow-body-style
const Sitemap = () => {
  return null;
};

export const getServerSideProps = async ({ res }) => {
  const sitemap = `User-agent: *
  Disallow: *?
  
  User-agent: MJ12bot
  Disallow: /
  
  User-agent: GermCrawler
  Disallow: /
  
  User-agent: Sogou
  Disallow: /
  
  User-agent: AhrefsBot
  Disallow: /
  
  User-Agent: RSiteAuditor
  Disallow: /
  
  User-agent: Screaming-frog
  Disallow: /
  
  User-agent: SemrushBot
  Disallow: /
  
  User-agent: SiteAuditBot
  Disallow: / 
  
  sitemap: https://www.kiddenz.com/sitemap.xml
  sitemap: https://www.kiddenz.com/sitemap-dtl.xml
  sitemap: https://www.kiddenz.com/sitemap-areas.xml
  sitemap: https://www.kiddenz.com/sitemap-blogs.xml
  sitemap: https://www.kiddenz.com/sitemap-cities.xml  
  `;

  res.setHeader('Content-Type', 'text');
  res.write(sitemap);
  res.end();

  return {
    props: {},
  };
};

export default Sitemap;
