/* eslint-disable no-unused-vars */
/* eslint-disable eqeqeq */
import React from 'react';
import Head from 'next/head';
import PropTypes from 'prop-types';
import AuthHoc from 'hoc/authCheck';
import dynamic from 'next/dynamic';
import { SEO_TAGS } from 'utils/constants';
import { getData } from 'shared/utils';
import { toast } from 'react-toastify';
import MascotLoader from 'components/common/MascotLoader';
import { GApageView } from 'utils/google';

const PROGRAM_LIMIT = 9;
const DEFAULT_MAX_PRICE = 500000;
const AGE_LIMIT = 15;

const HomePage = dynamic(() => import('containers/OnlineProgram'), {
  loading: () => <MascotLoader />,
});

const successToast = msg => toast.success(msg);

const errorToast = msg => toast.error(msg);

const Home = props => {
  const {
    router: { asPath },
  } = props;

  React.useEffect(() => {
    GApageView(asPath, 'FAQ Page');
  }, []);

  return (
    <div>
      <Head>
        <title>{SEO_TAGS.index.title}</title>
        <meta name="description" content={SEO_TAGS.index.description} />
        <meta name="keyword" content={SEO_TAGS.index.keyword} />
        <meta name="og:title" content={SEO_TAGS.index.title} />
        <meta name="og:description" content={SEO_TAGS.index.description} />
      </Head>

      <HomePage
        {...props}
        getData={getData}
        successToast={successToast}
        errorToast={errorToast}
      />
    </div>
  );
};

Home.propTypes = {
  router: PropTypes.object,
};

Home.getInitialProps = async props => {
  const {
    ONLINE_PROGRAM_SEARCH_API_CALL,
    ONLINE_CATEGORY_LIST_API_CALL,
    ONLINE_PROGRAM_MAX_PRICE_API_CALL,
    ONLINE_PROGRAM_CITIES_API_CALL,
    GET_ONLINE_PROGRAM_WISHLIST_API_CALL,
    query: {
      search = '',
      city = '',
      categories = [],
      programs = '',
      courses = [],
      isCertificate = '',
      isMoneyback = '',
      school = '',
      startPrice = 0,
      endPrice = DEFAULT_MAX_PRICE,
      startAge = 0,
      endAge = AGE_LIMIT * 12,
      isTrial = '',
      isDiscount = '',
    } = {},
    // isLoggedIn,
  } = props;
  if (props.req) {
    ONLINE_CATEGORY_LIST_API_CALL();
    ONLINE_PROGRAM_MAX_PRICE_API_CALL();
    ONLINE_PROGRAM_CITIES_API_CALL();
    GET_ONLINE_PROGRAM_WISHLIST_API_CALL();
    ONLINE_PROGRAM_SEARCH_API_CALL({
      query: {
        limit: PROGRAM_LIMIT,
        skip: 0,
        search,
        city,
        category: categories,
        online_tag: programs != 'all' && programs != '' ? programs : '',
        course_type: courses,
        certificate: isCertificate,
        money_back_gurantee: isMoneyback,
        school_type: school,
        start_price: startPrice,
        end_price: endPrice,
        start_age: startAge,
        end_age: endAge,
        discount: isDiscount,
        is_trial_class: isTrial,
      },
    });
  }

  const isLoggedIn = false;
  // props.DASHBOARD_GET_CATEGORY_API_CALL({});
  if (isLoggedIn) return { getData };

  return { getData };
};

export default AuthHoc(Home, false);
