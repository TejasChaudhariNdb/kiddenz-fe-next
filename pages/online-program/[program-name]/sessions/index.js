import React from 'react';
// import axios from 'axios';
// import Qs from 'query-string';
import Head from 'next/head';
import AuthHoc from 'hoc/authCheck';
import PropTypes from 'prop-types';
import dynamic from 'next/dynamic';
import { SEO_TAGS } from 'utils/constants';
import { getData } from 'shared/utils';
import { toast } from 'react-toastify';
import MascotLoader from 'components/common/MascotLoader';
// import { ON_SUCCESS } from 'shared/utils/commonReduxSagaConverter/commonConstants';

const HomePage = dynamic(() => import('containers/OnlineProgram/session'), {
  loading: () => <MascotLoader />,
});

const successToast = msg => toast.success(msg);

const errorToast = msg => toast.error(msg);

const Home = props => {
  // const { query: { id } = {}, router: { asPath = '' } = {} } = props;

  // const { query: { name = '' } = {} } = Qs.parseUrl(asPath);

  const seoTitle = SEO_TAGS.index.title;

  return (
    <div>
      <Head>
        <title>{seoTitle}</title>
        <meta name="description" content={SEO_TAGS.index.description} />
        <meta name="keyword" content={SEO_TAGS.index.keyword} />
        <meta name="og:title" content={SEO_TAGS.index.title} />
        <meta name="og:description" content={SEO_TAGS.index.description} />
      </Head>

      <HomePage
        {...props}
        getData={getData}
        successToast={successToast}
        errorToast={errorToast}
      />
    </div>
  );
};
Home.propTypes = {
  query: PropTypes.object,
  router: PropTypes.object,
};

Home.getInitialProps = async props => {
  const {
    query: { id: programId } = {},
    ONLINE_PROGRAM_DETAIL_API_CALL,
    ONLINE_PROGRAM_PURCHASED_API_CALL,
    GET_ONLINE_PROGRAM_WISHLIST_API_CALL,
  } = props;

  const isLoggedIn = false;

  ONLINE_PROGRAM_DETAIL_API_CALL({
    params: {
      id: programId,
    },
  });
  ONLINE_PROGRAM_PURCHASED_API_CALL();
  GET_ONLINE_PROGRAM_WISHLIST_API_CALL();

  if (isLoggedIn) return {};
  return {};
};

export default AuthHoc(Home, false);
