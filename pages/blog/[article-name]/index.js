import React from 'react';
import { useRouter } from 'next/router';
// import axios from 'axios';
import Qs from 'query-string';
import Head from 'next/head';
import AuthHoc from 'hoc/authCheck';
import PropTypes from 'prop-types';
import dynamic from 'next/dynamic';
import { SEO_TAGS } from 'utils/constants';
import { getData } from 'shared/utils';
import { toast } from 'react-toastify';
import { GApageView, GAevent } from 'utils/google';
import MascotLoader from 'components/common/MascotLoader';
import { ON_SUCCESS } from 'shared/utils/commonReduxSagaConverter/commonConstants';

const HomePage = dynamic(() => import('containers/Articles/reading'), {
  loading: () => <MascotLoader />,
});

const successToast = msg => toast.success(msg);

const errorToast = msg => toast.error(msg);
const Home = props => {
  const { query: { category } = {}, router: { asPath = '' } = {} } = props;

  const { query: { name = '' } = {} } = Qs.parseUrl(asPath);

  const routerPath = useRouter();

  const origin =
    typeof window !== 'undefined' && window.location.origin
      ? window.location.origin
      : '';

  React.useEffect(() => {
    GApageView(asPath, 'Article Details Page');
    GAevent('Article Visit', name);
    console.log(origin);
  }, []);

  const description = `“${category}” article on “${name}”`;

  return (
    <div>
      <Head>
        <title>{name}</title>
        <meta name="description" content={description} />
        <meta name="keyword" content={SEO_TAGS.index.keyword} />
        <meta name="og:title" content={name} />
        <meta name="og:description" content={description} />
        <link
          rel="canonical"
          href={`https://kiddenz.com${routerPath.asPath.slice(
            0,
            routerPath.asPath.indexOf('?') >= 0
              ? routerPath.asPath.indexOf('?')
              : undefined,
          )}`}
        />
      </Head>

      <HomePage
        {...props}
        getData={getData}
        successToast={successToast}
        errorToast={errorToast}
      />
    </div>
  );
};
Home.propTypes = {
  query: PropTypes.object,
  router: PropTypes.object,
};

Home.getInitialProps = async props => {
  console.log(props.query);
  console.log(props.query['article-name']);
  const {
    GET_ARTICLES_BY_ID_API_CALL,
    GET_USER_BOOKMARK_ARTICLES_API_CALL,
    GET_ARTICLE_LIKE_VIEW_COUNT_API_CALL,
    GET_USER_LIKED_ARTICLES_API_CALL,
    // ADD_ARTICLE_VIEW_API_CALL,
    GET_ARTICLES_BY_ID_API_CUSTOM_TASK,
    ARTICLES_GET_ALL_CATEGORIES_API_CALL,
    GET_USER_IP_API_CALL,
  } = props;

  const isLoggedIn = false;

  let idArticle = '';
  const blogNamesArray = props.query['article-name'].split('-');
  idArticle = blogNamesArray[blogNamesArray.length - 1];
  console.log('*****************');
  console.log(idArticle);
  console.log('*****************');

  ARTICLES_GET_ALL_CATEGORIES_API_CALL({ filter: ['detailMainCat'] });

  GET_ARTICLES_BY_ID_API_CALL({
    params: {
      id: idArticle,
    },
  });
  GET_USER_BOOKMARK_ARTICLES_API_CALL();
  GET_ARTICLE_LIKE_VIEW_COUNT_API_CALL({
    query: { article_id: idArticle },
    successCallback: ({
      res: { data: { data: { like_count: likeCount } = {} } = {} } = {},
    }) => {
      GET_ARTICLES_BY_ID_API_CUSTOM_TASK(ON_SUCCESS, {
        tasks: [
          {
            task: 'isUpdate',
            params: {
              subKey: ['data'],
            },
            value: { data: { likeCount } },
          },
        ],
      });
    },
  });
  GET_USER_LIKED_ARTICLES_API_CALL();
  GET_USER_IP_API_CALL();
  // axios
  //   .get('https://api.ipify.org/?format=json')
  //   .then(res => res && res.json())
  //   .then(({ ip }) => {
  //     const payload = {
  //       article_id: articleId,
  //       ip_address: ip,
  //     };

  //     ADD_ARTICLE_VIEW_API_CALL({
  //       payload,
  //       successCallback: ({ data: { data: { count: viewCount } = {} } = {} }) =>
  //         GET_ARTICLES_BY_ID_API_CUSTOM_TASK(ON_SUCCESS, {
  //           tasks: [
  //             {
  //               task: 'isUpdate',
  //               params: {
  //                 subKey: ['data'],
  //               },
  //               value: { data: { viewCount } },
  //             },
  //           ],
  //         }),
  //     });
  //   });

  // const data = await getIpInfo();
  // console.log(data, 'query');

  if (isLoggedIn) return {};
  return {};
};

// const getIpInfo = async () => {
//   const res = fetch('https://api.ipify.org/?format=json');
//   // const res = await axios('https://extreme-ip-lookup.com/json');
//   // eslint-disable-next-line no-return-await
//   return await res;
// };

export default AuthHoc(Home, false);
