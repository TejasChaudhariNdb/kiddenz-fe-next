import React from 'react';
import Head from 'next/head';
import AuthHoc from 'hoc/authCheck';
import PropTypes from 'prop-types';
import dynamic from 'next/dynamic';
import { SEO_TAGS } from 'utils/constants';
import { getData } from 'shared/utils';
import { toast } from 'react-toastify';
import { GApageView } from 'utils/google';
import MascotLoader from 'components/common/MascotLoader';

const HomePage = dynamic(() => import('containers/Articles/category'), {
  loading: () => <MascotLoader />,
});
const successToast = msg => toast.success(msg);

const errorToast = msg => toast.error(msg);

const Home = props => {
  const {
    query: { 'category-name': categoryName } = {},
    router: { asPath },
  } = props;

  React.useEffect(() => {
    GApageView(asPath, 'Article Category Page');
  }, []);

  const title = `Articles by experts and parents on “${categoryName}” - Kiddenz`;
  const description = `Discover best articles written by experts and parents on “${categoryName}”. Find answers to all child care related queries and questions on Kiddenz`;
  return (
    <div>
      <Head>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta name="keyword" content={SEO_TAGS.index.keyword} />
        <meta name="og:title" content={title} />
        <meta name="og:description" content={description} />
      </Head>

      <HomePage
        {...props}
        getData={getData}
        successToast={successToast}
        errorToast={errorToast}
      />
    </div>
  );
};

Home.propTypes = {
  query: PropTypes.object,
  router: PropTypes.object,
};

Home.getInitialProps = async props => {
  const {
    query: { 'category-name': categoryName, id } = {},
    GET_CATEGORY_ARTICLES_API_CALL,
    ARTICLES_GET_ALL_CATEGORIES_API_CALL,
    GET_USER_BOOKMARK_ARTICLES_API_CALL,
    GET_MOST_READ_ARTICLES_API_CALL,
  } = props;
  const isLoggedIn = false;
  GET_CATEGORY_ARTICLES_API_CALL({
    params: {
      slugName: categoryName,
    },
  });

  ARTICLES_GET_ALL_CATEGORIES_API_CALL({
    query: {
      category_id: id,
    },
    successCallback: () => {},
    filter: ['subCats'],
  });

  ARTICLES_GET_ALL_CATEGORIES_API_CALL({
    filter: ['mainCats'],
    successCallback: () => {},
  });

  GET_USER_BOOKMARK_ARTICLES_API_CALL();
  GET_MOST_READ_ARTICLES_API_CALL();
  if (isLoggedIn) return {};
  return {};
};

export default AuthHoc(Home, false);
