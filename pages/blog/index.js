import React from 'react';
import Head from 'next/head';
import PropTypes from 'prop-types';
import AuthHoc from 'hoc/authCheck';
import dynamic from 'next/dynamic';
import { SEO_TAGS } from 'utils/constants';
import { getData } from 'shared/utils';
import { toast } from 'react-toastify';
import { GApageView } from 'utils/google';
import MascotLoader from 'components/common/MascotLoader';

const HomePage = dynamic(() => import('containers/Articles'), {
  loading: () => <MascotLoader />,
});

const successToast = msg => toast.success(msg);

const errorToast = msg => toast.error(msg);
const Home = props => {
  const {
    router: { asPath },
  } = props;

  React.useEffect(() => {
    GApageView(asPath, 'Article Landing Page');
  }, []);

  return (
    <div>
      <Head>
        <title>{SEO_TAGS.articleList.title}</title>
        <meta name="description" content={SEO_TAGS.articleList.description} />
        <meta name="keyword" content={SEO_TAGS.articleList.keyword} />
        <meta name="og:title" content={SEO_TAGS.articleList.title} />
        <meta
          name="og:description"
          content={SEO_TAGS.articleList.description}
        />
      </Head>

      <HomePage
        {...props}
        getData={getData}
        successToast={successToast}
        errorToast={errorToast}
      />
    </div>
  );
};

Home.propTypes = {
  router: PropTypes.object,
};

Home.getInitialProps = async props => {
  const isLoggedIn = false;
  const {
    ARTICLES_GET_DASHBOARD_LIST_API_CALL,
    ARTICLES_GET_ALL_CATEGORIES_API_CALL,
    GET_USER_BOOKMARK_ARTICLES_API_CALL,
    GET_MOST_READ_ARTICLES_API_CALL,
  } = props;

  ARTICLES_GET_ALL_CATEGORIES_API_CALL();
  ARTICLES_GET_DASHBOARD_LIST_API_CALL();
  GET_USER_BOOKMARK_ARTICLES_API_CALL();
  GET_MOST_READ_ARTICLES_API_CALL();

  if (isLoggedIn) return {};
  return {};
};

export default AuthHoc(Home, false);
