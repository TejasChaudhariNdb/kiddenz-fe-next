import React from 'react';
import Head from 'next/head';
import PropTypes from 'prop-types';
import AuthHoc from 'hoc/authCheck';
import dynamic from 'next/dynamic';
import { SEO_TAGS } from 'utils/constants';
import { GApageView } from 'utils/google';
import MascotLoader from 'components/common/MascotLoader';

const HomePage = dynamic(() => import('containers/Notification'), {
  loading: () => <MascotLoader />,
});

const Home = props => {
  const {
    router: { asPath },
  } = props;

  React.useEffect(() => {
    GApageView(asPath, 'Notifications Page');
  }, []);

  return (
    <div>
      <Head>
        <title>{SEO_TAGS.index.title}</title>
        <meta name="description" content={SEO_TAGS.index.description} />
        <meta name="keyword" content={SEO_TAGS.index.keyword} />
        <meta name="og:title" content={SEO_TAGS.index.title} />
        <meta name="og:description" content={SEO_TAGS.index.description} />
      </Head>

      <HomePage {...props} />
    </div>
  );
};

Home.propTypes = {
  router: PropTypes.object,
};

Home.getInitialProps = async () => {
  const isLoggedIn = false;
  // props.DASHBOARD_GET_CATEGORY_API_CALL({});
  if (isLoggedIn) return {};

  return {};
};

export default AuthHoc(Home, false);
