import React from 'react';
import Head from 'next/head';
import AuthHoc from 'hoc/authCheck';
import PropTypes from 'prop-types';
import dynamic from 'next/dynamic';
import { SEO_TAGS } from 'utils/constants';
import { getData } from 'shared/utils';
import { toast } from 'react-toastify';
import { GApageView } from 'utils/google';
import MascotLoader from 'components/common/MascotLoader';

const HomePage = dynamic(() => import('containers/Articles/search'), {
  loading: () => <MascotLoader />,
});

const successToast = msg => toast.success(msg);

const errorToast = msg => toast.error(msg);
const Home = props => {
  const {
    router: { asPath },
  } = props;

  React.useEffect(() => {
    GApageView(asPath, 'Article Search Page');
  }, []);

  return (
    <div>
      <Head>
        <title>{SEO_TAGS.index.title}</title>
        <meta name="description" content={SEO_TAGS.index.description} />
        <meta name="keyword" content={SEO_TAGS.index.keyword} />
        <meta name="og:title" content={SEO_TAGS.index.title} />
        <meta name="og:description" content={SEO_TAGS.index.description} />
      </Head>
      <HomePage
        {...props}
        getData={getData}
        successToast={successToast}
        errorToast={errorToast}
      />
    </div>
  );
};
Home.propTypes = {
  query: PropTypes.object,
  router: PropTypes.object,
};

Home.getInitialProps = async props => {
  const {
    query: { key } = {},
    ARTICLES_SEARCH_API_CALL,
    GET_USER_BOOKMARK_ARTICLES_API_CALL,
    ARTICLES_GET_ALL_CATEGORIES_API_CALL,
    GET_HASHTAG_ARTICLES_API_CALL,
    GET_MOST_READ_ARTICLES_API_CALL,
  } = props;

  const isLoggedIn = false;

  const containsHash = key.slice(0, 1) === '#';
  if (containsHash) {
    GET_HASHTAG_ARTICLES_API_CALL({
      params: {
        hashtag: key.slice(1),
      },
    });
  } else {
    ARTICLES_SEARCH_API_CALL({
      query: { search: key },
    });
  }
  ARTICLES_GET_ALL_CATEGORIES_API_CALL({ filter: ['searchPageMainCat'] });
  GET_USER_BOOKMARK_ARTICLES_API_CALL();
  GET_MOST_READ_ARTICLES_API_CALL();

  if (isLoggedIn) return {};
  return {};
};

export default AuthHoc(Home, false);
