/* eslint-disable jsx-a11y/img-redundant-alt */
import React from 'react';
// import './style.css';
import { Group } from 'components/Group';
import { Breadcrumbs } from 'components/Breadcrumbs';

function Brand() {
  return (
    <>
      <div className="desktop">
        <div className="div">
          <div className="frame">
            <p className="founders-of-eurokids">
              <span className="span">Founders of Eurokids:</span>
              <span className="text-wrapper-2">&nbsp;</span>
              <span className="text-wrapper-3">
                Prajodh Rajan and Vikas Phadnis
                <br />
                <br />
              </span>
              <span className="text-wrapper-2">Awards to Eurokids:</span>
              <span className="text-wrapper-3">
                {' '}
                In 2018, it was awarded the Best Innovative and Experimental
                Preschool Chain Award by the Indian Education Awards. EuroKids
                was also recognized as one of the Top 100 Franchise
                Opportunities in India by Franchise India in 2019.
                <br />
                <br />
              </span>
              <span className="text-wrapper-2">Rating of Eurokids: </span>
              <span className="text-wrapper-3">
                EuroKids has received positive reviews and ratings from parents
                and guardians, reflecting its reputation for providing quality
                early childhood education. However, the specific rating may vary
                depending on the location and individual experiences.
                <br />
                <br />
              </span>
              <span className="text-wrapper-2">
                Address of Eurokids head-office:
              </span>
              <span className="text-wrapper-3">
                {' '}
                Windsor, 801, 8th Floor, Off, Manipada Rd, Kalina, Santacruz
                East Mumbai, Maharashtra 400098
                <br />
                <br />
                Contact details for Eurokids: tool free: 1800 209 5656,
                Corporate number: 022 6156 5656
                <br />
                <br />
                Geo location of Eurokids headoffice: 19.0694418,72.8606402
                <br />
                <br />
                Opening hours of Eurokids office: 9:30 AM to 5:30 PM, Monday to
                Friday
              </span>
            </p>
          </div>
          <div className="eurokids-is-a-unique-wrapper">
            <p className="eurokids-is-a-unique">
              <span className="text-wrapper-4">
                Eurokids is a unique and incomparable option for me as a parent.
                I have faith in Eurokids and feel secure knowing my children are
                in their care
                <br />
              </span>
              <span className="text-wrapper-5">
                Ms. Isha Kushal, Mothe of Aviraj
                <br />
              </span>
              <span className="text-wrapper-4">
                <br />I am thankful to have found amazing teachers for my
                daughter who emphasize academic learning excellence, training
                and a positive learning environment, leading to her remarkable
                development.
                <br />
              </span>
              <span className="text-wrapper-5">
                Ms. Natasha Gupta, Mother of Karen
              </span>
            </p>
          </div>
          <div className="frame-wrapper">
            <div className="div-wrapper">
              <div className="text-wrapper-6">Eurokids Organization</div>
            </div>
          </div>
          <div className="frame-2">
            <div className="div-wrapper">
              <div className="text-wrapper-6">FAQ about Eurokids</div>
            </div>
          </div>
          <div className="frame-3">
            <div className="div-wrapper">
              <div className="text-wrapper-6">Eurokids Center Reviews</div>
            </div>
          </div>
          <p className="p">Share this page with your friends</p>
          <div className="frame-4">
            <div className="rectangle" />
            <div className="frame-5">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="19"
                height="19"
                viewBox="0 0 19 19"
                fill="white"
              >
                <path
                  d="M9.5 0C4.25315 0 0 4.27904 0 9.55783C0 14.3281 3.47415 18.2822 8.0161 19V12.32H5.6031V9.55783H8.0161V7.45224C8.0161 5.05704 9.4335 3.73424 11.6042 3.73424C12.6435 3.73424 13.7303 3.92062 13.7303 3.92062V6.27184H12.5333C11.3525 6.27184 10.9848 7.00875 10.9848 7.76478V9.55783H13.6192L13.1983 12.32H10.9848V19C15.5258 18.2832 19 14.3272 19 9.55783C19 4.27904 14.7469 0 9.5 0Z"
                  fill="#FFFFFF"
                />
              </svg>
              {/* <LogoWhatsapp className="icon-instance-node" color="white" /> */}
              <div className="text-wrapper-7">Whatsapp</div>
            </div>
          </div>
          <div className="frame-6">
            <div className="rectangle-2" />
            <div className="frame-7">
              {/* <LogoFacebook className="icon-instance-node" /> */}
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="19"
                height="19"
                viewBox="0 0 19 19"
                fill="white"
              >
                <path
                  d="M9.5 0C4.25315 0 0 4.27904 0 9.55783C0 14.3281 3.47415 18.2822 8.0161 19V12.32H5.6031V9.55783H8.0161V7.45224C8.0161 5.05704 9.4335 3.73424 11.6042 3.73424C12.6435 3.73424 13.7303 3.92062 13.7303 3.92062V6.27184H12.5333C11.3525 6.27184 10.9848 7.00875 10.9848 7.76478V9.55783H13.6192L13.1983 12.32H10.9848V19C15.5258 18.2832 19 14.3272 19 9.55783C19 4.27904 14.7469 0 9.5 0Z"
                  fill="#FFFFFF"
                />
              </svg>
              <div className="text-wrapper-7">Facebook</div>
            </div>
          </div>
          <Group
            className="group-3"
            property1="default"
            text="What is the fee in eurokids?"
            text1="Once the fee details are available, will update here."
          />
          <Group
            className="group-instance"
            property1="default"
            text="Is food provided to children?"
            text1="Generally, eurokids preschools don’t provide food to children. It is expected that parents send water and snacks for children. Children can eat the food during the breaks. Teachers and staff are available to help the children"
          />
          <Group
            className="group-3-instance"
            property1="default"
            text="What are the safety measures in eurokids preschool?"
            text1="Eurokids is India’s first safe school network complete with CCTV surveillance and monitor 24/7, adherence to early education safety regulations, 360 safety certifications and regular inspection, background verification of teachers and staff, doctor on call &amp; medical preparedness, and child-friendly classrooms and play area."
          />
          <Group
            className="design-component-instance-node"
            property1="default"
            text="Is eurokids franchise profitable?"
            text1="The profitability of a EuroKids franchise can vary depending on several factors such as location, local market demand, competition, operational costs, and the ability of the franchise owner to effectively manage and promote the business. Franchise profitability is generally influenced by factors like initial investment, ongoing expenses, revenue generation, brand reputation, and customer satisfaction. It is essential to conduct thorough research, including speaking with current franchise owners and reviewing the franchisor&#39;s financial disclosures, to assess the potential profitability of a specific EuroKids franchise opportunity. Before investing in any franchise, it is advisable to seek advice from financial advisors, consult with existing franchisees, and carefully evaluate the franchise agreement to make an informed decision."
          />
          <Group
            className="group-2"
            property1="default"
            text="How much does it cost to start a eurokids franchise?"
            text1="To become a EuroKids franchisee, you need an investment ranging from ₹12-15 lakh*. This investment covers the initial setup costs, including infrastructure, training, and the EuroKids brand fee. EuroKids offers a strong return on investment (ROI) and a well-established revenue model, making it a lucrative business opportunity."
          />
          <Group
            className="group-4"
            property1="default"
            text="What is the language used to communicate with children?"
            text1="At EuroKids, the main language used for instructions and interaction is English. However, during the initial stages of the PlayGroup and Nursery programs, communication is conducted in the regional language or the child&#39;s mother tongue until they begin to understand English."
          />
          <Group
            className="group-5"
            property1="default"
            text="What is the teacher : student ratio at EuroKids?"
            text1="Eurokids ensures that each child receives individual attention by maintaining small batch sizes, resulting in a teacher-to-student ratio of 1:10 in PlayGroup and 1:15 in KinderGarten. Additionally, for Pre-School, the adult-to-child ratio is 1:6, prioritizing child safety and catering to every child&#39;s needs effectively."
          />
          <div className="group-6">
            <p className="how-can-i-take-2">
              What is the Eurokids admissions process?
            </p>
            <p className="you-can-talk-to-the-2">
              <span className="text-wrapper-8">
                Choosing the right preschool or daycare for your child is an
                important decision that requires thorough research and
                consideration. That&#39;s where{' '}
              </span>
              <a
                href="https://www.kiddenz.com/"
                rel="noopener noreferrer"
                target="_blank"
              >
                <span className="text-wrapper-9">Kiddenz</span>
              </a>
              <span className="text-wrapper-8">
                , an online platform dedicated to early childhood education, can
                assist you.{' '}
              </span>
              <a
                href="https://kiddenz.com"
                rel="noopener noreferrer"
                target="_blank"
              >
                <span className="text-wrapper-9">Kiddenz</span>
              </a>
              <span className="text-wrapper-8">
                {' '}
                offers a comprehensive database of preschools and daycares,
                providing detailed information on facilities, curriculum, staff
                qualifications, and parent reviews.
                <br />
                <br />
                At eurokids, enrollments for Nursery, EuroJunior (Junior KG),
                and EuroSenior (Senior KG) programs are available from January
                through August. The last date for full-term admission to these
                programs is the 31st of August. Additionally, we offer mid-term
                admissions for Nursery between October and December. As for
                PlayGroup, admissions are open for a significant part of the
                year.
              </span>
            </p>
            <img
              className="img"
              alt="Chevron down"
              src="https://c.animaapp.com/QFxCdMhV/img/chevron-down-9.svg"
            />
          </div>
          <img
            className="line"
            alt="Line"
            src="https://c.animaapp.com/QFxCdMhV/img/line-3.svg"
          />
          <img
            className="line-2"
            alt="Line"
            src="https://c.animaapp.com/QFxCdMhV/img/line-3.svg"
          />
          <div className="frame-8">
            <div className="frame-9">
              <p className="text-wrapper-10">
                EuroKids Preschools: Building Strong Foundations for Lifelong
                Learning
              </p>
            </div>
            <div className="eurokids-preschools-wrapper">
              <p className="eurokids-preschools">
                EuroKids Preschools, a leading name in the early childhood
                education domain, stands as a beacon of excellence, nurturing
                young minds with a commitment to fostering holistic development.
                EuroKids was founded in 2001 by Prajodh Rajan and Vikas Phadnis
                in Mumbai, India. Currently it has more than 1200 centers in
                over 350 cities and over 5,00,000 children have nurtured at
                eurokids centers.
                <br />
                <br />
                EuroKids has established itself as a trusted partner for parents
                seeking the best start for their children&#39;s educational
                journey. Eurokids is India’s first safe school network complete
                with CCTV surveillance and monitori24/7, adherence to early
                education safety regulations, 360 safety certifications and
                regular inspection, background verification of teachers and
                staff, doctor on call &amp; medical preparedness, and
                child-friendly classrooms and play area.
                <br />
                <br />
                At EuroKids, the essence of learning lies in creating a vibrant
                and nurturing environment that sparks a child&#39;s curiosity
                and fuels their desire to explore the world around them. Every
                aspect of their curriculum is meticulously designed to cater to
                the unique learning needs of children aged 2 to 6 years. With a
                perfect blend of play-based learning and structured academics,
                EuroKids ensures that children not only embrace knowledge but
                also develop critical life skills that will serve them well in
                their future endeavors.
              </p>
            </div>
          </div>
          <div className="additionally-wrapper">
            <p className="additionally">
              Additionally, EuroKids embraces modern teaching methodologies and
              leverages cutting-edge technology to enhance the learning
              experience, keeping pace with the evolving educational landscape.
              Their commitment to maintaining a low student-teacher ratio
              ensures personalized attention, fostering a strong teacher-student
              bond that nurtures each child&#39;s unique talents and strengths.
              <br />
              <br />
              The curriculum at EuroKids is based on the theory of multiple
              intelligences, which emphasizes that each child has a unique
              learning style and intelligence. To cater to this, EuroKids uses a
              variety of teaching techniques and methods, such as storytelling,
              music, and role-playing, to ensure that every child is engaged and
              learning at their own pace.
              <br />
              <br />
              Apart from the regular preschool programs, EuroKids also offers
              after-school programs and summer camps for children. These
              programs are designed to provide children with additional learning
              opportunities and help them explore their interests.
              <br />
              Here are some key components that might be found in their
              curriculum:
            </p>
          </div>
          <p className="element-play-based">
            <span className="text-wrapper-5">1. Play-based learning:</span>
            <span className="text-wrapper-4">&nbsp;</span>
            <span className="text-wrapper-11">
              EuroKids Preschools often emphasize play-based learning,
              recognizing that young children learn best through hands-on
              experiences, exploration, and creative activities.
              <br />
            </span>
            <span className="text-wrapper-5">
              <br />
              2. Language and literacy development:
            </span>
            <span className="text-wrapper-11">
              {' '}
              The curriculum may include activities that promote language
              skills, such as story reading, rhymes, phonics, vocabulary
              building, and early writing exercises.
              <br />
            </span>
            <span className="text-wrapper-4">
              <br />
            </span>
            <span className="text-wrapper-5">3. Numeracy and math skills:</span>
            <span className="text-wrapper-4">&nbsp;</span>
            <span className="text-wrapper-11">
              Basic mathematical concepts like counting, number recognition,
              shapes, and patterns are introduced through interactive and
              engaging methods.
              <br />
              <br />
            </span>
            <span className="text-wrapper-5">4. Science and discovery:</span>
            <span className="text-wrapper-11">
              {' '}
              Preschoolers are encouraged to explore the world around them
              through science-based activities, experiments, and nature-based
              learning.
              <br />
              <br />
            </span>
            <span className="text-wrapper-5">
              5. Social and emotional development:
            </span>
            <span className="text-wrapper-11">
              {' '}
              EuroKids may incorporate activities to help children develop
              social skills, emotional intelligence, empathy, and teamwork.
              <br />
              <br />
            </span>
            <span className="text-wrapper-5">
              6. Fine and gross motor skill development:{' '}
            </span>
            <span className="text-wrapper-11">
              Activities such as drawing, coloring, cutting, running, jumping,
              and other physical exercises are included to improve motor skills.
              <br />
              <br />
            </span>
            <span className="text-wrapper-5">7. Arts and crafts: </span>
            <span className="text-wrapper-11">
              Creative expression is encouraged through arts and crafts
              activities, allowing children to develop their imagination and
              fine motor skills.
              <br />
            </span>
            <span className="text-wrapper-5">
              <br />
              8. Music and movement:{' '}
            </span>
            <span className="text-wrapper-11">
              Preschoolers may be introduced to music, dance, and rhythm to
              promote self-expression and coordination.
              <br />
              <br />
            </span>
            <span className="text-wrapper-5">
              9. Cultural and global awareness:
            </span>
            <span className="text-wrapper-11">
              {' '}
              EuroKids often aims to expose children to diverse cultures,
              traditions, and customs to promote understanding and inclusivity.
              <br />
              <br />
            </span>
            <span className="text-wrapper-5">10. Field trips and events:</span>
            <span className="text-wrapper-11">
              {' '}
              Some EuroKids Preschools organize field trips and events to
              provide children with real-world experiences beyond the classroom.
            </span>
          </p>
          <div className="frame-10">
            <p className="text-wrapper-12">
              Sometimes also referred to as playschool is the suitable for
              children between 2 to 3 years of age. Playgroup/playschool
              activities focus on holistic development through play, arts, and
              learning. Children engage in free play, circle time with songs and
              stories, arts and crafts, outdoor play, and pretend play. They
              explore music and sensory activities, fostering cognitive, social,
              and physical skills while celebrating diversity and organizing
              field trips for real-world experiences.
            </p>
          </div>
          <div className="frame-11">
            <p className="text-wrapper-12">
              Preschool fees can vary depending on the reputation of the
              institution, location, facilities, curriculum, and the duration of
              the program. In urban areas and metropolitan cities, preschool
              fees tend to be higher compared to smaller towns and rural areas.
              On average, monthly fees for preschool in India can range from
              ₹3,000 to ₹15,000 or more. Some prestigious preschools may have
              higher fees due to their brand value and exclusive facilities.
              Additionally, preschools may charge an admission or registration
              fee at the time of enrollment.
            </p>
          </div>
          <div className="daycare-fees-are-wrapper">
            <p className="text-wrapper-12">
              Daycare fees are typically calculated based on the number of hours
              or the duration of care required. Full-day daycare, which provides
              care for the entire working day, generally has higher fees
              compared to half-day or part-time options. The cost of daycare in
              India can range from ₹4,000 to ₹15,000 or more per month,
              depending on the location and additional services provided. Some
              daycare centers may offer additional services such as meals,
              transportation, and extracurricular activities, which can
              influence the overall cost.
              <br />
              <br />
              It&#39;s important to note that these fee ranges are approximate
              and can vary based on the specific preschool or daycare center, as
              well as the region in which it is located. It&#39;s advisable to
              research and compare fees from multiple providers in your desired
              location to get a better understanding of the pricing range.
              <br />
              <br />
              Additionally, it&#39;s crucial to consider other expenses that may
              be associated with preschool or daycare, such as uniforms, books,
              materials, transportation, meals, and any additional services or
              activities offered by the institution. These additional expenses
              can impact the overall cost of your child&#39;s preschool or
              daycare experience.
              <br />
              <br />
              Furthermore, some preschools and daycares in India may offer fee
              structures with different payment options, such as monthly,
              quarterly, or annual payments. It&#39;s important to inquire about
              the fee structure and payment policies of each preschool or
              daycare center you are considering.
              <br />
              <br />
              When evaluating the costs, it&#39;s essential to strike a balance
              between your budget and the quality of care and education
              provided. Remember that the cost should not be the sole
              determining factor in selecting a preschool or daycare center for
              your child. Consider factors such as the reputation of the
              institution, curriculum, facilities, safety measures, teacher
              qualifications, and parent reviews to make an informed decision.
              <br />
              <br />
              The cost of preschool and daycare in India can vary significantly
              based on factors such as location, facilities, curriculum,
              duration of care, and additional services provided. Preschool fees
              typically range from ₹3,000 to ₹15,000 or more per month, while
              daycare fees can range from ₹4,000 to ₹15,000 or more per month.
              It&#39;s important to research and compare fees from multiple
              providers in your desired location to get a better understanding
              of the pricing range. However, it&#39;s crucial to consider other
              aspects, such as the quality of care, educational programs,
              facilities, and safety measures when making a decision. Striking a
              balance between affordability and quality is key to ensuring a
              positive and enriching early education experience for your child.
            </p>
          </div>
          <div className="frame-12">
            <p className="text-wrapper-12">
              Nursery is for children between 3 to 4 years of age. In the
              preschool nursery, little ones joyfully engage in a wide range of
              activities. They revel in free play, circle time with enchanting
              songs and stories, creative arts and crafts, exciting outdoor
              adventures, and imaginative play. As they delve into the world of
              music, basic math, and science, they blossom intellectually,
              socially, and physically, preparing for their bright educational
              path ahead.
            </p>
          </div>
          <div className="frame-13">
            <p className="text-wrapper-12">
              EuroJunior is equivalent to LKG (Lower Kindergarten) education.
              Children between 4 to 5 years generally qualify for LKG
              admissions. In Lower Kindergarten (LKG), children are introduced
              to a well-rounded curriculum that nurtures their cognitive,
              social, emotional, and motor skills. Kindergarten English
              worksheets and Math worksheets provide age-appropriate language
              and math exercises for young learners. Kindergarten worksheets
              encompass various subjects and activities to promote cognitive,
              motor, and social skills. Kindergarten activities include arts,
              crafts, music, storytelling, and more, fostering a holistic
              learning experience for children. These resources play a crucial
              role in preparing kindergartners for their educational journey in
              a fun and engaging manner. They learn language development through
              phonics and basic sight words, early math concepts, and engage in
              art, craft, and physical activities to develop fine and gross
              motor skills. Social skills are encouraged through peer
              interactions, while exposure to environmental studies,
              storytelling, and music fosters a love for learning. LKG
              curriculum is designed to be age-appropriate, playful, and
              enjoyable, laying a strong foundation for their educational
              journey.
            </p>
          </div>
          <div className="frame-14">
            <p className="text-wrapper-12">
              EuroSenior is equivalent to UKG (Upper Kindergarten) education.
              Children between 5 to 6 years generally qualify for UKG
              admissions. In Upper Kindergarten (UKG), children continue to
              explore a well-rounded curriculum that nurtures their cognitive,
              social, emotional, and motor skills. The UKG syllabus includes
              kindergarten English worksheets and Math worksheets, offering
              age-appropriate language and math exercises that build upon their
              previous learning. Along with a diverse range of UKG worksheets
              and activities encompassing various subjects, they promote
              cognitive, motor, and social skills, enhancing their knowledge and
              capabilities. Engaging in UKG activities such as arts, crafts,
              music, storytelling, and more continues to foster a holistic
              learning experience, preparing them for further educational
              milestones in an enjoyable and engaging manner. The UKG syllabus
              emphasizes further language development, building on phonics and
              sight words, along with more advanced math concepts. Additionally,
              UKG children continue to participate in art, craft, and physical
              activities to refine their fine and gross motor skills. Social
              skills are further encouraged through meaningful peer
              interactions, while they continue to explore environmental
              studies, storytelling, and music, deepening their love for
              learning. The carefully crafted UKG syllabus ensures
              age-appropriate, playful, and enjoyable learning, providing a
              strong foundation for their ongoing educational journey.
            </p>
          </div>
          <div className="frame-15">
            <p className="text-wrapper-12">
              EuroKids, a renowned and leading preschool chain, offers exciting
              franchise opportunities for aspiring entrepreneurs and education
              enthusiasts who are passionate about shaping the future of early
              education. With a strong foundation built on over two decades of
              experience, EuroKids has established itself as a trusted brand in
              the preschool industry, known for its quality education,
              child-centric approach, and commitment to fostering holistic
              development in young minds.
            </p>
          </div>
          <div className="in-the-bustling-wrapper">
            <p className="in-the-bustling">
              In the bustling world of early education, EuroKids faces
              formidable competition from several renowned players in the
              industry. As parents seek the best start for their children&#39;s
              educational journey, a plethora of preschool chains have emerged,
              each offering unique approaches and strengths.
              <br />
              <br />
              One of EuroKids&#39; leading competitors is Kidzee, a preschool
              giant known for its vast network and comprehensive curriculum.
              Kidzee emphasizes holistic development, encompassing language,
              math, science, and social skills, catering to the diverse needs of
              young learners.
              <br />
              <br />
              Shemrock Preschool, with its innovative &#34;SheStandard&#34;
              approach, focuses on providing quality education and consistent
              learning experiences across its franchises. Meanwhile, Bachpan
              Play School captivates parents with its nurturing environment and
              engaging curriculum.
              <br />
              <br />
              Podar Jumbo Kids is another notable contender, offering a blend of
              academics and extracurricular activities to foster well-rounded
              development. Little Millennium Preschool&#39;s creative teaching
              methods and Hello Kids Preschool&#39;s child-centric approach are
              also noteworthy competitors.
              <br />
              <br />
              Joining the fray are Little Elly and Sanfort Preschool, which
              endeavor to create nurturing environments that inspire a love for
              learning in children. The esteemed Maple Bear Canadian Preschool
              offers an international perspective, implementing the renowned
              Canadian early childhood education methodology.
              <br />
              <br />
              As parents navigate this landscape, they are presented with a
              myriad of options to suit their child&#39;s unique needs and
              preferences. The competition fosters a culture of continuous
              improvement and innovation, ensuring that preschool chains strive
              to deliver the best possible early education experiences.
              <br />
              <br />
              In this dynamic and vibrant industry, EuroKids continues to stand
              tall, drawing from its rich legacy and dedication to holistic
              development. By recognizing and embracing the competition, parents
              are empowered to make informed decisions for their child&#39;s
              bright future, while young learners embark on an enriching and
              fulfilling educational journey.
            </p>
          </div>
          <div className="frame-16">
            <p className="text-wrapper-12">
              To become a EuroKids franchisee, you need an investment ranging
              from ₹12-15 lakh*. This investment covers the initial setup costs,
              including infrastructure, training, and the EuroKids brand fee.
              EuroKids offers a strong return on investment (ROI) and a
              well-established revenue model, making it a lucrative business
              opportunity.
            </p>
          </div>
          <div className="frame-17">
            <p className="text-wrapper-12">
              As a EuroKids franchise partner, you become a part of a long-term
              vision to provide quality early education to young children.
              EuroKids is dedicated to shaping the leaders of tomorrow and
              nurturing a love for learning from an early age. By joining the
              EuroKids family, you contribute to the growth and development of
              young minds, creating a positive impact on society as a whole.
            </p>
          </div>
          <div className="to-ensure-a-spacious-wrapper">
            <p className="text-wrapper-12">
              To ensure a spacious and conducive learning environment, a minimum
              of 1500 sq ft space is required for setting up a EuroKids
              preschool. The franchise team will assist you in selecting the
              ideal location and guide you through the setup process to create
              an inviting and child-friendly preschool facility.
              <br />
              <br />
              *Please note that the investment amount mentioned is subject to
              change and may vary based on the location and specific
              requirements of the franchise.
            </p>
          </div>
          <div className="element-established-brand-wrapper">
            <p className="text-wrapper-12">
              1. Established Brand: As a EuroKids franchisee, you become a part
              of a well-established and recognized brand with a proven track
              record of success. The trust and credibility associated with the
              EuroKids name will attract parents seeking the best early
              education for their children.
              <br />
              <br />
              2. Comprehensive Support: EuroKids provides comprehensive support
              to its franchise partners, ranging from site selection and setup
              to curriculum implementation and marketing strategies. A team of
              experienced professionals will guide you at every step, ensuring a
              smooth and successful launch of your preschool.
              <br />
              <br />
              3. Play-Based Curriculum: The EuroKids curriculum is designed to
              offer a well-rounded and play-based learning experience. It
              focuses on nurturing cognitive, emotional, social, and physical
              development, setting the foundation for a child&#39;s lifelong
              love for learning.
              <br />
              <br />
              4. Teacher Training: EuroKids places great importance on teacher
              training to ensure that educators are equipped with the necessary
              skills and knowledge to create a nurturing and stimulating
              learning environment for children.
              <br />
              <br />
              5. Marketing and Advertising: Franchisees benefit from
              EuroKids&#39; marketing and advertising initiatives, including
              national and local campaigns, to attract and retain students and
              build a strong preschool community.
              <br />
              <br />
              6. Continuous Innovation: EuroKids consistently updates and
              enhances its curriculum and programs to stay at the forefront of
              early childhood education, incorporating the latest research and
              best practices.
              <br />
              <br />
              Investing in a EuroKids franchise is not just a business
              opportunity; it is a chance to play a significant role in shaping
              the future of early education. With a well-established brand,
              comprehensive support, play-based curriculum, and a long-term
              vision, EuroKids offers a rewarding and fulfilling journey for
              aspiring entrepreneurs. Take the first step towards making a
              positive impact on young lives by becoming a EuroKids franchise
              partner and fostering a love for learning in the leaders of
              tomorrow.
            </p>
          </div>
          <div className="frame-18">
            <div className="frame-19">
              <div className="text-wrapper-6">Popular Eurokids Centers</div>
            </div>
            <div className="kiddenz-a-renowned-wrapper">
              <p className="kiddenz-a-renowned">
                Kiddenz, a renowned search platform for preschools and daycares,
                has created a specialized list for those searching for
                information on the top Eurokids preschools and daycares. Kiddenz
                helps parents find the perfect preschool or daycare that meets
                their child&#39;s needs. Through partnerships with multiple
                Eurokids centers, Kiddenz provides parents with exceptional
                preschool and daycare options of the highest quality
              </p>
            </div>
          </div>
          <div className="frame-20">
            <div className="text-wrapper-6">Eurokids Curriculum</div>
          </div>
          <div className="frame-21">
            <div className="frame-22">
              <div className="text-wrapper-6">
                Pre-school Programs at Eurokids
              </div>
            </div>
          </div>
          <div className="frame-23">
            <div className="frame-22">
              <div className="text-wrapper-6">Eurokids Fee Structure</div>
            </div>
          </div>
          <div className="frame-24">
            <div className="frame-22">
              <div className="text-wrapper-13">1. Playgroup</div>
            </div>
          </div>
          <div className="frame-25">
            <div className="frame-22">
              <div className="text-wrapper-13">Preschool:</div>
            </div>
          </div>
          <div className="frame-26">
            <div className="frame-22">
              <div className="text-wrapper-13">Daycare:</div>
            </div>
          </div>
          <div className="frame-27">
            <div className="frame-22">
              <div className="text-wrapper-13">2. Nursery</div>
            </div>
          </div>
          <div className="frame-28">
            <div className="frame-22">
              <div className="text-wrapper-13">3. EuroJunior</div>
            </div>
          </div>
          <div className="frame-29">
            <div className="frame-22">
              <div className="text-wrapper-13">4. EuroSenior</div>
            </div>
          </div>
          <div className="frame-30">
            <div className="div-wrapper">
              <div className="text-wrapper-6">Eurokids Franchise</div>
            </div>
          </div>
          <div className="frame-31">
            <div className="div-wrapper">
              <div className="text-wrapper-6">Eurokids Competitiors</div>
            </div>
          </div>
          <div className="frame-32">
            <div className="div-wrapper">
              <p className="text-wrapper-13">
                Eurokid franchise cost (Investment* required):
              </p>
            </div>
          </div>
          <div className="frame-33">
            <div className="div-wrapper">
              <div className="text-wrapper-13">Long-term Vision:</div>
            </div>
          </div>
          <div className="frame-34">
            <div className="div-wrapper">
              <p className="text-wrapper-13">
                Minimum 1500 sq ft Place Required:
              </p>
            </div>
          </div>
          <div className="frame-35">
            <div className="div-wrapper">
              <div className="text-wrapper-13">
                Why Choose EuroKids Franchise?
              </div>
            </div>
          </div>
          <div className="frame-36">
            <div className="frame-22">
              <p className="text-wrapper-6">
                Eurokids Average Fee in different cities
              </p>
            </div>
          </div>
          <div className="frame-37">
            <div className="rectangle-3" />
            <div className="rectangle-4" />
            <div className="rectangle-4" />
            <div className="rectangle-4" />
            <div className="rectangle-4" />
            <div className="rectangle-4" />
            <div className="rectangle-4" />
            <div className="rectangle-4" />
            <div className="rectangle-4" />
            <div className="rectangle-4" />
            <div className="rectangle-4" />
            <div className="rectangle-4" />
            <div className="rectangle-4" />
            <div className="rectangle-4" />
            <div className="rectangle-4" />
            <div className="rectangle-4" />
            <div className="rectangle-4" />
            <div className="rectangle-4" />
            <div className="text-wrapper-14">Daycare/month</div>
            <div className="text-wrapper-15">8000 to 12000</div>
            <div className="text-wrapper-16">8000 to 12000</div>
            <div className="text-wrapper-17">8000 to 12000</div>
            <div className="text-wrapper-18">8000 to 12000</div>
            <div className="text-wrapper-19">8000 to 12000</div>
            <div className="text-wrapper-20">Preschool/year</div>
            <div className="text-wrapper-21">50000 to 80000</div>
            <div className="text-wrapper-22">50000 to 80000</div>
            <div className="text-wrapper-23">50000 to 80000</div>
            <div className="text-wrapper-24">50000 to 80000</div>
            <div className="text-wrapper-25">50000 to 80000</div>
            <div className="text-wrapper-26">City</div>
            <div className="text-wrapper-27">Bengaluru</div>
            <div className="text-wrapper-28">Pune</div>
            <div className="text-wrapper-29">Chennai</div>
            <div className="text-wrapper-30">Hyderabad</div>
            <div className="text-wrapper-31">Mumbai</div>
          </div>
          <img
            className="line-3"
            alt="Line"
            src="https://c.animaapp.com/QFxCdMhV/img/line-3.svg"
          />
          <img
            className="line-4"
            alt="Line"
            src="https://c.animaapp.com/QFxCdMhV/img/line-3.svg"
          />
          <img
            className="line-5"
            alt="Line"
            src="https://c.animaapp.com/QFxCdMhV/img/line-3.svg"
          />
          <img
            className="line-6"
            alt="Line"
            src="https://c.animaapp.com/QFxCdMhV/img/line-3.svg"
          />
          <img
            className="line-7"
            alt="Line"
            src="https://c.animaapp.com/QFxCdMhV/img/line-3.svg"
          />
          <img
            className="line-8"
            alt="Line"
            src="https://c.animaapp.com/QFxCdMhV/img/line-3.svg"
          />
          <img
            className="line-9"
            alt="Line"
            src="https://c.animaapp.com/QFxCdMhV/img/line-3.svg"
          />
          <img
            className="line-10"
            alt="Line"
            src="https://c.animaapp.com/QFxCdMhV/img/line-3.svg"
          />
          <img
            className="chevron-left"
            alt="Chevron left"
            src="https://c.animaapp.com/QFxCdMhV/img/chevron-left-1.svg"
          />
          <img
            className="chevron-left-2"
            alt="Chevron left"
            src="https://c.animaapp.com/QFxCdMhV/img/chevron-left.svg"
          />
          <p className="eurokids">
            Eurokids basaveshwara nagar <br />
            Eurokids horamavu <br />
            Eurokids yelahanka <br />
            Eurokids darasahalli
          </p>
          <p className="eurokids-magarpatta">
            Eurokids magarpatta city <br />
            Eurokids bhugaon&nbsp;&nbsp;
            <br />
            Eurokids baner&nbsp;&nbsp;
            <br />
            Eurokids bhawani peth
          </p>
          <p className="eurokids-kalyan">
            <span className="text-wrapper-32">
              Eurokids kalyan <br />
            </span>
            <a
              href="https://kiddenz.com/mumbai/palava/euro-kids-1709332"
              rel="noopener noreferrer"
              target="_blank"
            >
              <span className="text-wrapper-32">Eurokids palava</span>
            </a>
          </p>
          <div className="text-wrapper-33">
            Eurokids agarwal dharmashala road
          </div>
          <a
            className="text-wrapper-34"
            href="https://kiddenz.com/chennai/ashok-nagar/euro-kids-1467639"
            rel="noopener noreferrer"
            target="_blank"
          >
            Eurokids ashoknagar
          </a>
          <a
            className="text-wrapper-35"
            href="https://kiddenz.com/lucknow/jankipuram/euro-kids-jankipuram-1543553"
            rel="noopener noreferrer"
            target="_blank"
          >
            Eurokids jankipuram
          </a>
          <a
            className="text-wrapper-36"
            href="https://kiddenz.com/hyderabad/bandlaguda-jagir/eurokids-1405914"
            rel="noopener noreferrer"
            target="_blank"
          >
            Eurokids bandlaguda
          </a>
          <p className="eurokids-jayanagar">
            Eurokids Jayanagar <br />
            Eurokids chandapura <br />
            Eurokids yelahanka new town <br />
            Eurokids rt nagar
          </p>
          <p className="eurokids-kharadi">
            Eurokids kharadi <br />
            Eurokids dhayari <br />
            Eurokids kothrud
          </p>
          <div className="eurokids-kalamboli">
            Eurokids&nbsp;&nbsp;kalamboli
          </div>
          <div className="text-wrapper-37">Eurokids model town</div>
          <p className="eurokids-rmv">
            Eurokids rmv 2nd stage <br />
            Eurokids rajajinagar <br />
            Eurokids vidyaranyapura <br />
            Eurokids benniganahalli
          </p>
          <p className="eurokids-wadgaon">
            Eurokids wadgaon sheri <br />
            Eurokids dhanori <br />
            Eurokids wagholi
          </p>
          <div className="text-wrapper-38">Eurokids Bhandup west</div>
          <a
            className="text-wrapper-39"
            href="https://kiddenz.com/new-delhi/mayur-vihar/euro-kids-1289506"
            rel="noopener noreferrer"
            target="_blank"
          >
            Eurokids mayur vihar
          </a>
          <div className="frame-38">
            <div className="frame-22">
              <div className="text-wrapper-6">Bangalore -</div>
            </div>
          </div>
          <div className="frame-39">
            <div className="frame-22">
              <div className="text-wrapper-6">Pune -</div>
            </div>
          </div>
          <div className="frame-40">
            <div className="frame-22">
              <div className="text-wrapper-6">Mumbai -</div>
            </div>
          </div>
          <div className="frame-41">
            <div className="frame-22">
              <div className="text-wrapper-6">Eurokids New Delhi -</div>
            </div>
          </div>
          <div className="frame-42">
            <div className="frame-22">
              <div className="text-wrapper-6">Eurokids Chennai -</div>
            </div>
          </div>
          <div className="frame-43">
            <div className="frame-22">
              <div className="text-wrapper-6">Eurokids Lucknow -</div>
            </div>
          </div>
          <div className="frame-44">
            <div className="frame-22">
              <div className="text-wrapper-6">Eurokids Hyderabad -</div>
            </div>
          </div>
          <div className="overlap">
            <img
              className="rectangle-5"
              alt="Rectangle"
              src="https://c.animaapp.com/QFxCdMhV/img/rectangle-34.png"
            />
            <div className="rectangle-6" />
            <p className="text-wrapper-40">
              “EuroKids: Best Preschool for Kids, Nursery, Kindergarden”
            </p>
            <div className="ellipse" />
            <img
              className="image"
              alt="Image"
              src="https://c.animaapp.com/QFxCdMhV/img/image-1@2x.png"
            />
            <div className="overlap-group-wrapper">
              <div className="overlap-group">
                <div className="text-wrapper-41">Our Services</div>
                <img
                  className="image-2"
                  alt="Image"
                  src="https://c.animaapp.com/QFxCdMhV/img/image-2@2x.png"
                />
                <div className="image-wrapper">
                  <img
                    className="image-3"
                    alt="Image"
                    src="https://c.animaapp.com/QFxCdMhV/img/image-3@2x.png"
                  />
                </div>
                <div className="playschool-wrapper">
                  <img
                    className="playschool"
                    alt="Playschool"
                    src="https://c.animaapp.com/QFxCdMhV/img/playschool-1-982058-1@2x.png"
                  />
                </div>
              </div>
            </div>
            <p className="text-wrapper-42">
              EuroKids is a leading name in early childhood education, dedicated
              to nurturing young minds through innovative and holistic learning
              approaches. EuroKids provides a vibrant and safe environment where
              children can grow, thrive, and develop essential skills for a
              bright future.
            </p>
            <a
              className="text-wrapper-43"
              href="https://www.eurokidsindia.com/"
              rel="noopener noreferrer"
              target="_blank"
            >
              Preschool
            </a>
            <a
              className="text-wrapper-44"
              href="https://www.eurokidsindia.com/corporate-daycare-solutions/"
              rel="noopener noreferrer"
              target="_blank"
            >
              Daycare
            </a>
            <a
              className="text-wrapper-45"
              href="https://www.eurokidsindia.com/"
              rel="noopener noreferrer"
              target="_blank"
            >
              Playschool
            </a>
          </div>
          <img
            className="copy-of-kiddenz-logo"
            alt="Copy of kiddenz logo"
            src="https://c.animaapp.com/QFxCdMhV/img/copy-of-kiddenz-logo-03-3.svg"
          />
          <div className="text-wrapper-46">About</div>
          <div className="text-wrapper-47">Parenting Articles</div>
          <div className="text-wrapper-48">For Providers</div>
          <div className="frame-45">
            <div className="text-wrapper-49">Login</div>
          </div>
          <div className="frame-46">
            <div className="text-wrapper-50">Register</div>
          </div>
          <Breadcrumbs className="breadcrumbs-instance" />
          <img
            className="rectangle-7"
            alt="Rectangle"
            src="https://c.animaapp.com/QFxCdMhV/img/rectangle-39@2x.png"
          />
          <img
            className="rectangle-8"
            alt="Rectangle"
            src="https://c.animaapp.com/QFxCdMhV/img/rectangle-40@2x.png"
          />
          <img
            className="rectangle-9"
            alt="Rectangle"
            src="https://c.animaapp.com/QFxCdMhV/img/rectangle-41@2x.png"
          />
          <div className="text-wrapper-51">Children doing Yoga</div>
          <div className="text-wrapper-52">Exercise</div>
          <div className="text-wrapper-53">Sports Activity</div>
          <a
            className="text-wrapper-54"
            href="https://www.eurokidsindia.com/preschool-in-bangalore/"
            rel="noopener noreferrer"
            target="_blank"
          >
            “Eurokids in Bengaluru (10)”
          </a>
          <a
            className="text-wrapper-55"
            href="https://www.eurokidsindia.com/preschool-in-pune/"
            rel="noopener noreferrer"
            target="_blank"
          >
            “Eurokids in Pune”
          </a>
          <a
            className="text-wrapper-56"
            href="https://www.eurokidsindia.com/preschool-in-delhi/"
            rel="noopener noreferrer"
            target="_blank"
          >
            “Eurokids in Delhi”
          </a>
          <a
            className="text-wrapper-57"
            href="https://www.eurokidsindia.com/preschool-in-mumbai/"
            rel="noopener noreferrer"
            target="_blank"
          >
            “Eurokids in Mumbai”
          </a>
          <a
            className="text-wrapper-58"
            href="https://www.eurokidsindia.com/preschool-in-kolkata/"
            rel="noopener noreferrer"
            target="_blank"
          >
            “Eurokids in Kolkata”
          </a>
          <a
            className="text-wrapper-59"
            href="https://www.eurokidsindia.com/preschool-in-chennai/"
            rel="noopener noreferrer"
            target="_blank"
          >
            “Eurokids in Chennai”
          </a>
          <a
            className="text-wrapper-60"
            href="https://www.eurokidsindia.com/preschool-in-nagpur/"
            rel="noopener noreferrer"
            target="_blank"
          >
            “Eurokids in Nagpur”
          </a>
          <a
            className="text-wrapper-61"
            href="https://www.eurokidsindia.com/preschool-in-ahmedabad/"
            rel="noopener noreferrer"
            target="_blank"
          >
            “Eurokids in Ahmedabad”
          </a>
          <a
            className="text-wrapper-62"
            href="https://www.eurokidsindia.com/preschool-in-hyderabad/"
            rel="noopener noreferrer"
            target="_blank"
          >
            “Eurokids in Hyderbad”
          </a>
          <div className="text-wrapper-63">Show more</div>
        </div>
      </div>
    </>
  );
}

export default Brand;
