/* eslint-disable no-console */
import React from 'react';
import Head from 'next/head';
import PropTypes from 'prop-types';
import AuthHoc from 'hoc/authCheck';
import dynamic from 'next/dynamic';
import { SEO_TAGS } from 'utils/constants';
import { getData } from 'shared/utils';
import { GApageView } from 'utils/google';
import { toast } from 'react-toastify';
import MascotLoader from 'components/common/MascotLoader';
import Modal from 'components/Modal';
/*
NOTE: add documentjs and next to 9.0.2 for prod  else ^9.3.6
*/

const HomePage = dynamic(() => import('containers/LandingPage'), {
  loading: () => <MascotLoader />,
});

const successToast = msg => toast.success(msg);

const errorToast = msg => toast.error(msg);
const Home = props => {
  const {
    router: { asPath },
  } = props;
  const [modalActive, setModalActive] = React.useState(false);
  const [modalType, setModalType] = React.useState(null);
  React.useEffect(() => {
    GApageView(asPath, 'landing Page');
    // setModalActive(true);
    // setModalType('admissionopen');
  }, []);

  const closeHandler = () => {
    setModalActive(false);
    // setActiveCallback(false);
  };
  return (
    <>
      <script
        type="application/ld+json"
        // eslint-disable-next-line react/no-danger
        dangerouslySetInnerHTML={{
          __html: `
{
  "@context": "https://schema.org/",
  "@type": "WebSite",
  "name": "Kiddenz",
  "url": "https://www.kiddenz.com/",
  "potentialAction": {
    "@type": "SearchAction",
    "target": "https://www.kiddenz.com/daycare/search?search_type={search_term_string}",
    "query-input": "required name=search_term_string"
  }

}`,
        }}
      />

      <script
        type="application/ld+json"
        // eslint-disable-next-line react/no-danger
        dangerouslySetInnerHTML={{
          __html: `{
            "@context": "https://schema.org",
            "@type": "Organization",
            "name": "Vollgas Eduventures Private Limited",
            "alternateName": "Kiddenz",
            "url": "https://www.kiddenz.com/",
            "logo": "https://www.kiddenz.com/_next/static/images/kiddenz-purple%20logo-6ff9395edb6e54e6a2dda1c728993797.svg",
            "sameAs": [
              "https://www.facebook.com/kiddenzcare/",
              "https://twitter.com/Kiddenz_care",
              "https://www.instagram.com/kiddenz_care/",
              "https://www.youtube.com/channel/UCbMizh8bVnac4pMmYjoEWPg",
              "https://www.linkedin.com/company/kiddenz"
            ]
          }`,
        }}
      />

      <div>
        <Head>
          <title>{SEO_TAGS.index.title}</title>
          <meta name="description" content={SEO_TAGS.index.description} />
          <meta name="keyword" content={SEO_TAGS.index.keyword} />
          <meta name="og:title" content={SEO_TAGS.index.title} />
          <meta name="og:description" content={SEO_TAGS.index.description} />
          <link rel="canonical" href="https://kiddenz.com" />
        </Head>

        <HomePage
          {...props}
          getData={getData}
          successToast={successToast}
          errorToast={errorToast}
        />

        {modalActive && (
          <Modal
            type={modalType}
            setActive={setModalActive}
            setModalType={setModalType}
            closeHandler={closeHandler}
            authActionSelected="login"
            clickType={null}
            {...props}
          />
        )}
      </div>
    </>
  );
};

Home.propTypes = {
  getData: PropTypes.func,
  successToast: PropTypes.func,
  errorToast: PropTypes.func,
  router: PropTypes.object,
};

Home.getInitialProps = async props => {
  const {
    GET_INTRESTING_ARTICLES_API_CALL,
    DASHBOARD_ONLINE_PROGRAM_LIST_API_CALL,
    GET_ONLINE_PROGRAM_WISHLIST_API_CALL,
  } = props;

  if (props.req) {
    GET_INTRESTING_ARTICLES_API_CALL();
    GET_ONLINE_PROGRAM_WISHLIST_API_CALL();
    DASHBOARD_ONLINE_PROGRAM_LIST_API_CALL({
      // query: {
      //   program_type: 'small_batch',
      // },
    });
  }

  const isLoggedIn = false;

  if (isLoggedIn) return {};
  return {};
};

export default AuthHoc(Home, false);
