/* eslint-disable jsx-a11y/iframe-has-title */
/* eslint-disable react/style-prop-object */
/* eslint-disable react/no-danger */
import React, { useEffect } from 'react';
import Head from 'next/head';
import Geocode from 'react-geocode';
// eslint-disable-next-line no-unused-vars
import BaseUrl, { servertype } from 'utils/BaseUrl';
// import Router from 'next/router';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import GlobalStyle, { SanitizeStyle } from 'styles/global-styles';
import { Provider, connect } from 'react-redux';
import withRedux from 'next-redux-wrapper';
import withReduxSaga from 'next-redux-saga';
import { initStore } from 'store/createStore';
import nullCheck from 'utils/nullCheck';
// import ProgressBar from '@badrap/bar-of-progress';
import {
  // Router,
  useRouter,
} from 'next/router';
import NProgress from 'nprogress';
import { componentActions as DashboardActions } from 'shared/containers/Dashboard/actions';
import { componentActions as AuthenticationActions } from 'shared/containers/Authentication/actions';
import { getData, mapDispatchToProps } from 'shared/utils';
import { ToastContainer } from 'react-toastify';
import { MAPS_API_KEY, GA_KEY, initGA } from 'utils/google';
import { createStructuredSelector } from 'reselect';
import { makeSelectDashboardState } from 'shared/containers/Dashboard/selectors';
import { makeSelectAuthenticationState } from 'shared/containers/Authentication/selectors';
import background from 'images/banner6 (3).png';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import {
  // GA_TAG_MANAGER_ID,
  PHONE_CONVERSION_NUMBER,
  PHONE_CONVERSION_ID,
  // GA_ANALYTICS_ID,
  FACEBOOK_PIXEL_ID,
  LINKEDIN_INSIGHT_ID,
} from 'utils/gtag';
// import "slick-carousel/slick/slick.css";
// import "slick-carousel/slick/slick-theme.css";
import CarouselStyles from 'styles/carousel-styles';
import CarouselThemeStyles from 'styles/carousel-theme-styles';
import ReactInputRangeStyles from 'styles/react-input-range-styles';
import ReactToastifyStyles from 'styles/ReactToastify';
import SimpleBarStyles from 'styles/simpleBar-styles';

import 'react-datepicker/dist/react-datepicker.css';
// require('styles/react-datepicker.css');
import ReactDatepickerStyles from 'styles/react-datepicker-styles';
import RCTimePickerStyles from 'styles/rc-time-picker-styles';

// if (typeof window !== 'undefined') {
//   // eslint-disable-next-line global-require
//   const amplitude = require('amplitude-js/amplitude');
//   amplitude.getInstance().init('35cee043be5bef45c9f3a18d56d9d909');
// }

// let url = '';

export const mapStateToProps = createStructuredSelector({
  authentication: makeSelectAuthenticationState(),
  dashboard: makeSelectDashboardState(),
});

const AppWrapper = styled.div`
  position: relative;
  width: 100%;
  max-width: 2560px;
  overflow: hidden;
  margin: 0 auto;
  min-height: 100vh;
  // height: 100vh;

  // max-width: calc(768px + 16px * 2);

  // display: flex;
  // min-height: 100%;
  // padding: 0 16px;
  // flex-direction: column;
  /* .bar-of-progress {
    z-index: 9999;
  } */
`;

// const progress = new ProgressBar({
//   size: 2,
//   color: '#38a169',
//   className: 'bar-of-progress',
//   delay: 100,
// });

// Router.events.on('routeChangeStart', progress.start);
// Router.events.on('routeChangeComplete', progress.finish);
// Router.events.on('routeChangeError', progress.finish);

const MyApp = _props => {
  const { Component, pageProps, store, ...props } = _props;

  const router = useRouter();
  React.useEffect(() => {
    const routeChangeStart = () => {
      NProgress.start();
      console.log('ssssstarting');
    };
    const routeChangeComplete = () => {
      // if (typeof window !== 'undefined') window.scrollTo(0, 0);
      NProgress.done();
    };

    router.events.on('routeChangeStart', routeChangeStart);
    router.events.on('routeChangeComplete', routeChangeComplete);
    router.events.on('routeChangeError', routeChangeComplete);
    return () => {
      router.events.off('routeChangeStart', routeChangeStart);
      router.events.off('routeChangeComplete', routeChangeComplete);
      router.events.off('routeChangeError', routeChangeComplete);
    };
  }, []);

  useEffect(() => {
    initGA();
    if (!window.Object.fromEntries)
      window.Object.fromEntries = arr =>
        arr.reduce((acc, [key, value]) => ({ ...acc, [key]: value }), {});
  }, []);
  return (
    <html lang="en">
      <Head>
        <title>Day Care</title>
        {/* <meta name="viewport" content="width=device-width, initial-scale=1" /> */}
        <meta name="viewport" content="initial-scale=1, maximum-scale=1" />
        <link rel="shortcut icon" href="/static/favicon.ico" />

        <style>{`
            /* Make clicks pass-through */
            #nprogress {
              pointer-events: none;
            }

            #nprogress .bar {
              background: rgb(97, 58, 149);

              position: fixed;
              z-index: 1031;
              top: 0;
              left: 0;

              width: 100%;
              height: 3px;
            }

            /* Fancy blur effect */
            #nprogress .peg {
              display: block;
              position: absolute;
              right: 0px;
              width: 100px;
              height: 100%;
              /* box-shadow: 0 0 10px #29d, 0 0 5px #29d; */
              opacity: 1;

              -webkit-transform: rotate(3deg) translate(0px, -4px);
              -ms-transform: rotate(3deg) translate(0px, -4px);
              transform: rotate(3deg) translate(0px, -4px);
            }

            /* Remove these to get rid of the spinner */
            #nprogress .spinner {
              display: none;
              position: fixed;
              z-index: 1031;
              top: 15px;
              right: 15px;
            }

            #nprogress .spinner-icon {
              width: 20px;
              height: 20px;
              box-sizing: border-box;

              border: solid 3px transparent;
              border-top-color: #29d;
              border-left-color: #29d;
              border-radius: 50%;

              -webkit-animation: nprogress-spinner 400ms linear infinite;
              animation: nprogress-spinner 400ms linear infinite;
            }

            .nprogress-custom-parent {
              overflow: hidden;
              position: relative;
            }

            .nprogress-custom-parent #nprogress .spinner,
            .nprogress-custom-parent #nprogress .bar {
              position: absolute;
            }

            @-webkit-keyframes nprogress-spinner {
              0% {
                -webkit-transform: rotate(0deg);
              }
              100% {
                -webkit-transform: rotate(360deg);
              }
            }
            @keyframes nprogress-spinner {
              0% {
                transform: rotate(0deg);
              }
              100% {
                transform: rotate(360deg);
              }
            }
            
            .mainSectionStyles {
              position: relative;
              height: 753px;
              background-image: url("${background}");
              background-size: 100% 100%;
              z-index: 10;
              margin-top: 70px;
              margin-bottom: 70px;
            }
            @media only screen and (max-width: 2560px) and (min-width: 1441px) {
              .mainSectionStyles {
                background-size: cover;
                height: 900px;
              }
            }
            @media only screen and (max-width: 1300px) {
              .mainSectionStyles {
                height: 750px;
              }
            }
            img {
    pointer-events: none;
}
            @media only screen and (max-width: 1100px) {
              .mainSectionStyles {
                height: 650px;
              }
            }
            @media only screen and (max-width: 900px) {
              .mainSectionStyles {
                height: 600px;
              }
            }
            
            @media only screen and (max-width: 500px) {
              .mainSectionStyles {
                background-size: 100% 65% !important;
                background-repeat: no-repeat !important;
                background-position: left;
                height: 341px !important;
                max-height: 341px !important;
                margin-top: 40px !important;
                margin-bottom: 0 !important;  
              } 
            }
            .whatsappbtn{
              position:fixed;
              width:60px;
              height:60px;
              bottom:40px;
              right:40px;
              background-color:#25d366;
              color:#FFF;
              border-radius:50px;
              text-align:center;
              font-size:30px;
              box-shadow: 2px 2px 3px #999;
              z-index:100;
            }

            .my-float{
              margin-top:16px;
            }
          `}</style>

        {servertype === 'production' && (
          <>
            {/* Google Analytics start */}
            <script
              dangerouslySetInnerHTML={{
                __html: `
              window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
              ga('create', '${GA_KEY}', 'auto');
              ga('send', 'pageview');
          `,
              }}
            />
            <script async src="https://www.google-analytics.com/analytics.js" />
            {/* End Google Analytics */}
            {/* Google Tag Manager start */}
            <script
              async
              src={`https://www.googletagmanager.com/gtag/js?id=${GA_KEY}`}
            />
            <script
              dangerouslySetInnerHTML={{
                __html: `
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '${GA_KEY}');
          `,
              }}
            />
            <script
              dangerouslySetInnerHTML={{
                __html: `
            gtag('config', '${GA_KEY}/${PHONE_CONVERSION_ID}', { 'phone_conversion_number': '${PHONE_CONVERSION_NUMBER}' });
          `,
              }}
            />
            {/* Google Tag Manager start */}

            {/* <!-- Google Tag Manager --> */}
            <script
              dangerouslySetInnerHTML={{
                __html: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                })(window,document,'script','dataLayer','GTM-TND4SWH');
`,
              }}
            />
            {/* <!-- End Google Tag Manager --> */}

            {/* Facebook Pixel Code Start */}
            <script
              dangerouslySetInnerHTML={{
                __html: `
              !function(f,b,e,v,n,t,s)
              {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
              n.callMethod.apply(n,arguments):n.queue.push(arguments)};
              if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
              n.queue=[];t=b.createElement(e);t.async=!0;
              t.src=v;s=b.getElementsByTagName(e)[0];
              s.parentNode.insertBefore(t,s)}(window, document,'script',
              'https://connect.facebook.net/en_US/fbevents.js');
              fbq('init', '${FACEBOOK_PIXEL_ID}');
              fbq('track', 'PageView');
          `,
              }}
            />
            {/* End Facebook Pixel Code  */}

            {/* LinkedIn Insight Code Start */}
            <script
              dangerouslySetInnerHTML={{
                __html: `
              _linkedin_partner_id = '${LINKEDIN_INSIGHT_ID}';
              window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
              window._linkedin_data_partner_ids.push(_linkedin_partner_id);
          `,
              }}
            />

            <script
              dangerouslySetInnerHTML={{
                __html: `
              (function(){var s = document.getElementsByTagName("script")[0];
              var b = document.createElement("script");
              b.type = "text/javascript";b.async = true;
              b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
              s.parentNode.insertBefore(b, s);})();
            `,
              }}
            />
            {/* End LinkedIn Insight Code */}
          </>
        )}
        {/* Google Maps Code Start */}
        <script
          src={`https://maps.googleapis.com/maps/api/js?key=${MAPS_API_KEY}&libraries=places`}
        />
        {/* Google Maps Code End */}
      </Head>
      <body>
        <SanitizeStyle />
        <CarouselStyles />
        <CarouselThemeStyles />
        <ReactInputRangeStyles />
        <ReactToastifyStyles />
        <GlobalStyle />
        <ReactDatepickerStyles />
        <RCTimePickerStyles />
        <SimpleBarStyles />
        <Provider store={store}>
          <AppWrapper>
            <Component {...props} {...pageProps} safe={nullCheck} />
          </AppWrapper>
        </Provider>
        <ToastContainer autoClose={3000} />


        <a href="https://api.whatsapp.com/send?phone=918088340733&text=hello" className="whatsappbtn" target="_blank">
          <i className="fa fa-whatsapp my-float" />
        </a>

        <link
          href="https://fonts.googleapis.com/css?family=Open+Sans:400,700"
          rel="stylesheet"
        />
        <script defer src="https://checkout.razorpay.com/v1/checkout.js" />

        <script
          defer
          src="https://code.iconify.design/1/1.0.3/iconify.min.js"
        />
        <link
          href="https://fonts.googleapis.com/css?family=Quicksand:400,500,600,700|Roboto:300,400,500,700&display=swap"
          rel="stylesheet"
        />
        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
        />
        <noscript>
          <img
            height="1"
            width="1"
            style={{ display: 'none' }}
            alt=""
            src="https://www.facebook.com/tr?id=309597086791499&ev=PageView&noscript=1"
          />
        </noscript>
        <noscript>
          <iframe
            src="https://www.googletagmanager.com/ns.html?id=GTM-TND4SWH"
            height="0"
            width="0"
            style={{ display: 'none', visibility: 'hidden' }}
          />
        </noscript>
      </body>
    </html>
  );
};
MyApp.propTypes = {
  styleTags: PropTypes.any,
};
MyApp.getInitialProps = async ({ Component, ctx, ...props }) => {
  Geocode.setApiKey(MAPS_API_KEY);
  let pageProps = {};
  const actions = mapDispatchToProps(
    Object.assign({}, AuthenticationActions, DashboardActions),
  )(ctx.store.dispatch);
  if (Component.getInitialProps) {
    pageProps = await Component.getInitialProps({
      ctx,
      actions,
      getData,
      query: ctx.query,
    });
  }
  return {
    pageProps,
    getData,
    query: ctx.query,
    ...props,
  };
};

export default withRedux(initStore)(
  connect(mapStateToProps)(withReduxSaga(MyApp)),
);
