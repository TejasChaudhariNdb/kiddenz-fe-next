// eslint-disable-next-line arrow-body-style
const Sitemap = () => {
  return null;
};

export const getServerSideProps = async ({ res }) => {
  const sitemap = `<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
  <sitemap>
      <loc>https://www.kiddenz.com/sitemap-cities.xml</loc>
      </sitemap>
  <sitemap>
      <loc>https://www.kiddenz.com/sitemap-areas.xml</loc>
      </sitemap>
  <sitemap>
      <loc>https://www.kiddenz.com/sitemap-dtl.xml</loc>
      </sitemap>		
  <sitemap>
      <loc>https://www.kiddenz.com/sitemap-blogs.xml</loc>
      </sitemap>
</sitemapindex>`;

  res.setHeader('Content-Type', 'text/xml');
  res.write(sitemap);
  res.end();

  return {
    props: {},
  };
};

export default Sitemap;
