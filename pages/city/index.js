import React from 'react';
import Head from 'next/head';
import { useHistory, useLocation } from 'react-router-dom';
import PropTypes from 'prop-types';
import AuthHoc from 'hoc/authCheck';
import dynamic from 'next/dynamic';
import { SEO_TAGS } from 'utils/constants';
import { getData } from 'shared/utils';
import { toast } from 'react-toastify';
import { GApageView } from 'utils/google';
import MascotLoader from 'components/common/MascotLoader';
import SearchResultPage from 'containers/SearchResultPage';

const usePathname = () => {
  const currentURL = window.location.href;
  return currentURL;
};

export default usePathname;
