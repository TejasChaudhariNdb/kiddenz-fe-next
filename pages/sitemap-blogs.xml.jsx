// eslint-disable-next-line arrow-body-style
const Sitemap = () => {
  return null;
};

export const getServerSideProps = async ({ res }) => {
  const sitemap = `<?xml version="1.0" encoding="UTF-8"?>
  <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9">
  <url><loc>https://kiddenz.com/blog/discovering-the-wonders-of-early-education-a-comprehensive-guide-to-different-types-of-preschool-programs-1277</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/tips-to-encourage-your-toddler-to-talk-578</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/developmental-advantages-connected-to-early-swimming-573</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/avoid-losing-your-child-in-public-places-554</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/advantages-of-getting-involved-with-your-childs-school-548</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/simple-ways-to-help-your-child-be-sociable-508</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/help-your-child-achieve-the-crawling-milestone-584</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/all-you-need-to-know-about-your-babys-growth-chart-0-to-12-months-527</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/all-that-you-want-to-know-about-toddlers-developmental-delay-432</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/means-to-get-your-twins-on-a-similar-sleep-schedule-221</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/why-is-white-noise-good-for-babies-617</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/tips-for-getting-back-to-work-after-having-a-baby-658</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/tips-to-recognize-and-diagnose-your-babys-food-allergies-and-sensitivities-628</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/tips-to-deal-with-swearing-and-abusive-language-a-guide-for-parents-216</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/heres-how-you-can-keep-boredom-at-bay-when-travelling-with-a-child-during-a-road-trip-344</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/guidelines-on-how-to-track-your-babys-growth-517</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/at-what-age-should-i-start-potty-training-591</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/introducing-your-child-to-the-world-of-writing-566</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/tips-on-how-to-enhance-your-toddlers-self-confidence-with-words-605</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/tips-on-how-to-teach-your-baby-to-walk-494</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/avoid-these-toxic-ingredients-in-baby-care-products-680</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/6-secrets-to-raising-a-healthy-and-happy-child-239</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/6-ways-to-boost-your-toddlers-immune-system-59</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/enjoying-a-beach-vacation-with-your-toddler-53</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/8-tips-on-confident-parenting-57</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/helpful-tips-for-busy-parents-to-keep-homes-clean-709</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/tips-on-how-to-design-kids-rooms-to-foster-creativity-695</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/tips-on-how-to-encourage-your-toddlers-physical-development-448</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/potty-training-mistakes-to-avoid-419</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/tips-on-how-to-make-potty-training-for-you-little-one-fun-402</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/signs-causes-and-treatment-for-sleep-deprivation-in-toddlers-308</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/spark-your-childs-imagination-by-promoting-creative-skills-260</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/tips-on-raising-bilingual-multilingual-kids-256</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/tips-to-keep-your-child-away-from-junk-food-235</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/effective-techniques-to-improve-your-childs-skills-61</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/the-pleasure-in-parent-child-bonding-activities-55</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/negative-habits-that-lower-your-childs-immunity-51</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/baby-proof-your-home-for-safety-and-comfort-42</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/heres-how-to-help-your-child-overcome-fears-721</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/tips-on-teaching-your-kid-to-say-sorry-733</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/5-techniques-for-taming-the-anger-monster-in-children-895</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/what-are-the-most-comfortable-positions-to-carry-a-baby-893</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/what-should-you-do-if-your-toddler-is-too-clingy-891</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/5-things-to-avoid-for-dealing-with-working-mom-guilt-889</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/psychological-impact-of-fighting-in-front-of-your-child-887</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/is-my-child-ready-to-be-potty-trained-596</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/speech-and-language-development-in-children-560</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/right-time-to-have-the-talk-with-kids-about-their-private-parts-539</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/make-homework-fun-and-engaging-for-your-child-847</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/tips-to-get-your-kids-brush-their-teeth-833</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/safety-measures-to-follow-to-keep-your-child-safe-from-abuse-823</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/ways-to-help-your-child-with-autism-spectrum-disorder-asd-sleep-better-794</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/sweets-and-desserts-toddlers-should-avoid-and-why-806</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/is-it-ok-to-settle-my-baby-with-a-bottle-of-milk-784</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/right-drinks-for-proper-nutrition-levels-of-your-child-816</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/how-can-i-keep-my-baby-healthy-on-a-vegan-diet-772</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/when-can-my-baby-have-ice-cream-789</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/sleeping-arrangements-when-travelling-with-twins-811</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/how-to-introduce-new-foods-to-your-child-779</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/how-to-deal-with-saying-no-to-your-child-766</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/what-parents-can-do-about-their-children-lying-761</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/how-what-you-say-might-affect-your-childs-temperament-754</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/child-safety-identifying-the-common-signs-of-sexual-abuse-741</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/soothing-activities-to-carry-out-to-help-your-baby-sleep-229</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/how-to-prevent-your-toddler-from-watching-mobile-videos-during-meals-63</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/how-to-promote-positive-behaviour-in-your-toddler-49</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/what-are-the-cognitive-milestones-in-early-childhood-development-169</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/tips-on-how-to-manage-jet-lag-in-kids-368</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/media-effects-on-childrens-social-and-moral-development-161</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/secrets-of-easy-air-travel-with-a-baby-29</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  <url><loc>https://kiddenz.com/blog/how-to-handle-your-toddlers-tantrums-23</loc><changefreq>weekly</changefreq><priority>0.80</priority></url>
  </urlset>
  `;

  res.setHeader('Content-Type', 'text/xml');
  res.write(sitemap);
  res.end();

  return {
    props: {},
  };
};

export default Sitemap;
