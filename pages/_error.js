/* eslint-disable react/prop-types */
import React from 'react';
// import Head from "next/head";
import Router from 'next/router';
import ErrorPage from 'components/EmptyStates/error404';

// Script errors occuring during initial client render can cause the server-rendered
// content to be hidden by an error page. Track router events to determine if the
// error being handled happened during initial render, and throw within
// getInitialProps to allow the server-rendered content to remain visible.
const isClient = typeof window !== 'undefined';
const isInitialClientRender = isClient;
if (isClient) {
  Router.ready(() => {
    // NProgress.start();
    // Router.router.events("routeChangeStart", () => {
    //   isInitialClientRender = false;
    // });
    // Router.router.events("routeChangeComplete", () => {
    // });
    if (
      document &&
      (document.readyState === 'complete' ||
        document.readyState === 'interactive')
    ) {
      // NProgress.done();
    } else {
      document.addEventListener('DOMContentLoaded', () => {
        // NProgress.done();
      });
    }
  });
}
// const PageNotFoundComponent = ({ title }) => (
//   <div
//     style={{
//       display: 'flex',
//       alignItems: 'center',
//       justifyContent: 'center',
//       height: '100%',
//       width: '100%',
//       marginTop: '50px',
//     }}
//   >
//     <article>
//       <h1>{title || 'PAGE NOT FOUND'}</h1>
//     </article>
//   </div>
// );
const ERROR = props => {
  const { statusCode } = props;

  const title =
    statusCode === 404
      ? 'This page could not be found'
      : (statusCode && 'Internal Server Error') ||
        'An unexpected error has occurred';

  return <ErrorPage title={title} margin="220px auto 0px" {...props} />;
};

ERROR.getInitialProps = ({ ctx: { err, res, xhr } }) => {
  if (isInitialClientRender) {
    // rethrow to prevent the error view from displaying on initial client render
    throw err;
  }
  const statusCode = (res && res.statusCode) || (xhr && xhr.status) || null;
  return { statusCode };
};

ERROR.propTypes = {
  // statusCode: PropTypes.number
};

export default ERROR;
