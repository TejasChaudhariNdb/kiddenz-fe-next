import React from 'react';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import Qs from 'query-string';
import Head from 'next/head';
import AuthHoc from 'hoc/authCheck';
import dynamic from 'next/dynamic';
import { SEO_TAGS } from 'utils/constants';
import { getData } from 'shared/utils';
import { toast } from 'react-toastify';
import { GApageView, GAevent } from 'utils/google';
import MascotLoader from 'components/common/MascotLoader';

const HomePage = dynamic(() => import('components/SchoolProfile'), {
  loading: () => <MascotLoader />,
});

const Home = props => {
  const {
    query: { city = '', area = '' } = {},
    router: { asPath = '' } = {},
  } = props;
  const {
    query: { name = '', type = '', wishlist: isSchoolWishlisted = '' } = {},
  } = Qs.parseUrl(asPath);

  const successToast = msg => toast.success(msg);

  const errorToast = msg => toast.error(msg);

  const routerPath = useRouter();

  const origin =
    typeof window !== 'undefined' && window.location.origin
      ? window.location.origin
      : '';

  const rightClickHandler = event => event.preventDefault();
  React.useEffect(() => {
    document.addEventListener('contextmenu', rightClickHandler);
    GApageView(asPath, 'School Profile Page');
    GAevent('School Profile Visit', name);
    return () => {
      document.removeEventListener('contextmenu', rightClickHandler);
    };
  }, []);

  const title = `${`${name},`} ${`${type},`} ${`${city},`} ${area} - Kiddenz`;
  const description = `${`${name},`} ${`${city},`} ${`${area},`} ${`${type},`} view virtual tour, pictures, reviews, program and teacher information.`;

  // "${}"`
  return (
    <div>
      <Head>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta name="keyword" content={SEO_TAGS.index.keyword} />
        <meta name="og:title" content={title} />
        <meta name="og:description" content={description} />
        <link
          rel="canonical"
          href={`https://kiddenz.com${routerPath.asPath.slice(
            0,
            routerPath.asPath.indexOf('?') >= 0
              ? routerPath.asPath.indexOf('?')
              : undefined,
          )}`}
        />
      </Head>

      <HomePage
        {...props}
        getData={getData}
        successToast={successToast}
        errorToast={errorToast}
        isSchoolWishlisted={isSchoolWishlisted}
      />
    </div>
  );
};

Home.propTypes = {
  getData: PropTypes.func,
  successToast: PropTypes.func,
  errorToast: PropTypes.func,
  query: PropTypes.object,
  router: PropTypes.object,
};

Home.getInitialProps = async props => {
  const BY_PASS_SLUGS = [
    'koala-preschool-214598',
    'gurukul-mont-international-preschool-and-daycare-287459',
    'junior-dps-preschool-278980',
    'republick-high-pre-school-232109',
    'koala-preschool-179368',
    'cambridge-montessori-preschool-and-daycare-76899',
    'zion-kidz-preschool-273142',
    'little-millennium-preschool-252142',
  ];
  const {
    PROVIDER_MEDIA_API_CALL,
    PROVIDER_BASIC_INFO_API_CALL,
    PROVIDER_FACULTY_API_CALL,
    PROVIDER_LOCATION_API_CALL,
    PROVIDER_FACILITY_API_CALL,
    PROVIDER_DIRECTOR_INFO_API_CALL,
    PROVIDER_CONTACT_DETAILS_API_CALL,
    PROVIDER_LANGUAGES_API_CALL,
    PROVIDER_YEARLY_CALENDER_API_CALL,
    PROVIDER_SCHEDULE_FOR_DAY_API_CALL,
    PROVIDER_WORKING_DAY_API_CALL,
    PROVIDER_EMERGENCY_SERVICES_API_CALL,
    PROVIDER_SCHEDULE_CHOICE_API_CALL,
    GET_USERS_CHILDREN_API_CALL,
    GET_USERS_TOUR_SCHEDULE_API_CALL,

    POST_CLICK_COUNT_API_CALL,
    // DAYCARE_SUGGESTION_API_CALL,
    query: { 'daycare-id': id } = {},
    isLoggedIn,
  } = props;

  // API's for displayong things in the top section
  PROVIDER_MEDIA_API_CALL({ params: { id } });
  PROVIDER_LOCATION_API_CALL({ params: { id } });
  PROVIDER_BASIC_INFO_API_CALL({ params: { id } });
  PROVIDER_DIRECTOR_INFO_API_CALL({ params: { id } });
  PROVIDER_SCHEDULE_CHOICE_API_CALL({ params: {} });
  PROVIDER_SCHEDULE_FOR_DAY_API_CALL({ params: { id } });
  PROVIDER_WORKING_DAY_API_CALL({ params: { id } });

  // API's for displaynng things in the main content section
  if (isLoggedIn || BY_PASS_SLUGS.includes(id)) {
    if (!BY_PASS_SLUGS.includes(id)) {
      POST_CLICK_COUNT_API_CALL({
        payload: { click_type: 'school_profile_visits' },
      });
    }
    PROVIDER_CONTACT_DETAILS_API_CALL({ params: { id } });
    PROVIDER_FACULTY_API_CALL({ params: { id } });
    PROVIDER_FACILITY_API_CALL({
      params: { id },
      successCallback: () => {},
    });
    PROVIDER_LANGUAGES_API_CALL({ params: {} });
    PROVIDER_YEARLY_CALENDER_API_CALL({ params: { id } });
    // PROVIDER_CONTENT_SCHEDULE_API_CALL({ params: { id } });
    PROVIDER_EMERGENCY_SERVICES_API_CALL({ params: {} });
    GET_USERS_CHILDREN_API_CALL();
    GET_USERS_TOUR_SCHEDULE_API_CALL({
      query: {
        limit: 3,
        skip: 0,
      },
    });
  }

  // const isLoggedIn = false;
  // if (isLoggedIn) return {};
  return {
    getData,
  };
};

export default AuthHoc(Home, false);
