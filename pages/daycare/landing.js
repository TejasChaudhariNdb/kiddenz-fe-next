import React from 'react';
import Head from 'next/head';
import PropTypes from 'prop-types';
import AuthHoc from 'hoc/authCheck';
import dynamic from 'next/dynamic';
import { SEO_TAGS } from 'utils/constants';
import { getData } from 'shared/utils';
import { GApageView } from 'utils/google';
import { toast } from 'react-toastify';
import MascotLoader from 'components/common/MascotLoader';

const LandingPage = dynamic(() => import('containers/HomePage'), {
  loading: () => <MascotLoader />,
});

const Landing = props => {
  const successToast = msg => toast.success(msg);

  const errorToast = msg => toast.error(msg);

  const {
    router: { asPath },
  } = props;

  React.useEffect(() => {
    GApageView(asPath, [], 'Discovery Page');
  }, []);

  return (
    <div>
      <Head>
        <title>{SEO_TAGS.dayCareLanding.title}</title>
        <meta
          name="description"
          content={SEO_TAGS.dayCareLanding.description}
        />
        <meta name="keyword" content={SEO_TAGS.dayCareLanding.keyword} />
        <meta name="og:title" content={SEO_TAGS.dayCareLanding.title} />
        <meta
          name="og:description"
          content={SEO_TAGS.dayCareLanding.description}
        />
      </Head>

      <LandingPage
        {...props}
        getData={getData}
        successToast={successToast}
        errorToast={errorToast}
      />
    </div>
  );
};

Landing.propTypes = {
  getData: PropTypes.func,
  successToast: PropTypes.func,
  errorToast: PropTypes.func,
  router: PropTypes.object,
};

Landing.getInitialProps = async props => {
  const {
    GET_INTRESTING_ARTICLES_API_CALL,
    ARTICLES_GET_ALL_CATEGORIES_API_CALL,
  } = props;
  GET_INTRESTING_ARTICLES_API_CALL();
  ARTICLES_GET_ALL_CATEGORIES_API_CALL();

  const isLoggedIn = false;
  if (isLoggedIn) return {};

  return {};
};

export default AuthHoc(Landing, false);
