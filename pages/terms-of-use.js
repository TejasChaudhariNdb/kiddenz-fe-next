import React from 'react';
import Head from 'next/head';
import PropTypes from 'prop-types';
import AuthHoc from 'hoc/authCheck';
import dynamic from 'next/dynamic';
import { SEO_TAGS } from 'utils/constants';
import { getData } from 'shared/utils';
import { toast } from 'react-toastify';
import MascotLoader from 'components/common/MascotLoader';

const HomePage = dynamic(() => import('containers/Terms'), {
  loading: () => <MascotLoader />,
});

const successToast = msg => toast.success(msg);

const errorToast = msg => toast.error(msg);

const Home = props => {
  // const {
  //   router: { asPath },
  // } = props;

  // React.useEffect(() => {
  //   GApageView(asPath, 'Terms of Use page');
  // }, []);

  return (
    <div>
      <Head>
        <title>{SEO_TAGS.terms.title}</title>
        <meta name="description" content={SEO_TAGS.terms.description} />
        <meta name="keyword" content={SEO_TAGS.terms.keyword} />
        <meta name="og:title" content={SEO_TAGS.terms.title} />
        <meta name="og:description" content={SEO_TAGS.terms.description} />
      </Head>

      <HomePage
        {...props}
        getData={getData}
        successToast={successToast}
        errorToast={errorToast}
      />
    </div>
  );
};

Home.propTypes = {
  getData: PropTypes.func,
  successToast: PropTypes.func,
  errorToast: PropTypes.func,
  router: PropTypes.object,
};

Home.getInitialProps = async props => {
  const { GET_INTRESTING_ARTICLES_API_CALL } = props;

  GET_INTRESTING_ARTICLES_API_CALL();

  const isLoggedIn = false;
  if (isLoggedIn) return {};

  return {};
};

export default AuthHoc(Home, false);
