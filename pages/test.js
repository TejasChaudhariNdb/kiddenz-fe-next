import React from 'react';
import Head from 'next/head';
import { connectHoc } from 'shared/utils';
import { SEO_TAGS } from 'utils/constants';
import useTestHooks from 'shared/hooks/test';

const Test = props => {
  const test = useTestHooks(props);
  console.log(test);
  return (
    <div>
      <Head>
        <title>{SEO_TAGS.index.title}</title>
        <meta name="description" content={SEO_TAGS.index.description} />
        <meta name="keyword" content={SEO_TAGS.index.keyword} />
        <meta name="og:title" content={SEO_TAGS.index.title} />
        <meta name="og:description" content={SEO_TAGS.index.description} />
      </Head>
      <h1>Test</h1>
    </div>
  );
};

Test.getInitialProps = async props => {
  const isLoggedIn = false;
  if (isLoggedIn) return {};
  return {
    ...props.actions,
  };
};

export default connectHoc(Test);
