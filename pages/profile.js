/* eslint-disable camelcase */
import React from 'react';
import Head from 'next/head';
import PropTypes from 'prop-types';
import AuthHoc from 'hoc/authCheck';
import dynamic from 'next/dynamic';
import { SEO_TAGS } from 'utils/constants';
import { getData } from 'shared/utils';
import { toast } from 'react-toastify';
import { GApageView } from 'utils/google';
import MascotLoader from 'components/common/MascotLoader';
import { ON_SUCCESS } from 'shared/utils/commonReduxSagaConverter/commonConstants';

const HomePage = dynamic(() => import('containers/ParentProfile'), {
  loading: () => <MascotLoader />,
});

const Home = props => {
  const successToast = msg => toast.success(msg);

  const errorToast = msg => toast.error(msg);

  const {
    router: { asPath },
  } = props;

  React.useEffect(() => {
    GApageView(asPath, 'Profile Page');
  }, []);

  return (
    <div>
      <Head>
        <title>{SEO_TAGS.index.title}</title>
        <meta name="description" content={SEO_TAGS.index.description} />
        <meta name="keyword" content={SEO_TAGS.index.keyword} />
        <meta name="og:title" content={SEO_TAGS.index.title} />
        <meta name="og:description" content={SEO_TAGS.index.description} />
      </Head>

      <HomePage
        {...props}
        getData={getData}
        successToast={successToast}
        errorToast={errorToast}
      />
    </div>
  );
};

Home.propTypes = {
  router: PropTypes.object,
};

// eslint-disable-next-line consistent-return
Home.getInitialProps = async props => {
  const {
    GET_USER_DETAILS_API_CALL,
    GET_USER_WISHLISTED_PRODUCTS_API_CALL,
    GET_USER_BOOKMARK_ARTICLES_API_CALL,
    GET_USERS_TOUR_SCHEDULE_API_CALL,
    GET_USERS_TOUR_UNSCHEDULE_API_CALL,
    GET_USERS_CHILDREN_API_CALL,
    GET_ARTICLE_LIKE_VIEW_COUNT_API_CALL,
    GET_USER_BOOKMARK_ARTICLES_API_CUSTOM_TASK,
    GET_ONLINE_PROGRAM_WISHLIST_API_CALL,
    // ONLINE_PROGRAM_PURCHASED_PROGRAMS_API_CALL,
    GET_TRANSACTION_LIST_API_CALL,
  } = props;
  // ONLINE_PROGRAM_PURCHASED_PROGRAMS_API_CALL();
  GET_ONLINE_PROGRAM_WISHLIST_API_CALL();
  GET_TRANSACTION_LIST_API_CALL();
  GET_USER_DETAILS_API_CALL();
  GET_USER_WISHLISTED_PRODUCTS_API_CALL({
    query: {
      limit: 3,
      skip: 0,
    },
  });

  GET_USER_BOOKMARK_ARTICLES_API_CALL({
    successCallback: ({ res: { data: { data = [] } = {} } = {} }) => {
      const postIds = data.map(({ post_id }) => post_id);
      setTimeout(() => {
        postIds.forEach(d => {
          GET_ARTICLE_LIKE_VIEW_COUNT_API_CALL({
            query: { article_id: d },
            successCallback: ({
              res: {
                data: {
                  data: { like_count: likeCount, view_count: viewCount } = {},
                } = {},
              } = {},
            }) => {
              GET_USER_BOOKMARK_ARTICLES_API_CUSTOM_TASK(ON_SUCCESS, {
                tasks: [
                  {
                    task: 'isUpdate',
                    params: {
                      id: [d],
                      key: 'post_id',
                      values: {
                        [d]: { viewCount, likeCount },
                      },
                    },
                  },
                ],
              });
            },
          });
        });
      }, 500);
    },
  });

  GET_USERS_TOUR_SCHEDULE_API_CALL({
    query: {
      limit: 3,
      skip: 0,
    },
  });
  GET_USERS_TOUR_UNSCHEDULE_API_CALL({
    query: {
      limit: 3,
      skip: 0,
    },
  });
  GET_USERS_CHILDREN_API_CALL();

  const isLoggedIn = false;
  // props.DASHBOARD_GET_CATEGORY_API_CALL({});
  if (isLoggedIn) return {};
};

export default AuthHoc(Home, true);
