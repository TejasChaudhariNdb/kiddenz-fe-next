/* eslint-disable no-sequences */
/* eslint-disable no-console */
/* eslint-disable eqeqeq */
/* eslint-disable camelcase */
/* eslint-disable indent */
// eslint-disable-next-line import/no-unresolved
import React from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import AuthHoc from 'hoc/authCheck';
import dynamic from 'next/dynamic';
import { SEO_TAGS } from 'utils/constants';
import { getData } from 'shared/utils';
import { toast } from 'react-toastify';
import { GApageView, MAPS_API_KEY } from 'utils/google';
import MascotLoader from 'components/common/MascotLoader';
import Geocode from 'react-geocode';
// eslint-disable-next-line no-unused-vars
import fetch from 'isomorphic-fetch';
const HomePage = dynamic(() => import('containers/SearchResultPage'), {
  loading: () => <MascotLoader />,
});

const Home = props => {
  const successToast = msg => toast.success(msg);
  console.log(props.query);
  const errorToast = msg => toast.error(msg);

  const routerPath = useRouter();

  const origin =
    typeof window !== 'undefined' && window.location.origin
      ? window.location.origin
      : '';

  Geocode.setApiKey(MAPS_API_KEY);
  const {
    router: { asPath },
  } = props;

  React.useEffect(() => {
    GApageView(asPath, 'Daycare Search Result Page');
  }, []);

  const {
    query: {
      search_type: searchType = 'location',
      locationString = '',
      fromLocationString = '',
      toLocationString = '',
    } = {},
    dashboard: {
      DAYCARE_SEARCH_API: {
        data: { coordinates: locationSearchCords = [] } = {},
      } = {},
      DAYCARE_TRIP_SEARCH_CORDS_API: {
        data: { coordinates: tripSearchCords = [] } = {},
      } = {},
    } = {},
  } = props;

  const areResultsThere =
    searchType === 'location'
      ? locationSearchCords.length > 0
      : tripSearchCords.length > 0;

  const title =
    searchType === 'location'
      ? `Best Preschools, Daycare, and Playschools in  ${
          props.query.area
        }  Kiddenz`
      : `Best Preschools, Daycare, and Playschools in  ${
          props.query.area
        }  Kiddenz`;

  const description = (() =>
    !areResultsThere
      ? `No preschools/daycare or preschool and daycare in “${locationString}”. Find Best Preschool, Daycare and Play school near me, view program details, teachers, picture, virtual tour, read reviews, and book appointments.`
      : ` Top “${
          locationSearchCords.length
        }” Preschools , daycares, and playschools in “${props.query.area}” , “${
          props.query.city
        }”. View program details, teachers, picture, virtual tour, reviews, and book appointments`)();

  return (
    <div>
      <Head>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta name="keyword" content={SEO_TAGS.dayCareSearch.keyword} />
        <meta name="og:title" content={title} />
        <meta name="og:description" content={description} />
        <link
          rel="canonical"
          href={`https://kiddenz.com${routerPath.asPath.slice(
            0,
            routerPath.asPath.indexOf('?') >= 0
              ? routerPath.asPath.indexOf('?')
              : undefined,
          )}`}
        />
      </Head>

      <HomePage
        {...props}
        getData={getData}
        successToast={successToast}
        errorToast={errorToast}
      />
    </div>
  );
};

Home.propTypes = {
  getData: PropTypes.func,
  successToast: PropTypes.func,
  errorToast: PropTypes.func,
  query: PropTypes.object,
  dashboard: PropTypes.object,
  router: PropTypes.object,
};

Home.getInitialProps = async props => {
  const address = `${props.query.city}, ${props.query.area}`;
  const response = await Geocode.fromAddress(address);

  const { lat, lng } = response.results[0].geometry.location;

  const {
    DAYCARE_SEARCH_API_CALL,
    // DAYCARE_SUGGESTION_API_CALL,
    query: {
      locationString = 'pune',
      fromLat,
      fromLng,
      toLat,
      toLng,
      type = 'radius',
      timehrs,
      timemins,
      search_type: searchType = 'location',
      transportation,
      food,
      course_type,
      organisation,
      daycareprice,
      daycare_min_price,
      daycare_max_price,
      sort_by = 'distance',
      extended_hours,
      min_extended_hours,
      max_extended_hours,
      ...filters
    } = {},
    // isLoggedIn,
  } = props;

  // if (isLoggedIn) {

  // search_type=location&locationString=Pune,%20Maharashtra,%20India&latitude=18.5204303&longitude=73.8567437&type=radius&sort_by=distance

  DAYCARE_SEARCH_API_CALL({
    query: {
      ...(() => {
        const { ...rest } = filters;
        return type === 'bounds' ? rest : { ...rest };
      })(),
      type,
      limit: 6,
      skip: 0,
      suggestions: 4,
      latitude: lat,
      longitude: lng,
      searchType,
      ...(transportation == 1
        ? {
            transportation: 1,
          }
        : {}),

      ...(food == 1
        ? {
            food: 1,
          }
        : {}),

      ...(extended_hours == 1
        ? {
            min_extended_hours: min_extended_hours || 0,
            max_extended_hours: max_extended_hours || 24,
          }
        : {}),

      ...(course_type == 1
        ? {
            course_type: 'montessori',
          }
        : {}),
      ...(organisation == 3 ? {} : { organisation }),
      sort_by,
      locationString,

      ...(daycareprice == 1
        ? {
            daycare_min_price: daycare_min_price || 500,
            daycare_max_price: daycare_max_price || 20000,
            min_price: filters.min_price ? filters.min_price : 500,
            max_price: filters.max_price ? filters.max_price : 300000,
          }
        : {}),

      // ...(filters.max_age
      //   ? {
      //       max_age: +filters.max_age === 0 ? 6 : +filters.max_age * 12,
      //     }
      //   : {}),
    },
    successCallback: ({
      data: { data: { search_results: searchResults = [] } = {} } = {},
    }) => {
      if (searchResults.length === 0) {
        // DAYCARE_SUGGESTION_API_CALL({
        //   query: {
        // ...filters, limit: 3, skip: 0
        //   },
        // });
      }
    },
  });

  return {
    getData,
  };
};

export default AuthHoc(Home, false);
