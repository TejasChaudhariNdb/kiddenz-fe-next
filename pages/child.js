import React from 'react';
import Head from 'next/head';
import PropTypes from 'prop-types';
import AuthHoc from 'hoc/authCheck';
import dynamic from 'next/dynamic';
import { SEO_TAGS } from 'utils/constants';
import { getData } from 'shared/utils';
import { GApageView } from 'utils/google';
import { toast } from 'react-toastify';
import MascotLoader from 'components/common/MascotLoader';

const HomePage = dynamic(() => import('containers/ParentProfile/children'), {
  loading: () => <MascotLoader />,
});

const Home = props => {
  const successToast = msg => toast.success(msg);

  const errorToast = msg => toast.error(msg);

  const {
    router: { asPath },
  } = props;

  React.useEffect(() => {
    GApageView(asPath, 'My Children Page');
  }, []);

  return (
    <div>
      <Head>
        <title>{SEO_TAGS.index.title}</title>
        <meta name="description" content={SEO_TAGS.index.description} />
        <meta name="keyword" content={SEO_TAGS.index.keyword} />
        <meta name="og:title" content={SEO_TAGS.index.title} />
        <meta name="og:description" content={SEO_TAGS.index.description} />
      </Head>

      <HomePage
        {...props}
        getData={getData}
        successToast={successToast}
        errorToast={errorToast}
      />
    </div>
  );
};

Home.propTypes = {
  router: PropTypes.object,
};

// eslint-disable-next-line consistent-return
Home.getInitialProps = async props => {
  const { GET_USERS_CHILDREN_API_CALL, GET_USER_DETAILS_API_CALL } = props;

  GET_USERS_CHILDREN_API_CALL();
  GET_USER_DETAILS_API_CALL();

  const isLoggedIn = false;
  if (isLoggedIn) return {};
};

export default AuthHoc(Home, true);
