import React from 'react';
import Head from 'next/head';
import PropTypes from 'prop-types';
import AuthHoc from 'hoc/authCheck';
import dynamic from 'next/dynamic';
import { SEO_TAGS } from 'utils/constants';
import { getData } from 'shared/utils';
import { toast } from 'react-toastify';
import { GApageView } from 'utils/google';
import MascotLoader from 'components/common/MascotLoader';

const HomePage = dynamic(() => import('containers/ReviewForm'), {
  loading: () => <MascotLoader />,
});

const Review = props => {
  const successToast = msg => toast.success(msg);

  const errorToast = msg => toast.error(msg);

  const {
    router: { asPath },
  } = props;

  React.useEffect(() => {
    GApageView(asPath, 'Reviews Page');
  }, []);

  return (
    <div>
      <Head>
        <title>{SEO_TAGS.index.title}</title>
        <meta name="description" content={SEO_TAGS.index.description} />
        <meta name="keyword" content={SEO_TAGS.index.keyword} />
        <meta name="og:title" content={SEO_TAGS.index.title} />
        <meta name="og:description" content={SEO_TAGS.index.description} />
      </Head>

      <HomePage
        {...props}
        getData={getData}
        successToast={successToast}
        errorToast={errorToast}
      />
    </div>
  );
};

Review.propTypes = {
  router: PropTypes.object,
};

Review.getInitialProps = async () => {
  const isLoggedIn = false;
  if (isLoggedIn) return {};
  return {};
};

export default AuthHoc(Review, false);
