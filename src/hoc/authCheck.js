/* eslint-disable camelcase */
/* eslint-disable no-console */
/* eslint-disable import/no-unresolved */
import React from 'react';
import { connectHoc } from 'shared/utils';
import { useSpring, animated } from 'react-spring';
// import Router from 'next/router';
import nextCookie from 'next-cookies';
import axios from 'config/axios';
import { API_BASE_URL } from 'shared/config/apiEndPoints';
// import { setCookie, deleteCookie } from 'shared/utils/commonHelpers';

export default (WrapperComponent, redirect) => {
  const Home = props => {
    // eslint-disable-next-line no-underscore-dangle
    const _props = useSpring({
      opacity: 1,
      from: { opacity: 0 },
      animationDuration: 0.5,
    });
    return (
      // eslint-disable-next-line react/prop-types
      <animated.div style={props.isServer ? _props : {}}>
        <WrapperComponent {...props} />
      </animated.div>
    );
  };
  Home.getInitialProps = async props => {
    const { actions: { dispatch } = {} } = props;
    const token = nextCookie(props.ctx)['x-access-token'];
    let isLoggedIn = false;
    if (token) {
      try {
        axios.defaults.headers.common.Authorization = token
          ? `Bearer ${token}`
          : '';
        if (props.ctx && props.ctx.isServer) {
          const { data } = await axios.get(
            `${API_BASE_URL}users/user-auto-login/`,
          );
          isLoggedIn = true;
          const {
            data: {
              data: { data: loginData, selected_filter, parent_clicks },
            },
          } = data;
          dispatch({
            type: 'PROFILE_UPDATE',
            payload: {
              ...data.data,
              data: { ...loginData, selected_filter, parent_clicks },
            },
          });
        }
      } catch (err) {
        axios.defaults.headers.common.Authorization = '';
      }
    } else {
      axios.defaults.headers.common.Authorization = '';
    }
    const { res, req } = props.ctx;
    if (res && !isLoggedIn && redirect) {
      res.writeHead(302, {
        Location: '/',
      });
      res.end();
    } else {
      const data = await WrapperComponent.getInitialProps({
        ...props.actions,
        query: props.query,
        isLoggedIn,
        req,
        res,
      });
      return {
        isLoggedIn,
        query: props.query,
        ...data,
      };
    }
    return {};
  };
  return connectHoc(Home);
};
