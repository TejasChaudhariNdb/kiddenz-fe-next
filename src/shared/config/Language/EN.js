export default {
  Vijayraja: 'Vijayraja',
  Projects: 'Projects',
  Home: 'Home',
  'About Us': 'About Us',
  'Login or Register': 'Login or Register',
  Send: 'Send',
  'Mobile number should be 10 digits long':
    'Mobile number should be 10 digits long',
  'Mobile number must be 10 digits long':
    'Mobile number must be 10 digits long',
  'Enter 10 digit mobile number': 'Enter 10 digit mobile number',
  Explore: 'Explore',
  'Locality Advantages': 'Locality Advantages',
};
