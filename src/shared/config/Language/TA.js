export default {
  Vijayraja: 'விஜய்ராஜா',
  Projects: 'திட்டங்கள்',
  Home: 'முகப்பு',
  'About Us': 'எங்களை பற்றி',
  'Login or Register': 'உள்நுழைக அல்லது பதிவுசெய்க',
  Send: 'அனுப்பு',
  'Mobile number should be 10 digits long':
    'மொபைல் எண் 10 இலக்கங்கள் நீளமாக இருக்க வேண்டும்',
  'Mobile number must be 10 digits long':
    'மொபைல் எண் கட்டாயம் 10 இலக்கங்கள் நீளமாக இருக்கும்',
  'Enter 10 digit mobile number': '10 இலக்க மொபைல் எண்ணை உள்ளிடவும்',
  Explore: 'ஆராயுங்கள்',
  'Locality Advantages': 'வட்டார நன்மைகள்',
};
