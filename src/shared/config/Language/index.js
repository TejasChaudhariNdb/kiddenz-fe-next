/* eslint-disable global-require */
const defaultLang = require(`./DEFAULT.js`).default;
const checkLanguageIsAvailable = json =>
  Object.entries(defaultLang).reduce(
    (langObj, [key, value]) =>
      Object.assign(langObj, {
        [key]: json[key] || value,
      }),
    {},
  );
export const getLanguage = lang =>
  checkLanguageIsAvailable(require(`./${lang}.js`).default);
export const languageConfig = [
  {
    name: 'English',
    key: 'EN',
  },
  {
    name: 'தமிழ்',
    key: 'TA',
  },
];
