// const BASE_URL =
//   process.env.NODE_ENV !== 'production'
import BASE_URL from '../../utils/BaseUrl';

export const API_BASE_URL = `${BASE_URL}api/`;
/* ********************* Authentication API Start ********************* */

const REGISTER_API = {
  url: `${API_BASE_URL}users/user-signup/`,
  method: 'POST',
};
// const LOGIN_API = { url: `${API_BASE_URL}users/user-login/`, method: 'POST' };
const LOGIN_API = {
  url: `${API_BASE_URL}users/user-new-signin/`,
  method: 'POST',
};
const VERIFY_OTP_API = {
  url: `${API_BASE_URL}users/user-otp-check/`,
  method: 'POST',
};
const RESEND_OTP_API = {
  url: `${API_BASE_URL}users/user-otp-resend/`,
  method: 'POST',
};
const ONBOARDING_API = {
  url: `${API_BASE_URL}users/user-signup-questions/`,
  method: 'POST',
};
const LOGOUT_API = {
  url: `${API_BASE_URL}users/user-logout/`,
  method: 'GET',
};
const ONBOARDING_PROFILE_UPDATE_API = {
  url: `${API_BASE_URL}users/user-profile-update/`,
  method: 'POST',
};
const VERIFY_EMAIL_API = {
  url: ({ key }) => `${BASE_URL}email/verification/${key}`,
  method: 'GET',
};

export const authentication = {
  LOGIN_API,
  REGISTER_API,
  VERIFY_OTP_API,
  VERIFY_EMAIL_API,
  RESEND_OTP_API,
  ONBOARDING_API,
  LOGOUT_API,
  ONBOARDING_PROFILE_UPDATE_API,
};
/* ********************* Authentication End ********************* */

/* ********************* Dashboard API Start ********************* */

// Users
const TEST_API = {};
const TEST_SUB_API = {};
const USER_REVIEW_API = {
  url: `${API_BASE_URL}review/parent-review/`,
  method: 'POST',
};
const GET_USER_DETAILS_API = {
  url: `${API_BASE_URL}parents/get-profile/`,
  method: 'GET',
};
const EDIT_USER_DETAILS_API = {
  url: `${API_BASE_URL}parents/update-profile/`,
  method: 'PUT',
};
const USER_EMAIL_VERIFY_API = {
  url: `${API_BASE_URL}parents/send-email-verification/`,
  method: 'PUT',
};
const USER_ENQUIRY_API = {
  url: `${API_BASE_URL}parents/parent-enquire/`,
  method: 'POST',
};
const USER_CONNECT_API = {
  url: `${API_BASE_URL}parents/parent-connect/`,
  method: 'POST',
};
const GET_CLICK_COUNT_API = {
  url: `${API_BASE_URL}parents/parent-click/`,
  method: 'GET',
};
const POST_CLICK_COUNT_API = {
  url: `${API_BASE_URL}parents/parent-click/`,
  method: 'PUT',
};

// Wishlist
const GET_USER_WISHLISTED_PRODUCTS_API = {
  url: `${API_BASE_URL}users/wishlists/`,
  method: 'GET',
};
const ADD_WISLIST_ITEM_API = {
  url: `${API_BASE_URL}users/wishlists/`,
  method: 'POST',
};
const DELETE_WISLIST_ITEM_API = {
  url: `${API_BASE_URL}users/wishlists/`,
  method: 'DELETE',
};

// Bookmarks
const GET_USER_BOOKMARK_ARTICLES_API = {
  url: `${API_BASE_URL}article-user/article-bookmark-users-list/`,
  method: 'GET',
};
const ADD_BOOKMARK_ARTICLE_API = {
  url: `${API_BASE_URL}article-user/bookmark-api/`,
  method: 'POST',
};
const DELETE_BOOKMARK_ARTICLE_API = {
  url: `${API_BASE_URL}article-user/bookmark-api/`,
  method: 'DELETE',
};

// Views
const GET_ARTICLES_VIEWS_API = {
  url: `${API_BASE_URL}article-user/article-view-api/`,
  method: 'GET',
};
const ADD_ARTICLE_VIEW_API = {
  url: `${API_BASE_URL}article-user/article-view-api/`,
  method: 'POST',
};

// Likes
const GET_ARTICLES_LIKE_API = {
  url: `${API_BASE_URL}article-user/article-like-api/`,
  method: 'GET',
};
const ADD_ARTICLE_LIKE_API = {
  url: `${API_BASE_URL}article-user/article-like-api/`,
  method: 'POST',
};
const DELETE_ARTICLE_LIKE_API = {
  url: `${API_BASE_URL}article-user/article-like-api/`,
  method: 'DELETE',
};
const GET_USER_LIKED_ARTICLES_API = {
  url: `${API_BASE_URL}article-user/article-like-users-list/`,
  method: 'GET',
};

// Like & views
const GET_ARTICLE_LIKE_VIEW_COUNT_API = {
  url: `${API_BASE_URL}article-user/article-like-view-count/`,
  method: 'GET',
  effect: 'every',
};

// Comment
const GET_ARTILCE_COMMENT_API = {
  url: `${API_BASE_URL}article-user/article-comment-api/`,
  method: 'GET',
};
const ADD_ARTILCE_COMMENT_API = {
  url: `${API_BASE_URL}article-user/article-comment-api/`,
  method: 'POST',
};
const EDIT_ARTILCE_COMMENT_API = {
  url: ({ id }) =>
    `${API_BASE_URL}article-user/article-comment-update-api/${id}/`,
  method: 'PUT',
};
const DELETE_ARTILCE_COMMENT_API = {
  url: `${API_BASE_URL}article-user/article-comment-api/`,
  method: 'DELETE',
};

// Child
const GET_USERS_CHILDREN_API = {
  url: `${API_BASE_URL}parents/parents-child-profile/`,
  method: 'GET',
};
const ADD_USERS_CHILDREN_API = {
  url: `${API_BASE_URL}parents/parents-child-profile/`,
  method: 'POST',
};
const EDIT_USERS_CHILDREN_API = {
  url: ({ id }) => `${API_BASE_URL}parents/parents-child-profile-update/${id}/`,
  method: 'PUT',
};
const DELETE_USERS_CHILDREN_API = {
  url: `${API_BASE_URL}parents/parents-child-profile/`,
  method: 'DELETE',
};

// Schedule
const GET_USERS_TOUR_SCHEDULE_API = {
  url: `${API_BASE_URL}parents/schedule-tour/?type=schedule`,
  method: 'GET',
};
const GET_USERS_TOUR_UNSCHEDULE_API = {
  url: `${API_BASE_URL}parents/schedule-tour/?type=unschedule`,
  method: 'GET',
};
const ADD_USERS_TOUR_SCHEDULE_API = {
  url: `${API_BASE_URL}parents/schedule-tour/`,
  method: 'POST',
};
const EDIT_USERS_TOUR_SCHEDULE_API = {
  url: ({ id }) => `${API_BASE_URL}parents/schedule-tour-update/${id}/`,
  method: 'PUT',
};
const DELETE_USERS_TOUR_SCHEDULE_API = {
  url: `${API_BASE_URL}parents/schedule-tour/`,
  method: 'DELETE',
};
const SCHEDULE_CHECK_API = {
  url: `${API_BASE_URL}parents/schedule-tour-check/`,
  method: 'POST',
};

// Online Program
const DASHBOARD_ONLINE_PROGRAM_LIST_API = {
  url: `${API_BASE_URL}online/get-dashboard-list/`,
  method: 'GET',
};
const ONLINE_PROGRAM_SEARCH_API = {
  url: `${API_BASE_URL}online/online-provider-listing/`,
  method: 'GET',
};
const ONLINE_PROGRAM_DETAIL_API = {
  url: ({ id }) => `${API_BASE_URL}online/get-program-detail/${id}/`,
  method: 'GET',
};
const CREATE_ONLINE_PROGRAM_ORDER_API = {
  url: `${API_BASE_URL}online-order/create-order-online-program/`,
  method: 'POST',
};
const ONLINE_PROGRAM_PAYMENT_STATUS_API = {
  url: ({ orderId }) =>
    `${API_BASE_URL}online-order/update-order-by-payment-id/${orderId}/`,
  method: 'PUT',
};
const SUGGESTED_ONLINE_PROGRAMS_API = {
  url: ({ id }) => `${API_BASE_URL}online/suggested-programs/${id}/`,
  method: 'GET',
};
const ONLINE_CATEGORY_LIST_API = {
  url: `${API_BASE_URL}online/subcategory-list/`,
  method: 'GET',
};
const ONLINE_PROGRAM_MAX_PRICE_API = {
  url: `${API_BASE_URL}online/get-max-price/`,
  method: 'GET',
};
const ONLINE_PROGRAM_CITIES_API = {
  url: `${API_BASE_URL}online/get-city-list/`,
  method: 'GET',
};
const ONLINE_PROGRAM_PURCHASED_API = {
  url: `${API_BASE_URL}online-order/program-purchased-data/`,
  method: 'GET',
};
const ONLINE_PROGRAM_PURCHASED_PROGRAMS_API = {
  url: `${API_BASE_URL}online-order/purchased-programs/`,
  method: 'GET',
};
const GET_TRANSACTION_LIST_API = {
  url: `${API_BASE_URL}online-order/transaction-programs/`,
  method: 'GET',
};
const DOWNLOAD_INVOICE_API = {
  url: `${API_BASE_URL}online-order/download-invoice/`,
  method: 'POST',
};
const ONLINE_PROGRAM_CALLBACK_REQUEST_API = {
  url: `${API_BASE_URL}parents/parent-online-enquire/`,
  method: 'POST',
};
const ONLINE_PROGRAM_REVIEW_API = {
  url: `${API_BASE_URL}parent-online/parent-review-api/`,
  method: 'POST',
};
const GET_ONLINE_PROGRAMS_REVIEWS_API = {
  url: ({ id }) => `${API_BASE_URL}online/get-provider-reviews/${id}/`,
  method: 'GET',
};
const GET_ONLINE_PROGRAM_WISHLIST_API = {
  url: `${API_BASE_URL}parent-online/create-wishlist/`,
  method: 'GET',
};
const CREATE_ONLINE_PROGRAM_WISHLIST_API = {
  url: `${API_BASE_URL}parent-online/create-wishlist/`,
  method: 'POST',
};
const DELETE_ONLINE_PROGRAM_WISHLIST_API = {
  url: ({ id }) => `${API_BASE_URL}parent-online/remove-bookmark/${id}/`,
  method: 'DELETE',
};

// Daycare
const DAYCARE_SEARCH_API = {
  url: `${API_BASE_URL}day-care/`,
  method: 'GET',
};
const DAYCARE_TRIP_SEARCH_CORDS_API = {
  url: `${API_BASE_URL}day-care/route-coordinates`,
  method: 'GET',
};
const DAYCARE_TRIP_SEARCH_RESULTS_API = {
  url: `${API_BASE_URL}day-care/route-suggestions`,
  method: 'GET',
};
const SUBMIT_CONTACT_API = {
  url: `${API_BASE_URL}day-care/`,
  method: 'POST',
};
const DAYCARE_SUGGESTION_API = {
  url: `${API_BASE_URL}day-care/suggestions/`,
  method: 'GET',
};
const GET_DEFAULT_FILTERS_API = {
  url: `${API_BASE_URL}parents/filters/selected`,
  method: 'GET',
};

// DAYCARE DETAIL
const PROVIDER_MEDIA_API = {
  url: ({ id }) => `${API_BASE_URL}day-care/provider-media/${id}/`,
  method: 'GET',
};
const PROVIDER_BASIC_INFO_API = {
  url: ({ id }) => `${API_BASE_URL}day-care/provider-basic-info/${id}/`,
  method: 'GET',
};
const PROVIDER_FACULTY_API = {
  url: ({ id }) => `${API_BASE_URL}day-care/provider-faculty/${id}/`,
  method: 'GET',
};
const PROVIDER_LOCATION_API = {
  url: ({ id }) => `${API_BASE_URL}day-care/provider-location/${id}/`,
  method: 'GET',
};
const PROVIDER_FACILITY_API = {
  url: ({ id }) => `${API_BASE_URL}day-care/provider-facility/${id}/`,
  method: 'GET',
};
const PROVIDER_SCHEDULE_CHOICE_API = {
  url: `${API_BASE_URL}day-care/schedule-choice/`,
  method: 'GET',
};
const PROVIDER_DIRECTOR_INFO_API = {
  url: ({ id }) => `${API_BASE_URL}day-care/${id}/`,
  method: 'GET',
};
const PROVIDER_EMERGENCY_SERVICES_API = {
  url: `${API_BASE_URL}day-care/emergency-services/`,
  method: 'GET',
};
const PROVIDER_CONTACT_DETAILS_API = {
  url: ({ id }) => `${API_BASE_URL}day-care/provider-contact-detail/${id}/`,
  method: 'GET',
};
const PROVIDER_LANGUAGES_API = {
  url: `${API_BASE_URL}day-care/languages/`,
  method: 'GET',
};
const PROVIDER_YEARLY_CALENDER_API = {
  url: ({ id }) => `${API_BASE_URL}day-care/yearly-calendar/${id}/`,
  method: 'GET',
};
const PROVIDER_SCHEDULE_FOR_DAY_API = {
  url: ({ id }) => `${API_BASE_URL}day-care/schedule-for-the-day/${id}/`,
  method: 'GET',
};
const PROVIDER_WORKING_DAY_API = {
  url: ({ id }) => `${API_BASE_URL}day-care/working-day/${id}/`,
  method: 'GET',
};

const PROVIDER_CONTENT_SCHEDULE_API = {
  url: ({ id }) =>
    `https://adminbe.kiddenz.com/api/providers/provider-content-schedule/${id}/`,
  method: 'GET',
};

// Articles
const ARTICLES_GET_ALL_CATEGORIES_API = {
  url: `${API_BASE_URL}articles/get-category/`,
  method: 'GET',
  effect: 'every',
};
const ARTICLES_GET_DASHBOARD_LIST_API = {
  url: `${API_BASE_URL}articles/get-dashboard-list/`,
  method: 'GET',
};
const GET_CATEGORY_ARTICLES_API = {
  url: ({ slugName }) =>
    `${API_BASE_URL}articles/get-category-articles/${slugName}/`,
  method: 'GET',
};
const GET_ARTICLES_BY_ID_API = {
  url: ({ id }) => `${API_BASE_URL}articles/get-article/${id}/`,
  method: 'GET',
};
const GET_INTRESTING_ARTICLES_API = {
  url: `${API_BASE_URL}articles/get-featured-dashboard-list/`,
  method: 'GET',
};
const ARTICLES_SEARCH_API = {
  url: `${API_BASE_URL}articles/search-list/`,
  method: 'GET',
};
const GET_SUBCATEGORY_ARTICLES_API = {
  url: ({ slugName }) =>
    `${API_BASE_URL}articles/get-subcategory-articles/${slugName}/`,
  method: 'GET',
};
const GET_HASHTAG_ARTICLES_API = {
  url: ({ hashtag }) =>
    `${API_BASE_URL}articles/get-hastag-articles/${hashtag}/`,
  method: 'GET',
};
const GET_MOST_READ_ARTICLES_API = {
  url: `${API_BASE_URL}article-user/article-most-read/`,
  method: 'GET',
};
const GET_ARICLES_BY_HASHTAGS_LIST_API = {
  url: `${API_BASE_URL}articles/get-articles-hastag-list/`,
  method: 'POST',
};
const GET_USER_IP_API = {
  url: `${API_BASE_URL}users/get-client-ip/`,
  method: 'GET',
};

// Provider
const PROVIDER_DETAIL_API = {
  url: `${API_BASE_URL}parents/provider-add/`,
  method: 'POST',
};

export const dashboard = {
  // Test
  TEST_API,
  TEST_SUB_API,

  // Users
  USER_REVIEW_API,
  GET_USER_DETAILS_API,
  EDIT_USER_DETAILS_API,
  USER_EMAIL_VERIFY_API,
  USER_ENQUIRY_API,
  USER_CONNECT_API,
  GET_CLICK_COUNT_API,
  POST_CLICK_COUNT_API,

  // Child
  GET_USERS_CHILDREN_API,
  ADD_USERS_CHILDREN_API,
  EDIT_USERS_CHILDREN_API,
  DELETE_USERS_CHILDREN_API,

  // Schedule
  GET_USERS_TOUR_SCHEDULE_API,
  GET_USERS_TOUR_UNSCHEDULE_API,
  ADD_USERS_TOUR_SCHEDULE_API,
  EDIT_USERS_TOUR_SCHEDULE_API,
  DELETE_USERS_TOUR_SCHEDULE_API,
  SCHEDULE_CHECK_API,

  // Wishlist
  GET_USER_WISHLISTED_PRODUCTS_API,
  ADD_WISLIST_ITEM_API,
  DELETE_WISLIST_ITEM_API,

  // Bookmark
  GET_USER_BOOKMARK_ARTICLES_API,
  ADD_BOOKMARK_ARTICLE_API,
  DELETE_BOOKMARK_ARTICLE_API,

  // Views
  GET_ARTICLES_VIEWS_API,
  ADD_ARTICLE_VIEW_API,

  // Likes
  GET_ARTICLES_LIKE_API,
  ADD_ARTICLE_LIKE_API,
  DELETE_ARTICLE_LIKE_API,
  GET_USER_LIKED_ARTICLES_API,

  // Likes & Views
  GET_ARTICLE_LIKE_VIEW_COUNT_API,

  // Comment
  GET_ARTILCE_COMMENT_API,
  ADD_ARTILCE_COMMENT_API,
  EDIT_ARTILCE_COMMENT_API,
  DELETE_ARTILCE_COMMENT_API,

  // Online Program
  DASHBOARD_ONLINE_PROGRAM_LIST_API,
  ONLINE_PROGRAM_SEARCH_API,
  ONLINE_PROGRAM_DETAIL_API,
  CREATE_ONLINE_PROGRAM_ORDER_API,
  ONLINE_PROGRAM_PAYMENT_STATUS_API,
  SUGGESTED_ONLINE_PROGRAMS_API,
  ONLINE_CATEGORY_LIST_API,
  ONLINE_PROGRAM_MAX_PRICE_API,
  ONLINE_PROGRAM_CITIES_API,
  ONLINE_PROGRAM_PURCHASED_API,
  ONLINE_PROGRAM_PURCHASED_PROGRAMS_API,
  GET_TRANSACTION_LIST_API,
  DOWNLOAD_INVOICE_API,
  ONLINE_PROGRAM_CALLBACK_REQUEST_API,
  ONLINE_PROGRAM_REVIEW_API,
  GET_ONLINE_PROGRAMS_REVIEWS_API,
  GET_ONLINE_PROGRAM_WISHLIST_API,
  CREATE_ONLINE_PROGRAM_WISHLIST_API,
  DELETE_ONLINE_PROGRAM_WISHLIST_API,

  // DayCare Search
  DAYCARE_SEARCH_API,
  DAYCARE_TRIP_SEARCH_CORDS_API,
  DAYCARE_TRIP_SEARCH_RESULTS_API,
  SUBMIT_CONTACT_API,
  DAYCARE_SUGGESTION_API,
  GET_DEFAULT_FILTERS_API,

  // DAYCARE DETAIL
  PROVIDER_MEDIA_API,
  PROVIDER_BASIC_INFO_API,
  PROVIDER_FACULTY_API,
  PROVIDER_LOCATION_API,
  PROVIDER_FACILITY_API,
  PROVIDER_SCHEDULE_CHOICE_API,
  PROVIDER_DIRECTOR_INFO_API,
  PROVIDER_EMERGENCY_SERVICES_API,
  PROVIDER_CONTACT_DETAILS_API,
  PROVIDER_LANGUAGES_API,
  PROVIDER_YEARLY_CALENDER_API,
  PROVIDER_SCHEDULE_FOR_DAY_API,
  PROVIDER_WORKING_DAY_API,
  PROVIDER_CONTENT_SCHEDULE_API,

  // ARTICLES
  ARTICLES_GET_ALL_CATEGORIES_API,
  ARTICLES_GET_DASHBOARD_LIST_API,
  GET_CATEGORY_ARTICLES_API,
  GET_SUBCATEGORY_ARTICLES_API,
  GET_ARTICLES_BY_ID_API,
  GET_INTRESTING_ARTICLES_API,
  GET_HASHTAG_ARTICLES_API,
  ARTICLES_SEARCH_API,
  GET_MOST_READ_ARTICLES_API,
  GET_ARICLES_BY_HASHTAGS_LIST_API,
  GET_USER_IP_API,

  // Providers
  PROVIDER_DETAIL_API,
};
/* ********************* Dashboard End ********************* */

export const dontResetOnLogout = {
  dashboard: {
    ARTICLES_GET_DASHBOARD_LIST_API,
    ARTICLES_GET_ALL_CATEGORIES_API,
    GET_CATEGORY_ARTICLES_API,
    GET_ARTICLES_BY_ID_API,
    GET_INTRESTING_ARTICLES_API,
    ARTICLES_SEARCH_API,
    GET_SUBCATEGORY_ARTICLES_API,
    GET_ARTICLES_VIEWS_API,
    GET_ARTICLES_LIKE_API,
    GET_MOST_READ_ARTICLES_API,
    PROVIDER_MEDIA_API,
    PROVIDER_LOCATION_API,
    PROVIDER_BASIC_INFO_API,
    PROVIDER_DIRECTOR_INFO_API,
    PROVIDER_SCHEDULE_CHOICE_API,
    PROVIDER_SCHEDULE_FOR_DAY_API,
    PROVIDER_WORKING_DAY_API,
    DAYCARE_SEARCH_API,
    DAYCARE_TRIP_SEARCH_CORDS_API,
    DAYCARE_TRIP_SEARCH_RESULTS_API,
    DAYCARE_SUGGESTION_API,
  },
};

export default {
  authentication,
  dashboard,
};

export { BASE_URL };
