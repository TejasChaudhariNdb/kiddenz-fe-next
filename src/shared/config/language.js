const checkLanguageIsAvailable = (lang, json) => json[lang] || json.DEFAULT;
export default (lang = 'DEFAULT') => ({
  Vijayraja: checkLanguageIsAvailable(lang, {
    EN: 'Vijayraja',
    TA: 'விஜய்ராஜா',
    DEFAULT: 'Vijayraja',
  }),
  Projects: checkLanguageIsAvailable(lang, {
    EN: 'Projects',
    TA: 'திட்டங்கள்',
    DEFAULT: 'Projects',
  }),
  Home: checkLanguageIsAvailable(lang, {
    EN: 'Home',
    TA: 'வீடு',
    DEFAULT: 'Home',
  }),
  'About Us': checkLanguageIsAvailable(lang, {
    EN: 'About Us',
    TA: 'எங்களை பற்றி',
    DEFAULT: 'About Us',
  }),
  'Login or Register': checkLanguageIsAvailable(lang, {
    EN: 'Login or Register',
    TA: 'உள்நுழைக அல்லது பதிவுசெய்க',
    DEFAULT: 'Login or Register',
  }),
  Send: checkLanguageIsAvailable(lang, {
    EN: 'Send',
    TA: 'அனுப்பு',
    DEFAULT: 'Send',
  }),
  'Mobile number should be 10 digits long': checkLanguageIsAvailable(lang, {
    EN: 'Mobile number should be 10 digits long',
    TA: 'மொபைல் எண் 10 இலக்கங்கள் நீளமாக இருக்க வேண்டும்',
    DEFAULT: 'Mobile number should be 10 digits long',
  }),
  'Mobile number must be 10 digits long': checkLanguageIsAvailable(lang, {
    EN: 'Mobile number must be 10 digits long',
    TA: 'மொபைல் எண் கட்டாயம் 10 இலக்கங்கள் நீளமாக இருக்கும்',
    DEFAULT: 'Mobile number must be 10 digits long',
  }),
  'Enter 10 digit mobile number': checkLanguageIsAvailable(lang, {
    EN: 'Enter 10 digit mobile number',
    TA: '10 இலக்க மொபைல் எண்ணை உள்ளிடவும்',
    DEFAULT: 'Enter 10 digit mobile number',
  }),
});
