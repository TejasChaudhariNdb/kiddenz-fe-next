/* eslint-disable import/no-unresolved */
/* eslint-disable global-require */
/* eslint-disable no-console */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import injectSaga from '../../../utils/injectSaga';
import injectReducer, { useInjectReducer } from '../../../utils/injectReducer';
import { makeSelectDashboardState } from './selectors';
import { componentActions } from './actions';
import reducer from './reducer';
import saga from './generator';
import nullcheck from '../../utils/nullCheck';
import { getData, mapDispatchToProps } from '../../utils';
// import { useChatHooks } from '../../hooks';
const safe = nullcheck;
// let initial = true;
// eslint-disable-next-line no-unused-vars
export default function(WrapperComponent, isMobile = false) {
  function Dashboard(props) {
    useInjectReducer({
      key: 'dashboard',
      reducer,
    });
    // useChatHooks(props);
    // useEffect(() => {
    // if (initial) {
    //   initial = false;
    //   try {
    //     let firebase =
    //       // eslint-disable-next-line import/no-unresolved
    //       (isMobile && require('react-native-firebase').default) ||
    //       require('firebase');
    //     if (!isMobile) {
    //       const firebaseConfig = {
    //         apiKey: 'AIzaSyBtwfH-m6ausTQ0guPNP9I1kmtwVO4G7WE',
    //         authDomain: 'vijayraja-1bbc8.firebaseapp.com',
    //         databaseURL: 'https://vijayraja-1bbc8.firebaseio.com',
    //         projectId: 'vijayraja-1bbc8',
    //         storageBucket: '',
    //         messagingSenderId: '686222593179',
    //         appId: '1:686222593179:web:34a4771f0be38115450a3e',
    //       };
    //       firebase = firebase.initializeApp(firebaseConfig);
    //     }
    //     const messaging = firebase.messaging();
    //     messaging
    //       .getToken()
    //       .then(currentToken => {
    //         if (currentToken) {
    //           if (!isMobile) {
    //             fetch(
    //               `https://iid.googleapis.com/iid/v1/${currentToken}/rel/topics/general`,
    //               {
    //                 method: 'POST',
    //                 headers: new Headers({
    //                   Authorization:
    //                     'key=AAAAn8YNzJs:APA91bEn_xcpw2WQTrfdyT-J15RBl_oexelpYycxbyXR_z_bj_c-ZMFu3L1wRxZ-qHZpKiARB5Ql_7Ed0_boEM3grLCdYRAPJwnoo5CI5Un3SKHWpG27NJUn5PaYcNeXN6h9C9KCjfHn',
    //                 }),
    //               },
    //             )
    //               .then(response => {
    //                 if (response.status < 200 || response.status >= 400) {
    //                   console.log(
    //                     `Error subscribing to topic: ${
    //                       response.status
    //                     } - ${response.text()}`,
    //                   );
    //                 }
    //                 console.log('Subscribed to common topic');
    //               })
    //               .catch(error => {
    //                 console.error(error);
    //               });
    //           }
    //         } else {
    //           console.log(
    //             'No Instance ID token available. Request permission to generate one.',
    //           );
    //         }
    //       })
    //       .catch(err => {
    //         console.log('An error occurred while retrieving token. ', err);
    //       });
    //     if (isMobile) {
    //       firebase.messaging().subscribeToTopic('general');
    //       firebase.notifications().onNotification(notification => {
    //         // Process your notification as required
    //         console.log(notification);
    //         props.LIST_NOTIFICATIONS_API_CALL();
    //       });
    //     }
    //     // }
    //   } catch (err) {
    //     console.log(err);
    //   }
    // }
    //   if (
    //     props.authentication.isLoggedIn &&
    //     !props.dashboard.LIST_ALL_USERS_WISHLISTS_ID_API.loading.status &&
    //     props.dashboard.LIST_ALL_USERS_WISHLISTS_ID_API.lastUpdated ===
    //       undefined
    //   ) {
    //     props.LIST_ALL_USERS_WISHLISTS_ID_API_CALL();
    //     props.LIST_NOTIFICATIONS_API_CALL();
    //   }
    // }, [props.authentication.profile.id]);
    return <WrapperComponent safe={safe} {...props} getData={getData} />;
  }

  Dashboard.propTypes = {
    authentication: PropTypes.object.isRequired,
    dashboard: PropTypes.object.isRequired,
  };

  const mapStateToProps = createStructuredSelector({
    dashboard: makeSelectDashboardState(),
  });

  const dashboardReducer = injectReducer({
    key: 'dashboard',
    reducer,
  });
  const dashboardSaga = injectSaga({ key: 'dashboard', saga });

  const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps(componentActions),
  );

  return compose(
    withConnect,
    dashboardReducer,
    dashboardSaga,
  )(Dashboard);
}
