/* eslint-disable no-unused-vars */
import { call } from 'redux-saga/effects';
import { DEFAULT_SAGA_HANDLER as defaultSagaHandler } from '../../utils/commonHandlers/commonSagaHandler';
import { ON_REQUEST } from '../../utils/commonReduxSagaConverter/commonConstants';
import { newObject } from '../../utils/helpers';

export function* requestResponseHandler({
  data: {
    data: {
      status: successStatus,
      data: successData = {},
      message: successMessage,
      ...restSuccessData
    } = {},
  } = {},
  request,
  action,
  type,
  payload: { payload = {}, query = {}, params = {}, ...restPayload } = {},
  method,
  actionData,
  axiosCancel,
  error: {
    response: {
      data: {
        status: errorStatus,
        data: errorData = [],
        message: errorMessage,
        ...restErrorData
      } = {},
    } = {},
  } = {},
  cancelled,
}) {
  let requestData = {};
  if (method === ON_REQUEST) requestData = newObject(request);
  const DEFAULT_SAGA_HANDLER = defaultSagaHandler.bind(null, {
    method,
    action,
    successData,
    requestData,
    successStatus,
    restSuccessData,
    errorStatus,
    errorData,
  });
  switch (type) {
    default:
      return yield call(DEFAULT_SAGA_HANDLER);
  }
}

export default [];
