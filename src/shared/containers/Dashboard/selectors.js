import { createSelector } from 'reselect';
import { initialState } from './reducer';
import { constants, generatorKey } from './constants';
import * as apiEndPoints from '../../config/apiEndPoints';
import { CALL } from '../../utils/commonReduxSagaConverter/commonConstants';
import { newObject } from '../../utils/helpers';

const selectDashboardDomain = state => state.dashboard || initialState;

const makeSelectDashboardState = () =>
  createSelector(
    selectDashboardDomain,
    substate =>
      Object.keys(apiEndPoints[generatorKey]).reduce(
        (acc, key) => newObject(acc, { [key]: substate[constants[key][CALL]] }),
        {},
      ),
  );

export { makeSelectDashboardState };
