/* eslint-disable no-unused-vars */
import {
  ON_SUCCESS,
  CALL,
} from '../../utils/commonReduxSagaConverter/commonConstants';
import { isMobile as isMobileApp } from '../../config';
import { commmonStateHandler } from '../../utils';
import { newObject } from '../../utils/helpers';
import {
  initialState as InitialState,
  resetState as ResetState,
} from './constants';
import { DEFAULT_REDUCER_HANDLER } from '../../utils/commonHandlers/commonReducerHandler';
import { constants as authenticationConstants } from '../Authentication/constants';

export const initialState = newObject(InitialState, {});
const otherReducerConstants = [authenticationConstants.LOGOUT_API[CALL]];
// const otherReducerConstants = [];

function updateState({ state, newState, action, reset }) {
  const {
    response: {
      data: { data: successData = {}, ...restSuccessData } = {},
      payload: { payload = {}, query = {}, params = {}, ...restPayload } = {},
      method,
      type,
      status: loadingStatus,
      statusCode,
      message: statusMessage,
      error: { data: errorData = {}, ...restErrorData } = {},
    } = {},
  } = action;
  switch (type) {
    // case authenticationConstants.USER_LOGOUT_API[CALL]:
    //   switch (method) {
    //     case ON_SUCCESS:
    //       return isMobileApp
    //         ? newObject(InitialState)
    //         : newObject(state, ResetState);
    //     default:
    //       return state;
    //   }

    case authenticationConstants.LOGOUT_API[CALL]:
      switch (method) {
        case ON_SUCCESS:
          return newObject(state, ResetState);
        default:
          return state;
      }
    default:
      return DEFAULT_REDUCER_HANDLER({
        method,
        reset,
        state,
        action,
        type,
      });
  }
}

export default (state = initialState, action) => {
  switch (action.type) {
    default: {
      const newState = newObject.bind({}, state);
      const { response: { method, type } = {} } = action;
      const execute = Object.keys(InitialState)
        .concat(otherReducerConstants)
        .includes(type || action.type);
      const constants = InitialState;
      if (execute)
        return commmonStateHandler({
          constants,
          state,
          action,
          method,
          newState,
          updateState,
        });
      return state;
    }
  }
};
