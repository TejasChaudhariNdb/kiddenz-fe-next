import actionConverter from '../../utils/commonReduxSagaConverter/actionConverter';
import { convertData } from '../../utils/commonReduxSagaConverter/sagaConverter';
import { generatorKey } from './constants';
const {
  componentActions,
  actions,
  sagaActions,
  cancelActions,
} = actionConverter(convertData[generatorKey].actions);

export { componentActions, actions, sagaActions, cancelActions };
