import * as apiEndPoints from '../../config/apiEndPoints';
import { convertData } from '../../utils/commonReduxSagaConverter/sagaConverter';
import { CALL } from '../../utils/commonReduxSagaConverter/commonConstants';
import { newObject } from '../../utils/helpers';
const generatorKey = 'dashboard';
const initialState = Object.keys(apiEndPoints[generatorKey]).reduce(
  (acc, key) =>
    newObject(acc, {
      [convertData[generatorKey].constants[key][CALL]]: {
        loading: {},
        toast: {},
      },
    }),
  {},
);

const resetState = Object.keys(apiEndPoints[generatorKey]).reduce(
  (acc, key) =>
    (!Object.keys(apiEndPoints.dontResetOnLogout[generatorKey] || {}).includes(
      key,
    ) &&
      newObject(acc, {
        [convertData[generatorKey].constants[key][CALL]]: {
          loading: {},
          toast: {},
        },
      })) ||
    acc,
  {},
);
const { constants } = convertData[generatorKey];
export { constants, initialState, generatorKey, resetState };
/* <============================ END ==============================> */
