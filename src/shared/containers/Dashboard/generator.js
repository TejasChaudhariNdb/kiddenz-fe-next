import { all } from 'redux-saga/effects';
import _sagaHandler from '../../utils/commonReduxSagaConverter/commonGenerator';
import OtherGenerator, { requestResponseHandler } from './saga';
import { convertData } from '../../utils/commonReduxSagaConverter/sagaConverter';
import { generatorKey } from './constants';

const [generatorPattern, sagaGenerator] = _sagaHandler({
  requestResponseHandler,
  actionType: convertData[generatorKey].sagaConfig,
});

// For Test Purpose
export const Generator = sagaGenerator;

export default function*() {
  yield all(generatorPattern.concat(OtherGenerator || []));
}
