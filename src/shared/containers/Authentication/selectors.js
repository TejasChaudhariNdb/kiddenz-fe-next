import { createSelector } from 'reselect';
import { initialState } from './reducer';
import { constants, generatorKey } from './constants';
import { CALL } from '../../utils/commonReduxSagaConverter/commonConstants';
import * as apiEndPoints from '../../config/apiEndPoints';
import { newObject } from '../../utils/helpers';

const selectAuthenticationDomain = state =>
  state.authentication || initialState;

const makeSelectAuthenticationState = () =>
  createSelector(selectAuthenticationDomain, substate =>
    newObject(
      {
        profile: substate.profile,
        isLoggedIn: substate.isLoggedIn,
        authorization: substate.authorization,
        categoryColors: substate.categoryColors,
        categoriesConfig: substate.categoriesConfig,
        language: substate.language,
        currentLocation: substate.currentLocation,
      },
      Object.keys(apiEndPoints[generatorKey]).reduce(
        (acc, key) => ({
          ...acc,
          [key]: substate[constants[key][CALL]],
        }),
        {},
      ),
    ),
  );

export { selectAuthenticationDomain, makeSelectAuthenticationState };
