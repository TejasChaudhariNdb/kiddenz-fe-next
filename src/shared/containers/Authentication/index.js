/* eslint-disable import/no-unresolved */
/**
 * Dashboard
 */
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { getLanguage } from '../../config/Language/index';

import injectSaga from '../../../utils/injectSaga';
import injectReducer from '../../../utils/injectReducer';
import { makeSelectAuthenticationState } from './selectors';
import { componentActions } from './actions';
import reducer from './reducer';
import saga from './generator';
import nullcheck from '../../utils/nullCheck';
import { getData, mapDispatchToProps } from '../../utils';

const safe = nullcheck;

// eslint-disable-next-line no-unused-vars
export default function(WrapperComponent, autoLoginCheck = true) {
  function Authentication(props) {
    const [language, setLanguage] = useState(getLanguage('EN'));
    useEffect(
      () => {
        if (props.authentication.language !== language)
          setLanguage(getLanguage(props.authentication.language));
      },
      [props.authentication.language],
    );
    // useEffect(() => {
    //   /* auto login check */
    //   if (
    //     autoLoginCheck &&
    //     props.authentication.USER_PROFILE_API.lastUpdated === undefined &&
    //     !safe(props, '.authentication.profile.name', '')
    //   ) {
    //     props.USER_PROFILE_API_CALL();
    //   }
    // }, []);

    return (
      <WrapperComponent
        safe={safe}
        language={language}
        {...props}
        getData={getData}
      />
    );
  }

  Authentication.propTypes = {
    // USER_PROFILE_API_CALL: PropTypes.func.isRequired,
    authentication: PropTypes.object.isRequired,
  };

  const mapStateToProps = createStructuredSelector({
    authentication: makeSelectAuthenticationState(),
  });

  const authenticationReducer = injectReducer({
    key: 'authentication',
    reducer,
  });
  const authenticationSaga = injectSaga({ key: 'authentication', saga });

  const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps(componentActions),
  );

  return compose(
    withConnect,
    authenticationReducer,
    authenticationSaga,
  )(Authentication);
}
