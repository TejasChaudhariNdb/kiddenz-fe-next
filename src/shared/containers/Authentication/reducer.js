/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
import {
  ON_ERROR,
  ON_SUCCESS,
  CALL,
} from '../../utils/commonReduxSagaConverter/commonConstants';
import { commmonStateHandler } from '../../utils';
import { newObject, generateTimeStamp } from '../../utils/helpers';
import {
  constants as authenticationConstants,
  initialState as InitialState,
} from './constants';
import { COMMON_REDUCER_HANDLER } from '../../utils/commonHandlers/commonReducerHandler';

const componentState = {
  profile: {},
  isLoggedIn: false,
  authorization: true,
  currentLocation: {},
  language: 'EN',
  categoryColors: {
    Activities: ['hsla(162, 70%, 35%, 1)', 'hsla(162, 70%, 80%, 0.2)'],
    Awareness: ['hsla(18, 70%, 35%, 1)', 'hsla(18, 70%, 80%, 0.2)'],
    'Behaviour and Discipline': [
      'hsla(320, 70%, 35%, 1)',
      'hsla(320, 70%, 80%, 0.2)',
    ],
    Dental: ['hsla(207, 70%, 35%, 1)', 'hsla(207, 70%, 80%, 0.2)'],
    Development: ['hsla(323, 70%, 35%, 1)', 'hsla(323, 70%, 80%, 0.2)'],
    'Education and Curriculum': [
      'hsla(253, 70%, 35%, 1)',
      'hsla(253, 70%, 80%, 0.2)',
    ],
    Feeding: ['hsla(199, 70%, 35%, 1)', 'hsla(199, 70%, 80%, 0.2)'],
    Health: ['hsla(85, 70%, 35%, 1)', 'hsla(85, 70%, 80%, 0.2)'],
    Nutrition: ['hsla(246, 70%, 35%, 1)', 'hsla(246, 70%, 80%, 0.2)'],
    'Overall Health': ['hsla(46, 70%, 35%, 1)', 'hsla(46, 70%, 80%, 0.2)'],
    'Parenting and Childcare': [
      'hsla(41, 70%, 35%, 1)',
      'hsla(41, 70%, 80%, 0.2)',
    ],
    'Potty Training': ['hsla(295, 70%, 35%, 1)', 'hsla(295, 70%, 80%, 0.2)'],
    'Sleeping Habits': ['hsla(239, 70%, 35%, 1)', 'hsla(239, 70%, 80%, 0.2)'],
    'Special Child': ['hsla(320, 70%, 35%, 1)', 'hsla(320, 70%, 80%, 0.2)'],
    Technology: ['hsla(163, 70%, 35%, 1)', 'hsla(163, 70%, 80%, 0.2)'],
    Travel: ['hsla(313, 70%, 35%, 1)', 'hsla(313, 70%, 80%, 0.2)'],
    Upbringing: ['hsla(186, 70%, 35%, 1)', 'hsla(186, 70%, 80%, 0.2)'],
    fun: ['hsla(239, 70%, 35%, 1)', 'hsla(239, 70%, 80%, 0.2)'],
  },
  categoriesConfig: {
    Activities: {
      color: '',
      icon: '',
      name: 'Activities',
      redirection: '/blog/category/development/activities',
      slugName: 'activities',
    },
    Awareness: {
      color: '',
      icon: '',
      name: 'Awareness',
      redirection: '/blog/category/development/awareness',
      slugName: 'awareness',
    },
    'Behaviour and Discipline': {
      color: '',
      icon: '',
      name: 'Behaviour and Discipline',
      redirection: '/blog/category/behaviour-and-discipline',
      slugName: 'behaviour-and-discipline',
    },
    Dental: {
      color: '',
      icon: '',
      name: 'Dental',
      redirection: '/blog/category/health/dental',
      slugName: 'dental',
    },
    Development: {
      color: '',
      icon: '',
      name: 'Development',
      redirection: '/blog/category/development',
      slugName: 'development',
    },
    'Education and Curriculum': {
      color: '',
      icon: '',
      name: 'Education and Curriculum',
      redirection: '/blog/category/education-and-curriculum',
      slugName: 'education-and-curriculum',
    },
    Feeding: {
      color: '',
      icon: '',
      name: 'Feeding',
      redirection: '/blog/category/health/feeding-your-toddler',
      slugName: 'feeding-your-toddler',
    },
    Health: {
      color: '',
      icon: '',
      name: 'Health',
      redirection: '/blog/category/health',
      slugName: 'health',
    },
    Nutrition: {
      color: '',
      icon: '',
      name: 'Nutrition',
      redirection: '/blog/category/health/nutrition',
      slugName: 'nutrition',
    },
    'Overall Health': {
      color: '',
      icon: '',
      name: 'Overall Health',
      redirection: '/blog/category/health/overall-health',
      slugName: 'overall-health',
    },
    'Parenting and Childcare': {
      color: '',
      icon: '',
      name: 'Parenting and Childcare',
      redirection: '/blog/category/parenting-and-childcare',
      slugName: 'parenting-and-childcare',
    },
    'Potty Training': {
      color: '',
      icon: '',
      name: 'Potty Training',
      redirection: '/blog/category/development/potty-training',
      slugName: 'potty-training',
    },
    'Sleeping Habits': {
      color: '',
      icon: '',
      name: 'Sleeping Habits',
      redirection: '/blog/category/health/sleeping-habits',
      slugName: 'sleeping-habits',
    },
    'Special Child': {
      color: '',
      icon: '',
      name: 'Special Child',
      redirection: '/blog/category/parenting-and-childcare/special-child',
      slugName: 'special-child',
    },
    Technology: {
      color: '',
      icon: '',
      name: 'Technology',
      redirection: '/blog/category/development/technology',
      slugName: 'technology',
    },
    Travel: {
      color: '',
      icon: '',
      name: 'Travel',
      redirection: '/blog/category/parenting-and-childcare/travel',
      slugName: 'travel',
    },
    Upbringing: {
      color: '',
      icon: '',
      name: 'Upbringing',
      redirection: '/blog/category/parenting-and-childcare/upbringing',
      slugName: 'upbringing',
    },
    fun: {
      color: '',
      icon: '',
      name: 'Fun',
      redirection: '/blog/category/development/fun',
      slugName: 'fun',
    },
  },
  parent_clicks: {},
};

const otherReducerConstants = [];

const LANGUAGE_CONSTANTS = 'LANGUAGE';

export const initialState = newObject(InitialState, componentState);

function updateState({ state, newState, action, reset }) {
  const {
    response: {
      data: { data: successData = {}, ...restSuccessData } = {},
      payload: { payload = {}, query = {}, params = {}, ...restPayload } = {},
      status: loadingStatus,
      statusCode,
      type,
      method,
      message: statusMessage,
      show_email_popup: emailPopup = false,
      error: { data: errorData = {}, ...restErrorData } = {},
    } = {},
  } = action;
  const [commonHandler] = COMMON_REDUCER_HANDLER(action);
  switch (type) {
    case authenticationConstants.REGISTER_API[CALL]:
      switch (method) {
        case ON_SUCCESS: {
          return newState(({ profile }) => ({
            profile: newObject(profile, successData),
          }));
        }
        default:
          return state;
      }
    case authenticationConstants.LOGIN_API[CALL]:
      switch (method) {
        case ON_SUCCESS: {
          return newState(({ profile }) => ({
            profile: newObject(profile, successData),
          }));
        }
        default:
          return state;
      }
    case authenticationConstants.VERIFY_OTP_API[CALL]:
      switch (method) {
        case ON_SUCCESS:
          return newState(({ [type]: Data }) => {
            const {
              data: { data, selected_filter, parent_clicks },
            } = successData;
            const newData = {
              ...successData,
              data: { ...data, selected_filter, parent_clicks },
            };
            return {
              profile: state.profile.showEmailPopUp ? state.profile : newData,
              isLoggedIn: state.profile.showEmailPopUp
                ? false
                : !!data.mobile_number,
              [type]: newObject(Data, {
                lastUpdated: generateTimeStamp(),
                data: newData,
              }),
            };
          });

        default:
          return state;
      }
    case authenticationConstants.ONBOARDING_PROFILE_UPDATE_API[CALL]:
      switch (method) {
        case ON_SUCCESS:
          return newState(({ [type]: Data }) => ({
            profile: { data: successData },
            isLoggedIn: true,
          }));
        default:
          return state;
      }
    case authenticationConstants.ONBOARDING_API[CALL]:
      switch (method) {
        case ON_SUCCESS:
          return newState(({ [type]: Data }) => {
            const { question_serializer: selected_filter } = successData;
            return {
              profile: {
                ...state.profile,
                is_onboarded: true,
                data: { ...state.profile.data, selected_filter },
              },
              isLoggedIn: true,
            };
          });
        default:
          return state;
      }
    case authenticationConstants.LOGOUT_API[CALL]:
      switch (method) {
        case ON_SUCCESS:
          return newObject(state, {
            profile: {},
            isLoggedIn: false,
            authorization: false,
          });
        default:
          return state;
      }

    // case authenticationConstants.EDIT_MOBILE_NUMBER_API[CALL]:
    //   return state;
    // case authenticationConstants.VERIFY_OTP_FOR_EDIT_MOBILE_NUMBER_API[CALL]:
    //   switch (method) {
    //     case ON_SUCCESS:
    //       return newState(({ [type]: Data }) => ({
    //         profile: successData,
    //         [type]: newObject(Data, {
    //           lastUpdated: generateTimeStamp(),
    //           data: successData,
    //         }),
    //       }));
    //     default:
    //       return state;
    //   }
    // case authenticationConstants.UPDATE_USER_DETAILS_API[CALL]:
    //   switch (method) {
    //     case ON_SUCCESS:
    //       return newState(({ profile }) => ({
    //         profile: newObject(profile, payload, successData),
    //         isLoggedIn: true,
    //       }));
    //     default:
    //       return state;
    //   }
    // case authenticationConstants.USER_LOGOUT_API[CALL]:
    //   switch (method) {
    //     case ON_SUCCESS:
    //       return newObject(initialState, {
    //         authorization: true,
    //       });
    //     default:
    //       return state;
    //   }
    // case authenticationConstants.UPDATE_PROFILE_API[CALL]:
    //   switch (method) {
    //     case ON_SUCCESS:
    //       return newState(({ profile, [type]: Data }) => ({
    //         profile: newObject(profile, successData),
    //       }));
    //     default:
    //       return state;
    //   }
    // case authenticationConstants.USER_PROFILE_API[CALL]:
    //   switch (method) {
    //     case ON_SUCCESS:
    //       return newState(({ profile, [type]: Data }) => ({
    //         authorization: true,
    //         isLoggedIn: !!successData.name,
    //         profile: newObject(profile, successData),
    //         [type]: newObject(Data, commonHandler()),
    //       }));
    //     case ON_ERROR:
    //       return newState({
    //         authorization: true,
    //       });
    //     default:
    //       return state;
    //   }
    default:
      return state;
  }
}

export default (state = initialState, action) => {
  switch (action.type) {
    case LANGUAGE_CONSTANTS:
      return newObject(state, { language: action.payload });
    case 'PROFILE_UPDATE':
      return newObject(state, ({ profile }) => ({
        profile: newObject(profile, action.payload),
        isLoggedIn: true,
        authorization: true,
      }));

    case 'CURRENT_LOCATION_UPATE':
      return newObject(state, ({ profile }) => ({
        currentLocation: action.payload,
      }));

    case 'CATEGORY_COLORS_UPDATE':
      return newObject(
        state,
        ({
          profile,
          isLoggedIn,
          authorization,
          // categoryColors,
          categoriesConfig,
        }) => ({
          profile,
          isLoggedIn,
          authorization,
          // categoryColors: newObject(categoryColors, action.payload.colors),
          // categoriesConfig: newObject(categoriesConfig, action.payload.config),
        }),
      );

    default: {
      const newState = newObject.bind({}, state);
      const { response: { method, type } = {} } = action;
      const execute = Object.keys(InitialState)
        .concat(otherReducerConstants)
        .includes(type || action.type);
      const constants = InitialState;
      if (execute)
        return commmonStateHandler({
          constants,
          state,
          action,
          method,
          newState,
          updateState,
        });
      return state;
    }
  }
};
