/* eslint-disable consistent-return */
/* eslint-disable react-hooks/rules-of-hooks */
import { useEffect, useMemo, useState, useCallback } from 'react';
import { removeNullValuesFromObject } from 'shared/utils/commonHelpers/miscHelpers';
import validateForm from '../../utils/formValidation';

const FORM_CONFIG = {
  email: null,
  name: null,
  business_name: null,
  location_address: null,
  city: null,
  state: null,
  pincode: null,
  phone_number: null,
  additional_phone_number: null,
  website: true,
  google_list: true,
  hear_about_kiddenz: 'Saw on google',
  misc_info: null,
};

const NOT_REQUIRED_FIELDS = [
  'additional_phone_number',
  'website',
  'hear_about_kiddenz',
  'google_list',
  'misc_info',
];

const NUMBER_INPUTS_LENGTH_CONFIG = {
  pincode: 6,
  phone_number: 10,
  additional_phone_number: 10,
};

export const useProviderHook = (
  {
    getData,
    PROVIDER_DETAIL_API_CALL,
    PROVIDER_DETAIL_API_CANCEL,
    dashboard: { PROVIDER_DETAIL_API } = {},
  },
  { onPostSuccess = () => {}, onPostError = () => {} } = {},
) => {
  const [value, setValue] = useState(FORM_CONFIG);
  const [error, setError] = useState({});

  const VALIDATOR_CONFIG = {
    email: { type: 'email', value: value.email },
    name: { type: 'name', value: value.name },
    business_name: { type: 'name', value: value.business_name },
    location_address: { type: 'string', value: value.location_address },
    city: { type: 'name', value: value.city },
    state: { type: 'name', value: value.state },
    pincode: { type: 'number', value: value.pincode },
    phone_number: { type: 'number', value: value.phone_number },
  };

  useEffect(() => () => PROVIDER_DETAIL_API_CANCEL(), []);

  // On Input Change
  const onInputChange = useCallback(
    (e, key, type = 'text', inputValue) => {
      const tempForm = { ...value };
      if (type === 'text') {
        const val = getPlatformBasedFieldValue(e);
        tempForm[key] = val;
      }
      if (type === 'number') {
        const val = getPlatformBasedFieldValue(e);
        if (
          NUMBER_INPUTS_LENGTH_CONFIG[key] &&
          (Number(val) === 0 || Number(val)) &&
          val.length <= NUMBER_INPUTS_LENGTH_CONFIG[key]
        ) {
          tempForm[key] = val;
        } else {
          return 0;
        }
      }
      if (type === 'radio') {
        tempForm[key] = inputValue;
      }
      setValue(tempForm);
    },
    [value, error],
  );

  // On Input Blur
  const onInputBlur = useCallback(
    (e, key) => {
      if (!NOT_REQUIRED_FIELDS.includes(key)) e.preventDefault();
      let errorObj = {};
      if (!NOT_REQUIRED_FIELDS.includes(key)) {
        errorObj = validateForm({
          [key]: {
            type: VALIDATOR_CONFIG[key].type,
            value: VALIDATOR_CONFIG[key].value,
          },
        });
      }

      if (errorObj.isError) {
        setError({ ...error, [key]: errorObj[key] });
      } else {
        setError({ ...error, [key]: null });
      }
    },
    [value, error],
  );

  // On Form Submit
  const onSubmit = () => {
    let errorObj = {};
    errorObj = validateForm({
      email: { type: 'email', value: value.email },
      name: { type: 'name', value: value.name },
      business_name: { type: 'name', value: value.business_name },
      location_address: { type: 'string', value: value.location_address },
      city: { type: 'name', value: value.city },
      state: { type: 'name', value: value.state },
      pincode: { type: 'number', value: value.pincode },
      phone_number: { type: 'number', value: value.phone_number },
    });

    if (!errorObj.isError) {
      const payload = {
        ...removeNullValuesFromObject(value),
        misc_info: value.misc_info ? 'value.misc_info' : 'nothing',
      };
      PROVIDER_DETAIL_API_CALL({
        payload,
        successCallback: () => onPostSuccess(),
        errorCallback: () => onPostError(),
      });
    } else {
      setError(errorObj);
    }
  };

  const providerDetail = useMemo(
    () => getData(PROVIDER_DETAIL_API, [], false),
    [PROVIDER_DETAIL_API],
  );

  return {
    loader: providerDetail.loader,
    formValues: value,
    onChange: onInputChange,
    onBlur: onInputBlur,
    onSubmit,
    error,
  };
};

// const FORM_INITIAL_STATE = {
//   email: null,
//   name: null,
//   business_name: null,
//   location_address: null,
//   city: null,
//   state: null,
//   pincode: null,
//   phone_number: null,
//   additional_phone_number: null,
//   website: true,
//   google_list: true,
//   hear_about_kiddenz: 'saw on google',
//   misc_info: null,
// };

// Helpers
function getPlatformBasedFieldValue(e) {
  return typeof e === 'object' ? e.target.value : e;
}
