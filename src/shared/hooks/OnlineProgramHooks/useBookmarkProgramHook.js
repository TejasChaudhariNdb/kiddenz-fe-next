/* eslint-disable no-unused-vars */
/* eslint-disable no-nested-ternary */
/* eslint-disable eqeqeq */
/* eslint-disable camelcase */
// useReviewHooks
import { useEffect, useMemo } from 'react';
import { ON_SUCCESS } from 'shared/utils/commonReduxSagaConverter/commonConstants';

//   { onSuccess, onError },
export const useBookmarkProgramHook = (
  {
    GET_ONLINE_PROGRAM_WISHLIST_API_CALL,
    GET_ONLINE_PROGRAM_WISHLIST_API_CUSTOM_TASK,
    GET_ONLINE_PROGRAM_WISHLIST_API_CANCEL,
    CREATE_ONLINE_PROGRAM_WISHLIST_API_CALL,
    CREATE_ONLINE_PROGRAM_WISHLIST_API_CANCEL,
    DELETE_ONLINE_PROGRAM_WISHLIST_API_CALL,
    DELETE_ONLINE_PROGRAM_WISHLIST_API_CANCEL,
    dashboard: {
      GET_ONLINE_PROGRAM_WISHLIST_API,
      CREATE_ONLINE_PROGRAM_WISHLIST_API,
      DELETE_ONLINE_PROGRAM_WISHLIST_API,
    },
    getData,
  },
  {
    onBookmarkAddSuccess = () => {},
    onBookmarkDelSuccess = () => {},
    onBookmarkAddError = () => {},
    onBookmarkDelError = () => {},
  } = {},
) => {
  useEffect(
    () => () => {
      GET_ONLINE_PROGRAM_WISHLIST_API_CANCEL();
      CREATE_ONLINE_PROGRAM_WISHLIST_API_CANCEL();
      DELETE_ONLINE_PROGRAM_WISHLIST_API_CANCEL();
    },
    [],
  );

  useEffect(() => {
    GET_ONLINE_PROGRAM_WISHLIST_API_CALL();
  }, []);

  // Add to Bookmark
  const postBookmark = (programId, isPreview = false) => {
    const payload = { online_program: programId };
    CREATE_ONLINE_PROGRAM_WISHLIST_API_CALL({
      payload,
      successCallback: () => {
        // if (!isPreview) {
        //   GET_ONLINE_PROGRAM_WISHLIST_API_CUSTOM_TASK(ON_SUCCESS, {
        //     tasks: [
        //       {
        //         task: 'isInfinite',
        //         params: {
        //           isInfinite: true,
        //         },
        //         value: [{ online_program: programId }],
        //       },
        //     ],
        //   });
        // } else {
        //   GET_ONLINE_PROGRAM_WISHLIST_API_CALL();
        // }
        GET_ONLINE_PROGRAM_WISHLIST_API_CALL();
        onBookmarkAddSuccess();
      },
      errorCallback: ({ message }) => {
        GET_ONLINE_PROGRAM_WISHLIST_API_CALL();
        onBookmarkAddError(message);
      },
    });
  };

  // Remove Wishlist
  const deleteBookmark = (programId, isPreview = false) => {
    const params = {
      id: programId,
    };
    DELETE_ONLINE_PROGRAM_WISHLIST_API_CALL({
      params,
      successCallback: () => {
        GET_ONLINE_PROGRAM_WISHLIST_API_CALL();
        // if (!isPreview) {
        //   GET_ONLINE_PROGRAM_WISHLIST_API_CUSTOM_TASK(ON_SUCCESS, {
        //     isDelete: true,
        //     id: [programId],
        //     key: 'online_program',
        //   });
        // } else {
        //   GET_ONLINE_PROGRAM_WISHLIST_API_CALL();
        // }
        onBookmarkDelSuccess();
      },
      errorCallback: ({ message }) => {
        GET_ONLINE_PROGRAM_WISHLIST_API_CALL();
        onBookmarkDelError(message);
      },
    });
  };

  //   const onWishlistLoadMore = skip =>
  //     GET_ONLINE_PROGRAM_WISHLIST_API_CALL({
  //       query: {
  //         limit: 3,
  //         skip,
  //       },
  //       subKey: ['wishlists'],
  //       isInfinite: true,
  //     });

  // useEffect(
  //   () => {
  //     if (userBookmarks.data.length > 0) {
  //       const postIds = userBookmarks.data.map(({ post_id }) => post_id);
  //       postIds.map(d =>
  //         GET_ARTICLE_LIKE_VIEW_COUNT_API_CALL({ query: { article_id: d } }),
  //       );
  //       // console.log(userBookmarks.data.map(({ post_id }) => post_id));
  //     }
  //   },
  //   [userBookmarks],
  // );

  const bookmarkedPrograms = useMemo(
    () => getData(GET_ONLINE_PROGRAM_WISHLIST_API, [], false),
    [GET_ONLINE_PROGRAM_WISHLIST_API],
  );
  const createBookmarkPrograms = useMemo(
    () => getData(CREATE_ONLINE_PROGRAM_WISHLIST_API, {}, false),
    [CREATE_ONLINE_PROGRAM_WISHLIST_API],
  );
  const deleteBookmarkPrograms = useMemo(
    () => getData(DELETE_ONLINE_PROGRAM_WISHLIST_API, {}, false),
    [DELETE_ONLINE_PROGRAM_WISHLIST_API],
  );

  return {
    bookmarkedPrograms: {
      data: bookmarkedPrograms.data,
      loader: bookmarkedPrograms.loader,
    },
    createBookmarkPrograms: {
      data: createBookmarkPrograms.data,
      loader: createBookmarkPrograms.loader,
    },
    deleteBookmarkPrograms: {
      data: deleteBookmarkPrograms.data,
      loader: deleteBookmarkPrograms.loader,
    },
    bookmarkedItems: bookmarkedPrograms.data.map(
      ({ online_program }) => +online_program,
    ),
    addBookmark: postBookmark,
    removeBookmark: deleteBookmark,
  };
};
