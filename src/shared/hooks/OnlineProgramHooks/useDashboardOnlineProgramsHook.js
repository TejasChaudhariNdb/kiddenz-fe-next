/* eslint-disable react-hooks/rules-of-hooks */
import { useState, useEffect, useMemo } from 'react';
// import {
//   // ON_UNMOUNT,
//   ON_SUCCESS,
// } from '../../utils/commonReduxSagaConverter/commonConstants';

export const useDashboardOnlineProgramsHook = ({
  getData,
  DASHBOARD_ONLINE_PROGRAM_LIST_API_CALL,
  dashboard: { DASHBOARD_ONLINE_PROGRAM_LIST_API } = {},
}) => {
  const [programType, setProgramType] = useState('small_batch');

  useEffect(() => {
    DASHBOARD_ONLINE_PROGRAM_LIST_API_CALL({
      // query: {
      //   program_type: 'small_batch',
      // },
    });
  }, []);

  const onSelectProgramType = type => {
    let query = {};
    if (type !== programType) {
      query = {
        program_type: type,
      };
      setProgramType(type);
      DASHBOARD_ONLINE_PROGRAM_LIST_API_CALL({
        query,
      });
    }
  };

  const onlinePrograms = useMemo(
    () => getData(DASHBOARD_ONLINE_PROGRAM_LIST_API, {}),
    [DASHBOARD_ONLINE_PROGRAM_LIST_API],
  );

  return {
    onlinePrograms: {
      data: onlinePrograms.data,
      loader: onlinePrograms.loader,
      onSelect: onSelectProgramType,
      selected: programType,
    },
  };
};
