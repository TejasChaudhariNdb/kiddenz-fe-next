/* eslint-disable eqeqeq */
import { useEffect, useState, useMemo, useCallback } from 'react';
import Router from 'next/router';
import debounce from 'lodash.debounce';
// import { ON_UNMOUNT } from 'shared/utils/commonReduxSagaConverter/commonConstants';
// import { removeNullValuesFromObject } from '../../utils/commonHelpers/miscHelpers';

const PROGRAM_LIMIT = 9;
const DEFAULT_MAX_PRICE = 500000;
const AGE_LIMIT = 15;

export const useOnlineProgramSearchHook = (
  {
    ONLINE_PROGRAM_SEARCH_API_CALL,
    ONLINE_PROGRAM_SEARCH_API_CANCEL,
    dashboard: { ONLINE_PROGRAM_SEARCH_API, ONLINE_PROGRAM_MAX_PRICE_API },
    getData,
  },
  {
    search = '',
    city = '',
    programs = '',
    isCertificate = '',
    isMoneyback = '',
    school = '',
    categories = [],
    courses = [],
    startPrice = 0,
    endPrice = DEFAULT_MAX_PRICE,
    startAge = 0,
    endAge = AGE_LIMIT * 12,
    isTrial = '',
    isDiscount = '',
  },
) => {
  const [priceRange, setPriceRange] = useState({
    min: startPrice,
    max: endPrice,
  });

  const programMaxPrice = useMemo(
    () => {
      const price = getData(ONLINE_PROGRAM_MAX_PRICE_API, {}, false);
      if (price.data && price.data.full_course_cost__max) {
        setPriceRange({
          min: 0,
          max: price.data.full_course_cost__max || DEFAULT_MAX_PRICE,
        });
      }
      return price;
    },
    [ONLINE_PROGRAM_MAX_PRICE_API],
  );

  let endYear = 0;
  let isAge = false;
  let isPrice = false;
  const dynamicPrice =
    programMaxPrice &&
    programMaxPrice.data &&
    programMaxPrice.data.full_course_cost__max
      ? programMaxPrice.data.full_course_cost__max
      : DEFAULT_MAX_PRICE;

  if (
    (startPrice && +startPrice > 0) ||
    (endPrice && +endPrice < dynamicPrice && +dynamicPrice)
  ) {
    isPrice = true;
  }

  if (endAge == 0 || (endAge && +endAge)) {
    endYear = Math.trunc(endAge / 12);
    if (endAge != AGE_LIMIT * 12) {
      isAge = true;
    }
  } else {
    endYear = AGE_LIMIT * 12;
  }
  let endMonth = 0;
  if (endAge == 0 || (endAge && +endAge)) {
    endMonth = endAge % 12;
    if (endMonth) {
      isAge = true;
    }
  } else {
    endMonth = 0;
  }
  const [searchKey, setSearch] = useState(search);
  const [programType, setProgramType] = useState(programs || 'all');
  const [certificate, setCertificate] = useState(isCertificate);
  const [moneyBack, setMoneyBack] = useState(isMoneyback);
  const [trialClass, setTrialClass] = useState(isTrial);
  const [discount, setIsDiscount] = useState(isDiscount);
  const [schoolType, setSchoolType] = useState(school);
  const [selectedCity, setSelectedCity] = useState(city);
  const [isCitySelected, setIsCitySelected] = useState(false);
  const [showLoadMore, setShowLoadMore] = useState(true);
  const [showLoader, setShowLoader] = useState(false);
  const [isCostChanged, setIsCostChanged] = useState(isPrice);
  const [isAgeChanged, setIsAgeChanged] = useState(isAge);
  const [isFilterApplied, setIsFilterApplied] = useState(false);
  const [category, setCategoryList] = useState(
    typeof categories === 'string'
      ? [+categories]
      : categories.map(cat => +cat),
  );
  const [courseType, setCourseType] = useState(
    typeof courses === 'string' ? [courses] : courses,
  );
  const [ageYear, setAgeYear] = useState({
    id: endYear,
    key: endYear,
    name: endYear !== 15 ? `${endYear} Years` : 'Any',
  });
  const [startAgeYear, setStartAgeYear] = useState(startAge);

  const startAgeQuery = (startAgeYear && +startAgeYear * 12) || 0;
  const endAgeQuery =
    (ageYear && ageYear.key && +ageYear.key * 12) || AGE_LIMIT * 12;

  useEffect(
    () => () => {
      ONLINE_PROGRAM_SEARCH_API_CANCEL();
    },
    [],
  );

  useEffect(
    () => {
      const storedCity = window.localStorage.getItem('selectedCity');
      if (storedCity) {
        setIsCitySelected(window.localStorage.getItem('selectedCity') || '');
        if (storedCity !== selectedCity) {
          onSelectCity(storedCity);
        }
      }
    },
    [selectedCity],
  );

  const getProgramList = ({ query }) => {
    ONLINE_PROGRAM_SEARCH_API_CALL({
      query,
      successCallback: () => {
        setShowLoader(false);
      },
      errorCallback: () => {
        setShowLoader(false);
      },
    });
  };

  const handleLoadMore = skip => {
    setShowLoadMore(true);
    ONLINE_PROGRAM_SEARCH_API_CALL({
      query: {
        limit: PROGRAM_LIMIT,
        skip,
        search: searchKey,
        city: selectedCity,
        category,
        online_tag:
          programType !== 'all' && programType !== '' ? programType : '',
        course_type: courseType,
        certificate,
        discount,
        money_back_gurantee: moneyBack,
        school_type: schoolType,
        is_trial_class: trialClass,
        start_price: priceRange.min,
        end_price: priceRange.max,
        start_age: startAgeQuery,
        end_age: endAgeQuery,
      },
      // subKey: ['data'],
      // isInfinite: true,
      isUpdate: true,
      updateCallback: (a = {}, b = {}) => {
        if (!(b && b.length)) {
          setShowLoadMore(false);
        }
        return [
          ...a,
          ...b,
          // items: (a.items || []).concat(b.items),
        ];
      },
    });
  };

  const closeHandler = () => {
    setIsCitySelected(false);
  };

  const toggleLocationModal = () => {
    setIsCitySelected(!isCitySelected);
  };

  const debouncedSave = useCallback(
    debounce(nextValue => {
      // searchProgram(nextValue);
      const query = {
        limit: PROGRAM_LIMIT,
        skip: 0,
        search: nextValue,
        city: selectedCity,
        category,
        online_tag:
          programType !== 'all' && programType !== '' ? programType : '',
        course_type: courseType,
        certificate,
        discount,
        money_back_gurantee: moneyBack,
        school_type: schoolType,
        is_trial_class: trialClass,
        start_price: priceRange.min,
        end_price: priceRange.max,
        start_age: startAgeQuery,
        end_age: endAgeQuery,
      };
      getProgramList({ query });
      Router.replace({
        pathname: '/online-program/search',
        query: {
          limit: PROGRAM_LIMIT,
          skip: 0,
          search: nextValue,
          city: selectedCity,
          categories: category,
          programs:
            programType !== 'all' && programType !== '' ? programType : '',
          isCertificate: certificate,
          isMoneyback: moneyBack,
          isDiscount: discount,
          isTrial: trialClass,
          school: schoolType,
          courses: courseType,
          startPrice: priceRange.min,
          endPrice: priceRange.max,
          startAge: startAgeQuery,
          endAge: endAgeQuery,
        },
      });
    }, 500),
    [
      selectedCity,
      category,
      programType,
      certificate,
      moneyBack,
      discount,
      trialClass,
      schoolType,
      courseType,
      priceRange,
      startAgeQuery,
      endAgeQuery,
    ],
  );

  const onSearchProgram = e => {
    e.preventDefault();
    const value = getPlatformBasedFieldValue(e);
    setSearch(value);
    setShowLoadMore(true);
    setShowLoader(true);
    debouncedSave(value);
  };

  const onSelectProgramType = type => {
    let programQuery = '';
    if (programType !== type) {
      setProgramType(type);
      programQuery = type;
    } else {
      setProgramType('');
    }
    setShowLoadMore(true);
    setShowLoader(true);
    const query = {
      limit: PROGRAM_LIMIT,
      skip: 0,
      search: searchKey,
      city: selectedCity,
      category,
      online_tag: programQuery,
      course_type: courseType,
      certificate,
      discount,
      money_back_gurantee: moneyBack,
      school_type: schoolType,
      is_trial_class: trialClass,
      start_price: priceRange.min,
      end_price: priceRange.max,
      start_age: startAgeQuery,
      end_age: endAgeQuery,
    };
    getProgramList({ query });
    Router.replace({
      pathname: '/online-program/search',
      query: {
        search,
        city,
        programs: programQuery,
        isCertificate,
        isMoneyback,
        school,
        categories,
        courses,
        startPrice,
        endPrice,
        startAge: startAgeQuery,
        endAge: endAgeQuery,
        isTrial,
        isDiscount,
        skip: 0,
        limit: PROGRAM_LIMIT,
      },
    });
  };

  const onSelectSchoolType = type => {
    if (schoolType !== type) {
      setSchoolType(type);
    } else {
      setSchoolType('');
    }
  };

  const onSelectCity = type => {
    let cityQuery = '';
    if (selectedCity !== type) {
      if (typeof window !== 'undefined') {
        localStorage.setItem('selectedCity', type);
      }
      setSelectedCity(type);
      cityQuery = type;
      setIsCitySelected(true);
    } else {
      setSelectedCity('');
    }
    setIsCitySelected(true);
    setShowLoadMore(true);
    setShowLoader(true);
    const query = {
      limit: PROGRAM_LIMIT,
      skip: 0,
      search: searchKey,
      city: cityQuery,
      category,
      online_tag:
        programType !== 'all' && programType !== '' ? programType : '',
      course_type: courseType,
      certificate,
      discount,
      money_back_gurantee: moneyBack,
      school_type: schoolType,
      is_trial_class: trialClass,
      start_price: priceRange.min,
      end_price: priceRange.max,
      start_age: startAgeQuery,
      end_age: endAgeQuery,
    };
    getProgramList({ query });
    Router.replace({
      pathname: '/online-program/search',
      query: {
        search,
        city: cityQuery,
        programs,
        isCertificate,
        isMoneyback,
        school,
        categories,
        courses,
        startPrice,
        endPrice,
        startAge: startAgeQuery,
        endAge: endAgeQuery,
        isTrial,
        isDiscount,
        skip: 0,
        limit: PROGRAM_LIMIT,
      },
    });
  };

  const onSelectCategory = type => {
    const categoryCopy = [...category];
    if (categoryCopy.includes(type)) {
      categoryCopy.map((cat, i) => {
        if (categoryCopy[i] === type) {
          categoryCopy.splice(i, 1);
        }
        return null;
      });
    } else {
      categoryCopy.push(type);
    }
    setCategoryList(categoryCopy);
  };

  const handlePriceRange = val => {
    setPriceRange(val);
    setIsCostChanged(true);
  };

  const handlePriceComplete = val => {
    setPriceRange(val);
    setIsCostChanged(true);
  };

  const handleAgeYearChange = value => {
    setAgeYear(value);
    setStartAgeYear(value.key);
    setIsAgeChanged(true);
  };

  const onSelectCourseType = type => {
    const courseTypeCopy = [...courseType];
    if (courseTypeCopy.includes(type)) {
      courseTypeCopy.map((cat, i) => {
        if (courseTypeCopy[i] === type) {
          courseTypeCopy.splice(i, 1);
        }
        return null;
      });
    } else {
      courseTypeCopy.push(type);
    }
    setCourseType(courseTypeCopy);
  };

  const onSelectCertificate = type => {
    if (certificate !== type) {
      setCertificate(type);
    } else {
      setCertificate('');
    }
  };

  const onSelectMoneyBack = type => {
    if (moneyBack !== type) {
      setMoneyBack(type);
    } else {
      setMoneyBack('');
    }
  };

  const onSelectTrialClass = type => {
    if (trialClass !== type) {
      setTrialClass(type);
    } else {
      setTrialClass('');
    }
  };

  const onSelectDiscount = type => {
    if (discount !== type) {
      setIsDiscount(type);
    } else {
      setIsDiscount('');
    }
  };

  const handleClearPriceRange = () => {
    setIsCostChanged(false);
  };

  const handleClearAge = () => {
    setAgeYear({
      id: AGE_LIMIT,
      key: AGE_LIMIT,
      name: `Any`,
    });
    setStartAgeYear(0);
    setShowLoadMore(true);
    setIsAgeChanged(false);
  };

  const handleApplyFilter = () => {
    setShowLoadMore(true);
    setShowLoader(true);
    setIsFilterApplied(true);
    const query = {
      limit: PROGRAM_LIMIT,
      skip: 0,
      search: searchKey,
      city: selectedCity,
      category,
      online_tag:
        programType !== 'all' && programType !== '' ? programType : '',
      course_type: courseType,
      certificate,
      discount,
      money_back_gurantee: moneyBack,
      school_type: schoolType,
      is_trial_class: trialClass,
      start_price: priceRange.min,
      end_price: priceRange.max,
      start_age: startAgeQuery,
      end_age: endAgeQuery,
    };
    getProgramList({ query });
    Router.replace({
      pathname: '/online-program/search',
      query: {
        search,
        city,
        programs,
        school: schoolType,
        categories: category,
        startPrice: priceRange.min,
        endPrice: priceRange.max,
        startAge: startAgeQuery,
        endAge: endAgeQuery,
        // startAge: (+ageYear.key * 12 || 0) + ageMonth.key,
        courses: courseType,
        isCertificate: certificate,
        isMoneyback: moneyBack,
        isTrial: trialClass,
        isDiscount: discount,
        skip: 0,
        limit: PROGRAM_LIMIT,
      },
    });
  };

  const handleClearIndividualFilter = (key, itemToRemove = null) => {
    let startAgeValue = startAgeQuery;
    let endAgeValue = endAgeQuery;
    let priceValue = priceRange;
    let trialValue = trialClass;
    let certificateValue = certificate;
    let moneyBackValue = moneyBack;
    let dicountValue = discount;
    let schoolValue = schoolType;
    let categoriesValue = category;
    let coursesValue = courseType;
    if (key === 'age') {
      startAgeValue = 0;
      endAgeValue = AGE_LIMIT * 12;
      setAgeYear({
        id: AGE_LIMIT,
        key: AGE_LIMIT,
        name: `Any`,
      });
      setStartAgeYear(0);
      setIsAgeChanged(false);
    }
    if (key === 'price') {
      priceValue = {
        min: 0,
        max:
          (programMaxPrice.data &&
            programMaxPrice.data.full_course_cost__max) ||
          DEFAULT_MAX_PRICE,
      };
      handlePriceComplete(priceValue);
      setPriceRange(priceValue);
      setIsCostChanged(false);
    }
    if (key === 'trial') {
      trialValue = '';
      setTrialClass('');
    }
    if (key === 'certificate') {
      certificateValue = '';
      setCertificate('');
    }
    if (key === 'moneyBack') {
      moneyBackValue = '';
      setMoneyBack('');
    }
    if (key === 'dicount') {
      dicountValue = '';
      setIsDiscount('');
    }
    if (key === 'schoolType') {
      schoolValue = '';
      setSchoolType('');
    }
    if (key === 'category' && itemToRemove) {
      const categoryCopy = [...category];
      if (categoryCopy.includes(itemToRemove)) {
        categoryCopy.map((cat, i) => {
          if (categoryCopy[i] === itemToRemove) {
            categoryCopy.splice(i, 1);
          }
          return null;
        });
      }
      categoriesValue = categoryCopy;
      setCategoryList(categoryCopy);
    }
    if (key === 'courses' && itemToRemove) {
      const courseTypeCopy = [...courseType];
      if (courseTypeCopy.includes(itemToRemove)) {
        courseTypeCopy.map((cat, i) => {
          if (courseTypeCopy[i] === itemToRemove) {
            courseTypeCopy.splice(i, 1);
          }
          return null;
        });
      }
      coursesValue = courseTypeCopy;
      setCourseType(courseTypeCopy);
    }
    setShowLoadMore(true);
    setShowLoader(true);

    const query = {
      limit: PROGRAM_LIMIT,
      skip: 0,
      city: selectedCity,
      search: searchKey,
      online_tag: '',
      category: categoriesValue,
      course_type: coursesValue,
      certificate: certificateValue,
      money_back_gurantee: moneyBackValue,
      school_type: schoolValue,
      start_price: priceValue.min,
      end_price: priceValue.max,
      discount: dicountValue,
      is_trial_class: trialValue,
      start_age: startAgeValue,
      end_age: endAgeValue,
    };
    getProgramList({ query });

    Router.replace({
      pathname: '/online-program/search',
      query: {
        search: searchKey,
        city: selectedCity,
        programs: 'all',
        isCertificate: certificateValue,
        isMoneyback: moneyBackValue,
        school: schoolValue,
        categories: categoriesValue,
        courses: coursesValue,
        startPrice: priceValue.min,
        endPrice: priceValue.max,
        startAge: startAgeValue,
        endAge: endAgeValue,
        isTrial: trialValue,
        isDiscount: dicountValue,
        skip: 0,
        limit: PROGRAM_LIMIT,
      },
    });
  };

  const handleClearFilter = () => {
    setSearch('');
    setProgramType('all');
    setCertificate('');
    setMoneyBack('');
    setTrialClass('');
    setIsDiscount('');
    setSchoolType('');
    setCategoryList([]);
    setCourseType([]);
    setPriceRange({
      min: 0,
      max: DEFAULT_MAX_PRICE,
    });
    setAgeYear({
      id: AGE_LIMIT,
      key: AGE_LIMIT,
      name: `Any`,
    });
    setStartAgeYear(0);
    setShowLoadMore(true);
    setShowLoader(true);
    setIsCostChanged(false);
    setIsAgeChanged(false);
    setIsFilterApplied(false);
    const query = {
      limit: PROGRAM_LIMIT,
      skip: 0,
      search: '',
      city,
      category: [],
      online_tag: '',
      course_type: [],
      certificate: '',
      money_back_gurantee: '',
      school_type: '',
      start_price: 0,
      end_price:
        (programMaxPrice.data && programMaxPrice.data.full_course_cost__max) ||
        DEFAULT_MAX_PRICE,
      discount: '',
      is_trial_class: '',
      start_age: 0,
      end_age: AGE_LIMIT * 12,
    };
    getProgramList({ query });

    Router.replace({
      pathname: '/online-program/search',
      query: {
        search: '',
        city,
        programs: 'all',
        isCertificate: '',
        isMoneyback: '',
        school: '',
        categories: [],
        courses: [],
        startPrice: 0,
        endPrice:
          (programMaxPrice.data &&
            programMaxPrice.data.full_course_cost__max) ||
          DEFAULT_MAX_PRICE,
        startAge: 0,
        endAge: AGE_LIMIT * 12,
        isTrial: '',
        isDiscount: '',
        skip: 0,
        limit: PROGRAM_LIMIT,
      },
    });
  };

  // console.log(category, 'categorycategory');
  // console.log(categories, 'categoriescategories');
  // console.log(courseType, 'courseTypecourseType');
  // console.log(selectedCity, 'selectedCityselectedCity');
  // console.log(programType, 'programTypeprogramType');
  // console.log(courseType, 'courseTypecourseType');
  // console.log(moneyBack, 'moneyBackmoneyBack');
  // console.log(certificate, 'certificatecertificate');
  // console.log(discount, 'discountdiscount');
  // console.log(moneyBack, 'moneyBackmoneyBack');
  // console.log(schoolType, 'schoolTypeschoolType');
  // console.log(trialClass, 'trialClasstrialClass');
  // console.log(priceRange, 'priceRangepriceRange');
  // console.log(startAge, 'startAgestartAge');
  // console.log(endAge, 'endAgeendAge');

  // <****************DATA RETURN********************>

  const searchResults = useMemo(
    () => getData(ONLINE_PROGRAM_SEARCH_API, [], false),
    [ONLINE_PROGRAM_SEARCH_API],
  );

  return {
    searchResults: {
      data: searchResults.data,
      loader: searchResults.loader,
    },
    showLoadMore,
    showLoader,
    category: {
      value: category,
      onSelect: onSelectCategory,
    },
    selectedCity: {
      value: selectedCity,
      onSelect: onSelectCity,
    },
    isCitySelected,
    toggleLocationModal,
    closeHandler,
    searchKey: {
      value: searchKey,
      onSearch: onSearchProgram,
    },
    programType: {
      value: programType,
      onSelect: onSelectProgramType,
    },
    courseType: {
      value: courseType,
      onSelect: onSelectCourseType,
    },
    schoolType: {
      value: schoolType,
      onSelect: onSelectSchoolType,
    },
    certificate: {
      value: certificate,
      onSelect: onSelectCertificate,
    },
    moneyBack: {
      value: moneyBack,
      onSelect: onSelectMoneyBack,
    },
    trialClass: {
      value: trialClass,
      onSelect: onSelectTrialClass,
    },
    discount: {
      value: discount,
      onSelect: onSelectDiscount,
    },
    priceRange: {
      maxPrice:
        (programMaxPrice.data &&
          programMaxPrice.data.full_course_cost__max &&
          Math.ceil(programMaxPrice.data.full_course_cost__max / 100) * 100) ||
        DEFAULT_MAX_PRICE,
      loader: programMaxPrice.loader,
      range: priceRange,
      // range: {
      //   min: Math.ceil(priceRange.min / 100) * 100,
      //   max:
      //     priceRange.max > 100
      //       ? Math.ceil(priceRange.max / 100) * 100
      //       : priceRange.max,
      // },
      onChangePrice: handlePriceRange,
      onSliderComplete: handlePriceComplete,
      onClearPriceRange: handleClearPriceRange,
    },
    onClearAge: handleClearAge,
    ageYear: {
      value: ageYear,
      onChange: handleAgeYearChange,
    },
    onSearchLoadMore: handleLoadMore,
    onClearFilter: handleClearFilter,
    onClearIndividualFilter: handleClearIndividualFilter,
    onApplyFilter: handleApplyFilter,
    isCostChanged,
    isAgeChanged,
    isFilterApplied,
  };
};

// Helpers
function getPlatformBasedFieldValue(e) {
  return typeof e === 'object' ? e.target.value : e;
}
