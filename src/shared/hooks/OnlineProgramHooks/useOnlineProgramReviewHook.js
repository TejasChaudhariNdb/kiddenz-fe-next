/* eslint-disable react-hooks/rules-of-hooks */
import { useState, useEffect, useMemo, useCallback } from 'react';
// import {
//   // ON_UNMOUNT,
//   ON_SUCCESS,
// } from '../../utils/commonReduxSagaConverter/commonConstants';

export const useOnlineProgramReviewHook = (
  {
    getData,
    ONLINE_PROGRAM_REVIEW_API_CALL,
    GET_ONLINE_PROGRAMS_REVIEWS_API_CALL,
    ONLINE_PROGRAM_DETAIL_API_CALL,
    dashboard: {
      ONLINE_PROGRAM_REVIEW_API,
      GET_ONLINE_PROGRAMS_REVIEWS_API,
    } = {},
  },
  { onReviewSuccess = () => {}, onReviewError = () => {}, programId = null },
) => {
  const [rating, setRating] = useState(0);
  const [ratingError, setRatingError] = useState(null);
  const [description, setDescription] = useState(null);
  const [descriptionError, setDescriptionError] = useState(null);

  useEffect(
    () => {
      if (programId) {
        GET_ONLINE_PROGRAMS_REVIEWS_API_CALL({
          params: {
            id: programId,
          },
        });
      }
    },
    [programId],
  );

  const onChangeRating = value => {
    setRatingError('');
    setRating(value);
  };

  const onBlurRating = useCallback(
    e => {
      e.preventDefault();
      const error = validate(rating, 'rating');
      if (error) setRatingError(error);
    },
    [rating, ratingError],
  );

  const onChangeDescription = e => {
    const value = getPlatformBasedFieldValue(e);

    setDescriptionError('');
    setDescription(value);
  };

  const onBlurDescription = useCallback(
    e => {
      e.preventDefault();
      const error = validate(description, 'description');
      if (error) setDescriptionError(error);
    },
    [description, descriptionError],
  );

  const clearForm = () => {
    setRating(0);
    setRatingError(null);
    setDescription(null);
    setDescriptionError(null);
  };

  const handleSubmit = () => {
    let payload = {};
    const errorList = [];
    const isRatingError = validate(rating, 'rating');
    const isDescriptionError = validate(description, 'description');
    if (isRatingError) {
      setRatingError(isRatingError);
      errorList.push(null);
    }
    if (isDescriptionError) {
      setDescriptionError(isDescriptionError);
      errorList.push(null);
    }

    if (!errorList.length) {
      payload = {
        online_program: programId,
        rating,
        review: description,
      };
      ONLINE_PROGRAM_REVIEW_API_CALL({
        payload,
        successCallback: ({ data }) => {
          onReviewSuccess({ data });
          GET_ONLINE_PROGRAMS_REVIEWS_API_CALL({
            params: {
              id: programId,
            },
          });
          ONLINE_PROGRAM_DETAIL_API_CALL({
            params: {
              id: programId,
            },
          });
          clearForm();
        },
        errorCallback: ({ error }) => {
          onReviewError({ error });
        },
      });
    }
  };

  const postReview = useMemo(
    () => getData(ONLINE_PROGRAM_REVIEW_API, {}, false),
    [ONLINE_PROGRAM_REVIEW_API],
  );

  const reviewsList = useMemo(
    () => getData(GET_ONLINE_PROGRAMS_REVIEWS_API, [], false),
    [GET_ONLINE_PROGRAMS_REVIEWS_API],
  );

  return {
    rating: {
      onChange: onChangeRating,
      onBlur: onBlurRating,
      error: ratingError,
      value: rating,
    },
    description: {
      onChange: onChangeDescription,
      onBlur: onBlurDescription,
      error: descriptionError,
      value: description,
    },
    postReview: {
      data: postReview.data,
      loader: postReview.loader,
    },
    reviewsList: {
      data: reviewsList.data,
      loader: reviewsList.loader,
    },
    onReview: handleSubmit,
  };
};

// Helpers
function getPlatformBasedFieldValue(e) {
  return typeof e === 'object' ? e.target.value : e;
}

function validate(value, fieldName) {
  switch (fieldName) {
    case 'rating': {
      if (!value) return 'Kindly provide your overall rating';
      return '';
    }
    case 'description': {
      if (!value) return 'Kindly enter your review';
      return '';
    }
    default:
      if (!value) return 'This field is required';
      return '';
  }
}
