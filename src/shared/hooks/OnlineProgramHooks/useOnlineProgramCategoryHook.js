/* eslint-disable react-hooks/rules-of-hooks */
import { useState, useEffect, useMemo, useCallback } from 'react';
// import {
//   // ON_UNMOUNT,
//   ON_SUCCESS,
// } from '../../utils/commonReduxSagaConverter/commonConstants';

export const useOnlineProgramCategoryHook = ({
  getData,
  ONLINE_CATEGORY_LIST_API_CALL,
  // ONLINE_PROGRAM_MAX_PRICE_API_CALL,
  dashboard: { ONLINE_CATEGORY_LIST_API } = {},
}) => {
  const [searchKey, setSearchKey] = useState('');

  useEffect(() => {}, []);

  useEffect(
    () => {
      let query = {};
      if (searchKey.length) {
        query = {
          search: searchKey,
        };
      }
      ONLINE_CATEGORY_LIST_API_CALL({
        query,
      });
    },
    [searchKey],
  );

  // useEffect(
  //   () => {
  //     let query = [];
  //     if (searchKey.length) {
  //       query = {
  //         search: searchKey,
  //       };
  //     }
  //     ONLINE_CATEGORY_LIST_API_CALL({
  //       query,
  //     });
  //   },
  //   [searchKey],
  // );

  const onSearchCategory = useCallback(
    e => {
      e.preventDefault();
      const value = getPlatformBasedFieldValue(e);
      setSearchKey(typeof value === 'string' ? value : '');
    },
    [searchKey],
  );

  const categoryList = useMemo(() => getData(ONLINE_CATEGORY_LIST_API, []), [
    ONLINE_CATEGORY_LIST_API,
  ]);

  const handleClearFilter = () => {
    setSearchKey('');
  };

  // const programMaxPrice = useMemo(
  //   () => getData(ONLINE_PROGRAM_MAX_PRICE_API, []),
  //   [ONLINE_PROGRAM_MAX_PRICE_API],
  // );

  return {
    category: {
      searchKey,
      onSearch: onSearchCategory,
    },
    categoryList: {
      data: categoryList.data,
      loader: categoryList.loader,
    },
    onClearCategorySearch: handleClearFilter,
  };
};

function getPlatformBasedFieldValue(e) {
  return typeof e === 'object' ? e.target.value : e;
}
