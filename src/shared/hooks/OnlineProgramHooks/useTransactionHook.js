/* eslint-disable react-hooks/rules-of-hooks */
import { useEffect, useMemo } from 'react';

export const useTransactionHook = (
  {
    getData,
    GET_TRANSACTION_LIST_API_CALL,
    DOWNLOAD_INVOICE_API_CALL,
    dashboard: { GET_TRANSACTION_LIST_API, DOWNLOAD_INVOICE_API } = {},
  },
  { onDownloadSuccess = () => {}, onDownloadError = () => {} },
) => {
  useEffect(() => {}, []);

  const transactionList = useMemo(() => getData(GET_TRANSACTION_LIST_API, []), [
    GET_TRANSACTION_LIST_API,
  ]);

  const downloadInvoice = useMemo(() => getData(DOWNLOAD_INVOICE_API, {}), [
    DOWNLOAD_INVOICE_API,
  ]);

  const onDownloadInvoice = (id, transactionId) => {
    DOWNLOAD_INVOICE_API_CALL({
      payload: {
        online_order_id: id,
      },
      successCallback: ({ data }) => {
        onDownloadSuccess({ data, transactionId });
      },
      errorCallback: ({ error }) => {
        onDownloadError({ error });
      },
    });
  };

  const getTransactionList = () => {
    GET_TRANSACTION_LIST_API_CALL();
  };

  return {
    transactionList: {
      data: transactionList.data,
      loader: transactionList.loader,
    },
    downloadInvoice: {
      data: downloadInvoice.data,
      loader: downloadInvoice.loader,
    },
    getTransactionList,
    onDownloadInvoice,
  };
};
