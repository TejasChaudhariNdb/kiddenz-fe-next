/* eslint-disable react-hooks/rules-of-hooks */
import { useState, useEffect, useMemo, useCallback } from 'react';
// import {
//   // ON_UNMOUNT,
//   ON_SUCCESS,
// } from '../../utils/commonReduxSagaConverter/commonConstants';

export const useOnlineProgramCityHook = ({
  getData,
  ONLINE_PROGRAM_CITIES_API_CALL,
  // ONLINE_PROGRAM_MAX_PRICE_API_CALL,
  dashboard: { ONLINE_PROGRAM_CITIES_API } = {},
}) => {
  const [searchKey, setSearchKey] = useState('');

  useEffect(() => {}, []);

  useEffect(
    () => {
      let query = [];
      if (searchKey.length) {
        query = {
          search: searchKey,
        };
      }
      ONLINE_PROGRAM_CITIES_API_CALL({
        query,
      });
    },
    [searchKey],
  );

  useEffect(
    () => {
      let query = [];
      if (searchKey.length) {
        query = {
          search: searchKey,
        };
      }
      ONLINE_PROGRAM_CITIES_API_CALL({
        query,
      });
    },
    [searchKey],
  );

  const onSearchCity = useCallback(
    e => {
      e.preventDefault();
      const value = getPlatformBasedFieldValue(e);
      setSearchKey(value);
    },
    [searchKey],
  );

  const cityList = useMemo(() => getData(ONLINE_PROGRAM_CITIES_API, []), [
    ONLINE_PROGRAM_CITIES_API,
  ]);

  // const programMaxPrice = useMemo(
  //   () => getData(ONLINE_PROGRAM_MAX_PRICE_API, []),
  //   [ONLINE_PROGRAM_MAX_PRICE_API],
  // );

  return {
    cities: {
      searchKey,
      onSearch: onSearchCity,
    },
    cityList: {
      data: cityList.data,
      loader: cityList.loader,
    },
  };
};

function getPlatformBasedFieldValue(e) {
  return typeof e === 'object' ? e.target.value : e;
}
