/* eslint-disable react-hooks/rules-of-hooks */
import { useEffect, useMemo } from 'react';
// import {
//   // ON_UNMOUNT,
//   ON_SUCCESS,
// } from '../../utils/commonReduxSagaConverter/commonConstants';

export const usePurchasedOnlineProgramHook = ({
  getData,
  ONLINE_PROGRAM_PURCHASED_API_CALL,
  ONLINE_PROGRAM_PURCHASED_PROGRAMS_API_CALL,
  // ONLINE_PROGRAM_MAX_PRICE_API_CALL,
  dashboard: {
    ONLINE_PROGRAM_PURCHASED_API,
    ONLINE_PROGRAM_PURCHASED_PROGRAMS_API,
  } = {},
}) => {
  useEffect(() => {
    ONLINE_PROGRAM_PURCHASED_API_CALL();
  }, []);

  const purchasedPrograms = useMemo(
    () => getData(ONLINE_PROGRAM_PURCHASED_API, []),
    [ONLINE_PROGRAM_PURCHASED_API],
  );

  const purchasedOnlinePrograms = useMemo(
    () => getData(ONLINE_PROGRAM_PURCHASED_PROGRAMS_API, []),
    [ONLINE_PROGRAM_PURCHASED_PROGRAMS_API],
  );

  const getPurchasedPrograms = () => {
    ONLINE_PROGRAM_PURCHASED_API_CALL();
  };

  const getPurchasedOnlinePrograms = () => {
    ONLINE_PROGRAM_PURCHASED_PROGRAMS_API_CALL();
  };

  // const programMaxPrice = useMemo(
  //   () => getData(ONLINE_PROGRAM_MAX_PRICE_API, []),
  //   [ONLINE_PROGRAM_MAX_PRICE_API],
  // );

  return {
    purchasedPrograms: {
      data: purchasedPrograms.data,
      loader: purchasedPrograms.loader,
    },
    purchasedOnlinePrograms: {
      data: purchasedOnlinePrograms.data,
      loader: purchasedOnlinePrograms.loader,
    },
    getPurchasedPrograms,
    getPurchasedOnlinePrograms,
  };
};
