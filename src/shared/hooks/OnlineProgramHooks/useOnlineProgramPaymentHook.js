/* eslint-disable react-hooks/rules-of-hooks */
import { useState, useEffect, useMemo, useCallback } from 'react';
// import {
//   // ON_UNMOUNT,
//   ON_SUCCESS,
// } from '../../utils/commonReduxSagaConverter/commonConstants';

export const useOnlineProgramPaymentHook = (
  {
    getData,
    CREATE_ONLINE_PROGRAM_ORDER_API_CALL,
    dashboard: {
      CREATE_ONLINE_PROGRAM_ORDER_API,
      ONLINE_PROGRAM_PAYMENT_STATUS_API,
    } = {},
  },
  { onCreateOrderSuccess = () => {}, onCreateOrderError = () => {} },
) => {
  const [noOfSessions, setNoOfSessions] = useState(null);
  const [noOfSessionsError, setNoOfSessionsError] = useState(null);
  useEffect(() => {}, []);

  const onChangeNoOfSession = e => {
    const value = getPlatformBasedFieldValue(e);

    if (Number(value) === 0 || Number(value)) {
      setNoOfSessionsError('');
      setNoOfSessions(value);
    }
  };

  const onBlurNoOfSession = useCallback(
    e => {
      e.preventDefault();
      const error = validate(noOfSessions, 'noOfSessions');
      if (error) setNoOfSessionsError(error);
    },
    [noOfSessions, noOfSessionsError],
  );

  const handleBuyNow = (courseType = '', selectedBatch = {}) => {
    let payload = {};
    const errorList = [];
    if (courseType === 'Trial_Class') {
      payload = {
        currency: 'INR',
        online_batch: selectedBatch.id,
      };
    } else if (courseType === 'Single_Session') {
      const isError = validate(noOfSessions, 'noOfSessions');
      if (isError) {
        setNoOfSessionsError(isError);
        errorList.push(null);
      }
      payload = {
        no_of_classes: noOfSessions,
        currency: 'INR',
        online_batch: selectedBatch.id,
      };
    } else {
      payload = {
        currency: 'INR',
        online_batch: selectedBatch.id,
      };
    }
    if (!errorList.length && selectedBatch && selectedBatch.id)
      CREATE_ONLINE_PROGRAM_ORDER_API_CALL({
        payload,
        successCallback: ({ data }) => {
          onCreateOrderSuccess({ data });
        },
        errorCallback: ({ error }) => {
          onCreateOrderError({ error });
        },
      });
  };

  const createProgram = useMemo(
    () => getData(CREATE_ONLINE_PROGRAM_ORDER_API, {}, false),
    [CREATE_ONLINE_PROGRAM_ORDER_API],
  );

  const paymentStatus = useMemo(
    () => getData(ONLINE_PROGRAM_PAYMENT_STATUS_API, {}, false),
    [ONLINE_PROGRAM_PAYMENT_STATUS_API],
  );

  return {
    noOfSessions: {
      onChange: onChangeNoOfSession,
      onBlur: onBlurNoOfSession,
      error: noOfSessionsError,
      value: noOfSessions,
    },
    createProgram: {
      data: createProgram.data,
      loader: createProgram.loader,
    },
    paymentStatus: {
      data: paymentStatus.data,
      loader: paymentStatus.loader,
    },
    onBuyNow: handleBuyNow,
  };
};

// Helpers
function getPlatformBasedFieldValue(e) {
  return typeof e === 'object' ? e.target.value : e;
}

function validate(value, fieldName) {
  switch (fieldName) {
    case 'noOfSessions': {
      if (!value || +value < 1) return 'Please enter no of session';
      return '';
    }
    default:
      if (!value) return 'This field is required';
      return '';
  }
}
