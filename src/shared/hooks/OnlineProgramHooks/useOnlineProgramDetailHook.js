/* eslint-disable react-hooks/rules-of-hooks */
import { useEffect, useMemo } from 'react';
// import {
//   // ON_UNMOUNT,
//   ON_SUCCESS,
// } from '../../utils/commonReduxSagaConverter/commonConstants';

export const useOnlineProgramDetailHook = (
  {
    getData,
    ONLINE_PROGRAM_DETAIL_API_CALL,
    SUGGESTED_ONLINE_PROGRAMS_API_CALL,
    dashboard: {
      ONLINE_PROGRAM_DETAIL_API,
      SUGGESTED_ONLINE_PROGRAMS_API,
    } = {},
  },
  { programId = null },
) => {
  useEffect(
    () => {
      if (programId) {
        ONLINE_PROGRAM_DETAIL_API_CALL({
          params: {
            id: programId,
          },
        });

        SUGGESTED_ONLINE_PROGRAMS_API_CALL({
          params: {
            id: programId,
          },
        });
      }
    },
    [programId],
  );

  const programDetail = useMemo(() => getData(ONLINE_PROGRAM_DETAIL_API, {}), [
    ONLINE_PROGRAM_DETAIL_API,
  ]);

  const suggestedPrograms = useMemo(
    () => getData(SUGGESTED_ONLINE_PROGRAMS_API, []),
    [SUGGESTED_ONLINE_PROGRAMS_API],
  );

  return {
    programDetail: {
      data: programDetail.data,
      loader: programDetail.loader,
      lastUpdated: programDetail.lastUpdated,
    },
    suggestedPrograms: {
      data: suggestedPrograms.data,
      loader: suggestedPrograms.loader,
    },
  };
};
