// export { useHomePageHook } from './HomePageHook';
// export { useHeaderHook } from './HeaderHook';
// export { useArticleDetailPageHook } from './ArticleDetailHook';
// export { useCategoryPostHook } from './CategoryPostHook';
export {
  useRegisterHook,
  useLoginHook,
  useVerifyOTPHook,
  useOnboardingHook,
  useProfileUpdateHook,
} from './AuthenticationHooks';
export { useDropdownClose } from './common';
export {
  useArticleHook,
  useArticleCategoryHook,
  useArticleSubcategoryHook,
  useArticleDetail,
  useSearchHooks,
  useBookmarkHook,
  useLikeHook,
  useIntrestingArticlesHook,
  useArticleMostRead,
} from './articleHooks';
export {
  useReviewHook,
  useProfileHook,
  useChildrenHook,
  useScheduleHook,
  useWishlistHook,
  useClickCount,
} from './UserHooks';
export { useDayCareSearchHook, useDayCareDetailHook } from './DaycareHooks';
export { useProviderHook } from './providerHooks';
export {
  useDashboardOnlineProgramsHook,
  useOnlineProgramSearchHook,
  useOnlineProgramDetailHook,
  useOnlineProgramCategoryHook,
  useOnlineProgramPaymentHook,
  useOnlineProgramCityHook,
  usePurchasedOnlineProgramHook,
  useOnlineProgramReviewHook,
  useBookmarkProgramHook,
  useTransactionHook,
} from './OnlineProgramHooks';
