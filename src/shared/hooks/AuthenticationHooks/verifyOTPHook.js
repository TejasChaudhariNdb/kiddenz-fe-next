import { useState, useEffect, useMemo } from 'react';
const getPlatformBasedFieldValue = e =>
  typeof e === 'object' ? e.target.value : e;

const validateOTP = OTP => {
  if (!OTP) {
    return 'Please enter 4 Digit OTP';
  }
  if (OTP.length !== 4) {
    return 'OTP must be 4 digit long';
  }
  return false;
};

export const useVerifyOTPHook = (
  {
    VERIFY_OTP_API_CALL,
    GET_ONLINE_PROGRAM_WISHLIST_API_CALL,
    VERIFY_OTP_API_CANCEL,
    RESEND_OTP_API_CALL,
    RESEND_OTP_API_CANCEL,
    authentication: { profile, VERIFY_OTP_API },
    getData,
  },
  {
    onSuccess,
    successToast,
    errorToast,
    // userId
  },
) => {
  const [OTP, setOTP] = useState('');
  const [OTPError, setOTPError] = useState('');
  const [otpTimer, setOtpTimer] = useState('');

  useEffect(
    () => () => {
      VERIFY_OTP_API_CANCEL();
      RESEND_OTP_API_CANCEL();
    },
    [],
  );
  const handelOnSuccess = ({ res, data, message, status, showEmailPopUp }) => {
    GET_ONLINE_PROGRAM_WISHLIST_API_CALL();
    onSuccess({ res, data, message, status, showEmailPopUp });
  };

  const submitOtp = () => {
    const error = validateOTP(OTP);
    if (error) {
      setOTPError(error);
    } else {
      VERIFY_OTP_API_CALL({
        payload: {
          otp: +OTP,
          mobile_number: profile.mobile_number,
        },

        successCallback: data =>
          handelOnSuccess({
            ...data,
            showEmailPopUp: profile.showEmailPopUp,
          }),
        // eslint-disable-next-line consistent-return
        errorCallback: ({ message }) => {
          if (message) setOTPError(message);
          else {
            // errorToast('Something Went Wrong')
            return 0;
          }
        },
      });
    }
  };

  const onChangeOTP = e => {
    const value = getPlatformBasedFieldValue(e);
    if (OTPError) {
      setOTPError('');
    }
    if (
      (Number(value) === 0 || Number(value)) &&
      !VERIFY_OTP_API.loading.status &&
      value.length <= 4
    )
      setOTP(value);
  };

  const onBlurOTP = e => {
    e.preventDefault();
    const error = validateOTP(OTP);
    if (error && OTP) {
      setOTPError(error);
    }
  };

  const resendOtp = () => {
    setOTPError('');
    setOTP('');
    RESEND_OTP_API_CALL({
      payload: {
        mobile_number: profile.mobile_number,
      },
      successCallback: () => {
        successToast('Otp sent successfully');
        setOtpTimer('0:59');
      },
      errorCallback: ({ message: errorMessage }) => {
        errorToast(errorMessage || 'Unable to send otp');
      },
    });
  };

  const verifyOtp = useMemo(() => getData(VERIFY_OTP_API, {}, false), [
    VERIFY_OTP_API,
  ]);

  return {
    otp: {
      value: OTP,
      onChange: onChangeOTP,
      onBlur: onBlurOTP,
      error: OTPError,
      onSubmit: submitOtp,
      resend: resendOtp,
      otpTimer,
      loader: verifyOtp.loader,
    },
    mobileNo: profile.mobile_number,
    countryCode: (profile.countryCode && profile.countryCode.code) || '+91',
  };
};
