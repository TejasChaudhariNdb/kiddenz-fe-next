import { useState, useEffect, useCallback, useMemo } from 'react';
import debounce from 'lodash.debounce';
import validateForm from '../../utils/formValidation';
export const useRegisterHook = (
  {
    LOGIN_API_CALL,
    LOGIN_API_CANCEL,
    authentication: { LOGIN_API, profile },
    getData,
  },
  { onSuccess, onError, isEdit = false } = {},
) => {
  const [name, setName] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [emailAddress, setEmailAddress] = useState('default@kiddenz.com');
  const [locationAddress, setLocationAddress] = useState('');
  const [error, setError] = useState({});
  const [isSubscribed, setIsSubscribed] = useState(
    isEdit ? profile.is_subscribed : true,
  );

  const VALIDATOR_CONFIG = {
    name: { type: 'name', value: name },
    mobile: { type: 'mobile', value: mobileNo },
    email: { type: 'email', value: emailAddress, optional: true },
    locationAddress: { type: 'locationAddress', value: locationAddress },
  };

  useEffect(
    () => () => {
      LOGIN_API_CANCEL();
    },
    [],
  );

  useEffect(
    () => {
      setName(isEdit ? profile.name : '');
      setMobileNo(isEdit ? profile.mobile_number : '');
      setEmailAddress(isEdit ? profile.email : 'default@kiddenz.com');
      setLocationAddress(isEdit ? profile.location_address : '');
    },
    [isEdit],
  );

  const debouncedHandelAuthFunction = debounce(() => {
    handelAuth();
  }, 1000);

  const handelAuth = () => {
    if (!emailAddress || emailAddress.length === 0) {
      setEmailAddress('default@kiddenz.com');
    }
    const errorObj = validateForm({
      mobile: { type: 'mobile', value: mobileNo },
      email: { type: 'email', value: emailAddress },
      name: { type: 'name', value: name },
      locationAddress: { type: 'locationAddress', value: locationAddress },
    });

    if (!errorObj.isError) {
      const payload = {
        name,
        mobile_number: mobileNo,
        email: emailAddress,
        auth_type: 'register',
        location_address: locationAddress,
      };
      LOGIN_API_CALL({
        payload,
        errorCallback: ({ errorData, message }) => {
          onError({ errorData, message });
        },
        successCallback: ({ res, data, message, status }) => {
          onSuccess({ res, data, message, status });
        },
      });
    } else {
      setError(errorObj);
    }
  };

  const onChange = useCallback(
    (e, key) => {
      e.preventDefault();
      const value = getPlatformBasedFieldValue(e);
      if (key === 'name') setName(value);
      else if (key === 'email')
        setEmailAddress(
          value ? value.replace(/\s*$/, '') : 'default@kiddenz.com',
        );
      else if (key === 'locationAddress') setLocationAddress(value);
      else {
        // eslint-disable-next-line no-lonely-if
        if (
          (Number(value) === 0 || Number(value)) &&
          value.length <= 10 &&
          !LOGIN_API.loading.status
        )
          setMobileNo(value);
      }
    },
    [name, mobileNo, emailAddress, locationAddress],
  );

  const onChangeSubscribeEmail = e => {
    e.preventDefault();
    setIsSubscribed(!isSubscribed);
  };

  const onBlur = useCallback(
    (e, key) => {
      e.preventDefault();
      const errorObj = validateForm({
        [key]: {
          type: VALIDATOR_CONFIG[key].type,
          value: VALIDATOR_CONFIG[key].value,
        },
      });
      if (errorObj.isError) {
        setError({ ...error, [key]: errorObj[key] });
      } else {
        setError({ ...error, [key]: null });
      }
    },
    [name, mobileNo, emailAddress, locationAddress, error],
  );

  const authenticationRegister = useMemo(
    () => getData(LOGIN_API, [], false),
    LOGIN_API,
  );

  return {
    mobile: {
      value: mobileNo,
    },
    name: {
      value: name,
    },
    email: {
      value: emailAddress,
    },
    locationAddress: {
      value: locationAddress,
    },
    subscription: {
      onChange: onChangeSubscribeEmail,
      value: isSubscribed,
    },
    onBlur,
    onChange,
    error,
    onSubmit: debouncedHandelAuthFunction,
    registerLoader: authenticationRegister.loader,
  };
};

// Helpers
function getPlatformBasedFieldValue(e) {
  return typeof e === 'object' ? e.target.value : e;
}
