/* eslint-disable camelcase */
import { useState, useEffect, useCallback, useMemo } from 'react';
import axios from 'config/axios';
import validateForm from '../../utils/formValidation';
import { deleteCookie } from '../../utils/commonHelpers';

export const useLoginHook = (
  {
    LOGIN_API_CALL,
    LOGIN_API_CANCEL,
    authentication: { LOGIN_API, profile },
    getData,
  },
  { onSuccess, onError, isEdit = false } = {},
) => {
  const [mobileNo, setMobileNo] = useState(isEdit ? profile.mobile_number : '');
  const [error, setError] = useState({});

  useEffect(
    () => () => {
      LOGIN_API_CANCEL();
    },
    [],
  );

  // eslint-disable-next-line camelcase
  const handelAuth = (
    isSecondryFormElemError = false,
    { auth_type, name = '' } = {},
  ) => {
    deleteCookie('x-access-token');
    axios.defaults.headers.common.Authorization = '';
    const errorObj = validateForm({
      mobile: { type: 'mobile', value: mobileNo },
    });

    if (!errorObj.isError && !isSecondryFormElemError) {
      let payload = {
        mobile_number: mobileNo,
        auth_type,
      };
      if (auth_type === 'register') {
        payload = {
          ...payload,
          name,
        };
      }
      LOGIN_API_CALL({
        payload,
        errorCallback: ({ errors, message }) => {
          onError({ errors, message });
        },
        successCallback: ({ res, data, message, status }) => {
          onSuccess({ res, data, message, status });
        },
      });
    } else {
      setError(errorObj);
    }
  };

  const onChangeMobile = e => {
    const value = getPlatformBasedFieldValue(e);

    if (
      (Number(value) === 0 || Number(value)) &&
      value.length <= 10 &&
      !LOGIN_API.loading.status
    )
      setMobileNo(value);
  };

  const onBlurMobile = useCallback(
    e => {
      e.preventDefault();
      const errorObj = validateForm({
        mobile: { type: 'mobile', value: mobileNo },
      });

      if (errorObj.isError) setError({ ...error, mobile: errorObj.mobile });
      else setError({ ...error, mobile: null });
    },
    [mobileNo, error],
  );

  const authenticationLogin = useMemo(() => getData(LOGIN_API, [], false), [
    LOGIN_API,
  ]);

  return {
    mobile: {
      value: mobileNo,
      onChange: onChangeMobile,
      onBlur: onBlurMobile,
      error: error.mobile,
    },
    onSubmit: handelAuth,
    loginLoader: authenticationLogin.loader,
  };
};

// Helpers
function getPlatformBasedFieldValue(e) {
  return typeof e === 'object' ? e.target.value : e;
}
