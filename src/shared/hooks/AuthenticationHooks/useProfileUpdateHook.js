import { useState, useEffect, useCallback, useMemo } from 'react';
import validateForm from '../../utils/formValidation';

export const useProfileUpdateHook = (
  {
    ONBOARDING_PROFILE_UPDATE_API_CALL,
    ONBOARDING_PROFILE_UPDATE_API_CANCEL,
    // USER_EMAIL_VERIFY_API_CALL,
    USER_EMAIL_VERIFY_API_CANCEL,
    authentication: { ONBOARDING_PROFILE_UPDATE_API },
    getData,
  },
  { onSuccess, onError, signUpName = '' } = {},
) => {
  const [name, setName] = useState('');

  const [emailAddress, setEmailAddress] = useState('');
  const [error, setError] = useState({});

  const VALIDATOR_CONFIG = {
    name: { type: 'name', value: name },
    email: { type: 'email', value: emailAddress },
  };

  useEffect(
    () => {
      setName(signUpName);
    },
    [signUpName],
  );

  useEffect(
    () => () => {
      ONBOARDING_PROFILE_UPDATE_API_CANCEL();
      USER_EMAIL_VERIFY_API_CANCEL();
    },
    [],
  );

  const handelProfileSubmit = () => {
    const errorObj = validateForm({
      email: { type: 'email', value: emailAddress },
      name: { type: 'name', value: name },
    });

    if (!errorObj.isError) {
      const payload = {
        name,
        email: emailAddress,
      };
      ONBOARDING_PROFILE_UPDATE_API_CALL({
        payload,
        errorCallback: ({ errorData, message }) => {
          onError({ errorData, message });
        },
        successCallback: ({ res, data, message, status }) => {
          // USER_EMAIL_VERIFY_API_CALL({
          //   payload: {
          //     email: emailAddress,
          //   },
          // });
          onSuccess({ res, data, message, status });
        },
      });
    } else {
      setError(errorObj);
    }
  };

  const onChange = useCallback(
    (e, key) => {
      e.preventDefault();
      const value = getPlatformBasedFieldValue(e);
      if (key === 'name') setName(value);
      else if (key === 'email')
        setEmailAddress(value ? value.replace(/\s*$/, '') : '');
    },
    [name, emailAddress],
  );

  const onBlur = useCallback(
    (e, key) => {
      e.preventDefault();
      const errorObj = validateForm({
        [key]: {
          type: VALIDATOR_CONFIG[key].type,
          value: VALIDATOR_CONFIG[key].value,
        },
      });
      if (errorObj.isError) {
        setError({ ...error, [key]: errorObj[key] });
      } else {
        setError({ ...error, [key]: null });
      }
    },
    [name, emailAddress, error],
  );

  const profileSubmitData = useMemo(
    () => getData(ONBOARDING_PROFILE_UPDATE_API, [], false),
    [ONBOARDING_PROFILE_UPDATE_API],
  );

  return {
    name: {
      value: name,
    },
    email: {
      value: emailAddress,
    },
    onBlur,
    onChange,
    error,
    onSubmit: handelProfileSubmit,
    profileLoader: profileSubmitData.loader,
  };
};

// Helpers
function getPlatformBasedFieldValue(e) {
  return typeof e === 'object' ? e.target.value : e;
}
