import { useEffect, useMemo } from 'react';

export const useOnboardingHook = (
  {
    ONBOARDING_API_CALL,
    ONBOARDING_API_CANCEL,
    authentication: { ONBOARDING_API },
    getData,
  },
  { onSuccess, onError },
) => {
  useEffect(
    () => () => {
      ONBOARDING_API_CANCEL();
    },
    [],
  );

  const onSumitHandler = payload => {
    ONBOARDING_API_CALL({
      payload,
      errorCallback: ({ errorData, message }) => {
        onError({ errorData, message });
      },
      successCallback: ({ res, data, message, status }) => {
        onSuccess({ res, data, message, status });
      },
    });
  };

  const onboardingData = useMemo(() => getData(ONBOARDING_API, [], false), [
    ONBOARDING_API,
  ]);

  return {
    onSubmit: onSumitHandler,
    onboardingLoader: onboardingData.loader,
  };
};
