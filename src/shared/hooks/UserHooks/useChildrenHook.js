/* eslint-disable no-nested-ternary */
/* eslint-disable eqeqeq */
/* eslint-disable indent */
/* eslint-disable no-restricted-globals */
/* eslint-disable no-plusplus */
import { useEffect, useState, useMemo, useCallback } from 'react';
import findIndex from 'lodash/findIndex';
import axios from 'axios';
import cloneDeep from 'lodash/cloneDeep';
import { ON_SUCCESS } from 'shared/utils/commonReduxSagaConverter/commonConstants';
import validateForm from '../../utils/formValidation';

const FORM_CONFIG = {
  first_name: null,
  last_name: null,
  gender_type: 'male',
  date_of_birth: null,
  age: null,
  child_care_experience: 'nanny',
  food_type: [],
  food_choice: [],
  tution: [],
  tution_type: null,
  courses: [],
  allergies: null,
  medication_details: null,
  special_care: true,
  other_specific_needs: null,
  comments: null,
};

// const requiredFields = [
//   'first_name',
//   'last_name',
//   'date_of_birth',
//   //   'gender_type',
//   //   'child_care_experience',
//   //   'food_type',
//   //   'food_choice',
//   //   'special_care',
// ];

export const useChildrenHook = (
  {
    GET_USERS_CHILDREN_API_CUSTOM_TASK,
    ADD_USERS_CHILDREN_API_CALL,
    ADD_USERS_CHILDREN_API_CANCEL,
    EDIT_USERS_CHILDREN_API_CALL,
    EDIT_USERS_CHILDREN_API_CANCEL,
    DELETE_USERS_CHILDREN_API_CALL,
    DELETE_USERS_CHILDREN_API_CANCEL,
    dashboard: {
      GET_USERS_CHILDREN_API,
      ADD_USERS_CHILDREN_API,
      DELETE_USERS_CHILDREN_API,
    },
    getData,
  },
  {
    isEdit = false,
    childId = null,
    onChildAddSuccess = () => {},
    onChildAddError = () => {},
    onChildEditSuccess = () => {},
    onChildEditError = () => {},
    onChildDeleteSuccess = () => {},
    onChildDeleteError = () => {},
  } = {},
) => {
  const [value, setValue] = useState(FORM_CONFIG);
  const [error, setError] = useState({});
  const [image, setImage] = useState(null);
  const [imageId, setImageId] = useState(null);
  // const [isEdit, setIsEdit] = useState(false);

  const VALIDATOR_CONFIG = {
    first_name: { type: 'name', value: value.first_name },
    last_name: { type: 'name', value: value.last_name },
  };

  useEffect(
    () => () => {
      ADD_USERS_CHILDREN_API_CANCEL();
      EDIT_USERS_CHILDREN_API_CANCEL();
      DELETE_USERS_CHILDREN_API_CANCEL();
    },
    [],
  );
  useEffect(
    () => {
      if (isEdit) {
        onEditClick(childId);
      }
    },
    [isEdit],
  );

  // On Input Change
  const onInputChange = useCallback(
    (e, key, type = 'text', inputValue, actionType) => {
      // if (key !== 'date_of_birth') e.preventDefault();
      const tempForm = { ...value };
      if (type === 'text') {
        if (key !== 'date_of_birth') {
          const val = getPlatformBasedFieldValue(e);
          tempForm[key] = val;
        }
        if (key === 'date_of_birth') {
          tempForm.date_of_birth = inputValue;

          const ageVal = getAge(inputValue);
          tempForm.age = isNaN(ageVal) ? 0 : ageVal;
        }
      }
      if (type === 'radio') {
        tempForm[key] = inputValue;
      }
      if (type === 'checkbox') {
        if (actionType === 'add') tempForm[key].push(inputValue);
        else {
          const index = tempForm[key].indexOf(inputValue);
          if (index > -1) {
            tempForm[key].splice(index, 1);
          }
        }
      }
      setValue(tempForm);
    },
    [value, error],
  );

  // On Input Blur
  const onInputBlur = useCallback(
    (e, key) => {
      if (key !== 'date_of_birth' && key !== 'tution_type') e.preventDefault();
      let errorObj = {};
      if (key !== 'date_of_birth' && key !== 'tution_type') {
        errorObj = validateForm({
          [key]: {
            type: VALIDATOR_CONFIG[key].type,
            value: VALIDATOR_CONFIG[key].value,
          },
        });
      }

      if (key === 'date_of_birth' && !value.date_of_birth) {
        errorObj =
          key === 'date_of_birth' && !value.date_of_birth
            ? {
                ...errorObj,
                ...{
                  isError: true,
                  date_of_birth: 'Please provide the necessary details.',
                },
              }
            : {};
      }

      if (errorObj.isError) {
        setError({ ...error, [key]: errorObj[key] });
      } else {
        setError({ ...error, [key]: null });
      }
    },
    [value, error],
  );

  // On child submit
  const onSubmit = useCallback(
    id => {
      let errorObj = {};
      errorObj = validateForm({
        first_name: { type: 'name', value: value.first_name },
        last_name: { type: 'name', value: value.last_name },
      });

      errorObj = !value.date_of_birth
        ? {
            ...errorObj,
            ...{
              isError: true,
              date_of_birth: 'Please provide the necessary details.',
            },
          }
        : {};

      if (!errorObj.isError) {
        const payLoadValues = {
          ...value,
          food_choice: value.food_choice.toString(),
          food_type: value.food_type.toString(),
          tution: (value.tution || []).toString(),
          courses: (value.courses || []).toString(),
          ...(imageId && image ? { photo_id: imageId, photo_url: image } : {}),
        };
        const payload = isEdit
          ? removeEmpty(payLoadValues)
          : [{ ...payLoadValues }];

        (isEdit ? EDIT_USERS_CHILDREN_API_CALL : ADD_USERS_CHILDREN_API_CALL)({
          params: {
            id,
          },
          payload,
          successCallback: ({ data: { data = [] } = {} }) => {
            GET_USERS_CHILDREN_API_CUSTOM_TASK(
              ON_SUCCESS,
              isEdit
                ? {
                    isUpdate: true,
                    id: [id],
                    key: 'id',
                  }
                : { isInfinite: true },
              {
                data: isEdit
                  ? payload
                  : {
                      ...payload[0],
                      id: data[0].id,
                    },
              },
            );
            if (isEdit) onChildEditSuccess();
            else onChildAddSuccess();
          },
          errorCallback: ({ message }) => {
            if (isEdit) onChildEditError(message);
            else onChildAddError(message);
          },
        });
      } else {
        setError(errorObj);
      }
    },
    [value, error, isEdit],
  );

  // Child Delete handler
  const onChildDelete = id => {
    DELETE_USERS_CHILDREN_API_CALL({
      query: {
        id,
      },
      successCallback: () => {
        onChildDeleteSuccess();
        GET_USERS_CHILDREN_API_CUSTOM_TASK(ON_SUCCESS, {
          isDelete: true,
          id: [id],
          key: 'id',
        });
      },
      errorCallback: ({ message }) => {
        onChildDeleteError({ message });
      },
    });
  };
  // Reset Form
  const reset = () => {
    const formInitial = cloneDeep(FORM_INITIAL_STATE);
    setValue(formInitial);
    // setIsEdit(false);
  };

  // Image upload
  const handleImageChnage = e => {
    e.preventDefault();
    const reader = new FileReader();
    const img = e.target.files[0];
    reader.onloadend = () => {
      const PAYLOAD = new FormData();
      PAYLOAD.append('media', img);
      axios
        .post(
          'https://kiddenzbc.cartoonmango.com/api/media/upload-file/',
          PAYLOAD,
        )
        .then(res => res.data.data)
        .then(file => {
          setImage(file.image_url);
          setImageId(file.id);
        })
        // eslint-disable-next-line no-console
        .catch(err => console.log(err));
    };
    if (e.target.files[0]) {
      reader.readAsDataURL(img);
    }
  };

  // <****************DATA RETURN********************>
  const childrenList = useMemo(
    () => getData(GET_USERS_CHILDREN_API, [], true),
    [GET_USERS_CHILDREN_API],
  );

  const addChild = useMemo(() => getData(ADD_USERS_CHILDREN_API, [], false), [
    ADD_USERS_CHILDREN_API,
  ]);

  const deleteData = useMemo(
    () => getData(DELETE_USERS_CHILDREN_API, [], false),
    [DELETE_USERS_CHILDREN_API],
  );

  // on edit Click pre populate data from store
  const onEditClick = id => {
    const index = findIndex(childrenList.data, o => o.id == id);
    let tempChildData = { ...childrenList.data[index] };

    // Convert comma sep data to type arrays
    const modFoodChoice =
      typeof tempChildData.food_choice === 'string'
        ? tempChildData.food_choice.split(',')
        : [];
    const modFoodType =
      typeof tempChildData.food_type === 'string'
        ? tempChildData.food_type.split(',')
        : [];
    const modTution =
      typeof tempChildData.tution === 'string'
        ? tempChildData.tution.split(',')
        : [];
    const modCourses =
      typeof tempChildData.courses === 'string'
        ? tempChildData.courses.split(',')
        : [];

    tempChildData = {
      ...tempChildData,
      tution: modTution,
      courses: modCourses,
      food_choice: modFoodChoice,
      food_type: modFoodType,
      date_of_birth:
        new Date(tempChildData.date_of_birth).getFullYear() === 1000
          ? ''
          : tempChildData.date_of_birth,
    };
    // setIsEdit(true);
    setValue(tempChildData);
  };

  return {
    childList: childrenList.data,
    onChange: onInputChange,
    onBlur: onInputBlur,
    formValues: value,
    resetForm: reset,
    onChildSubmit: onSubmit,
    onChildEditClick: onEditClick,
    addChildLoader: addChild.loader,
    onChildDelete,
    deleteLoader: deleteData.loader,
    error,
    handleImageChnage,
    uploadedImage: image,
  };
};

// Helpers
function getPlatformBasedFieldValue(e) {
  return typeof e === 'object' ? e.target.value : e;
}

function getAge(dateString) {
  // milliseconds in a year 1000*24*60*60*365.24 = 31556736000;
  const today = new Date();
  // birthay has 'Dec 25 1998'
  const dob = new Date(dateString);
  // difference in milliseconds
  const diff = today.getTime() - dob.getTime();
  // convert milliseconds into years
  const years = Math.floor(diff / 31556736000);
  // 1 day has 86400000 milliseconds
  const daysDiff = Math.floor((diff % 31556736000) / 86400000);
  // 1 month has 30.4167 days
  const months = Math.floor(daysDiff / 30.4167);

  return years == 0 ? 0 : months == 0 ? years : months > 6 ? years + 1 : years;
}

const FORM_INITIAL_STATE = {
  first_name: null,
  last_name: null,
  gender_type: 'male',
  date_of_birth: null,
  age: null,
  child_care_experience: 'nanny',
  food_type: [],
  food_choice: [],
  allergies: null,
  tution: [],
  tution_type: null,
  courses: [],
  medication_details: null,
  special_care: true,
  other_specific_needs: null,
  comments: null,
};

const removeEmpty = obj =>
  Object.fromEntries(
    Object.entries(obj)
      // eslint-disable-next-line no-unused-vars
      .filter(([_, v]) => v != null)
      .map(
        ([k, v]) =>
          typeof v === 'object' && !Array.isArray(v)
            ? [k, removeEmpty(v)]
            : [k, v],
      ),
  );
