import { useEffect } from 'react';

export const useClickCount = ({
  GET_CLICK_COUNT_API_CALL,
  GET_CLICK_COUNT_API_CANCEL,
  POST_CLICK_COUNT_API_CALL,
  POST_CLICK_COUNT_API_CANCEL,
  dispatch,
  // dashboard: { GET_CLICK_COUNT_API },
  authentication: { profile = {} } = {},
  // getData,
} = {}) => {
  const { data: { parent_clicks: clickData = {} } = {} } = profile;
  useEffect(() => {
    if (GET_CLICK_COUNT_API_CALL) {
      GET_CLICK_COUNT_API_CALL({
        successCallback: ({ data: { data } = {} }) =>
          dispatch({
            type: 'PROFILE_UPDATE',
            payload: {
              data: { ...profile.data, parent_clicks: data },
            },
          }),
      });
    }
    return () => {
      if (GET_CLICK_COUNT_API_CANCEL && POST_CLICK_COUNT_API_CANCEL) {
        GET_CLICK_COUNT_API_CANCEL();
        POST_CLICK_COUNT_API_CANCEL();
      }
    };
  }, []);

  const postCount = type => {
    POST_CLICK_COUNT_API_CALL({
      payload: { click_type: type },
      successCallback: ({ data: { data } = {} }) =>
        dispatch({
          type: 'PROFILE_UPDATE',
          payload: {
            data: { ...profile.data, parent_clicks: data },
          },
        }),
    });
  };

  return {
    postCount,
    clickData,
  };
};
