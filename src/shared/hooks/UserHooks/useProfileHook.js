/* eslint-disable camelcase */
// useReviewHooks

import { useEffect, useState, useMemo, useCallback } from 'react';
import { ON_SUCCESS } from 'shared/utils/commonReduxSagaConverter/commonConstants';
import axios from 'axios';
import BASE_URL, { servertype } from 'utils/BaseUrl';
import Geocode from 'react-geocode';
import validateForm from '../../utils/formValidation';
export const useProfileHook = (
  {
    GET_USER_DETAILS_API_CUSTOM_TASK,
    EDIT_USER_DETAILS_API_CALL,
    EDIT_USER_DETAILS_API_CANCEL,
    USER_EMAIL_VERIFY_API_CALL,
    USER_EMAIL_VERIFY_API_CANCEL,
    dispatch,
    dashboard: { GET_USER_DETAILS_API, EDIT_USER_DETAILS_API },
    getData,
  },
  {
    onProfileEditSuccess,
    onProfileEditError,
    onVerifyEmailSendSuccess,
    onVerifyEmailSendError,
  },
) => {
  const [name, setName] = useState('');
  const [image, setImage] = useState(null);
  const [imageId, setImageId] = useState(null);
  const [email, setEmail] = useState('');
  const [nameDef, setNameDef] = useState('');
  const [emailDef, setEmailDef] = useState('');
  const [transportLocationStringDef, setTransportLocationStringDef] = useState(
    '',
  );
  const [areaLocationStringDef, setAreaLocationStringDef] = useState('');
  const [transportCordsDef, setTransportCordsDef] = useState('');
  const [areaCordsDef, setAreaCordsDef] = useState('');
  const [error, setError] = useState({});
  const [locationSearchSting, setLocationSearchSting] = useState('');
  const [area, setArea] = useState('');
  const [isImageUploading, setImageUploadingStatus] = useState(false);

  const [transportCords, setTransportCords] = useState({
    transport_lattitude: 0.0,
    transport_longitude: 0.0,
  });

  const [areaCords, setAreaCords] = useState({
    preferred_lattitude: 0.0,
    preferred_longitude: 0.0,
  });

  const [locationSearchError, setLocationSearchError] = useState(false);

  const [
    transportLocationSearchError,
    setTransportLocationSearchError,
  ] = useState(false);

  const VALIDATOR_CONFIG = {
    name: { type: 'name', value: name },
    email: { type: 'email', value: email },
  };

  useEffect(
    () => () => {
      EDIT_USER_DETAILS_API_CANCEL();
      USER_EMAIL_VERIFY_API_CANCEL();
    },
    [],
  );

  const test = () => {
    // onSubmit();
    // GET_USER_DETAILS_API_CUSTOM_TASK(ON_SUCCESS, {
    //   isUpdate: true,
    //   updateCallback: oldData => ({ ...oldData, email: 'test' }),
    // });
  };

  // On Input Change
  const onInputChange = useCallback(
    (e, key) => {
      e.preventDefault();
      const value = getPlatformBasedFieldValue(e);
      if (key === 'name') setName(value);
      if (key === 'email') setEmail(value);
    },
    [name, email],
  );

  // On Input Blur
  const onInputBlur = useCallback(
    (e, key) => {
      e.preventDefault();
      const errorObj = validateForm({
        [key]: {
          type: VALIDATOR_CONFIG[key].type,
          value: VALIDATOR_CONFIG[key].value,
        },
      });
      if (errorObj.isError) {
        setError({ ...error, [key]: errorObj[key] });
      } else {
        setError({ ...error, [key]: null });
      }
    },
    [name, email, error],
  );

  // On Profile Edit Submit
  const onSubmit = () => {
    const errorObj = validateForm({
      email: { type: 'email', value: email },
      name: { type: 'name', value: name },
    });

    // if (true) {
    if (
      !errorObj.isError &&
      !locationSearchError &&
      !transportLocationSearchError
    ) {
      const payload = {
        name,
        email,
        ...(imageId && image ? { photo_id: imageId, photo_url: image } : {}),
        ...transportCords,
        ...areaCords,
        prefered_location_string: area,
        transport_location_string: locationSearchSting,
      };

      EDIT_USER_DETAILS_API_CALL({
        payload,
        successCallback: ({ data: { data } = {} }) => {
          const { parent_profile } = data;
          onProfileEditSuccess();
          dispatch({
            type: 'PROFILE_UPDATE',
            payload: { data: parent_profile },
          });
          GET_USER_DETAILS_API_CUSTOM_TASK(ON_SUCCESS, {
            isUpdate: true,
            updateCallback: oldData => ({ ...oldData, ...data }),
          });
        },
        errorCallback: ({ message }) => {
          onProfileEditError({ message });
        },
      });
    } else {
      setError(errorObj);
    }
  };

  // on Edit Cancel
  const onEditCancel = () => {
    setName(nameDef);
    setEmail(emailDef);
    setLocationSearchSting(transportLocationStringDef);
    setArea(areaLocationStringDef);
    setTransportCords(transportCordsDef);
    setAreaCords(areaCordsDef);
  };

  // Verify Email Handler
  const onVerifyEmailClickHandler = mail => {
    USER_EMAIL_VERIFY_API_CALL({
      payload: {
        email: mail,
        server_type: servertype === 'staging' ? 'staging' : 'production',
      },
      errorCallback: ({ message }) => {
        onVerifyEmailSendError({ message });
      },
      successCallback: ({ res, data, message, status }) => {
        onVerifyEmailSendSuccess({ res, data, message, status });
      },
    });
  };

  // Image upload
  const handleImageChnage = e => {
    e.preventDefault();
    const reader = new FileReader();
    const img = e.target.files[0];
    reader.onloadend = () => {
      const PAYLOAD = new FormData();
      PAYLOAD.append('media', img);
      setImageUploadingStatus(true);
      axios
        .post(`${BASE_URL}api/media/upload-file/`, PAYLOAD)
        .then(res => res.data.data)
        .then(file => {
          setImage(file.image_url);
          setImageId(file.id);
          setImageUploadingStatus(false);
        })
        // eslint-disable-next-line no-console
        .catch(err => console.log(err));
    };
    if (e.target.files[0]) {
      reader.readAsDataURL(img);
    }
  };

  useEffect(
    () => {
      setTransportCords({
        transport_lattitude,
        transport_longitude,
      });
      setAreaCords({
        preferred_lattitude,
        preferred_longitude,
      });

      if (!transport_location_string) {
        Geocode.fromLatLng(transport_lattitude, transport_longitude).then(
          response => {
            const address = response.results[0].formatted_address;
            setLocationSearchSting(address);
            setTransportLocationStringDef(address);
          },
          err => {
            // eslint-disable-next-line no-console
            console.error(err);
          },
        );
      } else {
        setLocationSearchSting(transport_location_string);
        setTransportLocationStringDef(transport_location_string);
      }

      if (!prefered_location_string) {
        Geocode.fromLatLng(preferred_lattitude, preferred_longitude).then(
          response => {
            const address = response.results[0].formatted_address;
            setArea(address);
            setAreaLocationStringDef(address);
          },
          err => {
            // eslint-disable-next-line no-console
            console.error(err);
          },
        );
      } else {
        setArea(prefered_location_string);
        setAreaLocationStringDef(prefered_location_string);
      }
    },
    [
      transport_lattitude,
      transport_longitude,
      preferred_lattitude,
      preferred_longitude,
      prefered_location_string,
      transport_location_string,
    ],
  );

  // <****************DATA RETURN********************>
  const userDetails = useMemo(
    () => {
      const {
        data: {
          parent_profile: {
            name: useName,
            email: userEmail,
            mobile_number: userMobile,
            is_email_verified: isEmailVerified,
          } = {},
          parent_question: {
            transport_lattitude,
            transport_longitude,
            preferred_lattitude,
            preferred_longitude,
            prefered_location_string,
            transport_location_string,
          } = {},
        } = {},
      } = getData(GET_USER_DETAILS_API, {}, true);

      setName(useName);
      setNameDef(useName);
      setEmail(userEmail);
      setEmailDef(userEmail);

      setTransportCordsDef({
        transport_lattitude,
        transport_longitude,
      });
      setAreaCordsDef({
        preferred_lattitude,
        preferred_longitude,
      });

      return {
        userMobile,
        isEmailVerified,
        transport_lattitude,
        transport_longitude,
        preferred_lattitude,
        preferred_longitude,
        prefered_location_string,
        transport_location_string,
      };
    },
    [GET_USER_DETAILS_API],
  );

  const {
    userMobile,
    isEmailVerified,
    transport_lattitude,
    transport_longitude,
    preferred_lattitude,
    preferred_longitude,
    prefered_location_string,
    transport_location_string,
  } = userDetails;

  const userEditData = useMemo(
    () => getData(EDIT_USER_DETAILS_API, {}, false),
    [GET_USER_DETAILS_API],
  );

  return {
    name,
    email,
    mobile: userMobile,
    isMailVerified: isEmailVerified,
    onChange: onInputChange,
    onBlur: onInputBlur,
    onUpdateSubmit: onSubmit,
    onVerifyEmail: onVerifyEmailClickHandler,
    onEditCancel,
    editLoader: userEditData.loader,
    test,
    handleImageChnage,
    uploadedImage: image,
    isImageUploading,
    resetUploadedImage: () => setImage(null),
    setTransportCords,
    setAreaCords,
    locationSearchSting,
    setLocationSearchSting,
    area,
    setArea,
    locationSearchError,
    setLocationSearchError,
    transportLocationSearchError,
    setTransportLocationSearchError,
  };
};

// Helpers
function getPlatformBasedFieldValue(e) {
  return typeof e === 'object' ? e.target.value : e;
}
