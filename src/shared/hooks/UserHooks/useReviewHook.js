// useReviewHooks

import { useEffect, useState, useCallback } from 'react';

const RATING_PARAMETERS = {
  provider_id: '80',
  staff: '',
  facility: '',
  teachers: '',
  trust: '',
  curriculum: '',
  development: '',
  care: '',
  hygiene: '',
  safety: '',
  location: '',
  fees: '',
  communication: '',
  review: '',
  testimonial: '',
  improvement: '',
};

export const useReviewHook = (
  {
    USER_REVIEW_API_CALL,
    USER_REVIEW_API_CANCEL,
    // dashboard: { USER_REVIEW_API },
    // getData,
  },
  { onSuccess, onError },
) => {
  const [value, setValue] = useState(RATING_PARAMETERS);
  const [ratingError, setRatingError] = useState(false);
  const [reviewError, setReviewError] = useState(false);

  useEffect(
    () => () => {
      USER_REVIEW_API_CANCEL();
    },
    [],
  );

  const onSumitHandler = useCallback(
    customPayload => {
      const { review, testimonial, ...rest } = RATING_PARAMETERS;
      if (!review) setReviewError(true);
      else setReviewError(false);

      if (Object.values(rest).includes('')) setRatingError(true);
      else setRatingError(false);

      if (!ratingError && !reviewError)
        USER_REVIEW_API_CALL({
          payload: { ...value, ...customPayload },
          errorCallback: ({ errorData, message }) => {
            onError({ errorData, message });
          },
          successCallback: ({ res, data, message, status }) => {
            onSuccess({ res, data, message, status });
          },
        });
    },
    [ratingError, reviewError],
  );

  const onChnageHandler = useCallback(
    (key, val) => {
      RATING_PARAMETERS[key] = val;
      setValue(d => ({ ...d, ...RATING_PARAMETERS }));
    },
    [value],
  );

  //   const onboardingData = useMemo(() => getData(USER_REVIEW_API, [], false), [
  //     USER_REVIEW_API,
  //   ]);

  return {
    onSubmit: onSumitHandler,
    onChange: onChnageHandler,
    value,
    ratingError,
    reviewError,
    // onboardingLoader: onboardingData.loader,
  };
};
