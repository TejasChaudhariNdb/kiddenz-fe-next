/* eslint-disable no-nested-ternary */
/* eslint-disable eqeqeq */
/* eslint-disable camelcase */
// useReviewHooks
import { useMemo } from 'react';
import findIndex from 'lodash/findIndex';
import { ON_SUCCESS } from 'shared/utils/commonReduxSagaConverter/commonConstants';

//   { onSuccess, onError },
export const useWishlistHook = (
  {
    GET_USER_WISHLISTED_PRODUCTS_API_CALL,
    DAYCARE_SEARCH_API_CUSTOM_TASK,
    DAYCARE_TRIP_SEARCH_RESULTS_API_CUSTOM_TASK,
    DAYCARE_SUGGESTION_API_CUSTOM_TASK,
    GET_USER_WISHLISTED_PRODUCTS_API_CUSTOM_TASK,
    GET_USERS_TOUR_UNSCHEDULE_API_CUSTOM_TASK,
    GET_USERS_TOUR_SCHEDULE_API_CUSTOM_TASK,
    ADD_WISLIST_ITEM_API_CALL,
    DELETE_WISLIST_ITEM_API_CALL,
    // ADD_WISLIST_ITEM_API_CANCEL,
    // GET_USER_WISHLISTED_PRODUCTS_API_CANCEL,
    // DELETE_WISLIST_ITEM_API_CANCEL,
    // USER_REVIEW_API_CANCEL,
    dashboard: {
      GET_USER_WISHLISTED_PRODUCTS_API,
      GET_USERS_TOUR_SCHEDULE_API,
      GET_USERS_TOUR_UNSCHEDULE_API,
    },
    getData,
  },
  { onWishlistAddError = () => {}, onWishlistDelError = () => {} },
) => {
  // useEffect(
  //   () => () => {
  //     ADD_WISLIST_ITEM_API_CANCEL();
  //     DELETE_WISLIST_ITEM_API_CANCEL();
  //     GET_USER_WISHLISTED_PRODUCTS_API_CANCEL();
  //   },
  //   [],
  // );

  // Add to Wishlist
  const postWishlist = (provider_id, stateToChange = 'wishlist') => {
    const payload = { provider_id };
    const index =
      stateToChange === 'schedule'
        ? findIndex(schedule.data.data, o => o.id == provider_id)
        : stateToChange === 'unschedule'
          ? findIndex(unschedule.data.data, o => o.id == provider_id)
          : null;

    const payloadObj =
      stateToChange === 'schedule'
        ? schedule.data.data[index]
        : stateToChange === 'unschedule'
          ? unschedule.data.data[index]
          : {};

    ADD_WISLIST_ITEM_API_CALL({
      payload,
      successCallback: ({ data: { data: { count = 0 } = {} } = {} }) => {
        // Add Wishlist Id to the main arr
        // based on scope
        if (stateToChange === 'schedule' || stateToChange === 'unschedule') {
          (stateToChange === 'schedule'
            ? GET_USERS_TOUR_SCHEDULE_API_CUSTOM_TASK
            : GET_USERS_TOUR_UNSCHEDULE_API_CUSTOM_TASK)(ON_SUCCESS, {
            tasks: [
              {
                task: 'isUpdate',
                params: {
                  subKey: ['data'],
                  id: [provider_id],
                  key: 'id',
                  values: {
                    [provider_id]: { is_wishlisted: true },
                  },
                },
              },
            ],
          });

          // If Total Wishlist count < 3
          // Add the wishlisted item to list
          if (count < 3)
            GET_USER_WISHLISTED_PRODUCTS_API_CUSTOM_TASK(ON_SUCCESS, {
              tasks: [
                {
                  task: 'isInfinite',
                  params: {
                    subKey: ['wishlists'],
                    isInfinite: true,
                  },
                  value: {
                    wishlists: [{ ...payloadObj }],
                    count,
                  },
                },
              ],
            });
        }

        if (stateToChange === 'search') {
          DAYCARE_SEARCH_API_CUSTOM_TASK(ON_SUCCESS, {
            tasks: [
              {
                task: 'isUpdate',
                params: {
                  subKey: ['search_results'],
                  id: [provider_id],
                  key: 'id',
                  values: {
                    [provider_id]: { wishlist_id: true },
                  },
                },
              },
            ],
          });
        }

        if (stateToChange === 'trip') {
          DAYCARE_TRIP_SEARCH_RESULTS_API_CUSTOM_TASK(ON_SUCCESS, {
            tasks: [
              {
                task: 'isUpdate',
                params: {
                  subKey: ['items'],
                  id: [provider_id],
                  key: 'id',
                  values: {
                    [provider_id]: { wishlist_id: true },
                  },
                },
              },
            ],
          });
        }

        if (stateToChange === 'suggestion') {
          DAYCARE_SUGGESTION_API_CUSTOM_TASK(ON_SUCCESS, {
            tasks: [
              {
                task: 'isUpdate',
                params: {
                  subKey: ['search_results'],
                  id: [provider_id],
                  key: 'id',
                  values: {
                    [provider_id]: { wishlist_id: true },
                  },
                },
              },
            ],
          });
        }
      },
      errorCallback: ({ message }) => {
        onWishlistAddError(message);
      },
    });
  };

  // Remove Wishlist
  const deleteWishlist = (provider_id, stateToChange = 'wishlist') => {
    const payload = { provider_id };
    DELETE_WISLIST_ITEM_API_CALL({
      payload,
      successCallback: () => {
        // remove Wishlist Id to the main arr
        // based on scope
        if (stateToChange === 'wishlist') {
          GET_USER_WISHLISTED_PRODUCTS_API_CUSTOM_TASK(ON_SUCCESS, {
            isDelete: true,
            id: [provider_id],
            subKey: ['wishlists'],
            key: 'id',
          });

          // If present in unschedule then update
          GET_USERS_TOUR_UNSCHEDULE_API_CUSTOM_TASK(ON_SUCCESS, {
            tasks: [
              {
                task: 'isUpdate',
                params: {
                  subKey: ['data'],
                  id: [provider_id],
                  key: 'id',
                  values: {
                    [provider_id]: { is_wishlisted: false },
                  },
                },
              },
            ],
          });

          // If present in schedule then update
          GET_USERS_TOUR_SCHEDULE_API_CUSTOM_TASK(ON_SUCCESS, {
            tasks: [
              {
                task: 'isUpdate',
                params: {
                  subKey: ['data'],
                  id: [provider_id],
                  key: 'id',
                  values: {
                    [provider_id]: { is_wishlisted: false },
                  },
                },
              },
            ],
          });

          // call list api after delete
          onWishlistLoadMore(userWishlist.data.wishlists.length - 1);
        }

        if (stateToChange === 'schedule' || stateToChange === 'unschedule') {
          (stateToChange === 'schedule'
            ? GET_USERS_TOUR_SCHEDULE_API_CUSTOM_TASK
            : GET_USERS_TOUR_UNSCHEDULE_API_CUSTOM_TASK)(ON_SUCCESS, {
            tasks: [
              {
                task: 'isUpdate',
                params: {
                  subKey: ['data'],
                  id: [provider_id],
                  key: 'id',
                  values: {
                    [provider_id]: { is_wishlisted: false },
                  },
                },
              },
            ],
          });
        }

        if (stateToChange === 'search') {
          DAYCARE_SEARCH_API_CUSTOM_TASK(ON_SUCCESS, {
            tasks: [
              {
                task: 'isUpdate',
                params: {
                  subKey: ['search_results'],
                  id: [provider_id],
                  key: 'id',
                  values: {
                    [provider_id]: { wishlist_id: false },
                  },
                },
              },
            ],
          });
        }

        if (stateToChange === 'trip') {
          DAYCARE_TRIP_SEARCH_RESULTS_API_CUSTOM_TASK(ON_SUCCESS, {
            tasks: [
              {
                task: 'isUpdate',
                params: {
                  subKey: ['items'],
                  id: [provider_id],
                  key: 'id',
                  values: {
                    [provider_id]: { wishlist_id: false },
                  },
                },
              },
            ],
          });
        }

        if (stateToChange === 'suggestion') {
          DAYCARE_SUGGESTION_API_CUSTOM_TASK(ON_SUCCESS, {
            tasks: [
              {
                task: 'isUpdate',
                params: {
                  subKey: ['search_results'],
                  id: [provider_id],
                  key: 'id',
                  values: {
                    [provider_id]: { wishlist_id: false },
                  },
                },
              },
            ],
          });
        }
      },
      errorCallback: ({ message }) => {
        onWishlistDelError(message);
      },
    });
  };

  const onWishlistLoadMore = skip =>
    GET_USER_WISHLISTED_PRODUCTS_API_CALL({
      query: {
        limit: 3,
        skip,
      },
      subKey: ['wishlists'],
      isInfinite: true,
    });

  const userWishlist = useMemo(
    () => getData(GET_USER_WISHLISTED_PRODUCTS_API, {}, false),
    [GET_USER_WISHLISTED_PRODUCTS_API],
  );

  const schedule = useMemo(
    () => getData(GET_USERS_TOUR_SCHEDULE_API, {}, false),
    [GET_USERS_TOUR_SCHEDULE_API],
  );

  const unschedule = useMemo(
    () => getData(GET_USERS_TOUR_UNSCHEDULE_API, {}, false),
    [GET_USERS_TOUR_UNSCHEDULE_API],
  );

  return {
    wishList: userWishlist.data,
    wishlistLoader: userWishlist.loader,
    postWishlist,
    deleteWishlist,
    onWishlistLoadMore,
  };
};
