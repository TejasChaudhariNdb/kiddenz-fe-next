/* eslint-disable camelcase */
/* eslint-disable eqeqeq */
/* eslint-disable indent */
/* eslint-disable no-restricted-globals */
/* eslint-disable no-plusplus */
import { useEffect, useState, useMemo, useCallback } from 'react';
import findIndex from 'lodash/findIndex';
import moment from 'moment';
import { ON_SUCCESS } from 'shared/utils/commonReduxSagaConverter/commonConstants';

export const useScheduleHook = (
  {
    GET_USERS_TOUR_SCHEDULE_API_CALL,
    GET_USERS_TOUR_SCHEDULE_API_CANCEL,
    GET_USERS_TOUR_UNSCHEDULE_API_CALL,
    GET_USERS_TOUR_SCHEDULE_API_CUSTOM_TASK,
    GET_USERS_TOUR_UNSCHEDULE_API_CANCEL,
    GET_USERS_TOUR_UNSCHEDULE_API_CUSTOM_TASK,
    ADD_USERS_TOUR_SCHEDULE_API_CALL,
    ADD_USERS_TOUR_SCHEDULE_API_CANCEL,
    EDIT_USERS_TOUR_SCHEDULE_API_CALL,
    EDIT_USERS_TOUR_SCHEDULE_API_CANCEL,
    DELETE_USERS_TOUR_SCHEDULE_API_CALL,
    DELETE_USERS_TOUR_SCHEDULE_API_CANCEL,
    SCHEDULE_CHECK_API_CALL,
    SCHEDULE_CHECK_API_CANCEL,
    dashboard: {
      GET_USERS_TOUR_SCHEDULE_API,
      GET_USERS_TOUR_UNSCHEDULE_API,
      ADD_USERS_TOUR_SCHEDULE_API,
      DELETE_USERS_TOUR_SCHEDULE_API,
      EDIT_USERS_TOUR_SCHEDULE_API,
      SCHEDULE_CHECK_API,
      USER_CONNECT_API,
      USER_ENQUIRY_API,
    },
    getData,
  },
  {
    onScheduleAddSuccess = () => {},
    onScheduleAddError = () => {},
    onScheduleEditSuccess = () => {},
    onScheduleEditError = () => {},
    onScheduleDeleteSuccess = () => {},
    onScheduleDeleteError = () => {},
    scheduleCheckCallback = () => {},
  } = {},
) => {
  const [date, setDate] = useState('');
  const [time, setTime] = useState('');
  const [childId, setChildId] = useState([]);
  const [providerId, setProviderId] = useState(null);
  const [error, setError] = useState({});
  const [isEdit, setIsEdit] = useState(false);

  useEffect(
    () => () => {
      ADD_USERS_TOUR_SCHEDULE_API_CANCEL();
      EDIT_USERS_TOUR_SCHEDULE_API_CANCEL();
      DELETE_USERS_TOUR_SCHEDULE_API_CANCEL();
      GET_USERS_TOUR_SCHEDULE_API_CANCEL();
      GET_USERS_TOUR_UNSCHEDULE_API_CANCEL();
      SCHEDULE_CHECK_API_CANCEL();
    },
    [],
  );

  // On Input Change
  const onInputChange = useCallback(
    (key, value) => {
      if (key === 'date') setDate(value);
      if (key === 'time') setTime(value);
    },
    [date, time],
  );

  // On child submit
  const onSubmit = (id, isForceEdit = false, extras = {}) => {
    let errorObj = {};

    const { extraPayload = {}, byPassValication = true } = extras;
    errorObj = !date
      ? {
          ...errorObj,
          ...{
            isError: true,
            date: 'Date is required.',
          },
        }
      : { ...errorObj };

    errorObj = !time
      ? {
          ...errorObj,
          ...{
            isError: true,
            time: 'Time is required.',
          },
        }
      : { ...errorObj };

    errorObj =
      date === extraPayload.old_date &&
      moment(`${date} ${time}`).format('HH:mm') ===
        moment(`${extraPayload.old_date} ${extraPayload.old_time}`).format(
          'HH:mm',
        )
        ? {
            ...errorObj,
            ...{
              isError: true,
              time: 'Choose different time.',
            },
          }
        : { ...errorObj };

    errorObj =
      !isEdit && childId.length === 0
        ? {
            ...errorObj,
            ...{
              isError: true,
              child: 'Please select the child.',
            },
          }
        : { ...errorObj };

    if (!errorObj.isError || byPassValication) {
      const payload = {
        child: [...childId],
        date,
        time,
        provider: providerId,
        schedule_type: 'pending',
        ...(isEdit
          ? {
              schedule_status: 'declined',
            }
          : {}),
        ...extraPayload,
      };

      (isEdit
        ? EDIT_USERS_TOUR_SCHEDULE_API_CALL
        : ADD_USERS_TOUR_SCHEDULE_API_CALL)({
        params: {
          id,
        },
        payload,
        successCallback: ({
          data: {
            data: { data = {}, schedule_count, unschedule_count } = {},
          } = {},
        }) => {
          if (isEdit) {
            onScheduleEditSuccess(data);
          } else {
            onScheduleAddSuccess(data);
          }
          // if the scdhule.length < 3
          // Add the deleted element in schdule

          if (isForceEdit) {
            // GET_USERS_TOUR_UNSCHEDULE_API_CUSTOM_TASK(ON_SUCCESS, {
            //   isDelete: true,
            //   id: [id],
            //   subKey: ['data'],
            //   key: 'id',
            // });
            // onUnscheduleLoadMore(userUnschedule.data.data.length - 1);
          }

          if ((isForceEdit && schedule_count < 3) || isEdit || !isForceEdit) {
            GET_USERS_TOUR_SCHEDULE_API_CUSTOM_TASK(ON_SUCCESS, {
              tasks: [
                {
                  task: isEdit ? 'isUpdate' : 'isInfinite',
                  params: isEdit
                    ? {
                        id: [id],
                        subKey: ['data'],
                        key: 'id',
                        values: {
                          [id]: payload,
                        },
                      }
                    : { isInfinite: true, subKey: ['data'] },
                  value: isEdit
                    ? null
                    : {
                        data: [{ ...data, ...payload }],
                      },
                },
                {
                  task: 'isUpdate',
                  value: {
                    schedule_count,
                    unschedule_count,
                  },
                },
              ],
            });
          }
        },
        errorCallback: ({ message }) => {
          if (isEdit) onScheduleEditError(message);
          else onScheduleAddError(message);
        },
      });
    } else {
      setError(errorObj);
    }
  };

  // On Schedule Load More
  const onScheduleLoadMore = useCallback(
    skip => {
      GET_USERS_TOUR_SCHEDULE_API_CALL({
        query: {
          limit: 3,
          skip,
        },
        subKey: ['data'],
        isInfinite: true,
      });
    },
    [date, time, childId, providerId, error, isEdit],
  );

  // On Un-Schedule Load More
  const onUnscheduleLoadMore = useCallback(
    skip => {
      GET_USERS_TOUR_UNSCHEDULE_API_CALL({
        query: {
          limit: 3,
          skip,
        },
        subKey: ['data'],
        isInfinite: true,
      });
    },
    [date, time, childId, providerId, error, isEdit],
  );

  // Child Delete handler
  const onScheduleCancel = ({ id, reason }) => {
    const payload = {
      id,
      cancel_reason: reason,
      schedule_type: 'unschedule',
      schedule_status: 'declined',
    };
    const index = findIndex(userSchedule.data.data, o => o.id == id);
    const deletedObj = userSchedule.data.data[index];
    DELETE_USERS_TOUR_SCHEDULE_API_CALL({
      payload,
      successCallback: ({
        data: { data: { unschedule_count, schedule_count } = {} } = {},
      }) => {
        onScheduleDeleteSuccess();

        GET_USERS_TOUR_SCHEDULE_API_CUSTOM_TASK(ON_SUCCESS, {
          tasks: [
            {
              task: 'isDelete',
              params: {
                isDelete: true,
                id: [id],
                subKey: ['data'],
                key: 'id',
              },
            },
            {
              task: 'isUpdate',
              value: {
                schedule_count,
                unschedule_count,
              },
            },
          ],
        });

        onScheduleLoadMore(userSchedule.data.data.length - 1);

        // if the unscdhule.length < 3
        // Add the deleted element in unschdule
        if (unschedule_count < 3) {
          GET_USERS_TOUR_UNSCHEDULE_API_CUSTOM_TASK(
            ON_SUCCESS,
            {
              isInfinite: true,
              subKey: ['data'],
            },
            {
              data: { data: [deletedObj], unschedule_count, schedule_count },
            },
          );
        } else {
          const {
            data: {
              data: unScheduledTours = [],
              unschedule_count: unScheduleCount = 0,
            } = {},
          } = userUnschedule;
          if (unScheduledTours.length === unScheduleCount) {
            GET_USERS_TOUR_UNSCHEDULE_API_CUSTOM_TASK(
              ON_SUCCESS,
              {
                isInfinite: true,
                subKey: ['data'],
              },
              {
                data: {
                  data: [deletedObj],
                },
              },
            );
          }
        }
      },
      errorCallback: ({ message }) => {
        onScheduleDeleteError(message);
      },
    });
  };

  // Reset Form
  const reset = () => {
    setDate('');
    setTime('');
    setIsEdit(false);
  };

  const addChildToSchedule = id => {
    const tempChildArr = [...childId];
    if (!childId.includes(id)) {
      tempChildArr.push(id);
    } else {
      const index = tempChildArr.indexOf(id);
      if (index > -1) {
        tempChildArr.splice(index, 1);
      }
    }
    setChildId(tempChildArr);
  };

  const checkTourExists = id => {
    SCHEDULE_CHECK_API_CALL({
      payload: {
        provider_id: id,
      },
      successCallback: ({ data }) => {
        scheduleCheckCallback({ data, provId: id });
      },
    });
  };

  // <****************DATA RETURN********************>
  const userSchedule = useMemo(
    () => getData(GET_USERS_TOUR_SCHEDULE_API, {}, false),
    [GET_USERS_TOUR_SCHEDULE_API],
  );

  const userUnschedule = useMemo(
    () => getData(GET_USERS_TOUR_UNSCHEDULE_API, {}, false),
    [GET_USERS_TOUR_UNSCHEDULE_API],
  );

  const addSchedule = useMemo(
    () => getData(ADD_USERS_TOUR_SCHEDULE_API, [], false),
    [ADD_USERS_TOUR_SCHEDULE_API],
  );

  const editSchedule = useMemo(
    () => getData(EDIT_USERS_TOUR_SCHEDULE_API, [], false),
    [EDIT_USERS_TOUR_SCHEDULE_API],
  );

  const deleteSchedule = useMemo(
    () => getData(DELETE_USERS_TOUR_SCHEDULE_API, [], false),
    [DELETE_USERS_TOUR_SCHEDULE_API],
  );

  const scheduleCheck = useMemo(() => getData(SCHEDULE_CHECK_API, {}, false), [
    SCHEDULE_CHECK_API,
  ]);

  const userConnect = useMemo(() => getData(USER_CONNECT_API, {}, false), [
    USER_CONNECT_API,
  ]);

  const userEnquiry = useMemo(() => getData(USER_ENQUIRY_API, {}, false), [
    USER_ENQUIRY_API,
  ]);

  // on edit Click pre populate data from store
  const onEditClick = id => {
    const index = findIndex(userSchedule.data.data, o => o.id == id);
    const {
      date: editDate,
      time: editTime,
      // ...tempScheduleData
    } = userSchedule.data.data[index];
    setIsEdit(true);
    setDate(editDate);
    setTime(editTime);
  };

  return {
    schedule: userSchedule.data,
    onScheduleLoadMore,
    onUnscheduleLoadMore,
    unSchedule: userUnschedule.data,
    scheduleLoader: userSchedule.loader,
    unScheduleLoader: userUnschedule.loader,
    addScheduleLoader: addSchedule.loader,
    onChange: onInputChange,
    resetForm: reset,
    onScheduleSubmit: onSubmit,
    onRescheduleClick: onEditClick,
    addChildToSchedule,
    selectedChildren: childId,
    setProviderId,
    providerId,
    date,
    time,
    onScheduleCancel,
    scheduleDeleteLoader: deleteSchedule.loader,
    scheduleEditLoader: editSchedule.loader,
    userConnectLoader: userConnect.loader,
    userEnquiryLoader: userEnquiry.loader,
    checkTourExists,
    scheduleCheckLoader: scheduleCheck.loader,
    scheduleCheckData: scheduleCheck.data,
    error,
  };
};
