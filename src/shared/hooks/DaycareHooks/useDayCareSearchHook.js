/* eslint-disable no-nested-ternary */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable eqeqeq */
/* eslint-disable no-lonely-if */
/* eslint-disable no-plusplus */
/* eslint-disable no-param-reassign */
/* eslint-disable indent */
// useReviewHooks

import { useEffect, useState, useCallback, useMemo } from 'react';
import validateForm from '../../utils/formValidation';
import { removeNullValuesFromObject } from '../../utils/commonHelpers/miscHelpers';
import markerIcon from '../../../images/Path Copy.svg';
import activeLocationMarker from '../../../images/activeLocationMarker.png';

const SCHEDULE_CONFIG = { 'full time': 1, weekend: 3, flexible: 2 };

const FILTERS_INITIAL = {
  organisation: null,
  transportation: null,
  food: null,
  course_type: null,
  // min_age: null,
  max_age: null,
  min_price: null,
  max_price: null,
  daycareprice: null,
  daycare_min_price: null,
  daycare_max_price: null,
  schedule_type: [],
  cctv: null,
  ac_room: null,
  live_streaming: null,
  free_trial: null,
  outdoor_play: null,
  parent_app: null,
  massaging_bathing: null,
  extended_hours: null,
  special_care: null,
  activities: null,
  tution: null,
  min_extended_hours: null,
  max_extended_hours: null,
};

export const useDayCareSearchHook = ({
  DAYCARE_SEARCH_API_CALL,
  DAYCARE_SEARCH_API_CANCEL,
  // SUBMIT_CONTACT_API_CALL,
  SUBMIT_CONTACT_API_CANCEL,
  DAYCARE_SUGGESTION_API_CALL,
  DAYCARE_SUGGESTION_API_CANCEL,
  DAYCARE_TRIP_SEARCH_CORDS_API_CALL,
  DAYCARE_TRIP_SEARCH_CORDS_API_CANCEL,
  DAYCARE_TRIP_SEARCH_RESULTS_API_CALL,
  DAYCARE_TRIP_SEARCH_RESULTS_API_CANCEL,
  query: {
    locationString,
    search_type: searchType,
    toLocationString,
    fromLocationString,
    ...queryFilters
  } = {},
  dashboard: {
    DAYCARE_SEARCH_API,
    SUBMIT_CONTACT_API,
    DAYCARE_SUGGESTION_API,
    DAYCARE_TRIP_SEARCH_CORDS_API,
    DAYCARE_TRIP_SEARCH_RESULTS_API,
  },
  getData,
}) =>
  // { onSuccess, onError },
  {
    const [newCenter, setNewCenter] = useState({});

    const [locationSearchSting, setLocationSearchSting] = useState('');
    const [toSearchSting, setToSearchSting] = useState('');
    const [fromSearchSting, setFromSearchSting] = useState('');

    const [latitude, setLatitude] = useState(0.0);
    const [longitude, setLongitude] = useState(0.0);

    const [fromLatitude, setFromLatitude] = useState(0.0);
    const [fromLongitude, setFromLongitude] = useState(0.0);
    const [toLatitude, setToLatitude] = useState(0.0);
    const [toLongitude, setToLongitude] = useState(0.0);
    const [tripHrs, setTripHrs] = useState('00');
    const [tripMins, setTripMins] = useState('00');

    const [northEastLat, setNorthEastLat] = useState(0.0);
    const [northEastLng, setNorthEastLng] = useState(0.0);

    const [SouthWesttLat, setSouthWesttLat] = useState(0.0);
    const [SouthWesttLng, setSouthWesttLng] = useState(0.0);

    const [type, setType] = useState('radius');
    const [locationSearchMethod, setLocationSearchMethod] = useState(
      'location',
    );
    const [sortType, setSortType] = useState('distance');
    const [filters, setFilters] = useState(FILTERS_INITIAL);

    const [phone, setPhone] = useState(null);
    const [email, setEmail] = useState(null);
    const [error, setError] = useState({});

    const [isDragToSearchInitiated, setDragToSearchInitiated] = useState(false);
    const [isLoadMoreTriggered, setIsLoadMoreTriggered] = useState(false);
    const [
      mobileViewCurrentSchoolSlug,
      setMobileViewCurrentSchoolSlug,
    ] = useState('');
    const [hoveredState, setHoveredState] = useState('');
    const [mobileView, setMobileView] = useState(false);

    const VALIDATOR_CONFIG = {
      phone: { type: 'mobile', value: phone },
      email: { type: 'email', value: email },
    };

    useEffect(() => {
      const {
        type: searchMethod,
        latitude: queryLat,
        longitude: queryLng,
        sort_by: sortBy,
        fromLat,
        fromLng,
        timehrs,
        timemins,
        toLat,
        toLng,
        ...filtersFromQuery
      } = queryFilters;

      const tempFilters = { ...filters };
      setFilters({
        ...tempFilters,
        ...filtersFromQuery,
        // eslint-disable-next-line consistent-return
        schedule_type: (() => {
          if (
            filtersFromQuery.schedule_type &&
            filtersFromQuery.schedule_type.length > 0
          )
            return Array.isArray(filtersFromQuery.schedule_type)
              ? filtersFromQuery.schedule_type.map(d => +d)
              : [+filtersFromQuery.schedule_type];
        })(),
      });
    }, []);

    useEffect(
      () => () => {
        DAYCARE_SEARCH_API_CANCEL();
        SUBMIT_CONTACT_API_CANCEL();
        DAYCARE_SUGGESTION_API_CANCEL();
        DAYCARE_TRIP_SEARCH_CORDS_API_CANCEL();
        DAYCARE_TRIP_SEARCH_RESULTS_API_CANCEL();
      },
      [],
    );

    // Search Handler
    const searchHandler = useCallback(
      (
        meta = {},
        skip,
        isGetNew = false,
        appliesFilters = {},
        isDragSearch = false,
      ) => {
        setMobileViewCurrentSchoolSlug('');
        const { searchBy, calledFrom = 'null' } = meta;

        const {
          extended_hours,
          min_extended_hours,
          max_extended_hours,
          daycareprice,
          daycare_min_price,
          daycare_max_price,
          transportation,
          course_type,
          organisation,
          food,
          ...appliedFilters
        } = appliesFilters;
        if (searchBy === 'trip') {
          DAYCARE_TRIP_SEARCH_RESULTS_API_CALL({
            query: {
              source: [fromLatitude, fromLongitude],
              destination: [toLatitude, toLongitude],
              time: [tripHrs, tripMins],
              type,
              page: skip,
              items: 6,
              ...appliedFilters,

              // if organisation type is 3/Both send daycare price seperately
              ...(daycareprice == 1
                ? {
                    daycare_min_price: daycare_min_price || 500,
                    daycare_max_price: daycare_max_price || 20000,
                    min_price: appliedFilters.min_price
                      ? appliedFilters.min_price
                      : 500,
                    max_price: appliedFilters.max_price
                      ? appliedFilters.max_price
                      : 300000,
                  }
                : {}),

              ...(extended_hours == 1
                ? {
                    min_extended_hours: min_extended_hours || 0,
                    max_extended_hours: max_extended_hours || 24,
                  }
                : {}),

              ...(transportation == 1
                ? {
                    transportation: 1,
                  }
                : {}),

              ...(food == 1
                ? {
                    food: 1,
                  }
                : {}),

              ...(course_type == 1
                ? {
                    course_type: 'montessori',
                  }
                : {}),
              ...(organisation == 3 ? {} : { organisation }),
              sort_by: sortType,
            },
            paramsSerializer: { arrayFormat: 'comma' },
            ...(isGetNew ? {} : { subKey: ['items'] }),
            isInfinite: !isGetNew,
            isUpdate: isGetNew,
            updateCallback: (a = {}, b = {}) =>
              isGetNew
                ? b
                : {
                    ...a,
                    ...b,
                    items: (a.items || []).concat(b.items),
                  },
            successCallback: () => {
              setIsLoadMoreTriggered(false);
            },
            errorCallback: () => {},
          });
          if (calledFrom === 'applyFilters' || calledFrom === 'header') {
            DAYCARE_TRIP_SEARCH_CORDS_API_CALL({
              query: {
                source: [fromLatitude, fromLongitude],
                destination: [toLatitude, toLongitude],
                time: [tripHrs, tripMins],
                type,
                ...appliedFilters,
                ...(daycareprice == 1
                  ? {
                      daycare_min_price: daycare_min_price || 500,
                      daycare_max_price: daycare_max_price || 20000,
                      min_price: appliedFilters.min_price
                        ? appliedFilters.min_price
                        : 500,
                      max_price: appliedFilters.max_price
                        ? appliedFilters.max_price
                        : 300000,
                    }
                  : {}),

                ...(extended_hours == 1
                  ? {
                      min_extended_hours: min_extended_hours || 0,
                      max_extended_hours: max_extended_hours || 24,
                    }
                  : {}),

                ...(transportation == 1
                  ? {
                      transportation: 1,
                    }
                  : {}),

                ...(food == 1
                  ? {
                      food: 1,
                    }
                  : {}),

                ...(course_type == 1
                  ? {
                      course_type: 'montessori',
                    }
                  : {}),
                ...(organisation == 3 ? {} : { organisation }),
                sort_by: sortType,
              },
              paramsSerializer: { arrayFormat: 'comma' },
              successCallback: () => {
                // console.log('hi');
              },
            });
          }
        } else
          DAYCARE_SEARCH_API_CALL({
            query: {
              ...(!isDragSearch
                ? {
                    latitude,
                    longitude,
                  }
                : {}),
              type,
              limit: 6,
              skip,
              ...appliedFilters,
              ...(daycareprice == 1
                ? {
                    daycare_min_price: daycare_min_price || 500,
                    daycare_max_price: daycare_max_price || 20000,
                    min_price: appliedFilters.min_price
                      ? appliedFilters.min_price
                      : 500,
                    max_price: appliedFilters.max_price
                      ? appliedFilters.max_price
                      : 300000,
                  }
                : {}),

              ...(transportation == 1
                ? {
                    transportation: 1,
                  }
                : {}),

              ...(extended_hours == 1
                ? {
                    min_extended_hours: min_extended_hours || 0,
                    max_extended_hours: max_extended_hours || 24,
                  }
                : {}),

              ...(food == 1
                ? {
                    food: 1,
                  }
                : {}),

              ...(course_type == 1
                ? {
                    course_type: 'montessori',
                  }
                : {}),
              ...(organisation == 3 ? {} : { organisation }),
              sort_by: sortType,
            },
            ...(isGetNew ? {} : { subKey: ['search_results'] }),
            isInfinite: !isGetNew,
            isUpdate: isGetNew,
            updateCallback: (a = {}, b = {}) =>
              isGetNew
                ? b
                : {
                    ...a,
                    ...b,
                    search_results: (a.search_results || []).concat(
                      b.search_results,
                    ),
                  },
            successCallback: () => {
              setIsLoadMoreTriggered(false);
            },
            errorCallback: () => {},
          });
      },
      [
        latitude,
        longitude,
        filters,
        fromLatitude,
        fromLongitude,
        toLatitude,
        toLongitude,
        tripHrs,
        tripMins,
      ],
    );

    // On Search Load More
    const onSearchLoadMore = useCallback(
      (skip, searchBy) => {
        setIsLoadMoreTriggered(true);
        const tempFilterObj = { ...filters };
        searchHandler(
          { searchBy },
          skip,
          false,
          {
            // ...(type === 'radius' && !isDragToSearchInitiated
            ...(!isDragToSearchInitiated
              ? { latitude, longitude }
              : {
                  ne_lat: northEastLat,
                  ne_lng: northEastLng,
                  sw_lat: SouthWesttLat,
                  sw_lng: SouthWesttLng,
                }),
            skip,
            type: !isDragToSearchInitiated ? 'radius' : 'bounds',
            ...removeNullValuesFromObject(tempFilterObj),
          },
          type === 'bounds' && isDragToSearchInitiated,
        );
      },
      [
        latitude,
        longitude,
        filters,
        type,
        northEastLat,
        northEastLng,
        SouthWesttLat,
        SouthWesttLng,
      ],
    );

    // Apply Filters
    const onApplyFilter = useCallback(
      (e, addOnfilters = {}, searchBy) => {
        const tempFilterObj = { ...filters, ...addOnfilters };
        searchHandler(
          { searchBy, calledFrom: 'applyFilters' },
          searchBy === 'trip' ? 1 : 0,
          true,
          {
            ...(!isDragToSearchInitiated
              ? { latitude, longitude }
              : {
                  ne_lat: northEastLat,
                  ne_lng: northEastLng,
                  sw_lat: SouthWesttLat,
                  sw_lng: SouthWesttLng,
                }),
            type: !isDragToSearchInitiated ? 'radius' : 'bounds',
            ...removeNullValuesFromObject(tempFilterObj),
          },
          type === 'bounds' && isDragToSearchInitiated,
        );

        const tempFilterState = {};
        Object.keys(FILTERS_INITIAL).map(d => {
          tempFilterState[d] = tempFilterObj[d] || null;
          return 0;
        });
        setFilters(tempFilterState);
      },
      [
        latitude,
        longitude,
        filters,
        isDragToSearchInitiated,
        type,
        northEastLat,
        northEastLng,
        SouthWesttLat,
        SouthWesttLng,
        fromLatitude,
        fromLongitude,
        toLatitude,
        toLongitude,
        tripHrs,
        tripMins,
      ],
    );

    // Submit Contact
    const submitContactHandler = e => {
      e.preventDefault();
      const errorObj = validateForm({
        phone: { type: 'mobile', value: phone },
        email: { type: 'email', value: email },
      });
      if (!errorObj.isError) {
        // const payload = {
        //   mobile_number: phone,
        //   email,
        // };
        // SUBMIT_CONTACT_API_CALL({
        //   payload,
        //   errorCallback: ({ errorData, message }) => {
        //     onError({ errorData, message });
        //   },
        //   successCallback: ({ res, data, message, status }) => {
        //     onSuccess({ res, data, message, status });
        //   },
        // });
      } else {
        setError(errorObj);
      }
    };

    // on input chnage
    const onChange = useCallback(
      (e, key) => {
        e.preventDefault();
        const value = getPlatformBasedFieldValue(e);
        if (key === 'email') setEmail(value ? value.replace(/\s*$/, '') : '');
        else {
          // eslint-disable-next-line no-lonely-if
          if ((Number(value) === 0 || Number(value)) && value.length <= 10)
            setPhone(value);
        }
      },
      [phone, email],
    );

    // on input blur
    const onBlur = useCallback(
      (e, key) => {
        e.preventDefault();
        const errorObj = validateForm({
          [key]: {
            type: VALIDATOR_CONFIG[key].type,
            value: VALIDATOR_CONFIG[key].value,
          },
        });
        if (errorObj.isError) {
          setError({ ...error, [key]: errorObj[key] });
        } else {
          setError({ ...error, [key]: null });
        }
      },
      [phone, email],
    );

    // Load Mpre Suggestions
    const loadMoreSuggestions = useCallback(
      skip => {
        DAYCARE_SUGGESTION_API_CALL({
          query: {
            ...filters,
            limit: 3,
            skip,
          },
          subKey: ['search_results'],
          isInfinite: true,
        });
      },
      [latitude, longitude],
    );

    // Search as bounds change
    const searchAsBoundsChage = useCallback(
      ({ neLat, neLng, seLat, seLng }, searchBy) => {
        if (!isDragToSearchInitiated) {
          setDragToSearchInitiated(true);
        }

        setNorthEastLat(neLat);
        setNorthEastLng(neLng);
        setSouthWesttLat(seLat);
        setSouthWesttLng(seLng);

        const tempFilterObj = { ...filters };
        searchHandler(
          { searchBy, calledFrom: 'boundsChanged' },
          0,
          true,
          {
            ne_lat: neLat,
            ne_lng: neLng,
            sw_lat: seLat,
            sw_lng: seLng,
            type: 'bounds',
            ...removeNullValuesFromObject(tempFilterObj),
          },
          true,
        );
      },
      [
        latitude,
        longitude,
        filters,
        type,
        northEastLat,
        northEastLng,
        SouthWesttLat,
        SouthWesttLng,
      ],
    );

    // Handerler to modify filter state form component
    const modifyFilter = modObj => {
      const tempFilters = { ...filters };
      setFilters({ ...tempFilters, ...modObj });
    };

    // <****************DATA RETURN********************>

    const search = useMemo(() => getData(DAYCARE_SEARCH_API, {}, false), [
      DAYCARE_SEARCH_API,
    ]);

    const tripSearchCords = useMemo(
      () => getData(DAYCARE_TRIP_SEARCH_CORDS_API, {}, false),
      [DAYCARE_TRIP_SEARCH_CORDS_API],
    );

    const tripSearchResults = useMemo(
      () => getData(DAYCARE_TRIP_SEARCH_RESULTS_API, {}, false),
      [DAYCARE_TRIP_SEARCH_RESULTS_API],
    );

    const submitContact = useMemo(
      () => getData(SUBMIT_CONTACT_API, [], false),
      [SUBMIT_CONTACT_API],
    );

    const dayCareSuggestions = useMemo(
      () => getData(DAYCARE_SUGGESTION_API, [], false),
      [DAYCARE_SUGGESTION_API],
    );

    const { data: { coordinates = [] } = {} } = search;
    const {
      data: { coordinates: tripSearchResultCords = [] } = {},
    } = tripSearchCords;

    const {
      data: { items = [], max_page, next_page, previous_page } = {},
    } = tripSearchResults;

    return {
      isDragToSearchInitiated,
      newCenter,
      setNewCenter,
      locationSearchSting,
      setLocationSearchSting,
      toSearchSting,
      setToSearchSting,
      fromSearchSting,
      setFromSearchSting,
      locationLat: latitude,
      setLocationLat: setLatitude,
      locationLng: longitude,
      setLocationLng: setLongitude,
      searchAsBoundsChage,
      search: searchHandler,
      onSearchLoadMore,
      isLoadMoreTriggered,
      searchData:
        locationSearchMethod === 'trip'
          ? {
              search_results: items,
              count: tripSearchResultCords.length,
            }
          : search.data,
      searchCoordinates: (locationSearchMethod === 'trip'
        ? tripSearchResultCords
        : coordinates
      ).map(d => ({
        lat: +d.lattitude,
        lng: +d.longitude,
        id: d.id,
        slug: d.slug,
        address: d.location_address,
        name: d.business_name,
        city: d.city,
        state: d.state,
        area: d.area,
        pincode: d.pincode,
        img: d.url,
        marker:
          mobileView && d.slug === mobileViewCurrentSchoolSlug
            ? activeLocationMarker
            : d.slug === hoveredState
              ? activeLocationMarker
              : markerIcon,
      })),
      slugToSearchCoordinatesMap: (() => {
        const mappedObj = {};
        (locationSearchMethod === 'trip'
          ? tripSearchResultCords
          : coordinates
        ).map(({ slug, lattitude: cordLat, longitude: cordLng }) => {
          mappedObj[slug] = {
            lat: +cordLat,
            lng: +cordLng,
          };
          return 0;
        });
        return mappedObj;
      })(),
      searchLoader:
        locationSearchMethod === 'trip'
          ? tripSearchResults.loader
          : search.loader,
      searchType: type,
      setSearchType: setType,
      locationSearchMethod,
      setLocationSearchMethod,
      appliedFilters: filters,
      modifyFilter,
      onApplyFilter,
      sortType,
      setSortType,
      setIsMapMoved: setDragToSearchInitiated,
      setToLatitude,
      setToLongitude,
      setFromLatitude,
      setFromLongitude,
      setTripHrs,
      setTripMins,
      mobileViewCurrentSchoolSlug,
      setMobileViewCurrentSchoolSlug,
      setHoveredState,
      hoveredState,
      mobileView,
      setMobileView,
      fromLatitude,
      fromLongitude,
      toLatitude,
      toLongitude,
      tripHrs,
      tripMins,
      next_page,
      max_page,
      notFound: {
        phone,
        email,
        onNotFoundInputsChangeHandler: onChange,
        onNotFoundInputsBlurHandler: onBlur,
        submitContactHandler,
        error,
        submitContactLoader: submitContact.loader,
        dayCareSuggestions: dayCareSuggestions.data,
        dayCareSuggestionsLoader: dayCareSuggestions.loader,
        loadMoreSuggestions,
      },
    };
  };

// Helpers
function getPlatformBasedFieldValue(e) {
  return typeof e === 'object' ? e.target.value : e;
}
