/* eslint-disable react-hooks/rules-of-hooks */
import { useEffect, useMemo } from 'react';
import // ON_UNMOUNT,
// ON_SUCCESS,
'../../utils/commonReduxSagaConverter/commonConstants';

export const useDayCareDetailHook = ({
  getData,
  //   PROVIDER_MEDIA_API_CALL,
  //   PROVIDER_BASIC_INFO_API_CALL,
  //   PROVIDER_FACULTY_API_CALL,
  //   PROVIDER_LOCATION_API_CALL,
  //   PROVIDER_FACILITY_API_CALL,
  //   PROVIDER_DIRECTOR_INFO_API_CALL,
  //   PROVIDER_CONTACT_DETAILS_API_CALL,
  //   PROVIDER_LANGUAGES_API_CALL,
  //   PROVIDER_YEARLY_CALENDER_API_CALL,
  //   PROVIDER_SCHEDULE_FOR_DAY_API_CALL,
  //   PROVIDER_WORKING_DAY_API_CALL,
  //   PROVIDER_EMERGENCY_SERVICES_API_CALL,
  //   PROVIDER_SCHEDULE_CHOICE_API_CALL,
  // PROVIDER_YEARLY_CALENDER_API_CANCEL,
  // PROVIDER_EMERGENCY_SERVICES_API_CANCEL,
  // GET_USERS_CHILDREN_API_CANCEL,
  // GET_USERS_TOUR_SCHEDULE_API_CANCEL,
  // PROVIDER_LANGUAGES_API_CANCEL,
  // PROVIDER_CONTACT_DETAILS_API_CANCEL,
  // PROVIDER_FACILITY_API_CANCEL,
  // PROVIDER_FACULTY_API_CANCEL,
  PROVIDER_CONTACT_DETAILS_API_CALL,
  PROVIDER_FACULTY_API_CALL,
  PROVIDER_FACILITY_API_CALL,
  PROVIDER_LANGUAGES_API_CALL,
  PROVIDER_YEARLY_CALENDER_API_CALL,
  PROVIDER_EMERGENCY_SERVICES_API_CALL,
  GET_USERS_CHILDREN_API_CALL,
  GET_USERS_TOUR_SCHEDULE_API_CALL,
  dashboard: {
    PROVIDER_MEDIA_API,
    PROVIDER_BASIC_INFO_API,
    PROVIDER_FACULTY_API,
    PROVIDER_LOCATION_API,
    PROVIDER_EMERGENCY_SERVICES_API,
    PROVIDER_FACILITY_API,
    PROVIDER_DIRECTOR_INFO_API,
    PROVIDER_CONTACT_DETAILS_API,
    PROVIDER_LANGUAGES_API,
    PROVIDER_YEARLY_CALENDER_API,
    PROVIDER_SCHEDULE_FOR_DAY_API,
    PROVIDER_WORKING_DAY_API,
    PROVIDER_SCHEDULE_CHOICE_API,
    PROVIDER_CONTENT_SCHEDULE_API,
  } = {},
  query: { 'daycare-id': id } = {},
}) => {
  useEffect(
    () => {
      PROVIDER_CONTACT_DETAILS_API_CALL({ params: { id } });
      PROVIDER_FACULTY_API_CALL({ params: { id } });
      PROVIDER_FACILITY_API_CALL({
        params: { id },
        successCallback: () => {},
      });
      PROVIDER_LANGUAGES_API_CALL({ params: {} });
      PROVIDER_YEARLY_CALENDER_API_CALL({ params: { id } });
      // PROVIDER_CONTENT_SCHEDULE_API_CALL({ params: { id } });
      PROVIDER_EMERGENCY_SERVICES_API_CALL({ params: {} });
      GET_USERS_CHILDREN_API_CALL();
      GET_USERS_TOUR_SCHEDULE_API_CALL({
        query: {
          limit: 3,
          skip: 0,
        },
      });
    },
    [id],
  );

  const location = useMemo(() => getData(PROVIDER_LOCATION_API, []), [
    PROVIDER_LOCATION_API,
  ]);

  const provider = useMemo(() => getData(PROVIDER_DIRECTOR_INFO_API, {}), [
    PROVIDER_DIRECTOR_INFO_API,
  ]);

  const yearlyCalender = useMemo(
    () => getData(PROVIDER_YEARLY_CALENDER_API, []),
    [PROVIDER_YEARLY_CALENDER_API],
  );

  const scheduleForDay = useMemo(
    () => getData(PROVIDER_SCHEDULE_FOR_DAY_API, []),
    [PROVIDER_SCHEDULE_FOR_DAY_API],
  );

  const workingDay = useMemo(() => getData(PROVIDER_WORKING_DAY_API, []), [
    PROVIDER_WORKING_DAY_API,
  ]);

  const facilities = useMemo(() => getData(PROVIDER_FACILITY_API, []), [
    PROVIDER_FACILITY_API,
  ]);

  const faculty = useMemo(() => getData(PROVIDER_FACULTY_API, []), [
    PROVIDER_FACULTY_API,
  ]);

  const language = useMemo(() => getData(PROVIDER_LANGUAGES_API, []), [
    PROVIDER_LANGUAGES_API,
  ]);

  const contact = useMemo(() => getData(PROVIDER_CONTACT_DETAILS_API, []), [
    PROVIDER_CONTACT_DETAILS_API,
  ]);

  const media = useMemo(() => getData(PROVIDER_MEDIA_API, []), [
    PROVIDER_MEDIA_API,
  ]);

  const basicInfo = useMemo(() => getData(PROVIDER_BASIC_INFO_API, []), [
    PROVIDER_BASIC_INFO_API,
  ]);

  const emergecyServices = useMemo(
    () => getData(PROVIDER_EMERGENCY_SERVICES_API, []),
    [PROVIDER_EMERGENCY_SERVICES_API],
  );

  const scheduleChoice = useMemo(
    () => getData(PROVIDER_SCHEDULE_CHOICE_API, []),
    [PROVIDER_SCHEDULE_CHOICE_API],
  );

  const contentSchedule = useMemo(
    () => getData(PROVIDER_CONTENT_SCHEDULE_API, {}),
    [PROVIDER_CONTENT_SCHEDULE_API],
  );

  return {
    location,
    provider: {
      data: provider.data,
      loader: provider.loader,
    },
    yearlyCalender: {
      data: yearlyCalender.data,
      loader: yearlyCalender.loader,
    },
    scheduleForDay: {
      data: scheduleForDay.data,
      loader: scheduleForDay.loader,
    },
    workingDay: {
      data: workingDay.data,
      loader: workingDay.loader,
    },
    facilities: {
      data: facilities.data,
      loader: facilities.loader,
    },
    faculty: {
      data: faculty.data,
      loader: faculty.loader,
    },
    language: {
      data: language.data,
      loader: language.loader,
    },
    contact: {
      data: contact.data,
      loader: contact.loader,
    },
    media: {
      data: media.data,
      loader: media.loader,
    },
    basicInfo: {
      data: basicInfo.data,
      loader: basicInfo.loader,
    },
    emergecyServices: {
      data: emergecyServices.data,
      loader: emergecyServices.loader,
    },
    scheduleChoice: {
      data: scheduleChoice.data,
      loader: scheduleChoice.loader,
    },
    contentSchedule: {
      data: contentSchedule.data,
      loader: contentSchedule.loader,
    },
  };
};
