// useArticleHook

import { useEffect, useMemo } from 'react';
import randomHSL from 'utils/colorGen';
import { ON_SUCCESS } from 'shared/utils/commonReduxSagaConverter/commonConstants';
import uniq from 'lodash/uniq';
import flatten from 'lodash/flatten';

export const useArticleDetail = ({
  getData,
  dispatch,
  GET_ARTICLE_LIKE_VIEW_COUNT_API_CALL,
  ADD_ARTICLE_VIEW_API_CALL,
  GET_ARTICLES_BY_ID_API_CUSTOM_TASK,
  dashboard: {
    GET_ARTICLES_BY_ID_API,
    ARTICLES_GET_ALL_CATEGORIES_API,
    GET_USER_IP_API,
  } = {},
  authentication: { categoryColors = {} } = {},
  query: { 'article-name': articleId } = {},
} = {}) => {
  useEffect(
    () => {
      if (
        categoryItems.length > 0 &&
        Object.keys(categoryColors).length === 0
      ) {
        const colors = {};
        const config = {};
        if (categoryItems.length > 0) {
          categoryItems.map(
            // eslint-disable-next-line no-return-assign
            ({ name, slug, term_id: id }) => {
              colors[name] = randomHSL();
              config[name] = {};
              config[name].id = id;
              config[name].slugName = slug;
              return 0;
            },
          );

          dispatch({
            type: 'CATEGORY_COLORS_UPDATE',
            payload: { colors, config },
          });
        }
      }
    },
    [categoryItems],
  );

  useEffect(
    () => {
      const data = relatedArticles;

      const postIds = [];
      const postKeys = [];
      if (Object.keys(data).length > 0) {
        // eslint-disable-next-line no-unused-vars
        Object.entries(data).map(([key, value]) => {
          if (value && value.length > 0)
            value.map(d => postIds.push(d.post_id));
          postKeys.push(key);
          return 0;
        });

        uniq(postIds).forEach(d => {
          GET_ARTICLE_LIKE_VIEW_COUNT_API_CALL({
            query: { article_id: d },
            successCallback: ({
              res: {
                data: {
                  data: { like_count: likeCount, view_count: viewCount } = {},
                } = {},
              } = {},
            }) => {
              GET_ARTICLES_BY_ID_API_CUSTOM_TASK(ON_SUCCESS, {
                tasks: [
                  ...(() =>
                    postKeys.map(x => ({
                      task: 'isUpdate',
                      params: {
                        subKey: ['related_articles', x],
                        id: [d],
                        key: 'post_id',
                        values: {
                          [d]: { viewCount, likeCount },
                        },
                      },
                    })))(),
                ],
              });
            },
          });
        });
      }
    },
    [relatedArticles],
  );

  useEffect(
    () => {
      ADD_ARTICLE_VIEW_API_CALL({
        payload: {
          article_id: articleId,
          ip_address: userIp,
        },
        successCallback: ({ data: { data: { count: viewCount } = {} } = {} }) =>
          GET_ARTICLES_BY_ID_API_CUSTOM_TASK(ON_SUCCESS, {
            tasks: [
              {
                task: 'isUpdate',
                params: {
                  subKey: ['data'],
                },
                value: { data: { viewCount } },
              },
            ],
          }),
      });
    },
    [userIp],
  );

  const articleDetail = useMemo(
    () => getData(GET_ARTICLES_BY_ID_API, {}, false),
    [GET_ARTICLES_BY_ID_API],
  );

  const categories = useMemo(
    () =>
      getData(ARTICLES_GET_ALL_CATEGORIES_API, {}, false, ['detailMainCat']),
    [ARTICLES_GET_ALL_CATEGORIES_API],
  );

  const { data: userIp } = useMemo(() => getData(GET_USER_IP_API, '', false), [
    GET_USER_IP_API,
  ]);

  const { data: { data: categoryItems = [] } = {} } = categories;
  const {
    data: { related_articles: relatedArticles = [] } = {},
  } = articleDetail;

  return {
    articleDetail,
    suggestions: (() => {
      const keys = Object.keys(relatedArticles) || [];
      const mappedKeys = keys.map(d => relatedArticles[d].map(x => x)) || [];
      if (mappedKeys.length > 0) {
        return flatten(mappedKeys).slice(0, 3);
      }
      return [];
    })(),
  };
};
