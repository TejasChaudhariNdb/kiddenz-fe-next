// useArticleHook

import { useEffect, useMemo } from 'react';
import uniq from 'lodash/uniq';
import { ON_SUCCESS } from 'shared/utils/commonReduxSagaConverter/commonConstants';
import randomHSL from 'utils/colorGen';

export const useSearchHooks = (
  {
    getData,
    dispatch,
    ARTICLES_SEARCH_API_CUSTOM_TASK,
    GET_HASHTAG_ARTICLES_API_CUSTOM_TASK,
    GET_ARTICLE_LIKE_VIEW_COUNT_API_CALL,
    dashboard: {
      ARTICLES_SEARCH_API,
      ARTICLES_GET_ALL_CATEGORIES_API,
      GET_HASHTAG_ARTICLES_API,
    } = {},
    authentication: { categoryColors } = {},
  } = {},
  { containsHash = false, hash = '' },
) => {
  useEffect(
    () => {
      if (
        categoryItems.length > 0 &&
        Object.keys(categoryColors).length === 0
      ) {
        const colors = {};
        const config = {};
        if (categoryItems.length > 0) {
          categoryItems.map(
            // eslint-disable-next-line no-return-assign
            ({ name, slug, term_id: id }) => {
              colors[name] = randomHSL();
              config[name] = {};
              config[name].id = id;
              config[name].slugName = slug;
              return 0;
            },
          );

          dispatch({
            type: 'CATEGORY_COLORS_UPDATE',
            payload: { colors, config },
          });
        }
      }
    },
    [categoryItems],
  );

  useEffect(
    () => {
      const { data } = articleList;
      const postIds = [];

      if (Object.keys(data).length > 0) {
        // eslint-disable-next-line no-unused-vars
        Object.entries(data).map(([key, value]) => {
          if (value.length > 0) value.map(d => postIds.push(d.post_id));
          return 0;
        });

        uniq(postIds).forEach(d => {
          GET_ARTICLE_LIKE_VIEW_COUNT_API_CALL({
            query: { article_id: d },
            successCallback: ({
              res: {
                data: {
                  data: { like_count: likeCount, view_count: viewCount } = {},
                } = {},
              } = {},
            }) => {
              (containsHash
                ? GET_HASHTAG_ARTICLES_API_CUSTOM_TASK
                : ARTICLES_SEARCH_API_CUSTOM_TASK)(ON_SUCCESS, {
                tasks: [
                  {
                    task: 'isUpdate',
                    params: {
                      subKey: [containsHash ? hash : 'data'],
                      id: [d],
                      key: 'post_id',
                      values: {
                        [d]: { viewCount, likeCount },
                      },
                    },
                  },
                ],
              });
            },
          });
        });
      }
    },
    [articleList],
  );

  const categories = useMemo(
    () =>
      getData(ARTICLES_GET_ALL_CATEGORIES_API, {}, false, [
        'searchPageMainCat',
      ]),
    [ARTICLES_GET_ALL_CATEGORIES_API],
  );
  const { data: { data: categoryItems = [] } = {} } = categories;

  const articleList = useMemo(
    () =>
      getData(
        containsHash ? GET_HASHTAG_ARTICLES_API : ARTICLES_SEARCH_API,
        {},
        false,
      ),
    [containsHash ? GET_HASHTAG_ARTICLES_API : ARTICLES_SEARCH_API],
  );

  return { articles: articleList.data };
};
