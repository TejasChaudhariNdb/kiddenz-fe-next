/* eslint-disable no-nested-ternary */
/* eslint-disable eqeqeq */
/* eslint-disable camelcase */
// useReviewHooks
import { useEffect, useMemo } from 'react';
import { ON_SUCCESS } from 'shared/utils/commonReduxSagaConverter/commonConstants';
import uniq from 'lodash/uniq';
import flatten from 'lodash/flatten';
//   { onSuccess, onError },
export const useIntrestingArticlesHook = ({
  GET_ARTICLE_LIKE_VIEW_COUNT_API_CALL,
  GET_INTRESTING_ARTICLES_API_CALL,
  GET_ARTICLE_LIKE_VIEW_COUNT_API_CANCEL,
  GET_INTRESTING_ARTICLES_API_CUSTOM_TASK,
  dashboard: { GET_INTRESTING_ARTICLES_API },
  getData,
}) => {
  useEffect(
    () => () => {
      GET_ARTICLE_LIKE_VIEW_COUNT_API_CANCEL();
    },
    [],
  );

  useEffect(() => {
    GET_INTRESTING_ARTICLES_API_CALL();
  }, []);

  useEffect(() => {
    const { data } = articleList;
    const postIds = [];
    const postKeys = [];

    if (Object.keys(data).length > 0) {
      // eslint-disable-next-line no-unused-vars
      Object.entries(data).map(([key, value]) => {
        if (value && value.length > 0) value.map(d => postIds.push(d.post_id));
        postKeys.push(key);
        return 0;
      });

      uniq(postIds).forEach(d => {
        GET_ARTICLE_LIKE_VIEW_COUNT_API_CALL({
          query: { article_id: d },
          successCallback: ({
            res: {
              data: {
                data: { like_count: likeCount, view_count: viewCount } = {},
              } = {},
            } = {},
          }) => {
            GET_INTRESTING_ARTICLES_API_CUSTOM_TASK(ON_SUCCESS, {
              tasks: [
                ...(() =>
                  postKeys.map(x => ({
                    task: 'isUpdate',
                    params: {
                      subKey: [x],
                      id: [d],
                      key: 'post_id',
                      values: {
                        [d]: { viewCount, likeCount },
                      },
                    },
                  })))(),
              ],
            });
          },
        });
      });
    }
  }, []);

  const articleList = useMemo(
    () => getData(GET_INTRESTING_ARTICLES_API, {}, false),
    [GET_INTRESTING_ARTICLES_API],
  );
  const { data } = articleList;

  return {
    articles: (() => {
      const keys = Object.keys(data) || [];
      const mappedKeys =
        keys
          .map(
            d =>
              keys.length > 0 && Array.isArray(data[d]) && data[d].map(x => x),
          )
          .filter(d => d) || [];
      if (mappedKeys.length > 0) {
        return flatten(mappedKeys).slice(0, 3);
      }
      return [];
    })(),
  };
};
