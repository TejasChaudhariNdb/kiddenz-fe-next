/* eslint-disable no-bitwise */
// useArticleHook

import { useEffect, useMemo } from 'react';
import { ON_SUCCESS } from 'shared/utils/commonReduxSagaConverter/commonConstants';
import uniq from 'lodash/uniq';
import randomHSL from 'utils/colorGen';

export const useArticleHook = ({
  getData,
  GET_ARTICLE_LIKE_VIEW_COUNT_API_CALL,
  ARTICLES_GET_DASHBOARD_LIST_API_CUSTOM_TASK,
  dispatch,
  dashboard: {
    ARTICLES_GET_ALL_CATEGORIES_API,
    ARTICLES_GET_DASHBOARD_LIST_API,
  } = {},
  authentication: { categoryColors } = {},
} = {}) => {
  useEffect(
    () => {
      if (
        categoryItems.length > 0 &&
        Object.keys(categoryColors).length === 0
      ) {
        const colors = {};
        const config = {};
        if (categoryItems.length > 0) {
          categoryItems.map(
            // eslint-disable-next-line no-return-assign
            ({ name, slug, term_id: id }) => {
              colors[name] = randomHSL();
              config[name] = {};
              config[name].id = id;
              config[name].slugName = slug;
              return 0;
            },
          );

          dispatch({
            type: 'CATEGORY_COLORS_UPDATE',
            payload: { colors, config },
          });
        }
      }
    },
    [categoryItems],
  );

  useEffect(
    () => {
      const { data } = articleList;
      const postIds = [];
      const postKeys = [];

      if (Object.keys(data).length > 0) {
        // eslint-disable-next-line no-unused-vars
        Object.entries(data).map(([key, value]) => {
          if (value && value.length > 0)
            value.map(d => postIds.push(d.post_id));
          postKeys.push(key);
          return 0;
        });

        uniq(postIds).forEach(d => {
          GET_ARTICLE_LIKE_VIEW_COUNT_API_CALL({
            query: { article_id: d },
            successCallback: ({
              res: {
                data: {
                  data: { like_count: likeCount, view_count: viewCount } = {},
                } = {},
              } = {},
            }) => {
              ARTICLES_GET_DASHBOARD_LIST_API_CUSTOM_TASK(ON_SUCCESS, {
                tasks: [
                  ...(() =>
                    postKeys.map(x => ({
                      task: 'isUpdate',
                      params: {
                        subKey: [x],
                        id: [d],
                        key: 'post_id',
                        values: {
                          [d]: { viewCount, likeCount },
                        },
                      },
                    })))(),
                ],
              });
            },
          });
        });
      }
    },
    [articleList],
  );

  const categories = useMemo(
    () => getData(ARTICLES_GET_ALL_CATEGORIES_API, {}, false),
    [ARTICLES_GET_ALL_CATEGORIES_API],
  );
  const articleList = useMemo(
    () => getData(ARTICLES_GET_DASHBOARD_LIST_API, {}, false),
    [ARTICLES_GET_DASHBOARD_LIST_API],
  );

  const { data: { data: categoryItems = [] } = {} } = categories;

  return {
    categories,
    articleList: (() => {
      const articleIds = [];
      const data = {};

      Object.entries(articleList.data).map(([key, value]) => {
        data[key] = [];
        if (value && value.length > 0)
          value.map(d => {
            if (!articleIds.includes(d.post_id)) {
              articleIds.push(d.post_id);
              data[key].push(d);
            }
            return 0;
          });
        return 0;
      });

      return { data };
    })(),
    categoryColors: (() => {
      const colors = {};
      if (categoryItems.length > 0) {
        categoryItems.map(
          // eslint-disable-next-line no-return-assign
          ({ name }) => (colors[name] = randomHSL()),
        );
        return colors;
      }
      return {};
    })(),
    mappings: (() => {
      const slugTemp = {};
      const idTemp = {};
      const { data: { data = [] } = {} } = categories;
      (data.length > 0 ? data : []).map(d => {
        slugTemp[d.name] = d.slug;
        idTemp[d.name] = d.term_id;
        return 0;
      });
      return { categorySlugMap: slugTemp, categoryIdMap: idTemp };
    })(),
  };
};
