/* eslint-disable no-nested-ternary */
/* eslint-disable eqeqeq */
/* eslint-disable camelcase */
// useReviewHooks
import { useEffect, useMemo } from 'react';
import { ON_SUCCESS } from 'shared/utils/commonReduxSagaConverter/commonConstants';

//   { onSuccess, onError },
export const useLikeHook = (
  {
    GET_ARTICLES_BY_ID_API_CUSTOM_TASK,
    GET_USER_LIKED_ARTICLES_API_CUSTOM_TASK,
    ADD_ARTICLE_LIKE_API_CALL,
    ADD_ARTICLE_LIKE_API_CANCEL,
    DELETE_ARTICLE_LIKE_API_CALL,
    DELETE_ARTICLE_LIKE_API_CANCEL,
    // GET_ARTICLE_LIKE_VIEW_COUNT_API_CALL,
    GET_ARTICLE_LIKE_VIEW_COUNT_API_CANCEL,
    dashboard: {
      GET_USER_LIKED_ARTICLES_API,
      //   ADD_ARTICLE_LIKE_API,
      //   DELETE_ARTICLE_LIKE_API,
    },
    getData,
  },
  { onLikeError = () => {}, onUnLikeError = () => {} },
) => {
  useEffect(
    () => () => {
      ADD_ARTICLE_LIKE_API_CANCEL();
      DELETE_ARTICLE_LIKE_API_CANCEL();
      GET_ARTICLE_LIKE_VIEW_COUNT_API_CANCEL();
    },
    [],
  );

  // Like a Article
  const postLike = article_id => {
    const payload = { article_id };

    ADD_ARTICLE_LIKE_API_CALL({
      payload,
      successCallback: () => {
        GET_ARTICLES_BY_ID_API_CUSTOM_TASK(ON_SUCCESS, {
          isUpdate: true,
          updateCallback: oldData => ({
            data: { ...oldData.data, likeCount: oldData.data.likeCount + 1 },
            related_articles: oldData.related_articles,
          }),
        });

        GET_USER_LIKED_ARTICLES_API_CUSTOM_TASK(ON_SUCCESS, {
          tasks: [
            {
              task: 'isInfinite',
              params: {
                isInfinite: true,
              },
              value: [{ post_id: article_id }],
            },
          ],
        });
      },
      errorCallback: ({ message }) => {
        onLikeError(message);
      },
    });
  };

  // Remove Wishlist
  const deleteLike = article_id => {
    const payload = { article_id };
    DELETE_ARTICLE_LIKE_API_CALL({
      payload,
      successCallback: () => {
        GET_ARTICLES_BY_ID_API_CUSTOM_TASK(ON_SUCCESS, {
          isUpdate: true,
          updateCallback: oldData => ({
            data: { ...oldData.data, likeCount: oldData.data.likeCount - 1 },
            related_articles: oldData.related_articles,
          }),
        });
        GET_USER_LIKED_ARTICLES_API_CUSTOM_TASK(ON_SUCCESS, {
          isDelete: true,
          id: [article_id],
          key: 'post_id',
        });
      },
      errorCallback: ({ message }) => {
        onUnLikeError(message);
      },
    });
  };

  const userLikes = useMemo(
    () => getData(GET_USER_LIKED_ARTICLES_API, [], false),
    [GET_USER_LIKED_ARTICLES_API],
  );

  return {
    likes: userLikes.data,
    likesLoader: userLikes.loader,
    likedItems: userLikes.data.map(({ post_id }) => post_id),
    likeItem: postLike,
    unLikeItem: deleteLike,
  };
};
