// useArticleHook

import { useEffect, useMemo } from 'react';
import { ON_SUCCESS } from 'shared/utils/commonReduxSagaConverter/commonConstants';

export const useArticleMostRead = ({
  getData,
  GET_ARTICLE_LIKE_VIEW_COUNT_API_CALL,
  GET_MOST_READ_ARTICLES_API_CUSTOM_TASK,
  dashboard: { GET_MOST_READ_ARTICLES_API } = {},
} = {}) => {
  useEffect(
    () => {
      const { data = [] } = articleList;

      data.forEach(({ post_id: d }) => {
        GET_ARTICLE_LIKE_VIEW_COUNT_API_CALL({
          query: { article_id: d },
          successCallback: ({
            res: {
              data: {
                data: { like_count: likeCount, view_count: viewCount } = {},
              } = {},
            } = {},
          }) => {
            GET_MOST_READ_ARTICLES_API_CUSTOM_TASK(ON_SUCCESS, {
              tasks: [
                {
                  task: 'isUpdate',
                  params: {
                    id: [d],
                    key: 'post_id',
                    values: {
                      [d]: { viewCount, likeCount },
                    },
                  },
                },
              ],
            });
          },
        });
      });
    },
    [articleList],
  );

  const articleList = useMemo(
    () => getData(GET_MOST_READ_ARTICLES_API, [], false),
    [GET_MOST_READ_ARTICLES_API],
  );

  return { mostReadArticles: articleList.data.slice(0, 5) };
};
