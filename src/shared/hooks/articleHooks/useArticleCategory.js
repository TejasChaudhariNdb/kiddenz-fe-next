/* eslint-disable no-bitwise */
// useArticleHook

import { useEffect, useMemo } from 'react';
import uniq from 'lodash/uniq';
import { ON_SUCCESS } from 'shared/utils/commonReduxSagaConverter/commonConstants';
import randomHSL from 'utils/colorGen';

export const useArticleCategoryHook = ({
  getData,
  dispatch,
  GET_ARTICLE_LIKE_VIEW_COUNT_API_CALL,
  GET_CATEGORY_ARTICLES_API_CUSTOM_TASK,
  GET_ARICLES_BY_HASHTAGS_LIST_API_CALL,
  GET_ARICLES_BY_HASHTAGS_LIST_API_CANCEL,
  dashboard: {
    ARTICLES_GET_ALL_CATEGORIES_API,
    GET_CATEGORY_ARTICLES_API,
    GET_ARICLES_BY_HASHTAGS_LIST_API,
  } = {},
  authentication: { categoryColors } = {},
} = {}) => {
  useEffect(
    () => () => {
      GET_ARICLES_BY_HASHTAGS_LIST_API_CANCEL();
    },
    [],
  );

  useEffect(
    () => {
      if (
        categoryItems.length > 0 &&
        Object.keys(categoryColors).length === 0
      ) {
        const colors = {};
        const config = {};
        if (categoryItems.length > 0) {
          categoryItems.map(
            // eslint-disable-next-line no-return-assign
            ({ name, slug, term_id: id }) => {
              colors[name] = randomHSL();
              config[name] = {};
              config[name].id = id;
              config[name].slugName = slug;
              return 0;
            },
          );

          dispatch({
            type: 'CATEGORY_COLORS_UPDATE',
            payload: { colors, config },
          });
        }
      }
    },
    [categoryItems],
  );

  useEffect(
    () => {
      const { data } = articleList;
      const postIds = [];
      const postKeys = [];

      const articlesKeys = Object.keys(data);

      if (articlesKeys.length > 0) {
        // eslint-disable-next-line no-unused-vars
        // let totalArticles = 0;
        Object.entries(data).map(([key, value], i) => {
          // totalArticles += value.length;
          if (i === articlesKeys.length - 1 && totalArticles < 10) {
            const tagsOfFirstArticles =
              (articlesKeys[0] &&
                data[articlesKeys[0]] &&
                data[articlesKeys[0]].length > 0 &&
                data[articlesKeys[0]][0].post_tag.split(',')) ||
              [];

            // eslint-disable-next-line no-console
            GET_ARICLES_BY_HASHTAGS_LIST_API_CALL({
              payload: {
                post_tags: [...tagsOfFirstArticles],
                limit: 100,
                skip: 0,
              },
            });
          }
          if (value && value.length > 0)
            value.map(d => postIds.push(d.post_id));
          postKeys.push(key);
          return 0;
        });

        uniq(postIds).forEach(d => {
          GET_ARTICLE_LIKE_VIEW_COUNT_API_CALL({
            query: { article_id: d },
            successCallback: ({
              res: {
                data: {
                  data: { like_count: likeCount, view_count: viewCount } = {},
                } = {},
              } = {},
            }) => {
              GET_CATEGORY_ARTICLES_API_CUSTOM_TASK(ON_SUCCESS, {
                tasks: [
                  ...(() =>
                    postKeys.map(x => ({
                      task: 'isUpdate',
                      params: {
                        subKey: [x],
                        id: [d],
                        key: 'post_id',
                        values: {
                          [d]: { viewCount, likeCount },
                        },
                      },
                    })))(),
                ],
              });
            },
          });
        });
      }
    },
    [articleList],
  );

  const categoriesOverview = useMemo(
    () => getData(ARTICLES_GET_ALL_CATEGORIES_API, {}, false, ['subCats']),
    [ARTICLES_GET_ALL_CATEGORIES_API],
  );

  const categories = useMemo(
    () => getData(ARTICLES_GET_ALL_CATEGORIES_API, {}, false, ['mainCats']),
    [ARTICLES_GET_ALL_CATEGORIES_API],
  );

  const releatedArtciles = useMemo(
    () => getData(GET_ARICLES_BY_HASHTAGS_LIST_API, [], false),
    [GET_ARICLES_BY_HASHTAGS_LIST_API],
  );

  const { data: { data: categoryItems = [] } = {} } = categories;

  const articleList = useMemo(
    () => getData(GET_CATEGORY_ARTICLES_API, {}, false),
    [GET_CATEGORY_ARTICLES_API],
  );

  const { count: totalArticles, articlesIds: articlePostIds } = (() => {
    const articlesIds = [];
    // const articles = [];
    let count = 0;

    Object.entries(articleList.data).map(([, value]) => {
      if (value && value.length > 0)
        value.map(d => {
          if (!articlesIds.includes(d.post_id)) {
            articlesIds.push(d.post_id);
            count += 1;
          }
          return 0;
        });
      return 0;
    });

    return { count, articlesIds };
  })();

  // eslint-disable-next-line no-console

  return {
    categories: categoriesOverview,
    articleList: (() => {
      const articleIds = [];
      const data = {};

      Object.entries(articleList.data).map(([key, value]) => {
        data[key] = [];
        if (value && value.length > 0)
          value.map(d => {
            if (!articleIds.includes(d.post_id)) {
              articleIds.push(d.post_id);
              data[key].push(d);
            }
            return 0;
          });
        return 0;
      });
      return { data };
    })(),
    relatedArticle: (() => {
      const articleIds = [...articlePostIds];
      const data = releatedArtciles.data
        .map(d => {
          if (!articleIds.includes(d.post_id)) {
            articleIds.push(d.post_id);
            return d;
          }
          return false;
        })
        .filter(d => d);

      return { data };
    })(),
    articleCount: totalArticles,
    relatedArticleLoader: releatedArtciles.loader,
    mappings: (() => {
      const slugTemp = {};
      const idTemp = {};
      const { data: { data = [] } = {} } = categoriesOverview;
      (data.length > 0 ? data : []).map(d => {
        slugTemp[d.name] = d.slug;
        idTemp[d.name] = d.term_id;
        return 0;
      });
      return { categorySlugMap: slugTemp, categoryIdMap: idTemp };
    })(),
  };
};
