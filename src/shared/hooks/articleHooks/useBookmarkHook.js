/* eslint-disable no-nested-ternary */
/* eslint-disable eqeqeq */
/* eslint-disable camelcase */
// useReviewHooks
import { useEffect, useMemo } from 'react';
import { ON_SUCCESS } from 'shared/utils/commonReduxSagaConverter/commonConstants';

//   { onSuccess, onError },
export const useBookmarkHook = (
  {
    // GET_USER_BOOKMARK_ARTICLES_API_CALL,
    GET_USER_BOOKMARK_ARTICLES_API_CUSTOM_TASK,
    GET_USER_BOOKMARK_ARTICLES_API_CANCEL,
    ADD_BOOKMARK_ARTICLE_API_CALL,
    ADD_BOOKMARK_ARTICLE_API_CANCEL,
    DELETE_BOOKMARK_ARTICLE_API_CALL,
    DELETE_BOOKMARK_ARTICLE_API_CANCEL,
    // GET_ARTICLE_LIKE_VIEW_COUNT_API_CALL,
    GET_ARTICLE_LIKE_VIEW_COUNT_API_CANCEL,
    dashboard: {
      GET_USER_BOOKMARK_ARTICLES_API,
      //   ADD_BOOKMARK_ARTICLE_API,
      //   DELETE_BOOKMARK_ARTICLE_API,
    },
    getData,
  },
  { onBookmarkAddError = () => {}, onBookmarkDelError = () => {} },
) => {
  useEffect(
    () => () => {
      GET_USER_BOOKMARK_ARTICLES_API_CANCEL();
      ADD_BOOKMARK_ARTICLE_API_CANCEL();
      DELETE_BOOKMARK_ARTICLE_API_CANCEL();
      GET_ARTICLE_LIKE_VIEW_COUNT_API_CANCEL();
    },
    [],
  );

  // Add to Bookmark
  const postBookmark = article_id => {
    const payload = { article_id };

    ADD_BOOKMARK_ARTICLE_API_CALL({
      payload,
      successCallback: () => {
        GET_USER_BOOKMARK_ARTICLES_API_CUSTOM_TASK(ON_SUCCESS, {
          tasks: [
            {
              task: 'isInfinite',
              params: {
                isInfinite: true,
              },
              value: [{ post_id: article_id }],
            },
          ],
        });
      },
      errorCallback: ({ message }) => {
        onBookmarkAddError({ message });
      },
    });
  };

  // Remove Wishlist
  const deleteBookmark = article_id => {
    const payload = { article_id };
    DELETE_BOOKMARK_ARTICLE_API_CALL({
      payload,
      successCallback: () => {
        GET_USER_BOOKMARK_ARTICLES_API_CUSTOM_TASK(ON_SUCCESS, {
          isDelete: true,
          id: [article_id],
          key: 'post_id',
        });
      },
      errorCallback: ({ message }) => {
        onBookmarkDelError({ message });
      },
    });
  };

  //   const onWishlistLoadMore = skip =>
  //     GET_USER_BOOKMARK_ARTICLES_API_CALL({
  //       query: {
  //         limit: 3,
  //         skip,
  //       },
  //       subKey: ['wishlists'],
  //       isInfinite: true,
  //     });

  // useEffect(
  //   () => {
  //     if (userBookmarks.data.length > 0) {
  //       const postIds = userBookmarks.data.map(({ post_id }) => post_id);
  //       postIds.map(d =>
  //         GET_ARTICLE_LIKE_VIEW_COUNT_API_CALL({ query: { article_id: d } }),
  //       );
  //       // console.log(userBookmarks.data.map(({ post_id }) => post_id));
  //     }
  //   },
  //   [userBookmarks],
  // );

  const userBookmarks = useMemo(
    () => getData(GET_USER_BOOKMARK_ARTICLES_API, [], false),
    [GET_USER_BOOKMARK_ARTICLES_API],
  );

  return {
    bookmarks: userBookmarks.data,
    bookmarkLoader: userBookmarks.loader,
    bookmarkedItems: userBookmarks.data.map(({ post_id }) => post_id),
    addBookmark: postBookmark,
    removeBookmark: deleteBookmark,
    // deleteWishlist,
    // onWishlistLoadMore,
  };
};
