/* eslint-disable */
import { generateTimeStamp, updateIn } from '../helpers';
import Safe from '../nullCheck';
export const infiniteHandler = ({
  isInfinite,
  clearData,
  successData = {},
  errorData,
  updateCallback,
  subKey = [],
  limit,
  query,
  isError = false,
}) => ({ data: oldData = {} } = {}) => ({
  data: (() => {
    if (subKey.length > 0 && isInfinite) {
      const _oldCopyData = {
        ...oldData,
        ...successData,
        [subKey[0]]: oldData[subKey[0]],
      };
      // return _oldCopyData
      return updateIn(_oldCopyData, subKey, _oldData => {
        if (isInfinite) {
          if (clearData) return Safe(successData, `.${subKey.join('.')}`, []);
          return _oldData.concat(Safe(successData, `.${subKey.join('.')}`, []));
        }
      });
    }
    const getData = Array.isArray(successData) ? successData : [successData];
    const appendData = Array.isArray(oldData)
      ? oldData.concat(getData)
      : getData;
    const newData =
      (clearData && isInfinite && successData) ||
      (!isError && Array.isArray(successData) && isInfinite && appendData) ||
      (isError && (Array.isArray(oldData) ? [] : {})) ||
      (isInfinite && appendData) ||
      successData;
    return updateCallback ? updateCallback(newData, successData) : newData;
  })(),
  error: (isError && errorData) || null,
  lastUpdated: generateTimeStamp(),
  isInfinite: !!isInfinite,
  infiniteEnd:
    !isError && isInfinite
      ? successData.length < limit || query.limit
      : undefined,
});
