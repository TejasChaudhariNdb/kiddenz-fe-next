import { updateIn, newObject } from '../helpers';
import { infiniteHandler } from './infiniteHandler';

export const filterHandler = ({
  isInfinite,
  successData,
  query,
  filter,
  clearData,
  ...rest
}) => ({ data = {} } = {}) => ({
  data: newObject(data, ({ [filter]: filterData = {} } = {}) => ({
    [filter]: newObject(
      filterData,
      infiniteHandler({ isInfinite, successData, query, clearData, ...rest }),
    ),
  })),
});

export const filterArrayHandler = ({
  isInfinite,
  successData,
  clearData,
  query,
  filter,
  ...rest
} = {}) => ({ data: Data = {} }) => ({
  data: updateIn(Data, filter, oldData =>
    newObject(
      oldData,
      infiniteHandler({ isInfinite, successData, query, clearData, ...rest }),
    ),
  ),
});
