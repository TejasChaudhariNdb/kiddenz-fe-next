import { updateIn, newObject } from '../helpers';
import { infiniteHandler } from './infiniteHandler';
export const filterArrayErrorHandler = ({
  isInfinite,
  errorData,
  query,
  filter,
} = {}) => ({ data: Data = {} }) => ({
  data: updateIn(Data, filter, oldData =>
    newObject(
      oldData,
      infiniteHandler({ isInfinite, errorData, query, isError: true }),
    ),
  ),
});
export const errorHandler = ({ isInfinite, errorData, query } = {}) => ({
  data: Data = {},
}) => ({
  data: newObject(
    Data,
    infiniteHandler({ isInfinite, errorData, query, isError: true }),
  ),
});
