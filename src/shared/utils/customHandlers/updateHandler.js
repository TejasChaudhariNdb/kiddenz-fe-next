/* eslint-disable */
import { updateIn, newObject, generateTimeStamp } from '../helpers';
import Safe from '../nullCheck';
const updateData = (data, successData, updateCallback) => {
  if (updateCallback) return updateCallback(data, successData);
  if (
    typeof successData === 'object' &&
    !Array.isArray(successData) &&
    typeof data === 'object' &&
    !Array.isArray(data)
  )
    return newObject(data, successData);
  return successData;
};
export const updateHandler = ({
  key,
  id,
  successData,
  updateCallback,
  subKey = [],
  values = {},
}) => ({ data = [] } = {}) => ({
  data:
    subKey.length > 0
      ? updateIn(
          newObject(
            data,
            (() => {
              if (!Array.isArray(successData)) {
                const temp = { ...(successData || {}) };
                delete temp[subKey[0]];
                return temp;
              } else {
                return {};
              }
            })(),
          ),
          subKey,
          _Data =>
            (() => {
              let index = -1;
              const _values = Array.isArray(values);
              if (!Array.isArray(_Data))
                return updateData(
                  _Data,
                  Safe(successData, `.${subKey.join('.')}`),
                  updateCallback,
                );
              if (Array.isArray(id) && key)
                return _Data.reduce(
                  (acc, curr) =>
                    id.includes(curr[key])
                      ? (() => {
                          index = index + 1;
                          return acc.concat([
                            updateData(
                              curr,
                              values[_values ? index : curr[key]] ||
                                Safe(successData, `.${subKey.join('.')}`),
                              updateCallback,
                            ),
                          ]);
                        })()
                      : acc.concat([curr]),
                  [],
                );
              if ((id === 0 || id) && key)
                return _Data.map(
                  _data =>
                    _data[key] === id
                      ? (() => {
                          index = index + 1;
                          return updateData(
                            _data,
                            values[_values ? index : curr[key]] ||
                              Safe(successData, `.${subKey.join('.')}`),
                            updateCallback,
                          );
                        })()
                      : _data,
                );
              return updateData(
                _Data,
                Safe(successData, `.${subKey.join('.')}`),
                updateCallback,
              );
            })(),
        )
      : (() => {
          let index = -1;
          const _values = Array.isArray(values);
          if (!Array.isArray(data))
            return updateData(data, successData, updateCallback);
          if (Array.isArray(id) && key)
            return data.reduce(
              (acc, curr) =>
                id.includes(curr[key])
                  ? (() => {
                      index = index + 1;
                      return acc.concat([
                        updateData(
                          curr,
                          values[_values ? index : curr[key]] || successData,
                          updateCallback,
                        ),
                      ]);
                    })()
                  : acc.concat([curr]),
              [],
            );
          if ((id === 0 || id) && key)
            return data.map(
              _data =>
                _data[key] === id
                  ? (() => {
                      index = index + 1;
                      return updateData(
                        _data,
                        values[_values ? index : curr[key]] || successData,
                        updateCallback,
                      );
                    })()
                  : _data,
            );
          return updateData(data, successData, updateCallback);
        })(),
  lastUpdated: generateTimeStamp(),
});
export const filterArrayUpdateHandler = ({
  key,
  id,
  filter,
  subKey,
  successData,
  updateCallback,
  ...rest
} = {}) => ({ data: Data = {} } = {}) => ({
  data: (() => {
    const paramKey = { key, id, successData, updateCallback, subKey, ...rest };
    if (filter && filter.some(fil => Array.isArray(fil))) {
      return filter.reduce(
        (accumulator, filterArray) =>
          updateIn(accumulator, filterArray, data =>
            newObject(data, updateHandler(paramKey)),
          ),
        Data,
      );
    }
    if (filter.some(fil => Array.isArray(fil))) return Data;
    return updateIn(Data, filter, data =>
      newObject(data, updateHandler(paramKey)),
    );
  })(),
});
