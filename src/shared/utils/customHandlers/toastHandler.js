/* eslint-disable no-nested-ternary */
import { updateIn, newObject, generateTimeStamp } from '../helpers';
export const filterArrayToastEmptyHandler = ({
  isInfinite,
  filter,
  subKey = [],
}) => ({ data: Data = {} } = {}) => ({
  data: updateIn(Data, filter, oldData =>
    newObject(oldData, ({ toast = {}, data }) => ({
      data: isInfinite
        ? subKey.length > 0
          ? data
          : (Array.isArray(data) && data) || []
        : data,
      isInfinite,
      toast: newObject(toast, {
        message: '',
        status: '',
        isError: null,
        key: '',
      }),
    })),
  ),
});

export const filterToastEmptyHandler = ({
  isInfinite,
  filter,
  subKey = [],
}) => ({ data: Data = {} } = {}) => ({
  data: newObject(Data, ({ [filter]: filterData = {} }) => ({
    [filter]: newObject(filterData, ({ toast = {}, data = {} }) => ({
      data: isInfinite
        ? subKey.length > 0
          ? data
          : (Array.isArray(data) && data) || []
        : data,
      isInfinite,
      toast: newObject(toast, {
        message: '',
        status: '',
        isError: null,
        key: '',
      }),
    })),
  })),
});

export const filterArrayToastHandler = ({
  statusCode,
  filter,
  message,
  type,
} = {}) => ({ data: Data = {} } = {}) => ({
  data: updateIn(Data, filter, oldData =>
    newObject(oldData, {
      toast: {
        isError: ![200, 201].includes(statusCode),
        status: statusCode,
        message,
        key: type,
        lastUpdated: generateTimeStamp(),
      },
    }),
  ),
});
