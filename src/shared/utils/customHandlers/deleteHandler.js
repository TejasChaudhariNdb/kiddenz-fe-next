/* eslint-disable indent */
import { updateIn, newObject, generateTimeStamp } from '../helpers';
export const deleteHandler = ({ key, id, subKey = [], successData = {} }) => ({
  data = [],
} = {}) => ({
  data:
    subKey.length > 0
      ? updateIn(
          newObject(data, successData),
          subKey,
          _data =>
            (!Array.isArray(_data) && {}) ||
            (Array.isArray(id) &&
              _data.reduce(
                (acc, curr) =>
                  id.includes(curr[key]) ? acc : acc.concat([curr]),
                [],
              )) ||
            _data.filter(({ [key]: objId }) => objId !== id),
        )
      : (!Array.isArray(data) && successData) ||
        (Array.isArray(id) &&
          data.reduce(
            (acc, curr) => (id.includes(curr[key]) ? acc : acc.concat([curr])),
            [],
          )) ||
        data.filter(({ [key]: objId }) => objId !== id),
  lastUpdated: generateTimeStamp(),
});

export const filterDeleteHandler = ({
  filter,
  key,
  id,
  subKey,
  successData,
}) => ({ data: Data = {} } = {}) => ({
  data: newObject(Data, ({ [filter]: filterData = {} }) => ({
    [filter]: newObject(
      filterData,
      deleteHandler({ key, id, subKey, successData }),
    ),
  })),
});

export const filterArrayDeleteHandler = ({
  key,
  id,
  filter,
  subKey,
  successData,
}) => ({ data: Data = {} } = {}) => ({
  data: updateIn(Data, filter, data =>
    newObject(data, deleteHandler({ key, id, subKey, successData })),
  ),
});
