import { updateIn, newObject, generateTimeStamp } from '../helpers';

export const resetHandler = (
  state,
  newState,
  { response: { type } },
  customType = undefined,
) =>
  newObject(state, ({ [customType || type]: Data }) => ({
    [customType || type]: newObject(Data, ({ data, toast, infiniteEnd }) => ({
      data: (Array.isArray(data) && []) || {},
      toast: newObject(toast, {
        message: '',
        status: '',
      }),
      infiniteEnd: typeof infiniteEnd === 'boolean' ? false : undefined,
      lastUpdated: generateTimeStamp(),
    })),
  }));

export const filterArrayResetHandler = (
  state,
  newState,
  action,
  filter,
  customType = undefined,
) => {
  const {
    response: { type },
  } = action;
  return newObject(state, ({ [customType || type]: oldData }) => ({
    [type]: newObject(oldData, ({ data: Data = {} } = {}) => ({
      data: updateIn(Data, filter, updateData =>
        newObject(updateData, ({ data, toast, infiniteEnd }) => ({
          data: (Array.isArray(data) && []) || {},
          toast: newObject(toast, {
            message: '',
            status: '',
          }),
          infiniteEnd: typeof infiniteEnd === 'boolean' ? false : undefined,
          lastUpdated: generateTimeStamp(),
        })),
      ),
    })),
  }));
};
