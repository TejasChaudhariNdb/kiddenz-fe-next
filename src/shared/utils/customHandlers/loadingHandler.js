import { updateIn, newObject, generateTimeStamp } from '../helpers';
export const filterArrayloadingHandler = ({ filter, loader }) => ({
  data: Data = {},
} = {}) => ({
  data: updateIn(Data, filter, oldData =>
    newObject(oldData, {
      loading: {
        status: loader,
        lastUpdated: generateTimeStamp(),
      },
    }),
  ),
});
