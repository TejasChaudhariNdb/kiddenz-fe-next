/* eslint-disable no-underscore-dangle */
/* eslint-disable no-plusplus */

export const setCookie = (name, value) => {
  const expiresOn = new Date(
    Date.now() + 30 * 24 * 60 * 60 * 1000,
  ).toUTCString();
  document.cookie = `${name}=${value}; path=/; expires=${expiresOn}`;
};

export const deleteCookie = name => {
  const expiresOn = new Date(
    Date.now() + 30 * 24 * 60 * 60 * 1000,
  ).toUTCString();
  document.cookie = `${name}=; path=/; expires=${expiresOn}`;
};

export const getCookie = cname => {
  const name = `${cname}=`;
  const decodedCookie = decodeURIComponent(document.cookie);
  const ca = decodedCookie.split(';');
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) === ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) === 0) {
      return c.substring(name.length, c.length);
    }
  }
  return '';
};
