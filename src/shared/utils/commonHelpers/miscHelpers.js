export const removeNullValuesFromObject = obj =>
  Object.fromEntries(
    Object.entries(obj)
      // eslint-disable-next-line no-unused-vars
      .filter(([_, v]) => v != null)
      .map(
        ([k, v]) =>
          typeof v === 'object' && !Array.isArray(v)
            ? [k, removeNullValuesFromObject(v)]
            : [k, v],
      ),
  );
