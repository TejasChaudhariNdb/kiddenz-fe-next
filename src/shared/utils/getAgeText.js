const getAgeText = (startAge = 0, endAge = 0) => {
  let startYear = null;
  let startMonth = null;
  let startAgeText = '';
  let endAgeText = 'to ';
  let endYear = null;
  let endMonth = null;
  if (startAge) {
    startYear = Math.floor(startAge / 12);
    startMonth = startAge % 12;
  }
  if (endAge) {
    endYear = Math.floor(endAge / 12);
    endMonth = endAge % 12;
  }
  if (startYear) {
    startAgeText = `${startYear} year${startYear > 1 ? 's' : ''}`;
  }
  if (startMonth) {
    startAgeText = `${startAgeText} ${startMonth} month${
      startMonth > 1 ? 's' : ''
    }`;
  }
  if (endYear) {
    endAgeText = `${endAgeText} ${endYear} year${endYear > 1 ? 's' : ''}`;
  }
  if (endMonth) {
    endAgeText = `${endAgeText} ${endMonth} month${endMonth > 1 ? 's' : ''}`;
  }
  if (!endYear && !endMonth) {
    endAgeText = '';
  }
  return `${startAgeText} ${endAgeText}`;
};

export default getAgeText;
