/* eslint-disable indent */
function getIn(obj, arr) {
  let i = 0;
  let o = obj;
  function get() {
    return arr.length - 1 === i
      ? o[arr[i]]
      : (() => {
          o = o[arr[i]];
          i += 1;
          return get();
        })();
  }
  return get();
}

export { getIn };
