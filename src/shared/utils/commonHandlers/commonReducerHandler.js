/* eslint-disable indent */
import { newObject } from '../helpers';
import {
  ON_ERROR,
  ON_SUCCESS,
  ON_UNMOUNT,
} from '../commonReduxSagaConverter/commonConstants';
import {
  infiniteHandler as InfiniteHandler,
  deleteHandler,
  filterArrayHandler,
  filterArrayErrorHandler,
  toggleKeyHandler,
  filterArrayToggleKeyHandler,
  filterArrayUpdateHandler,
  filterArrayDeleteHandler,
  filterArrayDeleteKeyHandler,
  deleteKeyHandler,
  updateHandler,
  errorHandler,
  filterArrayUpdateKeyHandler,
  updateKeyHandler,
} from '../customHandlers';

const COMMON_HANDLER = (payload, data) => {
  let DATA = data;
  const bindAction = Action => Action(payload);
  (payload.tasks || Array(1).fill({})).forEach(
    ({ task = '', params = {}, value = {} } = {}) => {
      let customTaskBindAction = null;
      const isTask = task
        ? [
            'isDelete',
            'isUpdateKey',
            'isDeleteKey',
            'isUpdate',
            'isToggleKey',
          ].includes(task)
        : payload.isDelete ||
          payload.isDeleteKey ||
          payload.isUpdate ||
          payload.isToggleKey ||
          payload.isUpdateKey;
      if (payload.tasks)
        customTaskBindAction = Action =>
          Action({ query: {}, params: {}, ...params, successData: value });
      if (isTask) {
        if (task ? task === 'isUpdateKey' : payload.isUpdateKey)
          DATA = (task
          ? params.filter
          : payload.filter)
            ? newObject(
                DATA,
                (task ? customTaskBindAction : bindAction)(
                  filterArrayUpdateKeyHandler,
                ),
              )
            : newObject(
                DATA,
                (task ? customTaskBindAction : bindAction)(updateKeyHandler),
              );
        if (task ? task === 'isUpdate' : payload.isUpdate)
          DATA = (task
          ? params.filter
          : payload.filter)
            ? newObject(
                DATA,
                (task ? customTaskBindAction : bindAction)(
                  filterArrayUpdateHandler,
                ),
              )
            : newObject(
                DATA,
                (task ? customTaskBindAction : bindAction)(updateHandler),
              );
        if (task ? task === 'isDeleteKey' : payload.isDeleteKey)
          DATA = (task
          ? params.filter
          : payload.filter)
            ? newObject(
                DATA,
                (task ? customTaskBindAction : bindAction)(
                  filterArrayDeleteKeyHandler,
                ),
              )
            : newObject(
                DATA,
                (task ? customTaskBindAction : bindAction)(deleteKeyHandler),
              );
        if (task ? task === 'isDelete' : payload.isDelete)
          DATA = (task
          ? params.filter
          : payload.filter)
            ? newObject(
                DATA,
                (task ? customTaskBindAction : bindAction)(
                  filterArrayDeleteHandler,
                ),
              )
            : newObject(
                DATA,
                (task ? customTaskBindAction : bindAction)(deleteHandler),
              );
        if (task ? task === 'isToggleKey' : payload.isToggleKey)
          DATA = (task
          ? params.filter
          : payload.filter)
            ? newObject(
                DATA,
                (task ? customTaskBindAction : bindAction)(
                  filterArrayToggleKeyHandler,
                ),
              )
            : newObject(
                DATA,
                (task ? customTaskBindAction : bindAction)(toggleKeyHandler),
              );
      } else {
        DATA = (payload.tasks
        ? params.filter
        : payload.filter)
          ? newObject(
              DATA || {},
              (payload.tasks ? customTaskBindAction : bindAction)(
                filterArrayHandler,
              ),
            )
          : newObject(
              DATA || {},
              (payload.tasks ? customTaskBindAction : bindAction)(
                InfiniteHandler,
              ),
            );
      }
    },
  );

  return DATA;
};

export const COMMON_REDUCER_HANDLER = action => {
  const {
    response: {
      data: { data: successData = {} } = {},
      payload: { query = {}, isInfinite, filter, key, id, ...restPayload } = {},
      error: { data: errorData = {} } = {},
    } = {},
  } = action;

  const commonHandler = COMMON_HANDLER.bind(null, {
    ...restPayload,
    isInfinite,
    successData,
    errorData,
    query,
    filter: filter && (Array.isArray(filter) ? filter : [filter]),
    key,
    id,
  });
  const ErrorHandler = (filter && filterArrayErrorHandler) || errorHandler;
  const commmonErrorHandler = ErrorHandler.bind(null, {
    isInfinite,
    errorData,
    query,
    filter,
  });

  return [commonHandler, commmonErrorHandler];
};

export const DEFAULT_REDUCER_HANDLER = ({
  method,
  reset,
  state,
  action,
  type,
}) => {
  const [commonHandler, commmonErrorHandler] = COMMON_REDUCER_HANDLER(action);
  const {
    response: {
      data: { data: successData = {} } = {},
      payload: { updateStateCallback } = {},
    } = {},
  } = action;
  switch (method) {
    case ON_SUCCESS: {
      const updatedState = newObject(state, ({ [type]: Data }) => ({
        [type]: commonHandler(Data, state),
      }));
      return updateStateCallback
        ? updateStateCallback({ state: updatedState, data: successData }) ||
            updatedState
        : updatedState;
    }
    case ON_ERROR: {
      return newObject(state, ({ [type]: Data }) => ({
        [type]: newObject(Data, commmonErrorHandler()),
      }));
    }
    case ON_UNMOUNT:
      return reset();
    default:
      return state;
  }
};
