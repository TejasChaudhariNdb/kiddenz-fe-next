const getCardAgeText = (startAge = 0, endAge = 0) => {
  let startYear = null;
  let startMonth = null;
  let startAgeText = '';
  let endAgeText = 'to ';
  let endYear = null;
  let endMonth = null;
  if (startAge) {
    startYear = Math.floor(startAge / 12);
    startMonth = startAge % 12;
  }
  if (endAge) {
    endYear = Math.floor(endAge / 12);
    endMonth = endAge % 12;
  }
  if (startYear) {
    startAgeText = `${startYear}`;
  }
  if (startMonth) {
    startAgeText = `${startAgeText}.${startMonth}`;
  }
  if (endYear) {
    endAgeText = `${endAgeText} ${endYear}`;
  }
  if (endMonth) {
    endAgeText = `${endAgeText}.${endMonth}`;
  }
  if (!endYear && !endMonth) {
    endAgeText = '';
  }
  return `${startAgeText} ${endAgeText} years`;
};

export default getCardAgeText;
