/* eslint-disable consistent-return */
/* eslint-disable no-console */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable indent */
/* eslint-disable camelcase */
/* eslint-disable eqeqeq */
/* eslint-disable no-lonely-if */
/* eslint-disable no-nested-ternary */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
/* eslint-disable react/prop-types */
/*
 * FeaturePage
 *
 * List all the features
 */
import React, { useState, useRef, useEffect, useCallback } from 'react';
import Router, { useRouter } from 'next/router';
import Qs from 'query-string';
import Link from 'next/link';
// import { Helmet } from 'react-helmet';
// import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import Header from 'components/Header';
import Footer from 'components/Footer';
import Skeleton from 'react-loading-skeleton';
import Geocode from 'react-geocode';

import {
  GoogleMap,
  InfoBox,
  InfoWindow,
  useLoadScript,
  Marker,
  DirectionsService,
  DirectionsRenderer,
  useGoogleMap,
} from '@react-google-maps/api';
import { MAPS_API_KEY, MAP_STYLE } from 'utils/google';
import { CLICK_COUNTS } from 'utils/constants';
// import Footer from 'components/Footer';
import MainWrapper from 'components/MainWrapper';
import {
  useDayCareSearchHook,
  useWishlistHook,
  useClickCount,
} from 'shared/hooks';
import { removeNullValuesFromObject } from 'shared/utils/commonHelpers/miscHelpers';
import colors from 'utils/colors';
import Flex from '../../components/common/flex';
import Ruppee from '../../components/common/Ruppee';
import Button from '../../components/Button';
import CheckBox from '../../components/Checkbox';
import SchoolCard from '../../components/SchoolCard';
import Filter from '../../components/Filter';
import Dropdown from '../../components/Dropdown';
import Modal from '../../components/Modal';
import Refresh from '../../components/RefreshFilter';
import infoImage from '../../images/babyArticle.jpg';
import daycare from '../../images/Daycare.jpg';
import background from '../../images/queensroad.png';
import tripMarker from '../../images/location-oval.png';
import profile from '../../images/profile.jpg';
import background1 from '../../images/map2.png';
import markerIcon from '../../images/mapMarker.svg';
import pulseLoader from '../../images/pulse_loader.gif';
import buildingPlaceholder from '../../images/building-placeholder.png';
import elle from '../../images/elle.png';
import oi from '../../images/oi.png';
import tiny from '../../images/tiny.png';
import jumbo from '../../images/jumbo.png';
import currentLocMarker from '../../images/currentLocMarker.svg';
import activeLocationMarker from '../../images/activeLocationMarker.svg';
import Input from '../HomePage/Input';
import Form from '../HomePage/Form';
// import klya from '../../images/klya.png';
import schoolImage1 from '../../images/pre-school-1.png';
import schoolImage2 from '../../images/pre-school-2.png';
import schoolImage3 from '../../images/pre-school-3.png';
import schoolImage4 from '../../images/pre-school-4.png';
import schoolImage5 from '../../images/pre-school-5.png';
import schoolImage6 from '../../images/pre-school-6.png';

const SCHOOL_PLACEHOLDERS = [
  schoolImage1,
  schoolImage2,
  schoolImage3,
  schoolImage4,
  schoolImage5,
  schoolImage6,
];

const schoolPlaceholderImageMapper = id => {
  if (id % 6 === 0) {
    return schoolImage1;
  }
  if (id % 6 === 1) {
    return schoolImage2;
  }
  if (id % 6 === 2) {
    return schoolImage3;
  }
  if (id % 6 === 3) {
    return schoolImage4;
  }
  if (id % 6 === 4) {
    return schoolImage5;
  }
  if (id % 6 === 5) {
    return schoolImage6;
  }
};

const schoolSuggestionPlaceholderImageMapper = id => {
  if (id % 3 === 0) {
    return schoolImage1;
  }
  if (id % 3 === 1) {
    return schoolImage2;
  }
  if (id % 3 === 2) {
    return schoolImage3;
  }
};

const PROVIDER_MAP = ['Pre-school', 'Day care', 'Pre-school / Day care'];

const SCHEDULE_CONFIG = { 'full time': 1, weekend: 3, flexible: 2 };

export default function SearchResultPage(props) {
  const router = useRouter();
  const [filterState, setFilterState] = useState(FILTERS_INITIAL_STATE);

  const [active, setActive] = useState(false);
  const [modalStatus, setModalStatus] = useState(false);
  const [modalType, setModalType] = useState('email');
  const [restrictionType, setRestrictionType] = useState(null);
  const [filterSectionActive, setFilterSectionActive] = useState(false);
  const [mobileMapFixed, setMobileMapFixed] = useState(false);
  const [loading, setLoading] = useState(false);

  const { successToast, errorToast } = props;
  const [cityDescription, setCityDescription] = React.useState('');

  const onSuccess = msg => successToast('Successfully Submited.');
  const onError = ({ message }) => errorToast(message);
  const onWishlistDelError = message =>
    errorToast(message || 'Something went wrong.');
  Geocode.setApiKey(MAPS_API_KEY);
  useEffect(() => {
    setLoading(true);
    const timer = setTimeout(() => {
      setLoading(false);
    }, 3000);
    // Cancel the timer while unmounting
    return () => clearTimeout(timer);
  }, []);

  const {
    postCount,
    clickData: {
      filters_application: filterClickCount = 0,
      school_profile_visits: schoolProfileClickCount = 0,
    } = {},
  } = useClickCount(props);

  const {
    isDragToSearchInitiated,
    newCenter = {},
    setNewCenter,
    locationSearchSting,
    setLocationSearchSting,
    toSearchSting,
    setToSearchSting,
    fromSearchSting,
    setFromSearchSting,
    locationLat,
    setLocationLat,
    locationLng,
    setLocationLng,
    searchData: { search_results: SearchResults = [], count = 0 },
    slugToSearchCoordinatesMap,
    search,
    onSearchLoadMore,
    searchType,
    searchCoordinates,
    searchLoader,
    isLoadMoreTriggered,
    setSearchType,
    locationSearchMethod,
    setLocationSearchMethod,
    appliedFilters,
    setHoveredState,
    onApplyFilter,
    modifyFilter,
    setIsMapMoved,
    sortType,
    setSortType,
    searchAsBoundsChage,
    setToLatitude,
    setToLongitude,
    setFromLatitude,
    setFromLongitude,
    setTripHrs,
    setTripMins,
    mobileViewCurrentSchoolSlug,
    setMobileViewCurrentSchoolSlug,
    mobileView,
    setMobileView,
    fromLatitude,
    fromLongitude,
    toLatitude,
    toLongitude,
    tripHrs,
    tripMins,
    next_page,
    max_page,

    notFound: {
      phone,
      email,
      onNotFoundInputsChangeHandler,
      onNotFoundInputsBlurHandler,
      submitContactHandler,
      error,
      submitContactLoader,
      loadMoreSuggestions,
      dayCareSuggestions: {
        search_results: suggestions = [],
        count: suggestionsCount = 0,
      },
      dayCareSuggestionsLoader,
    },
  } = useDayCareSearchHook(props, { onSuccess, onError });

  const { postWishlist, deleteWishlist } = useWishlistHook(props, {
    onWishlistDelError,
  });

  const {
    locationString = '',
    fromLocationString = '',
    toLocationString = '',
    ...filters
  } = props.router.query;

  const {
    fromLat,
    fromLng,
    toLat,
    toLng,
    timehrs,
    timemins,
    search_type: searchMethod,
    latitude,
    longitude,
    type,
    sort_by = 'distance',
  } = filters;

  const { authentication: { isLoggedIn } = {} } = props;
  // const markerCluster =
  useEffect(
    () => {
      if (Object.keys(props.router.query).length > 0) {
        setLocationSearchSting(locationString);
        setFromSearchSting(fromLocationString);
        setToSearchSting(toLocationString);
        setLocationLat(latitude);
        setLocationLng(longitude);
        setSearchType(type);
        setLocationSearchMethod(searchMethod);
        setSortType(sort_by);

        setToLatitude(toLat);
        setToLongitude(toLng);
        setFromLatitude(fromLat);
        setFromLongitude(fromLng);
        setTripHrs(timehrs);
        setTripMins(timemins);
      }

      if (!props.router.query.locationString) {
        getLatLng().then(
          latlng => {
            setLocationLat(latlng.lat);
            setLocationLng(latlng.lng);
          },
          // eslint-disable-next-line no-shadow
          error => {
            // Handle any errors that occur in the getLatLng function
            console.log(error);
          },
        );
      }
    },
    [props.router.query],
  );

  async function getLatLng() {
    const address = `${props.router.query.city}, ${props.router.query.area}`;
    const response = await Geocode.fromAddress(address);
    const { lat, lng } = response.results[0].geometry.location;
    setLocationSearchSting(response.results[0].formatted_address);
    return { lat, lng };
  }

  React.useEffect(
    () => {
      console.log(props?.query);
      console.log(props?.query?.area);
      if (props?.query?.area) {
        fetchAreaDescription();
      } else {
        fetchCityDescription();
      }
    },
    [props?.query?.city, props?.query?.area],
  );

  const fetchCityDescription = async () => {
    try {
      const response = await fetch(
        `https://userprodbe.kiddenz.com/api/day-care/city-description/${
          props?.query?.city
        }?format=json`,
      );

      if (!response.ok) {
        throw new Error('Failed to fetch city description');
      }

      const data = await response.json();
      // Assuming the response has a field 'description'
      setCityDescription(data.data[0].description);
      console.log(data.data[0].description);
    } catch (errorr) {
      console.error('Error fetching city description:', errorr);
    }
  };

  const fetchAreaDescription = async () => {
    try {
      const response = await fetch(
        `https://userprodbe.kiddenz.com/api/day-care/city-area-description/${
          props?.query?.city
        }/${props?.query?.area}/?format=json`,
      );

      if (!response.ok) {
        throw new Error('Failed to fetch city description');
      }

      const data = await response.json();
      // Assuming the response has a field 'description'
      setCityDescription(data.data[0].description);
      console.log(data.data[0].description);
    } catch (errorr) {
      console.error('Error fetching city description:', errorr);
    }
  };

  // const handelDayCareClick = (id, branch) =>
  //   Router.push({
  //     pathname: `/daycare/${id}`,
  //     query: { id, branch },
  //   }).then(() => window.scrollTo(0, 0));

  const onNewLocationHeaderSearch = (
    method = 'location',
    locationDetails = {},
  ) => {
    const {
      locationSearchSting: location = '',
      locationLat: lat = 0.0,
      locationLng: lng = 0.0,
      fromLocationSearchSting = '',
      toLocationSearchSting = '',
      fromLocationLat = 0.0,
      toLocationLat = 0.0,
      fromLocationLng = 0.0,
      toLocationLng = 0.0,
      fromTime = '',
    } = locationDetails;
    const query = Qs.stringify(removeNullValuesFromObject(appliedFilters), {
      arrayFormat: 'brackets',
    });
    const hrs = fromTime.split(':')[0];
    const mins = fromTime.split(':')[1];
    const cleanloc = location.replace(/\s/g, '');
    const urlElements = cleanloc.split(',').reverse();
    let finalurl = '';
    // eslint-disable-next-line no-plusplus
    for (let i = 2; i < urlElements.length; i++) {
      finalurl += `/${urlElements[i]}`;
    }
    Router.replace(
      // `/daycare/search?`,
      `/daycare/search?search_type=${method}&locationString=${location}&toLocationString=${toLocationSearchSting}&fromLocationString=${fromLocationSearchSting}`,
      // { shallow: true },
      /*  { pathname: `${finalurl.toLowerCase()}` }, */
    );
    // console.log(forloc);
    if (method == 'trip') {
      console.log('this is a trip');
    }

    // Router.replace({
    //   pathname: `/daycare/search`,
    //   query: {
    //     search_type: method,
    //     locationString: location,
    //     toLocationString: toLocationSearchSting,
    //     fromLocationString: fromLocationSearchSting,
    //     type: 'radius',
    //     sort_by: 'sortType',
    //     ...(method === 'trip'
    //       ? {
    //           fromLat: fromLocationLat,
    //           toLat: toLocationLat,
    //           fromLng: fromLocationLng,
    //           toLng: toLocationLng,
    //           timehrs: hrs,
    //           timemins: mins,
    //         }
    //       : {
    //           latitude: lat,
    //           longitude: lng,
    //         }),
    //     ...appliedFilters,
    //   },
    // });

    setLocationSearchMethod(method);

    if (method === 'location') {
      setLocationSearchSting(location);
      setLocationLat(lat);
      setLocationLng(lng);
      // setNewCenter({
      //   lat: +lat,
      //   lng: +lng,
      // });
    } else {
      setFromSearchSting(fromLocationSearchSting);
      setToSearchSting(toLocationSearchSting);
      setToLatitude(toLocationLat);
      setToLongitude(toLocationLng);
      setFromLatitude(fromLocationLat);
      setFromLongitude(fromLocationLng);
      setTripHrs(hrs);
      setTripMins(mins);
    }

    // resetFilter();

    setIsMapMoved(false);
    setSearchType('radius');
    search(
      { searchBy: method, calledFrom: 'header' },
      method === 'trip' ? 1 : 0,
      true,
      {
        ...(method === 'location'
          ? {
              latitude: lat,
              longitude: lng,
            }
          : {
              source: [fromLocationLat, fromLocationLng],
              destination: [toLocationLat, toLocationLng],
              time: [hrs, mins],
            }),
        type: 'radius',
        sort_by: sortType,
        ...removeNullValuesFromObject(appliedFilters),
      },
    );
  };

  const handleScroll = e => {
    // console.log(window.scrollY);
    const mobileMap = 200;
    if (window.scrollY > mobileMap) {
      setMobileMapFixed(true);
    }

    if (window.scrollY < mobileMap) {
      setMobileMapFixed(false);
    }
  };

  useEffect(() => {
    // add when mounted
    document.addEventListener('scroll', handleScroll);
    // return function to be called when unmounted
    return () => {
      document.removeEventListener('scroll', handleScroll);
    };
  }, []);

  useEffect(
    () => {
      if (global.window && window.screen.width < 500) {
        setMobileView(true);
      } else {
        setMobileView(false);
      }
    },
    [global.window && global.window.screen.width],
  );

  useEffect(() => {}, [props]);

  const onAuthSuccess = ({
    child_care_type: organisation,
    schedule_type: schedule,
    transport_facility: transportation,
    food_facility: food,
  }) => {
    Router.replace(
      // `/daycare/search`,
      `/daycare/search?search_type=${locationSearchMethod}&locationString=${locationSearchSting}&toLocationString=${toSearchSting}&fromLocationString=${fromSearchSting}&${
        locationSearchMethod === 'trip'
          ? `fromLat=${fromLatitude}&toLat=${toLatitude}&fromLng=${fromLongitude}&toLng=${toLongitude}&timehrs=${tripHrs}&timemins=${tripMins}`
          : `latitude=${locationLat}&longitude=${locationLng}`
      }&organisation=${organisation}&transportation=${transportation}&food=${food}&schedule_type=${
        SCHEDULE_CONFIG[schedule]
      }&sort_by=${sort_by}&type=radius`,
      // { shallow: true },
    );

    setLocationSearchSting(locationString);
    setLocationLat(latitude);
    setLocationLng(longitude);
    setSortType(sort_by);
    setSearchType('radius');

    modifyFilter({
      organisation,
      transportation,
      food,
      schedule_type: [SCHEDULE_CONFIG[schedule]],
    });

    search({ searchBy: locationSearchMethod }, 0, true, {
      latitude,
      longitude,
      ...removeNullValuesFromObject({
        ...appliedFilters,
        organisation,
        transportation,
        food,
        schedule_type: [SCHEDULE_CONFIG[schedule]],
      }),
    });
  };

  const filtersOnChange = (key, value, actionType) => {
    const tempFilters = { ...filterState };

    // make live streaming null when cctv options there
    if (key === 'cctv' && (value == 0 || value == 1))
      tempFilters.live_streaming = null;

    // make cctv null when live fees options there
    if (key === 'live_streaming' && (value == 0 || value == 1))
      tempFilters.cctv = null;

    if (key !== 'schedule_type') {
      tempFilters[key] = value;
    } else {
      if (actionType === 'add') tempFilters[key].push(value);
      else {
        const index = tempFilters[key].indexOf(value);
        if (index > -1) {
          tempFilters[key].splice(index, 1);
        }
      }
    }

    setFilterState(tempFilters);
  };

  const onSliderChange = val => {
    const temp = { ...filterState };
    setFilterState({ ...temp, ...val });
  };

  const resetFilter = e => {
    setFilterState({ ...FILTERS_INITIAL_STATE });
    onApplyFilter(e, FILTERS_INITIAL_STATE, locationSearchMethod);
    Router.replace(
      // `/daycare/search`,
      `/daycare/search?search_type=${locationSearchMethod}&locationString=${locationSearchSting}&toLocationString=${toSearchSting}&fromLocationString=${fromSearchSting}&${
        locationSearchMethod === 'trip'
          ? `fromLat=${fromLatitude}&toLat=${toLatitude}&fromLng=${fromLongitude}&toLng=${toLongitude}&timehrs=${tripHrs}&timemins=${tripMins}`
          : `latitude=${locationLat}&longitude=${locationLng}`
      }&type=${searchType}`,
      // { shallow: true },
    );
  };

  const scrollSensorCallback = ({ isVisbile, slug }) => {
    if (mobileView) {
      if (isVisbile && locationSearchMethod !== 'trip') {
        console.log(isVisbile, slug);
        setMobileViewCurrentSchoolSlug(slug);
      }
      setNewCenter(slugToSearchCoordinatesMap[slug]);
    }
  };

  useEffect(
    () => {
      setFilterState({ ...FILTERS_INITIAL_STATE, ...appliedFilters });
    },
    [appliedFilters],
  );

  useEffect(
    () => {
      console.log(mobileViewCurrentSchoolSlug);
      if (
        mobileView &&
        !mobileViewCurrentSchoolSlug &&
        SearchResults.length > 0
      ) {
        setMobileViewCurrentSchoolSlug(SearchResults[0].slug);
        setNewCenter(slugToSearchCoordinatesMap[SearchResults[0].slug]);
      }
    },
    [SearchResults],
  );

  const queryChange = (e, param, extra = {}) => {
    const query = Qs.stringify(removeNullValuesFromObject(param), {
      arrayFormat: 'brackets',
    });

    Router.replace(
      // `/daycare/search`,
      `/daycare/search?search_type=${locationSearchMethod}&${
        locationSearchMethod === 'trip'
          ? `fromLat=${fromLatitude}&toLat=${toLatitude}&fromLng=${fromLongitude}&toLng=${toLongitude}&timehrs=${tripHrs}&timemins=${tripMins}`
          : `latitude=${locationLat}&longitude=${locationLng}`
      }&locationString=${locationSearchSting}&toLocationString=${toSearchSting}&fromLocationString=${fromSearchSting}&sort_by=${extra.sortType ||
        sortType}&type=${extra.searchMode || searchType}&${query}`,
      // { shallow: true },
    );
  };

  const locationDisplayName =
    locationSearchMethod === 'trip'
      ? `${
          fromSearchSting.length > 20
            ? `${fromSearchSting.slice(0, 20)}...`
            : fromSearchSting
        } - ${
          toSearchSting.length > 20
            ? `${toSearchSting.slice(0, 20)}...`
            : toSearchSting
        }`
      : locationSearchSting;

  const cityData = [
    {
      city: 'bengaluru',
      data: [
        'Bannerghetta',
        'Gottigere',
        'BTM Layout',
        'HSR Layout',
        'Indiranagar',
        'Akshayanagar',
        'Whitefield',
        'Ananthnagar',
        'Electronic city',
        'Varthur',
        'Kadugodi',
        'Frazer Town',
        'Marathahalli',
        'Haralur',
        'Sarjapur',
        'Bellandur',
        'Basavanagudi',
        'Govindraj nagar',
        'Koramangala',
        'Chandra Layout',
        'Mathikere',
        'KR Puram',
        'Vijayanagar',
        'Yelahanka',
        'JP Nagar',
        'Nagarbhavi',
        'Jayanagar',
        'Singsandra',
      ],
    },
    {
      city: 'pune',
      data: [
        'Kharadi',
        'Baner',
        'Hadapsar',
        'Kothrud',
        'Wagholi',
        'Aundh',
        'Pimpri Chinchwad',
        'Wakad',
        'Magarpatta',
        'Warje',
        'Lohegaon',
        'Kondhwa',
        'Talegaon',
        'Pimple Saudagar',
        'Bavdhan',
        'Kalyani Nagar',
        'Moshi',
        'Hinjawadi',
      ],
    },
  ];

  const cityObject = cityData.find(
    city => city?.city?.toLowerCase() === props?.query?.city?.toLowerCase(),
  );

  return (
    <>
      <Header
        type="secondary"
        screenWidth
        searchBar
        placeHolderValue={locationDisplayName}
        onSearch={onNewLocationHeaderSearch}
        isModalActive={modalStatus}
        activeModalType={modalType}
        setActiveCallback={setModalStatus}
        // disableAuthActionButtons
        // disableNavItems={!isLoggedIn}
        // disableLoginModalCloseIcon
        locationSearchMethod={locationSearchMethod}
        setLocationSearchMethod={setLocationSearchMethod}
        onOtpSuccessCallback={onAuthSuccess}
        onOnboardingSuccessCallback={onAuthSuccess}
        clickType={restrictionType}
        {...props}
      />
      <Filter
        type="primary"
        setActive={setActive}
        setModalType={setModalType}
        blockSearch={
          isLoggedIn
            ? filterClickCount >= CLICK_COUNTS.filterApplication
            : false
        }
        // onApplyFilter={onApplyFilter}
        onApplyFilter={(e, fltrs, mthd) => {
          e.preventDefault();
          e.stopPropagation();
          if (isLoggedIn) {
            if (filterClickCount >= CLICK_COUNTS.filterApplication) {
              setModalType('restriction');
              setRestrictionType('filter search');
              setModalStatus(true);
            } else {
              postCount('filters_application');
              onApplyFilter(e, fltrs, mthd);
            }
          } else {
            onApplyFilter(e, fltrs, mthd);
          }
        }}
        sentFilters={appliedFilters}
        appliedFilters={filterState}
        onSliderChange={onSliderChange}
        filtersOnChange={filtersOnChange}
        queryChange={queryChange}
        resetFilter={resetFilter}
        searchMethod={locationSearchMethod}
        filterSectionActive={filterSectionActive}
        setFilterSectionActive={setFilterSectionActive}
      />
      <MainWrapper
        className="searchResult"
        background="#fff"
        paddingTop="164px"
      >
        <Flex wrap>
          <MainContent column flexWidth="66.5%" flexPadding="35px 0px 0px">
            {(isLoadMoreTriggered ? (
              true
            ) : (
              !searchLoader
            )) ? (
              SearchResults.length > 0 ? (
                <>
                  <NotFoundWrapper className="found">
                    <Flex
                      flexPadding="0px 0px 30px 0px"
                      className="schoolNameTitle"
                      style={{ borderBottom: '1px solid #EAEDF2' }}
                    >
                      <Flex
                        column
                        flexWidth="80%"
                        className="searchTitleWrapper"
                      >
                        <Flex alignEnd className="searchTitle">
                          <SearchReasultsAddr className="toolTipWrapper toolTipLocation">
                            {searchType === 'radius' || !isDragToSearchInitiated
                              ? `${count} preschools, daycares, and playschools in ${locationDisplayName} `
                              : `${count} Results found`}

                            {searchType === 'radius' && (
                              <div className="msgTooltip">
                                <span>
                                  {locationSearchMethod === 'trip'
                                    ? `${fromSearchSting} / ${toSearchSting}`
                                    : locationSearchSting}
                                </span>
                              </div>
                            )}
                          </SearchReasultsAddr>
                        </Flex>
                        {!locationSearchSting && (
                          <Flex alignCenter style={{ marginTop: '10px' }}>
                            {/* TEMP */}
                            {/* <TimeInfo>
                        <Flex alignCenter>
                          <span
                            className="iconify"
                            data-icon="simple-line-icons:location-pin"
                            data-inline="false"
                          />
                          <Time>20-40 min</Time>
                          <Distance>(4kms)</Distance>
                        </Flex>
                      </TimeInfo>
                      <TimeInfo style={{ marginLeft: '10px' }}>
                        <Flex alignCenter>
                          <span
                            className="iconify"
                            data-icon="simple-line-icons:clock"
                            data-inline="false"
                          />
                          <Time>2:30 PM - 3:15 PM</Time>
                        </Flex>
                      </TimeInfo> */}
                          </Flex>
                        )}
                      </Flex>
                      <Flex
                        alignEnd
                        className="searchSortBy"
                        justifyEnd
                        style={{ flexGrow: '1' }}
                      >
                        <Dropdown
                          type="sort"
                          sortType={sortType}
                          setSortType={(e, val) => {
                            setSortType(val);
                            onApplyFilter(
                              e,
                              {
                                ...appliedFilters,
                                sort_by: val,
                              },
                              locationSearchMethod,
                            );
                            queryChange(e, appliedFilters, { sortType: val });
                          }}
                        />
                      </Flex>
                    </Flex>
                    <FixedMapWrapper
                      className={`mobileMap ${mobileMapFixed ? 'fixed' : ''}`}
                    >
                      <MapWrapper
                        flexWidth="100%%"
                        flexHeight="auto"
                        whitebg="#FFF"
                      >
                        <MapLoction>
                          {/* <img src={background} alt="" /> */}
                          {/* <img src={fromName ? background1 : background} alt="" /> */}
                          <Map
                            locationCluster={searchCoordinates}
                            lat={locationLat || latitude}
                            lng={locationLng || longitude}
                            searchType={searchType}
                            searchAsBoundsChage={searchAsBoundsChage}
                            newCenter={newCenter}
                            setNewCenter={setNewCenter}
                            onQueryChange={(e, cords) =>
                              queryChange(
                                e,
                                { ...appliedFilters, ...cords },
                                'bounds',
                              )
                            }
                            searchMethod={locationSearchMethod}
                            destination={{
                              lat: +fromLatitude,
                              lng: +fromLongitude,
                            }}
                            origin={{ lat: +toLatitude, lng: +toLongitude }}
                          />
                          {/* NOTE: As told by designer, the option for 'seach as i move' is removed  */}
                          {/* <SearchCheckbox>
                                <CheckBox
                                  label="Search as I move the map"
                                  id="id10"
                                  margin="0px 25px 0px 0px"
                                  filterFont="13px"
                                  filterChekbox="5px"
                                  filterLineheight="15px"
                                  checked={searchType !== 'radius'}
                                  onClickFunction={e => {
                                    e.preventDefault();
                                    if (searchType === 'radius') {
                                      setSearchType('bounds');
                                    } else {
                                      queryChange(e, appliedFilters, 'radius');
                                      setSearchType('radius');
                                      setIsMapMoved(false);
                                      if (isDragToSearchInitiated) {
                                        // resetFilter();
                                        setNewCenter({
                                          lat: +locationLat,
                                          lng: +locationLng,
                                        });
                                        search(0, true, {
                                          latitude: locationLat,
                                          longitude: locationLng,
                                          type: 'radius',
                                          ...appliedFilters,
                                        });
                                      }
                                    }
                                  }}
                                />
                              </SearchCheckbox>
                            */}
                        </MapLoction>
                      </MapWrapper>
                    </FixedMapWrapper>
                    <Flex
                      wrap
                      className={`preschoolSearchResults ${
                        mobileView && mobileMapFixed ? 'fixed' : ''
                      }`}
                    >
                      {loading &&
                        SearchResults.map((d, i) => (
                          <div
                            style={{ display: 'flex', flexDirection: 'column' }}
                            className="loadingSkeleton"
                          >
                            <Skeleton height={170} width={273} />
                            <Skeleton height={20} width={138} />
                            <Skeleton height={40} width={273} />
                          </div>
                        ))}
                      {!loading &&
                        SearchResults.map((d, i) => (
                          <>
                            <SchoolCard
                              scrollSensorCallback={isVisbile =>
                                scrollSensorCallback({
                                  isVisbile,
                                  slug: d.slug,
                                })
                              }
                              slug={d.slug}
                              onFocus={e => setHoveredState(e)}
                              isLink={
                                isLoggedIn
                                  ? !(
                                      schoolProfileClickCount >=
                                      CLICK_COUNTS.schoolProfileClick
                                    )
                                  : true
                              }
                              onSchoolCardClick={() => {
                                setModalType('restriction');
                                setRestrictionType('school profile visit');
                                setModalStatus(true);
                              }}
                              isLoggedIn={isLoggedIn}
                              isWishlisted={d.wishlist_id}
                              onWishlistIconClick={() => {
                                if (!d.wishlist_id) {
                                  if (locationSearchMethod === 'location') {
                                    postWishlist(d.id, 'search');
                                  } else {
                                    postWishlist(d.id, 'trip');
                                  }
                                } else {
                                  if (locationSearchMethod === 'location') {
                                    deleteWishlist(d.id, 'search');
                                  } else {
                                    deleteWishlist(d.id, 'trip');
                                  }
                                }
                              }}
                              category={
                                PROVIDER_MAP[+d.child_care_providers - 1]
                              }
                              schoolImage={
                                d.thumb_url || d.url
                                  ? (d.thumb_url || d.url).includes('://')
                                    ? d.thumb_url || d.url
                                    : `https://${d.thumb_url || d.url}`
                                  : i < 6
                                    ? SCHOOL_PLACEHOLDERS[i]
                                    : schoolPlaceholderImageMapper(i)
                              }
                              schoolName={d.name}
                              schoolId={d.id}
                              schoolBranch={d.area}
                              cardtype="schoolCardColumn"
                              schoolCardWidth="100%"
                              schoolCardHeight="auto"
                              schoolWrapperMarTop="30px"
                              schoolRating={null}
                              photocardType="liked"
                              schoolDesc=""
                              redirectionLink={{
                                pathname: `/${d?.city?.toLowerCase()}/${d.area
                                  .replace(/\s/g, '-')
                                  .replace(/,./g, '-')
                                  .toLowerCase()}/${d.slug}`,
                              }}
                              hasBoxShadow={
                                mobileView &&
                                d.slug === mobileViewCurrentSchoolSlug
                              }
                              // redirectionLink={`/daycare/${d.id}?wishlist=${
                              //   d.wishlist_id
                              // }`}

                              // schoolDesc="8:30AM-3:00PM · Full time · Flexible · Part time"
                              // schoolRating={"4.3"}
                              // photocardType="featured"
                              // openingStatus="Immediate Open"
                            />
                          </>
                        ))}
                    </Flex>
                    {count !== SearchResults.length && (
                      <Flex justifyCenter flexMargin="70px 0px 50px 0px">
                        <Button
                          text="Load More"
                          type="outline"
                          onClick={() => {
                            // next_page,
                            // max_page,
                            if (locationSearchMethod === 'trip') {
                              onSearchLoadMore(next_page, 'trip');
                            } else {
                              onSearchLoadMore(
                                SearchResults.length,
                                'location',
                              );
                            }
                          }}
                          isLoading={searchLoader}
                        />
                      </Flex>
                    )}
                  </NotFoundWrapper>
                </>
              ) : (
                <>
                  {/* {isLoggedIn && ( */}
                  <NotFoundWrapper>
                    {/* {searchType === 'radius' || !isDragToSearchInitiated ? (
                      <>
                        <NotFoundMsg className="toolTipWrapper">
                          Unfortunately, we could not find any schools in{' '}
                          {(locationSearchSting || '').length > 25 ? (
                            <span className="toolTipLocation">
                              {locationSearchSting.slice(0, 25)}
                              ...
                            </span>
                          ) : (
                            locationSearchSting
                          )}
                          .{' '}
                          <div className="msgTooltip">
                            <span>{locationSearchSting}</span>
                          </div>
                        </NotFoundMsg>
                      </>
                    ) : (
                      <NotFoundMsg className="toolTipWrapper">
                        Unfortunately, we could not find any schools matching
                        your query.
                      </NotFoundMsg>
                    )}
                    <NotFoundInfo>
                      Try searching in areas currently supported by Kiddenz.
                    </NotFoundInfo> */}
                    {/* {console.log(checkIfFiltersChnaged(appliedFilters))} */}
                    <Refresh
                      isFilterApplied={checkIfFiltersChnaged(appliedFilters)}
                    />

                    {/* NOTE: As discusses with vinod 
                    collection email and mobile when results not found 
                    is ON HOLD as of now
                    */}

                    {/* <NoResultform>
                      <NoResultLabel htmlFor="username">
                        Mobile Number
                        <Input
                          id="not_found_contact"
                          type="text"
                          placeholder=""
                          value={phone}
                          inputBorder="	1px solid #C0C8CD"
                          onChange={e =>
                            onNotFoundInputsChangeHandler(e, 'phone')
                          }
                          onBlur={e => onNotFoundInputsBlurHandler(e, 'phone')}
                        />
                        <InputError>{error.phone}</InputError>
                      </NoResultLabel>
                      <NoResultLabel htmlFor="username">
                        E-mail ID
                        <Input
                          id="not_found_email"
                          type="text"
                          placeholder=""
                          value={email}
                          inputBorder="	1px solid #C0C8CD"
                          onChange={e =>
                            onNotFoundInputsChangeHandler(e, 'email')
                          }
                          onBlur={e => onNotFoundInputsBlurHandler(e, 'email')}
                        />
                        <InputError>{error.email}</InputError>
                      </NoResultLabel>
                      <div style={{ width: '166px' }}>
                        <Button
                          text="Submit"
                          marginTop="18px"
                          marginRight="auto"
                          type="secondary"
                          headerButton
                          onClick={submitContactHandler}
                          isLoading={submitContactLoader}
                        />
                      </div>
                    </NoResultform>
                 */}
                  </NotFoundWrapper>
                  {/* )} */}
                  <Flex column>
                    {suggestionsCount ? (
                      <TextMsg className="toolTipWrapper">
                        Please check Preschools/Daycares near “
                        {locationSearchSting.length > 25
                          ? `${locationSearchSting.slice(0, 25)}...`
                          : locationSearchSting}
                        ” that meet your requirements.
                        <div className="msgTooltip">
                          <span>{locationSearchSting}</span>
                        </div>
                      </TextMsg>
                    ) : (
                      ''
                    )}
                    <Flex wrap>
                      {suggestions.map(d => (
                        <SchoolCard
                          isLoggedIn={isLoggedIn}
                          isWishlisted={d.wishlist_id}
                          onWishlistIconClick={() => {
                            if (!d.wishlist_id)
                              postWishlist(d.id, 'suggestion');
                            else deleteWishlist(d.id, 'suggestion');
                          }}
                          category={PROVIDER_MAP[+d.child_care_providers - 1]}
                          schoolImage={
                            d.thumb_url || d.url
                              ? (d.thumb_url || d.url).includes('://')
                                ? d.thumb_url || d.url
                                : `https://${d.thumb_url || d.url}`
                              : i < 3
                                ? SCHOOL_PLACEHOLDERS[i]
                                : schoolSuggestionPlaceholderImageMapper(i)
                          }
                          schoolName={
                            d.name.length > 24
                              ? `${d.name.slice(0, 24)}...`
                              : d.name
                          }
                          schoolBranch={d.area}
                          cardtype="schoolCardColumn"
                          schoolCardWidth="100%"
                          schoolCardHeight="auto"
                          redirectionLink={`/daycare/${d.state}/${d.city}/${
                            d.area
                          }/${d.slug}?wishlist=${d.wishlist_id}&name=${
                            d.name
                          }&type=${PROVIDER_MAP[+d.child_care_providers - 1]}`}
                          // schoolRating="4.3"
                          // photocardType="liked"
                          // schoolDesc="8:30AM-3:00PM · Full time · Flexible · Part time"
                        />
                      ))}
                    </Flex>
                    {suggestionsCount !== suggestions.length && (
                      <Flex justifyCenter flexMargin="70px 0px 160px 0px">
                        <Button
                          text="Load More"
                          type="outline"
                          onClick={() => {
                            if (isLoggedIn) {
                              loadMoreSuggestions(suggestions.length);
                            } else {
                              setModalStatus(true);
                              setModalType('login');
                            }
                          }}
                          isLoading={dayCareSuggestionsLoader}
                        />
                      </Flex>
                    )}
                  </Flex>
                </>
              )
            ) : (
              !isLoadMoreTriggered && (
                <SearchLoader>
                  <Flex
                    alignCenter
                    justifyCenter
                    flexWidth="100%"
                    flexHeight="100%"
                  >
                    <img src={pulseLoader} alt="" height={75} width={75} />
                  </Flex>
                </SearchLoader>
              )
            )}
            <FilterFooter>
              Be a part of Kiddenz.
              <FilterLink onClick={() => Router.push(`/provider`)}>
                Register your school
              </FilterLink>
            </FilterFooter>
          </MainContent>
          <FixedMapWrapper className="desktopMap">
            <MapWrapper flexWidth="100%" flexHeight="auto" whitebg="#FFF">
              <MapLoction>
                {/* <img src={background} alt="" /> */}
                {/* <img src={fromName ? background1 : background} alt="" /> */}
                <Map
                  locationCluster={searchCoordinates}
                  lat={locationLat || latitude}
                  lng={locationLng || longitude}
                  searchType={searchType}
                  searchAsBoundsChage={searchAsBoundsChage}
                  newCenter={newCenter}
                  setNewCenter={setNewCenter}
                  onQueryChange={(e, cords) =>
                    queryChange(e, { ...appliedFilters, ...cords }, 'bounds')
                  }
                  searchMethod={locationSearchMethod}
                  destination={{ lat: +fromLatitude, lng: +fromLongitude }}
                  origin={{ lat: +toLatitude, lng: +toLongitude }}
                  isMobileView={mobileView}
                  mobileViewCurrentSchoolSlug={mobileViewCurrentSchoolSlug}
                />
                {/* NOTE: As told by designer, the option for 'seach as i move' is removed  */}
                {/* <SearchCheckbox>
              <CheckBox
                label="Search as I move the map"
                id="id10"
                margin="0px 25px 0px 0px"
                filterFont="13px"
                filterChekbox="5px"
                filterLineheight="15px"
                checked={searchType !== 'radius'}
                onClickFunction={e => {
                  e.preventDefault();
                  if (searchType === 'radius') {
                    setSearchType('bounds');
                  } else {
                    queryChange(e, appliedFilters, 'radius');
                    setSearchType('radius');
                    setIsMapMoved(false);
                    if (isDragToSearchInitiated) {
                      // resetFilter();
                      setNewCenter({
                        lat: +locationLat,
                        lng: +locationLng,
                      });
                      search(0, true, {
                        latitude: locationLat,
                        longitude: locationLng,
                        type: 'radius',
                        ...appliedFilters,
                      });
                    }
                  }
                }}
              />
            </SearchCheckbox>
          */}
              </MapLoction>
            </MapWrapper>
          </FixedMapWrapper>
        </Flex>

        <CityHeading>
          More About {router.query.area}, {router.query.city}
        </CityHeading>

        <CityDescriptionStyle>{cityDescription}</CityDescriptionStyle>

        {cityObject && (
          <FatFooterContainer>
            <div>
              <CityTitle>
                Preschools in popular areas, {props.query.city}
              </CityTitle>
              <GridAutoWraper>
                {cityObject.data.map(area => (
                  <div key={area}>
                    <Link
                      href={`/${props.query.city}/${area}`}
                      className="schoolLink"
                      legacyBehavior
                    >
                      <a>Preschools in {area}</a>
                    </Link>
                  </div>
                ))}
              </GridAutoWraper>
            </div>
            <br />
            <div>
              <CityTitle>
                Daycares in popular areas, {props.query.city}
              </CityTitle>
              <GridAutoWraper>
                {cityObject.data.map(area => (
                  <div key={area}>
                    <Link
                      href={`/${props.query.city}/${area}`}
                      className="schoolLink"
                      legacyBehavior
                    >
                      <a>Daycares in {area}</a>
                    </Link>
                  </div>
                ))}
              </GridAutoWraper>
            </div>
            <br />
            <div>
              <CityTitle>Popular Preschools & Daycares</CityTitle>
              <GridAutoWraper>
                <Link
                  href="/bengaluru/vidyaranyapura/cherish-kidz-an-indi-international-preschool-and-daycare-1820952"
                  className="school_link"
                >
                  Cherish Kids
                </Link>
                <Link
                  href="/bengaluru/k-r-puram/discovery-montessori-usa-1785493"
                  className="school_link"
                >
                  Discovery Montessori USA
                </Link>
                <Link
                  href="/bangalore/padmanabhanagar/ace-montessori-1783520"
                  className="school_link"
                >
                  Ace Montessori
                </Link>
                <Link
                  href="/bengaluru/vijaynagar/step-by-step-the-learning-center-1787911"
                  className="school_link"
                >
                  Step by Step The Learning Center
                </Link>
                <Link
                  href="/bangalore/gottigere/aura-montessori-house-of-children-1788833"
                  className="school_link"
                >
                  Aura Montessori
                </Link>
                <Link
                  href="/bangalore/gottigere/the-pearls-montessori-1795828"
                  className="school_link"
                >
                  The Pearls Montessori
                </Link>
                <Link
                  href="/bangalore/gottigere-banerghatta/greenwood-high-preschool-227730"
                  className="school_link"
                >
                  Greenwood High
                </Link>
                <Link
                  href="/bengaluru/bohra-layout-gottigere/kalpaa-pre-school-1739888"
                  className="school_link"
                >
                  Kalpaa Preschools
                </Link>
                <Link
                  href="/bengaluru/koramangala/united-world-academy-1713483"
                  className="school_link"
                >
                  United World Academy
                </Link>
                <Link
                  href="/bengaluru/koramangala-1st-block/koala-preschool-181624"
                  className="school_link"
                >
                  Koala
                </Link>
                <Link
                  href="/bengaluru/koramangala/podar-jumbo-kids-393240"
                  className="school_link"
                >
                  Podar Jumbo Kids
                </Link>
                <Link
                  href="/bangalore/rpc-layout/st-michaels-kindergarten-1790584"
                  className="school_link"
                >
                  St. Michaels Kindergarten
                </Link>
                <Link
                  href="/bangalore/moodalapalya/kidsbee-play-home-1793959"
                  className="school_link"
                >
                  Kidsbee Playhome
                </Link>
                <Link
                  href="/bengaluru/govindraj-nagar/akshara-little-bees-pre-school-1786216"
                  className="school_link"
                >
                  Akshara Little Bees
                </Link>
                <Link
                  href="/bengaluru/v-v-puram/edumeta-the-i-school-1794859"
                  className="school_link"
                >
                  Edumeta the Ischool
                </Link>
                <Link
                  href="/bengaluru/vidyaranyapura/kidzee-vidyaranyapura-1798722"
                  className="school_link"
                >
                  Kidzee Vidyaranyapura
                </Link>
                <Link
                  href="/bengaluru/vidyaranyapura/gurukull-vidyalaya-1797820"
                  className="school_link"
                >
                  Gurukull Vidyalaya
                </Link>
                <Link
                  href="/bengaluru/phase-1/rainbow-kids-international-preschool-and-daycare-ananth-nagar-1799426"
                  className="school_link"
                >
                  Rainbow Kids International
                </Link>
                <Link
                  href="/bengaluru/kadugodi/vedavihaan-the-global-school-best-pre-school-day-care-belathur-campus-3-1815367"
                  className="school_link"
                >
                  Vedavihaan
                </Link>
                <Link
                  href="/bangalore/bannerghatta-road,/inspiro-preschool-daycare-1814585"
                  className="school_link"
                >
                  Inspiro Preschools
                </Link>
                <Link
                  href="/pune/kharadi/euro-kids-kharadi-1809387"
                  className="school_link"
                >
                  Eurokids Kharadi
                </Link>
                <Link
                  href="/bengaluru/vijaynagar/jaanvi-preschool-1807442"
                  className="school_link"
                >
                  Jaanvi Preschools
                </Link>
                <Link
                  href="/bengaluru/rmv-2nd-stage/20-1806636"
                  className="school_link"
                >
                  Kids Castle
                </Link>
                <Link
                  href="/bengaluru/jayanagar/podar-jumbo-kids-plus-1816916"
                  className="school_link"
                >
                  Podar Jumbo
                </Link>
              </GridAutoWraper>
            </div>
          </FatFooterContainer>
        )}
      </MainWrapper>

      <Footer
        bgColor="#613A95"
        className="straight"
        setFilterState={setFilterState}
        filterState={filterState}
        {...props}
      />

      {active && (
        <Modal
          type="default"
          setActive={setActive}
          sentFilters={filterState}
          blockSearch={
            isLoggedIn
              ? filterClickCount >= CLICK_COUNTS.filterApplication
              : false
          }
          onApplyFilter={(e, fltrs, mthd) => {
            e.preventDefault();
            e.stopPropagation();
            if (isLoggedIn) {
              if (filterClickCount >= CLICK_COUNTS.filterApplication) {
                setModalType('restriction');
                setRestrictionType('filter search');
                setModalStatus(true);
              } else {
                postCount('filters_application');
                onApplyFilter(e, fltrs, mthd);
              }
            } else {
              onApplyFilter(e, fltrs, mthd);
            }
          }}
          resetFilter={resetFilter}
          closeHandler={() => setActive(false)}
          // appliedFilters={filterState}
          onSliderChange={onSliderChange}
          filtersOnChange={filtersOnChange}
          queryChange={queryChange}
          searchMethod={locationSearchMethod}
          setFilterSectionActive={setFilterSectionActive}
        />
      )}
    </>
  );
}

const checkIfFiltersChnaged = obj =>
  !Object.entries(obj)
    .map(([key, value]) => {
      if (key === 'schedule_type') {
        if (value && value.length > 0) return true;
        // if (value && value.length > 0) return false;
        return true;
        // eslint-disable-next-line no-else-return
      } else {
        if (value === null) return true;
        return false;
      }
    })
    .includes(false);

const FILTERS_INITIAL_STATE = {
  organisation: null,
  transportation: null,
  food: null,
  course_type: null,
  // min_age: null,
  max_age: null,
  min_price: null,
  max_price: null,
  schedule_type: [],
  cctv: null,
  live_streaming: null,
  ac_room: null,
  free_trial: null,
  outdoor_play: null,
  parent_app: null,
  massaging_bathing: null,
  extended_hours: null,
  special_care: null,
  activities: null,
  tution: null,
  min_extended_hours: null,
  max_extended_hours: null,
  daycareprice: null,
  daycare_min_price: null,
  daycare_max_price: null,
};

const Map = React.memo(
  ({
    locationCluster,
    lat = 0.0,
    lng = 0.0,
    searchType,
    searchAsBoundsChage,
    newCenter = {},
    setNewCenter,
    // onQueryChange,
    searchMethod,
    destination,
    origin,
    mobileViewCurrentSchoolSlug,
    isMobileView,
  }) => {
    const [savedMap, saveMap] = useState(null);
    const [activeMarkerInfo, setActiveMarkerInfo] = useState(null);
    const schoolCardRef = useRef(null);

    // const [activeMarker, setActiveMarker] = useState(null);
    const handleClick = e => {
      console.log(e);
      if (
        schoolCardRef &&
        schoolCardRef.current &&
        schoolCardRef.current.contains(e.target)
      ) {
        return 0;
      }
      setActiveMarkerInfo(null);
      return 0;
    };

    useEffect(() => {
      // add when mounted
      document.addEventListener('mousedown', handleClick);

      // return function to be called when unmounted
      return () => {
        document.removeEventListener('mousedown', handleClick);
      };
    }, []);

    React.useEffect(
      () => {
        if (savedMap) {
          savedMap.panTo(newCenter);
          fitToBounds([
            { lat: +lat, lng: +lng },
            { lat: +newCenter.lat, lng: +newCenter.lng },
          ]);
        }
      },
      [newCenter],
    );

    const fitToBounds = path => {
      if (savedMap) {
        const bounds = new google.maps.LatLngBounds();
        path.map(position => {
          bounds.extend(position);
          return 0;
        });
        savedMap.fitBounds(bounds);
      }
    };

    const renderMap = () => {
      const options = {
        streetViewControl: false,
        fullscreenControl: false,
        mapTypeControl: false,
        styles: MAP_STYLE,
        // restriction: {
        //   latLngBounds: {
        //     sw: LatLng(23.63936, 68.14712),
        //     ne: LatLng(28.20453, 97.34466),
        //   },
        //   // LatLngBounds(
        //   //   LatLng(23.63936, 68.14712),
        //   //   LatLng(28.20453, 97.34466),
        //   // ),
        //   strictBounds: true,
        // },
      };

      return (
        <GoogleMap
          className="googleMap"
          onLoad={map => {
            saveMap(map);
          }}
          options={options}
          mapContainerStyle={{
            height: 'calc(100vh - 166px)',
            width: '100%',
            border: '0px solid #613A95',
          }}
          zoom={13}
          center={
            searchMethod === 'trip'
              ? {}
              : {
                  lat: newCenter.lat || +lat,
                  lng: newCenter.lng || +lng,
                }
          }
          onDragEnd={e => {
            // setNewCenter({
            //   lat: savedMap.getCenter().lat(),
            //   lng: savedMap.getCenter().lng(),
            // });

            if (searchType === 'bounds') {
              const neLat = savedMap
                .getBounds()
                .getNorthEast()
                .lat();
              const neLng = savedMap
                .getBounds()
                .getNorthEast()
                .lng();
              const seLat = savedMap
                .getBounds()
                .getSouthWest()
                .lat();
              const seLng = savedMap
                .getBounds()
                .getSouthWest()
                .lng();

              // onQueryChange(e, {
              //   ne_lat: +neLat.toFixed(5),
              //   ne_lng: +neLng.toFixed(5),
              //   sw_lat: +seLat.toFixed(5),
              //   sw_lng: +seLng.toFixed(5),
              // });
              searchAsBoundsChage({
                neLat: +neLat.toFixed(5),
                neLng: +neLng.toFixed(5),
                seLat: +seLat.toFixed(5),
                seLng: +seLng.toFixed(5),
              });
            }
          }}
          onZoomChanged={e => {
            if (searchType === 'bounds') {
              const neLat = savedMap
                .getBounds()
                .getNorthEast()
                .lat();
              const neLng = savedMap
                .getBounds()
                .getNorthEast()
                .lng();
              const seLat = savedMap
                .getBounds()
                .getSouthWest()
                .lat();
              const seLng = savedMap
                .getBounds()
                .getSouthWest()
                .lng();

              // onQueryChange(e, {
              //   ne_lat: +neLat.toFixed(5),
              //   ne_lng: +neLng.toFixed(5),
              //   sw_lat: +seLat.toFixed(5),
              //   sw_lng: +seLng.toFixed(5),
              // });
              searchAsBoundsChage({
                neLat: +neLat.toFixed(5),
                neLng: +neLng.toFixed(5),
                seLat: +seLat.toFixed(5),
                seLng: +seLng.toFixed(5),
              });
            }
          }}
        >
          {locationCluster.map((x, i) => {
            const {
              slug,
              address,
              name,
              city,
              state,
              pincode,
              area,
              img,
              marker,
              ...location
            } = x;

            return (
              <>
                <Marker
                  // onLoad={() => {
                  //   if (isMobileView && slug === mobileViewCurrentSchoolSlug) {
                  //     setActiveMarker(slug);
                  //   }
                  // }}
                  key={`MAP_${String(i)}`}
                  position={{
                    lat: location.lat,
                    lng: location.lng,
                  }}
                  icon={marker}
                  // onLoad={marker => {
                  //   marker.setIcon(
                  //     isMobileView && slug === mobileViewCurrentSchoolSlug
                  //       ? activeLocationMarker
                  //       : markerIcon,
                  //   );
                  // }}
                  onClick={() => setActiveMarkerInfo(i)}
                />
                {activeMarkerInfo === i ? (
                  <InfoWindow
                    onCloseClick={() => setActiveMarkerInfo(null)}
                    position={{
                      lat: location.lat,
                      lng: location.lng,
                    }}
                  >
                    <div ref={schoolCardRef}>
                      <Link
                        href="/daycare/[state]/[city]/[area]/[daycare-id]"
                        as={`/daycare/${state || null}/${city || null}/${area ||
                          null}/${slug}`}
                      >
                        <a
                          style={{
                            textDecoration: 'none',
                            color: 'inherit',
                          }}
                        >
                          <div style={{ cursor: 'pointer' }}>
                            <div className="infoImage">
                              <img
                                src={img || buildingPlaceholder}
                                alt="locationImage"
                              />
                            </div>
                            <div className="infoAddress">
                              <p style={{ fontWeight: 'bold' }}>{name}</p>
                              <p>{address}</p>
                              <p>
                                {city} - {pincode}
                              </p>
                              <p>{state}</p>
                            </div>
                            {/* <a href={`/daycare/${state}/${city}/${area}/${slug}`}>
                        See Details
                      </a> */}
                          </div>
                        </a>
                      </Link>
                    </div>
                  </InfoWindow>
                ) : null}
              </>
            );
          })}
          {searchMethod !== 'trip' && (
            <Marker
              position={{ lat: +lat, lng: +lng }}
              icon={currentLocMarker}
              zIndex={50}
            />
          )}

          {searchMethod === 'trip' ? (
            <>
              <Directions origin={origin} destination={destination} />
            </>
          ) : null}
        </GoogleMap>
      );
    };

    return renderMap();
  },
);

const Directions = props => {
  const [directions, setDirections] = useState();
  const { origin, destination } = props;
  const waypoints = useRef({ origin, destination });
  const count = useRef(0);

  const options = {
    polylineOptions: {
      strokeColor: '#613A95',
      strokeWeight: 6,
      strokeOpacity: 0.8,
    },
    markerOptions: { icon: tripMarker },
  };
  //
  useEffect(
    () => {
      count.current = 0;
    },
    [origin.lat, origin.lng, destination.lat, destination.lng],
  );
  const directionsCallback = (result, status) => {
    // if (!isLocationsEqual(waypoints.current, { origin, destination })) {
    //   waypoints.current = { origin, destination };
    //   count.current = 0;
    // }
    if (status === 'OK' && count.current === 0) {
      count.current += 1;
      setDirections(result);
    }
  };

  return (
    <>
      <DirectionsService
        options={{
          destination,
          origin,
          travelMode: 'DRIVING',
        }}
        callback={directionsCallback}
      />
      {directions && (
        <DirectionsRenderer directions={directions} options={options} />
      )}
    </>
  );
};

const CityHeading = styled.h2`
  padding-left: 20px;
  font-family: 'Roboto', sans-serif;
  text-transform: capitalize;
  font-size: 13px;
`;

const CityDescriptionStyle = styled.div`
  padding: 20px;
  font-family: 'Roboto', sans-serif;
  font-size: 13px;
`;

const CityTitle = styled.div`
  font-family: 'Quicksand', sans-serif;
  color: #2d3438;
  font-weight: bold;
  font-size: 0.9rem;
  padding-bottom: 5px;
`;
const FatFooterContainer = styled.div`
  padding: 30px 20px 100px;
`;
const GridAutoWraper = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(167px, 1fr));
  row-gap: 4px;
  color: #616161 !important;
  font-size: 0.75rem;
  font-family: 'Roboto', sans-serif;

  @media screen and (max-width: 767px) {
    grid-template-columns: repeat(2, 1fr);
  }

  a {
    color: #30333b !important;
    text-decoration: none !important;
  }
`;
const MapWrapper = styled(Flex)`
  align-self: stretch;
  z-index: 1;
  @media (max-width: 1152px) {
    width: 100%;
  }
  @media (max-width: 768px) {
    width: 100%;
    height: 100%;
    align-self: flex-start;
    position: relative;
    & > div {
      height: 100%;
      & > div {
        height: 100% !important;
      }
    }
  }
  @media (max-width: 500px) {
    height: 100%;
  }
`;
// height: 100vh; add this in MapLOcation
const MapLoction = styled.div`
  width: 100%;

  overflow: hidden;

  @media (max-width: 768px) {
    height: 100%;
    overflow: visible;
  }
  & > div {
    @media (max-width: 500px) {
      height: 100% !important;
    }
  }
  & img {
    width: 100%;
    height: 100%;
  }
  .gm-style .gm-style-iw-c {
    max-width: 220px !important;
    min-width: 220px !important;
    .infoAddress {
      margin-top: 125px;
    }
    p {
      font-family: 'Roboto', sans-serif !important;
      font-size: 12px;
      margin: 0px !important;
      text-transform: Capitalize;
      &:first-child {
        font-family: 'Quicksand', sans-serif !important;
        font-size: 14px !important;
        line-height: 20px;
        margin-bottom: 10px !important;
      }
    }
    a {
      margin-top: 5px !important;
    }
  }
  .infoImage {
    position: absolute;
    width: 100%;
    height: 120px;
    left: 0px;
    top: 0px;
    img {
      width: 100%;
      height: 100%;
    }
  }
  .gm-ui-hover-effect {
    right: 0px !important;
    top: 0px !important;
  }
`;
const FilterFooter = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  padding: 20px 0px;
  border-top: 1px solid #eaedf2;
  margin-top: -40px;

  @media (max-width: 768px) {
    width: 100%;
  }
  @media (max-width: 414px) {
    flex-direction: column;
    align-items: center;
    font-size: 14px;
  }
`;
const FilterLink = styled.div`
  color: ${colors.secondary};
  margin-left: 5px;
  cursor: pointer;
`;
const SearchCheckbox = styled.div`
  position: absolute;
  top: 20px;
  right: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 10px;
  box-shadow: 2px 2px 10px 0 rgba(0, 0, 0, 0.2);
  border-radius: 5px;
  background: ${colors.white};
`;
const SearchReasultsAddr = styled.h1`
  color: ${colors.lightBlack};
  font-family: 'Quicksand', sans-serif;
  font-size: 18px;
  font-weight: 600;
  line-height: 33px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  max-width: 100%;
  margin: 0px;
  @media (max-width: 768px) {
    font-size: 20px;
    line-height: 25px;
  }
  @media (max-width: 500px) {
    font-size: 20px;
    line-height: 25px;
    max-width: 100%;
  }
`;
const SearchReasultsInfo = styled.div`
  margin-left: 20px;
  color: ${colors.lightBlack};
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 16px;
  margin-bottom: 5px;
  @media (max-width: 500px) {
    margin: 10px 0px 16px;
  }
`;
const TimeInfo = styled.div`
  border-radius: 5px;
  background-color: #eff2f4;
  padding: 5px 10px;
  color: ${colors.inputPlaceholder};
  font-family: 'Quicksand', sans-serif;
  font-size: 14px;
  font-weight: 500;
  line-height: 18px;
`;
const Time = styled.span`
  margin-left: 10px;
`;
const Distance = styled.span`
  margin-left: 10px;
`;
const MainContent = styled(Flex)`
  @media (max-width: 1320px) {
    padding-top: 80px !important;
  }

  @media (max-width: 1180px) {
    width: 66.5%;
    padding-top: 110px !important;
  }
  @media (max-width: 1080px) {
    width: 55.5%;
  }

  @media (max-width: 768px) {
    width: 100%;
    padding: 30px 25px 0px !important;
    order: 2;
  }
  @media (max-width: 500px) {
    padding: 10px 0px 0px !important;
  }

  .toolTipWrapper .msgTooltip {
    display: none;
    position: absolute;
    max-width: 240px;
    right: 40%;
    padding: 0px 10px 10px;
    border: 1px solid #c0c8cd;
    border-radius: 5px;
    background-color: #ffffff;
    z-index: 111;
    span {
      color: #30333b;
      font-size: 12px;
      line-height: 20px;
      white-space: pre-wrap;
    }
    &::after {
      content: '';
      display: block;
      position: absolute;
      left: 140px;
      bottom: 100%;
      width: 10px;
      height: 10px;
      transform: rotate(-45deg);
      top: -6px;
      left: 50px;
      background-color: #fff;
      border-bottom: 0px solid black;
      border-top: 1px solid #c0c8cd;
      border-right: 1px solid #c0c8cd;
      border-left: 0px solid transparent;
    }
  }
  .toolTipLocation:hover + .msgTooltip {
    display: block;
  }
  .toolTipLocation:hover .msgTooltip {
    display: block;
  }
`;

const NotFoundMsg = styled.div`
  position: relative;
  color: #30333b;
  margin-bottom: 10px;
  font-family: 'Quicksand', sans-serif;
  font-size: 24px;
  font-weight: 500;
  line-height: 30px;
`;
const NotFoundInfo = styled.div`
  color: #30333b;
  max-width: 620px;
  margin-bottom: 30px;
  font-family: 'Quicksand', sans-serif;
  font-size: 20px;
  line-height: 33px;
`;
const NoResultform = styled(Form)`
  padding-top: 30px;
  display: flex;
  width: 100%;
  @media (max-width: 414px) {
    flex-direction: column;
    padding-top: 10px;
  }
`;
const NoResultLabel = styled.label`
  margin-right: 10px;
  flex-grow: 1;
  font-size: 14px;
  line-height: 19px;
  color: #666c78;
  font-family: 'Roboto', sans-serif;
  @media (max-width: 414px) {
    margin-right: 0px;
    margin-bottom: 10px;
  }
  &:last-of-type {
    @media (max-width: 414px) {
      margin-bottom: 0px;
    }
  }
`;
const TextMsg = styled.div`
  margin-top: 90px;
  // margin-bottom: 30px;
  color: #30333b;
  font-family: 'Quicksand', sans-serif;
  font-size: 18px;
  font-weight: 500;
  line-height: 22px;
`;
const InputError = styled.span`
  font-size: 12px;
  color: red;
`;

const NotFoundWrapper = styled.div`
  padding: 0px 20px;
  height: calc(100vh - 266px);
  z-index: 1;
  .schoolNameTitle {
    @media (max-width: 500px) {
      padding-bottom: 10px;
    }
  }
  &.found {
    height: auto;
    overflow-y: scroll;
    margin-bottom: 42px;
    max-height: calc(100vh - 266px);
    min-height: calc(100vh - 266px);
    @media (max-width: 1320px) {
      max-height: calc(100vh - 310px);
      min-height: calc(100vh - 310px);
    }
    @media (max-width: 1180px) {
      max-height: calc(100vh - 340px);
      min-height: calc(100vh - 340px);
    }
    @media (max-width: 900px) {
      max-height: 100%;
      padding: 0px;
    }
    @media (max-width: 500px) {
      padding: 0px 20px;
    }
  }
  .searchTitleWrapper {
    @media (max-width: 500px) {
      width: 65%;
    }
  }
  .searchTitle {
    @media (max-width: 500px) {
      flex-direction: column;
      align-items: flex-start;
    }
  }
  .searchSortBy {
    @media (max-width: 500px) {
      align-items: flex-start;
    }
  }
  .preschoolSearchResults {
    // flex-flow: row wrap;
    // justify-content: flex-start;
    // margin-bottom: 50px;

    display: grid;
    grid-template-columns: 30% 30% 30%;
    grid-column-gap: 5%;
    @media screen and (max-width: 768px) {
      display: flex;
      align-items: center;
      justify-content: center;
    }
    .loadingSkeleton {
      span:first-child {
        border-radius: 10px;
      }
      margin-top: 28px;
      @media (max-width: 500px) {
        width: 100%;
      }
      & > span:first-child .react-loading-skeleton {
        @media (max-width: 1300px) {
          width: 230px !important;
          height: auto !important;
        }
        @media (max-width: 500px) {
          width: 100% !important;
          height: 200px !important;
        }
      }
      & > span:nth-child(2) .react-loading-skeleton {
        @media (max-width: 1300px) {
          width: 100px !important;
          height: 20px !important;
        }
      }
      & > span:last-child .react-loading-skeleton {
        @media (max-width: 1300px) {
          width: 230px !important;
          height: 40px !important;
        }
        @media (max-width: 500px) {
          width: 100% !important;
          height: 40px !important;
        }
      }
    }
    &.fixed {
      @media (max-width: 500px) {
        margin-top: 253px;
      }
    }
    &::after {
      content: '';
      width: 30%;
    }
  }
`;

const FixedMapWrapper = styled.div`
  position: absolute;
  margin: 0px auto;
  top: 0px;
  bottom: 0px;
  right: 0px;
  width: 33.5%;
  z-index: 0;
  @media (max-width: 1320px) {
    top: 1px;
  }
  @media (max-width: 768px) {
    position: relative;
    top: 0px;
    border: 2px solid #e6e8ec;
    width: 100%;
    height: 250px;
    margin-top: 20px;
  }
  &.mobileMap {
    display: none;
    &.fixed {
      position: fixed;
      z-index: 1000;
      background: #fff;
      padding: 10px;
      border: 0px;
      margin-top: 52px;
      // transition: all 0.2s ease;
    }
    @media (max-width: 900px) {
      display: block;
      height: 400px;
    }
    @media (max-width: 500px) {
      height: 250px;
    }
  }
  &.desktopMap {
    display: block;
    @media (max-width: 1080px) {
      width: 44.5%;
    }
    @media (max-width: 900px) {
      display: none;
    }
  }
`;

const SearchLoader = styled.div`
  height: calc(100vh - 266px);
  width: 100%;
`;
