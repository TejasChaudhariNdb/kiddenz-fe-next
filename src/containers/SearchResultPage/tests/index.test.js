import React from 'react';
import { render } from 'react-testing-library';
import { IntlProvider } from 'react-intl';

import SearchResultPage from '../index';

describe('<SearchResultPage />', () => {
  it('should render its heading', () => {
    const {
      container: { firstChild },
    } = render(
      <IntlProvider locale="en">
        <SearchResultPage />
      </IntlProvider>,
    );

    expect(firstChild).toMatchSnapshot();
  });
});
