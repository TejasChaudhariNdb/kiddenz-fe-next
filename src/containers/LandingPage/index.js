/* eslint-disable react/no-unescaped-entities */
/* eslint-disable no-underscore-dangle */
/* eslint-disable no-nested-ternary */
/* eslint-disable react/prop-types */
/* eslint-disable indent */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, { useState, useEffect, useRef } from 'react';
import Slider from 'react-slick';
import Router from 'next/router';
import Link from 'next/link';
import moment from 'moment';
import styled from 'styled-components';
import {
  useIntrestingArticlesHook,
  useBookmarkHook,
  useDashboardOnlineProgramsHook,
  useDropdownClose,
  useClickCount,
  useBookmarkProgramHook,
} from 'shared/hooks';
import ClassCard from 'components/ClassCard';
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';
import Geocode from 'react-geocode';
import TimePicker from 'rc-time-picker';
import H1 from 'components/H1';
import { CLICK_COUNTS } from 'utils/constants';
import colors from 'utils/colors';
import { GAevent } from 'utils/google';
import Form from '../HomePage/Form';
import Input from '../HomePage/Input';
import FlexSection from '../HomePage/FlexSection';
import RadioButton from '../../components/RadioButton';
import Header from '../../components/Header';
import MainWrapper from '../../components/MainWrapper';
import Button from '../../components/Button';
// import background from '../../images/banner6 (3).svg';
import Flex from '../../components/common/flex';
import MainHeading from '../../components/MainHeading';
import KiddenOffer from '../../components/KiddenOffer';
import InterestCard from '../../components/InterestCard';
// import Error404 from '../../components/EmptyStates/error404';
// import NoOpening from '../../components/EmptyStates/noOpening';
// import NotFound from '../../components/EmptyStates/notFound';
import poweredByGoogle from '../../images/powered_by_google_on_white.png';
import route from '../../images/toroute.png';
import pinImage from '../../images/Technology (1).png';
import trustImage from '../../images/Trust&Quality (1).png';
import infoImage from '../../images/Detailed-Info.png';
// import offlineImage from '../../images/offlineImage.png';
import pulseLoader from '../../images/pulse_loader.gif';
// import tickImage from '../../images/tick.svg';
import videoImage from '../../images/1.png';
import background from '../../images/header bg (2).svg';
import Footer from '../../components/Footer';
import Refresh from '../../components/RefreshFilter';
// import Testimonial from '../../components/TestimonialCard/index';
import joinKiddensBg from '../../images/Purple bg-02.png';
import joinKiddensImage from '../../images/2.png';
import visual from '../../images/visual.svg';
import verbal from '../../images/verbal.svg';
import logical from '../../images/logical.svg';
import bodily from '../../images/bodily.svg';
import intrapersonal from '../../images/intrapersonal.svg';
import interpersonal from '../../images/interpersonal.svg';
import musical from '../../images/musical.svg';

// const programConfig = [
//   { label: 'Small Batch', key: 'small_batch' },
//   { label: 'One To One', key: 'one_to_one' },
//   { label: 'Homeschooling', key: 'home_school' },
// ];

// const settings = {
//   dots: true,
//   infinite: true,
//   speed: 900,
//   autoplay: true,
//   autoplaySpeed: 4000,
//   slidesToShow: 1,
//   slidesToScroll: 1,
// };

const LandingPage = props => {
  const { articles } = useIntrestingArticlesHook(props);
  const {
    postCount,
    clickData: {
      location_search: locationSearchClickCount = 0,
      trip_search: tripSearchClickCount = 0,
    } = {},
  } = useClickCount(props);

  const {
    successToast,
    errorToast,
    authentication: { categoryColors = {}, categoriesConfig = {} } = {},
  } = props;

  const locationSearchDropdown = useRef(null);
  const tripLocationSearchDropdown = useRef(null);

  const [searchType, setSearchType] = useState('location');
  // Location Type states
  const [locationSearchSting, setLocationSearchSting] = useState('');
  const [locationSearchError, setLocationSearchError] = useState(false);
  const [
    locationSearchDropdownState,
    setLocationSearchDropdownState,
  ] = useState(false);
  const [locationLat, setLocationLat] = useState(0.0);
  const [locationLng, setLocationLng] = useState(0.0);

  // Trip Type states
  const [activeTripLocationInput, setActiveTripLocationInput] = useState(null);
  const [fromLocationSearchSting, setFromLocationSearchSting] = useState('');
  const [toLocationSearchSting, setToLocationSearchSting] = useState('');

  const [fromLocationSearchError, setFromLocationSearchError] = useState(false);
  const [toLocationSearchError, setToLocationSearchError] = useState(false);

  const [
    tripLocationSearchDropdownState,
    setTripLocationSearchDropdownState,
  ] = useState(false);

  const [fromLocationLat, setFromLocationLat] = useState(0.0);
  const [toLocationLat, setToLocationLat] = useState(0.0);
  const [fromLocationLng, setFromLocationLng] = useState(0.0);
  const [toLocationLng, setToLocationLng] = useState(0.0);

  const [fromTime, setFromTime] = useState('08:00');
  // const [fromTimeError, setFromTimeError] = useState(false);

  // eslint-disable-next-line no-unused-vars
  const [locationItemClicked, setLocationItemClicked] = useState(false);
  const [locationGeoCodeInProgress, setlocationGeoCodeInProgress] = useState(
    false,
  );
  const [modalStatus, setModalStatus] = useState(false);
  const [modalType, setModalType] = useState('email');
  // eslint-disable-next-line no-unused-vars
  const [restrictionType, setRestrictionType] = useState(null);

  // detect current location
  const [showCureentLocation, setShowCureentLocation] = useState(false);
  useDropdownClose(locationSearchDropdown, setLocationSearchDropdownState);
  useDropdownClose(
    tripLocationSearchDropdown,
    setTripLocationSearchDropdownState,
  );

  useEffect(
    () => {
      if (searchBy) setSearchType(searchBy);
    },
    [searchBy],
  );

  const onBookmarkAddError = ({ message }) =>
    errorToast(message || 'Something went wrong.');

  const { bookmarkedItems = [], addBookmark, removeBookmark } = useBookmarkHook(
    props,
    { onBookmarkAddError },
  );
  const {
    onlinePrograms: {
      data: onlinePrograms,
      loader: onlineLoader,
      // onSelect: onSelectProgramType,
      // selected: programType,
    },
  } = useDashboardOnlineProgramsHook(props);
  // const [modalStatus, setModalStatus] = useState(false);
  // const [modalType, setModalType] = useState('email');
  const {
    authentication: {
      isLoggedIn,
      profile: {
        data: {
          selected_filter: {
            // child_care_type: organisation,
            // schedule_type: schedule,
            // transport_facility: transportation,
            // food_facility: food,
            average_age: maxAge,
          } = {},
        } = {},
      } = {},
    } = {},
    query: { search_by: searchBy } = {},
  } = props;

  // const handleCardClick = (program, e) => {
  //   e.preventDefault();
  //   if (program.online_prgram_detail) {
  //     const query = { id: program.online_program };
  //     const url = { pathname: '/online-program/[program-name]', query };
  //     const urlAs = {
  //       pathname: `/online-program/${program.online_prgram_detail.name
  //         .replace(/\s+/g, '-')
  //         .replace('/', '-')
  //         .toLowerCase()}`,
  //       query,
  //     };
  //     Router.push(url, urlAs).then(() => window.scrollTo(0, 0));
  //   }
  // };

  const settings = {
    dots: false,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    autoplay: false,
    autoplaySpeed: 2000,
    infinite: false,
    // className: 'center',
    // centerMode: true,
    // centerPadding: '30px',
    responsive: [
      {
        breakpoint: 1100,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  const handleViewAll = () => {
    Router.push({
      pathname: `/online-program/search`,
      // query: { programs: programType }, // Uncomment if you want with filter in search page
    }).then(() => window.scrollTo(0, 0));
  };

  const handleOnlineSchool = () => {
    Router.push({
      pathname: `/online-program/search`,
    }).then(() => window.scrollTo(0, 0));
  };

  const handleSelect = (selected, isDefault = true, type) => {
    setlocationGeoCodeInProgress(true);
    setLocationItemClicked(true);

    // If normal Location search
    if (isDefault) {
      setLocationSearchSting(selected);
      setLocationSearchDropdownState(false);
      setLocationSearchError(false);
    }

    // If Trip search
    if (type === 'from') {
      setFromLocationSearchSting(selected);
      setTripLocationSearchDropdownState(false);
      setFromLocationSearchError(false);
    }
    if (type === 'to') {
      setToLocationSearchSting(selected);
      setTripLocationSearchDropdownState(false);
      setToLocationSearchError(false);
    }
    geocodeByAddress(selected)
      .then(res => getLatLng(res[0]))
      .then(({ lat, lng }) => {
        // If normal Location search
        if (isDefault) {
          setLocationLat(lat);
          setLocationLng(lng);
          if (window.innerWidth <= 768) {
            if (isLoggedIn) {
              /* If user has click location search equal to or more than n times 
                Block the content access
            */
              if (locationSearchClickCount >= CLICK_COUNTS.locationSearch) {
                setModalType('restriction');
                setRestrictionType('location search');
                setModalStatus(true);
              } else {
                postCount('location_search');
                GAevent('Search', 'Location Search Clicked', 'Location Search');
                Router.push(
                  `/daycare/search?search_type=${searchType}&locationString=${selected}&latitude=${lat}&longitude=${lng}&max_age=${Math.floor(
                    maxAge * 12,
                  )}&transportation=0&food=0&type=radius&sort_by=distance`,
                ).then(() => window.scrollTo(0, 0));
              }
            } else {
              GAevent('Search', 'Location Search Clicked', 'Location Search');
              Router.push(
                `/daycare/search?search_type=${searchType}&locationString=${selected}&latitude=${lat}&longitude=${lng}&type=radius&sort_by=distance`,
              ).then(() => window.scrollTo(0, 0));
            }
          }
        }

        // If Trip search
        if (type === 'from') {
          setFromLocationLat(lat);
          setFromLocationLng(lng);
        } else {
          setToLocationLat(lat);
          setToLocationLng(lng);
        }

        setlocationGeoCodeInProgress(false);
      })
      .catch(error => {
        console.log(error, 'error');
        // this.setState({ isGeocoding: false });
      });
  };

  const getLocation = () => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        ({ coords: { latitude: lati, longitude: lngi } = {} }) => {
          setLocationLat(lati);
          setLocationLng(lngi);
          Geocode.fromLatLng(
            String(lati.toFixed(5)),
            String(lngi.toFixed(5)),
          ).then(({ results = [] }) => {
            if (results[0]) {
              const locName = results[0].formatted_address;
              setLocationSearchSting(locName);
              if (window.innerWidth <= 768) {
                if (isLoggedIn) {
                  /* If user has click location search equal to or more than n times 
                        Block the content access
                    */
                  if (locationSearchClickCount >= CLICK_COUNTS.locationSearch) {
                    setModalType('restriction');
                    setRestrictionType('location search');
                    setModalStatus(true);
                  } else {
                    postCount('location_search');
                    GAevent(
                      'Search',
                      'Location Search Clicked',
                      'Location Search',
                    );
                    Router.push(
                      `/daycare/search?search_type=location&locationString=${locName}&latitude=${lati}&longitude=${lngi}&max_age=${Math.floor(
                        maxAge * 12,
                      )}&transportation=0&food=0&type=radius&sort_by=distance`,
                    ).then(() => window.scrollTo(0, 0));
                  }
                } else {
                  GAevent(
                    'Search',
                    'Location Search Clicked',
                    'Location Search',
                  );
                  Router.push(
                    `/daycare/search?search_type=location&locationString=${locName}&latitude=${lati}&longitude=${lngi}&type=radius&sort_by=distance`,
                  ).then(() => window.scrollTo(0, 0));
                }
              }
            }
          });

          setlocationGeoCodeInProgress(false);
        },
        err => {
          console.log(err, 'err');
          window.alert(
            'Location access is blocked. Change your location settings in browser or select location manually',
          );
        },
      );
    }
  };

  const {
    // bookmarkedPrograms: {
    //   data: wishlistedPrograms = [],
    //   loader: wishlistLoader,
    // },
    bookmarkedItems: bookmamrkedPrograms,
    addBookmark: addBookmamrkProgram,
    removeBookmark: removeBookmamrkProgram,
  } = useBookmarkProgramHook(props, {
    onBookmarkDelSuccess,
    onBookmarkAddSuccess,
    onBookmarkDelError,
    onBookmarkAddError,
  });

  const onBookmarkAddSuccess = ({ message }) => successToast(message);
  const onBookmarkDelSuccess = ({ message }) => successToast(message);
  const onBookmarkDelError = ({ message }) => errorToast(message);

  const handleBookMark = id => {
    if (!isLoggedIn) {
      setModalType('login');
      setModalStatus(true);
      // setModalActive(true);
    } else {
      addBookmamrkProgram(id);
    }
  };

  const handleDeleteBookMark = id => {
    removeBookmamrkProgram(id);
  };

  return (
    <>
      <Header
        {...props}
        type="articles"
        searchBar={false}
        isModalActive={modalStatus}
        activeModalType={modalType}
        // isModalActive={true}
        // activeModalType="restriction"
        setActiveCallback={setModalStatus}
      />
      <MainWrapper background="#F8F4FE" paddingTop="0px">
        <div style={{ backgroundColor: '#fff' }}>
          <div className="mainSectionStyles">
            {/* <img src={background} alt="" /> */}

            <Flex
              column
              alignCenter
              flexMargin="0px auto 0px"
              flexWidth="100%"
              maxWidth="778px"
              className="landingTitle"
            >
              <LandingTitle>Find Best Preschool and Daycare</LandingTitle>
              {/* <Button
                text="Click here to find &#34;Preschools/Daycares&#34;"
                fitContent="fit-content"
                type="preCare ripple"
                marginRight="10px"
                onClick={() =>
                  Router.push({
                    pathname: '/daycare/landing',
                  }).then(() => window.scrollTo(0, 0))
                }
              /> */}
              <LandingSubTitle>
                Get Discounts upto ₹8000 on Admissions
              </LandingSubTitle>
              <Flex row alignCenter className="mobileColumn">
                {/* <Button
                  text="Explore extracurricular courses"
                  type="preCare ripple roundedGreen"
                  marginRight="50px"
                  background="#60B947"
                  onClick={handleOnlineSchool}
                /> */}
                <Button
                  text="Enter your location"
                  type="preCare ripple"
                  onClick={() =>
                    Router.push({
                      pathname: '/daycare/landing',
                    }).then(() => window.scrollTo(0, 0))
                  }
                />
              </Flex>
            </Flex>
          </div>

          {/* <ExtraCurricularSection>
            <h2>
              Exclusive extracurricular programs based on multiple intelligence
              theory
            </h2>
            <ul>
              <li>
                <div className="grouping green">
                  <div className="top">
                    <img src={visual} alt="intrapersonal" />
                  </div>
                  <div className="bottom">
                    <h4>Visual-Spatial</h4>
                    <p>Grow artistic skills and develop visualisation</p>
                  </div>
                </div>
              </li>
              <li>
                <div className="grouping voilet">
                  <div className="top">
                    <img src={verbal} alt="intrapersonal" />
                  </div>
                  <div className="bottom">
                    <h4>Verbal-Linguistic</h4>
                    <p>Better readers, writers and communicators</p>
                  </div>
                </div>
              </li>
              <li>
                <div className="grouping green">
                  <div className="top">
                    <img src={intrapersonal} alt="intrapersonal" />
                  </div>
                  <div className="bottom">
                    <h4>Intrapersonal</h4>
                    <p>Understand self and develop oneself</p>
                  </div>
                </div>
              </li>
              <li>
                <div className="grouping voilet">
                  <div className="top">
                    <img src={interpersonal} alt="intrapersonal" />
                  </div>
                  <div className="bottom">
                    <h4>Interpersonal</h4>
                    <p>Develop confidence and communication</p>
                  </div>
                </div>
              </li>
              <li>
                <div className="grouping green">
                  <div className="top">
                    <img src={logical} alt="intrapersonal" />
                  </div>
                  <div className="bottom">
                    <h4>Mathematical-Logical</h4>
                    <p>Develop logic, reasoning & problem-solving</p>
                  </div>
                </div>
              </li>
              <li>
                <div className="grouping voilet">
                  <div className="top">
                    <img src={musical} alt="intrapersonal" />
                  </div>
                  <div className="bottom">
                    <h4>Musical</h4>
                    <p>Learn rhythm and music</p>
                  </div>
                </div>
              </li>
              <li>
                <div className="grouping orange">
                  <div className="top">
                    <img src={bodily} alt="intrapersonal" />
                  </div>
                  <div className="bottom">
                    <h4>Bodily-Kinesthetic</h4>
                    <p>Excel in dance and sports</p>
                  </div>
                </div>
              </li>
            </ul>
            <Button
              text="Explore extracurricular courses"
              type="preCare ripple roundedGreen"
              background="#60B947"
              onClick={handleOnlineSchool}
            />
          </ExtraCurricularSection> */}
          {/* {Object.keys(onlinePrograms).length ? ( */}
          {/* <OnlinePreSchoolSection>
            <h2>We have exclusive courses like</h2>
            <>
              {onlineLoader ? (
                <SearchLoader>
                  <Flex
                    alignCenter
                    justifyCenter
                    flexWidth="100%"
                    flexHeight="100%"
                  >
                    <img src={pulseLoader} alt="" height={75} width={75} />
                  </Flex>
                </SearchLoader>
              ) : (
                <>
                  {Object.entries(onlinePrograms).length ? (
                    Object.entries(onlinePrograms).map(([key, value]) => (
                      <>
                        {value && value.length ? (
                          <Slider {...settings} key={key}>
                            {value.map(program => (
                              <div key={program.id}>
                                <ClassCard
                                  isLoggedIn={isLoggedIn}
                                  width="300px"
                                  isOneToOne={
                                    program.online_prgram_detail &&
                                    program.online_prgram_detail.online_tags ===
                                      'one_to_one'
                                  }
                                  name={
                                    program.online_prgram_detail &&
                                    program.online_prgram_detail.name
                                  }
                                  rating={
                                    program.rating && program.rating.rating__avg
                                  }
                                  providerName={
                                    program.online_prgram_detail &&
                                    program.online_prgram_detail.provider_name
                                  }
                                  startAge={
                                    program.online_prgram_detail &&
                                    program.online_prgram_detail.start_age
                                  }
                                  endAge={
                                    program.online_prgram_detail &&
                                    program.online_prgram_detail.end_age
                                  }
                                  isSingleSession={
                                    program.online_prgram_detail &&
                                    program.online_prgram_detail
                                      .batch_available &&
                                    program.online_prgram_detail.batch_available
                                      .length
                                      ? program.online_prgram_detail.batch_available.includes(
                                          'Single_Session',
                                        )
                                      : null
                                  }
                                  isTrial={
                                    program.online_prgram_detail &&
                                    program.online_prgram_detail
                                      .batch_available &&
                                    program.online_prgram_detail.batch_available
                                      .length
                                      ? program.online_prgram_detail.batch_available.includes(
                                          'Trial_Class',
                                        )
                                      : null
                                  }
                                  price={
                                    program.online_prgram_detail &&
                                    program.online_prgram_detail.price
                                  }
                                  moneyBack={
                                    program.online_prgram_detail &&
                                    program.online_prgram_detail.price &&
                                    program.online_prgram_detail.price
                                      .money_back_gurantee
                                  }
                                  isSponsered={
                                    program.online_prgram_detail &&
                                    program.online_prgram_detail.is_sponsored
                                  }
                                  media={
                                    program.online_prgram_detail &&
                                    program.online_prgram_detail.medias
                                  }
                                  href={`/online-program/${program.online_prgram_detail.name
                                    .replace(/\s+/g, '-')
                                    .replace('/', '-')
                                    .toLowerCase()}?id=${
                                    program.online_program
                                  }`}
                                  programId={program.online_program}
                                  batches={
                                    program.online_prgram_detail &&
                                    program.online_prgram_detail.batch_list
                                  }
                                  onBookmark={() =>
                                    handleBookMark(
                                      program && program.online_program,
                                    )
                                  }
                                  onDeleteBookmark={() =>
                                    handleDeleteBookMark(
                                      program && program.online_program,
                                    )
                                  }
                                  isBookMarked={bookmamrkedPrograms.includes(
                                    +program.online_program,
                                  )}
                                />
                              </div>
                            ))}
                          </Slider>
                        ) : null}
                      </>
                    ))
                  ) : (
                    <Refresh />
                  )}
                  <Button
                    text="Join us for a free trial class"
                    type="preCare ripple roundedGreen joinUs"
                    background="#60B947"
                    onClick={handleViewAll}
                  />
                </>
              )}
            </>
          </OnlinePreSchoolSection> */}
          {/* ) : null} */}
          {/* <OfflinePreSchoolSection>
            <div className="left">
              <h2 className="mobile">Offline Preschools/Daycares</h2>
              <img src={offlineImage} alt="Offline Preschools" />
            </div>
            <div className="right">
              <h2>Offline Preschools/Daycares</h2>
              <p>
                Lorem ipsum dolor sit amet, dignissim constituto an qui. Mea
                minimum sadipscing in, ad quo etiam habemus consectetuer, cu
                tale delicata sea
              </p>
              <button type="button">Know More</button>
            </div>
          </OfflinePreSchoolSection> */}
          <MainSection style={{ background: '#fff' }}>
            <img src={background} alt="" />
            <H1
              text="Discover and Select Best Preschool and Daycare"
              textAlign="center"
              margin="64px 0px 0px"
            />
            <SearchWrapper>
              {/* Location Search */}
              <Layout
                onClick={() => {
                  setSearchType('location');
                  setFromLocationSearchSting('');
                  setToLocationSearchSting('');
                  setToLocationSearchError(false);
                  setFromLocationSearchError(false);

                  if (searchType === 'Trip')
                    Router.replace(`/daycare/landing`, `/daycare/landing`, {
                      shallow: true,
                    });
                }}
              >
                <RadioButton
                  id="test1"
                  text="Location"
                  checkedColor="#fff"
                  checked={searchType === 'location'}
                  onClick={() => {}}
                />
                <Msg>Click here to search at a particular location</Msg>
                <Form className="locationForm">
                  <PlacesAutocomplete
                    value={locationSearchSting}
                    onChange={address => {
                      setLocationItemClicked(false);
                      // if (address) {
                      //   setLocationSearchError(false);
                      // } else {
                      //   setLocationSearchError(true);
                      // }
                      setLocationSearchSting(address);
                    }}
                    onSelect={address => handleSelect(address)}
                    onError={() => {}}
                    clearItemsOnError
                    shouldFetchSuggestions
                    searchOptions={{
                      componentRestrictions: { country: ['in'] },
                    }}
                  >
                    {({
                      getInputProps,
                      suggestions,
                      getSuggestionItemProps,
                    }) => {
                      if (suggestions.length > 0)
                        setLocationSearchDropdownState(true);
                      else setLocationSearchDropdownState(false);

                      return (
                        <>
                          <label htmlFor="search_box">
                            <div
                              className={`searchInputField ${
                                locationSearchError ? 'error' : ''
                              }`}
                            >
                              <Input
                                {...getInputProps({
                                  onBlur: () => {
                                    // if (locationSearchSting)
                                    //   setLocationSearchError(false);
                                    // else setLocationSearchError(true);
                                    // if (locationItemClicked)
                                    //   setLocationSearchError(false);
                                    // else setLocationSearchError(true);
                                  },
                                  onFocus: () => {
                                    setShowCureentLocation(true);
                                    setLocationSearchError(false);
                                  },
                                })}
                                id="search_box"
                                type="text"
                                placeholder="Enter your location"
                                value={locationSearchSting}
                                // disabled={searchType !== 'location'}
                                autoComplete="off"
                              />
                              {locationSearchError && (
                                <DestinationError>
                                  Location is Required
                                </DestinationError>
                              )}
                            </div>
                            {!locationSearchError &&
                              showCureentLocation && (
                                <div
                                  className="myCurrentLocation active"
                                  onClick={getLocation}
                                >
                                  <span
                                    className="iconify"
                                    data-icon="ic:outline-location-searching"
                                    data-inline="false"
                                  />
                                  <span>Detect my location</span>
                                </div>
                              )}
                          </label>

                          <div
                            style={{ width: '166px' }}
                            className="buttonWrapper"
                          >
                            <Button
                              text="Search"
                              headerButton
                              marginTop="10px"
                              className="locationBtn"
                              onClick={e => {
                                // amplitude.getInstance().logEvent('SEARCH');
                                e.preventDefault();
                                if (!locationSearchSting)
                                  setLocationSearchError(true);
                                else {
                                  setLocationSearchError(false);
                                  if (isLoggedIn) {
                                    /* If user has click location search equal to or more than n times
                                      Block the content access
                                  */
                                    if (
                                      locationSearchClickCount >=
                                      CLICK_COUNTS.locationSearch
                                    ) {
                                      setModalType('restriction');
                                      setRestrictionType('location search');
                                      setModalStatus(true);
                                    } else {
                                      postCount('location_search');
                                      GAevent(
                                        'Search',
                                        'Location Search Clicked',
                                        'Location Search',
                                      );
                                      Router.push(
                                        `/daycare/search?search_type=${searchType}&locationString=${locationSearchSting}&latitude=${locationLat}&longitude=${locationLng}&max_age=${Math.floor(
                                          maxAge * 12,
                                        )}&transportation=0&food=0&type=radius&sort_by=distance`,
                                      ).then(() => window.scrollTo(0, 0));
                                    }
                                  } else {
                                    GAevent(
                                      'Search',
                                      'Location Search Clicked',
                                      'Location Search',
                                    );
                                    Router.push(
                                      `/daycare/search?search_type=${searchType}&locationString=${locationSearchSting}&latitude=${locationLat}&longitude=${locationLng}&type=radius&sort_by=distance`,
                                    ).then(() => window.scrollTo(0, 0));
                                  }
                                }
                              }}
                              disabled={
                                searchType !== 'location' ||
                                locationGeoCodeInProgress
                              }
                            />
                          </div>

                          {locationSearchDropdownState &&
                            suggestions.length > 0 && (
                              <SearchOption
                                column
                                ref={locationSearchDropdown}
                                className="single"
                              >
                                {suggestions.map(suggestion => (
                                  <li
                                    {...getSuggestionItemProps(suggestion, {
                                      // onClick: () => {
                                      //   setLocationSearchSting(
                                      //     suggestion.formattedSuggestion.mainText,
                                      //   );
                                      // },
                                    })}
                                  >
                                    {suggestion.description}
                                  </li>
                                ))}
                                <li className="google">
                                  <img
                                    src={poweredByGoogle}
                                    alt="googleImage"
                                  />
                                </li>
                              </SearchOption>
                            )}
                        </>
                      );
                    }}
                  </PlacesAutocomplete>
                </Form>
                {/* <img src={registernow} onClick={getLocation} /> */}
              </Layout>

              {/* Trip Search */}
              <Layout
                onClick={() => {
                  setSearchType('trip');
                  setLocationSearchSting('');
                  setLocationSearchError(false);
                }}
              >
                <RadioButton
                  id="test2"
                  text="Trip Route"
                  checkedColor="#fff"
                  checked={searchType === 'trip'}
                  onClick={() => {}}
                />
                <Msg>
                  Click here to search the ideal care on the way to your
                  destination, office or work
                </Msg>
                <Form className="toFromSearch">
                  <PlacesAutocomplete
                    value={
                      activeTripLocationInput === 'from'
                        ? fromLocationSearchSting
                        : toLocationSearchSting
                    }
                    onChange={address => {
                      setLocationItemClicked(false);
                      if (activeTripLocationInput === 'from') {
                        setFromLocationSearchSting(address);
                      } else {
                        setToLocationSearchSting(address);
                      }
                    }}
                    onSelect={address =>
                      handleSelect(address, false, activeTripLocationInput)
                    }
                    onError={() => {}}
                    clearItemsOnError
                    shouldFetchSuggestions
                    searchOptions={{
                      componentRestrictions: { country: ['in'] },
                    }}
                  >
                    {({
                      getInputProps,
                      suggestions,
                      getSuggestionItemProps,
                    }) => {
                      if (suggestions.length > 0)
                        setTripLocationSearchDropdownState(true);
                      else setTripLocationSearchDropdownState(false);
                      return (
                        <>
                          <Location
                            flexMargin="27px 0px 0px 0px"
                            style={{ position: 'relative' }}
                          >
                            <LocationFromTo>
                              <LocationFrom>
                                <div>&nbsp;</div>
                              </LocationFrom>
                              <LocationTo>
                                <img
                                  src={route}
                                  alt=""
                                  height={14}
                                  width={12}
                                />
                              </LocationTo>
                            </LocationFromTo>
                            <Flex
                              flexWidth="95%"
                              flexMargin="0px 10px 0px 0px"
                              column
                              className="findLocation"
                            >
                              <Flex
                                style={{
                                  borderBottom: '1px solid #E5E5E5',
                                }}
                                className={`findLocation-from ${
                                  fromLocationSearchError ? 'error' : ''
                                }`}
                              >
                                <Destination
                                  {...getInputProps({
                                    onBlur: () => {
                                      // if (fromLocationSearchSting)
                                      //   setFromLocationSearchError(false);
                                      // else setFromLocationSearchError(true);
                                      // if (locationItemClicked)
                                      //   setFromLocationSearchError(false);
                                      // else setFromLocationSearchError(true);
                                    },
                                  })}
                                  type="text"
                                  placeholder="From"
                                  // disabled={searchType !== 'trip'}
                                  onClick={() => {
                                    setActiveTripLocationInput('from');
                                  }}
                                  value={fromLocationSearchSting}
                                />
                              </Flex>
                              <Flex
                                style={{ position: 'relative' }}
                                className={`findLocation-to ${
                                  toLocationSearchError ? 'error' : ''
                                }`}
                              >
                                <Destination
                                  {...getInputProps({
                                    onBlur: () => {
                                      // if (fromLocationSearchSting)
                                      //   setToLocationSearchError(false);
                                      // else setToLocationSearchError(true);
                                      // if (locationItemClicked)
                                      //   setToLocationSearchError(false);
                                      // else setToLocationSearchError(true);
                                    },
                                  })}
                                  type="text"
                                  placeholder="To"
                                  style={{ marginTop: '15px' }}
                                  // disabled={searchType !== 'trip'}
                                  onClick={() => {
                                    setActiveTripLocationInput('to');
                                  }}
                                  value={toLocationSearchSting}
                                />
                              </Flex>
                            </Flex>

                            <TimeInfo>
                              <Flex row flexPadding="10px 0px 10px 10px">
                                <Flex
                                  // flexPadding="10px 27px 10px 10px"
                                  alignCenter
                                  className="timeInfo"
                                >
                                  <span
                                    className="iconify"
                                    data-icon="simple-line-icons:clock"
                                    data-inline="false"
                                  />
                                </Flex>
                                <TimePicker
                                  defaultValue={moment()
                                    .hour(8)
                                    .minute(0)}
                                  placeholder="Ideal departure time"
                                  onChange={e => {
                                    // eslint-disable-next-line no-underscore-dangle
                                    setFromTime(moment(e._d).format('HH:MM'));
                                  }}
                                  showSecond={false}
                                  minuteStep={5}
                                  disabled={searchType !== 'trip'}
                                />
                              </Flex>
                            </TimeInfo>
                            {tripLocationSearchDropdownState &&
                              suggestions.length > 0 && (
                                <SearchOption
                                  column
                                  ref={tripLocationSearchDropdown}
                                  // top="435px"
                                >
                                  {suggestions.map(suggestion => (
                                    <li
                                      {...getSuggestionItemProps(
                                        suggestion,
                                        {},
                                      )}
                                    >
                                      {suggestion.description}
                                    </li>
                                  ))}
                                  <li className="google">
                                    <img
                                      src={poweredByGoogle}
                                      alt="googleImage"
                                    />
                                  </li>
                                </SearchOption>
                              )}

                            {(fromLocationSearchError ||
                              toLocationSearchError) && (
                              <DestinationError>
                                {(() => {
                                  if (
                                    fromLocationSearchError &&
                                    toLocationSearchError
                                  ) {
                                    return 'From and To locations are required';
                                  }
                                  if (fromLocationSearchError) {
                                    return 'From location is required';
                                  }
                                  if (toLocationSearchError) {
                                    return 'To location is required';
                                  }
                                  return '';
                                })()}
                              </DestinationError>
                            )}
                          </Location>

                          <div
                            style={{ width: '166px' }}
                            className="buttonWrapper"
                          >
                            <Button
                              // href="/"
                              text="Search"
                              headerButton
                              marginTop="10px"
                              disabled={searchType !== 'trip'}
                              onClick={e => {
                                // amplitude.getInstance().logEvent('SEARCH');
                                e.preventDefault();

                                if (
                                  !fromLocationSearchSting ||
                                  !toLocationSearchSting
                                ) {
                                  if (
                                    !fromLocationSearchSting &&
                                    !toLocationSearchSting
                                  ) {
                                    setFromLocationSearchError(true);
                                    setToLocationSearchError(true);
                                  }

                                  if (
                                    fromLocationSearchSting &&
                                    !toLocationSearchSting
                                  ) {
                                    setFromLocationSearchError(false);
                                    setToLocationSearchError(true);
                                  }

                                  if (
                                    toLocationSearchSting &&
                                    !fromLocationSearchSting
                                  ) {
                                    setFromLocationSearchError(true);
                                    setToLocationSearchError(false);
                                  }
                                  // setFromTimeError(true);
                                } else {
                                  setFromLocationSearchError(false);
                                  setToLocationSearchError(false);
                                  // setFromTimeError(false);

                                  if (isLoggedIn) {
                                    /* If user has click trip search equal to or more than n times 
                                      Block the content access
                                  */
                                    if (
                                      tripSearchClickCount >=
                                      CLICK_COUNTS.tripSearch
                                    ) {
                                      setModalType('restriction');
                                      setRestrictionType('trip search');
                                      setModalStatus(true);
                                    } else {
                                      postCount('trip_search');
                                      GAevent(
                                        'Search',
                                        'Trip Search Clicked',
                                        'Trip Search',
                                      );
                                      Router.push(
                                        `/daycare/search?search_type=${searchType}&toLocationString=${toLocationSearchSting}&fromLocationString=${fromLocationSearchSting}&fromLat=${fromLocationLat}&toLat=${toLocationLat}&fromLng=${fromLocationLng}&toLng=${toLocationLng}&timehrs=${
                                          fromTime.split(':')[0]
                                        }&timemins=${
                                          fromTime.split(':')[1]
                                        }&max_age=${Math.floor(
                                          maxAge * 12,
                                        )}&transportation=0&food=0&type=radius&sort_by=distance`,
                                      ).then(() => window.scrollTo(0, 0));
                                    }
                                  } else {
                                    GAevent(
                                      'Search',
                                      'Trip Search Clicked',
                                      'Trip Search',
                                    );
                                    Router.push(
                                      `/daycare/search?search_type=${searchType}&toLocationString=${toLocationSearchSting}&fromLocationString=${fromLocationSearchSting}&fromLat=${fromLocationLat}&toLat=${toLocationLat}&fromLng=${fromLocationLng}&toLng=${toLocationLng}&timehrs=${
                                        fromTime.split(':')[0]
                                      }&timemins=${
                                        fromTime.split(':')[1]
                                      }&type=radius&sort_by=distance`,
                                    ).then(() => window.scrollTo(0, 0));
                                  }
                                }
                              }}
                            />
                          </div>
                        </>
                      );
                    }}
                  </PlacesAutocomplete>
                </Form>
              </Layout>
            </SearchWrapper>
          </MainSection>
          <div style={{ backgroundColor: '#fff' }}>
            <OfferSection>
              <Flex column alignCenter>
                <MainHeading text="Kiddenz Offers" />
                <Flex
                  className="kiddenoffer-section"
                  flexWidth="100%"
                  justifyBetween
                  flexPadding="0px 0px 60px"
                >
                  <KiddenOffer
                    width="25%"
                    image={trustImage}
                    text="Trust and Quality"
                    desc="Get accurate &amp; upto date information about every preschool"
                  />
                  <KiddenOffer
                    image={infoImage}
                    text="Detailed Information"
                    desc="Detailed information complete with pictures, teachers, facilities, program details, fees and contact for every preschool/ daycare"
                  />
                  <KiddenOffer
                    width="24%"
                    image={pinImage}
                    text="Technology"
                    desc="Find preschool on your day to work &amp; use advanced filter to shortlist the ideal preschool/ daycare based on budget, distance and features"
                  />
                  {/* <KiddenOffer
                    width="25%"
                    image={trustImage}
                    text="Live Interactive Classes"
                    desc="Interactive courses in small batch or one-one setting"
                  />
                  <KiddenOffer
                    image={infoImage}
                    text="Curated courses from top experts"
                    desc="Book live extracurricular courses for all round development of your child"
                  />
                  <KiddenOffer
                    width="24%"
                    image={pinImage}
                    text="Preschools Programs"
                    desc="Explore best preschool and daycare programs from top preschool brands near your location"
                  /> */}
                </Flex>
              </Flex>
              <ForParents>
                <Flex column flexWidth="100%" alignCenter>
                  <MainHeading
                    text="For Parents"
                    fontSize="32px"
                    margin="0px 0px 30px 0px"
                  />
                  <VideoImage
                  // onClick={() => {
                  //   setModalType('video');
                  //   setModalStatus(true);
                  // }}
                  >
                    <img src={videoImage} alt="videoImage" />
                  </VideoImage>
                  <div className="forParents-desc">
                    {/* At Kiddenz our goal is to provide top quality
                    extracurricular courses and preschool programs that ensure
                    all round development of your child, delivered in a fun &
                    interactive learning environment. We’re incredibly proud to
                    partner with trustworthy and committed experts and providers */}
                    We help parents find the right preschool/daycare for their
                    children.
                    <br />
                    Our goal is to provide every child an opportunity for top
                    quality early education to ensure all round development.
                    <br />
                    We’re incredibly proud to partner with honest, trustworthy
                    and committed care providers who provide high-quality care
                    and early education.
                  </div>

                  <Flex row alignCenter className="mobileColumn">
                    <Button
                      text="Find top preschools and daycares"
                      type="preCare ripple"
                      marginRight="50px"
                      onClick={() =>
                        Router.push({
                          pathname: '/daycare/landing',
                        }).then(() => window.scrollTo(0, 0))
                      }
                    />
                    <Button
                      text="Explore learning programs"
                      type="preCare ripple roundedGreen"
                      background="#60B947"
                      onClick={handleOnlineSchool}
                    />
                  </Flex>
                  {/* 
                  {!isLoggedIn && (
                    <div className="registerBtn">
                      <Button
                        text="Register"
                        type="mobile"
                        marginRight="0px"
                        onClick={() => {
                          setModalType('signup');
                          setModalStatus(true);
                        }}
                      />
                    </div>
                  )} */}
                </Flex>
              </ForParents>
            </OfferSection>
          </div>
        </div>

        <JoinKiddens>
          <div className="whiteBg" />
          <img src={joinKiddensBg} alt="joinKiddensBg" />
          <Flex
            className="join-kiddenz"
            justifyBetween
            flexPadding="250px 30px 280px"
            flexWidth="100%"
            maxWidth="1200px"
            flexMargin="0px auto"
          >
            <JoinKiddensImage>
              <img src={joinKiddensImage} alt="joinKiddensImage" />
            </JoinKiddensImage>
            <JoinKiddensContent>
              {/* <h3>Register Your Early education programs</h3> */}
              <h3>Register Your School</h3>
              {/* <p>
                Be a part of growing community of top quality experts, Preschools
                & Daycares providers and parents.
              </p> */}
              <p>
                Be a part of growing community of  & Daycare providers
                and parents.
              </p>
              {/* <p>
                Register your program on Kiddenz to grow your business. Connect
                with parents looking for live classes and preschool and daycare
                in your locality. Create a free listing on Kiddenz, add photos,
                details, respond to reviews, post availability and fill open
                slots. Have a great online presence.
              </p> */}
              <p>
                Register your school on Kiddenz to grow your business. Connect
                with parents looking for preschool and daycare in your locality.
                Create a free listing on Kiddenz, add photos, details, respond
                to reviews, post availability and fill open slots. Have a great
                online presence.
              </p>
              <div className="registerBtn">
                <Button
                  text="Register Your School"
                  type="mobile"
                  marginRight="10px"
                  onClick={() =>
                    Router.push({
                      pathname: '/provider',
                    }).then(() => window.scrollTo(0, 0))
                  }
                />
              </div>
            </JoinKiddensContent>
          </Flex>
        </JoinKiddens>

        {/* <Testimonials>
          <MainHeading
            text="Testimonials"
            fontSize="32px"
            margin="0px 0px 30px 0px"
          />
          <Flex flexWidth="100%" column>
            <Slider {...settings}>
              <Testimonial
                type="primary"
                name="Shruti Gupta"
                job="Lead Engineer, IBM"
                comment="“Maria and her team are great, super loving. I love that they are always teaching her something new.”"
              />
              <Testimonial
                type="primary"
                name="Shruti Gupta"
                job="Lead Engineer, IBM"
                comment="“Maria and her team are great, super loving. I love that they are always teaching her something new.”"
              />
              <Testimonial
                type="primary"
                name="Shruti Gupta"
                job="Lead Engineer, IBM"
                comment="“Maria and her team are great, super loving. I love that they are always teaching her something new.”"
              />
              <Testimonial
                type="primary"
                name="Shruti Gupta"
                job="Lead Engineer, IBM"
                comment="“Maria and her team are great, super loving. I love that they are always teaching her something new.”"
              />
              <Testimonial
                type="primary"
                name="Shruti Gupta"
                job="Lead Engineer, IBM"
                comment="“Maria and her team are great, super loving. I love that they are always teaching her something new.”"
              />
            </Slider>
          </Flex>
        </Testimonials> */}

        <InterestingRead>
          <MainHeading
            text="Parenting Articles"
            fontSize="32px"
            margin="0px 0px 8px 0px"
          />
          <SubInfo>Read what our experts recommend on parenting.</SubInfo>
          <Flex className="interestContainer">
            {articles.length > 0
              ? articles.map(article => (
                  <Link href={`/blog/${article.post_name}-${article.post_id}`}>
                    <InterestCard
                      isLoggedIn={isLoggedIn}
                      categoriesConfig={categoriesConfig}
                      categoryColors={categoryColors}
                      interestImage={article.post_image}
                      heading={article.post_title}
                      categoryList={article.category}
                      list={article.post_tag}
                      dateNum={moment(article.post_modified).format(
                        'MMMM DD,YYYY',
                      )}
                      viewNum={article.viewCount}
                      likesNum={article.likeCount}
                      isBookmarked={bookmarkedItems.includes(article.post_id)}
                      onBookmarkClick={() => {
                        if (bookmarkedItems.includes(article.post_id))
                          removeBookmark(article.post_id);
                        else addBookmark(article.post_id);
                      }}
                    />
                  </Link>
                ))
              : null}
          </Flex>
        </InterestingRead>
        <Flex
          flexWidth="100%"
          justifyCenter
          flexMargin="0px 0px 140px 0px"
          className="viewArticle"
        >
          <Button
            text="View all articles"
            type="viewArticle"
            onClick={() =>
              Router.push({
                pathname: '/blog',
              }).then(() => window.scrollTo(0, 0))
            }
          />
        </Flex>

        <FatFooterContainer>
          <div>
            <CityTitle>Top Cities: </CityTitle>
            <GridAutoWraper>
              <Link href="/bengaluru" className="schoolLink">
                Daycares in Bengaluru
              </Link>
              <Link href="/Bengaluru" className="schoolLink">
              Preschools in Bengaluru
              </Link>
              <Link href="/pune" className="schoolLink">
                Preschools in Pune
              </Link>
              <Link href="/pune" className="schoolLink">
                Daycares in Pune
              </Link>
              <Link href="/mumbai" className="schoolLink">
                Preschools in Mumbai
              </Link>
              <Link href="/mumbai" className="schoolLink">
                Daycares in Mumbai
              </Link>
              <Link href="/noida" className="schoolLink">
                Preschools in Noida
              </Link>
              <Link href="/noida" className="schoolLink">
                Daycares in Noida
              </Link>
              <Link href="/new delhi" className="schoolLink">
                Preschools in New Delhi
              </Link>
              <Link href="/new delhi" className="schoolLink">
                Daycares in New Delhi
              </Link>
              <Link href="/hyderabad" className="schoolLink">
                Preschools in Hyderabad
              </Link>
              <Link href="/hyderabad" className="schoolLink">
                Daycares in Hyderabad
              </Link>
              <Link href="/chennai" className="schoolLink">
                Preschools in Chennai
              </Link>
              <Link href="/chennai" className="schoolLink">
                Daycares in Chennai
              </Link>
              <Link href="/gurugram" className="schoolLink">
                Preschools in Gurugram
              </Link>
              <Link href="/gurugram" className="schoolLink">
                Daycares in Gurugram
              </Link>
              <Link href="/kolkata" className="schoolLink">
                Preschools in Kolkata
              </Link>
              <Link href="/kolkata" className="schoolLink">
                Daycares in Kolkata
              </Link>
            </GridAutoWraper>
          </div>
          <br />
          <div>
            <CityTitle>Preschools in popular areas, Bengaluru: </CityTitle>
            <GridAutoWraper>
              <Link href="/bengaluru/bannerghetta" className="schoolLink">
                Preschools in Bannerghetta
              </Link>
              <Link href="/bengaluru/gottigere" className="schoolLink">
                Preschools in Gottigere
              </Link>
              <Link href="/bengaluru/btm-layout" className="schoolLink">
                Preschools in BTM Layout
              </Link>
              <Link href="/bengaluru/hsr-layout" className="schoolLink">
                Preschools in HSR Layout
              </Link>
              <Link href="/bengaluru/indiranagar" className="schoolLink">
                Preschools in Indiranagar
              </Link>
              <Link href="/bengaluru/akshayanagar" className="schoolLink">
                Preschools in Akshayanagar
              </Link>
              <Link href="/bengaluru/whitefield" className="schoolLink">
                Preschools in Whitefield
              </Link>
              <Link href="/bengaluru/ananthnagar" className="schoolLink">
                Preschools in Ananthnagar
              </Link>
              <Link href="/bengaluru/electronic-city" className="schoolLink">
                Preschools in Electronic City
              </Link>
              <Link href="/bengaluru/varthur" className="schoolLink">
                Preschools in Varthur
              </Link>
              <Link href="/bengaluru/kadugodi" className="schoolLink">
                Preschools in Kadugodi
              </Link>
              <Link href="/bengaluru/frazer-town" className="schoolLink">
                Preschools in Frazer Town
              </Link>
              <Link href="/bengaluru/marathahalli" className="schoolLink">
                Preschools in Marathahalli
              </Link>
              <Link href="/bengaluru/haralur" className="schoolLink">
                Preschools in Haralur
              </Link>
              <Link href="/bengaluru/sarjapur" className="schoolLink">
                Preschools in Sarjapur
              </Link>
              <Link href="/bengaluru/bellandur" className="schoolLink">
                Preschools in Bellandur
              </Link>
              <Link href="/bengaluru/basavanagudi" className="schoolLink">
                Preschools in Basavanagudi
              </Link>
              <Link href="/bengaluru/govindraj-nagar" className="schoolLink">
                Preschools in Govindraj Nagar
              </Link>
              <Link href="/bengaluru/koramangala" className="schoolLink">
                Preschools in Koramangala
              </Link>
              <Link href="/bengaluru/chandra-layout" className="schoolLink">
                Preschools in Chandra Layout
              </Link>
              <Link href="/bengaluru/mathikere" className="schoolLink">
                Preschools in Mathikere
              </Link>
              <Link href="/bengaluru/kr-puram" className="schoolLink">
                Preschools in KR Puram
              </Link>
              <Link href="/bengaluru/vijayanagar" className="schoolLink">
                Preschools in Vijayanagar
              </Link>
              <Link href="/bengaluru/yelahanka" className="schoolLink">
                Preschools in Yelahanka
              </Link>
              <Link href="/bengaluru/jp-nagar" className="schoolLink">
                Preschools in JP Nagar
              </Link>
              <Link href="/bengaluru/jayanagar" className="schoolLink">
                Preschools in Jayanagar
              </Link>
              <Link href="/bengaluru/nagarbhavi" className="schoolLink">
                Preschools in Nagarbhavi
              </Link>
              <Link href="/bengaluru/singasandra" className="schoolLink">
                Preschools in Singasandra
              </Link>
              <Link href="/bengaluru/Vidyaranyapura" className="schoolLink">
                Preschools in Vidyaranyapura
              </Link>
            </GridAutoWraper>
          </div>
          <br />
          <div>
            <CityTitle>Daycares in popular areas, Bengaluru: </CityTitle>
            <GridAutoWraper>
              <Link
                href="/bengaluru/bannerghetta?&organisation=2"
                className="schoolLink"
              >
                Daycares in Bannerghetta
              </Link>
              <Link
                href="/bengaluru/gottigere?&organisation=2"
                className="schoolLink"
              >
                Daycares in Gottigere
              </Link>
              <Link
                href="/bengaluru/btm-layout?&organisation=2"
                className="schoolLink"
              >
                Daycares in BTM Layout
              </Link>
              <Link
                href="/bengaluru/hsr-layout?&organisation=2"
                className="schoolLink"
              >
                Daycares in HSR Layout
              </Link>
              <Link
                href="/bengaluru/indiranagar?&organisation=2"
                className="schoolLink"
              >
                Daycares in Indiranagar
              </Link>
              <Link
                href="/bengaluru/akshayanagar?&organisation=2"
                className="schoolLink"
              >
                Daycares in Akshayanagar
              </Link>
              <Link
                href="/bengaluru/whitefield?&organisation=2"
                className="schoolLink"
              >
                Daycares in Whitefield
              </Link>
              <Link
                href="/bengaluru/ananthnagar?&organisation=2"
                className="schoolLink"
              >
                Daycares in Ananthnagar
              </Link>
              <Link
                href="/bengaluru/electronic-city?&organisation=2"
                className="schoolLink"
              >
                Daycares in Electronic City
              </Link>
              <Link
                href="/bengaluru/varthur?&organisation=2"
                className="schoolLink"
              >
                Daycares in Varthur
              </Link>
              <Link
                href="/bengaluru/kadugodi?&organisation=2"
                className="schoolLink"
              >
                Daycares in Kadugodi
              </Link>
              <Link
                href="/bengaluru/frazer-town?&organisation=2"
                className="schoolLink"
              >
                Daycares in Frazer Town
              </Link>
              <Link
                href="/bengaluru/marathahalli?&organisation=2"
                className="schoolLink"
              >
                Daycares in Marathahalli
              </Link>
              <Link
                href="/bengaluru/haralur?&organisation=2"
                className="schoolLink"
              >
                Daycares in Haralur
              </Link>
              <Link
                href="/bengaluru/sarjapur?&organisation=2"
                className="schoolLink"
              >
                Daycares in Sarjapur
              </Link>
              <Link
                href="/bengaluru/bellandur?&organisation=2"
                className="schoolLink"
              >
                Daycares in Bellandur
              </Link>
              <Link
                href="/bengaluru/basavanagudi?&organisation=2"
                className="schoolLink"
              >
                Daycares in Basavanagudi
              </Link>
              <Link
                href="/bengaluru/govindraj-nagar?&organisation=2"
                className="schoolLink"
              >
                Daycares in Govindraj Nagar
              </Link>
              <Link
                href="/bengaluru/koramangala?&organisation=2"
                className="schoolLink"
              >
                Daycares in Koramangala
              </Link>
              <Link
                href="/bengaluru/chandra-layout?&organisation=2"
                className="schoolLink"
              >
                Daycares in Chandra Layout
              </Link>
              <Link
                href="/bengaluru/mathikere?&organisation=2"
                className="schoolLink"
              >
                Daycares in Mathikere
              </Link>
              <Link
                href="/bengaluru/kr-puram?&organisation=2"
                className="schoolLink"
              >
                Daycares in KR Puram
              </Link>
              <Link
                href="/bengaluru/vijayanagar?&organisation=2"
                className="schoolLink"
              >
                Daycares in Vijayanagar
              </Link>
              <Link
                href="/bengaluru/yelahanka?&organisation=2"
                className="schoolLink"
              >
                Daycares in Yelahanka
              </Link>
              <Link
                href="/bengaluru/jp-nagar?&organisation=2"
                className="schoolLink"
              >
                Daycares in JP Nagar
              </Link>
              <Link
                href="/bengaluru/nagarbhavi?&organisation=2"
                className="schoolLink"
              >
                Daycares in Nagarbhavi
              </Link>
              <Link
                href="/bengaluru/jayanagar?&organisation=2"
                className="schoolLink"
              >
                Daycares in Jayanagar
              </Link>
              <Link
                href="/bengaluru/singsandra?&organisation=2"
                className="schoolLink"
              >
                Daycares in Singsandra
              </Link>
              <Link
                href="/bengaluru/Vidyaranyapura?&organisation=2"
                className="schoolLink"
              >
                Preschools in Vidyaranyapura
              </Link>
            </GridAutoWraper>
          </div>
          <br />
          <div>
            <CityTitle>Preschools in popular areas, Pune </CityTitle>
            <GridAutoWraper>
              <Link href="/pune/kharadi" className="schoolLink">
                Preschools in Kharadi
              </Link>
              <Link href="/pune/baner" className="schoolLink">
                Preschools in Baner
              </Link>
              <Link href="/pune/hadapsar" className="schoolLink">
                Preschools in Hadapsar
              </Link>
              <Link href="/pune/kothrud" className="schoolLink">
                Preschools in Kothrud
              </Link>
              <Link href="/pune/wagholi" className="schoolLink">
                Preschools in Wagholi
              </Link>
              <Link href="/pune/aundh" className="schoolLink">
                Preschools in Aundh
              </Link>
              <Link href="/pune/pimpri-chinchwad" className="schoolLink">
                Preschools in Pimpri Chinchwad
              </Link>
              <Link href="/pune/wakad" className="schoolLink">
                Preschools in Wakad
              </Link>
              <Link href="/pune/magarpatta" className="schoolLink">
                Preschools in Magarpatta
              </Link>
              <Link href="/pune/warje" className="schoolLink">
                Preschools in Warje
              </Link>
              <Link href="/pune/lohegaon" className="schoolLink">
                Preschools in Lohegaon
              </Link>
              <Link href="/pune/kondhwa" className="schoolLink">
                Preschools in Kondhwa
              </Link>
              <Link href="/pune/talegaon" className="schoolLink">
                Preschools in Talegaon
              </Link>
              <Link href="/pune/pimple-saudagar" className="schoolLink">
                Preschools in Pimple Saudagar
              </Link>
              <Link href="/pune/bavdhan" className="schoolLink">
                Preschools in Bavdhan
              </Link>
              <Link href="/pune/kalyani-nagar" className="schoolLink">
                Preschools in Kalyani Nagar
              </Link>
              <Link href="/pune/moshi" className="schoolLink">
                Preschools in Moshi
              </Link>
              <Link href="/pune/hinjawadi" className="schoolLink">
                Preschools in Hinjawadi
              </Link>
            </GridAutoWraper>
          </div>
          <br />
          <div>
            <CityTitle>Daycares in popular areas, Pune </CityTitle>
            <GridAutoWraper>
              <Link href="/pune/kharadi?&organisation=2" className="schoolLink">
                Daycares in Kharadi
              </Link>
              <Link href="/pune/baner?&organisation=2" className="schoolLink">
                Daycares in Baner
              </Link>
              <Link
                href="/pune/hadapsar?&organisation=2"
                className="schoolLink"
              >
                Daycares in Hadapsar
              </Link>
              <Link href="/pune/kothrud?&organisation=2" className="schoolLink">
                Daycares in Kothrud
              </Link>
              <Link href="/pune/wagholi?&organisation=2" className="schoolLink">
                Daycares in Wagholi
              </Link>
              <Link href="/pune/aundh?&organisation=2" className="schoolLink">
                Daycares in Aundh
              </Link>
              <Link
                href="/pune/pimpri-chinchwad?&organisation=2"
                className="schoolLink"
              >
                Daycares in Pimpri Chinchwad
              </Link>
              <Link href="/pune/wakad?&organisation=2" className="schoolLink">
                Daycares in Wakad
              </Link>
              <Link
                href="/pune/magarpatta?&organisation=2"
                className="schoolLink"
              >
                Daycares in Magarpatta
              </Link>
              <Link href="/pune/warje?&organisation=2" className="schoolLink">
                Daycares in Warje
              </Link>
              <Link
                href="/pune/lohegoan?&organisation=2"
                className="schoolLink"
              >
                Daycares in Lohegoan
              </Link>
              <Link href="/pune/kondhwa?&organisation=2" className="schoolLink">
                Daycares in Kondhwa
              </Link>
              <Link
                href="/pune/talegaon?&organisation=2"
                className="schoolLink"
              >
                Daycares in Talegaon
              </Link>
              <Link
                href="/pune/pimple-saudagar?&organisation=2"
                className="schoolLink"
              >
                Daycares in Pimple Saudagar
              </Link>
              <Link href="/pune/bavdhan?&organisation=2" className="schoolLink">
                Daycares in Bavdhan
              </Link>
              <Link
                href="/pune/kalyani-nagar?&organisation=2"
                className="schoolLink"
              >
                Daycares in Kalyani Nagar
              </Link>
              <Link href="/pune/moshi?&organisation=2" className="schoolLink">
                Daycares in Moshi
              </Link>
              <Link
                href="/pune/hinjawadi?&organisation=2"
                className="schoolLink"
              >
                Daycares in Hinjawadi
              </Link>
            </GridAutoWraper>
          </div>
          <br />
          <div>
            <CityTitle>Popular Preschools & Daycares</CityTitle>
            <GridAutoWraper>
              <Link
                href="/bengaluru/vidyaranyapura/cherish-kidz-an-indi-international-preschool-and-daycare-1820952"
                className="school_link"
              >
                Cherish Kids
              </Link>
              <Link
                href="/bengaluru/k-r-puram/discovery-montessori-usa-1785493"
                className="school_link"
              >
                Discovery Montessori USA
              </Link>
              <Link
                href="/bangalore/padmanabhanagar/ace-montessori-1783520"
                className="school_link"
              >
                Ace Montessori
              </Link>
              <Link
                href="/bengaluru/vijaynagar/step-by-step-the-learning-center-1787911"
                className="school_link"
              >
                Step by Step The Learning Center
              </Link>
              <Link
                href="/bangalore/gottigere/aura-montessori-house-of-children-1788833"
                className="school_link"
              >
                Aura Montessori
              </Link>
              <Link
                href="/bangalore/gottigere/the-pearls-montessori-1795828"
                className="school_link"
              >
                The Pearls Montessori
              </Link>
              <Link
                href="/bangalore/gottigere-banerghatta/greenwood-high-preschool-227730"
                className="school_link"
              >
                Greenwood High
              </Link>
              <Link
                href="/bengaluru/bohra-layout-gottigere/kalpaa-pre-school-1739888"
                className="school_link"
              >
                Kalpaa Preschools
              </Link>
              <Link
                href="/bengaluru/koramangala/united-world-academy-1713483"
                className="school_link"
              >
                United World Academy
              </Link>
              <Link
                href="/bengaluru/koramangala-1st-block/koala-preschool-181624"
                className="school_link"
              >
                Koala
              </Link>
              <Link
                href="/bengaluru/koramangala/podar-jumbo-kids-393240"
                className="school_link"
              >
                Podar Jumbo Kids
              </Link>
              <Link
                href="/bangalore/rpc-layout/st-michaels-kindergarten-1790584"
                className="school_link"
              >
                St. Michaels Kindergarten
              </Link>
              <Link
                href="/bangalore/moodalapalya/kidsbee-play-home-1793959"
                className="school_link"
              >
                Kidsbee Playhome
              </Link>
              <Link
                href="/bengaluru/govindraj-nagar/akshara-little-bees-pre-school-1786216"
                className="school_link"
              >
                Akshara Little Bees
              </Link>
              <Link
                href="/bengaluru/v-v-puram/edumeta-the-i-school-1794859"
                className="school_link"
              >
                Edumeta the Ischool
              </Link>
              <Link
                href="/bengaluru/vidyaranyapura/kidzee-vidyaranyapura-1798722"
                className="school_link"
              >
                Kidzee Vidyaranyapura
              </Link>
              <Link
                href="/bengaluru/vidyaranyapura/gurukull-vidyalaya-1797820"
                className="school_link"
              >
                Gurukull Vidyalaya
              </Link>
              <Link
                href="/bengaluru/phase-1/rainbow-kids-international-preschool-and-daycare-ananth-nagar-1799426"
                className="school_link"
              >
                Rainbow Kids International
              </Link>
              <Link
                href="/bengaluru/kadugodi/vedavihaan-the-global-school-best-pre-school-day-care-belathur-campus-3-1815367"
                className="school_link"
              >
                Vedavihaan
              </Link>
              <Link
                href="/bangalore/bannerghatta-road,/inspiro-preschool-daycare-1814585"
                className="school_link"
              >
                Inspiro Preschools
              </Link>
              <Link
                href="/pune/kharadi/euro-kids-kharadi-1809387"
                className="school_link"
              >
                Eurokids Kharadi
              </Link>
              <Link
                href="/bengaluru/vijaynagar/jaanvi-preschool-1807442"
                className="school_link"
              >
                Jaanvi Preschools
              </Link>
              <Link
                href="/bengaluru/rmv-2nd-stage/20-1806636"
                className="school_link"
              >
                Kids Castle
              </Link>
              <Link
                href="/bengaluru/jayanagar/podar-jumbo-kids-plus-1816916"
                className="school_link"
              >
                Podar Jumbo
              </Link>
            </GridAutoWraper>
          </div>
        </FatFooterContainer>
      </MainWrapper>

      <Footer bgColor="#F8F4FE" {...props} />
    </>
  );
};

export default LandingPage;

// const MainSection = styled.div`
//   // position: relative;
//   // height:753px;
//   // background-image: url("${background}");
//   // background-size:100% 100%;
//   // z-index: 10;
//   // margin-top:70px;
//   // @media only screen and (max-width:2560px) and (min-width:1441px)
//   // {
//   //   background-size:cover;
//   //   height:900px;
//   // }
//   // @media only screen and (max-width: 1300px) {
//   //   height:750px;
//   // }
//   // @media only screen and (max-width: 1100px) {
//   //   height:650px;
//   // }
//   // @media only screen and (max-width: 900px) {
//   //   height:600px;
//   // }

//   // @media only screen and (max-width: 500px) {
//   //   background-size: 100% 65% !important;
//   //   background-repeat: no-repeat !important;
//   //   background-position: left;
//   //   height:341px !important;
//   //   max-height: 341px !important;
//   //   margin-top:40px !important;
//   // }

// `;

const CityTitle = styled.div`
  font-family: 'Quicksand', sans-serif;
  color: #2d3438;
  font-weight: bold;
  font-size: 0.9rem;
  padding-bottom: 5px;
`;
const FatFooterContainer = styled.div`
  padding: 30px 20px 100px;
`;
const GridAutoWraper = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(167px, 1fr));
  row-gap: 4px;
  color: #616161 !important;
  font-size: 0.75rem;
  font-family: 'Roboto', sans-serif;

  @media screen and (max-width: 767px) {
    grid-template-columns: repeat(2, 1fr);
  }

  a {
    color: #30333b !important;
    text-decoration: none !important;
  }
`;

const LandingTitle = styled.h1`
  color: #000;
  font-family: 'Quicksand', sans-serif;
  font-size: 34px;
  font-weight: bold;
  // font-weight: 500;
  line-height: 10px;
  text-align: center;
  margin: 100px 0px 18px;
  @media screen and (max-width: 900px) {
    font-size: 30px;
    line-height: 30px;
    margin: 20px 5px;
  }
  @media screen and (max-width: 768px) {
    font-weight: bold;
    font-size: 18px;
    line-height: 22px;
    text-align: center;
  }
  @media screen and (max-width: 500px) {
    font-size: 18px;
    line-height: 25px;
    margin: 0px 0px 10px;
    padding: 0px 15px;
  }
`;

const ExtraCurricularSection = styled.section`
  margin: 0 auto;
  max-width: 1180px;
  border-bottom: 1px solid #ccc;
  padding-bottom: 70px;
  @media screen and (max-width: 768px) {
    padding-bottom: 40px;
    margin-bottom: 30px;
  }

  h2 {
    color: #000;
    font-family: 'Quicksand', sans-serif;
    font-size: 34px;
    font-weight: 500;
    line-height: 40px;
    text-align: center;
    margin: 100px 0px 45px;
    @media screen and (max-width: 768px) {
      font-weight: bold;
      font-size: 18px;
      line-height: 22px;
      text-align: center;
      margin: 10px auto 30px;
    }
  }
  ul {
    margin: 0;
    padding: 0;
    list-style-type: none;
    display: flex;
    flex-wrap: wrap;
    align-items: flex-start;
    justify-content: space-around;
    @media screen and (max-width: 500px) {
      justify-content: space-evenly;
    }

    li {
      width: 28%;
      background: #fff;
      // border-radius: 30px;
      @media screen and (max-width: 768px) {
        width: 45%;
      }
      @media screen and (max-width: 320px) {
        width: 47%;
      }
      div.grouping {
        box-shadow: 0px 6px 10px #ccc;
        border-radius: 20px;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        margin-bottom: 60px;
        @media screen and (max-width: 768px) {
          margin-bottom: 30px;
        }
        div.top {
          min-height: 150px;
          display: flex;
          align-items: flex-end;
          justify-content: center;
          margin-bottom: 25px;
          @media screen and (max-width: 500px) {
            min-height: 95px;
          }
          img {
            @media screen and (max-width: 768px) {
              max-width: 60%;
            }
          }
        }
        div.bottom {
          background: #fff;
          width: 100%;
          border-bottom-left-radius: 20px;
          border-bottom-right-radius: 20px;
          text-align: center;
          @media screen and (max-width: 500px) {
            padding: 0 10px;
          }
          h4 {
            color: #000;
            font-family: 'Quicksand', sans-serif;
            font-size: 22px;
            font-weight: 600;
            line-height: 40px;
            text-align: center;
            margin: 20px 0 0 0;
            @media screen and (max-width: 768px) {
              font-size: 18px;
              line-height: 1.6;
            }
            @media screen and (max-width: 500px) {
              margin-bottom: 10px;
              line-height: 1.2;
              margin: 14px 0 10px;
              font-size: 12px;
            }
          }
          p {
            margin: 0 0 30px;
            font-family: 'Roboto', sans-serif;
            @media screen and (max-width: 768px) {
              font-size: 14px;
              line-height: 1.6;
            }
            @media screen and (max-width: 500px) {
              line-height: 1.4;
              min-height: 32px;
              font-size: 11px;
              margin-bottom: 15px;
            }
          }
        }
      }
      div.green {
        background: #defed4;
      }
      div.voilet {
        background: #efe3fe;
      }
      div.orange {
        background: #ffe7d4;
      }
    }
  }
  button {
    margin: 0 auto !important;
  }
`;

const OnlinePreSchoolSection = styled.section`
  text-align: center;
  padding: 64px 0 79px;
  width: 100%;
  max-width: 1010px;
  margin: 0 auto;
  // border-bottom: 1px solid #e6e8ec;
  @media screen and (max-width: 1100px) {
    max-width: 800px;
  }
  @media screen and (max-width: 890px) {
    max-width: 680px;
  }
  @media screen and (max-width: 768px) {
    max-width: 320px;
    padding: 0 0 30px;
  }
  .slick-track {
    padding: 40px 0px;
    @media screen and (max-width: 768px) {
      padding: 0;
    }
  }
  .slick-slide > div {
    margin: 0 10px;
    @media screen and (max-width: 1350px) {
      margin: 0 15px;
    }
    @media screen and (max-width: 1250px) {
      margin: 0 10px;
    }
    > div {
      outline: none !important;
      border: none !important;
    }
  }
  h2 {
    font-family: Roboto;
    font-style: normal;
    font-weight: normal;
    font-size: 36px;
    line-height: 42px;
    color: #000000;
    margin: 0 0 34px;
    @media screen and (max-width: 768px) {
      font-size: 22px;
      margin: 0 0 20px;
    }
  }
  .buttonsWrapper {
    margin-bottom: 55px;
  }
  .mobileButtonsWrapper {
    @media screen and (max-width: 768px) {
      flex-direction: column;
      .mobile-button {
        margin-bottom: 10px;
        width: 200px;
      }
    }
  }
  .slick-prev,
  .slick-next {
    &:before {
      color: #00000075;
      font-size: 36px;
      font-weight: 300;
      @media screen and (max-width: 768px) {
        font-size: 14px;
        font-weight: 700;
        color: #00000099 !important;
        opacity: 1 !important;
      }
    }
  }
  .slick-prev:before {
    content: 'ᐸ';
  }
  .slick-next:before {
    content: 'ᐳ';
  }
  .slick-prev {
    left: -40px;
    @media screen and (max-width: 1100px) {
      left: 5px;
    }
    @media screen and (max-width: 890px) {
      left: -33px;
    }
    @media screen and (max-width: 768px) {
      background: #ffffff9e;
      right: auto;
      bottom: 0;
      left: -53px;
      margin: 0 auto;
      top: 18%;
      border: 1px solid #00000099;
      border-radius: 100%;
      z-index: 9;
      width: 40px;
      height: 40px;
      @media screen and (max-width: 435px) {
        width: 25px;
        height: 25px;
        left: -31px;
      }
      @media screen and (max-width: 390px) {
        width: 40px;
        height: 40px;
        left: 15px;
        top: 49.5%;
      }
    }
  }
  .slick-next {
    @media screen and (max-width: 1100px) {
      right: 5px;
    }
    @media screen and (max-width: 890px) {
      right: -25px;
    }
    @media screen and (max-width: 768px) {
      background: #ffffff9e;
      right: -53px;
      bottom: 0;
      left: auto;
      margin: 0 auto;
      top: 18%;
      border: 1px solid #00000099;
      border-radius: 100%;
      z-index: 9;
      width: 40px;
      height: 40px;
    }
    @media screen and (max-width: 435px) {
      width: 25px;
      height: 25px;
      right: -31px;
    }
    @media screen and (max-width: 390px) {
      width: 40px;
      height: 40px;
      right: 15px;
      top: 49.5%;
    }
  }
  button.joinUs {
    margin: 0 auto;
  }
`;

// const OfflinePreSchoolSection = styled.section`
//   max-width: 1200px;
//   margin: 0 auto;
//   padding: 58px 0 66px;
//   border-bottom: 1px solid #e6e8ec;
//   border-top: 1px solid #e6e8ec;
//   display: flex;
//   align-items: center;
//   justify-content: space-between;
//   @media screen and (max-width: 768px) {
//     flex-direction: column;
//     align-items: center;
//     padding: 30px 0;
//     margin-bottom: 30px;
//   }
//   .left {
//     @media screen and (max-width: 768px) {
//       margin-bottom: 24px;
//     }
//     img {
//       @media screen and (max-width: 768px) {
//         max-width: 65%;
//         margin: 0 auto;
//         display: table;
//       }
//     }
//     h2.mobile {
//       display: none;
//       font-family: Quicksand;
//       font-style: normal;
//       font-weight: bold;
//       font-size: 34px;
//       line-height: 42px;
//       color: #000000;
//       margin-bottom: 45px;
//       @media screen and (max-width: 768px) {
//         display: block;
//         font-size: 16px;
//         text-align: center;
//         line-height: 20px;
//         color: #000000;
//         margin-bottom: 34px;
//       }
//     }
//   }
//   .right {
//     max-width: 65%;
//     @media screen and (max-width: 768px) {
//       text-align: center;
//     }
//     h2 {
//       font-family: Quicksand;
//       font-style: normal;
//       font-weight: bold;
//       font-size: 34px;
//       line-height: 42px;
//       color: #000000;
//       margin-bottom: 45px;
//       @media screen and (max-width: 768px) {
//         display: none;
//       }
//     }
//     p {
//       font-family: Roboto;
//       font-style: normal;
//       font-weight: normal;
//       font-size: 22px;
//       line-height: 26px;
//       color: #666c78;
//       margin-bottom: 45px;
//       @media screen and (max-width: 768px) {
//         font-size: 12px;
//         line-height: 14px;
//         text-align: center;

//         color: #666c78;
//         margin-bottom: 24px;
//       }
//     }
//     button {
//       display: block;
//       border: 1ps solid #666c78;
//       border-radius: 5px;
//       background: #fff;
//       font-family: Quicksand;
//       font-style: normal;
//       font-weight: 500;
//       font-size: 12px;
//       line-height: 15px;
//       text-align: center;
//       color: #30333b;
//       width: 135px;
//       height: 37px;
//       cursor: pointer;
//       @media screen and (max-width: 768px) {
//         width: 135px;
//         height: 37px;
//         font-weight: 500;
//         font-size: 12px;
//         line-height: 15px;
//         margin: 0 auto;
//       }
//       &:hover {
//         transform: scale(1.05);
//         transition: 0.5s;
//       }
//     }
//   }
// `;

const OfferSection = styled.div`
  margin: 40px auto 0px;
  padding: 0px 30px;
  width: 100%;
  max-width: 1200px;
  @media screen and (max-width: 500px) {
    margin: 40px auto 0px;
  }
  .kiddenoffer-section {
    @media screen and (max-width: 900px) {
      padding: 0px 0px 70px;
    }
    @media screen and (max-width: 767px) {
      flex-direction: column;
      padding: 0px 0px 50px;
      align-items: center;
    }
  }
`;

const ForParents = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 50px 0px 56px 0px;
  border-top: 1px solid #e6e8ec;
  // & > div {
  //   // @media screen and (max-width: 900px) {
  //   //   width: 48%;
  //   // }
  //   @media screen and (max-width: 767px) {
  //     width: 100%;
  //   }
  // }
  @media screen and (max-width: 767px) {
    flex-direction: column;
    padding: 50px 0px 50px 0px;
  }
  .forParents-desc {
    color: #30333b;
    font-family: 'Quicksand', sans-serif;
    font-size: 22px;
    line-height: 36px;
    margin: 40px 0px;
    padding: 0px 85px;
    text-align: center;
    @media screen and (max-width: 1100px) {
      font-size: 20px;
      line-height: 30px;
      padding: 0px;
    }
    @media screen and (max-width: 900px) {
      font-size: 18px;
      line-height: 28px;
    }
    @media screen and (max-width: 767px) {
      font-size: 14px;
      line-height: 24px;
    }
  }
  .forParents-text {
    color: #666c78;
    font-family: 'Roboto', sans-serif;
    font-size: 16px;
    line-height: 24px;
    @media screen and (max-width: 900px) {
      font-size: 14px;
      line-height: 20px;
    }
    @media screen and (max-width: 767px) {
      margin: 30px 0px;
    }
  }
  // .registerBtn {
  //   width: 30%;
  // }
`;

// const ForParentsList = styled.ul`
//   margin: 0px 0px 30px 0px;
//   padding: 0px;
//   li {
//     display: flex;
//     color: #666c78;
//     font-family: 'Roboto', sans-serif;
//     font-size: 18px;
//     line-height: 34px;
//     text-transform: capitalize;
//     @media screen and (max-width: 1100px) {
//       font-size: 16px;
//       line-height: 24px;
//     }
//   }
//   img {
//     margin-right: 20px;
//   }
// `;

const VideoImage = styled.div`
  width: 100%;
  height: 373px;
  cursor: pointer;
  img {
    width: 100%;
    height: 100%;
    object-fit: contain;
  }
  @media screen and (max-width: 500px) {
    height: auto;
  }
`;
const JoinKiddens = styled.div`
  position: relative;
  // padding:130px 0px 400px;
  & > img {
    position: absolute;
    top: 0px;
    height: 100%;
    @media screen and (min-width: 1441px) and (max-width: 2560px) {
      width: 100%;
    }
    @media (max-width: 500px) {
      width: 300%;
    }
  }
  .whiteBg {
    position: absolute;
    height: 200px;
    background-color: #fff;
    left: 0px;
    right: 0px;
    top: -1px;
  }
  .join-kiddenz {
    @media screen and (max-width: 1100px) {
      padding: 150px 30px 180px;
    }
    @media screen and (max-width: 767px) {
      flex-direction: column;
    }
  }
`;
const JoinKiddensImage = styled.div`
  width: 544px;
  min-width: 544px;
  img {
    width: 100%;
    height: 100%;
    @media screen and (max-width: 1100px) {
      padding-bottom: 100px;
    }
    @media screen and (max-width: 767px) {
      padding-bottom: 50px;
    }
  }
  @media screen and (max-width: 1100px) {
    width: 444px;
    min-width: 444px;
  }
  @media screen and (max-width: 900px) and (min-width: 768px) {
    width: 400px;
    min-width: 400px;
  }
  @media screen and (max-width: 767px) {
    width: 100%;
    min-width: 0px;
  }
`;
const JoinKiddensContent = styled.div`
  margin-left: 50px;
  max-width: 488px;
  width: 100%;
  margin-right: auto;
  @media screen and (max-width: 1100px) {
    margin-left: 30px;
  }
  @media screen and (max-width: 767px) {
    margin-left: 0px;
  }
  h3 {
    color: #ffffff;
    font-family: 'Quicksand', sans-serif;
    font-size: 32px;
    font-weight: bold;
    line-height: 40px;
    margin: 0px 0px 30px 0px;
    @media screen and (max-width: 500px) {
      font-size: 18px;
      line-height: 1.2;
      margin-bottom: 20px;
    }
  }
  p {
    width: 100%;
    color: #ffffff;
    font-family: 'Quicksand', sans-serif;
    font-size: 24px;
    line-height: 36px;
    margin-bottom: 48px;
    @media screen and (max-width: 1100px) {
      font-size: 20px;
      line-height: 30px;
    }
    @media screen and (max-width: 900px) {
      font-size: 18px;
      margin-bottom: 30px;
    }
    @media screen and (max-width: 767px) {
      // font-size: 14px;
      // margin-bottom: 24px;
      font-size: 14px;
      margin-bottom: 16px;
      line-height: 1.6;
    }
    @media screen and (max-width: 500px) {
      text-align: center;
    }
  }
  .registerBtn {
    width: fit-content;
    margin: 0px auto;

    @media screen and (max-width: 500px) {
      margin: 0px auto;
    }
  }
`;

const SearchLoader = styled.div`
  height: calc(100vh - 266px);
  width: 100%;
`;

// const Testimonials = styled.div`
//   width: 100%;
//   max-width: 830px;
//   display: flex;
//   padding: 30px 30px 50px;
//   margin: 0px auto;
//   flex-direction: column;
//   align-items: center;
//   // .slick-slide
//   // {
//   //   margin:20px;
//   // }
//   @media screen and (max-width: 767px) {
//     padding: 30px 30px 70px;
//   }
//   @media screen and (max-width: 500px) {
//     padding: 30px 0px 70px;
//   }
//   .slick-dots {
//     bottom: 0px;
//   }
//   .slick-dots li {
//     width: 14px;
//     margin: 0px;
//   }
//   .slick-dots li button:before {
//     opacity: 1;
//     font-size: 22px;
//     color: #d6dae2;
//   }
//   .slick-dots li.slick-active button:before {
//     opacity: 1;
//     color: #613a95;
//   }
// `;

const InterestingRead = styled.div`
  width: 100%;
  max-width: 1180px;
  display: flex;
  // border-top: 1px solid #e6e8ec;
  padding: 30px 20px 100px;
  margin: 0px auto;
  flex-direction: column;
  .interestContainer {
    // flex-wrap:row wrap;
    // &::after
    // {
    //   content:"";
    //   width: 30.5%;
    // }
    @media screen and (max-width: 765px) {
      flex-direction: column;
      align-items: center;
    }
  }
  @media screen and (max-width: 765px) {
    padding: 30px 20px 30px;
  }
`;
const SubInfo = styled.div`
  color: #30333b;
  font-family: 'Quicksand', sans-serif;
  font-size: 16px;
  font-weight: 500;
  line-height: 20px;
  margin-bottom: 30px;
`;

const MainSection = styled.section`
position: relative;
  height: 610px;
  padding-top: 86px;
  // background: ${colors.secondary};
  background-size: 100%;
  z-index: 10;

  &>img{
    position :absolute;
    left:0;
    top:0px;
    z-index: -1;
    @media screen and (min-width:1441px) and (max-width:2560px)
  {
    width:100%;
  }

    @media (max-width: 1200px){
      height:103%;
    }
   
    @media (max-width: 500px){
      height:107%;
      left:-50%;
    }
  }
  @media screen and (min-width:1441px) and (max-width:2560px)
  {
    height: 850px;
  }
  @media (max-width: 900px){
    padding-top: 20px;
    height:730px;
   }
   @media (max-width: 500px){
    height:690px;
  }


`;

const Msg = styled.div`
  margin-top: 10px;
  margin-left: 35px;
  width: 70%;
  color: ${colors.white};
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 19px;
  min-height: 38px;
  @media (max-width: 768px) {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    margin-top: 50px;
    margin-left: 50%;
  }
  &.styledMsg {
    @media (max-width: 768px) {
      display: none;
      margin-left: -50%;
    }
    @media (max-width: 414px) {
      margin-left: -100%;
    }
  }
  @media (max-width: 900px) {
    width: 90%;
    margin-left: 33px;
    margin-top: 30px;
  }
`;
const SearchWrapper = styled(FlexSection)`
  @media (max-width: 900px) {
    flex-direction: column;
    padding: 40px 30px;
  }
  @media (max-width: 500px) {
    flex-direction: column;
    padding: 40px 15px;
  }
`;
const SearchOption = styled(Flex)`
  position: absolute;
  top: ${props => props.top || '115px'};
  left: 0px;
  width: 100%;
  max-width: 442px;
  padding: 20px 20px 40px;
  border-radius: 5px;
  background: ${colors.white};
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.3);
  z-index: 1000000;
  .google {
    position: absolute;
    right: 20px;
    bottom: 0px;
    // padding:20px 0px;
    @media (max-width: 500px) {
      right: 20px;
    }
    @media (min-width: 1400px) {
      display: block !important;
    }
  }
  @media (max-width: 500px) {
    left: 0px;
    top: 90px;
  }
  .google {
    position: absolute;
    right: 20px;
    bottom: 0px;
    // padding:20px 0px;
    @media (max-width: 500px) {
      right: 20px;
    }
    @media (min-width: 1400px) {
      display: block !important;
    }
  }
  &.single {
    top: 90px;
    @media (max-width: 500px) {
      top: 100px;
    }
  }

  li {
    font-size: 14px;
    line-height: 30px;
    border-bottom: 1px solid rgba(0, 0, 0, 0.1);
    list-style-type: none;
    padding: 5px 0px;
    color: #666c78;
    cursor: pointer;
    &:last-child {
      border-bottom: 0px solid rgba(0, 0, 0, 0.1);
    }
    &:hover {
      color: #000;
    }
  }
`;
const Location = styled(Flex)`
  height: 113px;
  width: 442px;
  padding: 12px 14px;
  // margin: 27px 0px 0px 0px;
  border-radius: 5px;
  background: ${colors.white};

  .findLocation {
    & > div:first-child {
      input {
        max-width: 210px;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        text-align: left;
        text-decoration: none;
        display: inline-block;
        @media (max-width: 500px) {
          font-size: 14px;
          margin-top: 3px;
          background-color: transparent !important;
        }
      }
    }
    &-from {
      position: relative;
      @media (max-width: 500px) {
        padding-right: 122px;
      }
      &.error::after {
        position: absolute;
        content: '';
        bottom: 0px;
        left: 20px;
        height: 4px;
        background-color: #ff6767;
        width: 94%;
        border-radius: 14px;
        @media (max-width: 500px) {
          left: 12px;
        }
      }
      &.error input::-webkit-input-placeholder {
        color: #ff6767;
      }
    }
    &-to {
      position: relative;

      &.error::after {
        position: absolute;
        content: '';
        bottom: -5px;
        left: 20px;
        height: 4px;
        background-color: #ff6767;
        width: 94%;
        border-radius: 14px;
        @media (max-width: 500px) {
          bottom: 2px;
          left: 12px;
          width: 160%;
        }
      }
      &.error input::-webkit-input-placeholder {
        color: #ff6767;
      }
    }
  }
  @media (max-width: 500px) {
    width: 100%;
    height: 86px;
    padding: 5px 14px;
    .findLocation {
      width: 93% !important;
      & + div {
        width: 34% !important;
        @media (max-width: 500px) {
          width: 36% !important;
        }
      }
      input:disabled {
        background-color: transparent !important;
        margin: 5px 0px;
      }
    }
  }
  @media (max-width: 320px) {
    width: 290px;
    margin: 10px 0px 0px 0px;
  }
`;
const LocationFromTo = styled.ul`
  list-style-type: none;
  padding: 0px;
  margin: 0px;
  margin-right: 12px;
  margin-top: -7px;
  @media (max-width: 500px) {
    margin-top: -4px;
  }
`;
const LocationFrom = styled.li`
  position: relative;
  height: 14px;
  width: 14px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 10px;
  border: 1px solid #63cab9;
  // box-shadow: 0px 0px 5px #63cab9;
  background: ${colors.white};
  border-radius: 50px;

  div {
    height: 8px;
    width: 8px;
    border-radius: 50px;
    background: #63cab9;
    color: #63cab9;
  }
  &:after {
    content: '';
    position: absolute;
    top: 15px;
    left: 5px;
    height: 40px;
    border-left: 1px dashed #bfc4ce;
    @media (max-width: 500px) {
      height: 31px !important;
    }
  }
`;
const Destination = styled.input`
  width: 100%;
  margin-bottom: 20px;
  font-size: 16px;
  border: 0px;
  &:disabled {
    background-color: transparent !important;
  }
  &:focus {
    outline: 0;
    border: 0px;
  }

  @media (max-width: 500px) {
    margin-bottom: 14px;
    font-size: 14px;
    margin-top: 11px;
    background-color: transparent !important;
  }
`;
const LocationTo = styled.li`
  margin-top: 35px;
  list-style-type: none;
  @media (max-width: 500px) {
    margin-top: 26px;
  }

  img {
    position: static;
    height: 14;
    width: 12;
  }
`;

const TimeInfo = styled.div`
  border-radius: 5px;
  background-color: #eff2f4;
  // padding: 5px 10px;
  color: ${colors.inputPlaceholder};
  font-family: 'Quicksand', sans-serif;
  font-size: 14px;
  font-weight: 500;
  line-height: 18px;
  position: absolute;
  right: 9px;
  top: 6px;
  height: fit-content;
  & > div {
    @media (max-width: 500px) {
      padding: 2px 0px 2px 4px;
    }
  }
  .rc-time-picker {
    width: 100%;
    padding-left: 0px;
  }
  .rc-time-picker-input {
    @media (max-width: 500px) {
      padding: 7px 0px 4px;
      font-size: 11px;
    }
  }
  @media (max-width: 375px) {
    font-size: 12px;
  }
  .timeInfo {
    @media (max-width: 500px) {
      padding: 6px 8px 6px 5px;
    }
    @media (max-width: 320px) {
      margin-bottom: 0px;
    }
  }
`;
const DestinationError = styled.div`
  margin: 10px 0px 0px 0px;
  font-family: Roboto;
  font-size: 14px;
  line-height: 19px;
  font-weight: 500;
  border-radius: 3px;
  position: absolute;
  left: 200px;
  width: 55%;
  bottom: -55px;
  background-color: #fff;
  color: #ff6767;
  padding: 10px 12px;
  max-width: 442px;
  &::after {
    content: '';
    position: absolute;
    left: 18px;
    top: -8px;
    width: 0;
    height: 0;
    border-radius: 4px;
    border-left: 10px solid transparent;
    border-right: 10px solid transparent;
    border-top: 10px solid #fff;
    clear: both;
    transform: rotate(180deg);
  }
  @media (max-width: 500px) {
    font-size: 10px;
    left: 165px;
    width: 54%;
    bottom: -50px;
    padding: 8px 8px;
  }
`;

const Layout = styled.section`
  height: 280px;
  width: 50%;
  padding-left: 116px;

  .buttonWrapper {
    @media (max-width: 900px) {
      justify-content: flex-start !important;
    }
    & > button {
      padding: 0px 56px;
      @media (max-width: 500px) {
        height: 40px;
        font-size: 14px;
      }
    }
  }
  .locationInput {
    width: 95%;
    @media (max-width: 500px) {
      height: 40px;
      margin-right: 0px;
    }
    &::-webkit-input-placeholder {
      @media (max-width: 500px) {
        font-size: 14px;
      }
    }
  }
  @media (max-width: 900px) {
    padding-left: 0px;
    height: 200px;
    width: 100%;
  }
  &:last-child {
    padding-left: 103px;
    @media (max-width: 1152px) {
      padding-left: 30px;
    }
    @media (max-width: 900px) {
      padding-left: 0px;
    }
  }
  &:first-child {
    @media (max-width: 768px) {
      // padding-left: 25%;
    }
    @media (max-width: 900px) {
      padding-left: 0px;
      margin-bottom: 66px;
    }
    @media (max-width: 500px) {
      margin-bottom: 36px;
    }
  }
  .locationForm {
    padding-top: 30px;
    border-right: 1px solid #3a205d;
    position: relative;
  }
  @media (max-width: 1152px) {
    padding-left: 30px;
  }
  @media (max-width: 500px) {
    padding-left: 0px;
  }
  @media (max-width: 768px) {
    position: relative;
    width: 100%;

    .locationForm {
      position: absolute;
      left: 0;
      margin-left: 50%;
      padding-top: 60px;
      width: 100%;
      border-right: 0px solid #000;
      @media (max-width: 500px) {
        margin-left: 0%;
        padding-top: 50px;
      }
    }

    .toFromSearch {
      // display: none;
      position: absolute;
      right: 0;
      top: 0;
      margin-left: 0px;
      margin-right: 50%;
      margin-top: 50px;
      width: 100%;
    }
  }
  @media (max-width: 900px) {
    .locationForm {
      min-width: 345px;
      margin-left: 0%;
    }
    .toFromSearch {
      width: 99%;
      margin: 50px 0px 0px;
    }
    .buttonWrapper {
      width: 100% !important;
      display: flex;
      justify-content: center;
    }
  }
  @media (max-width: 375px) {
    .locationForm {
      min-width: 315px;
    }
  }
  @media (max-width: 320px) {
    .locationForm {
      min-width: 290px;
    }
    .toFromSearch {
      margin-top: 95px;
    }
  }
`;

const LandingSubTitle = styled.h1`
  color: #000;
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 200;
  // font-weight: 500;
  line-height: 18px;
  text-align: center;
  margin: 18px 0px 19px;
  @media screen and (max-width: 900px) {
    font-size: 14px;
    line-height: 30px;
    margin: 10px 5px;
  }
  @media screen and (max-width: 768px) {
    font-weight: 200;
    font-size: 12px;
    line-height: 8px;
    text-align: center;
  }
  @media screen and (max-width: 500px) {
    font-size: 12px;
    line-height: 9px;
    margin: 0px 0px 12px;
    padding: 0px 6px;
  }
`;
