/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import { Switch, Route } from 'react-router-dom';

import HomePage from 'containers/HomePage/Loadable';
import FeaturePage from 'containers/FeaturePage/Loadable';
import SearchResultPage from 'containers/SearchResultPage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
// import Header from 'components/Header';
// import Footer from 'components/Footer';

import GlobalStyle from '../../styles/global-styles';
import SchoolProfile from '../../components/SchoolProfile';
import LandingPage from '../LandingPage';
import Articles from '../Articles/index';
import Category from '../Articles/category';
import SubCategory from '../Articles/subcategory';
import ParentProfile from '../ParentProfile';
import ReadingPage from '../Articles/reading';
import Review from '../ReviewForm';
import OnlineProgram from '../OnlineProgram';
import ProgramPreview from '../OnlineProgram/preview';

const AppWrapper = styled.div`
  position: relative;
  width: 100%;
  max-width: 2560px;
  overflow: hidden;
  margin: 0 auto;
  // height: 100vh;

  // max-width: calc(768px + 16px * 2);

  // display: flex;
  // min-height: 100%;
  // padding: 0 16px;
  // flex-direction: column;
`;

export default function App() {
  return (
    <AppWrapper>
      <Helmet titleTemplate="%s - Kiddenz" defaultTitle="Kiddenz">
        <meta name="description" content="Kiddenz" />
      </Helmet>
      {/* <Header /> */}
      <Switch>
        <Route exact path="/" component={LandingPage} />
        <Route path="/locationnotfound" component={FeaturePage} />
        <Route path="/searchresult" component={SearchResultPage} />
        <Route path="/schoolprofile" component={SchoolProfile} />
        <Route path="/home" component={HomePage} />
        <Route path="/article" component={Articles} />
        <Route path="/category" component={Category} />
        <Route path="/subcategory" component={SubCategory} />
        <Route path="/parentprofile" component={ParentProfile} />
        <Route path="/review" component={Review} />

        <Route path="/reading" component={ReadingPage} />

        <Route path="" component={NotFoundPage} />
        <Route path="/online-program" component={OnlineProgram} />
        <Route
          path="/online-program/[program-name]"
          component={ProgramPreview}
        />
      </Switch>
      {/* <Footer /> */}
      <GlobalStyle />
    </AppWrapper>
  );
}
