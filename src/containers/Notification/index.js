import React from 'react';
// import PropTypes from 'prop-types';
import styled from 'styled-components';
import Header from 'components/Header';
import Footer from 'components/Footer';
// import SearchBar from 'components/SearchBar';
// import MainHeading from 'components/MainHeading';
// import SubHeading from 'components/SubHeading';
import Flex from 'components/common/flex';
// import colors from 'utils/colors';
// import Apply from 'components/ApplyCard';
import SchoolCard from '../../components/SchoolCard';
import MainWrapper from '../../components/MainWrapper';
import Notifications from '../../components/Notify';
import staffImage from '../../images/elle.png';
import notifyImage from '../../images/user4.svg';

const Notification = props => (
  <>
    <Header type="articles" searchBar placeHolderValue="Search Article" />
    <MainWrapper background="#fff">
      <ArticleSection>
        <Flex justifyBetween className="subCategory">
          <Flex column flexWidth="65%">
            <Flex justifyBetween flexWidth="100%" flexMargin="0px 0px 30px">
              <KiddenText>Notifications</KiddenText>
              <div />
            </Flex>
            <Notifications
              text="Jumbo kids"
              desc="Thank you for your interest in Indus Early learning centre, we will get back to you on your requirement. Please watch this space for more details"
              date="05/09/2019"
              notifyImage={notifyImage}
            />
            <Notifications
              text="Indus early learning centre has suggested for a new schedule"
              date="05/09/2019"
              viewBtn="View Details"
            />
            <Notifications
              text="Parent contact details are below"
              date="05/09/2019"
              name="Rohan"
              phoneNum="+91 9876895849"
            />
            <Notifications
              text="Parent contact details are below"
              date="05/09/2019"
              contactPerson="Mrs. Geeta"
              cancelText="Cancel / Reschedule"
              tourId
            />
            <Notifications
              text="Euro kids"
              desc="Thank you for your interest in Indus Early learning centre, we will get back to you on your requirement. Please watch this space for more details"
              date="01/09/2019"
              notifyImage={notifyImage}
            />
          </Flex>
          <Flex flexWidth="31%" column>
            <Flex justifyBetween flexWidth="100%" flexMargin="0px 0px 30px">
              <KiddenText>Articles</KiddenText>
              <div />
            </Flex>

            <SchoolCard
              schoolImage={staffImage}
              schoolCardHeight="62px"
              schoolCardWidth="100px"
              schoolWrapperMarTop="0px"
              mostRead="11 surprising skills children need to successfully learn to write"
              dateNum="Nov 07, 2019"
              viewNum="22"
              likesNum="12"
              cardtype="mostRead"
              photocardType="bookmark"
            />
            <SchoolCard
              schoolImage={staffImage}
              schoolCardHeight="62px"
              schoolCardWidth="100px"
              schoolWrapperMarTop="0px"
              mostRead="11 surprising skills children need to successfully learn to write"
              dateNum="Nov 07, 2019"
              viewNum="22"
              likesNum="12"
              cardtype="mostRead"
              photocardType="bookmark"
            />
            <SchoolCard
              schoolImage={staffImage}
              schoolCardHeight="62px"
              schoolCardWidth="100px"
              schoolWrapperMarTop="0px"
              mostRead="11 surprising skills children need to successfully learn to write"
              dateNum="Nov 07, 2019"
              viewNum="22"
              likesNum="12"
              cardtype="mostRead"
              photocardType="bookmark"
            />
            <SchoolCard
              schoolImage={staffImage}
              schoolCardHeight="62px"
              schoolCardWidth="100px"
              schoolWrapperMarTop="0px"
              mostRead="11 surprising skills children need to successfully learn to write"
              dateNum="Nov 07, 2019"
              viewNum="22"
              likesNum="12"
              cardtype="mostRead"
              photocardType="bookmark"
            />
            <SchoolCard
              schoolImage={staffImage}
              schoolCardHeight="62px"
              schoolCardWidth="100px"
              schoolWrapperMarTop="0px"
              mostRead="11 surprising skills children need to successfully learn to write"
              dateNum="Nov 07, 2019"
              viewNum="22"
              likesNum="12"
              cardtype="mostRead"
              photocardType="bookmark"
            />
            <SchoolCard
              schoolImage={staffImage}
              schoolCardHeight="62px"
              schoolCardWidth="100px"
              schoolWrapperMarTop="0px"
              mostRead="11 surprising skills children need to successfully learn to write"
              dateNum="Nov 07, 2019"
              viewNum="22"
              likesNum="12"
              cardtype="mostRead"
              photocardType="bookmark"
            />
            <SchoolCard
              schoolImage={staffImage}
              schoolCardHeight="62px"
              schoolCardWidth="100px"
              schoolWrapperMarTop="0px"
              mostRead="11 surprising skills children need to successfully learn to write"
              dateNum="Nov 07, 2019"
              viewNum="22"
              likesNum="12"
              cardtype="mostRead"
              photocardType="bookmark"
            />
            <SchoolCard
              schoolImage={staffImage}
              schoolCardHeight="62px"
              schoolCardWidth="100px"
              schoolWrapperMarTop="0px"
              mostRead="11 surprising skills children need to successfully learn to write"
              dateNum="Nov 07, 2019"
              viewNum="22"
              likesNum="12"
              cardtype="mostRead"
              photocardType="bookmark"
            />
          </Flex>
        </Flex>
      </ArticleSection>
    </MainWrapper>

    <Footer {...props} />
  </>
);

export default Notification;

const ArticleSection = styled.div`
  padding: 100px 15px;
  margin: 0px auto;
  width: 100%;
  max-width: 1170px;
  .categories {
    padding: 10px 20px;
  }
  .subCategory {
    @media screen and (max-width: 500px) {
      flex-direction: column;
    }
    & > div {
      @media screen and (max-width: 500px) {
        width: 100%;
      }
    }
  }
  .moreCategories {
    @media screen and (max-width: 500px) {
      margin-bottom: 30px;
    }
  }
  @media screen and (max-width: 500px) {
    padding: 30px 15px 50px;
  }
`;

const KiddenText = styled.div`
  font-family: 'Quicksand', sans-serif;
  color: #30333b;
  font-size: 24px;
  font-weight: bold;
  line-height: 30px;
  text-align: center;
  @media screen and (max-width: 900px) {
    font-size: 20px;
  }
`;
