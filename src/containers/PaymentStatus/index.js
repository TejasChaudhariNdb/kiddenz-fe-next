/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import moment from 'moment';
import Router from 'next/router';
import Header from 'components/Header';
import Footer from 'components/Footer';
import {
  useOnlineProgramPaymentHook,
  usePurchasedOnlineProgramHook,
} from 'shared/hooks';
import Button from 'components/Button';
// import SearchBar from 'components/SearchBar';
import { Mainheading } from 'components/MainHeading/styled';

import kiddenLogo from '../../images/kiddenz-purple logo.svg';
import { PaymentSuccessWrapper, Logo } from './styled';

const paymentStatusConfig = {
  '1': {
    title: 'Transaction Pending',
    subTitle: 'Your transaction has been pending with Kiddenz',
    buttonTitle: 'Try Again',
    isBuy: true,
  },
  '2': {
    title: 'Transaction Cancelled',
    subTitle: 'Your transaction has been cancelled with Kiddenz',
    buttonTitle: 'Try Again',
    isBuy: true,
  },
  '3': {
    title: 'Congrats!',
    subTitle: 'Your transaction has been completed with Kiddenz',
    buttonTitle: 'Go to Homepage',
    isBuy: false,
  },
  '4': {
    title: 'Transaction Failed',
    subTitle: 'Your transaction has been failed with Kiddenz',
    buttonTitle: 'Try Again',
    isBuy: true,
  },
};
const PaymentStatus = props => {
  const {
    authentication: {
      // isLoggedIn,
      profile: {
        data: { name = '', mobile_number: contact = '', email = '' } = {},
      } = {},
    },
    query: { payment_id: orderId } = {},
    successToast,
    errorToast,
  } = props;

  const {
    paymentStatus: {
      data: paymentStatus,
      // loader: paymentStatusLoader,
    },
  } = useOnlineProgramPaymentHook(props, { orderId });

  const { getPurchasedPrograms } = usePurchasedOnlineProgramHook(props);

  const handleClick = () => {
    if (paymentStatus.payment_status === '3') {
      Router.replace({
        pathname: '/',
      }).then(() => window.scrollTo(0, 0));
    } else {
      const options = {
        key: 'rzp_test_jFHt0q9YaPyixx',
        name: 'Kiddenz',
        description: 'Online Program',
        image:
          'https://kiddenz-media-prod.s3.ap-south-1.amazonaws.com/kiddenz-purple+logo-6a687d0564d5c69c121bbe02a8b30117.svg',
        order_id: orderId,
        // eslint-disable-next-line no-unused-vars
        handler: async response => {
          try {
            successToast('Payment successfully completed');
            getPurchasedPrograms();
            Router.replace({
              pathname: `/payment-status`,
              query: { payment_id: orderId },
            });
            // const paymentId = response.razorpay_payment_id;
            // const url = `${API_URL}capture/${paymentId}`;
            // const captureResponse = await Axios.post(url, {});
            // console.log(captureResponse.data);
          } catch (err) {
            errorToast('Something went wrong');
            console.log(err, 'razor');
          }
        },
        prefill: {
          name,
          email,
          contact,
        },
        notes: {
          address: 'Razorpay Corporate Office',
        },
        theme: {
          color: '#3399cc',
        },
      };
      const rzp1 = new window.Razorpay(options);
      rzp1.open();
    }
  };
  return (
    <>
      <Header type="articles" placeHolderValue="Search Article" />
      <PaymentSuccessWrapper>
        <Logo
          onClick={() =>
            Router.replace({
              pathname: '/',
            }).then(() => window.scrollTo(0, 0))
          }
        >
          <img src={kiddenLogo} alt="Kiddenz logo" />
        </Logo>
        {paymentStatus.payment_status && (
          <div className="success">
            {paymentStatus.payment_status === '3' ? (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="16"
                height="16"
                fill="currentColor"
                className="bi bi-check-circle"
                viewBox="0 0 16 16"
              >
                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                <path d="M10.97 4.97a.235.235 0 0 0-.02.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-1.071-1.05z" />
              </svg>
            ) : (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="16"
                height="16"
                fill="currentColor"
                className="bi bi-x-circle   red"
                viewBox="0 0 16 16"
              >
                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
              </svg>
            )}

            <h1>{paymentStatusConfig[paymentStatus.payment_status].title}</h1>
            <p>{paymentStatusConfig[paymentStatus.payment_status].subTitle}</p>
            <div className="details reddetails">
              <div className="left">
                <p>Transaction id: {paymentStatus.payment_id}</p>
                <p>Mode of payment: {paymentStatus.payment_method}</p>
              </div>
              <div className="right">
                <p>Amount paid: Rs {paymentStatus.amount}</p>
                <p>
                  Time:{' '}
                  {moment(paymentStatus.created_on).format(
                    'MMM DD, YYYY h:mm a',
                  )}
                </p>
              </div>
            </div>

            <Button
              text={
                paymentStatusConfig[paymentStatus.payment_status].buttonTitle
              }
              onClick={handleClick}
            />
          </div>
        )}
      </PaymentSuccessWrapper>
      <Footer {...props} />
    </>
  );
};
PaymentStatus.propTypes = {
  query: PropTypes.object,
  authentication: PropTypes.object,
  successToast: PropTypes.func,
  errorToast: PropTypes.func,
};
export default PaymentStatus;

export const CareerSection = styled.div`
  position: relative;
  margin-top: 106px;
  display: flex;
  justify-content: flex-end;
  max-width: 1290px;
  margin-left: auto;
  @media (max-width: 500px) {
    margin-top: 70px;
  }
  .careerDetails {
    position: absolute;
    left: 20px;
    @media (max-width: 900px) {
      padding-top: 15px;
      width: 50%;
    }
    @media (max-width: 500px) {
      width: 90%;
      padding-top: 0px;
    }
  }
  ${Mainheading} {
    @media (max-width: 900px) {
      font-size: 24px;
    }
    @media (max-width: 500px) {
      font-size: 20px;
    }
  }
`;

export const FAQSection = styled.div`
  max-width: 1180px;
  margin: 0px auto;
  padding: 50px 20px 100px;
  //   border-top: 1px solid rgba(230, 232, 236, 1);
  ${Mainheading} {
    @media (max-width: 500px) {
      font-size: 20px;
      line-height: 25px;
      margin-bottom: 10px;
    }
  }
  .support {
    @media (max-width: 900px) {
      width: 40%;
    }
    @media (max-width: 500px) {
      width: 100%;
    }
  }
  .queries {
    @media (max-width: 900px) {
      width: 58%;
    }
    @media (max-width: 500px) {
      width: 100%;
    }
  }
  .applySection {
    display: flex;
    width: 100%;
    flex-flow: row wrap;
    justify-content: space-between;
  }

  .applySection::after {
    content: '';
    width: 24%;
    margin-left: 20px;
    @media (max-width: 900px) {
      width: 32%;
    }
  }
  .questions-section {
    @media (max-width: 500px) {
      flex-direction: column;
    }
    & > div {
      @media (max-width: 500px) {
        width: 100%;
      }
    }
  }
  .questions-list {
    @media (max-width: 500px) {
      margin-bottom: 20px;
      padding-bottom: 30px;
    }
  }
`;
export const CareerBannerImage = styled.div`
  width: 50%;
  height: 435px;
  @media (max-width: 900px) {
    height: 300px;
  }
  @media (max-width: 500px) {
    width: 100%;
    height: 230px;
    margin-top: 200px;
    margin-bottom: 70px;
  }
  img {
    width: 100%;
    height: 100%;
  }
`;
