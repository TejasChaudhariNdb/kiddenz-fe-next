import styled from 'styled-components';

export const PaymentSuccessWrapper = styled.div`
  font-family: Quicksand;
  text-align: center;
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  background: #fff;
  z-index: 999999;
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
  svg {
    fill: #60b947;
    width: 120px;
    height: 120px;
    margin-bottom: 30px;
    @media screen and (max-width: 768px) {
      width: 70px;
      height: 70px;
      margin-bottom: 10px;
    }
  }
  svg.red {
    fill: #fb4e4e;
  }
  h1 {
    margin: 0;
    font-weight: 600;
    font-size: 33px;
    @media screen and (max-width: 768px) {
      font-size: 22px;
    }
  }
  p {
    font-family: Quicksand;
    margin: 0;
  }
  .details {
    display: flex;
    align-items: center;
    justify-content: space-around;
    width: 700px;
    text-align: left;
    margin: 30px auto 0;
    background: #60b94712;
    padding: 30px 30px 20px 30px;
    border-radius: 20px;
    @media screen and (max-width: 768px) {
      margin: 20px auto 0;
      width: 90%;
      padding: 20px;
      font-size: 14px;
      flex-direction: column;
      align-items: center;
      justify-content: flex-start;
      text-align: center;
    }
    .left {
      margin-right: 20px;
      @media screen and (max-width: 768px) {
        margin: 0;
      }
    }
    p {
      margin: 0 0 10px;
    }
  }
  .reddetails {
    background: #fb4e4e1f;
  }
  button {
    margin: 50px auto 0;
    font-size: 24px;
    @media screen and (max-width: 768px) {
      margin: 30px auto 0;
      font-size: 15px;
      height: auto;
      padding: 10px 30px;
    }
  }
  .hide {
    display: none;
  }
`;
export const Logo = styled.div`
  // height: 34px;
  // width: 136px;
  cursor: pointer;
  position: absolute;
  top: 20px;
  left: 20px;
  @media (max-width: 995px) {
    width: 118px;
  }
  @media (max-width: 500px) {
    height: auto;
    width: 80px;
  }
  img {
    width: 100%;
    height: 100%;
    @media (max-width: 500px) {
      transform: scale(1.2);
    }
  }
`;
