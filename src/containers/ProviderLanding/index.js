/* eslint-disable react/prop-types */
// eslint-disable-next-line react/no-unescaped-entities
import React, { useState } from 'react';
// import Slider from 'react-slick';
import styled from 'styled-components';
import { Mainheading } from 'components/MainHeading/styled';
import Header from '../../components/Header';
import MainWrapper from '../../components/MainWrapper';
import Button from '../../components/Button';
import background from '../../images/providerBanner.png';
// import background1 from '../../images/bg.svg';
import shapeImage from '../../images/pattern1.svg';
import register1 from '../../images/register school.svg';
import register2 from '../../images/Submit the form.svg';
import register3 from '../../images/Kiddenz team connect-1.svg';
import register4 from '../../images/Start connecting with parents.svg';
// import { useProviderHook } from '../../shared/hooks';

import arrow1 from '../../images/step1.svg';
import arrow2 from '../../images/step2.svg';
import arrow3 from '../../images/step3.svg';

import reg1 from '../../images/step1 (1).svg';
import reg2 from '../../images/step2 (1).svg';
import reg3 from '../../images/step1-1.svg';

import Flex from '../../components/common/flex';
import MainHeading from '../../components/MainHeading';
import KiddenOffer from '../../components/KiddenOffer';

import displayImage from '../../images/display school.svg';
import leadImage from '../../images/recieve leads.svg';
import dedicateImage from '../../images/dedicated dashboard-1.svg';

import Footer from '../../components/Footer';
// import Testimonial from '../../components/TestimonialCard/index';

// const settings = {
//   dots: true,
//   infinite: true,
//   speed: 900,
//   autoplay: true,
//   autoplaySpeed: 4000,
//   slidesToShow: 1,
//   slidesToScroll: 1,
// };

const ProviderLanding = props => {
  const [modalStatus, setModalStatus] = useState(false);
  const [modalType, setModalType] = useState('provider');
  // const { authentication: { isLoggedIn } = {} } = props;
  // const onCardClickHandler = () =>
  //   Router.push({
  //     pathname: '/blog/[article-name]',
  //   }).then(() => window.scrollTo(0, 0));

  // const { onSubmit } = useProviderHook(props);
  // eslint-disable-next-line no-console
  // console.log(onSubmit);

  return (
    <>
      <Header
        {...props}
        type="articles"
        searchBar={false}
        isModalActive={modalStatus}
        activeModalType={modalType}
        setActiveCallback={setModalStatus}
      />
      <MainWrapper background="#fff" paddingTop="0px">
        <MainSection>
          <img className="patternImage" src={shapeImage} alt="" />
          <div className="providerBanner">
            <img src={background} alt="" />
          </div>

          <Flex
            column
            flexMargin="62px 60px 0px auto"
            flexWidth="30%"
            maxWidth="700px"
            className="providerTitle"
          >
            <ProvidingTitle>Join the</ProvidingTitle>
            <LandingTitle>growing community</LandingTitle>
            <ProvidingTitle>
              of Preschools/ Daycare providers trusted and loved by Parents.
            </ProvidingTitle>
            <Button
              marginTop="27px"
              text="Submit your details"
              fitContent="fit-content"
              type="preCare ripple"
              marginRight="10px"
              onClick={() => {
                setModalType('provider');
                setModalStatus(true);
              }}
            />
          </Flex>
        </MainSection>
        <OfferSection>
          <Flex column alignCenter>
            <MainHeading
              fontSize="26px"
              text="Use Kiddenz to grow Your Business"
            />
            <Flex
              className="kiddenoffer-section"
              flexWidth="100%"
              justifyBetween
              flexPadding="0px 0px 60px"
            >
              <KiddenOffer
                image={displayImage}
                width="30%"
                text="Display"
                subText=" your Programs"
                desc="Get free visibility, showcase your profile, program details, photos, reviews and more."
              />
              <KiddenOffer
                image={leadImage}
                width="30%"
                text="Receive"
                subText=" registrations and admissions"
                desc="Respond to price requests, track inquires &amp; schedule tours and connect with parents."
              />
              <KiddenOffer
                image={dedicateImage}
                width="30%"
                text="Dedicated Dashboard"
                desc="Respond to reviews, advertise offers, manage your page and promote your program."
              />
            </Flex>
          </Flex>
        </OfferSection>
        {/* <KiddenPartner>
          <Testimonials>
            <img className="patternImage" src={shapeImage} alt="" />
            <div className="providerBanner">
              <img src={background1} alt="" />
            </div>
            <MainHeading
              text="Testimonials"
              fontSize="26px"
              margin="0px 0px 30px 0px"
            />
            <Flex flexWidth="100%" column>
              <Slider {...settings}>
                <Testimonial
                  type="primary"
                  name="Shruti Gupta"
                  job="Lead Engineer, IBM"
                  comment="“Maria and her team are great, super loving. I love that they are always teaching her something new.”"
                />
                <Testimonial
                  type="primary"
                  name="Shruti Gupta"
                  job="Lead Engineer, IBM"
                  comment="“Maria and her team are great, super loving. I love that they are always teaching her something new.”"
                />
                <Testimonial
                  type="primary"
                  name="Shruti Gupta"
                  job="Lead Engineer, IBM"
                  comment="“Maria and her team are great, super loving. I love that they are always teaching her something new.”"
                />
                <Testimonial
                  type="primary"
                  name="Shruti Gupta"
                  job="Lead Engineer, IBM"
                  comment="“Maria and her team are great, super loving. I love that they are always teaching her something new.”"
                />
                <Testimonial
                  type="primary"
                  name="Shruti Gupta"
                  job="Lead Engineer, IBM"
                  comment="“Maria and her team are great, super loving. I love that they are always teaching her something new.”"
                />
              </Slider>
            </Flex>
          </Testimonials>
        </KiddenPartner> */}

        <HowStarted>
          <MainHeading
            text="How to get started?"
            fontSize="26px"
            margin="0px 0px 8px 0px"
          />
          <SubInfo>
            Lets make parenting easy in <span>four</span> simple steps
          </SubInfo>
          <Flex
            justifyBetween
            flexWidth="100%"
            flexMargin="70px 0px 0px"
            flexPadding="0px 20px"
            className="howStarted"
          >
            <RegisterCard>
              <div className="registerImage">
                <img src={register1} alt="" />
              </div>
              <Flex flexMargin="10px 0px 0px" flexWidth="100%" alignCenter>
                <h2>1.</h2>
                <div className="registerQuestion">
                  Click on <br />
                  &#34;Register your School&#34;
                </div>
              </Flex>
              <img className="registerArrow" src={arrow1} alt="" />
              <img className="registerArrow-mobile" src={reg1} alt="" />
            </RegisterCard>
            <RegisterCard>
              <div className="registerImage">
                <img src={register2} alt="" />
              </div>
              <Flex flexMargin="10px 0px 0px" flexWidth="100%" alignCenter>
                <h2>2.</h2>
                <div className="registerQuestion">Submit your details </div>
              </Flex>
              <img className="registerArrow two" src={arrow2} alt="" />
              <img className="registerArrow-mobile two" src={reg2} alt="" />
            </RegisterCard>
            <RegisterCard>
              <div className="registerImage">
                <img src={register3} alt="" />
              </div>
              <Flex flexMargin="10px 0px 0px" flexWidth="100%" alignCenter>
                <h2>3.</h2>
                <div className="registerQuestion">
                  Kiddenz team connects to list your program
                </div>
              </Flex>
              <img className="registerArrow" src={arrow3} alt="" />
              <img className="registerArrow-mobile" src={reg3} alt="" />
            </RegisterCard>
            <RegisterCard>
              <div className="registerImage">
                <img src={register4} alt="" />
              </div>
              <Flex flexMargin="10px 0px 0px" flexWidth="100%" alignCenter>
                <h2>4.</h2>
                <div className="registerQuestion">
                  Start connecting with parents
                </div>
              </Flex>
            </RegisterCard>
          </Flex>
        </HowStarted>
        <Flex flexWidth="100%" justifyCenter flexMargin="0px 0px 0 0px">
          <Button
            text="Submit your details"
            type="mobile"
            onClick={() => {
              setModalType('provider');
              setModalStatus(true);
            }}
            // onClick={() => onWishlistLoadMore(wishList.length)}
            // isLoading={wishlistLoader}
          />
        </Flex>
        <Contactus id="contactUs">
          <MainHeading
            text="Contact us"
            fontSize="26px"
            margin="0px 0px 8px 0px"
          />
          <Flex className="contactUs" justifyCenter flexMargin="20px 0px 0px">
            <div className="contactDetails">
              <span>
                Please dial our customer care number for any kind of assistance
              </span>
              <h2>+91 8088340733</h2>
            </div>
            <div className="contactDetails">
              <span>Write your queries and questions at</span>
              <h2>
                <EmailAction href="mailto:hello@kiddenz.com">
                  hello@kiddenz.com
                </EmailAction>
              </h2>
            </div>
          </Flex>
        </Contactus>
      </MainWrapper>
      <Footer {...props} />
    </>
  );
};

export default ProviderLanding;

const MainSection = styled.div`
  position: relative;
  height: 481px;
  margin-top: 75px;
  margin-bottom: 100px;
  @media (max-width: 1200px) {
    height: 420px;
  }
  @media (max-width: 768px) {
    height: 300px;
    margin-top: 12px;
    margin-bottom: 50px;
  }
  @media (max-width: 500px) {
    margin-top: 20px;
    margin-bottom: 20px;
  }
  .providerBanner {
    position: absolute;
    left: 0px;
    top: 0px;
    bottom: 0px;
    right: 0px;
    @media screen and (max-width: 500px) {
      // display: flex;
      // align-items: flex-end;
    }
    img {
      width: 76%;
      height: 100%;
      @media (max-width: 1200px) {
        width: 67%;
      }
      @media (max-width: 900px) {
        width: 60%;
      }
      @media screen and (max-width: 500px) {
        width: 87%;
        height: 42%;
      }
    }
  }
  .patternImage {
    position: absolute;
    right: 40%;
    @media screen and (max-width: 425px) {
      right: 10%;
      // bottom: 15%;
    }
  }
  @media screen and (max-width: 867px) {
    height: 700px;
  }
  @media screen and (max-width: 767px) {
    height: 600px;
  }
  @media screen and (max-width: 425px) {
    height: 450px;
  }
  .landingTitle {
    @media screen and (max-width: 767px) {
      margin: 100px auto 0px;
    }
  }
  .providerTitle {
    @media (max-width: 1200px) {
      width: 41%;
      margin: 64px 16px 0px auto;
    }
    @media (max-width: 900px) {
      width: 37%;
      margin-top: 30px;
      margin-left: auto;
    }
    @media (max-width: 500px) {
      width: 100%;
      align-items: center;
      position: absolute;
      top: 41%;
      padding: 0px 15px;
    }
    .preCare {
      width: fit-content;
      background-color: #60b947;
      border-color: #60b947;
      @media (max-width: 900px) {
      }
    }
  }
  .liteFont {
    font-weight: 300;
  }
`;

const LandingTitle = styled.div`
  color: #613a95;
  font-family: 'Quicksand', sans-serif;
  font-size: 28px;
  letter-spacing: 0;
  font-weight: 700;
  text-transform: Uppercase;
  margin: 10px 0px;
  line-height: 35px;
  @media screen and (max-width: 1200px) {
    font-size: 26px;
    line-height: 32px;
  }
  @media screen and (max-width: 900px) {
    font-size: 18px;
    line-height: 30px;
    margin: 5px 0px;
  }
`;
const ProvidingTitle = styled.div`
  font-family: 'Quicksand', sans-serif;
  color: #555457;
  font-size: 24px;
  font-weight: bold;
  letter-spacing: 0;
  line-height: 42px;
  @media screen and (max-width: 1200px) {
    font-size: 22px;
    line-height: 30px;
  }
  @media screen and (max-width: 900px) {
    font-size: 18px;
    line-height: 30px;
  }
  @media screen and (max-width: 500px) {
    font-size: 16px;
    line-height: 26px;
    text-align: center;
  }
`;

const OfferSection = styled.div`
  margin: 0px auto;
  padding: 0px 30px;
  width: 100%;
  max-width: 1200px;
  ${Mainheading} {
    font-size: 20px;
    line-height: 26px;
    text-align: center;
  }

  .kiddenoffer-section {
    @media screen and (max-width: 900px) {
      padding: 0px 0px 70px;
    }
    @media screen and (max-width: 767px) {
      flex-direction: column;
      padding: 0px 0px 50px;
      align-items: center;
    }
    span.liteFont {
      font-weight: 300;
    }
  }
`;

// const Testimonials = styled.div`
//   width: 100%;
//   max-width: 830px;
//   display: flex;
//   padding: 30px 30px 50px;
//   margin: 0px auto;
//   flex-direction: column;
//   align-items: center;
//   // .slick-slide
//   // {
//   //   margin:20px;
//   // }
//   @media screen and (max-width: 767px) {
//     padding: 30px 30px 70px;
//   }
//   @media screen and (max-width: 500px) {
//     padding: 30px 0px 70px;
//   }
//   .slick-dots {
//     bottom: -20px;
//   }
//   .slick-dots li {
//     width: 20px;
//     margin: 0px;
//   }
//   .slick-dots li button:before {
//     opacity: 1;
//     font-size: 22px;
//     color: #d6dae2;
//   }
//   .slick-dots li.slick-active button:before {
//     opacity: 1;
//     color: #613a95;
//   }
// `;

const HowStarted = styled.div`
  width: 100%;
  max-width: 1170px;
  display: flex;
  padding: 30px 0px 120px;
  margin: 0px auto;
  flex-direction: column;
  align-items: center;
  .howStarted {
    @media screen and (max-width: 500px) {
      flex-direction: column;
      align-items: center;
      margin-top: 30px;
    }
  }
  .interestContainer {
    @media screen and (max-width: 765px) {
      flex-direction: column;
      align-items: center;
    }
  }
  ${Mainheading} {
    @media screen and (max-width: 765px) {
      font-size: 20px;
      text-align: center;
      margin-bottom: 0px;
    }
  }
  @media screen and (max-width: 500px) {
    padding-bottom: 20px;
  }
`;
const SubInfo = styled.div`
  font-family: Quicksand;
  font-size: 22px;
  font-weight: 500;
  letter-spacing: 0;
  line-height: 24px;
  margin-top: 20px;

  span {
    color: #60b947;
  }
  @media screen and (max-width: 500px) {
    font-size: 16px;
    line-height: 24px;
    text-align: center;
    margin-top: 10px;
  }
`;

// const KiddenPartner = styled.div`
//   margin: 40px 0px 60px;
//   position: relative;
//   .providerBanner {
//     position: absolute;
//     left: 0px;
//     top: 0px;
//     bottom: 0px;
//     right: 0px;

//     img {
//       height: 100%;
//       float: right;
//     }
//   }
//   .patternImage {
//     position: absolute;
//     left: 14%;
//     bottom: 0px;
//   }
// `;

const RegisterCard = styled.div`
position:relative
width: 22%;
    display: flex;
    flex-direction: column;
    align-items: center;
    @media screen and (max-width: 500px) {
      width: 60%;
      // margin-bottom:40px;
    }
.registerImage
{
img
{
  @media screen and (max-width: 1200px) {
   transform:scale(0.8);
  }
}
}
h2 {
  width:20%;
  color: #666C78;
  font-family: Quicksand;
  font-size: 52px;
  font-weight: 500;
  letter-spacing: 0;
  line-height: 1;
margin:0px;
@media screen and (max-width: 1200px) {
  font-size: 40px;
}
}

.registerQuestion{
  width:80%;
  color: #666C78;
  font-family: Quicksand;
  font-size: 18px;
  font-weight: 500;
  letter-spacing: 0;
  line-height: 24px;
  @media screen and (max-width: 1200px) {
    font-size: 15px;
  }
}
.registerArrow
{
  position:absolute;
  top: 100%;
    left: 80%;
    &.two
    {
      top:-20px;
      left:87%;
    }
    &-mobile
    {
      display:none;
      position:absolute;
      top: 62px;
      right: -40px;
      &.two
      {
        right:106%;
        top:90%;
      }
      @media screen and (max-width: 500px) {
        display:block;
      }

    }
    @media screen and (max-width: 500px) {
      display:none;
    }
}
`;
const Contactus = styled.div`
  width: 100%;
  max-width: 1170px;
  display: flex;
  padding: 100px 0px 120px;
  margin: 0px auto;
  flex-direction: column;
  align-items: center;
  .contactUs {
    @media screen and (max-width: 500px) {
      flex-direction: column;
      width: 90%;
      margin: 0px auto;
      padding-top: 50px;
    }
  }
  .contactDetails {
    width: 37%;
    background-color: #f2eff5;
    padding: 35px 33px;
    border-radius: 20px;
    margin-right: 20px;
    @media screen and (max-width: 500px) {
      width: 100%;
      padding: 24px 20px;
      margin-bottom: 20px;
    }
    &:last-child {
      margin-right: 0px;
    }
    span {
      color: #666c78;
      font-family: Quicksand;
      font-size: 20px;
      font-weight: 500;
      letter-spacing: 0;
      line-height: 28px;
      @media screen and (max-width: 500px) {
        font-size: 14px;
        line-height: 20px;
      }
    }
    h2 {
      color: #55357f;
      font-family: Quicksand;
      font-size: 26px;
      letter-spacing: 0;
      line-height: 33px;
      margin: 10px 0px 0px;
      @media screen and (max-width: 500px) {
        font-size: 16px;
        line-height: 22px;
      }
    }
  }
`;

const EmailAction = styled.a`
  color: inherit;
  text-decoration: none;
`;
