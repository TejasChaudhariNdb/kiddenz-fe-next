/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable no-plusplus */
/* eslint-disable indent */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable react/prop-types */
import React, { useState, useRef } from 'react';
import InputRange from 'react-input-range';
import styled from 'styled-components';
import InfiniteScroll from 'react-infinite-scroll-component';
import Header from 'components/Header';
import Footer from 'components/Footer';
import MascotLoader from 'components/common/MascotLoader';
// import Router from 'next/router';
import Geocode from 'react-geocode';
import {
  useOnlineProgramSearchHook,
  useOnlineProgramCategoryHook,
  useOnlineProgramCityHook,
  useBookmarkProgramHook,
} from 'shared/hooks';
import Modal from 'components/Modal';
import Refresh from '../../components/RefreshFilter';
import CheckBox from '../../components/Checkbox';
import ClassCard from '../../components/ClassCard';
import Flex from '../../components/common/flex';
import pulseLoader from '../../images/pulse_loader.gif';
import DropDown from './dropdowns';
import AgeDropdown from './AgeDropdown';
// import Skeleton from 'react-loading-skeleton';
// import RadioButton, { Radio } from '../../components/RadioButton';
// import Button from '../../components/Button';
// import MainHeading from '../../components/MainHeading';
// import Flex from '../SchoolProfile/flex';
// import InputField from '../../../components/InputField/index';

const PROGRAM_LIMIT = 9;

const SchoolTypeConfig = [
  {
    label: 'Online',
    key: 'online',
  },
  {
    label: 'Teacher at home',
    key: 'offline',
  },
  // {
  //   label: 'Both',
  //   key: 'both',
  // },
];

const CourseTypeConfig = [
  {
    label: 'Full Course',
    key: 'Full_Course',
  },
  {
    label: 'Single Session',
    key: 'Single_Session',
  },
  {
    label: 'Monthly',
    key: 'Monthly',
  },
  // {
  //   label: 'Trial Class',
  //   key: 'Trial_Class',
  // },
];

// const ProgramTypeConfig = [
//   {
//     label: 'All',
//     key: 'all',
//   },
//   {
//     label: 'One To One',
//     key: 'one_to_one',
//   },
//   {
//     label: 'Small Batch',
//     key: 'small_batch',
//   },
//   {
//     label: 'Homeschooling',
//     key: 'home_school',
//   },
// ];

const Accordion = ({ title, className, children, isMobile = false }) => {
  const [isOpen, setOpen] = useState(!isMobile);
  return (
    <div className="filterItem">
      <h2 className="filterItem-heading" onClick={() => setOpen(!isOpen)}>
        {title}{' '}
        <div className={isOpen ? 'hide-icon' : ''}>
          <span
            className="iconify"
            data-icon="il:small-arrow-down"
            data-inline="false"
          />
        </div>
        <div className={!isOpen ? 'hide-icon' : ''}>
          <span
            className="iconify"
            data-icon="il:small-arrow-up"
            data-inline="false"
          />
        </div>
      </h2>

      <div className={`${className} ${!isOpen ? 'hide' : ''}`}>{children}</div>
    </div>
  );
};

const OnlineProgram = props => {
  const infiniteRef = useRef();
  const {
    authentication: { isLoggedIn },
    successToast,
    errorToast,
  } = props;

  const [locationLoader, setLocationLoader] = useState(false);
  const [modalType, setModalType] = useState('login');
  const [modalStatus, setModalStatus] = useState(false);
  const [filterSectionActive, setFilterSectionActive] = useState(false);

  const {
    search,
    city,
    categories,
    programs,
    courses,
    isCertificate,
    isMoneyback,
    school,
    startPrice,
    endPrice,
    startAge,
    endAge,
    isTrial,
    isDiscount,
  } = props.router.query;

  const {
    onSearchLoadMore,
    searchResults: { data: searchData = [], loader: searchLoader },
    showLoadMore,
    showLoader,
    searchKey,
    // programType,
    courseType,
    schoolType,
    certificate,
    discount,
    trialClass,
    category,
    moneyBack,
    priceRange: {
      maxPrice,
      loader: maxPriceLoader,
      range,
      onChangePrice,
      onSliderComplete,
      // onClearPriceRange,
    },
    // ageRange: {
    //   // maxPrice,
    //   // loader: maxPriceLoader,
    //   ageRange,
    //   // onChangeAge,
    //   // onAgeSliderComplete,
    // },
    ageYear,
    // onClearAge,
    // ageMonth,
    selectedCity,
    isCitySelected,
    toggleLocationModal,
    onClearFilter,
    onClearIndividualFilter,
    onApplyFilter,
    // closeHandler,
    isCostChanged,
    // isAgeChanged,
    // isFilterApplied,
  } = useOnlineProgramSearchHook(props, {
    onSuccess,
    onError,
    search,
    city,
    categories,
    programs,
    courses,
    isCertificate,
    isMoneyback,
    school,
    startPrice,
    endPrice,
    startAge,
    endAge,
    isTrial,
    isDiscount,
  });

  const {
    // bookmarkedPrograms: {
    //   data: wishlistedPrograms = [],
    //   loader: wishlistLoader,
    // },
    bookmarkedItems,
    addBookmark,
    removeBookmark,
  } = useBookmarkProgramHook(props, {
    onBookmarkDelSuccess,
    onBookmarkAddSuccess,
    onBookmarkDelError,
    onBookmarkAddError,
  });

  const onBookmarkAddSuccess = ({ message }) => successToast(message);
  const onBookmarkDelSuccess = ({ message }) => successToast(message);
  const onBookmarkDelError = ({ message }) => errorToast(message);
  const onBookmarkAddError = ({ message }) => errorToast(message);

  const {
    category: { searchKey: catSearchKey, onSearch: onSearchCategory },
    categoryList: { data: categoryList = [], loader: categoryLoader },
    onClearCategorySearch,
  } = useOnlineProgramCategoryHook(props);

  const {
    cities: { searchKey: citySearchKey, onSearch: onSearchCity },
    cityList: { data: cityList = [], loader: cityLoader },
  } = useOnlineProgramCityHook(props);

  const onSuccess = () => successToast('Successfully Submited.');

  const onError = ({ message }) => errorToast(message);

  const handleBookMark = id => {
    if (!isLoggedIn) {
      setModalType('login');
      setModalStatus(true);
      // setModalActive(true);
    } else {
      addBookmark(id);
    }
  };

  const handleDeleteBookMark = id => {
    removeBookmark(id);
  };

  // const handleCardClick = (program, e) => {
  //   e.preventDefault();
  //   if (program.id) {
  //     const query = { id: program.id };
  //     const url = { pathname: '/online-program/[program-name]', query };
  //     const urlAs = {
  //       pathname: `/online-program/${program.name
  //         .replace(/\s+/g, '-')
  //         .replace('/', '-')
  //         .toLowerCase()}`,
  //       query,
  //     };
  //     Router.push(url, urlAs).then(() => window.scrollTo(0, 0));
  //   }
  // };

  const getCityName = locName => {
    let currntCity;
    let isLevel2Available = false;
    for (let i = 0; i < locName.address_components.length; i++) {
      for (let j = 0; j < locName.address_components[i].types.length; j++) {
        switch (locName.address_components[i].types[j]) {
          case 'locality':
            currntCity = locName.address_components[i].long_name;
            selectedCity.onSelect(currntCity);
            isLevel2Available = true;
            toggleLocationModal();
            break;
          case 'administrative_area_level_2':
            if (!isLevel2Available) {
              currntCity = locName.address_components[i].long_name;
              selectedCity.onSelect(currntCity);
              isLevel2Available = true;
              toggleLocationModal();
            }
            break;
          case 'administrative_area_level_1':
            if (!isLevel2Available) {
              currntCity = locName.address_components[i].long_name;
              selectedCity.onSelect(currntCity);
              toggleLocationModal();
            }
            break;
          default:
        }
        setLocationLoader(false);
      }
    }
  };

  const getLocation = () => {
    setLocationLoader(true);
    const options = {
      enableHighAccuracy: true,
      // timeout: 5000,
      // maximumAge: 0,
    };
    navigator.geolocation.getCurrentPosition(
      ({ coords: { latitude: lati, longitude: lngi } = {} }) => {
        // setLatitude(lati);
        // setLongitude(lngi);
        Geocode.fromLatLng(String(lati), String(lngi)).then(
          ({ results = [] }) => {
            if (results[0]) {
              const locName = results[0];
              getCityName(locName);
              // const address = results[0].formatted_address;
              // .formatted_address;
              // setLocationString(locName);
              // dispatch({
              //   type: 'CURRENT_LOCATION_UPATE',
              //   payload: { name, lat: lati, lng: lngi },
              // });
              // }
            }
          },
        );
      },
      err => {
        setLocationLoader(false);
        // if (err.code === 1) {
        //   errorToast(
        //     'Kindly enable location permission to get your current location',
        //   );
        // } else {
        errorToast(
          err.message ||
            'Location access is blocked. Change your location settings in browser or select location manually',
        );
        // }
      },
      options,
    );
  };

  const geoLocationActions = result => {
    if (result === 'granted' || result === 'prompt') {
      getLocation();
    } else if (result === 'denied') {
      errorToast(
        'Kindly enable location permission to get your current location',
      );
    }
  };

  const handlePermission = () => {
    navigator.permissions
      .query({
        name: 'geolocation',
      })
      .then(result => {
        if (result.state === 'granted') {
          geoLocationActions(result.state);
        } else if (result.state === 'prompt') {
          geoLocationActions(result.state);
        } else if (result.state === 'denied') {
          geoLocationActions(result.state);
        }
      });
  };

  const onCurrentLocation = () => {
    // handlePermission();
    if (typeof window !== 'undefined' && window.navigator) {
      if (
        (window.navigator.userAgent.indexOf('Opera') ||
          window.navigator.userAgent.indexOf('OPR')) !== -1
      ) {
        handlePermission();
      } else if (window.navigator.userAgent.indexOf('Chrome') !== -1) {
        handlePermission();
      } else if (window.navigator.userAgent.indexOf('Safari') !== -1) {
        getLocation();
      } else if (window.navigator.userAgent.indexOf('Firefox') !== -1) {
        handlePermission();
      } else if (
        window.navigator.userAgent.indexOf('MSIE') !== -1 ||
        !!document.documentMode === true
      ) {
        getLocation();
      } else {
        getLocation();
      }
    }
  };

  return (
    <>
      <Header
        {...props}
        type="articles"
        placeHolderValue="Search Article"
        showLocation
        onLocationClick={e => {
          e.preventDefault();
          toggleLocationModal();
        }}
        selectedLocation={selectedCity.value}
        isModalActive={modalStatus}
        activeModalType={modalType}
        setActiveCallback={setModalStatus}
      />
      <OnlineProgramSection>
        {/* Filter Option Desktop */}
        <SideFilter className="desktop">
          <div className="changeSide">
            <div className="clearFilter">
              <button
                type="button"
                onClick={() => {
                  window.scrollTo(
                    0,
                    infiniteRef.current && infiniteRef.current.offsetTop,
                  );
                  onApplyFilter();
                }}
              >
                Apply Filters
              </button>
              <button
                type="button"
                onClick={() => {
                  onClearCategorySearch();
                  onClearFilter();
                }}
              >
                Clear Filter <span>X</span>{' '}
              </button>
            </div>

            <Accordion
              // title={`Age Group ${ageRange.min} - ${ageRange.max}`}
              title="Age"
            >
              <div>
                <div className="rangeSetting">
                  <Flex justifyBetween>
                    <div className="ageWrap">
                      {/* <span>Years</span> */}
                      <AgeDropdown
                        type="products"
                        placeHolder="Years"
                        width="40% !important"
                        margin="12px 0px 0px"
                        onChange={e => ageYear.onChange(e)}
                        selected={ageYear.value}
                        values={[
                          { id: 0, key: 0, name: 'Any' },
                          { id: 1, key: 1, name: '1 Year' },
                          { id: 2, key: 2, name: '2 Years' },
                          { id: 3, key: 3, name: '3 Years' },
                          { id: 4, key: 4, name: '4 Years' },
                          { id: 5, key: 5, name: '5 Years' },
                          { id: 6, key: 6, name: '6 Years' },
                          { id: 7, key: 7, name: '7 Years' },
                          { id: 8, key: 8, name: '8 Years' },
                          { id: 9, key: 9, name: '9 Years' },
                          { id: 10, key: 10, name: '10 Years' },
                          { id: 11, key: 11, name: '11 Years' },
                          { id: 12, key: 12, name: '12 Years' },
                          { id: 13, key: 13, name: '13 Years' },
                          { id: 14, key: 14, name: '14 Years' },
                          { id: 15, key: 15, name: '15 Years' },
                        ]}
                      />
                    </div>
                  </Flex>
                </div>
              </div>
            </Accordion>

            <Accordion title="Categories">
              <>
                {/* Remove hide from class small and filterItem-checkbox */}
                <SearchBar className="small">
                  <input
                    type="text"
                    placeholder="Search Category"
                    onChange={onSearchCategory}
                    value={catSearchKey}
                  />
                  <span
                    className="iconify"
                    data-icon="bytesize:search"
                    data-inline="false"
                  />
                </SearchBar>
                {/* {categoryLoader ? (
                  <FilterLoader>
                    <Flex
                      alignCenter
                      justifyCenter
                      flexWidth="100%"
                      flexHeight="100%"
                    >
                      <img src={pulseLoader} alt="" height={75} width={75} />
                    </Flex>
                  </FilterLoader>
                ) : ( */}
                <>
                  {categoryList.length ? (
                    <div className="filterItem-checkbox filterItem-checkbox-scroll">
                      {categoryList.map(cat => (
                        <CheckBox
                          label={cat.name}
                          id={cat.id}
                          margin="0px 0px 20px 0px"
                          onClickFunction={e => {
                            e.preventDefault();
                            category.onSelect(cat.id);
                          }}
                          checked={category.value.includes(cat.id)}
                        />
                      ))}
                    </div>
                  ) : (
                    <>{!categoryLoader && <p>No Categories</p>}</>
                  )}
                </>
                {/* )} */}
              </>
            </Accordion>

            <Accordion title="Mode" className="filterItem-checkbox">
              {SchoolTypeConfig.map(type => (
                <CheckBox
                  label={type.label}
                  id={type.key}
                  margin="0px 0px 20px 0px"
                  onClickFunction={e => {
                    e.preventDefault();
                    schoolType.onSelect(type.key);
                  }}
                  checked={type.key === schoolType.value}
                />
              ))}
            </Accordion>

            <Accordion
              title="Price Range"
              // title={`Price Range ₹${range.min} - ₹${range.max}`}
            >
              <div className="priceRangeWrap">
                {maxPriceLoader ? (
                  <SearchLoader>
                    <Flex
                      alignCenter
                      justifyCenter
                      flexWidth="100%"
                      flexHeight="100%"
                    >
                      <img src={pulseLoader} alt="" height={75} width={75} />
                    </Flex>
                  </SearchLoader>
                ) : (
                  <InputRange
                    maxValue={maxPrice}
                    minValue={0}
                    value={range}
                    formatLabel={() => null}
                    onChange={val => {
                      onChangePrice(val);
                    }}
                    onChangeComplete={val => onSliderComplete(val)}
                    allowSameValues={false}
                    step={100}
                  />
                )}
                <Flex justifyBetween>
                  <DropDown
                    type="products"
                    placeHolder="Min"
                    width="32% !important"
                    margin="12px 0px 0px"
                    onChange={e =>
                      onSliderComplete({ min: e.target.value, max: range.max })
                    }
                    value={range.min}
                  />
                  <DropDown
                    type="products"
                    placeHolder="Max"
                    width="32% !important"
                    margin="12px 0px 0px"
                    onChange={e =>
                      onSliderComplete({ min: range.min, max: e.target.value })
                    }
                    value={range.max}
                  />
                </Flex>
              </div>
            </Accordion>

            <Accordion title="Course Type" className="filterItem-checkbox">
              {CourseTypeConfig.map(type => (
                <CheckBox
                  label={type.label}
                  id={type.key}
                  margin="0px 0px 20px 0px"
                  checked={courseType.value.includes(type.key)}
                  onClickFunction={e => {
                    e.preventDefault();
                    courseType.onSelect(type.key);
                  }}
                />
              ))}
            </Accordion>

            <Accordion title="More Filters" className="filterItem-checkbox">
              <div>
                <div className="filterItem-checkbox">
                  <CheckBox
                    label="Trial Class"
                    id="isTrial"
                    margin="0px 0px 20px 0px"
                    checked={trialClass.value === 'True'}
                    onClickFunction={e => {
                      e.preventDefault();
                      trialClass.onSelect('True');
                    }}
                  />
                </div>
                <div className="filterItem-checkbox">
                  <CheckBox
                    label="Discount"
                    id="discount"
                    margin="0px 0px 20px 0px"
                    checked={discount.value === 'True'}
                    onClickFunction={e => {
                      e.preventDefault();
                      discount.onSelect('True');
                    }}
                  />
                </div>
                <div className="filterItem-checkbox">
                  <CheckBox
                    label="Certificate"
                    id="CertificateTrue"
                    margin="0px 0px 20px 0px"
                    checked={certificate.value === 'True'}
                    onClickFunction={e => {
                      e.preventDefault();
                      certificate.onSelect('True');
                    }}
                  />
                </div>

                <div className="filterItem-checkbox">
                  <CheckBox
                    label="Moneyback Guarantee"
                    id="MoneybackTrue"
                    margin="0px 0px 20px 0px"
                    checked={moneyBack.value === 'True'}
                    onClickFunction={e => {
                      e.preventDefault();
                      moneyBack.onSelect('True');
                    }}
                  />
                </div>
              </div>
            </Accordion>
          </div>
        </SideFilter>

        <MainContent>
          {' '}
          {/* <h1>Online Preschool</h1> */}
          <h1>Book Free Trial Class</h1>
          <SearchBar>
            <span
              className="iconify"
              data-icon="bytesize:search"
              data-inline="false"
            />
            <input
              type="text"
              placeholder="Search Programs"
              value={searchKey.value}
              onChange={searchKey.onSearch}
            />
          </SearchBar>
          {/* Filter Option Mobile */}
          <FilterHeader
            className="mobile"
            onClick={() => setFilterSectionActive(!filterSectionActive)}
          >
            <h3>Filter Section</h3>
            <div className={filterSectionActive ? 'hide-icon' : ''}>
              <span
                className="iconify"
                data-icon="eva:arrow-ios-downward-outline"
                data-inline="false"
              />
            </div>
            <div className={!filterSectionActive ? 'hide-icon' : ''}>
              <span
                className="iconify"
                data-icon="eva:arrow-ios-upward-outline"
                data-inline="false"
              />
            </div>
          </FilterHeader>
          {filterSectionActive && (
            <SideFilter className="mobile">
              <Accordion
                title="Age"
                // title={`Age Group
                //  ${ageRange.min} - ${ageRange.max}
                // `}
                isMobile
              >
                <div>
                  <div className="rangeSetting">
                    <Flex justifyBetween>
                      <AgeDropdown
                        type="products"
                        placeHolder="Years"
                        width="40% !important"
                        margin="12px 0px 0px"
                        onChange={e => ageYear.onChange(e)}
                        selected={ageYear.value}
                        values={[
                          { id: 0, key: 0, name: 'Any' },
                          { id: 1, key: 1, name: '1 Year' },
                          { id: 2, key: 2, name: '2 Years' },
                          { id: 3, key: 3, name: '3 Years' },
                          { id: 4, key: 4, name: '4 Years' },
                          { id: 5, key: 5, name: '5 Years' },
                          { id: 6, key: 6, name: '6 Years' },
                          { id: 7, key: 7, name: '7 Years' },
                          { id: 8, key: 8, name: '8 Years' },
                          { id: 9, key: 9, name: '9 Years' },
                          { id: 10, key: 10, name: '10 Years' },
                          { id: 11, key: 11, name: '11 Years' },
                          { id: 12, key: 12, name: '12 Years' },
                          { id: 13, key: 13, name: '13 Years' },
                          { id: 14, key: 14, name: '14 Years' },
                          { id: 15, key: 15, name: '15 Years' },
                        ]}
                      />
                    </Flex>
                  </div>
                </div>
              </Accordion>

              <Accordion title="Categories" isMobile>
                <>
                  {/* Remove hide from class small and filterItem-checkbox */}
                  <SearchBar className="small">
                    <input
                      type="text"
                      placeholder="Search Category"
                      onChange={onSearchCategory}
                      value={catSearchKey}
                    />
                    <span
                      className="iconify"
                      data-icon="bytesize:search"
                      data-inline="false"
                    />
                  </SearchBar>
                  {/* {categoryLoader ? (
                    <FilterLoader>
                      <Flex
                        alignCenter
                        justifyCenter
                        flexWidth="100%"
                        flexHeight="100%"
                      >
                        <img src={pulseLoader} alt="" height={75} width={75} />
                      </Flex>
                    </FilterLoader>
                  ) : ( */}
                  <>
                    {categoryList.length ? (
                      <div className="filterItem-checkbox filterItem-checkbox-scroll">
                        {categoryList.map(cat => (
                          <CheckBox
                            label={cat.name}
                            id={cat.id}
                            margin="0px 0px 20px 0px"
                            onClickFunction={e => {
                              e.preventDefault();
                              category.onSelect(cat.id);
                            }}
                            checked={category.value.includes(cat.id)}
                          />
                        ))}
                      </div>
                    ) : (
                      <>{!categoryLoader && <p>No Categories</p>}</>
                    )}
                  </>
                  {/* )} */}
                </>
              </Accordion>

              <Accordion title="Mode" className="filterItem-checkbox" isMobile>
                {SchoolTypeConfig.map(type => (
                  <CheckBox
                    label={type.label}
                    id={type.key}
                    margin="0px 0px 20px 0px"
                    onClickFunction={e => {
                      e.preventDefault();
                      schoolType.onSelect(type.key);
                    }}
                    checked={type.key === schoolType.value}
                  />
                ))}
              </Accordion>

              <Accordion
                title="Price Range"
                // title={`Price Range ₹${range.min} - ₹${range.max}`}
                isMobile
              >
                <div>
                  {maxPriceLoader ? (
                    <SearchLoader>
                      <Flex
                        alignCenter
                        justifyCenter
                        flexWidth="100%"
                        flexHeight="100%"
                      >
                        <img src={pulseLoader} alt="" height={75} width={75} />
                      </Flex>
                    </SearchLoader>
                  ) : (
                    <InputRange
                      maxValue={maxPrice}
                      minValue={0}
                      value={range}
                      formatLabel={() => null}
                      onChange={val => {
                        onChangePrice(val);
                      }}
                      onChangeComplete={val => onSliderComplete(val)}
                      allowSameValues={false}
                      step={100}
                    />
                  )}
                  <Flex justifyBetween>
                    <DropDown
                      type="products"
                      placeHolder="Min"
                      width="32% !important"
                      margin="12px 0px 0px"
                      onChange={e =>
                        onSliderComplete({
                          min: e.target.value,
                          max: range.max,
                        })
                      }
                      value={range.min}
                    />
                    <DropDown
                      type="products"
                      placeHolder="Max"
                      width="32% !important"
                      margin="12px 0px 0px"
                      onChange={e =>
                        onSliderComplete({
                          min: range.min,
                          max: e.target.value,
                        })
                      }
                      value={range.max}
                    />
                  </Flex>
                </div>
              </Accordion>

              <Accordion
                title="Course Type"
                className="filterItem-checkbox"
                isMobile
              >
                {CourseTypeConfig.map(type => (
                  <CheckBox
                    label={type.label}
                    id={type.key}
                    margin="0px 0px 20px 0px"
                    checked={courseType.value.includes(type.key)}
                    onClickFunction={e => {
                      e.preventDefault();
                      courseType.onSelect(type.key);
                    }}
                  />
                ))}
              </Accordion>

              <Accordion
                title="More Filters"
                className="filterItem-checkbox"
                isMobile
              >
                <div>
                  <div className="filterItem-checkbox">
                    <CheckBox
                      label="Trial Class"
                      id="isTrial"
                      margin="0px 0px 20px 0px"
                      checked={trialClass.value === 'True'}
                      onClickFunction={e => {
                        e.preventDefault();
                        trialClass.onSelect('True');
                      }}
                    />
                  </div>
                  <div className="filterItem-checkbox">
                    <CheckBox
                      label="Discount"
                      id="discount"
                      margin="0px 0px 20px 0px"
                      checked={discount.value === 'True'}
                      onClickFunction={e => {
                        e.preventDefault();
                        discount.onSelect('True');
                      }}
                    />
                  </div>
                  <div className="filterItem-checkbox">
                    <CheckBox
                      label="Certificate"
                      id="CertificateTrue"
                      margin="0px 0px 20px 0px"
                      checked={certificate.value === 'True'}
                      onClickFunction={e => {
                        e.preventDefault();
                        certificate.onSelect('True');
                      }}
                    />
                  </div>

                  <div className="filterItem-checkbox">
                    <CheckBox
                      label="Moneyback Gurantee"
                      id="MoneybackTrue"
                      margin="0px 0px 20px 0px"
                      checked={moneyBack.value === 'True'}
                      onClickFunction={e => {
                        e.preventDefault();
                        moneyBack.onSelect('True');
                      }}
                    />
                  </div>
                </div>
              </Accordion>
              {/* More Filter Drop Down */}
              {/* <div className="filterItem">
              <h2 className="filterItem-heading filterItem-heading-more">
                More Filter
              </h2>
              <div className="totalFilterItem hide">
                <h2>Filters</h2>
                <div className="filters">
                  <div className="left">
                    <ul>
                      <li>Mode</li>
                      <li className="active">Categories</li>
                      <li>Price Range</li>
                      <li>Course Type</li>
                      <li>Date</li>
                      <li>Time</li>
                      <li>Discount</li>
                      <li>Age Group</li>
                      <li>Certificate</li>
                      <li>By Rating</li>
                      <li>Financial Aid Available</li>
                      <li>Money Back Guarantee</li>
                    </ul>
                  </div>
                  <div className="right">
                    <div className="sorted">
                      <ul>
                        <li>
                          <CheckBox
                            label="Play Group (105+Years)"
                            margin="0 0 14px"
                          />
                        </li>
                        <li>
                          <CheckBox
                            label="Play Group (105+Years)"
                            margin="0 0 14px"
                          />
                        </li>
                        <li>
                          <CheckBox
                            label="Play Group (105+Years)"
                            margin="0 0 14px"
                          />
                        </li>
                        <li>
                          <CheckBox
                            label="Play Group (105+Years)"
                            margin="0 0 14px"
                          />
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div className="buttons">
                  <button type="button">Clear Filter</button>
                  <button type="button">Apply Filter</button>
                </div>
              </div>
            </div> */}
              <div className="selectedItems">
                <div className="split">
                  <button
                    type="button"
                    onClick={() => {
                      setFilterSectionActive(false);
                      onApplyFilter();
                    }}
                  >
                    Apply Filter
                  </button>
                  <button
                    type="button"
                    onClick={() => {
                      setFilterSectionActive(false);
                      onClearFilter();
                    }}
                  >
                    Clear Filter
                  </button>
                </div>
              </div>
            </SideFilter>
          )}
          <div className="selectedItems">
            {/* {isFilterApplied && ( */}
            <ul>
              {SchoolTypeConfig.map(type => (
                <>
                  {type.key === school && (
                    <li
                      onClick={e => {
                        e.preventDefault();
                        onClearIndividualFilter('schoolType');
                      }}
                    >
                      {type.label} <span>x</span>
                    </li>
                  )}
                </>
              ))}
              {categoryList.length ? (
                <>
                  {categoryList.map(cat => (
                    <>
                      {categories &&
                        (typeof categories === 'string'
                          ? [+categories]
                          : categories.map(id => +id)
                        ).includes(+cat.id) && (
                          <li
                            onClick={e => {
                              e.preventDefault();
                              onClearIndividualFilter('category', cat.id);
                            }}
                          >
                            {cat.name} <span>x</span>
                          </li>
                        )}
                    </>
                  ))}
                </>
              ) : null}

              {CourseTypeConfig.map(type => (
                <>
                  {courses &&
                    (typeof courses === 'string'
                      ? [courses]
                      : courses
                    ).includes(type.key) && (
                      <li
                        onClick={e => {
                          e.preventDefault();
                          onClearIndividualFilter('courses', type.key);
                        }}
                      >
                        {type.label} <span>x</span>
                      </li>
                    )}
                </>
              ))}

              {isTrial === 'True' && (
                <li
                  onClick={e => {
                    e.preventDefault();
                    onClearIndividualFilter('trial');
                  }}
                >
                  Trial Class <span>x</span>
                </li>
              )}

              {isDiscount === 'True' && (
                <li
                  onClick={e => {
                    e.preventDefault();
                    onClearIndividualFilter('dicount');
                  }}
                >
                  Discount <span>x</span>
                </li>
              )}

              {isCertificate === 'True' && (
                <li
                  onClick={e => {
                    e.preventDefault();
                    onClearIndividualFilter('certificate');
                  }}
                >
                  Certificate <span>x</span>
                </li>
              )}

              {isMoneyback === 'True' && (
                <li
                  onClick={e => {
                    e.preventDefault();
                    onClearIndividualFilter('moneyBack');
                  }}
                >
                  Moneyback <span>x</span>
                </li>
              )}
              {isCostChanged &&
                ((startPrice && +startPrice > 0) ||
                  (endPrice && +endPrice < maxPrice && +maxPrice)) && (
                  <li
                    onClick={() => {
                      onClearIndividualFilter('price');
                    }}
                  >
                    {`Price Range ₹${startPrice} - ₹${endPrice}`} <span>x</span>
                  </li>
                )}
              {endAge && (startAge && +startAge > 0) ? (
                <li onClick={() => onClearIndividualFilter('age')}>
                  {endAge ? `Age ${+endAge / 12} Years` : 'Any Age'}
                  {` `}
                  <span>x</span>
                </li>
              ) : null}
            </ul>
            {/* )} */}
          </div>
          {/* <SelectedItem>
            {ProgramTypeConfig.map(type => (
              <li
                className={type.key === programType.value && 'active'}
                onClick={() => programType.onSelect(type.key)}
              >
                {type.label}
              </li>
            ))}
          </SelectedItem> */}
          <InfiniteScroll
            ref={infiniteRef}
            dataLength={searchData.length}
            next={() => onSearchLoadMore(searchData.length)}
            hasMore={
              !!searchData.length &&
              searchData.length >= PROGRAM_LIMIT &&
              showLoadMore
            }
            loader={
              <SearchLoader>
                <Flex
                  alignCenter
                  justifyCenter
                  flexWidth="100%"
                  flexHeight="100%"
                >
                  <img src={pulseLoader} alt="" height={50} width={50} />
                </Flex>
              </SearchLoader>
            }
            endMessage={
              searchData.length >= PROGRAM_LIMIT ? (
                <p style={{ textAlign: 'center' }} className="endMessage">
                  Yay! You have seen it all
                </p>
              ) : null
            }
            scrollThreshold={0.1}
          >
            {categoryLoader || (showLoader && searchLoader) ? (
              <MascotLoader height="50vh" />
            ) : (
              <ClassSection>
                {searchData.length ? (
                  searchData.map(program => (
                    <ClassCard
                      isLoggedIn={isLoggedIn}
                      className="searchCards"
                      width="315px"
                      isOneToOne={program.online_tags === 'one_to_one'}
                      name={program.name}
                      rating={program.rating && program.rating.rating__avg}
                      providerName={program.provider_name}
                      // age={program.age}
                      startAge={program.start_age}
                      endAge={program.end_age}
                      isSingleSession={
                        program.batch_available &&
                        program.batch_available.length
                          ? program.batch_available.includes('Single_Session')
                          : null
                      }
                      isTrial={
                        program.batch_available &&
                        program.batch_available.length
                          ? program.batch_available.includes('Trial_Class')
                          : null
                      }
                      price={program.price}
                      moneyBack={
                        program.price && program.price.money_back_gurantee
                      }
                      isSponsered={program.is_sponsored}
                      media={program.medias}
                      onBookmark={() => handleBookMark(program && program.id)}
                      onDeleteBookmark={() =>
                        handleDeleteBookMark(program && program.id)
                      }
                      isBookMarked={bookmarkedItems.includes(+program.id)}
                      // onCardClick={e => handleCardClick(program, e)}
                      href={`/online-program/${program.name
                        .replace(/\s+/g, '-')
                        .replace('/', '-')
                        .toLowerCase()}?id=${program.id}`}
                      programId={program.id}
                      batches={program.batch_list}
                    />
                  ))
                ) : (
                  <Refresh
                  // isFilterApplied={checkIfFiltersChnaged(appliedFilters)}
                  />
                )}
              </ClassSection>
            )}
          </InfiniteScroll>
        </MainContent>
      </OnlineProgramSection>
      {!searchLoader && <Footer {...props} />}
      {!isCitySelected && (
        <Modal
          type="prefferedCity"
          setModalType={setModalType}
          // setAuthActionSelected={setAuthActionSelected}
          // setActive={setModalActive}
          closeHandler={toggleLocationModal}
          // authActionSelected={authActionSelected}
          // clickType={restrictionType}
          courseType={courseType}
          // onSubmit={selectedCity.onSubmit}
          cities={cityList}
          onSubmit={selectedCity.onSelect}
          selectedCity={selectedCity.value}
          onSearch={onSearchCity}
          searchKey={citySearchKey}
          loader={cityLoader}
          errorToast={errorToast}
          onCurrentLocation={onCurrentLocation}
          locationLoader={locationLoader}
          onSelectResult={getCityName}
          {...props}
        />
      )}
    </>
  );
};

OnlineProgram.propTypes = {};

export default OnlineProgram;

const OnlineProgramSection = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 140px 30px;
  @media screen and (max-width: 978px) {
    padding: 100px 30px;
  }
  @media screen and (max-width: 768px) {
    padding: 68px 10px;
    position: relative;
  }
  .desktop {
    @media screen and (max-width: 768px) {
      display: none;
    }
  }
  .hide-icon {
    display: none;
  }
  .mobile {
    display: none;
    @media screen and (max-width: 768px) {
      display: flex;
      flex-wrap: wrap;
      justify-content: space-between;
      height: auto;
      h3 {
        font-size: 12px;
        margin: 0;
        font-weight: 400;
        color: #4b5256;
      }
    }
    .filterItem-checkbox,
    .small {
      .sc-htpNat {
        flex-direction: column;
        .hBcAXb.products {
          width: 100% !important;
        }
      }
    }
  }
  .ctgNAw.products {
    @media screen and (max-width: 768px) {
      width: 47% !important;
    }
  }
  .productCategory__header {
    @media screen and (max-width: 768px) {
      height: 35px !important;
    }
  }

  .clearFilter {
    display: flex;
    justify-content: space-between;
    position: sticky;
    top: 0;
    z-index: 9;
    background: #fff;
    padding-bottom: 20px;
    flex-direction: row-reverse;
    button {
      cursor: pointer;
      background: #fff;
      padding: 10px;
      border: 1px solid #ccc;
      border-radius: 5px;
      margin-bottom: 20px;
      display: flex;
      align-items: center;
      justify-content: space-evenly;
      width: 110px;
      margin: 0;
      &:first-child {
        border: 1px solid #60b947;
        background-color: #60b947;
        color: #ffffff;
      }
    }
  }
  .selectedItems {
    display: none;
    @media screen and (max-width: 768px) {
      display: flex;
      width: 100%;
      flex-direction: column-reverse;
      border: none;
    }
    justify-content: space-between;
    align-items: center;
    margin: 0;
    padding: 16px 0;
    border-bottom: 1px solid #e6e8ec;
    flex-direction: row;
    ul {
      margin: 0 0;
      padding: 0;
      display: flex;
      flex-direction: row;
      flex-wrap: wrap;
      align-items: center;
      justify-content: space-evenly;
      @media screen and (max-width: 768px) {
        margin: 0 15px;
      }
      li {
        list-style-type: none;
        background: #603a95;
        border-radius: 3px;
        font-family: Roboto;
        font-style: normal;
        font-weight: normal;
        font-size: 11px;
        line-height: 13px;
        color: #fff;
        font-weight: 600;
        padding: 5px 10px;
        margin-right: 6px;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        white-space: pre;
        height: -webkit-max-content;
        height: -moz-max-content;
        height: max-content;
        margin: 0px 5px 10px 0;
        &:last-child {
          margin-right: 0;
        }
      }
    }
    .split {
      display: flex;
      flex-direction: row-reverse;
      justify-content: space-evenly;
      width: 100%;
      margin-bottom: 20px;
      @media screen and (max-width: 768px) {
        button {
          margin-bottom: 0;
          padding: 10px 20px;
          font-size: 13px;
        }
      }
    }
    button {
      font-family: Roboto;
      font-style: normal;
      font-weight: normal;
      font-size: 11px;
      line-height: 13px;
      color: #4b5256;
      background: none;
      border: none;
      white-space: nowrap;
      margin-bottom: 10px;
      border: 1px solid #ccc;
      border-radius: 5px;
      padding: 5px;
    }
    button:first-child {
      border: 1px solid #60b947;
      background-color: #60b947;
      color: #ffffff;
    }
  }
  .ageWrap {
    display: flex;
    flex-direction: column;
    span {
      margin-top: 15px;
      color: #666c78;
      font-family: 'Roboto', sans-serif;
      font-size: 14px;
      line-height: 19px;
    }
    .dropdown {
      width: 200px;
      max-width: 100%;
      justify-content: space-between;
    }
    ul {
      max-width: 100%;
      width: 200px;
      li {
      }
    }
  }
`;

const SideFilter = styled.div`
  width: 310px;
  min-height: 100%;
  overflow-y: scroll;
  overflow-x: hidden;
  // direction: rtl;
  scrollbar-width: 5px;
  height: 100%;
  position: fixed;
  z-index: 9;
  background: #fff;

  // /* Hide scrollbar for Chrome, Safari and Opera */
  // &::-webkit-scrollbar {
  //   display: none;
  // }
  // -ms-overflow-style: none; /* IE and Edge */
  // scrollbar-width: none; /* Firefox */
  @media screen and (max-width: 768px) {
    height: auto;
    width: 100%;
    margin-top: 20px;
    min-height: auto;
    overflow: visible;
    direction: ltr;
    position: initial;
  }

  .changeSide {
    // direction: ltr;
    margin-right: 15px;
    margin-bottom: 40%;
    @media screen and (max-width: 768px) {
      margin: 0;
    }
  }
  .filterItem {
    padding-bottom: 20px;
    padding-top: 20px;
    border-bottom: 1px solid #dbdfe7;
    z-index: 1;
    position: relative;
    @media screen and (max-width: 768px) {
      padding: 10px;
      border: 1px solid #bfc8ce;
      border-radius: 4px;
      margin-bottom: 15px;
      width: 48%;
      height: fit-content;
    }
    @media screen and (max-width: 500px) {
      width: 100%;
    }
    &-heading {
      display: flex;
      align-items: center;
      justify-content: space-between;
      font-family: Roboto;
      font-weight: 400;
      font-size: 20px;
      line-height: 23px;
      color: #4b5256;
      margin: 0px 0px 0;
      cursor: pointer;
      @media screen and (max-width: 768px) {
        font-size: 14px;
        line-height: 16px;
      }
      .iconify {
        width: 24px;
        height: 24px;
        @media screen and (max-width: 768px) {
          width: 12px;
          height: 12px;
        }
      }
    }
    &-heading-more {
      justify-content: center;
    }

    &-checkbox {
    }
    .input-range {
      margin-top: 20px;
    }
    .input-range__slider {
      background: #613a95;
      border: 5px solid #fff;
      box-shadow: 0px 0px 2px #000;
    }
    .input-range__track--background {
      margin-top: -0.05rem;
      height: 6px;
    }
    .input-range__label--min,
    .input-range__label--max {
      .input-range__label-container {
        display: none;
      }
    }
    .totalFilterItem {
      position: absolute;
      top: 56px;
      left: 0;
      right: 0;
      background: #fff;
      height: 100vh;
      overflow: scroll;
      padding: 0 15px 0 30px;
      h2 {
        margin: 0;
        font-weight: 500;
        font-size: 18px;
        line-height: 22px;
        color: #000000;
        padding: 19px 0 10px;
        border-bottom: 1px solid #eaedf2;
      }
      .filters {
        display: flex;
        flex-direction: row;
        .left {
          width: 50%;
          ul {
            padding: 0;
            margin: 0;
            border-bottom: 1px solid #eaedf2;
            li {
              list-style-type: none;
              font-size: 14px;
              line-height: 16px;
              color: #7e888e;
              padding: 17px 0 13px;
              border-bottom: 1px solid #eaedf2;
              border-right: 1px solid #eaedf2;
              &.active {
                border-right: none;
                color: #30333b;
              }
              &:last-child {
                border-bottom: none;
              }
            }
          }
        }
        .right {
          width: 50%;
          padding: 17px 0 0 10px;
          ul {
            margin: 0;
            padding: 0;
            li {
              list-style-type: none;
            }
          }
        }
      }
      .buttons {
        display: flex;
        padding: 20px 0 25px;
        border-top: 1px solid #eaedf2;
        button {
          padding: 11px 22px;
          border-radius: 3px;
        }
        button:first-child {
          border: 1px solid #666c78;
          box-sizing: border-box;
          background: none;
          margin-right: 15px;
          color: #30333b;
          font-weight: 500;
          font-size: 12px;
          line-height: 15px;
        }
        button:last-child {
          background: #60b947;
          border-radius: 3px;
          font-size: 12px;
          line-height: 15px;
          border: none;
          color: #ffffff;
        }
      }
    }
    .totalFilterItem.hide {
      display: none;
    }
    .priceRangeWrap {
      width: 94%;
      margin: 0 auto;
    }
  }
  .filterItem-checkbox {
    margin-top: 20px;
    padding-top: 1px;
  }
  .filterItem-checkbox-scroll {
    max-height: 200px;
    overflow-y: scroll;
  }
  .productCategory__header {
    font-size: 14px;
    line-height: 16px;
  }
  .rangeSetting {
    @media screen and (max-width: 768px) {
      width: 95%;
      margin: auto;
    }
  }
  .hide {
    display: none !important;
  }
`;

const MainContent = styled.div`
  // width: calc(100% - 345px);
  margin-left: 345px;
  width: 75%;
  @media screen and (max-width: 768px) {
    width: 100%;
    margin-left: 0;
  }
  & > h1 {
    font-family: Quicksand;
    font-weight: 600;
    font-size: 32px;
    line-height: 38px;
    color: #000000;
    margin: 0px 0px 32px;
    @media screen and (max-width: 768px) {
      font-weight: bold;
      font-size: 20px;
      line-height: 25px;
      color: #30333b;
      margin-bottom: 20px;
    }
  }
  .loadingSkeleton {
    span:first-child {
      border-radius: 10px;
    }
    margin-top: 28px;
    @media (max-width: 500px) {
      width: 100%;
    }
    & > span:first-child .react-loading-skeleton {
      @media (max-width: 1300px) {
        width: 230px !important;
        height: 170px !important;
      }
      @media (max-width: 500px) {
        width: 100% !important;
        height: 200px !important;
      }
    }
    & > span:nth-child(2) .react-loading-skeleton {
      @media (max-width: 1300px) {
        width: 100px !important;
        height: 20px !important;
      }
    }
    & > span:last-child .react-loading-skeleton {
      @media (max-width: 1300px) {
        width: 230px !important;
        height: 40px !important;
      }
      @media (max-width: 500px) {
        width: 100% !important;
        height: 40px !important;
      }
    }
  }
`;

const SearchBar = styled.div`
  border: 1px solid #dbdee5;
  border-radius: 5px;
  padding-left: 40px;
  // width: 400px;
  width: 100%;
  position: relative;
  margin-bottom: 40px;
  @media screen and (max-width: 768px) {
    width: 100%;
    margin-bottom: 20px;
  }
  &.small {
    width: 100%;
    margin-bottom: 20px;
    margin-top: 20px;
  }
  input {
    padding: 13px 0px;
    width: 100%;
    border: 0px;
    outline: 0px;
    height: 53px;
    @media screen and (max-width: 768px) {
      font-size: 12px;
      line-height: 15px;
      padding: 16px 10px 16px 0;
      margin-right: 12px;
    }
    &::-webkit-input-placeholder {
      font-family: 'Roboto', sans-serif;
      font-weight: 400;
      font-size: 14px;
      line-height: 16px;
      color: #4b5256;
    }
  }
  .iconify {
    width: 18px;
    height: 18px;
    position: absolute;
    color: #4b5256;
    left: 10px;
    top: 0;
    bottom: 0;
    margin: auto;
  }
`;

// const SelectedItem = styled.ul`
//   display: flex;
//   align-items: center;
//   padding: 0px;
//   margin-top: 34px;
//   margin-bottom: 26px;
//   flex-wrap: wrap;
//   margin-left: auto;
//   margin-right: auto;
//   width: 80%;
//   justify-content: space-between;
//   @media screen and (max-width: 768px) {
//     align-items: center;
//     justify-content: space-around;
//     margin-bottom: 10px;
//   }
//   li {
//     height: 50px;
//     display: flex;
//     align-items: center;
//     justify-content: space-between;
//     padding: 0px 30px;
//     filter: drop-shadow(2px 2px 8px rgba(186, 186, 186, 0.25));
//     background: #ffffff;
//     border-radius: 5px;
//     font-family: Quicksand;
//     font-weight: 500;
//     font-size: 14px;
//     line-height: 17px;
//     color: #666c78;
//     margin-right: 0;
//     margin-bottom: 16px;
//     cursor: pointer;
//     @media screen and (max-width: 1050px) {
//       margin-right: 10px;
//     }
//     @media screen and (max-width: 768px) {
//       height: 37px;
//       font-weight: 500;
//       font-size: 12px;
//       line-height: 15px;
//       margin-right: 8px;
//       padding: 0 12px;
//       width: 42%;
//       align-items: center;
//       justify-content: center;
//       margin-right: 0;
//     }
//     @media screen and (max-width: 500px) {
//       font-size: 10px;
//     }
//     &.active,
//     &:hover {
//       box-shadow: 2px 2px 8px rgba(186, 186, 186, 0.25);
//       color: #fff;
//       background: #613a95;
//     }
//   }
// `;

const ClassSection = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: flex-start;
  margin-top: 10px;
  // &::after {
  //   content: '';
  //   width: 30%;
  // }
  @media screen and (max-width: 1365px) {
    // justify-content: space-around;
    margin: 0 auto;
    width: 650px;
  }
  @media screen and (max-width: 1040px) {
    width: 320px;
  }
  @media screen and (max-width: 768px) {
    width: auto;
  }
  @media screen and (max-width: 768px) {
    justify-content: space-around;
  }
  .searchCards {
    margin: 0 5px 40px;
  }
`;

const SearchLoader = styled.div`
  height: calc(100vh - 266px);
  width: 100%;
`;

// const FilterLoader = styled.div`
//   height: calc(100vh - 500px);
//   width: 100%;
// `;

const FilterHeader = styled.div`
  width: 100%;
  display: none;
  align-items: center;
  justify-content: space-between;
  padding: 10px;
  border: 1px solid #dbdee5;
  border-radius: 5px;
  cursor: pointer;
  @media (max-width: 768px) {
    margin-bottom: 20px;
  }
  @media (max-width: 500px) {
    display: flex;

    h3 {
      color: #30333b;
      font-family: Quicksand;
      font-size: 14px;
      font-weight: 500;
      letter-spacing: 0;
      line-height: 25px;
      margin: 0px;
    }
    .iconify {
      width: 18px;
      height: 18px;
    }
  }
`;
