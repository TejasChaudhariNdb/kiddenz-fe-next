/* eslint-disable no-nested-ternary */
/* eslint-disable no-param-reassign */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable indent */
/* eslint-disable array-callback-return */
/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import Router from 'next/router';
import moment from 'moment';
import Header from 'components/Header';
import Footer from 'components/Footer';
// import Router from 'next/router';
import MascotLoader from 'components/common/MascotLoader';
import {
  useOnlineProgramDetailHook,
  useOnlineProgramPaymentHook,
  usePurchasedOnlineProgramHook,
} from 'shared/hooks';
import getAgeText from 'shared/utils/getAgeText';
// import DropDown from './dropdowns';
import Flex from '../../components/common/flex';
import Ruppee from '../../components/common/Ruppee';
import CheckBox from '../../components/Checkbox';
import Button from '../../components/Button';

const CourseTypeConfig = [
  {
    label: 'Full Course',
    key: 'Full_Course',
    costKey: 'full_course_cost',
    dicountKey: 'full_course_discount',
  },
  {
    label: 'Home School',
    key: 'Home_School',
    costKey: 'full_course_cost',
    dicountKey: 'full_course_discount',
  },
  {
    label: 'Single Session',
    key: 'Single_Session',
    costKey: 'single_session_cost',
    dicountKey: 'single_session_discount',
  },
  {
    label: 'Monthly',
    key: 'Monthly',
    costKey: 'monthly_cost',
    dicountKey: 'monthly_discount',
  },
  {
    label: 'Trial Class',
    key: 'Trial_Class',
    costKey: 'trial_cost',
    dicountKey: 'trial_discount',
  },
];

const getCourseList = (batches = []) => {
  const courseList = [];
  batches.map(batch => {
    courseList.push(batch.batch_type);
  });
  const unique = [...new Set(courseList)];
  return unique;
};

function SessionPage(props) {
  const {
    authentication: {
      isLoggedIn,
      profile: {
        data: { name = '', mobile_number: contact = '', email = '' } = {},
      } = {},
    },
    // errorToast,
    query: { id: programId } = {},
    successToast,
    errorToast,
  } = props;

  const [batches, setBatches] = useState([]);
  const [modalStatus, setModalStatus] = useState(false);
  const [modalType, setModalType] = useState('');
  const [selectedBatch, setSelectedBatch] = useState(null);
  const [courseType, setCourseType] = useState('Full_Course');

  const {
    programDetail: { data: programDetail = {}, loader: detailLoader },
  } = useOnlineProgramDetailHook(props, { programId });

  const {
    noOfSessions,
    onBuyNow,
    createProgram: { loader: createOrderLoader },
  } = useOnlineProgramPaymentHook(props, {
    programId,
    courseType,
    onCreateOrderSuccess,
    onCreateOrderError,
  });

  const {
    getPurchasedPrograms,
    purchasedPrograms: {
      data: purchasedPrograms = [],
      loader: purchasedLoader,
    },
  } = usePurchasedOnlineProgramHook(props);

  function onCreateOrderSuccess({ data: { data } = {} }) {
    if (!data.amount) {
      successToast(
        `${
          courseType === 'Trial_Class' ? 'Trial Class' : ''
        } Booked successfully`,
      );
      getPurchasedPrograms();
      Router.push({
        pathname: `/payment-status`,
        query: { payment_id: data.payment_id },
      });
    } else {
      const options = {
        key: 'rzp_test_jFHt0q9YaPyixx', // Enter the Key ID generated from the Dashboard
        name: 'Kiddenz',
        description: 'Online Program',
        image:
          'https://kiddenz-media-prod.s3.ap-south-1.amazonaws.com/kiddenz-purple+logo-6a687d0564d5c69c121bbe02a8b30117.svg',
        order_id: data.payment_id,
        // eslint-disable-next-line no-unused-vars
        handler: async response => {
          try {
            getPurchasedPrograms();
            successToast('Payment successfully completed');
            Router.push({
              pathname: `/payment-status`,
              query: { payment_id: data.payment_id },
            });
            // const paymentId = response.razorpay_payment_id;
            // const url = `${API_URL}capture/${paymentId}`;
            // const captureResponse = await Axios.post(url, {});
            // console.log(captureResponse.data);
          } catch (err) {
            errorToast('Something went wrong');
            console.log(err, 'razor');
          }
        },
        prefill: {
          name,
          email,
          contact,
        },
        notes: {
          address: 'Razorpay Corporate Office',
        },
        theme: {
          color: '#3399cc',
        },
      };
      const rzp1 = new window.Razorpay(options);
      rzp1.open();
    }
  }
  function onCreateOrderError() {}

  useEffect(
    () => {
      if (
        programDetail &&
        programDetail.batches &&
        programDetail.batches.length
      ) {
        const batchesData = [...programDetail.batches];
        const batchesList = [];
        batchesData.map(a => {
          a.open = false;
          batchesList.push(a);
        });
        setBatches(batchesData);
      }
    },
    [programDetail && programDetail.batches],
  );

  const toggleBatches = id => {
    const batch = [...batches];
    batch.map(a => {
      if (a.id === id) {
        a.open = !a.open;
      }
    });
    setBatches(batch);
  };

  useEffect(
    () => {
      if (
        // programDetail &&
        // programDetail.program_cost &&
        // programDetail.program_cost.full_course_cost &&
        programDetail.batches.find(batch => batch.batch_type === 'Full_Course')
      ) {
        setCourseType('Full_Course');
      } else if (
        // programDetail &&
        // programDetail.program_cost &&
        // programDetail.program_cost.full_course_cost &&
        programDetail.batches.find(batch => batch.batch_type === 'Home_School')
      ) {
        setCourseType('Home_School');
      } else if (
        // programDetail &&
        // programDetail.program_cost &&
        // programDetail.program_cost.single_session_cost
        programDetail.batches.find(
          batch => batch.batch_type === 'Single_Session',
        )
      ) {
        setCourseType('Single_Session');
      } else if (
        // programDetail &&
        // programDetail.program_cost &&
        // programDetail.program_cost.monthly_cost
        programDetail.batches.find(batch => batch.batch_type === 'Monthly')
      ) {
        setCourseType('Monthly');
      } else if (
        // programDetail.program_cost &&
        // programDetail.program_cost.is_trial_available &&
        // (programDetail.program_cost.trial_cost ||
        //   +programDetail.program_cost.trial_cost === 0)
        programDetail.batches.find(batch => batch.batch_type === 'Trial_Class')
      ) {
        setCourseType('Trial_Class');
      }
    },
    [programDetail && programDetail.batches && programDetail.batches.length],
  );

  const handleCourseType = type => {
    // const batch = [...batches];
    if (type !== courseType) {
      setCourseType(type);
      setSelectedBatch(null);
      if (type === 'Trial_Class' || type === 'Single_Session') {
        const selected = batches.find(batch => batch.batch_type === type);
        setSelectedBatch(selected);
      } else if (
        type !== 'Trial_Class' &&
        type !== 'Single_Session' &&
        programDetail.program_detail &&
        programDetail.program_detail.online_tags === 'home_school'
      ) {
        const selected = batches.find(
          batch => batch.batch_type === 'Home_School',
        );
        setSelectedBatch(selected);
      }
    }
    // const filteredBatches = batch.filter(d => d.batch_type === type);
    // setBatches(filteredBatches);
  };

  const handleSelectBatch = batch => {
    setSelectedBatch(batch);
    if (batch && batch.id) {
      window.scrollTo(0, 0);
    }
  };

  const handleBuyNow = () => {
    if (!isLoggedIn) {
      setModalType('signup');
      setModalStatus(true);
    } else if (
      courseType !== 'Trial_Class' &&
      courseType !== 'Single_Session' &&
      programDetail.program_detail &&
      programDetail.program_detail.online_tags === 'home_school'
    ) {
      const selected = batches.find(
        batch => batch.batch_type === 'Home_School',
      );
      onBuyNow('Full_Course', selected);
    } else if (
      courseType !== 'Trial_Class' &&
      courseType !== 'Single_Session' &&
      (programDetail.program_detail &&
        programDetail.program_detail.online_tags !== 'home_school') &&
      selectedBatch &&
      selectedBatch.id
    ) {
      onBuyNow(courseType, selectedBatch);
      // errorToast('Kindly seelct the session')
    }
    // else if (
    //   courseType !== 'Trial_Class' &&
    //   courseType !== 'Single_Session' &&
    //   selectedBatch &&
    //   selectedBatch.id
    // ) {
    //   onBuyNow(courseType, selectedBatch);
    //   // errorToast('Kindly seelct the session')
    // }
    else if (
      courseType !== 'Trial_Class' &&
      courseType !== 'Single_Session' &&
      !(selectedBatch && selectedBatch.id)
    ) {
      errorToast('Kindly select the session');
    } else if (courseType === 'Single_Session') {
      const selected = batches.find(batch => batch.batch_type === courseType);
      if (!noOfSessions.value) {
        errorToast('Please enter the no of session');
      } else {
        onBuyNow(courseType, selected);
      }
    } else if (courseType === 'Trial_Class') {
      const selected = batches.find(batch => batch.batch_type === courseType);
      onBuyNow(courseType, selected);
    }
  };

  const courseList = getCourseList(batches);

  return (
    <>
      <Header
        {...props}
        type="articles"
        placeHolderValue="Search Article"
        isModalActive={modalStatus}
        activeModalType={modalType}
        setActiveCallback={setModalStatus}
      />
      {detailLoader ? (
        <MascotLoader />
      ) : (
        <Session>
          <SessionLeft>
            {programDetail.program_detail &&
              programDetail.program_detail.name && (
                <h2>{programDetail.program_detail.name}</h2>
              )}
            {/* <DropDown
              type="products"
              placeHolder="Select Date"
              width="45% !important"
              margin="0px 0px 40px"
            /> */}
            {batches.length ? (
              <Flex
                flexWidth="100%"
                flexMargin="0px 0px 50px"
                className="mobile boxFlex"
              >
                {courseList.map(course => (
                  <>
                    {course === 'Home_School' && (
                      <CheckBox
                        label="Home School"
                        id="Home_School"
                        filterFont="18px"
                        onClickFunction={() => handleCourseType(course)}
                        checked={courseType === course}
                        margin="0px 30px 20px 0px"
                      />
                    )}
                    {course === 'Full_Course' && (
                      <CheckBox
                        label="Full Course"
                        id="Full_Course"
                        filterFont="18px"
                        onClickFunction={() => handleCourseType(course)}
                        checked={courseType === course}
                        margin="0px 30px 20px 0px"
                      />
                    )}
                    {course === 'Monthly' && (
                      <CheckBox
                        label="Monthly"
                        id="Monthly"
                        filterFont="18px"
                        onClickFunction={() => handleCourseType(course)}
                        checked={courseType === course}
                        margin="0px 30px 20px 0px"
                      />
                    )}
                    {course === 'Single_Session' && (
                      <CheckBox
                        label="Single Session"
                        id="Single_Session"
                        filterFont="18px"
                        onClickFunction={() => handleCourseType(course)}
                        checked={courseType === course}
                        margin="0px 30px 20px 0px"
                      />
                    )}
                    {course === 'Trial_Class' && (
                      <CheckBox
                        label="Trial Class"
                        id="Trial_Class"
                        filterFont="18px"
                        onClickFunction={() => handleCourseType(course)}
                        checked={courseType === course}
                        margin="0px 30px 20px 0px"
                      />
                    )}
                  </>
                ))}
              </Flex>
            ) : null}
            {programDetail.program_detail &&
            programDetail.program_detail.online_tags !== 'home_school' &&
            courseType !== 'Trial_Class' &&
            courseType !== 'Single_Session' &&
            batches &&
            batches.length ? (
              <>
                <h3>Upcoming Sessions</h3>

                {batches.filter(batch => batch.batch_type === courseType).map(
                  a =>
                    // eslint-disable-next-line indent
                    a.heading_name && (
                      <>
                        <BatchDetail className={a.open && 'active'}>
                          <div className="batch-header">
                            <Flex column>
                              <h4>{a.heading_name}</h4>
                              <div className="dates">
                                {a.start_date && a.end_date ? (
                                  <>
                                    <span>
                                      {moment(a.start_date).format('DD MMM YY')}
                                    </span>{' '}
                                    <span style={{ margin: '0 5px' }}>to</span>
                                    <span>
                                      {moment(a.end_date).format('DD MMM YY')}
                                    </span>
                                  </>
                                ) : (
                                  <span>{a.till_date}</span>
                                )}
                              </div>
                              <div className="batchSeats">
                                {a.no_of_seats && (
                                  <p>No of sessions: {`${a.no_of_seats}`}</p>
                                )}{' '}
                                |{' '}
                                {a.duration && (
                                  <p className="duration-section">
                                    {' '}
                                    Duration: {`${a.duration}`}
                                  </p>
                                )}
                              </div>
                            </Flex>
                            {/* <Flex column>
                              <h4>{a.heading_name}</h4>
                              {a.start_date && a.end_date ? (
                                <p>
                                  {moment(a.start_date).format('DD MMM')} –{' '}
                                  {moment(a.end_date).format('DD MMM')}
                                </p>
                              ) : (
                                <p>{a.till_date}</p>
                              )}
                              {a.no_of_seats && (
                                <p>No of sessions: {`${a.no_of_seats}`}</p>
                              )}
                              {a.duration && <p>Duration: {`${a.duration}`}</p>}
                            </Flex> */}
                            <Flex
                              alignCenter
                              onClick={() => toggleBatches(a.id)}
                            >
                              {/* <span style={{ marginRight: '10px' }}>
                                  {a.open ? 'Hide Schedule' : 'See Schedule'}
                                </span> */}
                              <div
                                className={a.open ? 'hide-icon' : ''}
                                style={{ cursor: 'pointer' }}
                              >
                                <div className="btn">
                                  <Button
                                    text={
                                      a.can_buy
                                        ? selectedBatch &&
                                          selectedBatch.id === a.id
                                          ? `Unselect Session`
                                          : `Select Session`
                                        : 'Expired'
                                    }
                                    disabled={!a.can_buy}
                                    height="30px"
                                    type={
                                      a.can_buy ? 'profile' : 'profile Expired'
                                    }
                                    onClick={() =>
                                      a.can_buy
                                        ? handleSelectBatch(
                                            selectedBatch &&
                                            selectedBatch.id === a.id
                                              ? null
                                              : a,
                                          )
                                        : null
                                    }
                                  />
                                  <i
                                    className="fa fa-plus"
                                    aria-hidden="true"
                                  />
                                </div>
                              </div>
                              <div
                                className={!a.open ? 'hide-icon' : ''}
                                style={{ cursor: 'pointer' }}
                              >
                                <i className="fa fa-minus" aria-hidden="true" />
                              </div>
                            </Flex>
                          </div>
                          {a.batch_slots && a.batch_slots.length ? (
                            <ul className="batch-dateList">
                              {a.batch_slots.map(d => (
                                <li className="batch-dateItem">
                                  <Flex column>
                                    <p>
                                      {moment(d.selected_date).format('DD MMM')}
                                    </p>
                                    {/* <span>Mon-thu</span> */}
                                  </Flex>
                                  {/* <div className="batch-time">
                                  {d.time.split(":") &&
                                  d.time.split(":")[0] > 12
                                    ? `${d.time.split(":")[0] - 12}:${
                                        d.time.split(":")[1]
                                      } PM`
                                    : `${d.time.split(":")[0]}:${
                                        d.time.split(":")[1]
                                      } AM`}
                                </div> */}
                                  {a.time && (
                                    <div className="batch-time">
                                      {moment(a.time, 'H:mm:ss').format(
                                        'h:mm a',
                                      )}
                                    </div>
                                  )}
                                  {/* <div>{a.time}</div> */}
                                </li>
                              ))}
                            </ul>
                          ) : null}
                          {a.batch_days && a.batch_days.length ? (
                            <ul className="batch-dateList">
                              {a.batch_days.map(d => (
                                <li className="batch-dateItem">
                                  <Flex column>
                                    <p>{d.selected_day}</p>
                                    {/* <span>Mon-thu</span> */}
                                  </Flex>
                                  {/* <div className="batch-time">
                                  {d.time.split(":") &&
                                  d.time.split(":")[0] > 12
                                    ? `${d.time.split(":")[0] - 12}:${
                                        d.time.split(":")[1]
                                      } PM`
                                    : `${d.time.split(":")[0]}:${
                                        d.time.split(":")[1]
                                      } AM`}
                                </div> */}
                                  {d.time && (
                                    <div className="batch-time">
                                      {moment(d.time, 'H:mm:ss').format(
                                        'h:mm a',
                                      )}
                                    </div>
                                  )}
                                  {/* <div>{a.time}</div> */}
                                </li>
                              ))}
                            </ul>
                          ) : null}
                          {a.additional_details && (
                            <p className="batch-buy">{a.additional_details}</p>
                          )}
                          <div className="batch-buy">
                            <span />
                            {/* <span>Sunday Off*</span> */}
                            <Button
                              text={
                                a.can_buy
                                  ? selectedBatch && selectedBatch.id === a.id
                                    ? `Unselect Session`
                                    : `Select Session`
                                  : 'Expired'
                              }
                              disabled={!a.can_buy}
                              height="30px"
                              type={a.can_buy ? 'profile' : 'profile Expired'}
                              onClick={() =>
                                a.can_buy
                                  ? handleSelectBatch(
                                      selectedBatch && selectedBatch.id === a.id
                                        ? null
                                        : a,
                                    )
                                  : null
                              }
                            />
                          </div>
                        </BatchDetail>
                      </>
                    ),
                )}
              </>
            ) : null}
            {courseType === 'Single_Session' && (
              <Inputfield>
                <h3>Select Sessions</h3>
                <h4>Number of sessions</h4>
                <InputBox className={noOfSessions.error && 'error'}>
                  <input
                    placeholder="No. of sessions"
                    onChange={noOfSessions.onChange}
                    onBlur={noOfSessions.onBlur}
                    value={noOfSessions.value}
                    type="number"
                    min="0"
                  />
                </InputBox>
                {noOfSessions.error && (
                  <div className="error">{noOfSessions.error}</div>
                )}
              </Inputfield>
            )}
          </SessionLeft>
          <SessionRight>
            <BuyNow>
              {/* {programDetail.program_detail &&
                programDetail.program_detail.age && (
                  <li>
                    <h2>
                      Age:
                      <span>{programDetail.program_detail.age} </span>
                    </h2>
                  </li>
                )} */}

              {programDetail.program_detail &&
                programDetail.program_detail.start_age && (
                  <li>
                    <h2>
                      Age:
                      <span>
                        {getAgeText(
                          programDetail.program_detail &&
                            programDetail.program_detail.start_age,
                          programDetail.program_detail &&
                            programDetail.program_detail.end_age,
                        )}
                      </span>
                    </h2>
                  </li>
                )}
              {noOfSessions.value &&
                courseType === 'Single_Session' && (
                  <li>
                    <h2>
                      No. Of session:
                      <span>{noOfSessions.value}</span>
                    </h2>
                  </li>
                )}

              {CourseTypeConfig.map(course => (
                <>
                  {course.key === courseType && (
                    <li>
                      <h2>
                        Course type:
                        <span>{course.label}</span>
                      </h2>
                    </li>
                  )}
                </>
              ))}
              {courseType !== 'Trial_Class' &&
                courseType !== 'Single_Session' &&
                courseType !== 'Home_School' &&
                selectedBatch &&
                selectedBatch.heading_name && (
                  <>
                    <li>
                      <h2>
                        Selected Batch:
                        <span>{selectedBatch.heading_name}</span>
                      </h2>
                    </li>
                    {selectedBatch.duration && (
                      <li>
                        <h2>
                          Duration:
                          <span>{selectedBatch.duration}</span>
                        </h2>
                      </li>
                    )}
                  </>
                )}
              <div className="proceed">
                {CourseTypeConfig.map(course => (
                  <>
                    {course.key === courseType && (
                      <Flex alignCenter flexMargin="36px 0px 8px">
                        {(programDetail.program_cost &&
                          programDetail.program_cost[course.costKey]) ||
                        (course.costKey === 'trial_cost' &&
                          programDetail.program_cost.is_trial_available) ? (
                          <>
                            {programDetail.program_cost &&
                            programDetail.program_cost[course.costKey] &&
                            programDetail.program_cost[course.dicountKey] ? (
                              <h3>
                                <Ruppee>₹</Ruppee>
                                {programDetail.program_cost[course.costKey] -
                                  programDetail.program_cost[
                                    course.dicountKey
                                  ] *
                                    (+noOfSessions.value || 1)}
                              </h3>
                            ) : (
                              <h3>
                                <Ruppee>₹</Ruppee>
                                {programDetail.program_cost[course.costKey] *
                                  (+noOfSessions.value || 1)}
                              </h3>
                            )}
                          </>
                        ) : null}
                        {programDetail.program_cost &&
                        programDetail.program_cost[course.dicountKey] ? (
                          <h6>
                            <span>
                              <Ruppee>₹</Ruppee>
                              {programDetail.program_cost[course.costKey]}
                            </span>
                            <Ruppee>₹</Ruppee>
                            {programDetail.program_cost[course.dicountKey]} Off
                          </h6>
                        ) : null}
                      </Flex>
                    )}
                  </>
                ))}

                {programDetail.program_cost &&
                  programDetail.program_cost.money_back_gurantee && (
                    <p>Money back guarantee</p>
                  )}
                {selectedBatch &&
                selectedBatch.id &&
                purchasedPrograms.find(
                  purchased =>
                    purchased.id === selectedBatch && selectedBatch.id,
                ) ? (
                  <span>
                    * You have previously purchased{' '}
                    {selectedBatch && selectedBatch.heading_name}
                  </span>
                ) : null}
                <Button
                  type="profile"
                  onClick={handleBuyNow}
                  text={courseType !== 'Trial_Class' ? 'Buy Now' : 'Book Now'}
                  isLoading={purchasedLoader || createOrderLoader}
                />
              </div>
            </BuyNow>
          </SessionRight>
        </Session>
      )}
      <Footer {...props} />
    </>
  );
}

SessionPage.propTypes = {};

export default SessionPage;

const InputBox = styled.div`
  position: relative;
  width: 50%;
  border: 1px solid #d9d9d9;
  border-radius: 4px;
  height: 60px;
  /* display: flex; */
  padding: 0px 24px;
  margin-top: 12px;
  background: #fff;
  @media (max-width: 1650px) {
    height: 48px;
    padding: 0px 18px;
  }

  &.error {
    border: 1px solid #e21212 !important;
  }

  & > input {
    border: 0px;
    width: 100%;
    font-family: Quicksand;
    font-weight: 500;
    font-size: 14px;
    line-height: 21px;
    color: #000000;
    text-transform: Capitalize;
    &:focus {
      outline: none;
    }
    &::-webkit-input-placeholder {
      font-family: Quicksand;
      font-weight: 400;
      font-size: 14px;
      line-height: 21px;
      color: rgba(0, 0, 0, 0.5);
    }
  }
  .error {
    color: red;
  }
`;

const Inputfield = styled.div`
  .error {
    font-family: Quicksand;
    font-weight: 500;
    font-size: 10px;
    line-height: 12px;
    color: #e21111;
    margin-top: 3px;
    margin-bottom: 10px;
  }
`;

const Session = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 140px 150px;
  @media screen and (max-width: 1030px) {
    padding: 140px 30px 140px 42px;
  }
  @media screen and (max-width: 768px) {
    padding: 75px 29px 75px 20px;
    flex-direction: column;
    align-items: center;
  }
  .boxFlex {
    align-items: flex-start !important;
  }
`;

const SessionLeft = styled.div`
  width: 65%;
  @media screen and (max-width: 950px) {
    width: 55%;
    margin-right: 40px;
  }
  @media screen and (max-width: 768px) {
    width: 100%;
    margin: 0;
  }
  & > h2 {
    font-family: Quicksand;
    font-weight: 600;
    font-size: 32px;
    line-height: 38px;
    color: #000000;
    margin: 0px 0px 40px;
    @media screen and (max-width: 768px) {
      font-size: 22px;
      line-height: 1.2;
      margin: 0px 0px 20px;
    }
  }
  & > h3 {
    font-family: Quicksand;
    font-weight: 500;
    font-size: 26px;
    line-height: 32px;
    color: #000000;
    margin: 0px 0px 40px;
    @media screen and (max-width: 768px) {
      margin: 0px 0px 10px;
      font-weight: bold;
      font-size: 16px;
      line-height: 20px;
      color: #000000;
    }
  }
  .mobile {
    @media screen and (max-width: 768px) {
      margin-bottom: 10px;
      flex-wrap: wrap;
    }
  }
  button.profile {
    @media screen and (max-width: 768px) {
      font-size: 14px;
      line-height: 1.2;
    }
  }
`;

const SessionRight = styled.div`
  width: 30%;
  @media screen and (max-width: 950px) {
    width: 50%;
  }
  @media screen and (max-width: 768px) {
    width: 100%;
  }
`;

const BatchDetail = styled.div`
  width: 100%;
  background: #ffffff;
  // margin-bottom: 32px;
  padding: 20px 0px;
  border-top: 1px dashed rgba(112, 112, 112, 0.22);
  &:last-child {
    border-bottom: 1px dashed rgba(112, 112, 112, 0.22);
  }
  @media screen and (max-width: 768px) {
    margin-bottom: 10px;
    border-radius: 4px;
  }
  &.active {
    // background: #f2eff5;
    .batch-dateList {
      display: block;
    }
    /* .batch-header span {
      display: none;
    } */
    .batch-buy {
      display: flex;
      @media screen and (max-width: 768px) {
        font-family: Roboto;
        font-style: normal;
        font-weight: normal;
        font-size: 14px;
        line-height: 16px;
        color: #4a5257;
      }
    }
    .batch-time {
      @media screen and (max-width: 768px) {
        padding: 0px 15px;
      }
    }
  }
  .hide-icon {
    display: none;
  }
  .batch {
    &-buy {
      display: none;
      justify-content: space-between;
      align-items: center;
      margin-top: 20px;
      span {
        font-family: 'Roboto', sans-serif;
        font-weight: 400;
        font-size: 13px;
        line-height: 15px;
        color: #4a5257;
      }
    }
    &-header {
      display: flex;
      justify-content: space-between;
      align-items: center;
      h4 {
        font-family: Quicksand;
        font-weight: 500;
        font-size: 18px;
        line-height: 26px;
        color: #000000;
        margin: 0px 0px 5px;
        @media screen and (max-width: 768px) {
          font-size: 16px;
        }
      }
      .dates {
        display: flex;
        align-items: center;
        margin: 0px 0px 0px;
        span {
          font-family: Quicksand;
          font-weight: bold;
          font-size: 15px;
          line-height: 21px;
          color: #613a94;
          @media screen and (max-width: 768px) {
            font-size: 12px;
          }
        }
      }
      .batchSeats {
        display: flex;
        align-items: center;
        color: #000000;
        opacity: 0.62;
      }
      p {
        font-family: Quicksand;
        font-weight: 400;
        font-size: 13px;
        line-height: 15px;
        color: #000000;
        margin: 0px 10px 0px 0px;
        @media screen and (max-width: 500px) {
          font-size: 10px;
          margin: 0px 10px 0px 0px;
        }
        @media screen and (max-width: 320px) {
          font-size: 9px;
          margin: 0px 2px 0px 0px;
        }
      }
      .duration-section {
        margin-left: 10px;
        @media screen and (max-width: 320px) {
          margin-left: 2px;
        }
      }
      span {
        display: block;
        font-family: 'Roboto', sans-serif;
        font-weight: 400;
        font-size: 12px;
        line-height: 14px;
        color: #613a95;
        cursor: pointer;
        // margin-right: 40px;
      }
    }
    &-dateList {
      padding-left: 0px;
      margin-top: 40px;
      display: none;
      @media screen and (max-width: 768px) {
        margin-top: 20px;
      }
    }
    &-dateItem {
      width: 58%;
      display: flex;
      justify-content: space-between;
      align-items: center;
      margin-bottom: 12px;
      @media screen and (max-width: 450px) {
        width: 100%;
      }
      p {
        font-family: Quicksand;
        font-weight: bold;
        font-size: 15px;
        line-height: 21px;
        color: #7ba959;
        margin: 0px;
      }
      span {
        font-family: 'Roboto', sans-serif;
        font-weight: 400;
        font-size: 14px;
        line-height: 16px;
        color: #4a5257;
        margin: 0px;
      }
    }
    &-time {
      display: flex;
      justify-content: center;
      align-items: center;
      height: 36px;
      background: #edf3e8;
      border-radius: 4px;
      border-radius: 4px;
      padding: 0px 10px;
      width: 100px;
      font-family: Quicksand;
      font-weight: bold;
      font-size: 15px;
      line-height: 21px;
      color: #7ba959;
    }
  }
  .btn {
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 0;
    button {
      margin: 0 15px 0 0;
      padding: 0 20px;
      height: 35px;
      font-size: 15px;
      @media screen and (max-width: 768px) {
        margin: 0 15px 0 0;
        padding: 15px;
        height: 20px;
        font-size: 12px;
      }
      @media screen and (max-width: 500px) {
        margin: 0 5px 0 0;
        padding: 13px 6px;
        height: 20px;
        font-size: 11px;
      }
    }
  }
  .Expired {
    background: #ccc !important;
    color: #fff !important;
    border: none !important;
    cursor: not-allowed !important;
  }
`;

// const BatchDetail = styled.div`
//   width: 100%;
//   padding: 18px;
//   background: #ffffff;
//   border: 1px solid #e4e4e4;
//   border-radius: 10px;
//   margin-bottom: 32px;
//   &.active {
//     background: #f2eff5;
//   }
//   .batch {
//     &-header {
//       display: flex;
//       justify-content: space-between;
//       align-items: center;
//       h4 {
//         font-family: Quicksand;
//         font-weight: 500;
//         font-size: 22px;
//         line-height: 27px;
//         color: #000000;
//         margin: 0px;
//       }
//       p {
//         font-family: Quicksand;
//         font-weight: bold;
//         font-size: 18px;
//         line-height: 22px;
//         margin: 4px 0px 0px;
//         color: #000000;
//       }
//       span {
//         font-family: 'Roboto', sans-serif;
//         font-weight: 400;
//         font-size: 20px;
//         line-height: 23px;
//         color: #4a5257;
//         margin-right: 40px;
//       }
//     }
//     &-dateList {
//       padding-left: 0px;
//       margin-top: 40px;
//     }
//     &-dateItem {
//       display: flex;
//       justify-content: space-between;
//       align-items: center;
//       margin-bottom: 38px;
//       p {
//         font-family: 'Roboto', sans-serif;
//         font-weight: bold;
//         font-size: 18px;
//         line-height: 21px;
//         color: #4a5257;
//         margin: 0px;
//       }
//       span {
//         font-family: 'Roboto', sans-serif;
//         font-weight: normal;
//         font-size: 14px;
//         line-height: 16px;
//         color: #4a5257;
//         margin: 0px;
//       }
//     }
//     &-time {
//       display: flex;
//       justify-content: center;
//       align-items: center;
//       height: 36px;
//       background: #ffffff;
//       border: 1px solid #bfc8ce;
//       border-radius: 4px;
//       padding: 0px 55px;
//     }
//   }
// `;

const BuyNow = styled.ul`
  width: 100%;
  padding: 0px 10px 20px;
  border: 1px solid #bfc8ce;
  border-radius: 10px;
  @media screen and (max-width: 768px) {
    padding: 0;
    background: #f2eff5;
    border: 0;
    border-radius: 0;
  }
  li {
    padding: 20px 0px;
    border-bottom: 1px solid #ddd9e1;
    list-style-type: none;
    @media screen and (max-width: 768px) {
      padding: 15px 10px;
    }
    h2 {
      display: flex;
      align-items: center;
      justify-content: space-between;
      font-family: Quicksand;
      font-weight: bold;
      font-size: 18px;
      line-height: 22px;
      color: #4b5256;
      margin: 0px;
      @media screen and (max-width: 768px) {
        font-size: 16px;
        line-height: 1.2;
      }
      span {
        font-family: 'Roboto', sans-serif;
        font-weight: 400;
        font-size: 18px;
        line-height: 21px;
        text-align: right;
        color: #4b5256;
        @media screen and (max-width: 768px) {
          font-size: 11px;
          line-height: 1.2;
        }
      }
    }
  }

  h3 {
    font-family: Quicksand;
    font-weight: bold;
    font-size: 24px;
    line-height: 30px;
    color: #000000;
    margin: 0px 8px 0px 0px;
  }
  h6 {
    margin: 0px;
    font-family: Quicksand;
    font-weight: bold;
    font-size: 14px;
    line-height: 17px;
    color: #000000;
    @media screen and (max-width: 768px) {
      font-size: 12px;
      line-height: 1.2;
    }
    span {
      font-weight: 400;
      text-decoration-line: line-through;
      opacity: 0.5;
      margin-right: 4px;
    }
  }
  p {
    font-family: Quicksand;
    font-weight: 400;
    font-size: 16px;
    line-height: 20px;
    color: #000000;
    margin: 0px 0px 18px;
    @media screen and (max-width: 768px) {
      font-size: 13px;
      line-height: 1.2;
    }
  }
  button {
    @media screen and (max-width: 768px) {
      font-size: 14px;
      line-height: 1.2;
    }
  }
  .proceed {
    @media screen and (max-width: 768px) {
      margin-top: 0;
      background: #613a95;
      padding: 20px;
      h1,
      h2,
      h3,
      h4,
      h5,
      h6,
      p {
        color: #ffffff;
      }
      button {
        background: #60b947;
        border-radius: 3px;
        padding: 10px;
        width: 100%;
        font-size: 16px;
        line-height: 20px;
        color: #ffffff;
      }
    }
  }
`;
