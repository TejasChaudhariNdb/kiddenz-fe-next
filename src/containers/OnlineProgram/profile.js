import React from 'react';
import Rating from 'react-rating';
import styled from 'styled-components';
import Flex from '../../components/common/flex';
import star from '../../images/star.svg';
import star1 from '../../images/star.1.svg';
// import DropDown from './dropdowns';
// import Button from '../../components/Button/index';
// import previewImage from '../../images/previewImage.png';
import profileImage from '../../images/Group 183.png';
// import star from '../../images/Symbols.png';
// import hour from '../../images/hour.png';
// import website from '../../images/Website.png';
// import mobile from '../../images/mobile.png';
// import classes from '../../images/classes.png';

function Profile() {
  return (
    <ProfileSection>
      <Flex>
        <ProfileImage>
          <img src={profileImage} alt="profileImage" />
        </ProfileImage>
        <ProfileContent>
          <h2>Elizabeth Curt</h2>
          <p>
            Come learn how to draw with Cd. You will learn different pencil
            stokes to create an awesome.
          </p>
          <Flex>
            {' '}
            <Rating
              emptySymbol={<img src={star1} alt="star1" className="starIcon" />}
              fullSymbol={<img src={star} alt="star" className="starIcon" />}
              className="star_Rating"
              initialRating="4"
              readonly="readonly"
            />
          </Flex>
          <ProfileList>
            <li>
              <span
                className="iconify"
                data-icon="octicon:location-24"
                data-inline="false"
              />
            </li>
            <li>
              <span
                className="iconify"
                data-icon="radix-icons:letter-case-capitalize"
                data-inline="false"
              />
            </li>
          </ProfileList>
        </ProfileContent>
      </Flex>
    </ProfileSection>
  );
}

Profile.propTypes = {};

export default Profile;
const ProfileSection = styled.div`
  background-color: #fff;
  padding: 150px;
`;

const ProfileImage = styled.div``;
const ProfileContent = styled.div``;
const ProfileList = styled.div``;
