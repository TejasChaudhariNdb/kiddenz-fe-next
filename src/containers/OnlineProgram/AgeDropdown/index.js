/* eslint-disable import/no-unresolved */
import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { useDropdownClose } from 'shared/hooks';
import styled from 'styled-components';
// import InputRange from 'react-input-range';
import colors from 'utils/colors';
// import Flex from '../common/flex';
import Button from 'components/Button';

function AgeDropdown({
  values,
  selected = {},
  placeHolder,
  sortType,
  onChange,
}) {
  const dropdownRef = useRef(null);
  const [status, setStatus] = useState(false);
  useDropdownClose(dropdownRef, setStatus);
  return (
    <DropdownWrapper column>
      <Button
        text={selected.name || placeHolder}
        type="dropdown"
        onClick={() => setStatus(!status)}
      />
      {status && (
        <DropdownMenu ref={dropdownRef}>
          {values.map(item => (
            <DropdownMenuItem
              key={item.id}
              className={sortType === item.key ? 'active' : ''}
              onClick={() => {
                onChange(item);
                setStatus(false);
              }}
            >
              {item.name}

              <span
                className="iconify"
                data-icon="mdi-light:check"
                data-inline="false"
              />
            </DropdownMenuItem>
          ))}
        </DropdownMenu>
      )}
    </DropdownWrapper>
  );
}

AgeDropdown.propTypes = {
  placeHolder: PropTypes.string,
  selected: PropTypes.string,
  onChange: PropTypes.func,
  sortType: PropTypes.string,
  values: PropTypes.array,
};

AgeDropdown.defaultProps = {
  placeHolder: 'Select',
};
export default AgeDropdown;

const DropdownWrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  z-index: 100;
  margin-top: 10px;
`;
const DropdownMenu = styled.ul`
  position: relative;
  top: 0;
  right: 0;
  width: 100px;
  padding: 5px;
  border-radius: 10px;
  background-color: #ffffff;
  box-shadow: 0 0 10px 0 rgb(0 0 0 / 10%);
  list-style-type: none;
  height: 250px;
  overflow-x: hidden;
  overflow-y: scroll;
  @media (max-width: 500px) {
    right: 0px;
    width: 100px;
    border-radius: 6px;
  }
`;
const DropdownMenuItem = styled.li`
  height: 40px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0px 10px;
  border-radius: 5px;
  color: ${colors.inputPlaceholder};
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 40px;
  cursor: pointer;
  list-style-type: none;
  @media (max-width: 500px) {
    // height: 24px;
    // font-size: 12px;
    // line-height: 24px;
  }
  .iconify {
    display: none;
    color: ${colors.secondary};
    font-size: 20px;
  }
  .dropdownArrow {
    transform: rotate(0deg);
    transition: transform 0.2s ease;
    &.active {
      transform: rotate(180deg);
      transition: transform 0.2s ease;
    }
  }
  &:hover {
    background-color: #ebf1fc;
    color: ${colors.secondary};
  }
  &.active {
    .iconify {
      display: inline-block;
    }
  }
`;
