/* eslint-disable react/jsx-key */
import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const DropDowns = ({
  margin,
  type,
  heading,
  placeHolder,
  width,
  onChange,
  value,
}) => {
  const ListItems = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
  const CategoryItems = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];

  return (
    <Dropdowns margin={margin} className={type} width={width}>
      {heading && <span className="heading">{heading}</span>}
      <div className="productCategory">
        <div className="productCategory__header">
          <input placeholder={placeHolder} onChange={onChange} value={value} />
        </div>
        {type === 'products' && (
          <div className="productCategory__dropdown">
            <div className="productCategory__dropdownList">
              <>
                {ListItems.map(d => (
                  // eslint-disable-next-line react/jsx-key
                  <li>{d}</li>
                ))}
              </>
            </div>
          </div>
        )}
        {type === 'category' && (
          <div className="productCategory__dropdown">
            <div className="productCategory__dropdownList">
              <>
                {CategoryItems.map(d => (
                  <li>{d}</li>
                ))}
              </>
            </div>
          </div>
        )}
      </div>
    </Dropdowns>
  );
};
DropDowns.propTypes = {
  margin: PropTypes.string,
  type: PropTypes.string,
  heading: PropTypes.string,
  placeHolder: PropTypes.string,
  width: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.string,
};
export default DropDowns;

const Dropdowns = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  margin: ${props => props.margin || '0px 0px 0px 0px;'};
  width: ${props => props.width || '100%'};

  .addMore {
    position: absolute;
    bottom: 15px;
    right: -30px;
    cursor: pointer;
    &:hover {
      .iconify {
        color: #108443;
      }
    }
    .iconify {
      width: 22px;
      height: 22px;
    }
  }
  &.products {
    width: 100%;
    .heading {
      font-weight: 500;
    }
    .productCategory {
      width: 100%;
    }
    .heading {
      font-size: 14px;
    }
    .productCategory__header {
      height: 50px;
      input {
        width: 100%;
        border: 0;
        outline: 0;
      }
    }
  }

  .heading {
    min-width: fit-content;
    font-family: Quicksand;
    font-weight: 500;
    font-size: 16px;
    line-height: 20px;
    color: #30333b;
    text-transform: capitalize;
    margin-bottom: 16px;
  }
  .lastUpdated {
    font-family: Poppins;
    font-weight: 400;
    font-size: 14px;
    line-height: 21px;
    color: #000000;
    opacity: 0.8;
    margin-top: 12px;
  }
  .productCategory {
    position: relative;
    margin-right: 2%;
    width: 100%;
    &__header {
      display: flex;
      width: 100%;
      justify-content: space-between;
      align-items: center;
      padding: 0px 12px;
      height: 44px;
      border: 1px solid #d9d9d9;
      background: #fff;
      border-radius: 5px;
      font-weight: 400;
      font-family: 'Roboto', sans-serif;
      font-size: 18px;
      line-height: 19px;
      color: rgba(0, 0, 0, 0.6);
      cursor: pointer;
      .iconify {
        margin-left: 16px;
        margin-top: 8px;
        width: 20px;
        height: 20px;
      }
      &.active {
        color: rgba(0, 0, 0, 1);
      }
    }
    &__dropdown {
      position: absolute;
      width: 200px;
      box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
      top: 51px;
      border-radius: 4px;
      height: 0px;
      max-height: 0px;
      overflow: hidden;
      transition: max-height 0.2s ease-in;
      background-color: #fff;
      z-index: 1;

      &.active,
      &:hover {
        max-height: 400px;
        height: auto;
        // transition: max-height 0.2s ease-;
        border: 1px solid #d9d9d9;
        border-top: 0px;
        border-bottom-left-radius: 5px;
        border-bottom-right-radius: 5px;
      }
    }
    &__dropdownList {
      li {
        font-family: Quicksand;
        font-weight: 400;
        font-size: 14px;
        line-height: 21px;
        color: #000000;
        opacity: 0.4;
        padding: 10px 12px;
        list-style-type: none;
        cursor: pointer;
        &:hover {
          background: #f3f3f3;
          opacity: 1;
        }
        @media screen and (max-width: 1650px) {
          font-size: 12px;
        }
      }
    }
  }
`;
