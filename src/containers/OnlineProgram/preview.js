/* eslint-disable no-lonely-if */
/* eslint-disable consistent-return */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/media-has-caption */
/* eslint-disable no-nested-ternary */
/* eslint-disable no-else-return */
/* eslint-disable indent */
/* eslint-disable array-callback-return */
/* eslint-disable no-param-reassign */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable react/prop-types */
import React, { useState, useEffect, useMemo, useRef } from 'react';
import styled from 'styled-components';
import moment from 'moment';
import Slider from 'react-slick';
import {
  useOnlineProgramDetailHook,
  useOnlineProgramPaymentHook,
  usePurchasedOnlineProgramHook,
  useOnlineProgramReviewHook,
  useBookmarkProgramHook,
} from 'shared/hooks';
import ReactPlayer from 'react-player';
import getAgeText from 'shared/utils/getAgeText';
import Header from 'components/Header';
import Footer from 'components/Footer';
import Modal from 'components/Modal';
import { useRouter } from 'next/router';
import MascotLoader from 'components/common/MascotLoader';
import ReactStars from 'react-rating-stars-component';
// import pulseLoader from '../../images/pulse_loader.gif';
import Flex from '../../components/common/flex';
import Ruppee from '../../components/common/Ruppee';
// import DropDown from './dropdowns';
import ClassCard from '../../components/ClassCard';
import Button from '../../components/Button/index';
import MainHeading from '../../components/MainHeading';
import previewImage from '../../images/previewImage.png';
import profile from '../../images/profile.jpg';
import profileImage from '../../images/Unisex.svg';
import InputBox from '../../components/InputBox';
// import user from '../../images/user.svg';
import star from '../../images/Symbols.png';
// import hour from '../../images/hour.png';
import website from '../../images/cake.svg';
import subCategory from '../../images/category.svg';
import category from '../../images/subCategory.svg';
import language from '../../images/language.svg';
import sessionImg from '../../images/no-of-session.svg';

const breakpoint = 979;

// import classes from '../../images/classes.png';
const getCostStyle = (cost = {}) => {
  let isSpaceBetween = true;
  const costs = [];
  if (Object.keys(cost).length) {
    if (cost.full_course_cost) {
      costs.push('full_course_cost');
    }
    if (cost.single_session_cost) {
      costs.push('single_session_cost');
    }
    if (cost.monthly_cost) {
      costs.push('monthly_cost');
    }
    if (cost.is_trial_available) {
      costs.push('trial_cost');
    }
  }
  if (costs.length < 3) {
    isSpaceBetween = false;
  }
  return { isSpaceBetween };
};

const CourseTypeConfig = [
  {
    label: 'Full Course',
    key: 'Full_Course',
    costKey: 'full_course_cost',
    dicountKey: 'full_course_discount',
  },
  {
    label: 'Single Session',
    key: 'Single_Session',
    costKey: 'single_session_cost',
    dicountKey: 'single_session_discount',
  },
  {
    label: 'Monthly',
    key: 'Monthly',
    costKey: 'monthly_cost',
    dicountKey: 'monthly_discount',
  },
  {
    label: 'Trial Class',
    key: 'Trial_Class',
    costKey: 'trial_cost',
    dicountKey: 'trial_discount',
  },
];

const ORDERS = {
  'program details': {
    order: 1,
  },
  'expected outcome': {
    order: 2,
  },
  'what do you need?': {
    order: 3,
  },
  'tool kit': {
    order: 4,
  },
  'about the instructor': {
    order: 5,
  },
  'cancellation policy': {
    order: 6,
  },
};

const orderRequirementsByName = (requirementInfo = []) => {
  let orderedList = [];
  if (requirementInfo.length) {
    orderedList = requirementInfo.map(req => ({
      ...req,
      order:
        (ORDERS[req.name] && ORDERS[req.name.toLowerCase()].order) || req.id,
    }));
  }
  const orderedReq = orderedList.sort((a, b) => (a.order > b.order && 1) || -1);
  return orderedReq;
};

const useViewport = () => {
  const [width, setWidth] = useState(null);

  const handleWindowResize = () => setWidth(window.innerWidth);

  useEffect(() => {
    setWidth(window.innerWidth);
  }, []);

  useEffect(() => {
    window.addEventListener('resize', handleWindowResize);
    return () => window.removeEventListener('resize', handleWindowResize);
  }, []);
  return { width };
};

function Preview(props) {
  const {
    authentication: {
      isLoggedIn,
      profile: {
        data: {
          name = '',
          mobile_number: contact = '',
          email = '',
          photo_url: photoUrl,
        } = {},
      } = {},
    },
    query: { id: programId } = {},
    successToast,
    errorToast,
    getData,
    dashboard: { ONLINE_PROGRAM_CALLBACK_REQUEST_API } = {},
  } = props;
  const videoRef = useRef();
  const router = useRouter();

  const { width } = useViewport();

  const [requirementDetails, setRequirementDetails] = useState([]);
  const [batches, setBatches] = useState([]);
  const [aboutEventOpen, setAboutEventOpen] = useState(true);
  const [reviewOpen, setReviewOpen] = useState(true);

  const [courseType, setCourseType] = useState('Full_Course');
  // const [modalActive, setModalActive] = useState(false);
  const [selectedBatch, setSelectedBatch] = useState(null);
  const [modalType, setModalType] = useState('login');
  const [modalStatus, setModalStatus] = useState(false);
  const [isPlaying, setIsPlaying] = useState(false);
  const [shareUrl, setShareUrl] = useState('');
  const callback = useMemo(
    () => getData(ONLINE_PROGRAM_CALLBACK_REQUEST_API, {}, false),
    [ONLINE_PROGRAM_CALLBACK_REQUEST_API],
  );

  const {
    programDetail: {
      data: programDetail = {},
      loader: detailLoader,
      // lastUpdated,
    },
    suggestedPrograms: {
      data: suggestedPrograms = [],
      loader: suggestedLoader,
    },
  } = useOnlineProgramDetailHook(props, { programId });

  const {
    // bookmarkedPrograms: {
    //   data: wishlistedPrograms = [],
    //   loader: wishlistLoader,
    // },
    bookmarkedItems,
    addBookmark,
    removeBookmark,
  } = useBookmarkProgramHook(props, {
    onBookmarkDelSuccess,
    onBookmarkAddSuccess,
    onBookmarkDelError,
    onBookmarkAddError,
  });
  const onBookmarkAddSuccess = ({ message }) => {
    successToast(message);
  };
  const onBookmarkDelSuccess = ({ message }) => {
    successToast(message);
  };
  const onBookmarkDelError = ({ message }) => errorToast(message);
  const onBookmarkAddError = ({ message }) => errorToast(message);

  const settings = {
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    beforeChange: () => {
      setIsPlaying(false);
    },
    // autoplay: false,
    // autoplaySpeed: 2000,
  };

  const settings1 = {
    dots: true,
    infinite: suggestedPrograms.length > 3,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    autoplay: true,
    autoplaySpeed: 5000,
    responsive: [
      {
        breakpoint: 978,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  };

  const {
    noOfSessions,
    onBuyNow,
    createProgram: { loader: createOrderLoader },
  } = useOnlineProgramPaymentHook(props, {
    programId,
    courseType,
    onCreateOrderSuccess,
    onCreateOrderError,
  });

  const {
    rating,
    description,
    postReview: { loader: postLoader },
    onReview,
    reviewsList: {
      data: reviews,
      // loader: reviewListLoader
    },
  } = useOnlineProgramReviewHook(props, {
    programId,
    onReviewSuccess,
    onReviewError,
  });

  function onReviewSuccess() {
    successToast('Your review successfully submitted');
  }
  function onReviewError() {}

  const {
    getPurchasedPrograms,
    purchasedPrograms: {
      data: purchasedPrograms = [],
      loader: purchasedLoader,
    },
  } = usePurchasedOnlineProgramHook(props);

  function onCreateOrderSuccess({ data: { data } = {} }) {
    if (!data.amount) {
      getPurchasedPrograms();
      if (courseType === 'Trial_Class') {
        setModalType('bookSuccess');
        setModalStatus(true);
      } else {
        successToast(
          `${
            courseType === 'Trial_Class' ? 'Trial Class' : ''
          } Booked successfully`,
        );
        closeHandler();
        router.push({
          pathname: `/payment-status`,
          query: { payment_id: data.payment_id },
        });
      }
    } else {
      const options = {
        key: 'rzp_test_jFHt0q9YaPyixx',
        name: 'Kiddenz',
        description: 'Online Program',
        image:
          'https://kiddenz-media-prod.s3.ap-south-1.amazonaws.com/kiddenz-purple+logo-6a687d0564d5c69c121bbe02a8b30117.svg',
        order_id: data.payment_id,
        // eslint-disable-next-line no-unused-vars
        handler: async response => {
          try {
            successToast('Payment successfully completed');
            getPurchasedPrograms();
            closeHandler();
            router.push({
              pathname: `/payment-status`,
              query: { payment_id: data.payment_id },
            });
            // const paymentId = response.razorpay_payment_id;
            // const url = `${API_URL}capture/${paymentId}`;
            // const captureResponse = await Axios.post(url, {});
          } catch (err) {
            errorToast('Something went wrong');
            console.log(err, 'razor');
          }
        },
        modal: {
          ondismiss: () => {
            setModalType('onlineProgram');
            setModalStatus(true);
          },
        },
        readonly: {
          email: false,
          contact: true,
        },
        prefill: {
          name,
          email,
          contact,
        },
        notes: {
          address: 'Razorpay Corporate Office',
        },
        theme: {
          color: '#3399cc',
        },
      };
      const rzp1 = new window.Razorpay(options);
      rzp1.open();
      setModalStatus(false);
    }
  }
  function onCreateOrderError() {}

  useEffect(
    () => {
      if (programDetail && programDetail.requirement_details) {
        const reqData = [...programDetail.requirement_details];
        const req = [];
        reqData.map(a => {
          a.open = true;
          req.push(a);
        });
        setRequirementDetails(reqData);
      }
    },
    [programDetail && programDetail.requirement_details],
  );
  useEffect(
    () => {
      if (
        programDetail &&
        programDetail.batches &&
        programDetail.batches.length
      ) {
        const batchesData = [...programDetail.batches];
        const batchesList = [];
        const sortedBatches = batchesData.sort(
          (x, y) => (x.can_buy === y.can_buy ? 0 : x.can_buy ? -1 : 1),
        );
        sortedBatches.map((a, index) => {
          a.open = !index && a.can_buy;
          batchesList.push(a);
        });
        setBatches(sortedBatches);
      }
    },
    [programDetail && programDetail.batches],
  );

  useEffect(
    () => {
      if (courseType) {
        if (batches.length) {
          const batchesList = [...batches];
          const isExist = batchesList.find(
            batch => batch.batch_type === courseType && batch.can_buy,
          );
          batchesList.map(a => {
            a.open = isExist && isExist.id === a.id && a.can_buy;
          });
          setBatches(batchesList);
        }
      }
    },
    [courseType, batches.length],
  );

  const toggleRequirements = id => {
    const reqirements = [...requirementDetails];
    reqirements.map(a => {
      if (a.id === id) {
        a.open = !a.open;
      }
    });
    setRequirementDetails(reqirements);
  };

  const toggleBatches = id => {
    const batch = [...batches];
    batch.map(a => {
      if (a.id === id) {
        a.open = !a.open;
      }
    });
    setBatches(batch);
  };

  useEffect(
    () => {
      if (
        // programDetail &&
        // programDetail.program_cost &&
        // programDetail.program_cost.full_course_cost &&
        programDetail &&
        programDetail.batches &&
        programDetail.batches.length &&
        programDetail.batches.find(
          batch =>
            batch.batch_type === 'Full_Course' ||
            batch.batch_type === 'Home_School',
        )
      ) {
        setCourseType('Full_Course');
      } else if (
        programDetail &&
        programDetail.batches &&
        programDetail.batches.length &&
        programDetail.batches.find(
          batch => batch.batch_type === 'Single_Session',
        )
      ) {
        setCourseType('Single_Session');
      } else if (
        programDetail &&
        programDetail.batches &&
        programDetail.batches.length &&
        programDetail.batches.find(batch => batch.batch_type === 'Monthly')
      ) {
        setCourseType('Monthly');
      } else if (
        programDetail &&
        programDetail.batches &&
        programDetail.batches.length &&
        programDetail.batches.find(batch => batch.batch_type === 'Trial_Class')
      ) {
        setCourseType('Trial_Class');
      }
    },
    [programDetail && programDetail.batches && programDetail.batches.length],
  );

  const handleCourseType = type => {
    // const batch = [...batches];
    if (type !== courseType) {
      setCourseType(type);
      setSelectedBatch(null);
      if (type === 'Trial_Class' || type === 'Single_Session') {
        const selected = batches.find(batch => batch.batch_type === type);
        setSelectedBatch(selected);
      } else if (
        type !== 'Trial_Class' &&
        type !== 'Single_Session' &&
        programDetail.program_detail &&
        programDetail.program_detail.online_tags === 'home_school'
      ) {
        const selected = batches.find(
          batch => batch.batch_type === 'Home_School',
        );
        setSelectedBatch(selected);
      }
    }
  };

  const handleSelectBatch = batch => {
    if (batch) {
      setSelectedBatch(batch);
    }
    // setModalActive(true);
    if (programId) {
      let queryParam = {
        id: programId,
      };

      if (batch && batch.id) {
        queryParam = {
          ...queryParam,
          selectedCourse: batch && batch.id,
        };
      }

      const url = {
        pathname: '/online-program/[program-name]',
        query: queryParam,
      };
      const urlAs = {
        pathname: `/online-program/${programDetail.program_detail.name
          .replace(/\s+/g, '-')
          .replace('/', '-')
          .toLowerCase()}`,
        query: queryParam,
      };
      router
        .push(url, urlAs, { shallow: true })
        .then(() => window.scrollTo(0, 0));
    }
  };

  const handleRequestCallBack = () => {
    if (!isLoggedIn) {
      setModalType('login');
      setModalStatus(true);
      // setModalActive(true);
    } else {
      // setModalType('callback');
      // setModalStatus(true);
      props.ONLINE_PROGRAM_CALLBACK_REQUEST_API_CALL({
        payload: {
          online_program: programId,
          enquire: 'Request for callback',
        },
        errorCallback: ({ message }) => {
          errorToast(message || 'Something Went wrong');
        },
        successCallback: () => {
          setModalType('callback');
          setModalStatus(true);
        },
      });
      // setModalActive(true);
    }
  };

  const handleBuyNow = () => {
    if (!isLoggedIn) {
      setModalType('signup');
      setModalStatus(true);
    } else if (
      courseType !== 'Trial_Class' &&
      courseType !== 'Single_Session' &&
      programDetail.program_detail &&
      programDetail.program_detail.online_tags === 'home_school'
    ) {
      const selected = batches.find(
        batch => batch.batch_type === 'Home_School',
      );
      onBuyNow(courseType, selected);
    } else if (
      courseType !== 'Trial_Class' &&
      courseType !== 'Single_Session' &&
      (programDetail.program_detail &&
        programDetail.program_detail.online_tags !== 'home_school') &&
      selectedBatch &&
      selectedBatch.id
    ) {
      onBuyNow(courseType, selectedBatch);
      // errorToast('Kindly seelct the session')
    } else if (
      courseType !== 'Trial_Class' &&
      courseType !== 'Single_Session' &&
      !(selectedBatch && selectedBatch.id)
    ) {
      errorToast('Kindly select the session');
    } else if (
      courseType === 'Trial_Class' ||
      courseType === 'Single_Session'
    ) {
      const selected = batches.find(batch => batch.batch_type === courseType);
      onBuyNow(courseType, selected);
    }
  };

  // const handleSeeAllSession = () => {
  //   router.push({
  //     pathname: `/online-program/${programDetail.program_detail.name
  //       .replace(/\s+/g, '-')
  //       .replace('/', '-')
  //       .toLowerCase()}/sessions`,
  //     query: { id: programDetail.program_detail.id },
  //   });
  // };

  const closeHandler = () => {
    setModalStatus(false);
    if (programId) {
      const query = { id: programId };
      const url = { pathname: '/online-program/[program-name]', query };
      const urlAs = {
        pathname: `/online-program/${programDetail.program_detail.name
          .replace(/\s+/g, '-')
          .replace('/', '-')
          .toLowerCase()}`,
        query,
      };
      router
        .replace(url, urlAs, { shallow: true })
        .then(() => window.scrollTo(0, 0));
    }
    // setModalActive(false);
  };

  const handleBookMark = id => {
    if (!isLoggedIn) {
      setModalType('login');
      setModalStatus(true);
      // setModalActive(true);
    } else {
      addBookmark(id);
    }
  };

  const handleToggleBookMark = () => {
    if (!isLoggedIn) {
      setModalType('login');
      setModalStatus(true);
      // setModalActive(true);
    } else {
      if (bookmarkedItems.includes(+programId)) {
        removeBookmark(programId, true);
      } else {
        addBookmark(programId, true);
      }
    }
  };

  const handleDeleteBookMark = id => {
    removeBookmark(id);
  };

  // const handleCardClick = (program, e) => {
  //   e.preventDefault();
  //   if (program.id) {
  //     const query = { id: program.id };
  //     const url = { pathname: '/online-program/[program-name]', query };
  //     const urlAs = {
  //       pathname: `/online-program/${program.name
  //         .replace(/\s+/g, '-')
  //         .replace('/', '-')
  //         .toLowerCase()}`,
  //       query,
  //     };
  //     router.push(url, urlAs).then(() => window.scrollTo(0, 0));
  //   }
  // };
  useEffect(
    () => {
      if (router.query.selectedCourse && batches.length) {
        setModalType('onlineProgram');
        setModalStatus(true);
        const selected = batches.find(
          batch => +batch.id === +router.query.selectedCourse,
        );
        setSelectedBatch(selected);
      } else {
        setModalType(null);
        setModalStatus(false);
        setSelectedBatch(null);
      }
    },
    [router.query.selectedCourse, batches && batches.length],
  );

  const handleViewAll = () => {
    router.push({
      pathname: `/online-program/search`,
      query: {},
    });
  };

  // const onAuthSuccess = () => {};
  const { isSpaceBetween } = getCostStyle(
    programDetail && programDetail.program_cost,
  );

  const handleShareProgram = () => {
    setModalStatus(true);
    setModalType('shareModal');
    setShareUrl(
      `https://www.kiddenz.com/online-program/${programDetail.program_detail.name
        .replace(/\s+/g, '-')
        .replace('/', '-')
        .toLowerCase()}?id=${programId}`,
    );
  };

  const orderedRequirements = orderRequirementsByName(requirementDetails);
  return (
    <OnlinePreview>
      <Header
        {...props}
        type="articles"
        placeHolderValue="Search Article"
        isModalActive={modalStatus}
        activeModalType={modalType}
        setActiveCallback={setModalStatus}
      />
      {purchasedLoader || createOrderLoader || detailLoader ? (
        <MascotLoader />
      ) : (
        <>
          <PreviewSection>
            <Flex
              justifyBetween
              flexBorderdashed="1px solid #E6E8EC"
              flexPadding="0px 0px 64px"
              className="flexChange"
            >
              <PreviewSectionLeft>
                <div className="preview-image">
                  {!width || width >= breakpoint ? (
                    <Slider {...settings}>
                      {programDetail &&
                      programDetail.program_detail &&
                      programDetail.program_detail.medias &&
                      programDetail.program_detail.medias.length ? (
                        programDetail.program_detail.medias.map(a => {
                          if (a.media_type === 'image')
                            return <img src={a.media_url} alt="media" />;
                          else if (a.media_type === 'video')
                            return (
                              <>
                                {a.media_url ? (
                                  <ReactPlayer
                                    ref={videoRef}
                                    playing={isPlaying}
                                    onPlay={() => setIsPlaying(true)}
                                    onStart={() => setIsPlaying(true)}
                                    onPause={() => setIsPlaying(false)}
                                    onEnded={() => setIsPlaying(false)}
                                    url={a.media_url}
                                    width="100%"
                                    height="100%"
                                    controls
                                  />
                                ) : a.youtube_url ? (
                                  <ReactPlayer
                                    title="youtube"
                                    width="100%"
                                    height="100%"
                                    url={`${
                                      a.youtube_url
                                    }?showinfo=0&enablejsapi=1&origin=http://localhost:9000`}
                                    frameBorder="0"
                                    // allow="picture-in-picture"
                                    allowFullScreen
                                    ref={videoRef}
                                    playing={isPlaying}
                                    onPlay={() => setIsPlaying(true)}
                                    onStart={() => setIsPlaying(true)}
                                    onPause={() => setIsPlaying(false)}
                                    onEnded={() => setIsPlaying(false)}
                                    controls
                                  />
                                ) : null}
                              </>
                            );
                        })
                      ) : (
                        <img src={previewImage} alt="" />
                      )}
                    </Slider>
                  ) : null}
                  {/* <img src={previewImage} alt="" /> */}
                </div>
                <ul className="preview-list">
                  {programDetail.program_detail &&
                    programDetail.program_detail.about_event && (
                      <li
                        className={`preview-item ${
                          aboutEventOpen ? 'active' : ''
                        }`}
                        onClick={() => setAboutEventOpen(!aboutEventOpen)}
                      >
                        <Flex column flexWidth="95%">
                          <h2>About the Event</h2>
                          <p>{programDetail.program_detail.about_event} </p>
                        </Flex>
                        <span
                          onClick={() => setAboutEventOpen(!aboutEventOpen)}
                          style={{ cursor: 'pointer' }}
                        >
                          {aboutEventOpen ? (
                            <i className="fa fa-minus" aria-hidden="true" />
                          ) : (
                            <i className="fa fa-plus" aria-hidden="true" />
                          )}
                        </span>
                      </li>
                    )}
                  {requirementDetails.length ? (
                    <>
                      {orderedRequirements.length
                        ? orderedRequirements.map(requirement => (
                            <>
                              {requirement.name !== 'certificate' &&
                              requirement.description &&
                              requirement.description.length ? (
                                <li
                                  className={`preview-item ${requirement.open &&
                                    'active'}`}
                                  onClick={e => {
                                    e.preventDefault();
                                    e.stopPropagation();
                                    toggleRequirements(requirement.id);
                                  }}
                                >
                                  <Flex column flexWidth="95%">
                                    <h2>
                                      {requirement.name === 'Iternary'
                                        ? 'Itinerary'
                                        : requirement.name === 'Pre Requisite'
                                          ? 'Pre-Requisite'
                                          : requirement.name}
                                    </h2>
                                    <p>{requirement.description}</p>
                                  </Flex>
                                  <span
                                    onClick={e => {
                                      e.preventDefault();
                                      e.stopPropagation();
                                      toggleRequirements(requirement.id);
                                    }}
                                    style={{ cursor: 'pointer' }}
                                  >
                                    {requirement.open ? (
                                      <i
                                        className="fa fa-minus"
                                        aria-hidden="true"
                                      />
                                    ) : (
                                      <i
                                        className="fa fa-plus"
                                        aria-hidden="true"
                                      />
                                    )}
                                  </span>
                                </li>
                              ) : null}
                            </>
                          ))
                        : null}
                      {requirementDetails.map(requirement => (
                        <>
                          {requirement.name === 'certificate' &&
                          requirement.certificate ? (
                            <li
                              className={`preview-item ${requirement.open &&
                                'active'}`}
                              onClick={e => {
                                e.preventDefault();
                                e.stopPropagation();
                                toggleRequirements(requirement.id);
                              }}
                            >
                              <Flex column flexWidth="95%">
                                <h2>certificate</h2>
                                <p>Yes, certificate will be provided</p>
                              </Flex>
                              <span
                                onClick={e => {
                                  e.preventDefault();
                                  e.stopPropagation();
                                  toggleRequirements(requirement.id);
                                }}
                                style={{ cursor: 'pointer' }}
                              >
                                {requirement.open ? (
                                  <i
                                    className="fa fa-minus"
                                    aria-hidden="true"
                                  />
                                ) : (
                                  <i
                                    className="fa fa-plus"
                                    aria-hidden="true"
                                  />
                                )}
                              </span>
                            </li>
                          ) : null}
                        </>
                      ))}
                    </>
                  ) : null}
                  <li
                    className={`preview-item ${reviewOpen ? 'active' : ''}`}
                    onClick={() => setReviewOpen(!reviewOpen)}
                  >
                    {/* <div className="commentSection"> */}
                    <Flex column flexWidth="95%" className="writeReview">
                      <h2>Reviews</h2>
                      <div className="review-section">
                        {!reviews.length ? (
                          <div className="reviewWrapper">
                            <p>No Reviews</p>
                          </div>
                        ) : null}
                        {isLoggedIn &&
                          programDetail.can_review &&
                          !programDetail.is_reviewed && (
                            <>
                              <h3>Write your review</h3>
                              <p>
                                Did your child attend this program? Write a
                                review to help other parents learn about this
                                program
                              </p>
                              <div
                                className="star"
                                onClick={e => {
                                  e.preventDefault();
                                  e.stopPropagation();
                                }}
                              >
                                <div className="left">
                                  <img src={photoUrl || profile} alt="User" />
                                </div>
                                <div className="right">
                                  <p>Overall Rating</p>
                                  <ReactStars
                                    count={5}
                                    value={rating.value}
                                    onChange={rating.onChange}
                                    size={24}
                                    isHalf
                                    emptyIcon={<i className="far fa-star" />}
                                    halfIcon={
                                      <i className="fa fa-star-half-alt" />
                                    }
                                    fullIcon={<i className="fa fa-star" />}
                                    activeColor="#ffd700"
                                  />
                                  <Error>{rating.error}</Error>
                                </div>
                              </div>

                              <div
                                className="reviewForm"
                                onClick={e => {
                                  e.preventDefault();
                                  e.stopPropagation();
                                }}
                              >
                                <InputBox
                                  placeholder="Write your review"
                                  type="textarea"
                                  value={description.value}
                                  onChange={description.onChange}
                                  onBlur={description.onBlur}
                                />
                                <Error>{description.error}</Error>
                                {/* <textarea placeholder="Write your review" /> */}
                                <Button
                                  onClick={onReview}
                                  isLoading={postLoader}
                                  text="Post"
                                  type="mobile"
                                />
                              </div>
                            </>
                          )}

                        {reviews.length ? (
                          <div>
                            <Flex
                              column
                              flexWidth="100%"
                              className="writeReview postedRatingWrapper"
                            >
                              {/* <h3>Reviews</h3> */}
                              {reviews.map(review => (
                                <div className="reviewWrapper">
                                  <div className="star">
                                    <div className="left">
                                      <img
                                        src={review.user_photo || profile}
                                        alt="User"
                                      />
                                    </div>
                                    <div className="right">
                                      <h2>{review.user_name}</h2>
                                      <div className="postedRating">
                                        <ReactStars
                                          count={5}
                                          size={24}
                                          value={review.rating}
                                          isHalf
                                          edit={false}
                                          emptyIcon={
                                            <i className="far fa-star" />
                                          }
                                          halfIcon={
                                            <i className="fa fa-star-half-alt" />
                                          }
                                          fullIcon={
                                            <i className="fa fa-star" />
                                          }
                                          activeColor="#ffd700"
                                        />
                                        {review.created_on && (
                                          <div className="postDate">
                                            <span>
                                              {moment(review.created_on).format(
                                                'DD MMMM YYYY | H:mA',
                                              )}
                                            </span>
                                          </div>
                                        )}
                                      </div>
                                    </div>
                                  </div>
                                  <p>{review.review}</p>
                                </div>
                              ))}
                            </Flex>
                          </div>
                        ) : null}
                      </div>
                    </Flex>
                    {/* </div> */}
                    <span
                      onClick={() => setReviewOpen(!reviewOpen)}
                      style={{ cursor: 'pointer' }}
                    >
                      {reviewOpen ? (
                        <i className="fa fa-minus" aria-hidden="true" />
                      ) : (
                        <i className="fa fa-plus" aria-hidden="true" />
                      )}
                    </span>
                  </li>
                </ul>
              </PreviewSectionLeft>
              <PreviewSectionRight>
                <Share>
                  <li onClick={handleToggleBookMark}>
                    <div
                      className={
                        !bookmarkedItems.includes(+programId) ? 'hide-icon' : ''
                      }
                    >
                      <span
                        className="iconify"
                        data-icon="clarity:heart-solid"
                        data-inline="false"
                      />
                      Saved
                    </div>
                    <div
                      className={
                        bookmarkedItems.includes(+programId) ? 'hide-icon' : ''
                      }
                    >
                      <span
                        className="iconify"
                        data-icon="clarity:heart-line"
                        data-inline="false"
                      />
                      Save
                    </div>
                  </li>
                  <li onClick={handleShareProgram}>
                    <span
                      className="iconify"
                      data-icon="clarity:share-line"
                      data-inline="false"
                    />
                    Share
                  </li>
                </Share>
                <Sponsored>
                  {programDetail.program_detail &&
                    programDetail.program_detail.is_sponsored && (
                      <span>Sponsored</span>
                    )}
                  {programDetail.program_detail &&
                    programDetail.program_detail.online_tags !==
                      'home_school' &&
                    programDetail.program_detail &&
                    programDetail.program_detail.school_choice &&
                    programDetail.program_detail.school_choice !== 'none' && (
                      <span className="school-choice">
                        {programDetail.program_detail.school_choice === 'both'
                          ? `Online & Offline`
                          : programDetail.program_detail.school_choice}
                      </span>
                    )}
                </Sponsored>
                {programDetail.program_detail &&
                  programDetail.program_detail.name && (
                    <h1>{programDetail.program_detail.name}</h1>
                  )}
                <ProfileDetail>
                  <div className="profile-image">
                    {' '}
                    <img src={profileImage} alt="" />
                  </div>
                  {programDetail.provider_name && (
                    <div className="profile-name">
                      By {programDetail.provider_name}
                    </div>
                  )}
                  {rating.rating__avg && (
                    <div className="profile-rating">
                      <img src={star} alt="" />
                      {rating.rating__avg}
                    </div>
                  )}
                </ProfileDetail>
                <div className="preview-image tabView">
                  {width && width < breakpoint ? (
                    <Slider {...settings}>
                      {programDetail &&
                      programDetail.program_detail &&
                      programDetail.program_detail.medias &&
                      programDetail.program_detail.medias.length ? (
                        programDetail.program_detail.medias.map(a => {
                          if (a.media_type === 'image')
                            return <img src={a.media_url} alt="media" />;
                          else if (a.media_type === 'video')
                            return (
                              <>
                                {a.media_url ? (
                                  <ReactPlayer
                                    ref={videoRef}
                                    playing={isPlaying}
                                    onPlay={() => setIsPlaying(true)}
                                    onStart={() => setIsPlaying(true)}
                                    onPause={() => setIsPlaying(false)}
                                    onEnded={() => setIsPlaying(false)}
                                    url={a.media_url}
                                    width="100%"
                                    height="100%"
                                    controls
                                  />
                                ) : a.youtube_url ? (
                                  <ReactPlayer
                                    title="youtube"
                                    width="100%"
                                    height="100%"
                                    url={a.youtube_url}
                                    frameBorder="0"
                                    // allow="picture-in-picture"
                                    allowFullScreen
                                    ref={videoRef}
                                    playing={isPlaying}
                                    onPlay={() => setIsPlaying(true)}
                                    onStart={() => setIsPlaying(true)}
                                    onPause={() => setIsPlaying(false)}
                                    onEnded={() => setIsPlaying(false)}
                                    controls
                                  />
                                ) : null}
                              </>
                            );
                        })
                      ) : (
                        <img src={previewImage} alt="" />
                      )}
                    </Slider>
                  ) : null}
                  {/* <img src={previewImage} alt="" /> */}
                </div>
                {/* <DropDown type="products" placeHolder="Beginner Level" /> */}

                <ProviderInfo>
                  {/* <li>
                  <img src={classes} alt="" />
                  <span>7 classes</span>
                </li> */}
                  {/* <li>
              <img src={hour} alt="" />1 hour classes
            </li> */}
                  {programDetail.program_detail &&
                    programDetail.program_detail.start_age && (
                      <li>
                        <img
                          src={website}
                          alt=""
                          width="20px"
                          height="20px"
                          style={{ marginBottom: '5px' }}
                        />
                        Age:{' '}
                        {getAgeText(
                          programDetail.program_detail &&
                            programDetail.program_detail.start_age,
                          programDetail.program_detail &&
                            programDetail.program_detail.end_age,
                        )}
                      </li>
                    )}
                  {/* {programDetail.program_detail &&
                programDetail.program_detail.category_list &&
                programDetail.program_detail.category_list.length ? (
                  <li>
                    {programDetail.program_detail.category_list.map(
                      (cat, index) => (
                        <>
                          {!index && cat.p_name && <img src={mobile} alt="" />}
                          {cat.p_name}
                        </>
                      ),
                    )}
                  </li>
                ) : null} */}
                  {programDetail.program_detail &&
                  programDetail.program_detail.category_list &&
                  programDetail.program_detail.category_list.length ? (
                    <li>
                      <img src={category} alt="" width="20px" height="20px" />

                      {programDetail.program_detail.category_list
                        .map(cat => cat.p_name)
                        .join(', ')}
                    </li>
                  ) : null}
                  {programDetail.program_detail &&
                  programDetail.program_detail.subcategory_list &&
                  programDetail.program_detail.subcategory_list.length ? (
                    <li>
                      <img
                        src={subCategory}
                        alt=""
                        width="20px"
                        height="20px"
                      />

                      {programDetail.program_detail.subcategory_list
                        .map(sub => sub.name)
                        .join(', ')}
                    </li>
                  ) : null}
                  {programDetail.program_detail &&
                    programDetail.program_detail.language && (
                      <li>
                        <img src={language} alt="" width="20px" height="20px" />
                        {programDetail.program_detail.language}
                      </li>
                    )}
                  {batches &&
                  batches.length &&
                  batches[0] &&
                  batches[0].no_of_seats ? (
                    <li>
                      <img src={sessionImg} alt="" width="20px" height="20px" />
                      {batches[0].no_of_seats}{' '}
                      {batches[0].no_of_seats > 1 ? 'sessions' : 'session'}
                    </li>
                  ) : null}
                </ProviderInfo>

                {programDetail &&
                programDetail.batches &&
                programDetail.batches.length ? (
                  <>
                    <Flex
                      justifyBetween={isSpaceBetween}
                      flexWidth="100%"
                      flexMargin="0px 0px 26px"
                      className="priceOffered"
                    >
                      {// programDetail.program_cost &&
                      // programDetail.program_cost.full_course_cost
                      programDetail.batches.find(
                        batch =>
                          batch.batch_type === 'Full_Course' ||
                          batch.batch_type === 'Home_School',
                      ) ? (
                        <PriceOffered
                          onClick={() => handleCourseType('Full_Course')}
                          className={courseType === 'Full_Course' && 'active'}
                          marginRight={!isSpaceBetween && '10px'}
                        >
                          <h2>
                            <Ruppee className="ruppee">₹</Ruppee>
                            {programDetail.program_cost.full_course_cost &&
                            programDetail.program_cost.full_course_discount
                              ? Math.floor(
                                  programDetail.program_cost.full_course_cost -
                                    programDetail.program_cost
                                      .full_course_discount,
                                )
                              : programDetail.program_cost.full_course_cost}
                          </h2>
                          {programDetail.program_cost.full_course_cost ? (
                            <>
                              {programDetail.program_cost
                                .full_course_discount ? (
                                <h5>
                                  <span>
                                    <Ruppee className="ruppee">₹</Ruppee>
                                    {Math.floor(
                                      programDetail.program_cost
                                        .full_course_cost,
                                    )}
                                  </span>{' '}
                                  <Ruppee className="ruppee">₹</Ruppee>{' '}
                                  {programDetail.program_cost
                                    .full_course_discount || 0}{' '}
                                  Off
                                </h5>
                              ) : null}
                            </>
                          ) : null}
                          <h4>
                            {programDetail.program_detail &&
                            programDetail.program_detail.online_tags ===
                              'home_school'
                              ? 'Home School'
                              : 'Full course '}
                          </h4>
                        </PriceOffered>
                      ) : null}

                      {// programDetail.program_cost &&
                      // programDetail.program_cost.single_session_cost
                      programDetail.batches.find(
                        batch => batch.batch_type === 'Single_Session',
                      ) ? (
                        <PriceOffered
                          onClick={() => handleCourseType('Single_Session')}
                          className={
                            courseType === 'Single_Session' && 'active'
                          }
                          marginRight={!isSpaceBetween && '10px'}
                        >
                          <h2>
                            <Ruppee className="ruppee">₹</Ruppee>
                            {programDetail.program_cost.single_session_cost &&
                            programDetail.program_cost.single_session_discount
                              ? Math.floor(
                                  programDetail.program_cost
                                    .single_session_cost -
                                    programDetail.program_cost
                                      .single_session_discount,
                                )
                              : programDetail.program_cost.single_session_cost}
                          </h2>
                          {programDetail.program_cost.single_session_cost ? (
                            <>
                              {programDetail.program_cost
                                .single_session_discount ? (
                                <h5>
                                  <span>
                                    <Ruppee className="ruppee">₹</Ruppee>
                                    {Math.floor(
                                      programDetail.program_cost
                                        .single_session_cost,
                                    )}
                                  </span>{' '}
                                  <Ruppee className="ruppee">₹</Ruppee>{' '}
                                  {programDetail.program_cost
                                    .single_session_discount || 0}{' '}
                                  Off
                                </h5>
                              ) : null}
                            </>
                          ) : null}
                          <h4>Single Session </h4>
                        </PriceOffered>
                      ) : null}

                      {// programDetail.program_cost &&
                      // programDetail.program_cost.monthly_cost
                      programDetail.batches.find(
                        batch => batch.batch_type === 'Monthly',
                      ) ? (
                        <PriceOffered
                          onClick={() => handleCourseType('Monthly')}
                          className={courseType === 'Monthly' && 'active'}
                          marginRight={!isSpaceBetween && '10px'}
                        >
                          <h2>
                            <Ruppee className="ruppee">₹</Ruppee>
                            {programDetail.program_cost.monthly_cost &&
                            programDetail.program_cost.monthly_discount
                              ? Math.floor(
                                  programDetail.program_cost.monthly_cost -
                                    programDetail.program_cost.monthly_discount,
                                )
                              : programDetail.program_cost.monthly_cost}
                          </h2>
                          {programDetail.program_cost.monthly_cost ? (
                            <>
                              {programDetail.program_cost.monthly_discount ? (
                                <h5>
                                  <span>
                                    <Ruppee className="ruppee">₹</Ruppee>
                                    {Math.floor(
                                      programDetail.program_cost.monthly_cost,
                                    )}
                                  </span>{' '}
                                  <Ruppee className="ruppee">₹</Ruppee>{' '}
                                  {programDetail.program_cost
                                    .monthly_discount || 0}{' '}
                                  Off
                                </h5>
                              ) : null}
                            </>
                          ) : null}
                          <h4>Monthly cost </h4>
                        </PriceOffered>
                      ) : null}

                      {// programDetail.program_cost &&
                      // programDetail.program_cost.is_trial_available &&
                      // (programDetail.program_cost.trial_cost ||
                      //   +programDetail.program_cost.trial_cost === 0)
                      programDetail.batches.find(
                        batch => batch.batch_type === 'Trial_Class',
                      ) ? (
                        <PriceOffered
                          onClick={() => handleCourseType('Trial_Class')}
                          className={courseType === 'Trial_Class' && 'active'}
                          marginRight={!isSpaceBetween && '10px'}
                        >
                          <h2>
                            <Ruppee className="ruppee">₹</Ruppee>{' '}
                            {programDetail.program_cost.trial_cost}
                          </h2>
                          {/* {programDetail.program_cost.trial_cost ? (
                      <>
                        {programDetail.program_cost.discount ? (
                          <h5>
                            <span>
                              <Ruppee className="ruppee">₹</Ruppee>
                              {Math.floor(
                                programDetail.program_cost.trial_cost
                              )}
                            </span>{" "}
                            {programDetail.program_cost.discount || 0}% Off
                          </h5>
                        ) : null}
                      </>
                    ) : null} */}
                          <h4>Trial cost </h4>
                        </PriceOffered>
                      ) : null}
                    </Flex>
                    {programDetail.program_cost.money_back_gurantee ? (
                      <p>* Money back guarantee available</p>
                    ) : null}
                  </>
                ) : null}

                {courseType === 'Single_Session' && (
                  <Inputfield>
                    <h3>Select Sessions</h3>
                    <h4>Select Number of sessions</h4>
                    <InputBoxSection className={noOfSessions.error && 'error'}>
                      <input
                        placeholder="No. of sessions"
                        onChange={noOfSessions.onChange}
                        onBlur={noOfSessions.onBlur}
                        value={noOfSessions.value}
                        type="number"
                        min="0"
                      />
                    </InputBoxSection>
                    {noOfSessions.error && (
                      <div className="error">{noOfSessions.error}</div>
                    )}
                  </Inputfield>
                )}

                {programDetail.program_detail &&
                programDetail.program_detail.online_tags !== 'home_school' &&
                courseType !== 'Trial_Class' &&
                courseType !== 'Single_Session' &&
                batches &&
                batches.length
                  ? batches
                      .filter(batch => batch.batch_type === courseType)
                      .map(
                        a =>
                          a.heading_name && (
                            <>
                              <BatchDetail className={a.open && 'active'}>
                                <div className="batch-header">
                                  <Flex column>
                                    <h4>{a.heading_name}</h4>
                                    <div className="dates">
                                      {a.start_date && a.end_date ? (
                                        <>
                                          <span>
                                            {moment(a.start_date).format(
                                              'DD MMM YY',
                                            )}
                                          </span>{' '}
                                          <span style={{ margin: '0 5px' }}>
                                            to
                                          </span>
                                          <span>
                                            {moment(a.end_date).format(
                                              'DD MMM YY',
                                            )}
                                          </span>
                                        </>
                                      ) : (
                                        <span>{a.till_date}</span>
                                      )}
                                    </div>
                                    <div className="batchSeats">
                                      {a.no_of_seats && (
                                        <p>
                                          No of sessions: {`${a.no_of_seats}`}
                                        </p>
                                      )}{' '}
                                      |{' '}
                                      {a.duration && (
                                        <p className="duration-section">
                                          {' '}
                                          Duration: {`${a.duration}`}
                                        </p>
                                      )}
                                    </div>
                                  </Flex>
                                  <Flex
                                    alignCenter
                                    onClick={() => toggleBatches(a.id)}
                                  >
                                    {/* <span style={{ marginRight: '10px' }}>
                                  {a.open ? 'Hide Schedule' : 'See Schedule'}
                                </span> */}
                                    <div
                                      className={a.open ? 'hide-icon' : ''}
                                      style={{ cursor: 'pointer' }}
                                    >
                                      <div className="btn">
                                        <Button
                                          text={
                                            a.can_buy ? 'Buy Now' : 'Expired'
                                          }
                                          disabled={!a.can_buy}
                                          type={
                                            a.can_buy
                                              ? 'primary'
                                              : 'primary Expired'
                                          }
                                          height="50px"
                                          width="100%"
                                          // marginRight="auto"
                                          marginTop="20px"
                                          onClick={() =>
                                            a.can_buy
                                              ? handleSelectBatch(
                                                  selectedBatch &&
                                                  selectedBatch.id === a.id
                                                    ? null
                                                    : a,
                                                )
                                              : null
                                          }
                                          isLoading={
                                            purchasedLoader || createOrderLoader
                                          }
                                        />
                                        <i
                                          className="fa fa-plus"
                                          aria-hidden="true"
                                        />
                                      </div>
                                    </div>
                                    <div
                                      className={!a.open ? 'hide-icon' : ''}
                                      style={{ cursor: 'pointer' }}
                                    >
                                      <i
                                        className="fa fa-minus"
                                        aria-hidden="true"
                                      />
                                    </div>
                                  </Flex>
                                </div>
                                {a.batch_slots && a.batch_slots.length ? (
                                  <ul className="batch-dateList">
                                    {a.batch_slots.map(d => (
                                      <li className="batch-dateItem">
                                        <Flex column>
                                          <p>
                                            {moment(d.selected_date).format(
                                              'DD MMM',
                                            )}
                                          </p>
                                          {/* <span>Mon-thu</span> */}
                                        </Flex>
                                        {/* <div className="batch-time">
                                  {d.time.split(":") &&
                                  d.time.split(":")[0] > 12
                                    ? `${d.time.split(":")[0] - 12}:${
                                        d.time.split(":")[1]
                                      } PM`
                                    : `${d.time.split(":")[0]}:${
                                        d.time.split(":")[1]
                                      } AM`}
                                </div> */}
                                        {a.time && (
                                          <div className="batch-time">
                                            {moment(a.time, 'H:mm:ss').format(
                                              'h:mm a',
                                            )}
                                          </div>
                                        )}
                                        {/* <div>{a.time}</div> */}
                                      </li>
                                    ))}
                                  </ul>
                                ) : null}
                                {a.batch_days && a.batch_days.length ? (
                                  <ul className="batch-dateList">
                                    {a.batch_days.map(d => (
                                      <li className="batch-dateItem">
                                        <Flex column>
                                          <p>{d.selected_day}</p>
                                          {/* <span>Mon-thu</span> */}
                                        </Flex>
                                        {/* <div className="batch-time">
                                  {d.time.split(":") &&
                                  d.time.split(":")[0] > 12
                                    ? `${d.time.split(":")[0] - 12}:${
                                        d.time.split(":")[1]
                                      } PM`
                                    : `${d.time.split(":")[0]}:${
                                        d.time.split(":")[1]
                                      } AM`}
                                </div> */}
                                        {d.time && (
                                          <div className="batch-time">
                                            {moment(d.time, 'H:mm:ss').format(
                                              'h:mm a',
                                            )}
                                          </div>
                                        )}
                                        {/* <div>{a.time}</div> */}
                                      </li>
                                    ))}
                                  </ul>
                                ) : null}
                                {a.additional_details && (
                                  <p className="batch-buy">
                                    {a.additional_details}
                                  </p>
                                )}
                                <div className="batch-buy">
                                  <Button
                                    text={a.can_buy ? 'Buy Now' : 'Expired'}
                                    disabled={!a.can_buy}
                                    type={
                                      a.can_buy ? 'primary' : 'primary Expired'
                                    }
                                    height="50px"
                                    width="100%"
                                    // marginRight="auto"
                                    marginTop="20px"
                                    onClick={() =>
                                      a.can_buy
                                        ? handleSelectBatch(
                                            selectedBatch &&
                                            selectedBatch.id === a.id
                                              ? null
                                              : a,
                                          )
                                        : null
                                    }
                                    isLoading={
                                      purchasedLoader || createOrderLoader
                                    }
                                  />
                                </div>
                                <div className="batch-buy">
                                  {purchasedPrograms.find(
                                    purchased => purchased.id === a.id,
                                  ) ? (
                                    <span style={{ margin: '20px 0' }}>
                                      * You have previously purchased{' '}
                                      {a.heading_name}
                                    </span>
                                  ) : null}
                                </div>
                              </BatchDetail>
                            </>
                          ),
                      )
                  : null}

                {/* <BatchDetail className="active">
              <div className="batch-header">
                <Flex column>
                  <h4>Batch 3</h4>
                  <p>18 – 20 Sept</p>
                </Flex>
                <Flex alignCenter>
                  <span>See Schedule</span>
                </Flex>
              </div>
              <ul className="batch-dateList">
                <li className="batch-dateItem">
                  <Flex column>
                    <p>18 – 20 Sept</p>
                    <span>Mon-thu</span>
                  </Flex>
                  <div className="batch-time">10 - 11 Am</div>
                </li>
                <li className="batch-dateItem">
                  <Flex column>
                    <p>18 – 20 Sept</p>
                    <span>Mon-thu</span>
                  </Flex>
                  <div className="batch-time">10 - 11 Am</div>
                </li>
                <li className="batch-dateItem">
                  <Flex column>
                    <p>18 – 20 Sept</p>
                    <span>Mon-thu</span>
                  </Flex>
                  <div className="batch-time">10 - 11 Am</div>
                </li>
              </ul>
            </BatchDetail> */}
                {((programDetail.program_detail &&
                  programDetail.program_detail.online_tags === 'home_school') ||
                  courseType === 'Trial_Class' ||
                  courseType === 'Single_Session') && (
                  <>
                    <Flex justifyBetween flexWidth="100%" flexMargin="25px 0px">
                      <Button
                        type="right-al"
                        text={
                          courseType === 'Trial_Class' ? 'Book Now' : 'Buy now'
                        }
                        headerButton="20px"
                        height="50px"
                        onClick={() => handleSelectBatch(selectedBatch)}
                        isLoading={purchasedLoader || createOrderLoader}
                      />
                    </Flex>
                    {purchasedPrograms.find(
                      purchased =>
                        +purchased.id === +(selectedBatch && selectedBatch.id),
                    ) ? (
                      <>
                        {CourseTypeConfig.map(course => (
                          <>
                            {course.key === courseType && (
                              <p style={{ marginBottom: '20px' }}>
                                * You have previously purchased{' '}
                                {programDetail.program_detail &&
                                (programDetail.program_detail.online_tags ===
                                  'home_school') ===
                                  'home_school'
                                  ? 'Home School'
                                  : course.label}
                              </p>
                            )}
                          </>
                        ))}
                      </>
                    ) : null}
                  </>
                )}
                <Flex justifyBetween flexWidth="100%" className="reqButtons">
                  <Button
                    text="Request for call back"
                    type="request"
                    fontSize="14px"
                    headerButton="10px"
                    height="50px"
                    onClick={handleRequestCallBack}
                    marginRight="5px"
                    isLoading={callback.loader}
                  />
                  {/* <Button
                  text="See All Sessions"
                  headerButton="10px"
                  fontSize="14px"
                  height="50px"
                  onClick={handleSeeAllSession}
                /> */}
                </Flex>
              </PreviewSectionRight>
            </Flex>
            {!suggestedLoader &&
            suggestedPrograms &&
            suggestedPrograms.length ? (
              <div className="suggestSlider">
                <Flex
                  column
                  flexWidth="80%"
                  flexMargin="50px auto"
                  className="suggested"
                >
                  <Flex flexWidth="100%" justifyBetween>
                    <MainHeading text="Suggested Programs" fontSize="24px" />
                    <Button
                      text="View all"
                      type="viewAll"
                      marginRight="10px"
                      height="30px !important"
                      onClick={handleViewAll}
                    />
                  </Flex>
                  <Slider {...settings1}>
                    {suggestedPrograms.map(program => (
                      <div key={program.id}>
                        <ClassCard
                          isLoggedIn={isLoggedIn}
                          width="100%"
                          isOneToOne={program.online_tags === 'one_to_one'}
                          name={program.name}
                          rating={program.rating && program.rating.rating__avg}
                          providerName={program.provider_name}
                          //   age={program.age}
                          startAge={program.start_age}
                          endAge={program.end_age}
                          isSingleSession={
                            program.batch_available &&
                            program.batch_available.length
                              ? program.batch_available.includes(
                                  'Single_Session',
                                )
                              : null
                          }
                          isTrial={
                            program.batch_available &&
                            program.batch_available.length
                              ? program.batch_available.includes('Trial_Class')
                              : null
                          }
                          price={program.price}
                          moneyBack={
                            program.price && program.price.money_back_gurantee
                          }
                          isSponsered={program.is_sponsored}
                          media={program.medias}
                          // onCardClick={e => handleCardClick(program, e)}
                          href={`/online-program/${program.name
                            .replace(/\s+/g, '-')
                            .replace('/', '-')
                            .toLowerCase()}?id=${program.id}`}
                          programId={program.id}
                          onBookmark={() =>
                            handleBookMark(program && program.id)
                          }
                          onDeleteBookmark={() =>
                            handleDeleteBookMark(program && program.id)
                          }
                          isBookMarked={bookmarkedItems.includes(+program.id)}
                          batches={program.batch_list}
                        />
                      </div>
                    ))}
                  </Slider>
                </Flex>
              </div>
            ) : null}
          </PreviewSection>
          <Footer {...props} />
        </>
      )}

      {modalType &&
        modalStatus && (
          <Modal
            type={modalType}
            // setAuthActionSelected={setAuthActionSelected}
            setActive={setModalStatus}
            closeHandler={closeHandler}
            setModalType={setModalType}
            // authActionSelected={authActionSelected}
            // clickType={restrictionType}
            courseType={courseType}
            programDetail={programDetail}
            selectedBatch={selectedBatch}
            onBuyNow={handleBuyNow}
            isLoading={createOrderLoader}
            programId={programId}
            shareUrl={shareUrl}
            onlineTag={
              programDetail.program_detail &&
              programDetail.program_detail.online_tags
            }
            noOfSession={noOfSessions.value}
            {...props}
          />
        )}
    </OnlinePreview>
  );
}

Preview.propTypes = {};

export default Preview;

const OnlinePreview = styled.div`
  margin: 0 auto;
  max-width: 1440px;
  width: 100%;
`;

const Error = styled.div`
  font-size: 12px;
  color: red;
`;

const InputBoxSection = styled.div`
  position: relative;
  width: 50%;
  border: 1px solid #d9d9d9;
  border-radius: 4px;
  height: 60px;
  /* display: flex; */
  padding: 0px 24px;
  margin-top: 12px;
  background: #fff;
  @media (max-width: 1650px) {
    height: 48px;
    padding: 0px 18px;
  }

  &.error {
    border: 1px solid #e21212 !important;
  }

  & > input {
    border: 0px;
    width: 100%;
    font-family: Quicksand;
    font-weight: 500;
    font-size: 14px;
    line-height: 21px;
    color: #000000;
    text-transform: Capitalize;
    &:focus {
      outline: none;
    }
    &::-webkit-input-placeholder {
      font-family: Quicksand;
      font-weight: 400;
      font-size: 14px;
      line-height: 21px;
      color: rgba(0, 0, 0, 0.5);
    }
  }
  .error {
    color: red;
  }
`;

const Inputfield = styled.div`
  .error {
    font-family: Quicksand;
    font-weight: 500;
    font-size: 10px;
    line-height: 12px;
    color: #e21111;
    margin-top: 3px;
    margin-bottom: 10px;
  }
`;

const PreviewSection = styled.div`
  background-color: #fff;
  /* padding: 30px 48px; */
  padding: 140px 30px;
  background: #fafafa;
  @media screen and (max-width: 978px) {
    padding: 100px 30px;
  }
  @media screen and (max-width: 768px) {
    padding: 60px 10px 50px;
  }
  .outline {
    border: 1px solid #ced1d8;
    color: #9fa2a9;
  }
  .links {
    font-size: 14px;
    color: #613a95;
  }
  .suggestSlider {
    @media screen and (max-width: 768px) {
      max-width: 320px;
      margin: 0 auto;
    }
  }
  .suggested {
    @media screen and (max-width: 768px) {
      margin: 20px auto !important;
      width: 100%;
    }
    .slick-slide > div {
      margin: 0 40px;
      max-width: 304px;
      @media screen and (max-width: 1350px) {
        margin: 0 15px;
      }
      @media screen and (max-width: 1250px) {
        margin: 0 10px;
      }
    }
    .slick-list {
      margin: 0 -40px;
      @media screen and (max-width: 768px) {
        margin: 0;
      }
    }
  }
  @media screen and (max-width: 978px) {
    .flexChange {
      flex-direction: column;
    }
  }
  .suggestSlider {
    .slick-prev,
    .slick-next {
      &:before {
        color: #00000075;
        font-size: 36px;
        font-weight: 300;
        @media screen and (max-width: 768px) {
          font-size: 10px;
          font-weight: 700;
          color: #00000099 !important;
          opacity: 1 !important;
        }
      }
    }
    .slick-prev:before {
      content: 'ᐸ';
    }
    .slick-next:before {
      content: 'ᐳ';
    }
    .slick-initialized .slick-slide {
      @media screen and (max-width: 768px) {
        max-width: 320px;
      }
    }
    .slick-prev {
      left: -80px;
      // @media screen and (max-width: 1100px) {
      //   left: 5px;
      // }
      // @media screen and (max-width: 890px) {
      //   left: -33px;
      // }
      @media screen and (max-width: 768px) {
        background: #ffffff9e;
        right: auto;
        bottom: 0;
        left: -53px;
        margin: 0 auto;
        top: 18%;
        border: 1px solid #00000099;
        border-radius: 100%;
        z-index: 9;
        width: 40px;
        height: 40px;
        @media screen and (max-width: 435px) {
          width: 25px;
          height: 25px;
          left: -31px;
        }
        @media screen and (max-width: 390px) {
          width: 40px;
          height: 40px;
          left: 15px;
          top: 50%;
        }
      }
    }
    .slick-next {
      right: -80px;
      // @media screen and (max-width: 1100px) {
      //   right: 5px;
      // }
      // @media screen and (max-width: 890px) {
      //   right: -25px;
      // }
      @media screen and (max-width: 768px) {
        background: #ffffff9e;
        right: -53px;
        bottom: 0;
        left: auto;
        margin: 0 auto;
        top: 18%;
        border: 1px solid #00000099;
        border-radius: 100%;
        z-index: 9;
        width: 40px;
        height: 40px;
      }
      @media screen and (max-width: 435px) {
        width: 25px;
        height: 25px;
        right: -31px;
      }
      @media screen and (max-width: 390px) {
        width: 40px;
        height: 40px;
        right: 15px;
        top: 50%;
      }
    }
  }
`;

const PreviewSectionLeft = styled.div`
  background: #ffffff;
  box-shadow: 0px 4px 30px rgba(186, 186, 186, 0.25);
  border-radius: 10px;
  width: 58%;
  // @media screen and (max-width: 1350px) {
  //   width: 55%;
  // }
  @media screen and (max-width: 978px) {
    order: 2;
    width: 100%;
  }
  .preview-image {
    width: 100%;
    // height: 419px;
    // height: 516px;
    @media screen and (max-width: 978px) {
      display: none;
    }
    .slick-dots {
      margin-bottom: -20px;
    }
    .slick-slider {
      height: 100%;
    }
    .slick-list {
      height: 100%;
    }
    img {
      width: 100%;
      height: 100%;
      max-height: 516px;
    }
    .slick-prev,
    .slick-next {
      border: 1px solid #00000075;
      border-radius: 100%;
      display: flex !important;
      align-items: center;
      justify-content: center;
      &:before {
        color: #00000075;
        font-size: 10px;
        font-weight: 700;
        @media screen and (max-width: 768px) {
          font-size: 14px;
          font-weight: 700;
          color: #00000099 !important;
          opacity: 1 !important;
        }
      }
    }
    .slick-prev:before {
      content: 'ᐸ';
    }
    .slick-next:before {
      content: 'ᐳ';
    }
  }
  
  .preview-list {
    padding: 40px 26px 20px;
    width: 100%;
    @media screen and (max-width: 768px) {
      padding: 0 10px;
    }
  }
  .preview-item {
    display: flex;
    justify-content: space-between;
    margin-bottom: 16px;
    padding-bottom: 16px;
    border-bottom: 1px solid #f2eff5;
    .fa
    {
      margin-top:20px;
    }
    &.active {
      h2
      {
        color: #623B95;
        font-weight: 600;
        &::after
        {
          display:block;
        }
       
      }
      p {
        display: block;
      }
      img {
        display: block;
      }
      .review-section {
        display: block;
      }
    }
    &:hover {
      cursor: pointer;
    }

    h2 {
      font-family: Quicksand;
      font-weight: 500;
      font-size: 20px;
      line-height: 25px;
      color: #30333b;
      text-transform: capitalize;
      position:relative;

      @media screen and (max-width: 768px) {
        font-size: 18px;
        line-height: 19px;
        margin-top: 0;
        margin-bottom: 10px;
      }
      &::after
      {
        display:none;
        content:"";
        position:absolute;
        width: 4px;
        height: 20px;
        top: 3px;
        left: -14px;
        background-color:#623B95;
        @media screen and (max-width: 768px) {
          top: 0;
        }
      }
    }
    h3 {
      font-family: Quicksand;
      font-weight: 500;
      font-size: 18px;
      line-height: 25px;
      color: #623B95;
      text-transform: capitalize;
      position:relative;

      @media screen and (max-width: 768px) {
        font-size: 16px;
        line-height: 19px;
        margin-top: 20px;
        margin-bottom: 10px;
      }
    }
    .review-section {
      display: none;
    }
    p {
      display: none;
      font-family: Quicksand;
      font-weight: 500;
      font-size: 14px;
      line-height: 1.5;
      color: #4b5256;
      margin: 0px;
      white-space: pre-wrap;
      @media screen and (max-width: 768px) {
        font-size: 15px;
        line-height: 1.7;
        // text-align: justify;
            // white-space: unset;
      }
    }
    img {
      display: none;
    }
  }
  .commentSection {
    display: flex;
    flex-direction: column;
    .writeReview {
      @media screen and (max-width: 768px) {
        width: 95%;
      }
    }
  }
  .star {
    display: flex;
    flex-direction: row;
    align-items: center;
    margin-top: 30px;
    margin-right: 20px;
    @media screen and (max-width: 768px) {
      margin: 10px 0;
      flex-direction: column;
      align-items: flex-start;
    }
    .left {
      margin-right: 20px;
      @media screen and (max-width: 768px) {
        margin: 0 0 20px;
      }
      img {
        width: 50px;
        height: 50px;
        border-radius: 7px;
      }
    }
    .right {
      h2 {
        font-weight: 600 !important;
        color: #4b5256 !important;
        &:after {
          display: none;
        }
      }
      p {
      }
      div {
        outline: 0;
      }
    }
  }
  .reviewForm {
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    margin: 20px 0 0;
    @media screen and (max-width: 768px) {
      margin: 10px 0 0;
    }
    textarea {
      height: 100px;
      width: 100%;
      border: 1px solid #c0c8cd;
      border-radius: 5px;
      background-color: #ffffff;
      padding: 10px;
      margin: 0 0 20px;
    }
    button {
      height: 56px;
      width: 140px;
      border-radius: 5px;
      background-color: #3b74e7;
      border: 1px solid #3b74e7;
      color: #ffffff;
      font-family: Quicksand;
      font-size: 15px;
      font-weight: 500;
      letter-spacing: 0;
      line-height: 19px;
      display: flex;
      align-items: center;
      justify-content: center;
      margin: 0 0 0 auto;
      cursor: pointer;
      height: 36px;
      width: 90px;
    }
    &:hover {
      background-color: #ffffff;
      color: #3b74e7;
    }
  }
}
.reviewWrapper {
  // margin-bottom: 20px;
  border-bottom: 1px dashed #cccccc70;
  padding-bottom: 20px;
  &:last-child {
    border-bottom: 0;
  }
  @media screen and (max-width: 768px) {
    margin-top: 20px;
    margin-bottom: 0;
  }
}
.postedRatingWrapper {
  .star {
    margin-top: 20px;
    margin-bottom: 20px;
      @media screen and (max-width: 768px) {
        margin-top: 0px;
      }
    }
    h3 {
      font-family: Quicksand;
      font-weight: 500;
      font-size: 20px;
      line-height: 25px;
      color: #30333b;
      text-transform: capitalize;
      margin: 40px 0 0;
      @media screen and (max-width: 768px) {
        font-size: 16px;
        line-height: 19px;
        margin-top: 30px;
      }
    }
    h2 {
      font-family: Roboto;
      font-style: normal;
      font-weight: 500;
      font-size: 16px;
      line-height: 19px;
      color: #4b5256;
      margin: 0;
      @media screen and (max-width: 768px) {
        font-size: 14px;
      }
    }
    p {
      font-family: Roboto;
      font-style: normal;
      font-weight: normal;
      font-size: 16px;
      line-height: 22px;
      color: #4b5256;
      @media screen and (max-width: 768px) {
        font-size: 14px;
        line-height: 1.5;
      }
    }
    .postedRating {
      display: flex;
      flex-direction: row;
      align-items: center;
      @media screen and (max-width: 768px) {
        flex-direction: column;
        align-items: flex-start;
      }
      .postDate {
        font-family: Roboto;
        font-style: normal;
        font-weight: normal;
        font-size: 12px;
        line-height: 14px;
        color: #666c78;
        margin-left: 20px;
        @media screen and (max-width: 768px) {
          margin: 10px 0 0;
        }
      }
    }
  }
  video {
    width: 580px;
    margin: 0 auto;
    display: table; 
    @media screen and (max-width: 1100px) {
      width: 520px;
    }
  } 

  iframe {
    @media screen and (max-width: 1800px) {
      height: 500px !important;
    } 
    @media screen and (max-width: 1172px) {
      height: 403px !important;
    }
    @media screen and (max-width: 1024px) {
      height: 349px !important;
    }
    @media screen and (max-width: 768px) {
      height: 467px !important;
    }
    @media screen and (max-width: 628px) {
      height: 380px !important;
    }
    @media screen and (max-width: 540px) {
      height: 325px !important;
    }
    @media screen and (max-width: 425px) {
      height: 253px !important;
    }
    @media screen and (max-width: 375px) {
      height: 222px !important; 
    }
    @media screen and (max-width: 320px) {
      height: 187px !important;
    }
    @media screen and (max-width: 280px) {
      height: 162px !important; 
    }  
  }   
`;

const PreviewSectionRight = styled.div`
  background-color: #fff;
  padding: 28px;
  width: 40%;
  margin-left: 25px;
  @media screen and (max-width: 978px) {
    order: 1;
    width: 100%;
    margin: 0 0 30px;
  }
  @media screen and (max-width: 768px) {
    margin: 0 0 15px;
    padding: 15px 0 0;
  }
  @media screen and (max-width: 320px) {
    // padding: 0 25px;
  }
  & > h1 {
    font-family: Quicksand;
    font-weight: 600;
    font-size: 24px;
    line-height: 32px;
    color: #000000;
    margin: 10px 0px 25px;
    @media screen and (max-width: 768px) {
      font-weight: bold;
      font-size: 20px;
      line-height: 25px;
      margin-bottom: 20px;
    }
  }
  & > p {
    font-family: 'Roboto', sans-serif;
    font-weight: 300;
    font-size: 14px;
    line-height: 16px;
    color: rgba(75, 82, 86, 1);
  }
  .preview-image.tabView {
    display: none;
    @media screen and (max-width: 978px) {
      display: initial;
    }
    // .slick-prev,
    // .slick-next {
    //   border: 1px solid #00000075;
    //   border-radius: 100%;
    //   display: flex !important;
    //   align-items: center;
    //   justify-content: center;
    //   &:before {
    //     color: #00000075;
    //     font-size: 10px;
    //     font-weight: 700;
    //     @media screen and (max-width: 768px) {
    //       font-size: 14px;
    //       font-weight: 700;
    //       color: #00000099 !important;
    //       opacity: 1 !important;
    //       display: none !important;
    //     }
    //   }
    // }
    // .slick-prev:before {
    //   content: 'ᐸ';
    // }
    // .slick-next:before {
    //   content: 'ᐳ';
    // }
  }
  .reqButtons {
    border-top: 1px solid #e6e2ea;
    padding-top: 20px;
    @media screen and (max-width: 978px) {
      justify-content: space-evenly;
    }
    @media screen and (max-width: 768px) {
      flex-direction: column;
      button {
        margin: 0 0 20px;
      }
    }
  }
  .priceOffered {
    @media screen and (max-width: 768px) {
      flex-wrap: wrap;
      justify-content: flex-start;
    }
  }
  video {
    width: 100%;
    margin: 0 auto;
    display: table;
  }
  .right-al {
    @media screen and (max-width: 768px) {
      margin-left: auto;
    }
  }
  .request {
    background-color: #fff !important;
    border: 2px solid #613a95 !important;
    color: #613a95 !important;
    &:hover {
      background-color: #613a95 !important;
      color: #fff !important;
    }
  }
`;

const Sponsored = styled.div`
  span {
    padding: 4px;
    font-family: Quicksand;
    font-weight: 600;
    font-size: 14px;
    line-height: 13px;
    color: #fff;
    margin-right: 10px;
    text-transform: uppercase;
    border: 1px solid #10cc3b;
    background: #10cc3b;
    border-radius: 5px;
    padding: 7px;
  }
  .school-choice {
    background-color: #613a95;
    border: 1px solid #613a95;
  }
`;

const ProfileDetail = styled.div`
  display: flex;
  align-items: center;
  // margin-top: 30px;
  margin-bottom: 25px;
  @media screen and (max-width: 978px) {
    margin-bottom: 20px;
  }
  @media screen and (max-width: 768px) {
    margin-top: 0;
    margin-bottom: 20px;
  }
  .profile-name {
    font-family: Quicksand;
    font-weight: 500;
    font-size: 16px;
    line-height: 20px;
    color: #000000;
    margin-right: 10px;
    @media screen and (max-width: 768px) {
      font-weight: 500;
      font-size: 14px;
      line-height: 17px;
      color: #666c78;
    }
  }
  .profile-image {
    width: 32px;
    height: 32px;
    border-radius: 50%;
    overflow: hidden;
    margin-right: 10px;
    display: none;
    img {
      width: 100%;
      height: 100%;
    }
  }
  .profile-rating {
    padding-left: 12px;
    border-left: 1px solid #000000;
    display: flex;
    align-items: center;
    @media screen and (max-width: 768px) {
      font-weight: 500;
      font-size: 14px;
      line-height: 17px;
      color: #666c78;
    }
    img {
      margin-right: 12px;
    }
  }
`;

const ProviderInfo = styled.ul`
  display: flex;
  align-items: center;
  flex-wrap: wrap;
  padding: 0px;
  margin-top: 24px;
  margin-bottom: 0px;
  li {
    display: flex;
    font-family: 'Roboto', sans-serif;
    font-weight: 400;
    font-size: 14px;
    line-height: 16px;
    color: #4a5257;
    margin-bottom: 30px;
    margin-right: 12px;
    list-style-type: none;
    align-items: center;
    @media screen and (max-width: 768px) {
      font-size: 13px;
      font-weight: 500;
      margin-bottom: 0;
      margin: 7px 10px 7px 0;
    }
    img {
      margin-right: 10px;
    }
  }
`;

// const PriceOffered = styled.div`
//   padding: 7px 7px 7px 20px;
//   display: flex;
//   flex-direction: column;
//   border: 1px solid #bfc8ce;
//   border-radius: 5px;
//   width: 48%;
//   h2 {
//     font-family: Quicksand;
//     font-weight: bold;
//     font-size: 16px;
//     line-height: 20px;
//     color: #4a5257;
//     margin: 0px 0px 3px;
//   }
//   h5 {
//     font-family: Quicksand;
//     font-weight: bold;
//     font-size: 10px;
//     line-height: 12px;
//     color: #4a5257;
//     margin: 0px 0px 3px;
//     span {
//       font-family: Quicksand;
//       font-weight: 400;
//       font-size: 10px;
//       line-height: 12px;
//       text-decoration-line: line-through;
//       color: #4a5257;
//       opacity: 0.5;
//     }
//   }
//   h4 {
//     font-family: Quicksand;
//     font-weight: bold;
//     font-size: 14px;
//     line-height: 17px;
//     color: #4a5257;
//     margin: 0px;
//   }
// `;

const PriceOffered = styled.div`
  padding: 10px 16px;
  display: flex;
  flex-direction: column;
  border: 1px solid #bfc8ce;
  border-radius: 7px;
  width: 30%;
  cursor: pointer;
  margin-top: 10px;
  margin-right: ${props => props.marginRight || '0px'};
  justify-content: space-between;
  @media screen and (max-width: 768px) {
    width: 49%;
    width: 152px;
    // height: 71px;
  }
  // @media screen and (max-width: 110px) {
  //   padding: 10px 5px;
  // }
  @media screen and (max-width: 500px) {
    padding: 10px 5px;
    width: 132px;
    margin-right: 10px;
  }
  h2,
  h4,
  h5 {
    margin: 0 0 0.5rem !important;
  }
  &.active {
    background-color: #613a95;
    h2 {
      color: #fff;
      // margin: 0 0 0.5rem !important;
    }
    h4 {
      color: #fff;
      // margin: 0 0 0.5rem !important;
    }
    h5 {
      color: #fff;
      span {
        color: #fff;
        opacity: 1;
      }
      // margin: 0 0 0.5rem !important;
    }
  }
  h2 {
    // font-family: Quicksand;
    font-weight: 600;
    font-size: 18px;
    line-height: 26px;
    color: #000;
    margin: 0 0 2px !important;
  }
  h3 {
    font-family: Quicksand;
    font-weight: bold;
    font-size: 16px;
    line-height: 20px;
    color: #4a5257;
    margin: 0 0 0.5rem !important;
    font-weight: 600;
  }
  h5 {
    font-family: Quicksand;
    font-weight: bold;
    font-size: 10px;
    line-height: 12px;
    color: #4a5257;
    margin: 0 0 0.5rem !important;
    @media screen and (max-width: 1100px) {
      font-size: 9px;
      line-height: 12px;
    }
    span {
      font-family: Quicksand;
      font-weight: 400;
      font-size: 10px;
      line-height: 12px;
      text-decoration-line: line-through;
      color: #4a5257;
      opacity: 0.5;
    }
  }
  h4 {
    font-family: Quicksand;
    font-weight: 400;
    font-size: 13px;
    line-height: 15px;
    color: #000000;
    opacity: 0.62;
    margin: 0px;
  }
`;

const BatchDetail = styled.div`
  width: 100%;
  background: #ffffff;
  // margin-bottom: 32px;
  padding: 20px 0px;
  border-top: 1px dashed rgba(112, 112, 112, 0.22);

  &.active {
    // background: #f2eff5;
    .batch-dateList {
      display: block;
    }
    /* .batch-header span {
      display: none;
    } */
    .batch-buy {
      display: flex;
      font-family: Quicksand;
      font-weight: 500;
      font-size: 14px;
      line-height: 1.5;
      color: #4b5256;
      opacity: 1;
      margin: 0px;
      button {
        background: #613a95;
        border-color: #613a95;
        &:hover {
          border-color: #613a95;
          color: #613a95;
          background: #fff;
        }
        @media screen and (max-width: 768px) {
          margin-left: auto;
        }
      }
    }
  }
  .hide-icon {
    display: none;
  }
  .batch {
    &-buy {
      display: none;
      justify-content: space-between;
      align-items: center;
      margin-top: 20px;
      span {
        font-family: 'Roboto', sans-serif;
        font-weight: 400;
        font-size: 13px;
        line-height: 15px;
        color: #4a5257;
      }
    }
    &-header {
      display: flex;
      justify-content: space-between;
      align-items: center;
      position: relative;
      h4 {
        font-family: Quicksand;
        font-weight: 500;
        font-size: 18px;
        line-height: 26px;
        color: #000000;
        margin: 0px 0px 5px;
        @media screen and (max-width: 768px) {
          font-size: 16px;
        }
      }
      > div:nth-of-type(2) {
        width: 100%;
        position: absolute;
        right: 0;
        text-align: right;
        height: 100%;
        display: flex;
        align-items: center;
        justify-content: flex-end;
        cursor: pointer;
      }
      .dates {
        display: flex;
        align-items: center;
        margin: 0px 0px 0px;
        span {
          font-family: Quicksand;
          font-weight: bold;
          font-size: 15px;
          line-height: 21px;
          color: #613a94;
          @media screen and (max-width: 768px) {
            font-size: 13px;
          }
        }
      }
      .batchSeats {
        display: flex;
        align-items: center;
        color: #000000;
        opacity: 0.62;
      }
      p {
        font-family: Quicksand;
        font-weight: 400;
        font-size: 13px;
        line-height: 15px;
        color: #000000;
        margin: 0px 10px 0px 0px;
        @media screen and (max-width: 500px) {
          font-size: 11px;
          margin: 0px 10px 0px 0px;
        }
      }
      .duration-section {
        margin-left: 10px;
      }
      span {
        display: block;
        font-family: 'Roboto', sans-serif;
        font-weight: 400;
        font-size: 12px;
        line-height: 14px;
        color: #613a95;
        cursor: pointer;
        // margin-right: 40px;
      }
    }
    &-dateList {
      padding-left: 0px;
      margin-top: 40px;
      display: none;
      @media screen and (max-width: 768px) {
        margin-top: 20px;
      }
    }
    &-dateItem {
      width: 58%;
      display: flex;
      justify-content: space-between;
      align-items: center;
      margin-bottom: 12px;
      @media screen and (max-width: 450px) {
        width: 100%;
      }
      p {
        font-family: Quicksand;
        font-weight: bold;
        font-size: 15px;
        line-height: 21px;
        color: #7ba959;
        margin: 0px;
      }
      span {
        font-family: 'Roboto', sans-serif;
        font-weight: 400;
        font-size: 14px;
        line-height: 16px;
        color: #4a5257;
        margin: 0px;
      }
    }
    &-time {
      display: flex;
      justify-content: center;
      align-items: center;
      height: 36px;
      background: #edf3e8;
      border-radius: 4px;
      border-radius: 4px;
      padding: 0px 10px;
      width: 100px;
      font-family: Quicksand;
      font-weight: bold;
      font-size: 15px;
      line-height: 21px;
      color: #7ba959;
    }
  }
  .btn {
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 0;
    button.primary {
      margin: 0 25px 0 0;
      padding: 0 20px;
      height: 35px;
      font-size: 15px;
      background: #613a95;
      border-color: #613a95;
      &:hover {
        border-color: #613a95;
        color: #613a95;
        background: #fff;
      }
      @media screen and (max-width: 768px) {
        margin: 0 15px 0 0;
        padding: 15px;
        height: 20px;
        font-size: 12px;
      }
      @media screen and (max-width: 345px) {
        padding: 7px;
        height: 27px;
      }
    }
  }
  .Expired {
    background: #ccc !important;
    color: #fff !important;
    border: none !important;
    cursor: not-allowed !important;
  }
`;
const Share = styled.div`
  display: flex;
  margin-bottom: 20px;
  @media screen and (max-width: 768px) {
    justify-content: flex-end;
    float: right;
    margin-bottom: 0;
  }
  li {
    display: flex;
    justify-content: center;
    align-items: center;
    border: 1px solid #e3e4e8;
    font-family: Quicksand;
    font-weight: bold;
    font-size: 15px;
    line-height: 21px;
    color: #000000;
    opacity: 0.62;
    padding: 0px 10px;
    height: 32px;
    margin-right: 20px;
    cursor: pointer;
    border-radius: 5px;
    @media screen and (max-width: 768px) {
      font-size: 0;
      margin-right: 10px;
    }
    &:hover {
      border: 1px solid #000;
    }
    .iconify {
      width: 20px;
      height: 20px;
      margin-right: 10px;
      @media screen and (max-width: 768px) {
        width: 15px;
        height: 15px;
        margin-right: 0;
      }
    }
  }
  .hide-icon {
    display: none;
  }
`;

// const ProgramLoader = styled.div`
//   height: calc(100vh - 500px);
//   width: 100%;
// `;
