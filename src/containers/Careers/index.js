import React from 'react';
// import PropTypes from 'prop-types';
import styled from 'styled-components';
import Header from 'components/Header';
import Footer from 'components/Footer';
// import SearchBar from 'components/SearchBar';
// import Button from 'components/Button';
// import MainHeading from 'components/MainHeading';
// import { Mainheading } from 'components/MainHeading/styled';
// import SubHeading from 'components/SubHeading';
import Flex from 'components/common/flex';
// import colors from 'utils/colors';
// import Apply from 'components/ApplyCard';
import careerBanner from '../../images/career_kiddenz.png';

export const CareerSection = styled.div`
  position: relative;
  margin-top: 106px;
  display: flex;
  justify-content: flex-end;
  max-width: 1290px;
  margin-left: auto;
  @media (max-width: 500px) {
    margin-top: 70px;
  }
  .careerDetails {
    position: absolute;
    left: 20px;
    @media (max-width: 900px) {
      padding-top: 15px;
      width: 55%;
    }
    @media (max-width: 500px) {
      width: 90%;
      padding-top: 0px;
    }
  }
`;

export const ApplySection = styled.div`
  max-width: 1180px;
  margin: 0px auto;
  padding: 50px 20px 100px;
  //   border-top: 1px solid rgba(230, 232, 236, 1);

  .applySection {
    display: flex;
    width: 100%;
    flex-flow: row wrap;
    justify-content: space-between;
  }

  .applySection::after {
    content: '';
    flex-grow: 1;
    margin-left: 20px;
    @media (max-width: 900px) {
      width: 32%;
    }
  }
`;
export const CareerBannerImage = styled.div`
  width: 50%;
  height: 435px;
  @media (max-width: 900px) {
    height: 300px;
  }
  @media (max-width: 500px) {
    width: 100%;
    height: 200px;
    margin-top: 200px;
    margin-bottom: 70px;
  }
  img {
    width: 100%;
    height: 100%;
  }
`;
const Title = styled.h1`
  font-family: 'Quicksand', sans-serif;
  margin: 0px 0px 10px 0px;
  font-size: 32px;
  color: #2d3438;
  font-weight: bold;
  line-height: 45px;
  z-index: 1;
  @media (max-width: 900px) {
    font-size: 24px;
  }
  @media (max-width: 500px) {
    font-size: 20px;
  }
`;

const KiddenText = styled.h2`
  font-family: 'Quicksand', sans-serif;
  margin: ${props => props.textmargin || '0px 0px 28px 0px;'};
  color: #30333b;
  font-size: 18px;
  font-weight: bold;
  line-height: 30px;
  padding-right: 100px;
  @media (max-width: 900px) {
    font-size: 14px;
    line-height: 20px;
    padding-right: 30px;
  }
`;

// const NavBar = styled.ul`
//   display: flex;
//   width: 100%;
//   justify-content: space-between;
//   margin-top: auto;
//   margin-bottom: 0px;
//   padding: 0px;
//   h2 {
//     position: relative;
//     color: rgba(48, 51, 59, 1);
//     font-family: 'Quicksand', sans-serif;
//     font-size: 16px;
//     font-weight: 700;
//     line-height: 20px;
//     padding: 0px 20px 28px 20px;
//     list-style-type: none;
//     cursor: pointer;
//     margin: 0px;
//     @media (max-width: 900px) {
//       font-size: 13px;
//       padding-bottom: 15px;
//     }
//     @media (max-width: 500px) {
//       padding: 0px 12px 28px 12px;
//     }
//     @media (max-width: 360px) {
//       padding: 0px 3px 28px 2px;
//     }

//     &:hover,
//     &.active {
//       color: ${colors.secondary};
//     }
//     &::after {
//       position: absolute;
//       content: '';
//       width: 0%;
//       left: 0px;
//       bottom: 0px;
//       height: 3px;
//       transition: width 0.3s ease;
//       background-color: ${colors.secondary};
//     }
//     &:hover::after,
//     &.active::after {
//       width: 100%;
//       transition: width 0.3s ease;
//     }
//   }
// `;

const CareerLink = styled.a`
  height: 40px;
  text-decoration: none;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100px;
  border-radius: 10px;
  background: #55357f;
  color: #fff;
  border: 1px solid #55357f;
  &:hover {
    transition: 0.3s;
    background: #fff;
    color: #55357f;
  }
`;

// const NavItems = ['All', 'Technology', 'Operations', 'Commercials'];
const Careers = props => (
  <>
    <Header
      {...props}
      type="articles"
      placeHolderValue="Search Article"
      searchBar={false}
    />
    <CareerSection>
      <Flex
        column
        className="careerDetails"
        flexWidth="40%"
        flexHeight="100%"
        flexPadding="80px 0px 0px"
      >
        {/* <MainHeading fontSize="32px" text="Careers" margin="0px 0px 10px 0px" /> */}
        <Title>Careers</Title>
        <KiddenText>
          Join our team! We are Bangalore based start-up and we are ready to
          hire new talent to our team
        </KiddenText>
        {/* <MainHeading fontSize="18px" margin="0px 0px 28px 0px" text="Join our team! We are Bangalore based start-up and we are ready to hire new talent to our team" /> */}
        <Flex row alignCenter className="mobileColumn">
          <CareerLink
            href="https://docs.google.com/forms/d/e/1FAIpQLSfJj79C_CtadxB6KrB_20UJa2Z0GL55eA4vJfeCjZoBUGc4fw/viewform?usp=sf_link"
            target="_blank"
          >
            Apply
          </CareerLink>
        </Flex>
        {/* <SearchBar margin="0px" width="100%" /> */}
        {/* <NavBar>
          {NavItems.map(x => (
            <>
              <h2>{x}</h2>
            </>
          ))}
        </NavBar> */}
      </Flex>
      <CareerBannerImage>
        <img src={careerBanner} alt="" />
      </CareerBannerImage>
    </CareerSection>
    {/* <hr style={{ margin: 0 }} />
    <ApplySection>
      <div className="applySection">
        <Apply
          name="Sales Manager"
          opening="HSR Layout, Bangalore"
          experience="5 yrs"
          Desc="Lorem ipsum dolor sit amet, mel ex ipsum dolore. Sit commodo eripuit an, has et natum invenire. An vix nostro accusata omittantur.
Lorem ipsum dolor sit amet, mel ex ipsum dolore."
        />
        <Apply
          name="Sales Manager"
          opening="HSR Layout, Bangalore"
          experience="5 yrs"
          Desc="Lorem ipsum dolor sit amet, mel ex ipsum dolore. Sit commodo eripuit an, has et natum invenire. An vix nostro accusata omittantur.
Lorem ipsum dolor sit amet, mel ex ipsum dolore."
        />
        <Apply
          name="Sales Manager"
          opening="HSR Layout, Bangalore"
          experience="5 yrs"
          Desc="Lorem ipsum dolor sit amet, mel ex ipsum dolore. Sit commodo eripuit an, has et natum invenire. An vix nostro accusata omittantur.
Lorem ipsum dolor sit amet, mel ex ipsum dolore."
        />
        <Apply
          name="Sales Manager"
          opening="HSR Layout, Bangalore"
          experience="5 yrs"
          Desc="Lorem ipsum dolor sit amet, mel ex ipsum dolore. Sit commodo eripuit an, has et natum invenire. An vix nostro accusata omittantur.
Lorem ipsum dolor sit amet, mel ex ipsum dolore."
        />
        <Apply
          name="Sales Manager"
          opening="HSR Layout, Bangalore"
          experience="5 yrs"
          Desc="Lorem ipsum dolor sit amet, mel ex ipsum dolore. Sit commodo eripuit an, has et natum invenire. An vix nostro accusata omittantur.
Lorem ipsum dolor sit amet, mel ex ipsum dolore."
        />
        <Apply
          name="Sales Manager"
          opening="HSR Layout, Bangalore"
          experience="5 yrs"
          Desc="Lorem ipsum dolor sit amet, mel ex ipsum dolore. Sit commodo eripuit an, has et natum invenire. An vix nostro accusata omittantur.
Lorem ipsum dolor sit amet, mel ex ipsum dolore."
        />
      </div>
    </ApplySection> */}

    <Footer {...props} />
  </>
);
// Careers.propTypes = {
//   text: PropTypes.string,
//   fontSize: PropTypes.string,
//   margin: PropTypes.string,
// };
export default Careers;
