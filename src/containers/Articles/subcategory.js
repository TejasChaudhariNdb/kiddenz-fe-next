import React from 'react';
import Router from 'next/router';
import {
  useArticleSubcategoryHook,
  useBookmarkHook,
  useArticleMostRead,
} from 'shared/hooks';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import moment from 'moment';
import Link from 'next/link';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import MainWrapper from '../../components/MainWrapper';
// import Smileycard from '../../components/SmileyCard';
import Flex from '../../components/common/flex';
// import InputBox from '../../components/InputBox';
import InterestCard from '../../components/InterestCard';
import Button from '../../components/Button';
import SchoolCard from '../../components/SchoolCard';
import LisitingEmptyState from './LisitingEmptyState';
import pulseLoader from '../../images/pulse_loader.gif';

// import interestImage1 from '../../images/Rectangle.svg';

const Articles = props => {
  const {
    query: {
      'category-name': categoryName,
      'sub-category-name': subCategoryName,
      id,
      name,
      main,
      back = true,
    } = {},
    errorToast,
    GET_USER_BOOKMARK_ARTICLES_API_CALL,
    authentication: {
      isLoggedIn,
      categoryColors = {},
      categoriesConfig = {},
    } = {},
  } = props;

  const {
    subCategoriesData: { data: { data: categories = [] } = {} } = {},
    articleList: {
      data: { [subCategoryName]: featuredArticle = [], count = 0 } = {},
    } = {},
    loadMoreArticles,
    getArticlesloader: loadMoreLoader,
    relatedArticleLoader,
    articleCount,
    relatedArticle: { data: relatedArticle = [] } = {},
  } = useArticleSubcategoryHook(props);

  const onBookmarkAddError = ({ message }) =>
    errorToast(message || 'Something went wrong.');

  const { bookmarkedItems = [], addBookmark, removeBookmark } = useBookmarkHook(
    props,
    { onBookmarkAddError },
  );

  const { mostReadArticles = [] } = useArticleMostRead(props);

  const handelSubCategoryClick = (slug, categoryId, category, hasBack = true) =>
    Router.push(
      `/blog/category/${categoryName}/${slug}?main=${main}&id=${categoryId}&name=${category}&back=${hasBack}`,
    ).then(() => window.scrollTo(0, 0));

  // const handelCategoryClick = (slug, categoryId, fullName) =>
  //   Router.push(
  //     `/blog/category/${slug}?id=${categoryId}&name=${fullName}`,
  //   );

  const onAuthSuccess = () => {
    GET_USER_BOOKMARK_ARTICLES_API_CALL();
  };

  return (
    <>
      <Article>
        <Header
          type="articles"
          placeHolderValue="Search by keyword "
          searchBar
          isArticles
          onOtpSuccessCallback={onAuthSuccess}
          onOnboardingSuccessCallback={onAuthSuccess}
          {...props}
        />
        <MainWrapper background="#fff">
          <ArticleSection>
            {back !== 'false' ? (
              <Button
                type="attach"
                text={`Back to ${main}`}
                onClick={() => Router.back()}
              />
            ) : null}
            <Flex
              justifyBetween
              className="subCategory"
              flexMargin="20px 0px 0px"
            >
              <Flex column flexWidth="65%">
                <Flex justifyBetween flexWidth="100%">
                  <Title>{name}</Title>
                  <div />
                </Flex>
                {count > 0 ? (
                  <>
                    {featuredArticle.slice(0, 1).map(article => (
                      <Flex justifyBetween flexMargin="30px 0px 50px">
                        <Link
                          href={`/blog/${article.post_name}-${
                            article.post_id
                          }?category=${article.category}&name=${
                            article.post_title
                          }`}
                        >
                          <InterestCard
                            categoriesConfig={categoriesConfig}
                            isLoggedIn={isLoggedIn}
                            imageHeight="462px"
                            interestImage={article.post_image}
                            heading={article.post_title}
                            list={article.post_tag}
                            categoryList={article.category}
                            dateNum={moment(article.post_modified).format(
                              'MMMM DD,YYYY',
                            )}
                            viewNum={article.viewCount}
                            likesNum={article.likeCount}
                            width="100%"
                            isBookmarked={bookmarkedItems.includes(
                              article.post_id,
                            )}
                            onBookmarkClick={() => {
                              if (bookmarkedItems.includes(article.post_id))
                                removeBookmark(article.post_id);
                              else addBookmark(article.post_id);
                            }}
                            categoryColors={categoryColors}
                          />
                        </Link>
                      </Flex>
                    ))}
                    <Flex column flexWidth="100%">
                      {featuredArticle.slice(1).map(article => (
                        <Link
                          href={`/blog/${article.post_name}-${
                            article.post_id
                          }?category=${article.category}&name=${
                            article.post_title
                          }`}
                        >
                          <InterestCard
                            categoriesConfig={categoriesConfig}
                            isLoggedIn={isLoggedIn}
                            imageHeight="154px"
                            imageWidth="247px"
                            interestImage={article.post_image}
                            heading={article.post_title}
                            list={article.post_tag}
                            categoryList={article.category}
                            dateNum={moment(article.post_modified).format(
                              'MMMM DD,YYYY',
                            )}
                            viewNum={article.viewCount}
                            likesNum={article.likeCount}
                            width="100%"
                            type="primary"
                            margin="0px 0px 50px"
                            isBookmarked={bookmarkedItems.includes(
                              article.post_id,
                            )}
                            onBookmarkClick={() => {
                              if (bookmarkedItems.includes(article.post_id))
                                removeBookmark(article.post_id);
                              else addBookmark(article.post_id);
                            }}
                            categoryColors={categoryColors}
                          />
                        </Link>
                      ))}
                    </Flex>

                    {count !== featuredArticle.length && (
                      <Flex justifyCenter flexMargin="50px 0px 50px 0px">
                        <Button
                          href="/"
                          text="Load More"
                          type="subscribe"
                          isLoading={loadMoreLoader}
                          onClick={() => {
                            loadMoreArticles(
                              subCategoryName,
                              featuredArticle.length,
                            );
                          }}
                        />
                      </Flex>
                    )}

                    {articleCount < 10 ? (
                      <>
                        <Flex justifyBetween flexWidth="100%">
                          <KiddenText>Related articles</KiddenText>
                        </Flex>
                        {relatedArticleLoader ? (
                          // {true ? (
                          <div className="articleLoader">
                            <img
                              src={pulseLoader}
                              alt=""
                              height={75}
                              width={75}
                            />
                          </div>
                        ) : (
                          <Flex
                            column
                            justifyBetween={false}
                            flexMargin="30px 0px 100px"
                            className="parentingChilcare"
                            // flexMargin="30px 0px 50px"
                          >
                            {relatedArticle.slice(0, 10).map(article => (
                              <Link
                                href={`/blog/${article.post_name}-${
                                  article.post_id
                                }?category=${article.category}&name=${
                                  article.post_title
                                }`}
                              >
                                <InterestCard
                                  categoriesConfig={categoriesConfig}
                                  isLoggedIn={isLoggedIn}
                                  imageHeight="154px"
                                  imageWidth="247px"
                                  interestImage={article.post_image}
                                  heading={article.post_title}
                                  list={article.post_tag}
                                  categoryList={article.category}
                                  dateNum={moment(article.post_modified).format(
                                    'MMMM DD,YYYY',
                                  )}
                                  viewNum={article.viewCount}
                                  likesNum={article.likeCount}
                                  width="100%"
                                  type="primary"
                                  margin="0px 0px 50px"
                                  isBookmarked={bookmarkedItems.includes(
                                    article.post_id,
                                  )}
                                  onBookmarkClick={() => {
                                    if (
                                      bookmarkedItems.includes(article.post_id)
                                    )
                                      removeBookmark(article.post_id);
                                    else addBookmark(article.post_id);
                                  }}
                                  categoryColors={categoryColors}
                                />
                              </Link>
                            ))}
                          </Flex>
                        )}
                      </>
                    ) : null}
                  </>
                ) : (
                  <LisitingEmptyState title="No Article Found" />
                )}
              </Flex>
              <Flex flexWidth="31%" column>
                {/* <Link href="/blog">
                  <Flex justifyBetween flexMargin="0px 0px 50px">
                    <Smileycard
                      text="Categories"
                      type="category"
                      margin="0px"
                    />
                    <Button type="subscribe" text="Subscribe" />
                  </Flex>
                </Link> */}

                <Flex justifyBetween flexWidth="100%" flexMargin="0px 0px 30px">
                  <SubCatHeading>
                    {back === 'false'
                      ? `Related to ${name}`
                      : `More from ${main}`}
                  </SubCatHeading>
                  <div />
                </Flex>
                <Flex wrap flexMargin="0px 0px 90px" className="moreCategories">
                  {categories
                    .filter(
                      d => (back === 'false' ? d.slug !== subCategoryName : d),
                    )
                    .map(({ name: subCatname, slug }) => (
                      <MoreBond
                        onClick={() => {
                          if (subCatname !== name)
                            handelSubCategoryClick(slug, id, subCatname, back);
                        }}
                        color={subCatname === name ? '#613A95' : null}
                      >
                        {subCatname}
                      </MoreBond>
                    ))}
                </Flex>
                <Flex justifyBetween flexWidth="100%" flexMargin="0px 0px 30px">
                  <KiddenText>Most Read</KiddenText>
                  <div />
                </Flex>

                {mostReadArticles.map(
                  ({
                    post_image: img = '',
                    likeCount,
                    viewCount,
                    post_title: title,
                    post_modified: date,
                    ...article
                  }) => (
                    <SchoolCard
                      schoolImage={img}
                      schoolCardHeight="62px"
                      schoolCardWidth="100px"
                      schoolWrapperMarTop="0px"
                      mostRead={title}
                      dateNum={moment(date).format('MMMM DD, YYYY')}
                      viewNum={viewCount}
                      likesNum={likeCount}
                      cardtype="mostRead"
                      photocardType="bookmark"
                      redirectionLink={`/blog/${article.post_name}-${
                        article.post_id
                      }?category=${article.category}&name=${title}`}
                    />
                  ),
                )}
              </Flex>
            </Flex>
          </ArticleSection>
        </MainWrapper>
        <Footer {...props} />
      </Article>
    </>
  );
};

export default Articles;

Articles.propTypes = {
  query: PropTypes.object,
  authentication: PropTypes.object,
  errorToast: PropTypes.func,
  GET_USER_BOOKMARK_ARTICLES_API_CALL: PropTypes.func,
};

const Article = styled.div`
  .ternary {
    box-shadow: none;
  }
`;

const ArticleSection = styled.div`
  padding: 100px 15px;
  margin: 0px auto;
  width: 100%;
  max-width: 1170px;
  .categories {
    padding: 10px 20px;
  }
  .subCategory {
    @media screen and (max-width: 500px) {
      flex-direction: column;
    }
    & > div {
      @media screen and (max-width: 500px) {
        width: 100%;
      }
    }
  }
  .moreCategories {
    @media screen and (max-width: 500px) {
      margin-bottom: 30px;
    }
  }
  @media screen and (max-width: 500px) {
    padding: 30px 15px 50px;
  }
  .articleLoader {
    margin: 200px 0px 200px;
    display: flex;
    justify-content: center;
  }
`;

const Title = styled.h1`
  font-family: 'Quicksand', sans-serif;
  color: #30333b;
  font-size: 32px;
  font-weight: bold;
  line-height: 30px;
  text-align: center;
  margin: 0px;
  @media screen and (max-width: 900px) {
    font-size: 20px;
  }
  @media screen and (max-width: 500px) {
    font-size: 20px !important;
  }
`;

const KiddenText = styled.div`
  font-family: 'Quicksand', sans-serif;
  color: #30333b;
  font-size: 24px;
  font-weight: bold;
  line-height: 30px;
  text-align: center;
  @media screen and (max-width: 900px) {
    font-size: 20px;
  }
`;

const SubCatHeading = styled.div`
  font-family: 'Quicksand', sans-serif;
  color: #30333b;
  font-size: 24px;
  font-weight: bold;
  line-height: 30px;
  text-align: initial;
  @media screen and (max-width: 900px) {
    font-size: 20px;
  }
`;

const MoreBond = styled.div`
  cursor: pointer;
  padding: 10px 20px;
  width: fit-content;
  border-radius: 5px;
  background-color: #ffffff;
  box-shadow: 2px 2px 8px 0 rgba(0, 0, 0, 0.05);
  color: ${({ color }) => color || '#666c78'};
  font-family: 'Quicksand', sans-serif;
  font-size: 14px;
  font-weight: 500;
  line-height: 28px;
  margin: 0px 10px 10px 0px;
  cursor: pointer;
  transition: all 0.2s;
  &:hover {
    transform: scale(1.02);
    box-shadow: 0 0 5px 0 rgba(0, 0, 0, 0.2);
    transition: all 0.2s;
  }
`;
