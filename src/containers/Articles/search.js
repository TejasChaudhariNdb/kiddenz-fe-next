/* eslint-disable react/no-unescaped-entities */
/* eslint-disable indent */
import React from 'react';
import {
  useSearchHooks,
  useBookmarkHook,
  useArticleMostRead,
} from 'shared/hooks';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import moment from 'moment';
import Link from 'next/link';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import MainWrapper from '../../components/MainWrapper';
import Flex from '../../components/common/flex';
// import InputBox from '../../components/InputBox';
import InterestCard from '../../components/InterestCard';
import SchoolCard from '../../components/SchoolCard';

// import interestImage1 from '../../images/Rectangle.svg';

const Articles = props => {
  const { query: { key } = {} } = props;
  const containsHash = key.slice(0, 1) === '#';
  const { articles } = useSearchHooks(props, {
    containsHash,
    hash: key.slice(1),
  });
  const searchResults = containsHash
    ? articles[key.slice(1)] || []
    : articles.data || [];

  const {
    query: { key: searchKey } = {},
    errorToast,
    GET_USER_BOOKMARK_ARTICLES_API_CALL,
    authentication: {
      isLoggedIn,
      categoryColors = {},
      categoriesConfig = {},
    } = {},
  } = props;

  const onBookmarkAddError = ({ message }) =>
    errorToast(message || 'Something went wrong.');

  const { bookmarkedItems = [], addBookmark, removeBookmark } = useBookmarkHook(
    props,
    { onBookmarkAddError },
  );

  const { mostReadArticles = [] } = useArticleMostRead(props);
  const onAuthSuccess = () => {
    GET_USER_BOOKMARK_ARTICLES_API_CALL();
  };
  return (
    <>
      <Article>
        <Header
          type="articles"
          placeHolderValue="Search by keyword "
          searchBar
          isArticles
          onOtpSuccessCallback={onAuthSuccess}
          onOnboardingSuccessCallback={onAuthSuccess}
          {...props}
        />
        <MainWrapper background="#fff">
          <ArticleSection>
            <Flex justifyBetween>
              <Flex column flexWidth="65%">
                <Flex justifyBetween flexWidth="100%">
                  <KiddenText style={{ fontSize: '32px' }}>
                    Results for '{`${searchKey}`}'
                  </KiddenText>
                </Flex>

                <Flex column flexWidth="100%" flexMargin="30px 0px 50px">
                  {searchResults.length > 0
                    ? searchResults.map(article => (
                        <Link
                          href={`/blog/${article.post_name}-${
                            article.post_id
                          }?category=${article.category}&name=${
                            article.post_title
                          }`}
                        >
                          <InterestCard
                            isLoggedIn={isLoggedIn}
                            imageHeight="154px"
                            imageWidth="247px"
                            interestImage={article.post_image}
                            heading={article.post_title}
                            list={article.post_tag}
                            categoryList={article.category}
                            dateNum={moment(article.post_modified).format(
                              'MMMM DD,YYYY',
                            )}
                            viewNum={article.viewCount}
                            likesNum={article.likeCount}
                            width="100%"
                            type="primary"
                            margin="0px 0px 50px"
                            isBookmarked={bookmarkedItems.includes(
                              article.post_id,
                            )}
                            onBookmarkClick={() => {
                              if (bookmarkedItems.includes(article.post_id))
                                removeBookmark(article.post_id);
                              else addBookmark(article.post_id);
                            }}
                            categoryColors={categoryColors}
                            categoriesConfig={categoriesConfig}
                          />
                        </Link>
                      ))
                    : 'No Data'}
                </Flex>
              </Flex>
              <Flex flexWidth="31%" column>
                <Flex justifyBetween flexWidth="100%" flexMargin="0px 0px 30px">
                  <KiddenText>Most Read</KiddenText>
                  <div />
                </Flex>
                {mostReadArticles.map(
                  ({
                    post_image: img = '',
                    likeCount,
                    viewCount,
                    post_title: title,
                    post_modified: date,
                    ...article
                  }) => (
                    <SchoolCard
                      schoolImage={img}
                      schoolCardHeight="62px"
                      schoolCardWidth="100px"
                      schoolWrapperMarTop="0px"
                      mostRead={title}
                      dateNum={moment(date).format('MMMM DD, YYYY')}
                      viewNum={viewCount}
                      likesNum={likeCount}
                      cardtype="mostRead"
                      photocardType="bookmark"
                      redirectionLink={`/blog/${article.post_name}-${
                        article.post_id
                      }?category=${article.category}&name=${title}`}
                    />
                  ),
                )}
              </Flex>
            </Flex>
          </ArticleSection>
        </MainWrapper>
        <Footer {...props} />
      </Article>
    </>
  );
};

export default Articles;

Articles.propTypes = {
  query: PropTypes.object,
  authentication: PropTypes.object,
  errorToast: PropTypes.func,
  GET_USER_BOOKMARK_ARTICLES_API_CALL: PropTypes.func,
};

const Article = styled.div`
  .ternary {
    box-shadow: none;
  }
`;

const ArticleSection = styled.div`
  padding: 100px 15px;
  margin: 0px auto;
  width: 100%;
  max-width: 1170px;
  .categories {
    padding: 10px 20px;
  }
`;
const KiddenText = styled.div`
  font-family: 'Quicksand', sans-serif;
  color: #30333b;
  font-size: 24px;
  font-weight: bold;
  line-height: 30px;
  text-align: center;
  @media screen and (max-width: 900px) {
    font-size: 20px;
  }
`;
