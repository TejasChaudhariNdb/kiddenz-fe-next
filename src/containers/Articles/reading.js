/* eslint-disable no-nested-ternary */
/* eslint-disable indent */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Link from 'next/link';
import { useArticleDetail, useBookmarkHook, useLikeHook } from 'shared/hooks';
import moment from 'moment';
import Head from 'next/head';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import MainWrapper from '../../components/MainWrapper';
import IconButton from '../../components/Iconcard';
import Flex from '../../components/common/flex';
import { Photo } from '../../components/PhotoCard/styled';
import { IconCard } from '../../components/Iconcard/styled';
import InterestCard from '../../components/InterestCard';
// import babyImage from '../../images/pacifier.jpg';
// import babyImage1 from '../../images/pottyImage.jpg';
// import babyImage2 from '../../images/babyImage2.jpg';

// // import interestImage1 from '../../images/Rectangle';

const Articles = props => {
  const [modalStatus, setModalStatus] = useState(false);
  const [modalType, setModalType] = useState('email');
  const [actionIconClass, setActionIconClass] = useState('show');

  const {
    errorToast,
    // successToast,
    GET_USER_BOOKMARK_ARTICLES_API_CALL,
    authentication: { categoryColors = {}, categoriesConfig = {} } = {},
    query: { id: articleId } = {},
  } = props;

  const {
    articleDetail: { data: { data: articleDetail = {} } = {} } = {},
    suggestions = [],
  } = useArticleDetail(props);

  const onBookmarkAddError = ({ message }) =>
    errorToast(message || 'Something went wrong.');
  const onUnLikeError = ({ message }) =>
    errorToast(message || 'Something went wrong.');

  const { bookmarkedItems = [], addBookmark, removeBookmark } = useBookmarkHook(
    props,
    { onBookmarkAddError },
  );

  const { likedItems = [], likeItem, unLikeItem } = useLikeHook(props, {
    onUnLikeError,
  });
  const { authentication: { isLoggedIn } = {} } = props;

  React.useEffect(() => {
    document.addEventListener('scroll', onScrollChange);
    return () => {
      document.removeEventListener('scroll', onScrollChange);
    };
  }, []);

  const onScrollChange = React.useCallback(
    () => {
      const suggestionListOffset = document.getElementById('suggestedArticles')
        .offsetTop;
      if (window.pageYOffset + 400 > suggestionListOffset) {
        setActionIconClass('hide');
      } else {
        setActionIconClass('show');
      }
      return 0;
    },
    [actionIconClass],
  );
  const onAuthSuccess = () => {
    GET_USER_BOOKMARK_ARTICLES_API_CALL();
  };

  return (
    <>
      <script
        type="application/ld+json"
        // eslint-disable-next-line react/no-danger
        dangerouslySetInnerHTML={{
          __html: `
          {
            "@context": "https://schema.org",
            "@type": "Article",
            "mainEntityOfPage": {
              "@type": "WebPage",
              "@id": "https://www.kiddenz.com/blog/secrets-of-easy-air-travel-with-a-baby-29"
            },
            "headline": "${articleDetail.post_title}",
            "description": "${articleDetail.post_title}",
            "image": "${articleDetail.post_image}",
            "author": {
              "@type": "Organization",
              "name": "Kiddenz",
              "url": "author"
            },
            "publisher": {
              "@type": "Organization",
              "name": "https://www.kiddenz.com/author",
              "logo": {
                "@type": "ImageObject",
                "url": "https://www.kiddenz.com/_next/static/images/kiddenz-purple%20logo-6ff9395edb6e54e6a2dda1c728993797.svg"
              }
            },
            "datePublished": "${moment(articleDetail.post_date).format(
              'MMMM DD, YYYY',
            )}",
            "dateModified": "${moment(articleDetail.post_date).format(
              'MMMM DD, YYYY',
            )}"
          }`,
        }}
      />
      <Head>
        <title>{`"${articleDetail.category}" article on "${
          articleDetail.post_title
        }"`}</title>
      </Head>
      <Article>
        <Header
          type="articles"
          placeHolderValue="Search by keyword "
          searchBar
          isArticles
          isModalActive={modalStatus}
          activeModalType={modalType}
          setActiveCallback={setModalStatus}
          onOtpSuccessCallback={onAuthSuccess}
          onOnboardingSuccessCallback={onAuthSuccess}
          {...props}
        />
        <MainWrapper background="#fff">
          <ArticleSection>
            <IconList id="actionIcons" className={actionIconClass}>
              <IconButton
                iconName="bi:bookmark"
                margin="0px 0px 20px"
                background={
                  bookmarkedItems.includes(+articleId) ? '#613b97' : '#fff'
                }
                fill={bookmarkedItems.includes(+articleId) ? '#fff' : null}
                onClick={() => {
                  if (isLoggedIn) {
                    if (bookmarkedItems.includes(+articleId))
                      removeBookmark(+articleId);
                    else addBookmark(+articleId);
                  } else {
                    setModalStatus(true);
                    setModalType('login');
                  }
                }}
              />
              <IconButton
                iconName="ant-design:like-outlined"
                margin="0px 0px 20px"
                background={
                  likedItems.includes(+articleId) ? '#613b97' : '#fff'
                }
                fill={likedItems.includes(+articleId) ? '#fff' : null}
                onClick={() => {
                  if (isLoggedIn) {
                    if (likedItems.includes(+articleId)) unLikeItem(+articleId);
                    else likeItem(+articleId);
                  } else {
                    setModalStatus(true);
                    setModalType('login');
                  }
                }}
              />
            </IconList>
            <InterestCard
              categoriesConfig={categoriesConfig}
              //   interestImage={interestImage}
              list={articleDetail.post_tag}
              categoryList={articleDetail.category}
              heading={articleDetail.post_title}
              dateNum={moment(articleDetail.post_date).format('MMMM DD, YYYY')}
              viewNum={articleDetail.viewCount}
              likesNum={articleDetail.likeCount}
              width="100%"
              type="secondary article"
              margin="0px 0px 50px"
              categoryColors={categoryColors}
            />
            {/* <ArticleImage>
              <img src={articleDetail.post_image} alt="" />
            </ArticleImage> */}
            <ArticleContent
              dangerouslySetInnerHTML={{ __html: articleDetail.post_content }}
            />

            <MobileIconList>
              <IconButton
                iconName="bi:bookmark"
                margin="0px 0px 20px"
                background={
                  bookmarkedItems.includes(+articleId) ? '#613b97' : '#fff'
                }
                fill={bookmarkedItems.includes(+articleId) ? '#fff' : null}
                onClick={() => {
                  if (isLoggedIn) {
                    if (bookmarkedItems.includes(+articleId))
                      removeBookmark(+articleId);
                    else addBookmark(+articleId);
                  } else {
                    setModalStatus(true);
                    setModalType('login');
                  }
                }}
              />
              <IconButton
                iconName="ant-design:like-outlined"
                margin="0px 0px 20px"
                background={
                  likedItems.includes(+articleId) ? '#613b97' : '#fff'
                }
                fill={likedItems.includes(+articleId) ? '#fff' : null}
                onClick={() => {
                  if (isLoggedIn) {
                    if (likedItems.includes(+articleId)) unLikeItem(+articleId);
                    else likeItem(+articleId);
                  } else {
                    setModalStatus(true);
                    setModalType('login');
                  }
                }}
              />
            </MobileIconList>
          </ArticleSection>
          <ReadingSection id="suggestedArticles">
            <KiddenText className="center" margin="0px 0px 50px">
              Related Articles
            </KiddenText>

            <Flex justifyBetween className="moreParent">
              {suggestions.map(article => (
                <Link
                  href={`/blog/${article.post_name}-${
                    article.post_id
                  }?category=${article.category}&name=${article.post_title}`}
                >
                  <InterestCard
                    categoriesConfig={categoriesConfig}
                    categoryColors={categoryColors}
                    interestImage={article.post_image}
                    heading={article.post_title}
                    list={article.post_tag}
                    dateNum={moment(article.post_modified).format(
                      'MMMM DD,YYYY',
                    )}
                    viewNum={article.viewCount}
                    likesNum={article.likeCount}
                    width="30%"
                  />
                </Link>
              ))}
            </Flex>
          </ReadingSection>
        </MainWrapper>
        <Footer {...props} />
      </Article>
    </>
  );
};

export default Articles;

Articles.propTypes = {
  authentication: PropTypes.object,
  errorToast: PropTypes.func,
  GET_USER_BOOKMARK_ARTICLES_API_CALL: PropTypes.func,
  query: PropTypes.number,
};

const Article = styled.div`
  .ternary {
    box-shadow: none;
  }
`;

const ArticleSection = styled.div`
  position: relative;
  padding: 100px 20px 0px;
  margin: 0px auto;
  width: 100%;
  max-width: 942px;
  .categories {
    padding: 10px 20px;
  }
  @media screen and (max-width: 900px) {
    padding-top: 30px;
  }
  @media screen and (max-width: 500px) {
    padding-top: 0px;
  }
  .articleList {
    @media screen and (max-width: 800px) {
      padding: 0px 20px;
      margin: 50px 0px;
    }
    @media screen and (max-width: 500px) {
      margin: 20px 0px;
      padding: 0px 5px;
    }
  }
  .articleList-two {
    @media screen and (max-width: 500px) {
      flex-direction: column;
    }
    ${Photo} {
      @media screen and (max-width: 500px) {
        width: 55%;
        margin-bottom: 20px;
      }
    }
  }
`;

const ArticleContent = styled.section`
  font-family: 'Roboto', sans-serif;
  p {
    max-width: 1000px;
    margin-bottom: 1.25em;
    font-family: 'Roboto', sans-serif;
    color: #666c78;
    font-size: 20px;
    letter-spacing: 0;
    line-height: 38px;
    strong {
      color: #30333b !important;
    }
    @media (max-width: 500px) {
      font-size: 16px;
      letter-spacing: 0;
      line-height: 32px;
    }
  }
  h5 {
    max-width: 1000px;
    margin: 1.25em 0;
    color: #30333b;
    font-family: Quicksand;
    font-size: 24px;
    letter-spacing: 0;
    line-height: 30px;
    @media (max-width: 500px) {
      font-size: 22px;
      line-height: 26px;
    }
  }
  h6 {
    max-width: 1000px;
    margin: 1.25em 0;
    color: #30333b;
    font-family: Quicksand;
    font-size: 18px;
    letter-spacing: 0;
    line-height: 26px;
    @media (max-width: 500px) {
      font-size: 14px;
      line-height: 20px;
    }
  }

  h4 {
    max-width: 1000px;
    margin: 1.25em 0;
    color: #30333b;
    font-family: Quicksand;
    font-size: 24px;
    letter-spacing: 0;
    line-height: 30px;
    @media (max-width: 500px) {
      font-size: 22px;
      line-height: 26px;
    }
  }

  ol {
    max-width: 1000px;
    li {
      margin: 0.5rem 0 0 2rem;
      color: #666c78;
      font-family: Roboto;
      font-size: 20px;
      letter-spacing: 0;
      line-height: 38px;
      strong {
        color: #30333b !important;
      }
      @media (max-width: 500px) {
        font-size: 16px;
        line-height: 32px;
        margin: 0.5rem 0 0 0rem;
      }
    }
    @media (max-width: 500px) {
      padding-left: 10px;
    }
  }
  ol[type='I'] {
    padding-left: 0px;
    li {
      margin-left: 15px;
    }
  }

  ul {
    max-width: 1000px;
    li {
      margin: 0.5rem 0 0 2rem;
      color: #666c78;
      font-family: Roboto;
      font-size: 20px;
      letter-spacing: 0;
      line-height: 38px;
      strong {
        color: #30333b !important;
      }
      img {
        width: 100%;
        max-width: 100%;
      }
      @media (max-width: 500px) {
        font-size: 16px;
        line-height: 32px;
        margin: 0.5rem 0 0 0rem;
      }
    }
    @media (max-width: 500px) {
      padding-left: 10px;
    }
  }

  figure {
    max-width: 1000px;
    margin: 0;
    ul {
      list-style: none;
      margin: 0 auto;
      padding: 0;
      max-width: 1000px;

      li {
        list-style: none;
        margin: 0;
        padding: 0;
        font-size: 20px;
        max-width: 1000px;

        img {
          width: 100%;
          max-width: 100%;
        }
      }
    }
    img {
      width: 100%;
      max-width: 100%;
    }
  }
`;

const ReadingSection = styled.div`
  padding: 70px 15px 70px;
  margin: 0px auto;
  width: 100%;
  max-width: 1140px;
  .moreParent {
    @media screen and (max-width: 500px) {
      flex-direction: column;
    }
  }
`;

const IconList = styled.div`
  position: fixed;
  left: 8%;
  top: 30%;
  z-index: 99;
  &.hide {
    opacity: 0;
    transition: opacity 0.3s ease;
  }
  &.show {
    display: block;
    opacity: 1;
    transition: opacity 0.3s ease;
  }
  @media screen and (max-width: 1180px) {
    left: 2%;
  }
  @media screen and (max-width: 767px) {
    display: none !important;
  }
`;
// const ArticleImage = styled.div`
//   width: 100%;
//   //   height: 587px;
//   img {
//     width: 100%;
//     height: 100%;
//   }
// `;

const KiddenText = styled.div`
  font-family: 'Quicksand', sans-serif;
  color: #30333b;
  font-size: 24px;
  font-weight: bold;
  line-height: 30px;
  margin: ${props => props.margin || '0px 0px 20px'};
  &.center {
    text-align: center;
  }
  @media screen and (max-width: 900px) {
    font-size: 20px;
  }
`;

const MobileIconList = styled.div`
  display: none;
  padding: 12px 0px;
  justify-content: flex-end;
  align-items: center;
  border-top: 1px solid #e5e5e5;
  border-bottom: 1px solid #e5e5e5;
  ${IconCard} {
    margin-left: 20px;
    margin-bottom: 0px;
    transform: scale(0.8);
  }
  @media (max-width: 500px) {
    display: flex;
  }
`;

// const Quote = styled.div`
//   padding: 0px 55px 0px 86px;
//   color: #30333b;
//   font-family: 'Quicksand', sans-serif;
//   font-size: 32px;
//   font-weight: 500;
//   line-height: 40px;
//   position: relative;
//   margin: ${props => props.margin || '0px 0px 30px'};

//   .iconify {
//     height: 47px;
//     width: 47px;
//     color: #4bd498;
//     position: absolute;
//     left: 0px;
//     top: 10px;
//   }
// `;

// const Content = styled.div`
//   width: ${props => props.width || '100%'};
//   color: #666c78;
//   font-family: 'Roboto', sans-serif;
//   font-size: 20px;
//   line-height: 38px;
//   margin: ${props => props.margin || '0px 0px 50px'};
//   text-align: justify;
//   @media screen and (max-width: 500px) {
//     font-size: 14px;
//     line-height: 20px;
//     width: 100%;
//   }
// `;
// const ReadingImage = styled.div``;

// const BabyImage = styled.div`
//   width: 100%;
//   margin-bottom: 20px;

//   img {
//     width: 100%;
//     height: 100%;
//   }
// `;
