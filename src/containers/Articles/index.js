import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import Router from 'next/router';
import Qs from 'query-string';
import PropTypes from 'prop-types';
import Link from 'next/link';
import moment from 'moment';
import {
  useArticleHook,
  useBookmarkHook,
  useArticleMostRead,
} from 'shared/hooks';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import MainWrapper from '../../components/MainWrapper';
import Smileycard from '../../components/SmileyCard';
import Flex from '../../components/common/flex';
// import InputBox from '../../components/InputBox';
import InterestCard from '../../components/InterestCard';
import Button from '../../components/Button';
import SchoolCard, { MostRead, Likes } from '../../components/SchoolCard';
// import { Interest } from '../../components/InterestCard';

// import interestImage from '../../images/read1.svg';
import education from '../../images/articleIcons/education.svg';
import behaviour from '../../images/articleIcons/behaviour.svg';
import feeding from '../../images/articleIcons/feeding.svg';
import abuse from '../../images/articleIcons/abuse.svg';
import childActivities from '../../images/articleIcons/child_ctivities.svg';
import dental from '../../images/articleIcons/dental.svg';
import development from '../../images/articleIcons/development.svg';
import nutrition from '../../images/articleIcons/nutrition.svg';
import overallHealth from '../../images/articleIcons/overall_health.svg';
import parenting from '../../images/articleIcons/parenting.svg';
import funImage from '../../images/articleIcons/fun.svg';

import sleep from '../../images/articleIcons/sleep.svg';
import specialChildren from '../../images/articleIcons/special_children.svg';
import technology from '../../images/articleIcons/technology.svg';
import travel from '../../images/articleIcons/travel.svg';
import pottyTraining from '../../images/articleIcons/potty_training.svg';

const handelCategoryClick = (slug, categoryId, name) =>
  Router.push(`/blog/category/${slug}?id=${categoryId}&name=${name}`);

// const handelArticleClick = id =>
//   Router.push({
//     pathname: `/blog/${id}`,
//   });

const Articles = props => {
  const {
    errorToast,
    // successToast,
    GET_USER_BOOKMARK_ARTICLES_API_CALL,
    authentication: {
      isLoggedIn,
      categoryColors = {},
      categoriesConfig = {},
    } = {},
  } = props;
  const {
    // categories: { data: { data: categories = [] } = {} } = {},
    articleList: {
      data: { featured_article: featuredArticle = [], ...restArticleList } = {},
    } = {},
    mappings: { categorySlugMap = {}, categoryIdMap = {} } = {},
    // categoryColors = {},
  } = useArticleHook(props);

  const onBookmarkAddError = ({ message }) =>
    errorToast(message || 'Something went wrong.');

  const { bookmarkedItems = [], addBookmark, removeBookmark } = useBookmarkHook(
    props,
    { onBookmarkAddError },
  );

  const { mostReadArticles = [] } = useArticleMostRead(props);

  const onAuthSuccess = () => {
    GET_USER_BOOKMARK_ARTICLES_API_CALL();
  };

  // const tesklink = `“${category}” article on “${name}”`;
  const [postName, setPostName] = useState('');
  const [postId, setPostId] = useState('');
  const [postImage, setPostImage] = useState('');
  const [postTitle, setPostTile] = useState('');
  const [postCat, setPostCat] = useState('');

  useEffect(() => {
    featuredArticle.map(item => {
      setPostName(item.post_name);
      setPostId(item.post_id);
      setPostImage(item.post_image);
      setPostTile(item.post_title);
      setPostCat(item.category);
    });
  }, []);

  return (
    <>
      <Article>
        <Header
          type="articles"
          placeHolderValue="Search by keyword "
          searchBar
          isArticles
          onOtpSuccessCallback={onAuthSuccess}
          onOnboardingSuccessCallback={onAuthSuccess}
          {...props}
        />
        <MainWrapper background="#fff">
          <ArticleSection>
            <Title>Explore Parenting Blogs</Title>

            <Flex justifyCenter wrap>
              {Object.entries(CATEGORY_LOOK_CONFIG).map(
                ([, { name, redirection, icon }]) => (
                  <Smileycard
                    onClick={() => Router.push(redirection)}
                    text={name}
                    bgcolor="#fff"
                    type="landing"
                    icon={icon}
                  />
                ),
              )}
            </Flex>
            {/* <Flex flexPadding="0px 60px" justifyCenter>
              
            </Flex> */}
            <Flex
              flexWidth="50%"
              flexMargin="20px auto 100px"
              className="parentSubscribe"
            >
              {/* <InputBox margin="0px" type="subscribe" /> */}
            </Flex>
            <Flex
              justifyBetween
              flexMargin="0px 0px 150px"
              className="parentArticle"
            >
              {featuredArticle.map(article => (
                <Link href={`/blog/${article.post_name}-${article.post_id}`}>
                  <InterestCard
                    categoriesConfig={categoriesConfig}
                    isLoggedIn={isLoggedIn}
                    imageHeight="341px"
                    interestImage={article.post_image}
                    heading={article.post_name || ''}
                    list={article.post_tag}
                    categoryList={postCat}
                    dateNum={moment(article.post_modified).format(
                      'MMMM DD, YYYY',
                    )}
                    viewNum={article.viewCount}
                    likesNum={article.likeCount}
                    width="48%"
                    isBookmarked={bookmarkedItems.includes(article.post_id)}
                    onBookmarkClick={() => {
                      if (bookmarkedItems.includes(article.post_id))
                        removeBookmark(article.post_id);
                      else addBookmark(article.post_id);
                    }}
                    categoryColors={categoryColors}
                  />
                </Link>
              ))}
            </Flex>
            <Flex justifyBetween className="fromBabies">
              <Flex column flexWidth="65%">
                {Object.entries(restArticleList)
                  .filter(([key]) => key.toLowerCase() !== 'uncategorized')
                  .filter(([, value]) => value && value.length > 0)
                  .map(([key, value]) => [
                    <Flex justifyBetween flexWidth="100%">
                      <KiddenText>{key}</KiddenText>
                      <Button
                        text="View all"
                        type="viewAll"
                        onClick={() =>
                          handelCategoryClick(
                            categorySlugMap[key],
                            categoryIdMap[key],
                            key,
                          )
                        }
                      />
                    </Flex>,
                    <Flex
                      justifyBetween
                      flexMargin="30px 0px 100px"
                      className="subArticle"
                    >
                      {value.slice(0, 2).map(article => (
                        <Link
                          href={`/blog/${article.post_name}-${
                            article.post_id
                          }?category=${article.category}&name=${article.slug}`}
                        >
                          <InterestCard
                            categoriesConfig={categoriesConfig}
                            imageHeight={value.length > 1 ? '216px' : '463px'}
                            isLoggedIn={isLoggedIn}
                            interestImage={article.post_image}
                            heading={article.post_title}
                            list={article.post_tag}
                            categoryList={article.category}
                            dateNum={moment(article.post_modified).format(
                              'MMMM DD, YYYY',
                            )}
                            viewNum={article.viewCount}
                            likesNum={article.likeCount}
                            width={value.length === 1 ? '100%' : '48%'}
                            isBookmarked={bookmarkedItems.includes(
                              article.post_id,
                            )}
                            onBookmarkClick={() => {
                              if (bookmarkedItems.includes(article.post_id))
                                removeBookmark(article.post_id);
                              else addBookmark(article.post_id);
                            }}
                            categoryColors={categoryColors}
                          />
                        </Link>
                      ))}
                    </Flex>,
                  ])}
              </Flex>
              <Flex flexWidth="31%" column>
                <Flex justifyBetween flexWidth="100%" flexMargin="0px 0px 30px">
                  <KiddenText>Most Read</KiddenText>
                  <div />
                </Flex>
                {mostReadArticles.map(
                  ({
                    post_image: img = '',
                    likeCount,
                    viewCount,
                    post_title: title,
                    post_modified: date,
                    ...article
                  }) => (
                    <SchoolCard
                      schoolImage={img}
                      schoolCardHeight="62px"
                      schoolCardWidth="100px"
                      schoolWrapperMarTop="0px"
                      mostRead={title}
                      dateNum={moment(date).format('MMMM DD, YYYY')}
                      viewNum={viewCount}
                      likesNum={likeCount}
                      cardtype="mostRead"
                      photocardType="bookmark"
                      redirectionLink={`/blog/${article.post_name}-${
                        article.post_id
                      }?category=${article.category}`}
                    />
                  ),
                )}
              </Flex>
            </Flex>
          </ArticleSection>
        </MainWrapper>
        <Footer {...props} />
      </Article>
    </>
  );
};

export default Articles;

Articles.propTypes = {
  query: PropTypes.object,
  router: PropTypes.object,
  authentication: PropTypes.object,
  successToast: PropTypes.func,
  errorToast: PropTypes.func,
  GET_USER_BOOKMARK_ARTICLES_API_CALL: PropTypes.func,
};

const CATEGORY_LOOK_CONFIG = {
  'behaviour-and-discipline': {
    name: 'Behaviour and Discipline',
    icon: behaviour,
    color: '',
    redirection: `/blog/category/behaviour-and-discipline/`,
  },
  development: {
    name: 'Development',
    icon: development,
    color: '',
    redirection: `/blog/category/development/`,
  },
  activities: {
    name: 'Activities',
    icon: childActivities,
    color: '',
    redirection: `/blog/category/development/activities/`,
  },
  awareness: {
    name: 'Awareness',
    icon: abuse,
    color: '',
    redirection: `/blog/category/development/awareness/`,
  },
  'potty-training': {
    name: 'Potty Training',
    icon: pottyTraining,
    color: '',
    redirection: `/blog/category/development/potty-training/`,
  },
  technology: {
    name: 'Technology',
    icon: technology,
    color: '',
    redirection: `/blog/category/development/technology/`,
  },
  'education-and-curriculum': {
    name: 'Education and Curriculum',
    icon: education,
    color: '',
    redirection: `/blog/category/education-and-curriculum/`,
  },
  health: {
    name: 'Health',
    icon: overallHealth,
    color: '',
    redirection: `/blog/category/health/`,
  },
  dental: {
    name: 'Dental',
    icon: dental,
    color: '',
    redirection: `/blog/category/health/dental/`,
  },
  'feeding-your-toddler': {
    name: 'Feeding',
    icon: feeding,
    color: '',
    redirection: `/blog/category/health/feeding-your-toddler/`,
  },
  nutrition: {
    name: 'Nutrition',
    icon: nutrition,
    color: '',
    redirection: `/blog/category/health/nutrition/`,
  },
  // 'overall-health': {
  //   name: 'Overall Health',
  //   icon: overallHealth,
  //   color: '',
  //   redirection: `/blog/category/health/overall-health?main=Health&id=20&name=Overall Health&back=false`,
  // },
  'sleeping-habits': {
    name: 'Sleeping Habits',
    icon: sleep,
    color: '',
    redirection: `/blog/category/health/sleeping-habits/`,
  },
  'parenting-and-childcare': {
    name: 'Parenting and Childcare',
    icon: parenting,
    color: '',
    redirection: `/blog/category/parenting-and-childcare/`,
  },
  'special-child': {
    name: 'Special Child',
    icon: specialChildren,
    color: '',
    redirection: `/blog/category/parenting-and-childcare/special-child/`,
  },
  travel: {
    name: 'Travel',
    icon: travel,
    color: '',
    redirection: `/blog/category/parenting-and-childcare/travel/`,
  },
  fun: {
    name: 'Fun',
    icon: funImage,
    color: '',
    redirection: `/blog/category/development/fun/`,
  },
  // upbringing: {
  //   name: 'Upbringing',
  //   icon: parenting,
  //   color: '',
  //   redirection: `/blog/category/parenting-and-childcare/upbringing?main=Parenting and Childcare&id=17&name=Upbringing&back=false`,
  // },
};

const Article = styled.div`
  .ternary {
    box-shadow: none;
  }
`;

const Title = styled.h1`
  color: #30333b;
  font-family: 'Quicksand', sans-serif;
  font-size: 26px;
  font-weight: 600;
  line-height: 33px;
  text-align: center;
  margin: 20px 0px 50px;
`;

const ArticleSection = styled.div`
  padding: 100px 15px;
  margin: 0px auto;
  width: 100%;
  max-width: 1170px;
  @media screen and (max-width: 1000px) {
    padding: 30px 15px;
  }
  @media screen and (max-width: 500px) {
    padding: 15px 15px;
  }
  .parentArticle {
    @media screen and (max-width: 1000px) {
      margin: 0px 0px 70px;
      & > a {
        width: 49%;
      }
    }
    @media screen and (max-width: 500px) {
      flex-direction: column;
      margin: 0px 0px 30px;
      & > a {
        width: 100%;
      }
    }
  }
  .fromBabies {
    @media screen and (max-width: 767px) {
      flex-direction: column;
    }
    & > div:first-child {
      @media screen and (max-width: 1000px) {
        width: 66%;
      }
      @media screen and (max-width: 767px) {
        width: 100%;
      }
      h3 {
        @media screen and (max-width: 1000px) {
          font-size: 16px;
        }
      }
    }
    & > div:last-child {
      @media screen and (max-width: 1000px) {
        width: 32%;
      }
      @media screen and (max-width: 767px) {
        width: 100%;
      }
      ${MostRead} {
        @media screen and (max-width: 1000px) {
          font-size: 12px;
        }
      }
      ${Likes} {
        @media screen and (max-width: 1000px) {
          font-size: 10px;
        }
      }
    }
  }
  .parentSubscribe {
    @media screen and (max-width: 1000px) {
      width: 60%;
      margin: 15px auto;
    }
    @media screen and (max-width: 1000px) {
      width: 65%;
    }
    @media screen and (max-width: 1000px) {
      width: 100%;
    }
  }
  .subArticle {
    @media screen and (max-width: 500px) {
      flex-direction: column;
      margin-bottom: 30px;
    }
    & > a {
      @media screen and (max-width: 900px) {
        width: 100%;
      }
      @media screen and (max-width: 500px) {
        width: 100%;
      }
    }
  }
`;
const KiddenText = styled.h2`
  font-family: 'Quicksand', sans-serif;
  color: #30333b;
  font-size: 24px;
  font-weight: bold;
  line-height: 30px;
  text-align: center;
  margin: 0px;
  @media screen and (max-width: 900px) {
    font-size: 20px;
  }
`;
