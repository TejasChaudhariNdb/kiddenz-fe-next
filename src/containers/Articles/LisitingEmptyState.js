import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import NoArticle from '../../images/article_empty.png';

export default function LisitingEmptyState({ title }) {
  return (
    <Error>
      <img src={NoArticle} alt="" height={300} />
      <Title>{title}</Title>
    </Error>
  );
}

LisitingEmptyState.propTypes = {
  title: PropTypes.string,
};

const Error = styled.div`
  display: flex;
  justify-content: center;
  height: 50%;
  align-items: center;
  flex-direction: column;
  img {
    @media (max-width: 500px) {
      transform: scale(0.8);
    }
  }
`;

const Title = styled.p`
  color: #555457;
  font-family: Quicksand;
  font-size: 26px;
  letter-spacing: 0;
  line-height: 34px;
  text-align: center;
  font-weight: 800;
`;
