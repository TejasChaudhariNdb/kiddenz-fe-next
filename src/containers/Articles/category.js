/* eslint-disable indent */
import React from 'react';
import Router from 'next/router';
import moment from 'moment';
import styled from 'styled-components';
import Link from 'next/link';
import PropTypes from 'prop-types';
import {
  useArticleCategoryHook,
  useBookmarkHook,
  useArticleMostRead,
} from 'shared/hooks';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import MainWrapper from '../../components/MainWrapper';
// import Smileycard from '../../components/SmileyCard';
import Flex from '../../components/common/flex';
// import InputBox from '../../components/InputBox';
import InterestCard from '../../components/InterestCard/index';
import Button from '../../components/Button';
import SchoolCard from '../../components/SchoolCard';
import LisitingEmptyState from './LisitingEmptyState';
import pulseLoader from '../../images/pulse_loader.gif';

// import interestImage from '../../images/read1.svg';
// import interestImage1 from '../../images/Rectangle.svg';

const Articles = props => {
  const {
    query: { 'category-name': categoryName, id, name } = {},
    errorToast,
    GET_USER_BOOKMARK_ARTICLES_API_CALL,
    authentication: {
      isLoggedIn,
      categoryColors = {},
      categoriesConfig = {},
    } = {},
  } = props;

  const {
    categories: { data: { data: categories = [] } = {} },
    articleList: {
      data: { [categoryName]: featuredArticle = [], ...restArticleList } = {},
    },
    mappings: { categorySlugMap = {} } = {},
    articleCount = 0,
    relatedArticleLoader,
    relatedArticle: { data: relatedArticle = [] } = {},
  } = useArticleCategoryHook(props);

  const handelCategoryClick = (slug, categoryId, category) =>
    Router.push(
      `/blog/category/${categoryName}/${slug}?main=${name}&id=${categoryId}&name=${category}`,
    ).then(() => window.scrollTo(0, 0));

  const onBookmarkAddError = ({ message }) =>
    errorToast(message || 'Something went wrong.');

  const { bookmarkedItems = [], addBookmark, removeBookmark } = useBookmarkHook(
    props,
    { onBookmarkAddError },
  );
  const { mostReadArticles = [] } = useArticleMostRead(props);

  const onAuthSuccess = () => {
    GET_USER_BOOKMARK_ARTICLES_API_CALL();
  };
  return (
    <>
      <Article>
        <Header
          type="articles"
          placeHolderValue="Search by keyword "
          searchBar
          isArticles
          onOtpSuccessCallback={onAuthSuccess}
          onOnboardingSuccessCallback={onAuthSuccess}
          {...props}
        />
        <MainWrapper background="#fff">
          <ArticleSection>
            <Flex justifyBetween className="category">
              <Flex column flexWidth="65%">
                {!areAllSectionsEmpty(restArticleList) ||
                featuredArticle.length > 0 ? (
                  <>
                    <Flex justifyBetween flexWidth="100%">
                      <Title>{name}</Title>
                      {featuredArticle.length > 0 && (
                        <Button
                          text="View all"
                          type="viewAll"
                          onClick={() =>
                            handelCategoryClick(categoryName, id, name)
                          }
                        />
                      )}
                    </Flex>
                    {/* <Flex justifyBetween flexMargin="30px 0px 50px">
                  <InterestCard
                  isLoggedIn={isLoggedIn}
                    interestImage={interestImage}
                    heading="11 surprising skills children need to successfully learn to write"
                    list="#health &nbsp;  #chiledcare &nbsp; #article"
                    dateNum="November 07, 2019"
                    viewNum="22"
                    likesNum="12"
                    width="100%"
                  />
                </Flex> */}
                    {featuredArticle.slice(0, 1).map(article => (
                      <Flex justifyBetween flexMargin="30px 0px 50px">
                        <Link
                          href={`/blog/${article.post_name}-${
                            article.post_id
                          }?category=${article.category}&name=${
                            article.post_title
                          }`}
                        >
                          <InterestCard
                            categoriesConfig={categoriesConfig}
                            isLoggedIn={isLoggedIn}
                            imageHeight="462px"
                            interestImage={article.post_image}
                            heading={article.post_title}
                            list={article.post_tag}
                            categoryList={article.category}
                            dateNum={moment(article.post_modified).format(
                              'MMMM DD,YYYY',
                            )}
                            viewNum={article.viewCount}
                            likesNum={article.likeCount}
                            width="100%"
                            isBookmarked={bookmarkedItems.includes(
                              article.post_id,
                            )}
                            onBookmarkClick={() => {
                              if (bookmarkedItems.includes(article.post_id))
                                removeBookmark(article.post_id);
                              else addBookmark(article.post_id);
                            }}
                            categoryColors={categoryColors}
                          />
                        </Link>
                      </Flex>
                    ))}
                    <Flex column flexWidth="100%">
                      {featuredArticle.slice(1).map(article => (
                        <Link
                          href={`/blog/${article.post_name}-${
                            article.post_id
                          }?category=${article.category}&name=${
                            article.post_title
                          }`}
                        >
                          <InterestCard
                            categoriesConfig={categoriesConfig}
                            isLoggedIn={isLoggedIn}
                            imageHeight="154px"
                            imageWidth="247px"
                            interestImage={article.post_image}
                            heading={article.post_title}
                            list={article.post_tag}
                            categoryList={article.category}
                            dateNum={moment(article.post_modified).format(
                              'MMMM DD,YYYY',
                            )}
                            viewNum={article.viewCount}
                            likesNum={article.likeCount}
                            width="100%"
                            type="primary"
                            margin="0px 0px 50px"
                            isBookmarked={bookmarkedItems.includes(
                              article.post_id,
                            )}
                            onBookmarkClick={() => {
                              if (bookmarkedItems.includes(article.post_id))
                                removeBookmark(article.post_id);
                              else addBookmark(article.post_id);
                            }}
                            categoryColors={categoryColors}
                          />
                        </Link>
                      ))}
                    </Flex>

                    {Object.entries(restArticleList)
                      .filter(([, value]) => value && value.length > 0)
                      .map(([key, value]) => [
                        <Flex justifyBetween flexWidth="100%">
                          <KiddenText>{key}</KiddenText>
                          {value.length > 0 && (
                            <Button
                              text="View all"
                              type="viewAll"
                              onClick={() =>
                                handelCategoryClick(
                                  categorySlugMap[key],
                                  id,
                                  key,
                                )
                              }
                            />
                          )}
                        </Flex>,
                        <Flex
                          column={value.length > 1}
                          justifyBetween={value.length === 1}
                          flexMargin="30px 0px 100px"
                          className="parentingChilcare"
                          // flexMargin="30px 0px 50px"
                        >
                          {value.map(article => (
                            <Link
                              href={`/blog/${article.post_name}-${
                                article.post_id
                              }?category=${article.category}&name=${
                                article.post_title
                              }`}
                            >
                              <InterestCard
                                categoriesConfig={categoriesConfig}
                                isLoggedIn={isLoggedIn}
                                imageHeight={
                                  value.length === 1 ? '462px' : '154px'
                                }
                                imageWidth={
                                  value.length === 1 ? '100%' : '247px'
                                }
                                interestImage={article.post_image}
                                heading={article.post_title}
                                list={article.post_tag}
                                categoryList={article.category}
                                dateNum={moment(article.post_modified).format(
                                  'MMMM DD,YYYY',
                                )}
                                viewNum={article.viewCount}
                                likesNum={article.likeCount}
                                width="100%"
                                type={value.length > 1 ? 'primary' : ''}
                                margin={value.length > 1 ? '0px 0px 50px' : ''}
                                isBookmarked={bookmarkedItems.includes(
                                  article.post_id,
                                )}
                                onBookmarkClick={() => {
                                  if (bookmarkedItems.includes(article.post_id))
                                    removeBookmark(article.post_id);
                                  else addBookmark(article.post_id);
                                }}
                                categoryColors={categoryColors}
                              />
                            </Link>
                          ))}
                        </Flex>,
                      ])}

                    {articleCount < 10 ? (
                      <>
                        <Flex justifyBetween flexWidth="100%">
                          <KiddenText>Related articles</KiddenText>
                        </Flex>
                        {relatedArticleLoader ? (
                          <div className="articleLoader">
                            <img
                              src={pulseLoader}
                              alt=""
                              height={75}
                              width={75}
                            />
                          </div>
                        ) : (
                          <Flex
                            column
                            justifyBetween={false}
                            flexMargin="30px 0px 100px"
                            className="parentingChilcare"
                            // flexMargin="30px 0px 50px"
                          >
                            {relatedArticle.slice(0, 10).map(article => (
                              <Link
                                href={`/blog/${article.post_name}-${
                                  article.post_id
                                }?category=${article.category}&name=${
                                  article.post_title
                                }`}
                              >
                                <InterestCard
                                  categoriesConfig={categoriesConfig}
                                  isLoggedIn={isLoggedIn}
                                  imageHeight="154px"
                                  imageWidth="247px"
                                  interestImage={article.post_image}
                                  heading={article.post_title}
                                  list={article.post_tag}
                                  categoryList={article.category}
                                  dateNum={moment(article.post_modified).format(
                                    'MMMM DD,YYYY',
                                  )}
                                  viewNum={article.viewCount}
                                  likesNum={article.likeCount}
                                  width="100%"
                                  type="primary"
                                  margin="0px 0px 50px"
                                  isBookmarked={bookmarkedItems.includes(
                                    article.post_id,
                                  )}
                                  onBookmarkClick={() => {
                                    if (
                                      bookmarkedItems.includes(article.post_id)
                                    )
                                      removeBookmark(article.post_id);
                                    else addBookmark(article.post_id);
                                  }}
                                  categoryColors={categoryColors}
                                />
                              </Link>
                            ))}
                          </Flex>
                        )}
                      </>
                    ) : null}
                  </>
                ) : (
                  <>
                    <Flex justifyBetween flexWidth="100%">
                      <Title>{name}</Title>
                    </Flex>
                    <LisitingEmptyState title="No Article Found" />
                  </>
                )}
              </Flex>
              <Flex flexWidth="31%" column>
                {/* <Link href="/blog">
                  <Flex justifyBetween flexMargin="0px 0px 50px">
                    <Smileycard
                      text="Categories"
                      type="category"
                      margin="0px"
                    />
                    <Button type="subscribe" text="Subscribe" />
                  </Flex>
                </Link> */}

                {categories.length > 0 ? (
                  <>
                    <Flex
                      justifyBetween
                      flexWidth="100%"
                      flexMargin="0px 0px 30px"
                    >
                      <SubCatHeading>More from {name}</SubCatHeading>
                      <div />
                    </Flex>
                    <Flex
                      wrap
                      flexMargin="0px 0px 90px"
                      className="parentingChilcare"
                    >
                      {categories.map(({ name: subCatname, slug }) => (
                        <MoreBond
                          onClick={() =>
                            handelCategoryClick(slug, id, subCatname)
                          }
                        >
                          {subCatname}
                        </MoreBond>
                      ))}
                      {/* <MoreBond onClick={handelSubCategoryClick}>Emotions</MoreBond>
                  <MoreBond onClick={handelSubCategoryClick}>Mother</MoreBond>
                  <MoreBond onClick={handelSubCategoryClick}>
                    Father & Son
                  </MoreBond>
                  <MoreBond onClick={handelSubCategoryClick}>
                    Pampering
                  </MoreBond>
                  <MoreBond onClick={handelSubCategoryClick}>
                    Father & Daughter
                  </MoreBond> */}
                    </Flex>
                  </>
                ) : null}
                <Flex justifyBetween flexWidth="100%" flexMargin="0px 0px 30px">
                  <KiddenText>Most Read</KiddenText>
                  <div />
                </Flex>
                {mostReadArticles.map(
                  ({
                    post_image: img = '',
                    likeCount,
                    viewCount,
                    post_title: title,
                    post_modified: date,
                    ...article
                  }) => (
                    <SchoolCard
                      schoolImage={img}
                      schoolCardHeight="62px"
                      schoolCardWidth="100px"
                      schoolWrapperMarTop="0px"
                      mostRead={title}
                      dateNum={moment(date).format('MMMM DD, YYYY')}
                      viewNum={viewCount}
                      likesNum={likeCount}
                      cardtype="mostRead"
                      photocardType="bookmark"
                      redirectionLink={`/blog/${article.post_name}-${
                        article.post_id
                      }?category=${article.category}&name=${title}`}
                    />
                  ),
                )}
              </Flex>
            </Flex>
          </ArticleSection>
        </MainWrapper>
        <Footer {...props} />
      </Article>
    </>
  );
};

export default Articles;

Articles.propTypes = {
  query: PropTypes.object,
  authentication: PropTypes.object,
  errorToast: PropTypes.func,
  GET_USER_BOOKMARK_ARTICLES_API_CALL: PropTypes.func,
};

const areAllSectionsEmpty = obj => {
  let isEmpty = true;

  Object.values(obj).map(d => {
    if (d.length > 0) {
      isEmpty = false;
    }
    return 0;
  });
  return isEmpty;
};

const Article = styled.div`
  .ternary {
    box-shadow: none;
  }
`;

const ArticleSection = styled.div`
  padding: 100px 15px;
  margin: 0px auto;
  width: 100%;
  max-width: 1170px;
  @media screen and (max-width: 500px) {
    padding: 20px 15px;
  }
  .categories {
    padding: 10px 20px;
  }
  .category {
    @media screen and (max-width: 500px) {
      flex-direction: column;
    }
    & > div {
      @media screen and (max-width: 500px) {
        width: 100% !important;
      }
    }
  }

  .parentingChilcare {
    @media screen and (max-width: 500px) {
      margin-bottom: 20px;
    }
  }
  .articleLoader {
    margin: 200px 0px 300px;
    display: flex;
    justify-content: center;
  }
`;
const Title = styled.h1`
  font-family: 'Quicksand', sans-serif;
  color: #30333b;
  font-size: 32px;
  font-weight: bold;
  line-height: 30px;
  text-align: center;
  margin: 0px;
  @media screen and (max-width: 900px) {
    font-size: 20px;
  }
  @media screen and (max-width: 500px) {
    font-size: 20px !important;
  }
`;
const KiddenText = styled.h2`
  font-family: 'Quicksand', sans-serif;
  color: #30333b;
  font-size: 24px;
  font-weight: bold;
  line-height: 30px;
  text-align: center;
  margin: 0px;
  @media screen and (max-width: 900px) {
    font-size: 20px;
  }
  @media screen and (max-width: 500px) {
    font-size: 20px !important;
  }
`;

const SubCatHeading = styled.div`
  font-family: 'Quicksand', sans-serif;
  color: #30333b;
  font-size: 24px;
  font-weight: bold;
  line-height: 30px;
  text-align: initial;
  @media screen and (max-width: 900px) {
    font-size: 20px;
  }
`;

const MoreBond = styled.div`
  cursor: pointer;
  padding: 10px 20px;
  width: fit-content;
  border-radius: 5px;
  background-color: #ffffff;
  box-shadow: 2px 2px 8px 0 rgba(0, 0, 0, 0.05);
  color: #666c78;
  font-family: 'Quicksand', sans-serif;
  font-size: 14px;
  font-weight: 500;
  line-height: 28px;
  margin: 0px 10px 10px 0px;
  @media screen and (max-width: 500px) {
    padding: 7px 19px;
  }
`;
