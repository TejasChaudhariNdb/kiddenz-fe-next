/* eslint-disable react/no-unescaped-entities */
import React from 'react';
import PropTypes from 'prop-types';
import Router from 'next/router';
import colors from 'utils/colors';
import styled from 'styled-components';
import { useReviewHook } from 'shared/hooks';
import Flex from '../../components/common/flex';
import Button from '../../components/Button';
import kiddenLogo from '../../images/kiddenz_logo-08.png';
import emplyStar from '../../images/Group 11.png';
import filledStar from '../../images/Group 10.png';
import filledStar1 from '../../images/Group 9-2.png';
import filledStar2 from '../../images/Group 8.png';
import filledStar3 from '../../images/Group 7.png';
import filledStar4 from '../../images/Group 6.png';
import MainWrapper from '../../components/MainWrapper';

const RATING_PARAMETERS = [
  { title: 'Friendliness of director and staff', key: 'staff' },
  { title: 'Facilities', key: 'facility' },
  { title: 'Teachers', key: 'teachers' },
  { title: 'Trust', key: 'trust' },
  { title: 'Curriculum', key: 'curriculum' },
  { title: 'Development of your children', key: 'development' },
  { title: 'Care for your child', key: 'care' },
  { title: 'Hygiene', key: 'hygiene' },
  { title: 'Safety', key: 'safety' },
  { title: 'Location', key: 'location' },
  { title: 'Fees and other expenses', key: 'fees' },
  { title: 'Communication', key: 'communication' },
];

const START_TYPE_CONSTANT = [
  filledStar,
  filledStar1,
  filledStar2,
  filledStar3,
  filledStar4,
];

const Review = props => {
  const {
    router: { query: { provider_name: providerName, slug } = {} } = {},
  } = props;
  const { successToast, errorToast } = props;
  const onSuccess = () => {
    successToast('Successfully Submitted.');
    setTimeout(() => {
      Router.replace({
        pathname: '/',
      });
    }, 2000);
  };

  const onError = ({ message }) => errorToast(message);

  const { onSubmit, onChange, value, ratingError, reviewError } = useReviewHook(
    props,
    {
      onSuccess,
      onError,
    },
  );

  return (
    <MainWrapper background="#fff" paddingTop="0px">
      <ReviewForm>
        <Logo>
          <img src={kiddenLogo} alt="" />
        </Logo>
        <Title>Submit your review</Title>
        <p>
          Dear Parent, <br />
          As you are an important part of your child's journey in our
          preschool/daycare <b>{providerName}</b> . We request you to rate and
          review us. This will help us to improve our services and care
        </p>
        <p>Please rate us between 1 to 5. 5 being the best and 1 the worst</p>
        <ReviewTable>
          <ReviewTableHeader>
            <Title margin="0px 0px 34px">Rate us your opinion:</Title>
            <TableHeading>
              <li>Parameters</li>
              <Flex flexWidth="65%">
                <li>5-Best</li>
                <li>4-Like it</li>
                <li>3-Neutral</li>
                <li>2-Dislike</li>
                <li>1-Worst</li>
              </Flex>
            </TableHeading>
          </ReviewTableHeader>
          <ReviewTableBody>
            {RATING_PARAMETERS.map(d => (
              <ReviewTableList>
                <span>{d.title}</span>
                <Flex flexWidth="65%">
                  {[...Array(5)].map((_, i) => (
                    <StarComponent
                      id={i}
                      // eslint-disable-next-line eqeqeq
                      containerClass={value[d.key] == 5 - i ? 'active' : ''}
                      onChange={onChange}
                      facilityKey={d.key}
                    />
                  ))}
                </Flex>
              </ReviewTableList>
            ))}
          </ReviewTableBody>
        </ReviewTable>
        {ratingError && (
          <span
            style={{
              fontSize: '12px',
              color: 'red',
            }}
          >
            Please fill all the ratings.
          </span>
        )}

        <Title>Write your Review*</Title>
        <textarea
          rows="4"
          className="commentArea"
          placeholder="Write your review..."
          onChange={e => onChange('review', e.target.value)}
        />
        {reviewError && (
          <span
            style={{
              fontSize: '12px',
              color: 'red',
            }}
          >
            Review is required.
          </span>
        )}
        <Title>Improvements</Title>
        <textarea
          rows="4"
          className="commentArea"
          placeholder="Any improvements you feel..."
          onChange={e => onChange('improvement', e.target.value)}
        />
        <Title>Testimonial</Title>
        <textarea
          rows="4"
          className="commentArea"
          placeholder="Your testimonial..."
          onChange={e => onChange('testimonial', e.target.value)}
        />
        <Flex justifyEnd flexMargin="30px 0px 0px">
          <Button
            text="Submit"
            type="mobile"
            onClick={() => onSubmit({ provider_id: slug })}
          />
        </Flex>
      </ReviewForm>
    </MainWrapper>
  );
};

Review.propTypes = {
  errorToast: PropTypes.func,
  successToast: PropTypes.func,
  router: PropTypes.object,
};

export default Review;

const StarComponent = ({ id, containerClass, onChange, facilityKey }) => (
  <Star
    className={containerClass}
    onClick={() => onChange(facilityKey, 5 - id)}
  >
    <img src={emplyStar} alt="" className="emptystar" />
    <img src={START_TYPE_CONSTANT[id]} alt="" className="filledstar" />
  </Star>
);

StarComponent.propTypes = {
  id: PropTypes.number,
  containerClass: PropTypes.string,
  onChange: PropTypes.func,
  facilityKey: PropTypes.string,
};

const ReviewForm = styled.div`
  max-width: 1180px;
  width: 100%;
  padding: 90px 20px;
  margin: 0px auto;
  @media screen and (max-width: 900px) {
    padding: 30px 15px;
  }
  @media screen and (max-width: 500px) {
    padding: 0px 15px 30px;
  }

  p {
    color: #666c78;
    font-family: 'Roboto', sans-serif;
    font-size: 20px;
    line-height: 38px;
    margin: ${props => props.margin || '20px 0px 0px'};
    @media screen and (max-width: 900px) {
      font-size: 18px;
    }
    @media screen and (max-width: 800px) {
      font-size: 16px;
    }
    @media screen and (max-width: 500px) {
      font-size: 12px;
      line-height: 20px;
    }
    @media screen and (max-width: 425px) {
      font-size: 11px;
    }
    @media screen and (max-width: 378px) {
      font-size: 10px;
    }
  }
  .commentArea {
    width: 100%;
    padding: 14px 30px;
    border: 1px solid #c0c8cd;
    border-radius: 5px;
    resize: none;
    outline: none;
    margin: 20px 0px 0px;
    &:focus {
      border: 1px solid ${colors.secondary};
    }
    @media screen and (max-width: 500px) {
      padding: 14px 14px;
      font-size: 12px;
    }
    &::placeholder {
      opacity: 0.5;
      color: #30333b;
      font-family: 'Roboto', sans-serif;
      font-size: 16px;
      line-height: 21px;
      @media screen and (max-width: 500px) {
        font-size: 12px;
      }
      @media screen and (max-width: 500px) {
        font-size: 10px;
      }
    }
  }
`;
const Logo = styled.div`
  height: 34px;
  width: 136px;
  img {
    width: 100%;
    height: 100%;
  }
`;
const Title = styled.div`
  color: #30333b;
  font-family: 'Quicksand', sans-serif;
  font-size: 24px;
  font-weight: bold;
  line-height: 30px;
  margin: ${props => props.margin || '40px 0px 0px'};
  @media screen and (max-width: 900px) {
    font-size: 22px;
  }
  @media screen and (max-width: 500px) {
    font-size: 16px;
    margin-bottom: 20px;
  }
  @media screen and (max-width: 425px) {
    font-size: 16px;
    margin-bottom: 0px;
    margin-top: 10px;
  }
`;
const ReviewTable = styled.div`
  background-color: #faf7ff;
  width: 100%;
  border-radius: 5px;
  margin-top: 20px;
`;

const ReviewTableHeader = styled.div`
  padding: 34px 0px 16px 42px;
  border-bottom: 1px solid #e6e8ec;
  @media screen and (max-width: 900px) {
    padding: 30px 0px 16px 30px;
  }
  @media screen and (max-width: 500px) {
    padding: 20px 0px 8px 12px;
  }
`;
const ReviewTableBody = styled.div``;
const ReviewTableList = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0px 0px 0px 42px;
  ${Flex} {
    @media screen and (max-width: 500px) {
      width: 72%;
    }
    @media screen and (max-width: 500px) {
      width: 80%;
    }
  }
  @media screen and (max-width: 900px) {
    padding: 0px 0px 0px 30px;
  }
  @media screen and (max-width: 500px) {
    padding: 0px 0px 0px 7px;
  }
  span {
    padding: 30px 0px;
    color: #666c78;
    font-family: 'Roboto', sans-serif;
    font-size: 20px;
    line-height: 38px;
    @media screen and (max-width: 900px) {
      font-size: 18px;
      padding: 20px 0px;
    }
    @media screen and (max-width: 800px) {
      font-size: 16px;
    }
    @media screen and (max-width: 500px) {
      font-size: 12px;
      line-height: 16px;
      padding: 5px 0px 0px;
    }
    @media screen and (max-width: 425px) {
      font-size: 11px;
    }
    @media screen and (max-width: 378px) {
      font-size: 10px;
      width: 21%;
    }
  }
`;
const Star = styled.div`
  position: relative;
  width: 25%;
  text-align: center;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 35px 0px;
  border-left: 1px solid #e6e8ec;
  @media screen and (max-width: 900px) {
    padding: 25px 0px;
  }
  @media screen and (max-width: 500px) {
    padding: 15px 0px;
  }

  &.active,
  &:hover {
    .filledstar {
      display: inline;
      position: absolute;
      // margin-top:-10px;
    }
    .emptystar {
      display: none;
    }
  }

  .filledstar {
    display: none;
    cursor: pointer;
    @media screen and (max-width: 900px) {
      transform: scale(0.8);
    }
    @media screen and (max-width: 500px) {
      transform: scale(0.6);
    }
    @media screen and (max-width: 400px) {
      transform: scale(0.4);
    }
  }
  .emptystar {
    cursor: pointer;
    @media screen and (max-width: 900px) {
      transform: scale(0.8);
    }
    @media screen and (max-width: 500px) {
      transform: scale(0.6);
    }
  }
`;

const TableHeading = styled.div`
  display: flex;
  justify-content: space-between;
  & > li {
    color: #30333b;
    font-family: 'Quicksand', sans-serif;
    font-size: 20px;
    font-weight: bold;
    line-height: 25px;
    @media screen and (max-width: 900px) {
      font-size: 18px;
    }
    @media screen and (max-width: 800px) {
      font-size: 16px;
    }
    @media screen and (max-width: 500px) {
      font-size: 12px;
      line-height: 16px;
    }
    @media screen and (max-width: 425px) {
      font-size: 11px;
      line-height: 16px;
    }
    @media screen and (max-width: 378px) {
      font-size: 10px;
      line-height: 14px;
    }
  }
  li {
    list-style-type: none;
    color: #30333b;
    font-family: 'Quicksand', sans-serif;
    font-size: 20px;
    font-weight: bold;
    line-height: 25px;
    width: 25%;
    text-align: center;
    @media screen and (max-width: 900px) {
      font-size: 18px;
    }
    @media screen and (max-width: 800px) {
      font-size: 16px;
    }
    @media screen and (max-width: 500px) {
      font-size: 12px;
    }
    @media screen and (max-width: 425px) {
      font-size: 11px;
    }
    @media screen and (max-width: 378px) {
      font-size: 10px;
    }
  }
  ${Flex} {
    @media screen and (max-width: 500px) {
      width: 72%;
    }
    @media screen and (max-width: 500px) {
      width: 80%;
    }
  }
`;
