/* eslint-disable */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Link from 'next/link';
import Header from 'components/Header';
import Footer from 'components/Footer';
// import SearchBar from 'components/SearchBar';
import MainHeading from 'components/MainHeading';
import { Mainheading } from 'components/MainHeading/styled';
import Flex from 'components/common/flex';
import colors from 'utils/colors';

import careerBanner from '../../images/career_kiddenz.png';
import supportImage from '../../images/register school.svg';

const NavItems = ['General', 'Parents', 'Providers'];
const FAQs = props => {
  const [activeTab, setActiveTab] = useState('General');
  const [activeItem, setActiveItem] = useState('privacy');
  const { query: { section, question } = {} } = props;

  const reviewsRef = useRef(null);
  const advertiseRef = useRef(null);

  useEffect(
    () => {
      if (section && question) {
        setActiveTab(section);
        setActiveItem(question);

        if (
          section === 'Providers' &&
          question === 'howGetReview' &&
          reviewsRef.current
        ) {
          reviewsRef.current.scrollIntoView({
            behavior: 'smooth',
          });
        }

        if (
          section === 'Providers' &&
          question === 'howGetAdvertise' &&
          advertiseRef.current
        ) {
          advertiseRef.current.scrollIntoView({
            behavior: 'smooth',
          });
        }
      }
    },
    [section, question, reviewsRef.current, advertiseRef.current],
  );

  return (
    <>
      <Header
        {...props}
        type="articles"
        placeHolderValue="Search Article"
        searchBar={false}
      />
      <CareerSection>
        <Flex
          column
          className="careerDetails"
          flexWidth="40%"
          flexHeight="100%"
          flexPadding="80px 0px 0px"
        >
          <Title>Frequently Asked Questions</Title>

          <KiddenText>
            Our Frenquently asked questions will help you find answers for all
            your questions
          </KiddenText>
          {/* <MainHeading fontSize="18px" margin="0px 0px 28px 0px" text="Join our team! We are Bangalore based start-up and we are ready to hire new talent to our team" /> */}

          {/* <SearchBar margin="0px" width="100%" /> */}
          <NavBar>
            {NavItems.map(x => (
              <>
                <h2
                  className={activeTab === x ? 'active' : ''}
                  onClick={() => setActiveTab(x)}
                >
                  {x}
                </h2>
              </>
            ))}
          </NavBar>
        </Flex>
        <CareerBannerImage>
          <img src={careerBanner} alt="" />
        </CareerBannerImage>
      </CareerSection>
      <hr style={{ margin: 0 }} />
      <FAQSection>
        <Flex justifyBetween className="questions-section">
          <Flex flexWidth="60%" column className="queries">
            {activeTab === 'General' ? (
              <div className="generalQuestion">
                <Flex
                  column
                  className="questions-list"
                  flexBorderdashed="1px solid #E6E8EC"
                  flexPadding="0px 0px 50px"
                  flexMargin="0px 0px 50px"
                >
                  <MainHeading
                    text="Resources"
                    fontSize="24px"
                    margin="0px 0px 40px 0px"
                  />
                  {/* <MainHeading
                text="Resources"
                fontSize="24px"
                margin="0px 0px 40px 0px"
              /> */}
                  <Question
                    className={activeItem === 'privacy' ? 'active' : ''}
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(activeItem === 'privacy' ? '' : 'privacy')
                      }
                    >
                      <h2>
                        1. Privacy policy
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      Click to view
                      <a
                        href="https://kiddenz-media.s3-ap-southeast-1.amazonaws.com/Privacy+Policy_Kiddenz-converted.pdf"
                        target="_blank"
                      >
                        privacy policy
                      </a>
                      .
                    </div>
                  </Question>
                  <Question className={activeItem === 'terms' ? 'active' : ''}>
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(activeItem === 'terms' ? '' : 'terms')
                      }
                    >
                      <h2>
                        2. Terms of service
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      Click to view{' '}
                      <a
                        href="https://kiddenz-media.s3-ap-southeast-1.amazonaws.com/Terms+of+Use_Kiddenz-converted.pdf"
                        target="_blank"
                      >
                        terms of service
                      </a>
                      .
                    </div>
                  </Question>
                  <Question
                    className={activeItem === 'postGuideline' ? 'active' : ''}
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'postGuideline' ? '' : 'postGuideline',
                        )
                      }
                    >
                      <h2>
                        3. What’s okay to post on Kiddenz?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      <ol type="i">
                        <li>Genuine reviews</li>
                        <li>Correct information</li>
                        <li>Questions</li>
                        <li>
                          Comments on articles. You may disagree with
                          article/comments, however, be polite and respectful
                          for other person’s options.
                        </li>
                      </ol>
                    </div>
                  </Question>
                  <Question
                    className={activeItem === 'howIConnect' ? 'active' : ''}
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'howIConnect' ? '' : 'howIConnect',
                        )
                      }
                    >
                      <h2>
                        4. How do I contact someone at Kiddenz?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      Please email on{' '}
                      <span>
                        <EmailAction href="mailto:hello@kiddenz.com">
                          hello@kiddenz.com
                        </EmailAction>
                      </span>
                    </div>
                  </Question>
                </Flex>
                <Flex
                  column
                  className="questions-list"
                  flexBorderdashed="1px solid #E6E8EC"
                  flexPadding="0px 0px 50px"
                  flexMargin="0px 0px 50px"
                >
                  <MainHeading
                    text="Account Queries"
                    fontSize="24px"
                    margin="0px 0px 40px 0px"
                  />
                  <Question
                    className={activeItem === 'howIregister' ? 'active' : ''}
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'howIregister' ? '' : 'howIregister',
                        )
                      }
                    >
                      <h2>
                        1. How do I register?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      Click on the <span>“Register”</span> icon on the top right
                      corner of the Kiddenz page to register. Alternatively
                      Kiddenz will prompt Parents to register at appropriate
                      times.
                    </div>
                  </Question>
                  <Question
                    className={activeItem === 'howIdelete' ? 'active' : ''}
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'howIdelete' ? '' : 'howIdelete',
                        )
                      }
                    >
                      <h2>
                        2. How do I delete my account?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      Go to Account page and click on{' '}
                      <span>“delete my account”.</span>
                    </div>
                  </Question>
                  <Question
                    className={activeItem === 'howIunsubscribe' ? 'active' : ''}
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'howIunsubscribe'
                            ? ''
                            : 'howIunsubscribe',
                        )
                      }
                    >
                      <h2>
                        3. How do I unsubscribe from email or notifications?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      Click on unsubscribe link in the emails/promotions.
                    </div>
                  </Question>

                  <Question
                    className={activeItem === 'howIrecommend' ? 'active' : ''}
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'howIrecommend' ? '' : 'howIrecommend',
                        )
                      }
                    >
                      <h2>
                        4. How do I recommend Kiddenz to other parents?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      Write a review or testimonial for Kiddenz. Share Kiddenz
                      link and website. Write a review on Kiddenz Facebook and
                      LinkedIn page.
                    </div>
                  </Question>
                  <Question
                    className={activeItem === 'howIchangeEmail' ? 'active' : ''}
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'howIchangeEmail'
                            ? ''
                            : 'howIchangeEmail',
                        )
                      }
                    >
                      <h2>
                        5. How can I change my email address and password?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      Please go to Account page and click on edit profile
                      option.
                    </div>
                  </Question>
                </Flex>
              </div>
            ) : null}
            {activeTab === 'Parents' ? (
              <div className="parentQuestion">
                <Flex
                  column
                  className="questions-list"
                  flexBorderdashed="1px solid #E6E8EC"
                  flexPadding="0px 0px 50px"
                  flexMargin="0px 0px 50px"
                >
                  <MainHeading
                    text="Getting Started"
                    fontSize="24px"
                    margin="0px 0px 40px 0px"
                  />
                  <Question
                    className={activeItem === 'whatIsKiddenx' ? 'active' : ''}
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'whatIsKiddenx' ? '' : 'whatIsKiddenx',
                        )
                      }
                    >
                      <h2>
                        1.What is Kiddenz?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      Kiddenz is a Preschool and Daycare search platform,
                      designed to help parents find quality preschool and
                      daycare for their kids. Check{' '}
                      <Link href="/about-us">
                        <a>“About Us”</a>
                      </Link>{' '}
                      to know more.
                    </div>
                  </Question>
                  <Question
                    className={activeItem === 'whatServcies' ? 'active' : ''}
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'whatServcies' ? '' : 'whatServcies',
                        )
                      }
                    >
                      <h2>
                        2. What kind of child care services are available on
                        Kiddenz?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      Kiddenz providers a list of trusted, licensed child care
                      providers.
                      <ol type="i">
                        <li>Daycare</li>
                        <li>Preschool</li>
                        <li>Play school</li>
                        <li>Creche</li>
                      </ol>
                    </div>
                  </Question>
                </Flex>
                <Flex
                  column
                  className="questions-list"
                  flexBorderdashed="1px solid #E6E8EC"
                  flexPadding="0px 0px 50px"
                  flexMargin="0px 0px 50px"
                >
                  <MainHeading
                    text="Using Kiddenz"
                    fontSize="24px"
                    margin="0px 0px 40px 0px"
                  />
                  <Question
                    className={activeItem === 'howIuse' ? 'active' : ''}
                    onClick={() =>
                      setActiveItem(activeItem === 'howIuse' ? '' : 'howIuse')
                    }
                  >
                    <div className="questionTop">
                      <h2>
                        1. How do I use Kiddenz?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      <ol type="i">
                        <li>
                          Kiddenz is simple and easy to use. Click on find
                          preschool/daycare, then just select either Location
                          (Enter the location) or Trip Route (your most
                          frequently travelled route, ex: Home location to
                          Office location, select usual time of travel).
                        </li>
                        <li>
                          Browse the results on the Map or scroll down. Use the
                          filters on the top of the result page to refine your
                          search and select the preschool/daycare that meets
                          your requirement.
                        </li>
                        <li>
                          Check the care provider page and book appointment,
                          request information or directly connect with the
                          provider.
                        </li>
                      </ol>
                    </div>
                  </Question>
                  <Question
                    className={
                      activeItem === 'locationSearchMean' ? 'active' : ''
                    }
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'locationSearchMean'
                            ? ''
                            : 'locationSearchMean',
                        )
                      }
                    >
                      <h2>
                        2. What does search by location mean?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      Enter the location where you need the child care and
                      search by location displays most relevant
                      preschools/daycare in and near that location.
                    </div>
                  </Question>
                  <Question
                    className={activeItem === 'tripSearchMean' ? 'active' : ''}
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'tripSearchMean'
                            ? ''
                            : 'tripSearchMean',
                        )
                      }
                    >
                      <h2>
                        3. How to use search by trip route?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      Enter the “From” and “To” location (Ex. Home location to
                      Office location), enter usual time when you leave (From
                      Home to Office) and search by trip route will show all the
                      preschool/daycare that are near From ( Ex. Home location)
                      ,To (Ex. Office location) and in between the two location.
                    </div>
                  </Question>
                  <Question
                    className={activeItem === 'whenTrip' ? 'active' : ''}
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'whenTrip' ? '' : 'whenTrip',
                        )
                      }
                    >
                      <h2>
                        4. When should I use search by trip route?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      <ol type="i">
                        <li>
                          Trip route function helps you locate all the
                          preschool/daycare on your daily travel route. The
                          facilities are located along the route in order to
                          facilitate quick drop and pickup.
                        </li>
                        <li>
                          It is useful when you don’t have find the ideal care
                          near home or office.{' '}
                        </li>
                      </ol>
                    </div>
                  </Question>
                  <Question
                    className={
                      activeItem === 'howParentRegsiter' ? 'active' : ''
                    }
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'howParentRegsiter'
                            ? ''
                            : 'howParentRegsiter',
                        )
                      }
                    >
                      <h2>
                        5. How do I register?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      Click on the “Register” icon on the top right corner of
                      the Kiddenz page to register. Alternatively Kiddenz will
                      prompt Parents to register at appropriate times.
                    </div>
                  </Question>
                  <Question
                    className={
                      activeItem === 'whyCompleteParentProfile' ? 'active' : ''
                    }
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'whyCompleteParentProfile'
                            ? ''
                            : 'whyCompleteParentProfile',
                        )
                      }
                    >
                      <h2>
                        6. Why should I complete my parent profile?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      A complete parent profile, enables care providers to offer
                      better service to parents when parents go to enroll their
                      kids. Also profiles that are complete receive promotional
                      offers from care providers.
                    </div>
                  </Question>
                  <Question
                    className={activeItem === 'howShortlist' ? 'active' : ''}
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'howShortlist' ? '' : 'howShortlist',
                        )
                      }
                    >
                      <h2>
                        7. How do I shortlist favorite providers?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      Once you like the page of the care provider, you can
                      shortlist the preschool/daycare by clicking at the “heart
                      symbol” next the name of the preschool/daycare.
                    </div>
                  </Question>
                  <Question
                    className={
                      activeItem === 'wishlistedBookmarked' ? 'active' : ''
                    }
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'wishlistedBookmarked'
                            ? ''
                            : 'wishlistedBookmarked',
                        )
                      }
                    >
                      <h2>
                        8. Where can I see my shortlisted providers and
                        articles?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      The shortlisted providers and articles are available in
                      the user profile section.
                    </div>
                  </Question>
                </Flex>
                <Flex
                  column
                  className="questions-list"
                  flexBorderdashed="1px solid #E6E8EC"
                  flexPadding="0px 0px 50px"
                  flexMargin="0px 0px 50px"
                >
                  <MainHeading
                    text="Connecting with a Child care provider"
                    fontSize="24px"
                    margin="0px 0px 40px 0px"
                  />
                  <Question
                    className={activeItem === 'howDoIFind' ? 'active' : ''}
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'howDoIFind' ? '' : 'howDoIFind',
                        )
                      }
                    >
                      <h2>
                        1. What do I do after I find a child care provider I am
                        interested in?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      <ol type="i">
                        <li>
                          Click on contact information to get contact details.
                        </li>
                        <li>Book and appointment and schedule a visit.</li>
                        <li>Write a review for the provider. (optional)</li>
                      </ol>
                    </div>
                  </Question>
                  <Question
                    className={
                      activeItem === 'connectWithParentWhat' ? 'active' : ''
                    }
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'connectWithParentWhat'
                            ? ''
                            : 'connectWithParentWhat',
                        )
                      }
                    >
                      <h2>
                        2. What does “connect with parent” mean?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      “Connect with parents” function with connect with a parent
                      whose kid is currently enrolled in the preschool/daycare.
                      This helps you to get an unbiased feedback before
                      enrollment.
                    </div>
                  </Question>
                  <Question
                    className={
                      activeItem === 'confirmScheduleTime' ? 'active' : ''
                    }
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'confirmScheduleTime'
                            ? ''
                            : 'confirmScheduleTime',
                        )
                      }
                    >
                      <h2>
                        3. How long does it take to confirm a scheduled tour?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      Update on schedule tour requests will be confirmed within
                      24 hours. If not then please connect on
                      <span>
                        <EmailAction href="mailto:hello@kiddenz.com">
                          hello@kiddenz.com
                        </EmailAction>
                      </span>
                    </div>
                  </Question>
                  <Question
                    className={activeItem === 'noAdmission' ? 'active' : ''}
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'noAdmission' ? '' : 'noAdmission',
                        )
                      }
                    >
                      <h2>
                        4. What do to when you can&#39;t get through to a
                        daycare or preschool?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      Sometimes it might happen that the Child care providers
                      are busy with children — and may not always be able to
                      pick up the phone. Connect with Kiddenz team via{' '}
                      <span>
                        <EmailAction href="mailto:service@kiddenz.com ">
                          service@kiddenz.com
                        </EmailAction>
                      </span>
                      or Connect with counselor tab.
                    </div>
                  </Question>
                  <Question
                    className={
                      activeItem === 'howToWriteTestimonial' ? 'active' : ''
                    }
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'howToWriteTestimonial'
                            ? ''
                            : 'howToWriteTestimonial',
                        )
                      }
                    >
                      <h2>
                        5. How do I write review and testimonial?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      <ol type="i">
                        <li>
                          You may write review on the provider page. Reviews are
                          verified and go live within 24 hours.
                        </li>
                        <li>
                          To write a testimonial for Kiddenz or Provider, kindly
                          write and send it on
                          <span>
                            <EmailAction href="mailto:hello@kiddenz.com">
                              hello@kiddenz.com
                            </EmailAction>
                          </span>
                        </li>
                      </ol>
                    </div>
                  </Question>
                  <Question
                    className={activeItem === 'wrongInfo' ? 'active' : ''}
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'wrongInfo' ? '' : 'wrongInfo',
                        )
                      }
                    >
                      <h2>
                        6. How do I report out of date and incorrect information
                        about a provider?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      Contact information can change over time and sometimes the
                      listing is out of date. If you find the phone number or
                      email address on the Kiddenz listing is disconnected or
                      goes to the wrong person, report the page to let us know
                      what happened and we&#39;ll fix it right away.
                    </div>
                  </Question>
                  <Question
                    className={activeItem === 'reportProvider' ? 'active' : ''}
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'reportProvider'
                            ? ''
                            : 'reportProvider',
                        )
                      }
                    >
                      <h2>
                        7. How do I report a provider?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      Please connect on
                      <span>
                        <EmailAction href="mailto:hello@kiddenz.com">
                          hello@kiddenz.com
                        </EmailAction>
                      </span>{' '}
                      to report a provider.
                    </div>
                  </Question>
                </Flex>
              </div>
            ) : null}
            {activeTab === 'Providers' ? (
              <div className="providerQuestion">
                <Flex
                  column
                  className="questions-list"
                  flexBorderdashed="1px solid #E6E8EC"
                  flexPadding="0px 0px 50px"
                  flexMargin="0px 0px 50px"
                >
                  <MainHeading
                    text="Getting Started"
                    fontSize="24px"
                    margin="0px 0px 40px 0px"
                  />
                  <Question
                    className={
                      activeItem === 'howRegisterSchool' ? 'active' : ''
                    }
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'howRegisterSchool'
                            ? ''
                            : 'howRegisterSchool',
                        )
                      }
                    >
                      <h2>
                        1.How do I register my preschool, daycare, crèche, and
                        play school on Kiddenz?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      Please check the
                      <Link href="/provider">
                        <a>Provider Page”</a>
                      </Link>
                    </div>
                  </Question>
                  <Question
                    className={activeItem === 'kiddenzConnect' ? 'active' : ''}
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'kiddenzConnect'
                            ? ''
                            : 'kiddenzConnect',
                        )
                      }
                    >
                      <h2>
                        2.How to connect with Kiddenz?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      Please email on{' '}
                      <span>
                        <EmailAction href="mailto:operations@kiddenz.com">
                          operations@kiddenz.com
                        </EmailAction>
                      </span>
                    </div>
                  </Question>
                </Flex>
                <Flex
                  column
                  className="questions-list"
                  flexBorderdashed="1px solid #E6E8EC"
                  flexPadding="0px 0px 50px"
                  flexMargin="0px 0px 50px"
                >
                  <MainHeading
                    text="Dashboard Management"
                    fontSize="24px"
                    margin="0px 0px 40px 0px"
                  />
                  <Question
                    className={activeItem === 'dashboard' ? 'active' : ''}
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'dashboard' ? '' : 'dashboard',
                        )
                      }
                    >
                      <h2>
                        1.What is a dashboard?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      A provider dashboard helps you manage your Kiddenz page,
                      track your leads, update information and respond to
                      parents.
                    </div>
                  </Question>
                  <Question
                    className={activeItem === 'accessDashboard' ? 'active' : ''}
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'accessDashboard'
                            ? ''
                            : 'accessDashboard',
                        )
                      }
                    >
                      <h2>
                        2.How will I get access to dashboard?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      <ol type="i">
                        <li>
                          If you have not registered your preschool/daycare on
                          Kiddenz, then please check the “ Provider Page”
                        </li>
                        <li>
                          If you have registered, then please click here to get
                          the login credentials.
                        </li>
                      </ol>
                    </div>
                  </Question>
                  <Question className={activeItem === 'leads' ? 'active' : ''}>
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(activeItem === 'leads' ? '' : 'leads')
                      }
                    >
                      <h2>
                        3.Lead Management
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      <ol type="i">
                        <li>Enquires </li>
                        <li>Tours </li>
                        <li>Reviews </li>
                      </ol>
                    </div>
                  </Question>
                  <Question
                    className={activeItem === 'insights' ? 'active' : ''}
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'insights' ? '' : 'insights',
                        )
                      }
                    >
                      <h2>
                        4.Page Insights (Launching Soon)
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      Thank you for your interest in Kiddenz Insights. We are
                      currently not active with this service. If you have any
                      questions or feedback, please email{' '}
                      <span>
                        <EmailAction href="mailto:operations@kiddenz.com">
                          operations@kiddenz.com
                        </EmailAction>
                      </span>
                    </div>
                  </Question>
                  <Question
                    className={activeItem === 'advertise' ? 'active' : ''}
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'advertise' ? '' : 'advertise',
                        )
                      }
                    >
                      <h2>
                        5.Advertise
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      <ol type="i">
                        <li>
                          Upgrade to a featured listing and we&#39;ll advertise
                          your program to local parents searching for preschool
                          and daycare.
                          <ol type="1">
                            <li>
                              Quickly fill your current and upcoming vacancies.
                            </li>
                            <li>Build an admission pipeline.</li>
                            <li>
                              Advertise your new centers to maximum number of
                              parents.
                            </li>
                            <li>Start getting leads as soon as you upgrade.</li>
                          </ol>
                        </li>
                        <li>
                          Advertising on Kiddenz is easy! Follow these steps to
                          select and start your plan!
                          <ol type="1">
                            <li>Email on advertise@kiddenz.com</li>
                            <li>
                              Kiddenz team will update on suitable plans that
                              suit your requirement.
                            </li>
                            <li>Pay online</li>
                            <li>
                              Congratulations! Your subscription is live, start
                              receiving leads.
                            </li>
                          </ol>
                        </li>
                      </ol>
                    </div>
                  </Question>
                  <Question className={activeItem === 'edit' ? 'active' : ''}>
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(activeItem === 'edit' ? '' : 'edit')
                      }
                    >
                      <h2>
                        6.Edit Profile
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      Edit the information on your page (Program, contact
                      information, Teacher information, Pictures, Facilities and
                      more) using the edit profile function. The changes will be
                      live in 24 to 48 hours after submission. Use this function
                      to keep your page updated for high parent engagement and
                      leads.
                    </div>
                  </Question>
                </Flex>
                <Flex
                  column
                  className="questions-list"
                  flexBorderdashed="1px solid #E6E8EC"
                  flexPadding="0px 0px 50px"
                  flexMargin="0px 0px 50px"
                >
                  <MainHeading
                    text="Trust and Safety"
                    fontSize="24px"
                    margin="0px 0px 40px 0px"
                  />
                  <Question className={activeItem === 'review' ? 'active' : ''}>
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(activeItem === 'review' ? '' : 'review')
                      }
                    >
                      <h2>
                        1.How does Kiddenz ensure genuine reviews?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      Kiddenz verifies each and every critical review with both
                      reviewer and the provider, so that every review is genuine
                      and helps other parents.
                    </div>
                  </Question>
                  <Question
                    className={activeItem === 'elseEdit' ? 'active' : ''}
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'elseEdit' ? '' : 'elseEdit',
                        )
                      }
                    >
                      <h2>
                        2.Can anyone else edit my page?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      The page can be edited only through the dashboard. Hence
                      to edit the user has to login in the provider dashboard.
                    </div>
                  </Question>
                  <Question
                    className={activeItem === 'pageReview' ? 'active' : ''}
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'pageReview' ? '' : 'pageReview',
                        )
                      }
                    >
                      <h2>
                        3.Can I remove my page?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      Yes, please write an email to{' '}
                      <span>
                        <EmailAction href="mailto:operations@kiddenz.com">
                          operations@kiddenz.com
                        </EmailAction>
                      </span>
                    </div>
                  </Question>
                  <Question
                    className={activeItem === 'infoSafe' ? 'active' : ''}
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'infoSafe' ? '' : 'infoSafe',
                        )
                      }
                    >
                      <h2>
                        4.Is my information safe?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      Yes, provider information is safe, Kiddenz doesn’t share
                      provider information with third parties. Only users with
                      verified credentials get to see the provider data on
                      Kiddenz.
                    </div>
                  </Question>
                </Flex>
                <Flex
                  column
                  className="questions-list"
                  flexBorderdashed="1px solid #E6E8EC"
                  flexPadding="0px 0px 50px"
                  flexMargin="0px 0px 50px"
                >
                  <MainHeading
                    text="Offer Promotions"
                    fontSize="24px"
                    margin="0px 0px 40px 0px"
                  />
                  <Question
                    className={
                      activeItem === 'offerHighlighlight' ? 'active' : ''
                    }
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'offerHighlighlight'
                            ? ''
                            : 'offerHighlighlight',
                        )
                      }
                    >
                      <h2>
                        1.Highlight offers
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      Highlight the promotions or offers (Ex. % off on admission
                      fee, discounts) Use this to reach all the parents in your
                      locality.
                    </div>
                  </Question>
                  <Question
                    className={activeItem === 'openPlaces' ? 'active' : ''}
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'openPlaces' ? '' : 'openPlaces',
                        )
                      }
                    >
                      <h2>
                        2.List availability (Open places)
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      Highlight vacancies to quickly fill the open slots in your
                      preschool/daycare.
                    </div>
                  </Question>
                </Flex>
                <Flex
                  column
                  className="questions-list"
                  flexBorderdashed="1px solid #E6E8EC"
                  flexPadding="0px 0px 50px"
                  flexMargin="0px 0px 50px"
                  ref={reviewsRef}
                >
                  <MainHeading
                    text="Referrals"
                    fontSize="24px"
                    margin="0px 0px 40px 0px"
                  />
                  <Question
                    ref={advertiseRef}
                    className={
                      activeItem === 'howGetReview' ? 'long active' : 'long'
                    }
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'howGetReview' ? '' : 'howGetReview',
                        )
                      }
                    >
                      <h2>
                        1.How do I get reviews for my program on Kiddenz?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      <ol type="i">
                        <li>
                          Parent reviews can really help you showcase what&#39;s
                          great about your program. User reviews are an
                          important part of your daycare or preschool listing.
                          Our users want to hear from other parents and feel
                          confident that you&#39;re a great provider. Child care
                          pages with more than 4 parent reviews get much more
                        </li>
                        <li>
                          Ground rules
                          <ol type="1">
                            <li>
                              No incentivized reviews. It is okay to ask happy
                              customers to leave a review, but don&#39;t pay
                              them for it!
                            </li>
                            <li>
                              Only ask real customers who have used your
                              services. Reviews from employees or associates of
                              your business will be removed.
                            </li>
                            <li>
                              No copy/pasting from other review sites.
                              Duplicated reviews will be removed.
                            </li>
                            <li>
                              Remember review will help parents visiting
                              Kiddenz! Anyone you ask for a review should be
                              able to review your business from the perspective
                              of a parent.
                            </li>
                          </ol>
                        </li>
                        <li>
                          Step by step process of review.
                          <ol type="1">
                            <li>
                              Online reviews are a crucial part of marketing
                              your business and it&#39;s good to get in the
                              practice of reminding your customers to write
                              them. Here are a few strategies you can use.
                            </li>
                            <li>
                              Invite parents to review by entering their email
                              address
                            </li>
                            <li>
                              Email specific parents asking them to leave a
                              review. We already created the invitation email
                              template for you, all you need to do is to enter
                              the parents&#39; emails and send invitations.
                            </li>
                          </ol>
                        </li>
                      </ol>
                      <ol type="1">
                        <li>
                          Go to the Manage Reviews page on your dashboard.
                        </li>
                        <li>Enter your customer&#39;s full email address</li>
                        <li>
                          Click the Send Invites button. A formatted email
                          asking for review will be sent to the parent.
                        </li>
                        <li>
                          Done! Your review invitation has been sent to the
                          email addresses you have entered.
                        </li>
                      </ol>
                      Share the link of your Kiddenz review page by copy and
                      paste
                      <ol type="1">
                        <li>Copy your page link on Kiddenz.</li>
                        <li>
                          You can now paste this link to all of your social
                          media accounts!{' '}
                        </li>
                        <li>
                          Paste your Kiddenz review URL to your email signature.
                          An example might be: &#34;Review us on Kiddenz to help
                          us reach more families!&#34;
                        </li>
                        <li>
                          Similar to the above, you can also add the review URL
                          link in your website footer.
                        </li>
                      </ol>
                    </div>
                  </Question>
                  <Question
                    className={
                      activeItem === 'howGetAdvertise' ? 'long active' : 'long'
                    }
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'howGetAdvertise'
                            ? ''
                            : 'howGetAdvertise',
                        )
                      }
                    >
                      <h2>
                        2.Can I advertise my program?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      <ol type="i">
                        <li>
                          Upgrade to a featured listing and we&#39;ll advertise
                          your program to local parents searching for preschool
                          and daycare.
                          <ol type="1">
                            <li>
                              Quickly fill your current and upcoming vacancies.
                            </li>
                            <li>Build an admission pipeline.</li>
                            <li>
                              Advertise your new centers to maximum number of
                              parents.
                            </li>
                            <li>Start getting leads as soon as you upgrade.</li>
                          </ol>
                        </li>
                        <li>
                          Advertising on Kiddenz is easy! Follow these steps to
                          select and start your plan!
                          <ol type="1">
                            <li>Email on advertise@kiddenz.com</li>
                            <li>
                              Kiddenz team will update on suitable plans that
                              suit your requirement.
                              <li>Pay online</li>
                              <li>
                                Congratulations! Your subscription is live,
                                start receiving leads.
                              </li>
                            </li>
                          </ol>
                        </li>
                      </ol>
                    </div>
                  </Question>
                  <Question
                    className={
                      activeItem === 'stopLeads' ? 'long active' : 'long'
                    }
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'stopLeads' ? '' : 'stopLeads',
                        )
                      }
                    >
                      <h2>
                        3.Fully enrolled? Stop getting leads.
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      Please email to{' '}
                      <span>
                        <EmailAction href="mailto:operations@kiddenz.com">
                          operations@kiddenz.com
                        </EmailAction>
                      </span>
                    </div>
                  </Question>
                </Flex>
                <Flex
                  column
                  className="questions-list"
                  flexBorderdashed="1px solid #E6E8EC"
                  flexPadding="0px 0px 50px"
                  flexMargin="0px 0px 50px"
                >
                  <MainHeading
                    text="Others"
                    fontSize="24px"
                    margin="0px 0px 40px 0px"
                  />
                  <Question
                    className={activeItem === 'isItFree' ? 'active' : ''}
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'isItFree' ? '' : 'isItFree',
                        )
                      }
                    >
                      <h2>
                        1.Is Kiddenz free?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      Yes registering and creating your page on Kiddenz is
                      completely free.
                    </div>
                  </Question>
                  <Question
                    className={activeItem === 'iAmaChain' ? 'active' : ''}
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'iAmaChain' ? '' : 'iAmaChain',
                        )
                      }
                    >
                      <h2>
                        2.I have more than one center, how do I enroll all my
                        centers?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      Yes, please connect with Kiddenz team on
                      <EmailAction href="mailto:operations@kiddenz.com">
                        operations@kiddenz.com
                      </EmailAction>
                    </div>
                  </Question>
                  <Question
                    className={
                      activeItem === 'manageAllCenters' ? 'active' : ''
                    }
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'manageAllCenters'
                            ? ''
                            : 'manageAllCenters',
                        )
                      }
                    >
                      <h2>
                        3.Can I manage all my centers from one dashboard?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      Yes, you may manage all center from a centralized
                      dashboard. Please connect with Kiddenz team on
                      operations@kiddenz.com for more information.
                    </div>
                  </Question>
                  <Question
                    className={
                      activeItem === 'whoIsAllowedToReview' ? 'active' : ''
                    }
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'whoIsAllowedToReview'
                            ? ''
                            : 'whoIsAllowedToReview',
                        )
                      }
                    >
                      <h2>
                        4.Who is allowed to review my page?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      Registered users are allowed to review your page. However,
                      every critical review is verified by Kiddenz team.
                    </div>
                  </Question>
                  <Question
                    className={
                      activeItem === 'howToRemovePhotos' ? 'active' : ''
                    }
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'howToRemovePhotos'
                            ? ''
                            : 'howToRemovePhotos',
                        )
                      }
                    >
                      <h2>
                        5.How do I add or remove photos?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      You may add and remove photos from the edit profile
                      section of the dashboard.
                    </div>
                  </Question>
                  <Question
                    className={activeItem === 'editInfoOnPage' ? 'active' : ''}
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'editInfoOnPage'
                            ? ''
                            : 'editInfoOnPage',
                        )
                      }
                    >
                      <h2>
                        6.How do I edit information in my page?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      You may edit information from the edit profile section of
                      the dashboard.
                    </div>
                  </Question>
                  <Question
                    className={activeItem === 'getMeVirtual' ? 'active' : ''}
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'getMeVirtual' ? '' : 'getMeVirtual',
                        )
                      }
                    >
                      <h2>
                        7.How do I get the virtual tour done for my facility?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      Kiddenz team supports in getting the virtual tour done at
                      a subsidized cost. Alternatively you can get it done
                      externally. Please connect on{' '}
                      <span>
                        <EmailAction href="mailto:operations@kiddenz.com">
                          operations@kiddenz.com
                        </EmailAction>
                      </span>{' '}
                      to connect with Kiddenz.
                    </div>
                  </Question>
                  <Question
                    className={activeItem === 'PleaseManageMe' ? 'active' : ''}
                  >
                    <div
                      className="questionTop"
                      onClick={() =>
                        setActiveItem(
                          activeItem === 'PleaseManageMe'
                            ? ''
                            : 'PleaseManageMe',
                        )
                      }
                    >
                      <h2>
                        8.How do I add other people to manage my page?
                        <span
                          className="iconify"
                          data-icon="eva:arrow-ios-downward-outline"
                          data-inline="false"
                        />
                      </h2>
                    </div>
                    <div className="questionBottom">
                      User management can be enabled on request. Please connect
                      on <span>operations@kiddenz.com</span> to connect with
                      Kiddenz.
                    </div>
                  </Question>
                </Flex>
              </div>
            ) : null}
          </Flex>
          <Flex flexWidth="28%" className="support">
            <Support>
              <img src={supportImage} alt="" />
              <h2>Support</h2>
              <h3>
                For assistance call us at
                <span>+91 8088340733</span>
              </h3>
              <h3>
                You can write to us at
                <span>
                  <EmailAction href="mailto:hello@kiddenz.com">
                    hello@kiddenz.com
                  </EmailAction>
                </span>
              </h3>
            </Support>
          </Flex>
        </Flex>
      </FAQSection>

      <Footer {...props} />
    </>
  );
};
FAQs.propTypes = {
  query: PropTypes.object,
};
export default FAQs;

export const CareerSection = styled.div`
  position: relative;
  margin-top: 106px;
  display: flex;
  justify-content: flex-end;
  max-width: 1290px;
  margin-left: auto;
  @media (max-width: 500px) {
    margin-top: 70px;
  }
  .careerDetails {
    position: absolute;
    left: 20px;
    @media (max-width: 900px) {
      padding-top: 15px;
      width: 50%;
    }
    @media (max-width: 500px) {
      width: 90%;
      padding-top: 0px;
    }
  }
  ${Mainheading} {
    @media (max-width: 900px) {
      font-size: 24px;
    }
    @media (max-width: 500px) {
      font-size: 20px;
    }
  }
`;

export const FAQSection = styled.div`
  max-width: 1180px;
  margin: 0px auto;
  padding: 50px 20px 100px;
  //   border-top: 1px solid rgba(230, 232, 236, 1);
  ${Mainheading} {
    @media (max-width: 500px) {
      font-size: 20px;
      line-height: 25px;
      margin-bottom: 10px;
    }
  }
  .support {
    @media (max-width: 900px) {
      width: 40%;
    }
    @media (max-width: 500px) {
      width: 100%;
    }
  }
  .queries {
    @media (max-width: 900px) {
      width: 58%;
    }
    @media (max-width: 500px) {
      width: 100%;
    }
  }
  .applySection {
    display: flex;
    width: 100%;
    flex-flow: row wrap;
    justify-content: space-between;
  }

  .applySection::after {
    content: '';
    width: 24%;
    margin-left: 20px;
    @media (max-width: 900px) {
      width: 32%;
    }
  }
  .questions-section {
    @media (max-width: 500px) {
      flex-direction: column;
    }
    & > div {
      @media (max-width: 500px) {
        width: 100%;
      }
    }
  }
  .questions-list {
    @media (max-width: 500px) {
      margin-bottom: 20px;
      padding-bottom: 30px;
    }
  }
`;
export const CareerBannerImage = styled.div`
  width: 50%;
  height: 435px;
  @media (max-width: 900px) {
    height: 300px;
  }
  @media (max-width: 500px) {
    width: 100%;
    height: 230px;
    margin-top: 200px;
    margin-bottom: 70px;
  }
  img {
    width: 100%;
    height: 100%;
  }
`;
const Title = styled.h1`
  font-family: 'Quicksand', sans-serif;
  margin: 0px 0px 10px 0px;
  font-size: 32px;
  color: #2d3438;
  font-weight: bold;
  line-height: 45px;
  z-index: 1;
  @media (max-width: 900px) {
    font-size: 24px;
  }
  @media (max-width: 500px) {
    font-size: 20px;
    margin-bottom: 10px;
  }
`;
const KiddenText = styled.h2`
  font-family: 'Quicksand', sans-serif;
  margin: ${props => props.textmargin || '0px 0px 28px 0px;'};
  color: #30333b;
  font-size: 18px;
  font-weight: bold;
  line-height: 30px;
  padding-right: 100px;
  @media (max-width: 900px) {
    font-size: 14px;
    line-height: 20px;
    padding-right: 30px;
  }
`;

const NavBar = styled.ul`
  display: flex;
  width: 100%;
  justify-content: space-between;
  margin-top: auto;
  margin-bottom: 0px;
  padding: 0px;
  h2 {
    position: relative;
    color: rgba(48, 51, 59, 1);
    font-family: 'Quicksand', sans-serif;
    font-size: 16px;
    font-weight: 700;
    line-height: 20px;
    padding: 0px 20px 28px 20px;
    list-style-type: none;
    cursor: pointer;
    margin: 0px;
    @media (max-width: 900px) {
      font-size: 13px;
      padding-bottom: 15px;
    }
    @media (max-width: 500px) {
      padding: 0px 12px 28px 12px;
    }

    &:hover,
    &.active {
      color: ${colors.secondary};
    }
    &::after {
      position: absolute;
      content: '';
      width: 0%;
      left: 0px;
      bottom: 0px;
      height: 3px;
      transition: width 0.3s ease;
      background-color: ${colors.secondary};
    }
    &:hover::after,
    &.active::after {
      width: 100%;
      transition: width 0.3s ease;
    }
  }
`;

const Question = styled.div`
  height: 40px;
  max-height: 40px;
  overflow: hidden;
  transition: max-height 0.3s ease;
  margin-bottom: 30px;
  @media (max-width: 500px) {
    margin-bottom: 10px;
  }
  &.active {
    height: auto;
    max-height: 500px;
    transition: max-height 0.3s ease;
    &.long {
      max-height: 1000px;
    }
    @media (max-width: 500px) {
      max-height: 400px;
    }
    h2 {
      color: #613a95;
    }
    .iconify {
      transform: rotate(180deg) !important;
      transition: transform 0.3s ease;
    }
  }
  &:last-child {
    margin-bottom: 0px;
  }
  .questionBottom {
    padding: 0px 15px;
    color: #666c78;
    font-family: Roboto;
    font-size: 14px;
    letter-spacing: 0;
    line-height: 21px;
    @media (max-width: 500px) {
      font-size: 13px;
      padding: 0px 0px 0px 15px;
    }
    ol[type='i'] {
      @media (max-width: 900px) {
        padding-left: 15px;
      }
      @media (max-width: 500px) {
        padding-left: 0px;
      }
    }
    ol[type='1'] {
      @media (max-width: 900px) {
        padding-left: 20px;
      }
      @media (max-width: 500px) {
        padding-left: 15px;
      }
    }
    p {
      position: relative;
      list-style-type: none;
      margin-bottom: 4px;
      @media (max-width: 500px) {
        margin-bottom: 2px;
      }
      &::after {
        position: absolute;
        content: '';
        width: 5px;
        height: 5px;
        border-radius: 50%;
        background-color: #613a95;
        left: -12px;
        top: 8px;
      }
    }
    span {
      color: #613a95;
    }
    a {
      text-decoration: none;
      margin-left: 6px;
    }
  }
  h2 {
    display: flex;
    justify-content: space-between;
    color: #30333b;
    font-family: Quicksand;
    font-size: 18px;
    letter-spacing: 0;
    line-height: 23px;
    margin: 18px 0px;
    @media (max-width: 500px) {
      font-size: 15px;
    }
    .iconify {
      transform: rotate(180deg);
      transition: transform 0.3s ease;
      @media (max-width: 500px) {
        min-width: 20px;
        height: 20px;
        margin-left: 10px;
      }
    }
  }
`;

const Support = styled.div`
  height: fit-content;
  display: flex;
  align-items: center;
  flex-direction: column;
  padding: 30px 50px;
  width: 100%;
  border-radius: 10px;
  background-color: #ffffff;
  box-shadow: 2px 2px 20px 0 rgba(0, 0, 0, 0.05);
  @media (max-width: 500px) {
    padding: 20px 20px;
  }
  h2 {
    color: #613a95;
    font-family: Quicksand;
    font-size: 20px;
    letter-spacing: 0;
    line-height: 25px;
    text-align: center;
    @media (max-width: 500px) {
      font-size: 18px;
    }
  }
  h3 {
    display: flex;
    flex-direction: column;
    color: #666c78;
    font-family: Quicksand;
    font-size: 15px;
    font-weight: 500;
    letter-spacing: 0;
    line-height: 21px;
    text-align: center;
    @media (max-width: 500px) {
      font-size: 13px;
    }
    span {
      color: #613a95;
    }
  }
`;

const EmailAction = styled.a`
  color: inherit;
  text-decoration: none;
`;
