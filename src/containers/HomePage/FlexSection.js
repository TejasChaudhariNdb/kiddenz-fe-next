import styled from 'styled-components';
import Section from './Section';

const FlexSection = styled(Section)`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export default FlexSection;
