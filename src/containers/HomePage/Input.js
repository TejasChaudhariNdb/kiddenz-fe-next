import styled from 'styled-components';
import colors from 'utils/colors';

const Input = styled.input`
  height: 56px;
  width: 100%;
  max-width: 442px;
  display: flex;
  align-items: center;
  padding: 12px 34px 12px 20px;
  outline: none;
  border: ${props => props.inputBorder || 'none'};
  border-radius: 5px;
  background-color: ${colors.white};
  font-size: 16px;
  line-height: 21px;
  text-overflow: ellipsis;
  &.search {
    height: auto;
    display: block;
    padding: 0px;
    outline: none;
    border: ${props => props.inputBorder || 'none'};
    font-size: 16px;
    line-height: 21px;
    text-overflow: ellipsis;
    padding: 0px !important;
    white-space: nowrap;
    overflow: hidden;
    border-radius: 0px;
    background-color: transparent;
    @media (max-width: 500px) {
      font-size: 14px;
      padding: 0px;
    }
  }

  &::placeholder {
    color: ${colors.inputPlaceholder};
    font-family: 'Roboto', sans-serif;
    font-size: 16px;
    line-height: 21px;
  }
  @media (max-width: 500px) {
    font-size: 14px;
    padding: 12px 12px;
  }
  @media (max-width: 320px) {
    font-size: 14px;
  }
`;

export default Input;
