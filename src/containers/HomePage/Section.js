import styled from 'styled-components';

const Section = styled.section`
  flex-grow: 1;
  padding: 40px 30px;
  // padding-top: 40px;
  // padding-bottom: 40px;
  // background-image: url("./../../images/headerBg.svg")
  // margin: 3em auto;

  // &:first-child {
  //   margin-top: 0;
  // }
`;

export default Section;
