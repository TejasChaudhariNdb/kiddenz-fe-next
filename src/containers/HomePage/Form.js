import styled from 'styled-components';
import colors from 'utils/colors';
const Form = styled.form`
  margin-bottom: 1em;
  .myCurrentLocation {
    display: none;
    border-radius: 5px;
    background-color: #fff;
    height: 56px;
    width: 100%;
    max-width: 442px;
    align-items: center;
    padding: 12px 20px;
    margin-top: 0px;
    cursor: pointer;
    span {
      font-size: 16px;
      line-height: 21px;
      color: #a3a9b7;
      font-family: 'Roboto', sans-serif;
      @media (max-width: 500px) {
        font-size: 14px;
        line-height: 16px;
      }
    }
    .iconify {
      width: 20px;
      height: 20px;
      color: #60b947;
      margin-right: 10px;
    }
    @media (max-width: 900px) {
      margin-top: 1px;
    }
    @media (max-width: 500px) {
      height: 46px;
      padding: 12px 12px;
    }
    &.active {
      display: flex;
    }
  }
  .searchInputField {
    position: relative;
    height: 56px;
    width: 100%;
    max-width: 442px;
    display: flex;
    align-items: center;
    padding: 12px 20px;
    outline: none;
    border: ${props => props.inputBorder || 'none'};
    border-radius: 5px;
    background-color: ${colors.white};
    &.error::after {
      position: absolute;
      content: '';
      bottom: 0px;
      left: 20px;
      height: 4px;
      background-color: #ff6767;
      width: 94%;
      border-radius: 14px;
      @media (max-width: 500px) {
        left: 12px;
      }
    }
    &.error input::-webkit-input-placeholder {
      color: #ff6767;
    }
    @media (max-width: 500px) {
      padding: 12px 12px;
    }

    &.two {
      border: 1px solid #c0c8cd;
      input {
        @media (max-width: 500px) {
          font-size: 14px;
        }
      }
      input::-webkit-input-placeholder {
        @media (max-width: 500px) {
          font-size: 14px;
        }
      }
    }
  }
  &.locationForm {
    position: relative;
    .searchInputField {
      @media (max-width: 500px) {
        height: 46px;
      }
    }
    input {
      padding: 0px 20px 0px 0px;
      max-height: 20px;
      @media (max-width: 500px) {
        padding: 0px;
        height: auto;
        font-size: 14px;
      }
      &::-webkit-input-placeholder {
        @media (max-width: 500px) {
          font-size: 14px;
        }
      }
    }
  }
`;

export default Form;
