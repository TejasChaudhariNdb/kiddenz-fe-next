/* eslint-disable no-nested-ternary */
/* eslint-disable indent */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
/* eslint-disable-line no-alert */
/* eslint-disable */
/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React, { useEffect, useState, useRef } from 'react';
import Router from 'next/router';
import Link from 'next/link';
import TimePicker from 'rc-time-picker';
import PropTypes from 'prop-types';
// import { css } from 'styled-components';
import styled from 'styled-components';
// import amplitude from 'amplitude-js';
import Header from 'components/Header';
import moment from 'moment';
import Footer from 'components/Footer';
import MainWrapper from 'components/MainWrapper';
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';
import Geocode from 'react-geocode';
import Button from 'components/Button';
import H1 from 'components/H1';
import H2 from 'components/H2';
import colors from 'utils/colors';
import { MAPS_API_KEY, GAevent } from 'utils/google';
import {
  useDropdownClose,
  useDayCareSearchHook,
  useIntrestingArticlesHook,
  useBookmarkHook,
  useArticleHook,
  useClickCount,
} from 'shared/hooks';
import { CLICK_COUNTS } from 'utils/constants';
import Form from './Form';
import Input from './Input';
// import Section from './Section';
import FlexSection from './FlexSection';
import Flex from '../../components/common/flex';
import Ruppee from '../../components/common/Ruppee';
import SchoolCard from '../../components/SchoolCard';
import RadioButton from '../../components/RadioButton';
import MainHeading from '../../components/MainHeading';
import { Mainheading } from '../../components/MainHeading/styled';
import InterestCard from '../../components/InterestCard';
import Smileycard from '../../components/SmileyCard';
import interestImage from '../../images/read1.svg';
import interestImage1 from '../../images/Rectangle.svg';
import interestImage2 from '../../images/read3.svg';
import background from '../../images/header bg (2).svg';
import registernow from '../../images/registernow.svg';
import daycare from '../../images/Daycare.jpg';
import route from '../../images/toroute.png';
import elle from '../../images/elle.png';
import oi from '../../images/oi.png';
import tiny from '../../images/tiny.png';
import jumbo from '../../images/jumbo.png';
import klya from '../../images/klya.png';
import currentLocation from '../../images/currentLocMarker.svg';
import poweredByGoogle from '../../images/powered_by_google_on_white.png';

import education from '../../images/articleIcons/education.svg';
import behaviour from '../../images/articleIcons/behaviour.svg';
import feeding from '../../images/articleIcons/feeding.svg';
import abuse from '../../images/articleIcons/abuse.svg';
import childActivities from '../../images/articleIcons/child_ctivities.svg';
import dental from '../../images/articleIcons/dental.svg';
import development from '../../images/articleIcons/development.svg';
import nutrition from '../../images/articleIcons/nutrition.svg';
import overallHealth from '../../images/articleIcons/overall_health.svg';
import parenting from '../../images/articleIcons/parenting.svg';
import funImage from '../../images/articleIcons/fun.svg';

import sleep from '../../images/articleIcons/sleep.svg';
import specialChildren from '../../images/articleIcons/special_children.svg';
import technology from '../../images/articleIcons/technology.svg';
import travel from '../../images/articleIcons/travel.svg';
import pottyTraining from '../../images/articleIcons/potty_training.svg';
import { mdiConsoleNetwork } from '@mdi/js';
const SCHEDULE_CONFIG = { 'full time': 1, weekend: 3, flexible: 2 };

export function HomePage(props) {
  const { articles } = useIntrestingArticlesHook(props);
  const {
    categories: { data: { data: categories = [] } = {} } = {},
    mappings: { categorySlugMap = {}, categoryIdMap = {} } = {},
  } = useArticleHook(props);

  const {
    postCount,
    clickData: {
      location_search: locationSearchClickCount = 0,
      trip_search: tripSearchClickCount = 0,
    } = {},
  } = useClickCount(props);
  const {
    errorToast,
    authentication: {
      categoryColors = {},
      categoriesConfig = {},
      isLoggedIn,
      profile: {
        data: {
          selected_filter: {
            child_care_type: organisation,
            schedule_type: schedule,
            transport_facility: transportation,
            food_facility: food,
            average_age: maxAge,
          } = {},
        } = {},
      } = {},
    } = {},
    query: { search_by: searchBy } = {},
  } = props;
  const onBookmarkAddError = ({ message }) =>
    errorToast(message || 'Something went wrong.');

  const { bookmarkedItems = [], addBookmark, removeBookmark } = useBookmarkHook(
    props,
    { onBookmarkAddError },
  );

  const locationSearchDropdown = useRef(null);
  const tripLocationSearchDropdown = useRef(null);

  const [searchType, setSearchType] = useState('location');
  // Location Type states
  const [locationSearchSting, setLocationSearchSting] = useState('');
  const [locationSearchError, setLocationSearchError] = useState(false);
  const [
    locationSearchDropdownState,
    setLocationSearchDropdownState,
  ] = useState(false);
  const [locationLat, setLocationLat] = useState(0.0);
  const [locationLng, setLocationLng] = useState(0.0);

  // Trip Type states
  const [activeTripLocationInput, setActiveTripLocationInput] = useState(null);
  const [fromLocationSearchSting, setFromLocationSearchSting] = useState('');
  const [toLocationSearchSting, setToLocationSearchSting] = useState('');

  const [fromLocationSearchError, setFromLocationSearchError] = useState(false);
  const [toLocationSearchError, setToLocationSearchError] = useState(false);

  const [
    tripLocationSearchDropdownState,
    setTripLocationSearchDropdownState,
  ] = useState(false);

  const [fromLocationLat, setFromLocationLat] = useState(0.0);
  const [toLocationLat, setToLocationLat] = useState(0.0);
  const [fromLocationLng, setFromLocationLng] = useState(0.0);
  const [toLocationLng, setToLocationLng] = useState(0.0);

  const [fromTime, setFromTime] = useState('08:00');
  // const [fromTimeError, setFromTimeError] = useState(false);

  const [locationItemClicked, setLocationItemClicked] = useState(false);
  const [locationGeoCodeInProgress, setlocationGeoCodeInProgress] = useState(
    false,
  );
  const [modalStatus, setModalStatus] = useState(false);
  const [modalType, setModalType] = useState('email');
  const [restrictionType, setRestrictionType] = useState(null);

  // detect current location
  const [showCureentLocation, setShowCureentLocation] = useState(false);
  useDropdownClose(locationSearchDropdown, setLocationSearchDropdownState);
  useDropdownClose(
    tripLocationSearchDropdown,
    setTripLocationSearchDropdownState,
  );

  useEffect(
    () => {
      if (searchBy) setSearchType(searchBy);
    },
    [searchBy],
  );

  const handleSelect = (selected, isDefault = true, type) => {
    setlocationGeoCodeInProgress(true);
    setLocationItemClicked(true);

    // If normal Location search
    if (isDefault) {
      setLocationSearchSting(selected);
      setLocationSearchDropdownState(false);
      setLocationSearchError(false);
    }

    // If Trip search
    if (type === 'from') {
      setFromLocationSearchSting(selected);
      setTripLocationSearchDropdownState(false);
      setFromLocationSearchError(false);
    }
    if (type === 'to') {
      setToLocationSearchSting(selected);
      setTripLocationSearchDropdownState(false);
      setToLocationSearchError(false);
    }
    geocodeByAddress(selected)
      .then(res => getLatLng(res[0]))
      .then(({ lat, lng }) => {
        // If normal Location search
        if (isDefault) {
          setLocationLat(lat);
          setLocationLng(lng);
          if (window.innerWidth <= 768) {
            if (isLoggedIn) {
              /* If user has click location search equal to or more than n times 
                Block the content access
            */
              if (locationSearchClickCount >= CLICK_COUNTS.locationSearch) {
                setModalType('restriction');
                setRestrictionType('location search');
                setModalStatus(true);
              } else {
                postCount('location_search');
                GAevent('Search', 'Location Search Clicked', 'Location Search');
                Router.push(
                  `/daycare/search?search_type=${searchType}&locationString=${selected}&latitude=${lat}&longitude=${lng}&max_age=${Math.floor(
                    maxAge * 12,
                  )}&transportation=0&food=0&type=radius&sort_by=distance`,
                ).then(() => window.scrollTo(0, 0));
              }
            } else {
              GAevent('Search', 'Location Search Clicked', 'Location Search');
              Router.push(
                `/daycare/search?search_type=${searchType}&locationString=${selected}&latitude=${lat}&longitude=${lng}&type=radius&sort_by=distance`,
              ).then(() => window.scrollTo(0, 0));
            }
          }
        }

        // If Trip search
        if (type === 'from') {
          setFromLocationLat(lat);
          setFromLocationLng(lng);
        } else {
          setToLocationLat(lat);
          setToLocationLng(lng);
        }

        setlocationGeoCodeInProgress(false);
      })
      .catch(error => {
        // this.setState({ isGeocoding: false });
      });
  };

  const handelCategoryClick = (slug, categoryId, name) =>
    Router.push({ pathname: `/blog/category/${slug}` });

  const locationRedirection = ({ lat, lng, locationString, businessType }) => {
    Router.push(
      `/daycare/search?search_type=location&locationString=${locationString}&latitude=${lat}&longitude=${lng}&organisation=${businessType}&type=radius&sort_by=distance`,
    ).then(() => window.scrollTo(0, 0));
  };

  const getLocation = () => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        ({ coords: { latitude: lati, longitude: lngi } = {} }) => {
          setLocationLat(lati);
          setLocationLng(lngi);
          Geocode.fromLatLng(
            String(lati.toFixed(5)),
            String(lngi.toFixed(5)),
          ).then(({ results = [] }) => {
            if (results[0]) {
              const locName = results[0].formatted_address;
              setLocationSearchSting(locName);
              if (window.innerWidth <= 768) {
                if (isLoggedIn) {
                  /* If user has click location search equal to or more than n times 
                        Block the content access
                    */
                  if (locationSearchClickCount >= CLICK_COUNTS.locationSearch) {
                    setModalType('restriction');
                    setRestrictionType('location search');
                    setModalStatus(true);
                  } else {
                    postCount('location_search');
                    GAevent(
                      'Search',
                      'Location Search Clicked',
                      'Location Search',
                    );
                    Router.push(
                      `/daycare/search?search_type=location&locationString=${locName}&latitude=${lati}&longitude=${lngi}&max_age=${Math.floor(
                        maxAge * 12,
                      )}&transportation=0&food=0&type=radius&sort_by=distance`,
                    ).then(() => window.scrollTo(0, 0));
                  }
                } else {
                  GAevent(
                    'Search',
                    'Location Search Clicked',
                    'Location Search',
                  );
                  Router.push(
                    `/daycare/search?search_type=location&locationString=${locName}&latitude=${lati}&longitude=${lngi}&type=radius&sort_by=distance`,
                  ).then(() => window.scrollTo(0, 0));
                }
              }
            }
          });

          setlocationGeoCodeInProgress(false);
        },
        err => {
          window.alert(
            'Location access is blocked. Change your location settings in browser or select location manually',
          );
        },
      );
    }
  };

  return (
    <>
      <Header
        {...props}
        isModalActive={modalStatus}
        activeModalType={modalType}
        setActiveCallback={setModalStatus}
        clickType={restrictionType}
      />
      <MainWrapper>
        <title>Home Page</title>
        <meta
          name="description"
          content="A React.js Boilerplate application homepage"
        />

        <MainSection style={{ background: '#fff' }}>
          <img src={background} alt="" />
          <H1
            text="Discover and Select Best Preschool and Daycare"
            textAlign="center"
            margin="64px 0px 0px"
          />
          <LandingSubTitle>
            Register to get Discounts up-to ₹8000 on Admissions in any
            Preschool or Daycare
          </LandingSubTitle>
          <SearchWrapper>
            {/* Location Search */}
            <Layout
              onClick={() => {
                setSearchType('location');
                setFromLocationSearchSting('');
                setToLocationSearchSting('');
                setToLocationSearchError(false);
                setFromLocationSearchError(false);

                if (searchType === 'Trip')
                  Router.replace(`/daycare/landing`, `/daycare/landing`, {
                    shallow: true,
                  });
              }}
            >
              <RadioButton
                id="test1"
                text="Location"
                checkedColor="#fff"
                checked={searchType === 'location'}
                onClick={() => {}}
              />
              <Msg>Click here to search at a particular location</Msg>
              <Form className="locationForm">
                <PlacesAutocomplete
                  value={locationSearchSting}
                  onChange={address => {
                    setLocationItemClicked(false);
                    // if (address) {
                    //   setLocationSearchError(false);
                    // } else {
                    //   setLocationSearchError(true);
                    // }
                    setLocationSearchSting(address);
                  }}
                  onSelect={address => handleSelect(address)}
                  onError={() => {}}
                  clearItemsOnError
                  shouldFetchSuggestions
                  searchOptions={{
                    componentRestrictions: { country: ['in'] },
                  }}
                >
                  {({ getInputProps, suggestions, getSuggestionItemProps }) => {
                    if (suggestions.length > 0)
                      setLocationSearchDropdownState(true);
                    else setLocationSearchDropdownState(false);

                    return (
                      <>
                        <label htmlFor="search_box">
                          <div
                            className={`searchInputField ${
                              locationSearchError ? 'error' : ''
                            }`}
                          >
                            <Input
                              {...getInputProps({
                                onBlur: () => {
                                  // if (locationSearchSting)
                                  //   setLocationSearchError(false);
                                  // else setLocationSearchError(true);
                                  // if (locationItemClicked)
                                  //   setLocationSearchError(false);
                                  // else setLocationSearchError(true);
                                },
                                onFocus: () => {
                                  setShowCureentLocation(true);
                                  setLocationSearchError(false);
                                },
                              })}
                              id="search_box"
                              type="text"
                              placeholder="Enter your location"
                              value={locationSearchSting}
                              // disabled={searchType !== 'location'}
                              autoComplete="off"
                            />
                            {locationSearchError && (
                              <DestinationError>
                                Location is Required
                              </DestinationError>
                            )}
                          </div>
                          {!locationSearchError &&
                            showCureentLocation && (
                              <div
                                className="myCurrentLocation active"
                                onClick={getLocation}
                              >
                                <span
                                  className="iconify"
                                  data-icon="ic:outline-location-searching"
                                  data-inline="false"
                                />
                                <span>Detect my location</span>
                              </div>
                            )}
                        </label>

                        <div
                          style={{ width: '166px' }}
                          className="buttonWrapper"
                        >
                          <Button
                            text="Search"
                            headerButton
                            marginTop="10px"
                            className="locationBtn"
                            onClick={e => {
                              // amplitude.getInstance().logEvent('SEARCH');
                              e.preventDefault();
                              if (!locationSearchSting)
                                setLocationSearchError(true);
                              else {
                                setLocationSearchError(false);
                                let cleanloc = locationSearchSting;
                                cleanloc = cleanloc.replace(/\s/g, '');
                                let urlElements = cleanloc.split(',').reverse();
                                var finalurl = '';
                                for (let i = 2; i < urlElements.length; i++) {
                                  finalurl += `/${urlElements[i]}`;
                                }
                                // let finalurl = `${urlElements[2]}/${urlElements[3]}/${urlElements[4]}/${urlElements[5]}/${urlElements[6]}/${urlElements[6]}/${urlElements[7]}`
                                console.log('cleanloc:', cleanloc);
                                console.log('urlelemnonrev:', urlElements);
                                console.log('finalurl:', finalurl);
                                // console.log('afterpop:',part1)
                                if (isLoggedIn) {
                                  /* If user has click location search equal to or more than n times
                                      Block the content access
                                  */
                                  if (
                                    locationSearchClickCount >=
                                    CLICK_COUNTS.locationSearch
                                  ) {
                                    setModalType('restriction');
                                    setRestrictionType('location search');
                                    setModalStatus(true);
                                  } else {
                                    postCount('location_search');
                                    GAevent(
                                      'Search',
                                      'Location Search Clicked',
                                      'Location Search',
                                    );
                                    Router.push(
                                      `/daycare/search?search_type=${searchType}&locationString=${locationSearchSting}&latitude=${locationLat}&longitude=${locationLng}&max_age=${Math.floor(
                                        maxAge * 12,
                                      )}&transportation=0&food=0&type=radius&sort_by=distance`,
                                    ).then(() => window.scrollTo(0, 0));
                                  }
                                } else {
                                  GAevent(
                                    'Search',
                                    'Location Search Clicked',
                                    'Location Search',
                                  );
                                  Router.push(
                                    `/daycare/search?search_type=${searchType}&locationString=${locationSearchSting}&latitude=${locationLat}&longitude=${locationLng}&type=radius&sort_by=distance`,
                                  ).then(() => window.scrollTo(0, 0));
                                }
                              }
                            }}
                            disabled={
                              searchType !== 'location' ||
                              locationGeoCodeInProgress
                            }
                          />
                        </div>

                        {locationSearchDropdownState &&
                          suggestions.length > 0 && (
                            <SearchOption
                              column
                              ref={locationSearchDropdown}
                              className="single"
                            >
                              {suggestions.map(suggestion => (
                                <li
                                  {...getSuggestionItemProps(suggestion, {
                                    // onClick: () => {
                                    //   setLocationSearchSting(
                                    //     suggestion.formattedSuggestion.mainText,
                                    //   );
                                    // },
                                  })}
                                >
                                  {suggestion.description}
                                </li>
                              ))}
                              <li className="google">
                                <img src={poweredByGoogle} alt="googleImage" />
                              </li>
                            </SearchOption>
                          )}
                      </>
                    );
                  }}
                </PlacesAutocomplete>
              </Form>
              {/* <img src={registernow} onClick={getLocation} /> */}
            </Layout>

            {/* Trip Search */}
            <Layout
              onClick={() => {
                setSearchType('trip');
                setLocationSearchSting('');
                setLocationSearchError(false);
              }}
            >
              <RadioButton
                id="test2"
                text="Trip Route"
                checkedColor="#fff"
                checked={searchType === 'trip'}
                onClick={() => {}}
              />
              <Msg>
                Click here to search the ideal care on the way to your
                destination, office or work
              </Msg>
              <Form className="toFromSearch">
                <PlacesAutocomplete
                  value={
                    activeTripLocationInput === 'from'
                      ? fromLocationSearchSting
                      : toLocationSearchSting
                  }
                  onChange={address => {
                    setLocationItemClicked(false);
                    if (activeTripLocationInput === 'from') {
                      setFromLocationSearchSting(address);
                    } else {
                      setToLocationSearchSting(address);
                    }
                  }}
                  onSelect={address =>
                    handleSelect(address, false, activeTripLocationInput)
                  }
                  onError={() => {}}
                  clearItemsOnError
                  shouldFetchSuggestions
                  searchOptions={{
                    componentRestrictions: { country: ['in'] },
                  }}
                >
                  {({ getInputProps, suggestions, getSuggestionItemProps }) => {
                    if (suggestions.length > 0)
                      setTripLocationSearchDropdownState(true);
                    else setTripLocationSearchDropdownState(false);
                    return (
                      <>
                        <Location
                          flexMargin="27px 0px 0px 0px"
                          style={{ position: 'relative' }}
                        >
                          <LocationFromTo>
                            <LocationFrom>
                              <div>&nbsp;</div>
                            </LocationFrom>
                            <LocationTo>
                              <img src={route} alt="" height={14} width={12} />
                            </LocationTo>
                          </LocationFromTo>
                          <Flex
                            flexWidth="95%"
                            flexMargin="0px 10px 0px 0px"
                            column
                            className="findLocation"
                          >
                            <Flex
                              style={{
                                borderBottom: '1px solid #E5E5E5',
                              }}
                              className={`findLocation-from ${
                                fromLocationSearchError ? 'error' : ''
                              }`}
                            >
                              <Destination
                                {...getInputProps({
                                  onBlur: () => {
                                    // if (fromLocationSearchSting)
                                    //   setFromLocationSearchError(false);
                                    // else setFromLocationSearchError(true);
                                    // if (locationItemClicked)
                                    //   setFromLocationSearchError(false);
                                    // else setFromLocationSearchError(true);
                                  },
                                })}
                                type="text"
                                placeholder="From"
                                // disabled={searchType !== 'trip'}
                                onClick={() => {
                                  setActiveTripLocationInput('from');
                                }}
                                value={fromLocationSearchSting}
                              />
                            </Flex>
                            <Flex
                              style={{ position: 'relative' }}
                              className={`findLocation-to ${
                                toLocationSearchError ? 'error' : ''
                              }`}
                            >
                              <Destination
                                {...getInputProps({
                                  onBlur: () => {
                                    // if (fromLocationSearchSting)
                                    //   setToLocationSearchError(false);
                                    // else setToLocationSearchError(true);
                                    // if (locationItemClicked)
                                    //   setToLocationSearchError(false);
                                    // else setToLocationSearchError(true);
                                  },
                                })}
                                type="text"
                                placeholder="To"
                                style={{ marginTop: '15px' }}
                                // disabled={searchType !== 'trip'}
                                onClick={() => {
                                  setActiveTripLocationInput('to');
                                }}
                                value={toLocationSearchSting}
                              />
                            </Flex>
                          </Flex>

                          <TimeInfo>
                            <Flex row flexPadding="10px 0px 10px 10px">
                              <Flex
                                // flexPadding="10px 27px 10px 10px"
                                alignCenter
                                className="timeInfo"
                              >
                                <span
                                  className="iconify"
                                  data-icon="simple-line-icons:clock"
                                  data-inline="false"
                                />
                              </Flex>
                              <TimePicker
                                defaultValue={moment()
                                  .hour(8)
                                  .minute(0)}
                                placeholder="Ideal departure time"
                                onChange={e => {
                                  // eslint-disable-next-line no-underscore-dangle
                                  setFromTime(moment(e._d).format('HH:MM'));
                                }}
                                showSecond={false}
                                minuteStep={5}
                                disabled={searchType !== 'trip'}
                              />
                            </Flex>
                          </TimeInfo>
                          {tripLocationSearchDropdownState &&
                            suggestions.length > 0 && (
                              <SearchOption
                                column
                                ref={tripLocationSearchDropdown}
                                // top="435px"
                              >
                                {suggestions.map(suggestion => (
                                  <li
                                    {...getSuggestionItemProps(suggestion, {})}
                                  >
                                    {suggestion.description}
                                  </li>
                                ))}
                                <li className="google">
                                  <img
                                    src={poweredByGoogle}
                                    alt="googleImage"
                                  />
                                </li>
                              </SearchOption>
                            )}

                          {(fromLocationSearchError ||
                            toLocationSearchError) && (
                            <DestinationError>
                              {(() => {
                                if (
                                  fromLocationSearchError &&
                                  toLocationSearchError
                                ) {
                                  return 'From and To locations are required';
                                }
                                if (fromLocationSearchError) {
                                  return 'From location is required';
                                }
                                if (toLocationSearchError) {
                                  return 'To location is required';
                                }
                                return '';
                              })()}
                            </DestinationError>
                          )}
                        </Location>

                        <div
                          style={{ width: '166px' }}
                          className="buttonWrapper"
                        >
                          <Button
                            // href="/"
                            text="Search"
                            headerButton
                            marginTop="10px"
                            disabled={searchType !== 'trip'}
                            onClick={e => {
                              // amplitude.getInstance().logEvent('SEARCH');
                              e.preventDefault();

                              if (
                                !fromLocationSearchSting ||
                                !toLocationSearchSting
                              ) {
                                if (
                                  !fromLocationSearchSting &&
                                  !toLocationSearchSting
                                ) {
                                  setFromLocationSearchError(true);
                                  setToLocationSearchError(true);
                                }

                                if (
                                  fromLocationSearchSting &&
                                  !toLocationSearchSting
                                ) {
                                  setFromLocationSearchError(false);
                                  setToLocationSearchError(true);
                                }

                                if (
                                  toLocationSearchSting &&
                                  !fromLocationSearchSting
                                ) {
                                  setFromLocationSearchError(true);
                                  setToLocationSearchError(false);
                                }
                                // setFromTimeError(true);
                              } else {
                                setFromLocationSearchError(false);
                                setToLocationSearchError(false);
                                // setFromTimeError(false);

                                if (isLoggedIn) {
                                  /* If user has click trip search equal to or more than n times 
                                      Block the content access
                                  */
                                  if (
                                    tripSearchClickCount >=
                                    CLICK_COUNTS.tripSearch
                                  ) {
                                    setModalType('restriction');
                                    setRestrictionType('trip search');
                                    setModalStatus(true);
                                  } else {
                                    postCount('trip_search');
                                    GAevent(
                                      'Search',
                                      'Trip Search Clicked',
                                      'Trip Search',
                                    );
                                    Router.push(
                                      `/daycare/search?search_type=${searchType}&toLocationString=${toLocationSearchSting}&fromLocationString=${fromLocationSearchSting}&fromLat=${fromLocationLat}&toLat=${toLocationLat}&fromLng=${fromLocationLng}&toLng=${toLocationLng}&timehrs=${
                                        fromTime.split(':')[0]
                                      }&timemins=${
                                        fromTime.split(':')[1]
                                      }&max_age=${Math.floor(
                                        maxAge * 12,
                                      )}&transportation=0&food=0&type=radius&sort_by=distance`,
                                    ).then(() => window.scrollTo(0, 0));
                                  }
                                } else {
                                  GAevent(
                                    'Search',
                                    'Trip Search Clicked',
                                    'Trip Search',
                                  );
                                  let newtoLocationSearchSting = toLocationSearchSting;
                                  let newfromLocationSearchSting = fromLocationSearchSting;

                                  newtoLocationSearchSting = newtoLocationSearchSting.replace(
                                    /\s/g,
                                    '',
                                  );
                                  newfromLocationSearchSting = newfromLocationSearchSting.replace(
                                    /\s/g,
                                    '',
                                  );

                                  let newurl1 = newtoLocationSearchSting.split(
                                    ',',
                                  );
                                  let newurl2 = newfromLocationSearchSting.split(
                                    ',',
                                  );

                                  let finalurl1 = newurl1[0].toLowerCase();
                                  let finalurl2 = newurl2[0].toLowerCase();

                                  console.log(finalurl1, finalurl2);
                                  Geocode.fromLatLng(
                                    fromLocationLat,
                                    toLocationLng,
                                  ).then(
                                    response => {
                                      const address =
                                        response.results[0].formatted_address;
                                      let city, state, country, cityurl;
                                      for (
                                        let i = 0;
                                        i <
                                        response.results[0].address_components
                                          .length;
                                        i++
                                      ) {
                                        for (
                                          let j = 0;
                                          j <
                                          response.results[0]
                                            .address_components[i].types.length;
                                          j++
                                        ) {
                                          switch (
                                            response.results[0]
                                              .address_components[i].types[j]
                                          ) {
                                            case 'locality':
                                              city =
                                                response.results[0]
                                                  .address_components[i]
                                                  .long_name;
                                              cityurl = city
                                                .replace(/\s/g, '')
                                                .toLowerCase();

                                              break;
                                            case 'administrative_area_level_1':
                                              state =
                                                response.results[0]
                                                  .address_components[i]
                                                  .long_name;
                                              break;
                                            case 'country':
                                              country =
                                                response.results[0]
                                                  .address_components[i]
                                                  .long_name;
                                              break;
                                          }
                                        }
                                      }
                                      Router.push(
                                        `/daycare/search?search_type=${searchType}&toLocationString=${toLocationSearchSting}&fromLocationString=${fromLocationSearchSting}&fromLat=${fromLocationLat}&toLat=${toLocationLat}&fromLng=${fromLocationLng}&toLng=${toLocationLng}&timehrs=${
                                          fromTime.split(':')[0]
                                        }&timemins=${
                                          fromTime.split(':')[1]
                                        }&type=radius&sort_by=distance`,
                                        {
                                          pathname: `/${cityurl}/${finalurl2}-${finalurl1}`,
                                        },
                                        console.log(
                                          'Lat-Long',
                                          locationLat,
                                          locationLng,
                                        ),
                                      ).then(() => window.scrollTo(0, 0));
                                      console.log(
                                        'City,State,Country',
                                        city,
                                        state,
                                        country,
                                      );
                                      console.log('Address', address);
                                    },
                                    error => {
                                      console.error(error);
                                    },
                                  );
                                }
                              }
                            }}
                          />
                        </div>
                      </>
                    );
                  }}
                </PlacesAutocomplete>
              </Form>
            </Layout>
          </SearchWrapper>
        </MainSection>
        <Content>
          <CardWrapper>
            <H2 text="Top preschools and daycares" textAlign="left" />
            <Flex wrap justifyBetween>
              {TEMP_POPULAR_DATA.map(d => (
                <SchoolCard
                  category={
                    d.child_care_providers === '3'
                      ? 'pre-school/daycare'
                      : d.child_care_providers === '2'
                        ? 'daycare'
                        : 'preschool'
                  }
                  schoolImage={d.url}
                  schoolName={d.name}
                  schoolBranch={d.area}
                  schoolRating="4.3"
                  schoolDesc=""
                  photocardType="featured"
                  cardtype="popularSchool"
                  redirectionLink={`/${d.city}/${d.area}/${
                    d.slug
                  }?wishlist=${d.wishlist_id}&name=${d.name}&type=${
                    d.child_care_providers === '3'
                      ? 'pre-school/daycare'
                      : d.child_care_providers === '2'
                        ? 'daycare'
                        : 'preschool'
                  }`}
                />
              ))}
            </Flex>
            <Flex
              justifyCenter
              className="loadMore"
              flexMargin="50px 0px 0px 0px"
            >
              {/* <Button href="/" text="Load More" type="subscribe" /> */}
            </Flex>
          </CardWrapper>
          <SideSection>
            {!isLoggedIn ? (
              <RegisterNow>
                <ImgBlock>
                  <img src={registernow} alt="" />
                </ImgBlock>
                <RegisterTitle>
                  Register with us to get exciting discount.
                </RegisterTitle>
                <RegisterSubTitle>
                  We will help you find the best place for your children.
                </RegisterSubTitle>
                <Button
                  href="/"
                  text="Register Now"
                  type="mobile"
                  fullwidth
                  onClick={() => {
                    setModalType('signup');
                    setModalStatus(true);
                  }}
                />
              </RegisterNow>
            ) : null}
            <LayoutWrapper>
              <SchoolsTitle>Kiddenz Schools in Bangalore</SchoolsTitle>
              <SchoolList>
                <SchoolListItem
                  onClick={() =>
                    locationRedirection({
                      businessType: 1,
                      locationString: 'Indiranagar',
                      lat: '12.9783692',
                      lng: '77.6408356',
                    })
                  }
                >
                  Preschool in Indiranagar
                </SchoolListItem>
                <SchoolListItem
                  onClick={() =>
                    locationRedirection({
                      businessType: 1,
                      locationString: 'Kalyan Nagar',
                      lat: '13.0239923',
                      lng: '77.643294',
                    })
                  }
                >
                  Preschool in Kalyan Nagar
                </SchoolListItem>
                <SchoolListItem
                  onClick={() =>
                    locationRedirection({
                      businessType: 1,
                      locationString: 'Banashankari',
                      lat: '12.9254533',
                      lng: '77.546757',
                    })
                  }
                >
                  Preschool in Banashankari
                </SchoolListItem>
                <SchoolListItem
                  onClick={() =>
                    locationRedirection({
                      businessType: 2,
                      locationString: 'Indiranagar',
                      lat: '12.9783692',
                      lng: '77.6408356',
                    })
                  }
                >
                  Daycare in Indiranagar
                </SchoolListItem>
                <SchoolListItem
                  onClick={() =>
                    locationRedirection({
                      businessType: 2,
                      locationString: 'Kalyan Nagar',
                      lat: '13.0239923',
                      lng: '77.643294',
                    })
                  }
                >
                  Daycare in Kalyan Nagar
                </SchoolListItem>{' '}
                <SchoolListItem
                  onClick={() =>
                    locationRedirection({
                      businessType: 2,
                      locationString: 'Banashankari',
                      lat: '12.9254533',
                      lng: '77.546757',
                    })
                  }
                >
                  Daycare in Banashankari
                </SchoolListItem>{' '}
              </SchoolList>
            </LayoutWrapper>
          </SideSection>
        </Content>
        <InterestingRead flexDirection="row">
          <Flex flexWidth="73%" column>
            <MainHeading
              text="Parenting Articles"
              fontSize="32px"
              margin="0px 0px 8px 0px"
            />
            <SubInfo>
              Follow our articles to get interesting and informative insights
              into parenting
            </SubInfo>
            <Flex className="interestContainer">
              {articles.map(article => (
                <Link href={`/blog/${article.post_name}-${article.post_id}`}>
                  <InterestCard
                    // onClick={() => onCardClickHandler('id')}
                    margin="0px 4% 0px 0px"
                    categoryList={article.category}
                    isLoggedIn={isLoggedIn}
                    categoriesConfig={categoriesConfig}
                    categoryColors={categoryColors}
                    interestImage={article.post_image}
                    heading={article.post_title}
                    list={article.post_tag}
                    dateNum={moment(article.post_modified).format(
                      'MMMM DD,YYYY',
                    )}
                    viewNum={article.viewCount}
                    likesNum={article.likeCount}
                    isBookmarked={bookmarkedItems.includes(article.post_id)}
                    onBookmarkClick={() => {
                      if (bookmarkedItems.includes(article.post_id))
                        removeBookmark(article.post_id);
                      else addBookmark(article.post_id);
                    }}
                  />
                </Link>
              ))}
            </Flex>
            <Flex
              justifyCenter
              className="loadMore"
              flexMargin="100px 0px 0px 0px"
            >
              <Button
                href="/"
                text="View all articles"
                type="subscribe"
                onClick={() =>
                  Router.push({
                    pathname: '/blog',
                  }).then(() => window.scrollTo(0, 0))
                }
              />
            </Flex>
          </Flex>

          <Flex flexWidth="23%" column>
            <KiddenText>Categories</KiddenText>
            <Flex
              wrap
              flexWidth="100%"
              flexHeight="fit-content"
              className="categorySection"
            >
              {(categories.length > 0 ? categories : [])
                .slice(0, 4)
                .filter(d => d.name.toLowerCase() !== 'uncategorized')
                .map(({ name, slug, term_id: categoryId }) => (
                  <Smileycard
                    onClick={() => handelCategoryClick(slug, categoryId, name)}
                    text={name}
                    bgcolor="#fff"
                    type="landing"
                    icon={CATEGORY_LOOK_CONFIG[slug].icon}
                  />
                ))}
              <Smileycard
                type="category"
                text="more"
                margin="0px 10px 20px 0px"
                onClick={() =>
                  Router.push({
                    pathname: '/blog',
                  }).then(() => window.scrollTo(0, 0))
                }
              />
            </Flex>
          </Flex>
        </InterestingRead>
      </MainWrapper>
      <Footer {...props} />
    </>
  );
}
HomePage.propTypes = {
  top: PropTypes.string,
};
export default HomePage;

const CATEGORY_LOOK_CONFIG = {
  'behaviour-and-discipline': {
    name: 'Behaviour and Discipline',
    icon: behaviour,
    color: '',
    redirection: `/blog/category/behaviour-and-discipline?id=26&name=Behaviour and Discipline`,
  },
  development: {
    name: 'Development',
    icon: development,
    color: '',
    redirection: `/blog/category/development?id=31&name=Development`,
  },
  activities: {
    name: 'Activities',
    icon: childActivities,
    color: '',
    redirection: `/blog/category/development/activities?main=Development&id=31&name=Activities&back=false`,
  },
  awareness: {
    name: 'Awareness',
    icon: abuse,
    color: '',
    redirection: `/blog/category/development/awareness?main=Development&id=31&name=Awareness&back=false`,
  },
  'potty-training': {
    name: 'Potty Training',
    icon: pottyTraining,
    color: '',
    redirection: `/blog/category/development/potty-training?main=Development&id=31&name=Potty Training&back=false`,
  },
  technology: {
    name: 'Technology',
    icon: technology,
    color: '',
    redirection: `/blog/category/development/technology?main=Development&id=31&name=Technology&back=false`,
  },
  'education-and-curriculum': {
    name: 'Education and Curriculum',
    icon: education,
    color: '',
    redirection: `/blog/category/education-and-curriculum?id=35&name=Education and Curriculum`,
  },
  health: {
    name: 'Health',
    icon: overallHealth,
    color: '',
    redirection: `/blog/category/health?id=20&name=Health`,
  },
  dental: {
    name: 'Dental',
    icon: dental,
    color: '',
    redirection: `/blog/category/health/dental?main=Health&id=20&name=Dental&back=false`,
  },
  'feeding-your-toddler': {
    name: 'Feeding',
    icon: feeding,
    color: '',
    redirection: `/blog/category/health/feeding-your-toddler?main=Health&id=20&name=Feeding&back=false`,
  },
  nutrition: {
    name: 'Nutrition',
    icon: nutrition,
    color: '',
    redirection: `/blog/category/health/nutrition?main=Health&id=20&name=Nutrition&back=false`,
  },
  'overall-health': {
    name: 'Overall Health',
    icon: overallHealth,
    color: '',
    redirection: `/blog/category/health/overall-health?main=Health&id=20&name=Overall Health&back=false`,
  },
  'sleeping-habits': {
    name: 'Sleeping Habits',
    icon: sleep,
    color: '',
    redirection: `/blog/category/health/sleeping-habits?main=Health&id=20&name=Sleeping Habits&back=false`,
  },
  'parenting-and-childcare': {
    name: 'Parenting and Childcare',
    icon: parenting,
    color: '',
    redirection: `/blog/category/parenting-and-childcare?id=17&name=Parenting and Childcare`,
  },
  'special-child': {
    name: 'Special Child',
    icon: specialChildren,
    color: '',
    redirection: `/blog/category/parenting-and-childcare/special-child?main=Parenting and Childcare&id=17&name=Special Child&back=false`,
  },
  travel: {
    name: 'Travel',
    icon: travel,
    color: '',
    redirection: `/blog/category/parenting-and-childcare/travel?main=Parenting and Childcare&id=17&name=Travel&back=false`,
  },
  upbringing: {
    name: 'Upbringing',
    icon: parenting,
    color: '',
    redirection: `/blog/category/parenting-and-childcare/upbringing?main=Parenting and Childcare&id=17&name=Upbringing&back=false`,
  },
  fun: {
    name: 'Fun',
    icon: funImage,
    color: '',
    redirection: `/blog/category/development/fun?main=Development&id=31&name=Fun&back=false`,
  },
};

const MainSection = styled.section`
position: relative;
  height: 610px;
  padding-top: 86px;
  // background: ${colors.secondary};
  background-size: 100%;
  z-index: 10;

  &>img{
    position :absolute;
    left:0;
    top:0px;
    z-index: -1;
    @media screen and (min-width:1441px) and (max-width:2560px)
  {
    width:100%;
  }

    @media (max-width: 1200px){
      height:103%;
    }
   
    @media (max-width: 500px){
      height:107%;
      left:-50%;
    }
  }
  @media screen and (min-width:1441px) and (max-width:2560px)
  {
    height: 850px;
  }
  @media (max-width: 900px){
    padding-top: 20px;
    height:730px;
   }
   @media (max-width: 500px){
    height:690px;
  }


`;
const Msg = styled.div`
  margin-top: 10px;
  margin-left: 35px;
  width: 70%;
  color: ${colors.white};
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 19px;
  min-height: 38px;
  @media (max-width: 768px) {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    margin-top: 50px;
    margin-left: 50%;
  }
  &.styledMsg {
    @media (max-width: 768px) {
      display: none;
      margin-left: -50%;
    }
    @media (max-width: 414px) {
      margin-left: -100%;
    }
  }
  @media (max-width: 900px) {
    width: 90%;
    margin-left: 33px;
    margin-top: 30px;
  }
`;
const SideSection = styled.aside`
  width: 22.5%;
  @media (max-width: 1366px) {
    width: 36%;
  }
  @media screen and (max-width: 1365px) {
    width: 25%;
  }
  @media screen and (max-width: 1280px) {
    width: 21%;
  }
  @media (max-width: 768px) {
    width: 100%;
    display: flex;
    justify-content: space-between;
    margin-top: 50px;
  }
  @media (max-width: 500px) {
    flex-direction: column;
    margin-top: 0px;
  }
`;
const CardWrapper = styled.section`
  width: 76.5%;
  @media screen and (max-width: 1365px) {
    width: 75%;
  }
  @media screen and (max-width: 1280px) {
    width: 77%;
  }
  @media screen and (max-width: 1090px) {
    width: 100%;
  }
  .loadMore {
    @media (max-width: 500px) {
      margin: 30px 0px;
    }
  }
  .subscribe {
    @media (max-width: 500px) {
      height: 40px;
      padding: 0px 24px;
      font-size: 14px;
    }
  }
  @media (max-width: 768px) {
    width: 100%;
  }
`;
const SearchWrapper = styled(FlexSection)`
  @media (max-width: 900px) {
    flex-direction: column;
    padding: 40px 30px;
  }
  @media (max-width: 500px) {
    flex-direction: column;
    padding: 40px 15px;
  }
`;
const Layout = styled.section`
  height: 280px;
  width: 50%;
  padding-left: 116px;

  .buttonWrapper {
    @media (max-width: 900px) {
      justify-content: flex-start !important;
    }
    & > button {
      padding: 0px 56px;
      @media (max-width: 500px) {
        height: 40px;
        font-size: 14px;
      }
    }
  }
  .locationInput {
    width: 95%;
    @media (max-width: 500px) {
      height: 40px;
      margin-right: 0px;
    }
    &::-webkit-input-placeholder {
      @media (max-width: 500px) {
        font-size: 14px;
      }
    }
  }
  @media (max-width: 900px) {
    padding-left: 0px;
    height: 200px;
    width: 100%;
  }
  &:last-child {
    padding-left: 103px;
    @media (max-width: 1152px) {
      padding-left: 30px;
    }
    @media (max-width: 900px) {
      padding-left: 0px;
    }
  }
  &:first-child {
    @media (max-width: 768px) {
      // padding-left: 25%;
    }
    @media (max-width: 900px) {
      padding-left: 0px;
      margin-bottom: 66px;
    }
    @media (max-width: 500px) {
      margin-bottom: 36px;
    }
  }
  .locationForm {
    padding-top: 30px;
    border-right: 1px solid #3a205d;
    position: relative;
  }
  @media (max-width: 1152px) {
    padding-left: 30px;
  }
  @media (max-width: 500px) {
    padding-left: 0px;
  }
  @media (max-width: 768px) {
    position: relative;
    width: 100%;

    .locationForm {
      position: absolute;
      left: 0;
      margin-left: 50%;
      padding-top: 60px;
      width: 100%;
      border-right: 0px solid #000;
      @media (max-width: 500px) {
        margin-left: 0%;
        padding-top: 50px;
      }
    }

    .toFromSearch {
      // display: none;
      position: absolute;
      right: 0;
      top: 0;
      margin-left: 0px;
      margin-right: 50%;
      margin-top: 50px;
      width: 100%;
    }
  }
  @media (max-width: 900px) {
    .locationForm {
      min-width: 345px;
      margin-left: 0%;
    }
    .toFromSearch {
      width: 99%;
      margin: 50px 0px 0px;
    }
    .buttonWrapper {
      width: 100% !important;
      display: flex;
      justify-content: center;
    }
  }
  @media (max-width: 375px) {
    .locationForm {
      min-width: 315px;
    }
  }
  @media (max-width: 320px) {
    .locationForm {
      min-width: 290px;
    }
    .toFromSearch {
      margin-top: 95px;
    }
  }
`;
const RegisterNow = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 20px;
  border-radius: 10px;
  background-color: ${colors.white};
  box-shadow: 2px 2px 20px 0 rgba(0, 0, 0, 0.05);
  margin-bottom: 50px;

  @media (max-width: 768px) {
    width: 50%;
    order: 2;
    margin-left: 20px;
  }
  @media (max-width: 500px) {
    width: 100%;
    margin-left: 0px;
    order: initial;
  }
`;
const RegisterTitle = styled.h3`
  margin: 0px 0px 10px;
  color: ${colors.secondary};
  font-size: 20px;
  font-family: 'Quicksand', sans-serif;
  font-weight: 700;
  line-height: 25px;
  text-align: center;
`;
const RegisterSubTitle = styled.section`
  margin-bottom: 20px;
  color: ${colors.lightGrey};
  font-size: 15px;
  font-family: 'Quicksand', sans-serif;
  font-weight: 500;
  line-height: 19px;
  text-align: center;
`;
const ImgBlock = styled.div`
  height: 174px;
  width: 159px;
  margin-bottom: 22px;

  & img {
    height: 100%;
    width: 100%;
  }
`;
const LayoutWrapper = styled.div`
  padding-top: 0px;
`;
const SchoolsTitle = styled(RegisterTitle)`
  color: ${colors.darkGrey};
  text-align: left;
`;
const SchoolList = styled.ul`
  margin: 0px;
  padding: 0px;
  padding-left: 15px;
  color: ${colors.lightGrey};
  font-size: 14px;
  line-height: 28px;
  list-style-type: none;
`;
const SchoolListItem = styled.h4`
  cursor: pointer;
  position: relative;
  font-weight: 400;
  margin: 0px 0px 5px;
  &::before {
    content: '';
    position: absolute;
    top: 12px;
    left: 0;
    width: 6px;
    height: 6px;
    margin-left: -15px;
    border-radius: 50px;
    background-color: #60b947;
    display: inline-block;
  }
`;

const SearchOption = styled(Flex)`
  position: absolute;
  top: ${props => props.top || '115px'};
  left: 0px;
  width: 100%;
  max-width: 442px;
  padding: 20px 20px 40px;
  border-radius: 5px;
  background: ${colors.white};
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.3);
  z-index: 1000000;
  .google {
    position: absolute;
    right: 20px;
    bottom: 0px;
    // padding:20px 0px;
    @media (max-width: 500px) {
      right: 20px;
    }
    @media (min-width: 1400px) {
      display: block !important;
    }
  }
  @media (max-width: 500px) {
    left: 0px;
    top: 90px;
  }
  .google {
    position: absolute;
    right: 20px;
    bottom: 0px;
    // padding:20px 0px;
    @media (max-width: 500px) {
      right: 20px;
    }
    @media (min-width: 1400px) {
      display: block !important;
    }
  }
  &.single {
    top: 90px;
    @media (max-width: 500px) {
      top: 100px;
    }
  }

  li {
    font-size: 14px;
    line-height: 30px;
    border-bottom: 1px solid rgba(0, 0, 0, 0.1);
    list-style-type: none;
    padding: 5px 0px;
    color: #666c78;
    cursor: pointer;
    &:last-child {
      border-bottom: 0px solid rgba(0, 0, 0, 0.1);
    }
    &:hover {
      color: #000;
    }
  }
`;
const Location = styled(Flex)`
  height: 113px;
  width: 442px;
  padding: 12px 14px;
  // margin: 27px 0px 0px 0px;
  border-radius: 5px;
  background: ${colors.white};

  .findLocation {
    & > div:first-child {
      input {
        max-width: 210px;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        text-align: left;
        text-decoration: none;
        display: inline-block;
        @media (max-width: 500px) {
          font-size: 14px;
          margin-top: 3px;
          background-color: transparent !important;
        }
      }
    }
    &-from {
      position: relative;
      @media (max-width: 500px) {
        padding-right: 122px;
      }
      &.error::after {
        position: absolute;
        content: '';
        bottom: 0px;
        left: 20px;
        height: 4px;
        background-color: #ff6767;
        width: 94%;
        border-radius: 14px;
        @media (max-width: 500px) {
          left: 12px;
        }
      }
      &.error input::-webkit-input-placeholder {
        color: #ff6767;
      }
    }
    &-to {
      position: relative;

      &.error::after {
        position: absolute;
        content: '';
        bottom: -5px;
        left: 20px;
        height: 4px;
        background-color: #ff6767;
        width: 94%;
        border-radius: 14px;
        @media (max-width: 500px) {
          bottom: 2px;
          left: 12px;
          width: 160%;
        }
      }
      &.error input::-webkit-input-placeholder {
        color: #ff6767;
      }
    }
  }
  @media (max-width: 500px) {
    width: 100%;
    height: 86px;
    padding: 5px 14px;
    .findLocation {
      width: 93% !important;
      & + div {
        width: 34% !important;
        @media (max-width: 500px) {
          width: 36% !important;
        }
      }
      input:disabled {
        background-color: transparent !important;
        margin: 5px 0px;
      }
    }
  }
  @media (max-width: 320px) {
    width: 290px;
    margin: 10px 0px 0px 0px;
  }
`;
const LocationFromTo = styled.ul`
  list-style-type: none;
  padding: 0px;
  margin: 0px;
  margin-right: 12px;
  margin-top: -7px;
  @media (max-width: 500px) {
    margin-top: -4px;
  }
`;
const LocationFrom = styled.li`
  position: relative;
  height: 14px;
  width: 14px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 10px;
  border: 1px solid #63cab9;
  // box-shadow: 0px 0px 5px #63cab9;
  background: ${colors.white};
  border-radius: 50px;

  div {
    height: 8px;
    width: 8px;
    border-radius: 50px;
    background: #63cab9;
    color: #63cab9;
  }
  &:after {
    content: '';
    position: absolute;
    top: 15px;
    left: 5px;
    height: 40px;
    border-left: 1px dashed #bfc4ce;
    @media (max-width: 500px) {
      height: 31px !important;
    }
  }
`;
const Destination = styled.input`
  width: 100%;
  margin-bottom: 20px;
  font-size: 16px;
  border: 0px;
  &:disabled {
    background-color: transparent !important;
  }
  &:focus {
    outline: 0;
    border: 0px;
  }

  @media (max-width: 500px) {
    margin-bottom: 14px;
    font-size: 14px;
    margin-top: 11px;
    background-color: transparent !important;
  }
`;
const LocationTo = styled.li`
  margin-top: 35px;
  list-style-type: none;
  @media (max-width: 500px) {
    margin-top: 26px;
  }

  img {
    position: static;
    height: 14;
    width: 12;
  }
`;

const TimeInfo = styled.div`
  border-radius: 5px;
  background-color: #eff2f4;
  // padding: 5px 10px;
  color: ${colors.inputPlaceholder};
  font-family: 'Quicksand', sans-serif;
  font-size: 14px;
  font-weight: 500;
  line-height: 18px;
  position: absolute;
  right: 9px;
  top: 6px;
  height: fit-content;
  & > div {
    @media (max-width: 500px) {
      padding: 2px 0px 2px 4px;
    }
  }
  .rc-time-picker {
    width: 100%;
    padding-left: 0px;
  }
  .rc-time-picker-input {
    @media (max-width: 500px) {
      padding: 7px 0px 4px;
      font-size: 11px;
    }
  }
  @media (max-width: 375px) {
    font-size: 12px;
  }
  .timeInfo {
    @media (max-width: 500px) {
      padding: 6px 8px 6px 5px;
    }
    @media (max-width: 320px) {
      margin-bottom: 0px;
    }
  }
`;
const Time = styled.span`
  margin-left: 10px;
`;
const DestinationError = styled.div`
  margin: 10px 0px 0px 0px;
  font-family: Roboto;
  font-size: 14px;
  line-height: 19px;
  font-weight: 500;
  border-radius: 3px;
  position: absolute;
  left: 200px;
  width: 55%;
  bottom: -55px;
  background-color: #fff;
  color: #ff6767;
  padding: 10px 12px;
  max-width: 442px;
  &::after {
    content: '';
    position: absolute;
    left: 18px;
    top: -8px;
    width: 0;
    height: 0;
    border-radius: 4px;
    border-left: 10px solid transparent;
    border-right: 10px solid transparent;
    border-top: 10px solid #fff;
    clear: both;
    transform: rotate(180deg);
  }
  @media (max-width: 500px) {
    font-size: 10px;
    left: 165px;
    width: 54%;
    bottom: -50px;
    padding: 8px 8px;
  }
`;
const Content = styled(FlexSection)`
  background: ${colors.white};
  align-items: flex-start;
  justify-content: space-between;
  @media screen and (max-width: 1090px) {
    flex-direction: column;
  }
  @media (max-width: 800px) {
    padding: 40px 20px;
  }
  @media (max-width: 768px) {
    flex-direction: column;
    padding-top: 0px;
  }
  @media (max-width: 500px) {
    flex-direction: column;
    padding: 100px 20px 0px;
  }
  // @media (max-width: 375px) {
  //   padding: 15px;
  // }
`;
const InterestingRead = styled.div`
  width: 100%;
  max-width: 2560px;
  display: flex;
  padding: 30px 30px 120px;
  margin: 0px auto;
  flex-direction: ${props => props.flexDirection || 'column'};
  justify-content: space-between;

  ${Mainheading} {
    @media screen and (max-width: 500px) {
      font-size: 16px;
      line-height: 20px;
    }
  }

  @media screen and (max-width: 900px) {
    flex-direction: column-reverse;
    padding: 0px 20px 120px;
    & > div {
      width: 100%;
      margin-bottom: 50px;
    }
    .loadMore {
      margin: 20px 0px 30px;
    }
  }
  .interestContainer {
    @media screen and (max-width: 765px) {
      flex-direction: column;
      align-items: center;
    }
  }
  .subscribe {
    @media screen and (max-width: 500px) {
      height: 40px;
      padding: 0px 24px;
      font-size: 14px;
    }
  }
`;
const SubInfo = styled.div`
  color: #30333b;
  font-family: 'Quicksand', sans-serif;
  font-size: 16px;
  font-weight: 500;
  line-height: 20px;
  margin-bottom: 30px;
  @media screen and (max-width: 500px) {
    font-size: 14px;
  }
`;
const LandingSubTitle = styled.h1`
  color: #fff;
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 200;
  // font-weight: 500;
  line-height: 18px;
  text-align: center;
  margin: 18px 0px 19px;
  @media screen and (max-width: 900px) {
    font-size: 14px;
    line-height: 30px;
    margin: 10px 5px;
  }
  @media screen and (max-width: 768px) {
    font-weight: 200;
    font-size: 12px;
    line-height: 23px;
    text-align: center;
  }
  @media screen and (max-width: 500px) {
    font-size: 12px;
    line-height: 22px;
    margin: 0px 0px 12px;
    padding: 0px 6px;
  }
`;
const KiddenText = styled.div`
  font-family: 'Quicksand', sans-serif;
  margin: ${props => props.textmargin || '0px 0px 22px 0px;'};
  color: #30333b;
  font-size: 16px;
  font-weight: bold;
  line-height: 30px;
  @media screen and (max-width: 900px) {
    font-size: 20px;
  }
`;

const TEMP_POPULAR_DATA = [
  {
    id: 214,
    slug: 'the-pearls-montessori-1795828',
    distance: 0.20307736495707956,
    url: 'https://s3-ap-south-1.amazonaws.com/kiddenz-media-prod/1A58FBAE86blob',
    thumb_url:
      'https://s3-ap-south-1.amazonaws.com/kiddenz-media-prod/1A58FBAE86blob',
    media_name: '325C2E2FFBblob',
    name: 'The Pearls Montessori',
    area: 'gottigere',
    city: 'bangalore',
    state: 'Karnataka',
    pincode: '560083',
    child_care_providers: '3',
    hours: '18',
  },
  {
    id: 214,
    slug: 'aakruti-montessori-house-of-children-1784415',
    distance: 0.20307736495707956,
    url: 'https://s3-ap-south-1.amazonaws.com/kiddenz-media-prod/07CE9187A4blob',
    thumb_url:
      'https://s3-ap-south-1.amazonaws.com/kiddenz-media-prod/07CE9187A4blob',
    media_name: '07CE9187A4blob',
    name: 'Aakruti Montessori House of Children',
    area: 'bannerghatta-road',
    city: 'bangalore',
    state: 'Karnataka',
    pincode: '560076',
    child_care_providers: '3',
    hours: '18',
  },
  {
    id: 214,
    slug: 'aura-montessori-house-of-children-1788833',
    distance: 0.20307736495707956,
    url: 'https://s3-ap-south-1.amazonaws.com/kiddenz-media-prod/2B39A48322blob',
    thumb_url:
      'https://s3-ap-south-1.amazonaws.com/kiddenz-media-prod/2B39A48322blob',
    media_name: '2B39A48322blob',
    name: 'Aura Montessori House of children - Gottigere',
    area: 'gottigere',
    city: 'bangalore',
    state: 'Karnataka',
    pincode: '560083',
    child_care_providers: '3',
    hours: '18',
  },
  {
    id: 214,
    slug: 'koala-preschool-214598',
    distance: 0.20307736495707956,
    url: 'https://s3-ap-southeast-1.amazonaws.com/kiddenz-media/325C2E2FFBblob',
    thumb_url:
      'https://s3-ap-southeast-1.amazonaws.com/kiddenz-media/thumbnail_325C2E2FFBblob',
    media_name: '325C2E2FFBblob',
    name: 'Koala Preschool',
    area: 'BTM Layout 1st Stage, Madiwala',
    city: 'Bengaluru',
    state: 'Karnataka',
    pincode: '560068',
    child_care_providers: '3',
    hours: '18',
  },
  {
    id: 287,
    slug: 'gurukul-mont-international-preschool-and-daycare-287459',
    distance: 0,
    url: 'https://s3-ap-southeast-1.amazonaws.com/kiddenz-media/CEECE2B92Eblob',
    thumb_url:
      'https://s3-ap-southeast-1.amazonaws.com/kiddenz-media/thumbnail_CEECE2B92Eblob',
    media_name: 'CEECE2B92Eblob',
    name: 'Gurukul Mont International Preschool and Daycare',
    area: 'Horamavu',
    city: 'Bangalore',
    state: 'Karnataka',
    pincode: '560043',
    child_care_providers: '3',
    hours: '',
  },
  {
    id: 278,
    slug: 'junior-dps-preschool-278980',
    distance: 0.6879037766396112,
    url: 'https://s3-ap-southeast-1.amazonaws.com/kiddenz-media/DCE633A175blob',
    thumb_url:
      'https://s3-ap-southeast-1.amazonaws.com/kiddenz-media/thumbnail_DCE633A175blob',
    media_name: 'DCE633A175blob',
    name: 'Junior DPS Preschool',
    area: 'Malleshpalya',
    city: 'Bengaluru',
    state: 'Karnataka',
    pincode: '560075',
    child_care_providers: '3',
    hours: '19',
    wishlist_id: null,
  },
/*   {
    id: 232,
    slug: 'republick-high-pre-school-232109',
    distance: 3.68997349335793,
    url: 'https://s3-ap-southeast-1.amazonaws.com/kiddenz-media/C78A85C6E5blob',
    thumb_url:
      'https://s3-ap-southeast-1.amazonaws.com/kiddenz-media/thumbnail_C78A85C6E5blob',
    media_name: 'C78A85C6E5blob',
    name: 'Republick High Pre School',
    area: 'Srinivasapura',
    city: 'Bengaluru',
    state: 'Karnataka',
    pincode: '562149',
    child_care_providers: '3',
    hours: '19',
    wishlist_id: null,
  }, */
/*   {
    id: 179,
    slug: 'koala-preschool-179368',
    distance: 0,
    url: 'https://s3-ap-southeast-1.amazonaws.com/kiddenz-media/0C827B4342blob',
    thumb_url:
      'https://s3-ap-southeast-1.amazonaws.com/kiddenz-media/thumbnail_0C827B4342blob',
    media_name: '0C827B4342blob',
    name: 'Koala Preschool',
    area: 'Kalyan Nagar',
    city: 'Bangalore',
    state: 'Karnataka',
    pincode: '560043',
    child_care_providers: '3',
    hours: '18',
    wishlist_id: null,
  }, */
  {
    id: 76,
    slug: 'cambridge-montessori-preschool-and-daycare-76899',
    distance: 0.5298489436266435,
    url: 'https://s3-ap-southeast-1.amazonaws.com/kiddenz-media/44D2ACEA59blob',
    thumb_url:
      'https://s3-ap-southeast-1.amazonaws.com/kiddenz-media/thumbnail_44D2ACEA59blob',
    media_name: '44D2ACEA59blob',
    name: 'Cambridge Montessori Preschool And Daycare',
    area: 'Arekere micolayout',
    city: 'Bengaluru',
    state: 'Karnataka',
    pincode: '560076',
    child_care_providers: '3',
    hours: '20',
    wishlist_id: null,
  },
/*   {
    id: 273,
    slug: 'zion-kidz-preschool-273142',
    distance: 0.00009493529796600342,
    url: 'https://s3-ap-southeast-1.amazonaws.com/kiddenz-media/1C00BDBD26blob',
    thumb_url:
      'https://s3-ap-southeast-1.amazonaws.com/kiddenz-media/thumbnail_1C00BDBD26blob',
    media_name: '1C00BDBD26blob',
    name: 'Zion Kidz Preschool',
    area: 'Near Rama Temple',
    city: 'Bangalore',
    state: 'Karnataka',
    pincode: '560015',
    child_care_providers: '3',
    hours: '17',
    wishlist_id: null,
  },
 */
  {
    id: 252,
    slug: 'little-millennium-preschool-252142',
    distance: 0.21113947491116475,
    url: 'https://s3-ap-southeast-1.amazonaws.com/kiddenz-media/3170D341F2blob',
    thumb_url:
      'https://s3-ap-southeast-1.amazonaws.com/kiddenz-media/thumbnail_3170D341F2blob',
    media_name: '3170D341F2blob',
    name: 'Little Millennium Preschool',
    area: 'RT Nagar',
    city: 'Bengaluru',
    state: 'Karnataka',
    pincode: '560032',
    child_care_providers: '3',
    hours: '20',
    daycare_full_day_fee_min: '4000',
    daycare_full_day_fee_max: '12000',
    preschool_fee_min: '48000',
    preschool_fee_max: '57000',
    wishlist_id: null,
  },
];
