/* eslint-disable */
// eslint-disable-next-line react/no-unescaped-entities
import React, { useState } from 'react';
// import Slider from 'react-slick';
// import Router from 'next/router';
import Link from 'next/link';
import moment from 'moment';
import styled from 'styled-components';
// import { useIntrestingArticlesHook, useBookmarkHook } from 'shared/hooks';
import { useIntrestingArticlesHook, useBookmarkHook } from 'shared/hooks';
// import { Mainheading } from 'components/MainHeading/styled';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import MainWrapper from '../../components/MainWrapper';
import Button from '../../components/Button';
import background from '../../images/header bg (1).svg';
import Flex from '../../components/common/flex';
import MainHeading from '../../components/MainHeading';
import { Mainheading } from '../../components/MainHeading/styled';

// import KiddenOffer from '../../components/KiddenOffer';
import InterestCard from '../../components/InterestCard';
import character from '../../images/Character1.svg';
// import mainImage from '../../images/video.svg';
import videoImage from '../../images/1.png';
// import infoImage from '../../images/Detailed-Info.png';
// import leadImage from '../../images/recieve leads.svg';
// import ourImage from '../../images/profileImage.jpg';
import ensureImage1 from '../../images/Kiddenz_illustration_quality-information.svg';
import ensureImage2 from '../../images/Kiddenz_illustration_transperancy.svg';
import ensureImage3 from '../../images/Kiddenz_illustration_trust.svg';
import ensureImage4 from '../../images/Kiddenz_illustration_convenience.svg';
import parentMessage from '../../images/Kiddenz_message for parents.png';

const CopyRight = props => {
  const { articles } = useIntrestingArticlesHook(props);

  const [modalStatus, setModalStatus] = useState(false);
  const [modalType, setModalType] = useState('email');

  const {
    errorToast,
    authentication: { categoryColors = {}, categoriesConfig = {} } = {},
  } = props;
  const onBookmarkAddError = ({ message }) =>
    errorToast(message || 'Something went wrong.');

  const { bookmarkedItems = [], addBookmark, removeBookmark } = useBookmarkHook(
    props,
    { onBookmarkAddError },
  );
  const { authentication: { isLoggedIn } = {} } = props;

  return (
    <>
      <Header
        {...props}
        type="articles"
        searchBar={false}
        isModalActive={modalStatus}
        activeModalType={modalType}
        setActiveCallback={setModalStatus}
      />
      <MainWrapper background="#fff" paddingTop="0px">
        <TopSection>
          <Image>
            <img src={background} alt="" />
          </Image>
          <Title>Copyright Dispute Policy</Title>
        </TopSection>
        <MainSection>
          <img className="character" src={character} alt="" />
          <p className="left">
            Copyright Dispute Policy Effective date:
            <br /> November 07, 2020 In accordance with the Indian Copyright Act
            , <br />
            we’ve adopted the policy below toward copyright infringement. We
            reserve the right to
            <br />
            (1) block access to or remove material that we believe in good faith
            to be copyrighted material that has been illegally copied and
            distributed by any of our advertisers, affiliates, content
            providers, members or users and
            <br /> (2) remove and discontinue service to repeat offenders.
          </p>
          <p>
            Remember that your use of Kiddenz&rsquo;s Services is at all times
            subject to the&nbsp;Terms of Use, which incorporates this Copyright
            Dispute Policy. Any terms we use in this Policy without defining
            them have the definitions given to them in the&nbsp;Terms of Use.
          </p>
          <p>
            I. Procedure for Reporting Copyright Infringements.&nbsp;If you
            believe that material or content residing on or accessible through
            the Services infringes your copyright (or the copyright of someone
            whom you are authorized to act on behalf of), please send a notice
            of copyright infringement containing the following information to
            the Kiddenz&rsquo;s Designated Agent to Receive Notification of
            Claimed Infringement (our &ldquo;Designated Agent,&rdquo; whose
            contact details are listed below):
          </p>
          <p>
            i. A physical or electronic signature of a person authorized to act
            on behalf of the owner of the copyright that has been allegedly
            infringed;
          </p>
          <p>ii. Identification of works or materials being infringed;</p>
          <p>
            iii. Identification of the material that is claimed to be infringing
            including information regarding the location of the infringing
            materials that the copyright owner seeks to have removed, with
            sufficient detail so that Kiddenz is capable of finding and
            verifying its existence;
          </p>
          <p>
            iv. Contact information about the notifier including address,
            telephone number and, if available, email address;
          </p>
          <p>
            v. A statement that the notifier has a good faith belief that the
            material identified in (1)(c) is not authorized by the copyright
            owner, its agent, or the law; and
          </p>
          <p>
            vi. A statement made under penalty of perjury that the information
            provided is accurate and the notifying party is authorized to make
            the complaint on behalf of the copyright owner.
          </p>
          <p>
            II. Once Proper Bona Fide Infringement Notification Is Received by
            the Designated Agent.&nbsp;Upon receipt of a proper notice of
            copyright infringement, we reserve the right to:
          </p>
          <ul>
            <li>remove or disable access to the infringing material;</li>
            <li>
              notify the content provider who is accused of infringement that we
              have removed or disabled access to the applicable material; and
            </li>
            <li>
              iii. terminate such content provider&rsquo;s access to the
              Services if he or she is a repeat offender.
            </li>
          </ul>
          <p>
            III. Procedure to Supply a Counter-Notice to the Designated
            Agent.&nbsp;If the content provider believes that the material that
            was removed (or to which access was disabled) is not infringing, or
            the content provider believes that it has the right to post and use
            such material from the copyright owner, the copyright owner&rsquo;s
            agent, or, pursuant to the law, the content provider may send us a
            counter-notice containing the following information to the
            Designated Agent:
          </p>
          <ul>
            <li>A physical or electronic signature of the content provider;</li>
            <li>
              Identification of the material that has been removed or to which
              access has been disabled and the location at which the material
              appeared before it was removed or disabled;
            </li>
            <li>
              A statement that the content provider has a good faith belief that
              the material was removed or disabled as a result of mistake or
              misidentification of the material; and iv. Content
              provider&rsquo;s name, address, telephone number, and, if
              available, email address, and a statement that such person or
              entity consents to the jurisdiction of the Federal Court for the
              judicial district in which the content provider&rsquo;s address is
              located, or, if the content provider&rsquo;s address is located
              outside India, for any judicial district in which Kiddenz is
              located, and that such person or entity will accept service of
              process from the person who provided notification of the alleged
              infringement. If a counter-notice is received by the Designated
              Agent, Kiddenz may, in its discretion, send a copy of the
              counter-notice to the original complaining party informing that
              person that Kiddenz may replace the removed material or cease
              disabling it in 10 business days. Unless the copyright owner files
              an action seeking a court order against the content provider
              accused of committing infringement, the removed material may be
              replaced or access to it restored in 10 to 14 business days or
              more after receipt of the counter-notice, at Kiddenz&rsquo;s
              discretion.
            </li>
          </ul>
          <p>
            Please contact Kiddenz&rsquo;s Designated Agent at the following
            address: Kiddenz,
          </p>
          <p>
            <span class="s1">
              <strong>&nbsp;</strong>
            </span>
            #677, 1st Floor, 13th Cross, 27th Main Rd, 1st Sector, HSR Layout,
            Bengaluru, Karnataka 560102
          </p>
        </MainSection>
      </MainWrapper>
      <Footer bgColor="#fff" {...props} />
    </>
  );
};

export default CopyRight;

const TopSection = styled.div`
  width: 100%;
  height: 377px;
  position: relative;
  background-color: #fff;
  @media (max-width: 900px) {
    height: 200px;
  }
  @media (max-width: 500px) {
    height: 175px;
    margin-bottom: 50px;
  }
`;
const MainSection = styled.div`
  position: relative;
  max-width: 1180px;
  padding: 0px 20px;
  margin: 0px auto;
  background-color: #fff;
  margin-bottom: 50px;
  .character {
    position: absolute;
    right: 100px;
    top: -200px;
    @media (max-width: 900px) {
      transform: scale(0.8);
      right: 44px;
      top: -219px;
    }
    @media (max-width: 500px) {
      transform: scale(0.3);
      right: -44px;
      top: -300px;
    }
    @media screen and (max-width: 345px) {
      right: -84px;
    }
  }
  p,
  ul li {
    color: #30333b;
    font-family: Quicksand;
    font-size: 22px;
    letter-spacing: 0;
    line-height: 36px;
    @media (max-width: 900px) {
      font-size: 18px;
      line-height: 28px;
    }
    @media (max-width: 500px) {
      width: 100%;

      font-size: 14px;
      line-height: 24px;
    }
  }
  p.left {
    width: 48%;
    @media (max-width: 500px) {
      width: 100%;
    }
  }
  .main-image {
    width: 100%;
    height: 529px;
    margin: 30px 0px;

    img {
      width: 100%;
      height: 100%;
      object-fit: cover;
    }
    @media screen and (max-width: 500px) {
      height: auto;
    }
  }
  ${Mainheading} {
    @media screen and (max-width: 500px) {
      width: 100%;
    }
  }
`;

const Image = styled.div`
  position: absolute;
  left: 0px;
  right: 0px;
  bottom: 0px;
  top: 0px;
  z-index: 0;
  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
`;

const Title = styled.h1`
  color: #ffffff;
  font-family: Quicksand;
  font-size: 60px;
  letter-spacing: 0;
  line-height: 75px;
  font-weight: 600;
  position: relative;
  max-width: 1180px;
  padding: 0px 20px;
  margin: 120px auto 0px;
  @media (max-width: 900px) {
    margin-top: 36px;
    font-size: 46px;
  }
  @media (max-width: 900px) {
    margin-top: 10px;
    font-size: 22px;
  }
  @media (max-width: 500px) {
    margin-top: 10px;
    font-size: 22px;
    max-width: 190px;
    line-height: 1.2;
    margin: 50px 0 0;
  }
`;

const InterestingRead = styled.div`
  width: 100%;
  max-width: 1180px;
  display: flex;
  //   border-top: 1px solid #e6e8ec;
  padding: 60px 20px 120px;
  margin: 0px auto;
  flex-direction: column;
  .interestContainer {
    @media screen and (max-width: 765px) {
      flex-direction: column;
      align-items: center;
    }
  }
  @media screen and (max-width: 765px) {
    padding: 30px 20px 30px;
  }
`;

const SubInfo = styled.div`
  color: #30333b;
  font-family: 'Quicksand', sans-serif;
  font-size: 16px;
  font-weight: 500;
  line-height: 20px;
  margin-bottom: 30px;
`;

const WeEnsure = styled.div`
  display: flex;
  align-items: center;
  flex-direction: ${props => props.direction || 'row'};
  margin: ${props => props.margin || '0px auto 60px 0px;'};
  width: 60%;
  @media screen and (max-width: 900px) {
    width: 80%;
    margin-bottom: 30px;
  }
  @media screen and (max-width: 500px) {
    flex-direction: column;
    width: 100%;
    margin: 0px 0px 60px;
    align-items: flex-start;
  }
  ${Flex} {
    @media screen and (max-width: 500px) {
      margin: 0px;
      align-items: flex-start;
    }
  }
  img {
    @media screen and (max-width: 900px) {
      transform: scale(0.9);
    }
    @media screen and (max-width: 500px) {
      transform: scale(1);
      margin-bottom: 40px;
    }
  }
  h3 {
    color: #30333b;
    font-family: Quicksand;
    font-size: 22px;
    letter-spacing: 0;
    line-height: 28px;
    margin: 0px 0px 20px;
    @media screen and (max-width: 900px) {
      font-size: 20px;
    }
    @media screen and (max-width: 500px) {
      font-size: 16px;
      line-height: 24px;
      margin: 0px 0px 16px;
    }
  }
  span {
    color: #30333b;
    font-family: Quicksand;
    font-size: 18px;
    letter-spacing: 0;
    line-height: 24px;
    @media screen and (max-width: 900px) {
      font-size: 16px;
    }
    @media screen and (max-width: 500px) {
      text-align: left !important;
      font-size: 14px;
      line-height: 24px;
    }
  }
`;

// const OurTeam = styled.div`
//   display: flex;
//   margin: ${props => props.margin || '0px auto 80px 0px;'};
//   width: 100%;
//   @media screen and (max-width: 500px) {
//     background: #f2eff5;
//     border-radius: 5px;
//     margin-bottom: 24px;
//     padding: 20px;
//     flex-direction: column;
//   }
//   ${Flex} {
//     @media screen and (max-width: 500px) {
//       margin: 0px;
//     }
//   }
//   .ourImage {
//     height: 225px;
//     width: 225px;
//     border-radius: 50%;
//     overflow: hidden;
//     img {
//       width: 100%;
//       height: 100%;
//     }
//     @media screen and (max-width: 500px) {
//       height: 146px;
//       width: 146px;
//       margin-bottom: 24px;
//     }
//   }
//   h2 {
//     color: #30333b;
//     font-family: Quicksand;
//     font-size: 22px;
//     letter-spacing: 0;
//     line-height: 28px;
//     margin: 0px 0px 20px;
//     @media screen and (max-width: 500px) {
//       font-size: 14px;
//       line-height: 24px;
//       margin-bottom: 10px;
//     }
//   }
//   span {
//     color: #30333b;
//     font-family: Quicksand;
//     font-size: 18px;
//     letter-spacing: 0;
//     line-height: 24px;
//     @media screen and (max-width: 500px) {
//       font-size: 12px;
//       line-height: 24px;
//     }
//   }
// `;

const ParentsMessage = styled.div`
  position: relative;
  padding: 253px 0px;
  @media (max-width: 500px) {
    padding: 100px 0px 170px;
  }
  @media (max-width: 375px) {
    padding: 90px 0px 177px;
  }
  .parentMessage {
    position: absolute;
    top: 0px;
    right: 0px;
    left: 0px;
    height: 100%;

    img {
      width: 100%;
      height: 100%;
      object-fit: cover;
    }

    &-content {
      display: flex;
      //   justify-content: space-between;
      height: auto;
      width: 100%;
      max-width: 1180px;
      align-items: baseline;
      padding: 0px 20px 0px;
      margin: 0px auto;
      .outline {
        &:hover {
          background-color: transparent;
          border-color: #fff;
          color: #fff;
        }
      }
      @media (max-width: 500px) {
        flex-direction: column;
        width: 100%;
        align-items: center;
      }
    }
    &-right {
      @media (max-width: 500px) {
        width: 95%;
        align-items: center;
      }
    }
  }
  h2 {
    width: fit-content;
    color: #ffffff;
    font-family: Quicksand;
    font-size: 32px;
    letter-spacing: 0;
    line-height: 40px;
    z-index: 1;
    margin-right: 20%;
    @media (max-width: 500px) {
      margin-right: 0px;
      font-size: 20px;
    }
  }
  h3 {
    color: #ffffff;
    font-family: Quicksand;
    font-size: 26px;
    font-weight: 500;
    letter-spacing: 0;
    line-height: 35px;
    margin-bottom: 30px;
    margin-top: 30px;
    @media (max-width: 500px) {
      font-size: 14px;
      line-height: 26px;
      margin-top: 10px;
      text-align: center;
    }
  }
  .outline {
    padding: 0px 30px;
    @media (max-width: 500px) {
      height: 36px;
      font-size: 12px;
    }
  }
`;
