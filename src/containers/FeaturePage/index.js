/*
 * FeaturePage
 *
 * List all the features
 */
import React from 'react';
// import { Helmet } from 'react-helmet';
// import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import Header from 'components/Header';
// import Footer from 'components/Footer';
import MainWrapper from 'components/MainWrapper';
import colors from 'utils/colors';
import Flex from '../../components/common/flex';
import Button from '../../components/Button';
import CheckBox from '../../components/Checkbox';
import SchoolCard from '../../components/SchoolCard';
import Filter from '../../components/Filter';
// import RangeSlider from '../../components/RangeSlider';
// import background1 from '../../images/playhome.png';
import daycare from '../../images/Daycare.jpg';
import Form from '../HomePage/Form';
import Input from '../HomePage/Input';
import background from '../../images/queensroad.png';

const MapWrapper = styled(Flex)`
  align-self: stretch;
  @media (max-width: 1152px) {
    width: 45.5%;
  }

  @media (max-width: 768px) {
    width: 100%;
    align-self: flex-start;
  }
`;
const MapLoction = styled.div`
  width: 100%;
  height: 100vh;
  overflow: hidden;

  @media (max-width: 768px) {
    height: 550px;
    overflow: vissible;
  }
  & img {
    width: 100%;
    height: 100%;
  }
  // & img {
  //   margin-top: -78px;
  //   // margin-left: -81%;

  //   @media (max-width: 1334px) {
  //     margin-left: -89%;
  //   }
  //   @media (max-width: 1280px) {
  //     margin-left: -92%;
  //   }
  // }
`;
const NotFoundMsg = styled.div`
  color: #30333b;
  margin-bottom: 10px;
  font-family: 'Quicksand', sans-serif;
  font-size: 24px;
  font-weight: 500;
  line-height: 30px;
`;
const NotFoundInfo = styled.div`
  color: #30333b;
  max-width: 620px;
  margin-bottom: 30px;
  font-family: 'Quicksand', sans-serif;
  font-size: 20px;
  line-height: 33px;
`;
const TextMsg = styled.div`
  margin-top: 90px;
  // margin-bottom: 30px;
  color: #30333b;
  font-family: 'Quicksand', sans-serif;
  font-size: 18px;
  font-weight: 500;
  line-height: 22px;
`;
const FilterFooter = styled.div`
  display: flex;
  justify-content: center;
  padding: 20px 0px;
  border-top: 1px solid #eaedf2;
  @media (max-width: 414px) {
    flex-direction: column;
    align-items: center;
    font-size: 14px;
  }
`;
const FilterLink = styled.div`
  color: ${colors.secondary};
  margin-left: 5px;
  cursor: pointer;
`;
const SearchCheckbox = styled.div`
  position: absolute;
  top: 20px;
  right: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 5px;
  box-shadow: 2px 2px 10px 0 rgba(0, 0, 0, 0.2);
  border-radius: 5px;
  background: ${colors.white};
`;
const MainContent = styled(Flex)`
  @media (max-width: 1152px) {
    width: 55.5%;
  }

  @media (max-width: 768px) {
    width: 100%;
    padding: 30px 25px;
    order: 2;
  }
`;
const NoResultform = styled(Form)`
  padding-top: 30px;
  display: flex;
  width: 100%;
  @media (max-width: 414px) {
    flex-direction: column;
    padding-top: 10px;
  }
`;
const NoResultLabel = styled.label`
  margin-right: 10px;
  flex-grow: 1;
  font-size: 14px;
  line-height: 19px;
  color: #666c78;
  font-family: 'Roboto', sans-serif;
  @media (max-width: 414px) {
    margin-right: 0px;
    margin-bottom: 10px;
  }
  &:last-of-type {
    @media (max-width: 414px) {
      margin-bottom: 0px;
    }
  }
`;
export default function FeaturePage() {
  return (
    <>
      <Header type="secondary" />
      <Filter />
      <MainWrapper background="#fff" paddingTop="190px">
        <Flex wrap>
          <MainContent column flexWidth="65.5%" flexPadding="35px 30px">
            <NotFoundMsg>
              Unfortunately, we could not find any schools in Queens Road.{' '}
            </NotFoundMsg>
            <NotFoundInfo>
              Try searching in areas currently supported by Kiddenz or please
              provide your details and we will notify you.
            </NotFoundInfo>

            <NoResultform>
              <NoResultLabel htmlFor="username">
                Mobile Number
                <Input
                  id="username"
                  type="text"
                  placeholder=""
                  value=""
                  inputBorder="	1px solid #C0C8CD"
                />
              </NoResultLabel>
              <NoResultLabel htmlFor="username">
                E-mail ID
                <Input
                  id="username"
                  type="text"
                  placeholder=""
                  value=""
                  inputBorder="	1px solid #C0C8CD"
                />
              </NoResultLabel>
              <div style={{ width: '166px' }}>
                <Button
                  href="/"
                  text="Submit"
                  marginTop="18px"
                  marginRight="auto"
                  type="secondary"
                  headerButton
                />
              </div>
            </NoResultform>
            <Flex column>
              <TextMsg>
                Please check Preschools/Daycares near “Queens Road” that meet
                your requirements.
              </TextMsg>
              <Flex wrap>
                <SchoolCard
                  category="pre-school"
                  schoolImage={daycare}
                  schoolName="Euro Kids"
                  schoolBranch="Whitefield"
                  schoolRating="4.3"
                  schoolDesc="8:30AM-3:00PM · Full time · Flexible · Part time"
                  photocardType="liked"
                  cardtype="schoolCardColumn"
                  schoolCardWidth="273px"
                  schoolCardHeight="170px"
                />
                <SchoolCard
                  category="Pre-school / Day care"
                  schoolImage={daycare}
                  schoolName="Euro Kids"
                  schoolBranch="Whitefield"
                  schoolRating="4.3"
                  schoolDesc="8:30AM-3:00PM · Full time · Flexible · Part time"
                  photocardType="liked"
                  cardtype="schoolCardColumn"
                  schoolCardWidth="273px"
                  schoolCardHeight="170px"
                />
                <SchoolCard
                  category="pre-school"
                  schoolImage={daycare}
                  schoolName="Euro Kids"
                  schoolBranch="Whitefield"
                  schoolRating="4.3"
                  schoolDesc="8:30AM-3:00PM · Full time · Flexible · Part time"
                  photocardType="liked"
                  cardtype="schoolCardColumn"
                  schoolCardWidth="273px"
                  schoolCardHeight="170px"
                  schoolWrapperMarRight="0px"
                />
              </Flex>
              <Flex justifyCenter flexMargin="70px 0px 160px 0px">
                <Button href="/" text="Load More" type="outline" />
              </Flex>
            </Flex>
          </MainContent>
          <MapWrapper flexWidth="34.5%" flexHeight="auto" whitebg="#FFF">
            <MapLoction>
              <img src={background} alt="" />
              <SearchCheckbox>
                <CheckBox
                  label="Search as I move the map"
                  id="id10"
                  margin="0px 25px 0px 0px"
                  filterFont="13px"
                  filterChekbox="5px"
                  filterLineheight="15px"
                />
              </SearchCheckbox>
            </MapLoction>
          </MapWrapper>
        </Flex>
        {/* <Button href="/" text="Primary Button" fullwidth />
       
      <Button href="/" text="Header Primary Button" fullwidth headerButton />
      
    
      <Button href="/" text="Header white Button" type="btnWhite" />
      <Button href="/" text="Header white Button" type="btnWhite footer" />
      <Button href="/" text="Secondary Button" type="secondary" />
      <Button href="/" text="Outline Button" type="outline" />
      <Button href="/" text="link" type="links" /> */}
      </MainWrapper>
      <FilterFooter>
        Be a part of our Kiddenz chain.
        <FilterLink>Register your school</FilterLink>
      </FilterFooter>
    </>
  );
}
