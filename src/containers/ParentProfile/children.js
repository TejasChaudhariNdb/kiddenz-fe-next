/* eslint-disable eqeqeq */
/* eslint-disable no-nested-ternary */
/* eslint-disable indent */
import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import moment from 'moment';
import PropTypes from 'prop-types';
import { useProfileHook, useChildrenHook } from 'shared/hooks';
import Header from '../../components/Header';
import IconCard from '../../components/Iconcard';
import Flex from '../../components/common/flex';
import Button from '../../components/Button';
import MainWrapper from '../../components/MainWrapper';
// import profilePic from '../../images/profile.jpg';
import profilePic from '../../images/parents-placeholder.png';
import maleKid from '../../images/maleKid.png';
import femaleKid from '../../images/femaleKid.png';

const Children = ({ successToast, errorToast, ...props }) => {
  const [active, setActive] = useState(0);
  const [modalStatus, setModalStatus] = useState(false);
  const [modalType, setModalType] = useState('');
  const [isEdit, setIsEdit] = useState(false);
  const [editChildId, setEditChildId] = useState(null);
  const [deleteId, setDeleteId] = useState(null);
  const [modalTitle, setModalTitle] = useState('');

  const onVerifyEmailSendSuccess = () => {
    setModalType('email');
    setModalStatus(true);
  };
  const onVerifyEmailSendError = ({ message }) =>
    errorToast(message || 'Something went wrong.');

  const onChildEditSuccess = () => {
    successToast('Successfully updated.');
    setModalType(null);
    setModalStatus(false);
  };
  const onChildEditError = ({ message }) =>
    errorToast(message || 'Something went wrong.');

  const onChildDeleteSuccess = () => successToast('Successfully deleted.');
  const onChildDeleteError = ({ message }) =>
    errorToast(message || 'Something went wrong.');

  const { name, email, mobile, isMailVerified, onVerifyEmail } = useProfileHook(
    props,
    {
      onVerifyEmailSendSuccess,
      onVerifyEmailSendError,
    },
  );

  const {
    childList = [],
    onChildDelete,
    deleteLoader,
    resetForm,
  } = useChildrenHook(props, {
    onChildDeleteSuccess,
    onChildDeleteError,
    onChildEditSuccess,
    onChildEditError,
  });

  const {
    authentication: {
      profile: { data: { photo_url: photoUrl } = {} } = {},
    } = {},
  } = props;

  const isInValidYear = date => new Date(date).getFullYear() === 1000;

  return (
    <ChildrenInfo>
      <Header
        type="articles"
        placeHolderValue="Search Article"
        isModalActive={modalStatus}
        activeModalType={modalType}
        setActiveCallback={val => {
          setModalStatus(val);
          setDeleteId(null);
        }}
        successToast={successToast}
        errorToast={errorToast}
        isChildEdit={isEdit}
        editChildId={editChildId}
        confirmationText="Are you sure you want to delete this profile ?"
        onConfirmClick={() => {
          onChildDelete(deleteId);
          setActive(null);
          setModalStatus(false);
          setModalType(null);
          setDeleteId(null);
        }}
        email={email}
        childModalTitle={modalTitle}
        {...props}
      />
      <MainWrapper background="#fff">
        <Container>
          <ParentCard>
            <ParentPhoto>
              <img src={photoUrl || profilePic} alt="" />
            </ParentPhoto>
            <Flex column flexWidth="100%" flexMargin="50px 0px 0px">
              <Name>{name}</Name>
              <Number>
                +91 {mobile}
                <span
                  className="iconify"
                  data-icon="feather:check"
                  data-inline="false"
                />
              </Number>
              <Email>
                {email}
                {isMailVerified ? (
                  <VerfiedMail>
                    <span
                      className="iconify"
                      data-icon="feather:check"
                      data-inline="false"
                    />
                  </VerfiedMail>
                ) : (
                  <Button
                    type="attach"
                    text="Verify"
                    onClick={() => onVerifyEmail(email)}
                  />
                )}
              </Email>
            </Flex>
          </ParentCard>
          <Flex flexWidth="67%" column>
            <Flex
              justifyBetween
              flexWidth="100%"
              flexMargin="0px 0px 50px"
              alignCenter
            >
              <KiddenText fontSize="32px">My Children</KiddenText>
              <Button
                type="subscribe"
                text="Add Child"
                onClick={() => {
                  resetForm();
                  setIsEdit(false);
                  setModalStatus(true);
                  setModalType('addChild');
                  setModalTitle('Add a Child');
                }}
              />
            </Flex>
            {childList.length > 0 ? (
              childList.map((d, i) => (
                <ChildCard
                  // image={maleKid}
                  image={
                    d.gender_type === 'male'
                      ? maleKid
                      : d.gender_type === 'female'
                        ? femaleKid
                        : maleKid
                  }
                  active={active}
                  setActive={setActive}
                  index={i}
                  name={`${d.first_name} ${d.last_name}`}
                  dob={isInValidYear(d.date_of_birth) ? null : d.date_of_birth}
                  age={
                    isInValidYear(d.date_of_birth)
                      ? d.age
                      : getAge(d.date_of_birth)
                  }
                  gender={d.gender_type}
                  experience={d.child_care_experience}
                  foodType={d.food_type}
                  foodChoice={d.food_choice}
                  allergies={d.allergies || '-'}
                  medicalDetails={d.medication_details || '-'}
                  comments={d.comments || '-'}
                  tution={d.tution || '-'}
                  courses={d.courses || '-'}
                  specialCare={d.special_care ? 'Yes' : 'No'}
                  specificNeeds={d.other_specific_needs || '-'}
                  onEdit={() => {
                    setIsEdit(true);
                    setModalStatus(true);
                    setEditChildId(d.id);
                    setModalType('addChild');
                    setModalTitle('Edit Child');
                  }}
                  deleteLoader={deleteLoader}
                  setModalType={setModalType}
                  setModalStatus={setModalStatus}
                  onDeleteClick={() => {
                    setDeleteId(d.id);
                  }}
                  // deleteLoader={true}
                />
              ))
            ) : (
              <ExploreArticle width="30%">
                <IconCard iconName="fa-solid:child" type="favourite" />
                <h4>Nothing added yet</h4>
                <Button type="subscribe" text="Explore Schools" />
              </ExploreArticle>
            )}
          </Flex>
        </Container>
      </MainWrapper>
    </ChildrenInfo>
  );
};

export default Children;

Children.propTypes = {
  successToast: PropTypes.func,
  errorToast: PropTypes.func,
  onChildDelete: PropTypes.func,
  authentication: PropTypes.object,
};

const commaSeperatorBeautifier = commaString => {
  if (commaString !== '-') {
    const tempExp = commaString.split(',');
    if (tempExp.length > 1) {
      const lastElm = tempExp[tempExp.length - 1];
      tempExp[tempExp.length - 1] = 'and';
      tempExp.push(lastElm);
    }

    const modArr = tempExp.map(
      d => (d !== 'and' ? d.charAt(0).toUpperCase() + d.slice(1) : 'and'),
    );

    return modArr.join(' ');
  }
  return '-';
};

const ChildCard = ({
  image,
  active,
  setActive,
  index,
  name,
  dob,
  age,
  gender,
  experience,
  foodType,
  foodChoice,
  allergies,
  medicalDetails,
  comments,
  tution,
  courses,
  specialCare,
  specificNeeds,
  onEdit,
  onDeleteClick,
  deleteLoader,
  setModalStatus,
  setModalType,
}) => {
  const [mobileView, setMobileView] = useState(false);

  useEffect(
    () => {
      if (global.window && window.screen.width < 500) {
        setMobileView(true);
      } else {
        setMobileView(false);
      }
    },
    [global.window && global.window.screen.width],
  );
  return (
    <ChildDetails
      className={active === index ? 'active' : ''}
      style={mobileView && name && active !== index ? { maxHeight: 122 } : {}}
    >
      <Flex
        justifyBetween
        alignCenter
        flexPadding="28px 0px"
        className="kidDetails-flex"
        onClick={() => setActive(active === index ? null : index)}
      >
        <ChildPhoto>
          {' '}
          <img src={image} alt="" />
        </ChildPhoto>
        <Flex column flexWidth="80%" className="kidDetails">
          {name && <Childname>{name}</Childname>}
          <Flex
            flexWidth="90%"
            justifyBetween
            flexMargin="10px 0px 0px"
            className="kidDetails-content"
          >
            <h2>
              Age: <span> {age}</span>
            </h2>
            <h2>
              Gender: <span> {gender !== 'null' ? gender : '-'}</span>
            </h2>
            <h2>
              Date of Birth:{' '}
              <span> {dob ? moment(dob).format('DD/MM/YYYY') : '-'}</span>
            </h2>
          </Flex>
        </Flex>
        <ArrowIcon className={active === index ? 'active' : ''}>
          <span
            className="iconify"
            data-icon="simple-line-icons:arrow-down"
            data-inline="false"
          />
        </ArrowIcon>
      </Flex>
      <Flex className="dropdown" flexPadding="28px 0px" column>
        <h2 className="column">
          Previous childcare experience
          <span>{experience}</span>
        </h2>
        <h2 className="column">
          Food Type
          <span>
            {foodType
              ? foodType[0] === ','
                ? commaSeperatorBeautifier(foodType.slice(1))
                : commaSeperatorBeautifier(foodType)
              : ''}
          </span>
        </h2>
        <h2 className="column">
          Food Choice
          <span>
            {foodChoice
              ? foodChoice[0] === ','
                ? commaSeperatorBeautifier(foodChoice.slice(1))
                : commaSeperatorBeautifier(foodChoice)
              : ''}
          </span>
        </h2>
        <h2 className="column">
          Tuition
          <span>
            {tution
              ? tution[0] === ','
                ? commaSeperatorBeautifier(tution.slice(1))
                : commaSeperatorBeautifier(tution)
              : ''}
          </span>
        </h2>
        <h2 className="column">
          Courses
          <span>
            {courses
              ? courses[0] === ','
                ? commaSeperatorBeautifier(courses.slice(1))
                : commaSeperatorBeautifier(courses)
              : ''}
          </span>
        </h2>
        <h2 className="column">
          Allergies
          <span>{allergies.charAt(0).toUpperCase() + allergies.slice(1)}</span>
        </h2>
        <h2 className="column">
          Medical Details
          <span>
            {medicalDetails.charAt(0).toUpperCase() + medicalDetails.slice(1)}
          </span>
        </h2>
        <h2 className="column">
          Special care needed
          <span>{specialCare}</span>
        </h2>
        <h2 className="column">
          Other specific needs
          <span>
            {specificNeeds.charAt(0).toUpperCase() + specificNeeds.slice(1)}
          </span>
        </h2>
        <h2 className="column">
          Additional Info
          <span>{comments.charAt(0).toUpperCase() + comments.slice(1)}</span>
        </h2>
        <Flex justifyEnd>
          <Button
            type="subscribe"
            text="Edit"
            marginRight="20px"
            onClick={onEdit}
          />
          <Button
            type="subscribe"
            text="Delete"
            onClick={() => {
              onDeleteClick();
              setModalType('confirmation');
              setModalStatus(true);
            }}
            isLoading={deleteLoader}
          />
        </Flex>
      </Flex>
    </ChildDetails>
  );
};

function getAge(dateString) {
  // milliseconds in a year 1000*24*60*60*365.24 = 31556736000;
  const today = new Date();
  // birthay has 'Dec 25 1998'
  const dob = new Date(dateString);
  // difference in milliseconds
  const diff = today.getTime() - dob.getTime();
  // convert milliseconds into years
  const years = Math.floor(diff / 31556736000);
  // 1 day has 86400000 milliseconds
  const daysDiff = Math.floor((diff % 31556736000) / 86400000);
  // 1 month has 30.4167 days
  const months = Math.floor(daysDiff / 30.4167);

  return years == 0
    ? `${months} months`
    : months == 0
      ? `${years} years`
      : `${years} years ${months} months`;
}

ChildCard.propTypes = {
  active: PropTypes.number,
  setActive: PropTypes.func,
  setModalStatus: PropTypes.func,
  setModalType: PropTypes.func,
  onEdit: PropTypes.func,
  onDeleteClick: PropTypes.func,
  index: PropTypes.number,
  name: PropTypes.string,
  dob: PropTypes.string,
  age: PropTypes.string,
  gender: PropTypes.string,
  experience: PropTypes.string,
  foodType: PropTypes.string,
  foodChoice: PropTypes.string,
  allergies: PropTypes.string,
  medicalDetails: PropTypes.string,
  comments: PropTypes.string,
  tution: PropTypes.string,
  courses: PropTypes.string,
  specialCare: PropTypes.string,
  specificNeeds: PropTypes.string,
  image: PropTypes.string,
  deleteLoader: PropTypes.bool,
};

const ChildrenInfo = styled.div``;
const Container = styled.div`
  display: flex;
  justify-content: space-between;
  max-width: 1180px;
  padding: 100px 20px 0px;
  width: 100%;
  margin: 0px auto;
  @media (max-width: 500px) {
    flex-direction: column;
    padding-top: 30px;
    & > div {
      width: 100%;
    }
  }
  .subscribe {
    @media (max-width: 500px) {
      padding: 0px 30px;
      height: 40px;
      font-size: 14px;
    }
  }
`;

const ParentCard = styled.div`
  width: 30%;
  max-height: 324px;
  border: 1px solid #dbdee5;
  border-radius: 10px;
  background-color: #fff;
  color: #613a95;
  margin: ${props => props.margin || '0px 0px 20px'};
  padding: 30px;
  @media (max-width: 500px) {
    width: 100%;
    padding: 20px;
    margin-bottom: 50px;
    ${Flex} {
      margin-top: 20px;
    }
  }
`;

const ParentPhoto = styled.div`
  height: 100px;
  min-width: 100px;
  max-width: 100px;

  border-radius: 50%;
  img {
    width: 100%;
    height: 100%;
    border-radius: 50%;
  }
`;
const Name = styled.div`
  color: #30333b;
  font-size: 20px;
  line-height: 24px;
  margin-bottom: 20px;
  @media (max-width: 500px) {
    font-size: 16px;
    line-height: 20px;
  }
`;
const Number = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  color: #30333b;
  font-size: 16px;
  line-height: 19px;
  margin-bottom: 10px;
  .iconify {
    color: #60b947;
    width: 22px;
    height: 22px;
  }
  @media (max-width: 500px) {
    font-size: 14px;
    line-height: 17px;
  }
`;
const Email = styled.div`
  width: 100%;
  color: #30333b;
  font-size: 16px;
  line-height: 19px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const ChildDetails = styled.div`
  max-height: 110px;
  width: 100%;
  border-radius: 10px;
  background-color: #f2eff5;
  margin-bottom: 20px;
  overflow: hidden;
  border: 1px solid #f2eff5;
  padding: 0px 28px;
  transition: max-height 0.5s ease-in-out;
  @media (max-width: 500px) {
    padding: 0px 10px;
    max-height: 98px;
  }
  @media (max-width: 320px) {
    margin-top: 30px;
  }
  &.active {
    max-height: 1500px;
    transition: max-height 0.5s ease-in-out;
    border: 1px solid #e0e3eb;
    border-radius: 10px;
    background-color: #ffffff;
    box-shadow: 0 0 30px 0 rgba(0, 0, 0, 0.05);
  }
  .kidDetails-flex {
    cursor: pointer;
    @media (max-width: 500px) {
      padding: 10px 0px;
    }
  }
  .kidDetails {
    @media (max-width: 500px) {
      width: 70%;
    }
  }
  .kidDetails-content {
    @media (max-width: 500px) {
      flex-direction: column;
    }
  }
  .dropdown {
    border-top: 1px solid #dbdee5;
  }
  h2 {
    display: flex;
    color: #666c78;
    font-family: Roboto;
    font-size: 16px;
    line-height: 19px;
    font-weight: 400;
    margin: 0px;
    span {
      color: #30333b;
      margin-left: 5px;
      text-transform: capitalize;
    }
    @media (max-width: 500px) {
      margin-bottom: 3px;
      font-size: 12px;
    }
    &.column {
      flex-direction: column;
      margin-bottom: 30px;
      span {
        margin-top: 10px;
      }
    }
  }
`;

const ChildPhoto = styled.div`
  height: 54px;
  width: 54px;
  min-width: 54px;
  border: 2px solid #ffffff;
  border-radius: 50px;
  overflow: hidden;
  img {
    width: 100%;
    height: 100%;
  }
  @media (max-width: 500px) {
    height: 40px;
    width: 40px;
    min-width: 40px;
    margin-right: 10px;
  }
`;

const Childname = styled.div`
  color: #30333b;
  font-size: 20px;
  font-weight: 600;
  font-family: 'Quicksand', sans-serif;
  line-height: 25px;
`;

const ArrowIcon = styled.div`
  padding: 3px;
  cursor: pointer;
  transform: rotate(0deg);
  transition: transform 0.2s ease;
  .iconify {
    color: #30333b;
    width: 14px;
    height: 14px;
  }
  &.active {
    transform: rotate(180deg);
    transition: transform 0.2s ease;
  }
`;
const KiddenText = styled.div`
  font-family: 'Quicksand', sans-serif;
  color: #30333b;
  font-size: ${props => props.fontSize || '24px'};
  font-weight: bold;
  line-height: 30px;
  text-align: center;
  @media screen and (max-width: 900px) {
    font-size: 20px;
  }
`;

const VerfiedMail = styled.div`
  font-size: 22px;
  color: #60b947;
`;

const ExploreArticle = styled.div`
  width: ${props => props.width || '100%'};
  display: flex;
  flex-direction: column;
  align-items: center;
  align-self: center;
  margin-top: 100px;
  margin-bottom: 200px;
  &.show {
    display: flex;
  }
  h4 {
    color: #30333b;
    font-size: 16px;
    font-weight: 600;
    line-height: 20px;
    margin-top: 24px;
    text-align: center;
  }
  h3 {
    color: #613a95;
    font-size: 12px;
    font-weight: 500;
    line-height: 15px;
    text-align: center;
  }
`;
