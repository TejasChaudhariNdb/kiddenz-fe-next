/* eslint-disable indent */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable no-nested-ternary */
import React, { useState } from 'react';
import styled from 'styled-components';
import Router from 'next/router';
import moment from 'moment';
import PropTypes from 'prop-types';
import {
  useProfileHook,
  useScheduleHook,
  useChildrenHook,
  useWishlistHook,
  useBookmarkHook,
  useBookmarkProgramHook,
  // usePurchasedOnlineProgramHook,
  useTransactionHook,
} from 'shared/hooks';
import Modal from 'components/Modal';
import Header from '../../components/Header';
import MainWrapper from '../../components/MainWrapper';
import Flex from '../../components/common/flex';
import SchoolCard from '../../components/SchoolCard';
import Transaction from '../../components/Transaction';
import IconCard from '../../components/Iconcard';
import ParentInfo from '../../components/ParentInfo';
import Button from '../../components/Button';

// import profile from '../../images/profile.jpg';
import profile from '../../images/parents-placeholder.png';
import buildingPlaceholder from '../../images/building-placeholder.png';
import pulseLoader from '../../images/pulse_loader.gif';
// import placeholderImage from '../../images/ReadingImage1.png';

// const SCHEDULE_ITEM_CONFIG = {
//   pending: {
//     true: {},
//     false: {

//     },
//   },
// };

const scheduleButtontext = ({ type, status, isAdminSchedule }) => {
  if (type === 'pending') {
    if (isAdminSchedule) {
      return 'Schedule change';
    }
    return 'Pending';
  }

  if (type === 'schedule' && status === 'confirmed') {
    return 'Confirmed';
  }

  if (type === 'schedule' && status === 'declined') {
    return 'Declined';
  }
  return 0;
};

const scheduleButtonType = ({ type, status }) => {
  if (type === 'pending') {
    return 'pending';
  }

  if (type === 'schedule' && status === 'confirmed') {
    return 'confirm';
  }

  if (type === 'schedule' && status === 'declined') {
    return 'decline';
  }
  return 0;
};

const ParentProfile = ({ successToast, errorToast, ...props }) => {
  const {
    authentication: {
      isLoggedIn,
      profile: { data: { photo_url: photoUrl } = {} } = {},
    } = {},
  } = props;
  const [isUpdate, setIsUpdate] = useState(false);
  const [modalStatus, setModalStatus] = useState(false);
  const [modalType, setModalType] = useState('email');
  const [providerId, setProviderId] = useState(null);
  const [selectedSchedule, setSelectedSchedule] = useState({});
  const [isForceSchedule, setIsForceSchedule] = useState(false);
  const [batchInfo, setBatchInfo] = useState(null);

  const PROVIDER_MAP = ['Pre-school', 'Day care', 'Pre-school / Day care'];

  const onProfileEditSuccess = () => {
    successToast('Successfully updated.');
    setIsUpdate(false);
  };
  const onProfileEditError = ({ message }) => errorToast(message);

  const onVerifyEmailSendSuccess = () => {
    Router.push('/email');
    // setModalType('email');
    // setModalStatus(true);
  };
  const onVerifyEmailSendError = ({ message }) =>
    errorToast(message || 'Something went wrong.');

  // const onWishlistAddError = () => {};
  const onWishlistDelError = message =>
    errorToast(message || 'Something went wrong.');

  const onBookmarkDelError = message =>
    errorToast(message || 'Something went wrong.');

  React.useEffect(
    () => () => {
      setModalType('');
    },
    [],
  );

  const {
    name,
    email,
    mobile,
    isMailVerified,
    onChange,
    onBlur,
    onEditCancel,
    onUpdateSubmit,
    editLoader,
    onVerifyEmail,
    handleImageChnage,
    uploadedImage,
    resetUploadedImage,
    setTransportCords,
    setAreaCords,
    locationSearchSting,
    setLocationSearchSting,
    area,
    setArea,
    locationSearchError,
    setLocationSearchError,
    transportLocationSearchError,
    setTransportLocationSearchError,
    isImageUploading,
  } = useProfileHook(props, {
    onProfileEditSuccess,
    onProfileEditError,
    onVerifyEmailSendSuccess,
    onVerifyEmailSendError,
  });

  const onScheduleEditSuccess = () => {
    successToast('Re-schedules successfully.');
  };
  const onScheduleEditError = ({ message }) =>
    errorToast(message || 'Something went worng.');

  const scheduleCheckCallback = ({
    data: {
      data: { schedule_tour_created: isTourScheduled, data = [] } = {},
    } = {},
    provId,
  }) => {
    const schduleDetails = data[0];
    if (isTourScheduled) {
      setProviderId(provId);
      setSelectedSchedule(schduleDetails);
      setModalStatus(true);
      setModalType('forceReschule');
      // onRescheduleClick
    }
    if (!isTourScheduled) {
      setIsForceSchedule(true);
      setModalStatus(true);
      setModalType('tour');
      setProviderId(provId);
    }
  };

  const {
    schedule: {
      data: scheduledTours = [],
      schedule_count: scheduleCount = 0,
    } = {},
    unSchedule: {
      data: unScheduledTours = [],
      unschedule_count: unScheduleCount = 0,
    } = {},
    onScheduleLoadMore,
    onUnscheduleLoadMore,
    scheduleLoader,
    unScheduleLoader,
    checkTourExists,
    scheduleCheckLoader,
    // scheduleCheckData: { data } = {},
    // onScheduleSubmit,
  } = useScheduleHook(props, {
    onScheduleEditSuccess,
    onScheduleEditError,
    scheduleCheckCallback,
  });

  const { childList = [] } = useChildrenHook(props, {});

  const {
    wishList: { wishlists: wishList = [], count: wishlistCount } = {},
    postWishlist,
    onWishlistLoadMore,
    wishlistLoader = false,
    deleteWishlist,
  } = useWishlistHook(props, { onWishlistDelError });

  const { bookmarks = [], removeBookmark } = useBookmarkHook(props, {
    onBookmarkDelError,
  });

  const {
    bookmarkedPrograms: { data: bookmarkedPrograms, loader: bookmarkLoader },
    removeBookmark: removeBookmamrkProgram,
  } = useBookmarkProgramHook(props);

  const {
    transactionList: { data: transactionList, loader: transactionLoader },
    onDownloadInvoice,
  } = useTransactionHook(props, {
    onDownloadSuccess,
    onDownloadError,
  });

  const downloadURI = (uri, urlName) => {
    const link = document.createElement('a');
    link.download = urlName;
    link.href = uri;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
    // delete link;
  };

  function onDownloadSuccess({ data: { data } = {}, transactionId }) {
    // window.location.href = data;
    downloadURI(data, `Invoice-${transactionId}`);
    successToast('Invoice downloaded successfully');
  }
  function onDownloadError({ message }) {
    errorToast(message || 'Something went wrong');
  }

  const handleDownloadInvoice = (id, transactionId) => {
    onDownloadInvoice(id, transactionId);
  };

  const handleCardClick = (programName, programId) => {
    if (programId) {
      const query = { id: programId };
      const url = { pathname: '/online-program/[program-name]', query };
      const urlAs = {
        pathname: `/online-program/${programName
          .replace(/\s+/g, '-')
          .replace('/', '-')
          .toLowerCase()}`,
        query,
      };

      Router.push(url, urlAs).then(() => window.scrollTo(0, 0));
    }
  };

  const handleShowSession = batch => {
    setModalType('onlineProgramBatches');
    setBatchInfo(batch);
    setModalStatus(true);
  };

  const closeHandler = () => {
    setModalStatus(false);
    setBatchInfo(null);
    // setModalActive(false);
  };

  const childrenLength = childList.length;
  return (
    <>
      <Parentprofile>
        <Header
          type="articles"
          placeHolderValue="Search Article"
          isModalActive={modalStatus}
          activeModalType={modalType}
          setActiveCallback={setModalStatus}
          successToast={successToast}
          errorToast={errorToast}
          providerId={providerId}
          selectedSchedule={selectedSchedule}
          isForceSchedule={isForceSchedule}
          setIsForceSchedule={setIsForceSchedule}
          email={email}
          {...props}
          // searchBar
        />
        <MainWrapper background="#fff">
          <Container>
            {/* <NewNotification>
              <h2>You have new notifications</h2>
              <span>View</span>
            </NewNotification> */}

            <Flex justifyBetween className="parentDetails">
              <Flex flexWidth="65%" column flexMargin="0px 0px 300px">
                <ParentInfo
                  isUpdate={isUpdate}
                  setIsUpdate={setIsUpdate}
                  image={photoUrl || profile}
                  name={name}
                  email={email}
                  mobile={mobile}
                  isVerified={isMailVerified}
                  onChange={onChange}
                  onBlur={onBlur}
                  onCancel={() => {
                    resetUploadedImage();
                    onEditCancel();
                  }}
                  onVerify={onVerifyEmail}
                  onSubmit={onUpdateSubmit}
                  loader={editLoader}
                  handleImageChnage={handleImageChnage}
                  tempImage={uploadedImage}
                  setTransportCords={setTransportCords}
                  setAreaCords={setAreaCords}
                  locationSearchSting={locationSearchSting}
                  setLocationSearchSting={setLocationSearchSting}
                  area={area}
                  setArea={setArea}
                  locationSearchError={locationSearchError}
                  setLocationSearchError={setLocationSearchError}
                  transportLocationSearchError={transportLocationSearchError}
                  setTransportLocationSearchError={
                    setTransportLocationSearchError
                  }
                  isImageUploading={isImageUploading}
                />
                <AddChild>
                  <Flex column>
                    <h2>Children profiles</h2>
                    <ChildrenLink
                      onClick={() =>
                        Router.push('/child').then(() => window.scrollTo(0, 0))
                      }
                    >
                      {childrenLength === 0
                        ? 'No Children added'
                        : childrenLength === 1
                          ? '1 Child added'
                          : `${childrenLength} Children added`}
                    </ChildrenLink>
                  </Flex>
                  <Button
                    text="Add Child"
                    type="links"
                    onClick={() => {
                      setModalStatus(true);
                      setModalType('addChild');
                    }}
                  />
                </AddChild>

                <Flex
                  justifyBetween
                  className="mainHeading"
                  flexWidth="100%"
                  flexMargin="60px 0px 30px"
                >
                  <KiddenText fontSize="32px">
                    <span className="liteFont">Saved</span> Online Programs
                  </KiddenText>
                  <div />
                </Flex>
                {bookmarkedPrograms.length > 0 ? (
                  <>
                    {bookmarkLoader ? (
                      <ProgramLoader>
                        <Flex
                          alignCenter
                          justifyCenter
                          flexWidth="100%"
                          flexHeight="100%"
                        >
                          <img
                            src={pulseLoader}
                            alt=""
                            height={75}
                            width={75}
                          />
                        </Flex>
                      </ProgramLoader>
                    ) : (
                      <Flex column>
                        {bookmarkedPrograms.map(d => (
                          <SchoolCard
                            isLoggedIn={isLoggedIn}
                            category={PROVIDER_MAP.child_care_providers}
                            schoolImage={
                              d.online_program_detail &&
                              d.online_program_detail.medias &&
                              d.online_program_detail.medias.length
                                ? d.online_program_detail.medias.filter(
                                    media => media.media_type === 'image',
                                  )[0].media_url
                                : null
                            }
                            schoolName={
                              d.online_program_detail &&
                              d.online_program_detail.name
                            }
                            schoolBranch={
                              d.online_program_detail &&
                              d.online_program_detail.provider_detail &&
                              d.online_program_detail.provider_detail.name
                            }
                            // purchasedBatch={d.purchased_batch}
                            schoolCardWidth="245px"
                            schoolCardHeight="153px"
                            schoolWrapperMarTop="0px"
                            schoolWrapperMarRight="0px"
                            padding="28px 0px"
                            isWishlisted
                            onWishlistIconClick={() =>
                              removeBookmamrkProgram(d.online_program_detail.id)
                            }
                            onCardClick={() =>
                              handleCardClick(
                                d.online_program_detail.name,
                                d.online_program_detail.id,
                              )
                            }
                            href={{
                              pathname: '/online-program/[program-name]',
                              query: { id: d.online_program_detail.id },
                            }}
                            redirectionLink={`/online-program/${d.online_program_detail.name
                              .replace(/\s+/g, '-')
                              .replace('/', '-')
                              .toLowerCase()}?id=${d.online_program_detail &&
                              d.online_program_detail.id}`}
                            schoolRating={
                              d.online_program_detail &&
                              d.online_program_detail.rating &&
                              d.online_program_detail.rating.rating__avg
                            }
                            photocardType="liked"
                            cardtype="wishList"
                          />
                        ))}

                        {/* {programCount !== purchasedPrograms.length && (
                        // {wishList.length > 0 && (
                        <Flex justifyCenter flexMargin="70px 0px 50px 0px">
                          <Button
                            text="Load More"
                            type="outline"
                            onClick={() => onProgramLoadMore(wishList.length)}
                            isLoading={wishlistLoader}
                          />
                        </Flex>
                      )} */}
                      </Flex>
                    )}
                  </>
                ) : (
                  <EmptyState
                    // emptyStateIcon="ant-design:heart-filled"
                    noExplore
                    message="Nothing added yet"
                  />
                )}
                <Flex
                  justifyBetween
                  className="mainHeading"
                  flexWidth="100%"
                  flexMargin="60px 0px 30px"
                >
                  <KiddenText fontSize="32px">
                    <span className="liteFont">Enrolled</span> Courses
                  </KiddenText>
                  <div />
                </Flex>
                {transactionList.length > 0 ? (
                  <>
                    {transactionLoader ? (
                      <ProgramLoader>
                        <Flex
                          alignCenter
                          justifyCenter
                          flexWidth="100%"
                          flexHeight="100%"
                        >
                          <img
                            src={pulseLoader}
                            alt=""
                            height={75}
                            width={75}
                          />
                        </Flex>
                      </ProgramLoader>
                    ) : (
                      <>
                        {transactionList.map(d => (
                          <Transaction
                            name={d.name}
                            image={
                              d.medias && d.medias.length
                                ? d.medias.filter(
                                    media => media.media_type === 'image',
                                  )[0].media_url
                                : null
                            }
                            batchName={
                              d.purchased_batch &&
                              d.purchased_batch.heading_name
                            }
                            transaction={d.transaction_details}
                            onDownloadInvoice={(e, transactionId) => {
                              e.preventDefault();
                              e.stopPropagation();
                              handleDownloadInvoice(d.id, transactionId);
                            }}
                            onClick={e => {
                              e.preventDefault();
                              e.stopPropagation();
                              handleCardClick(
                                d.name,
                                d.purchased_batch &&
                                  d.purchased_batch.online_program,
                              );
                            }}
                            onShowSession={e => {
                              e.preventDefault();
                              e.stopPropagation();
                              handleShowSession(d.purchased_batch);
                            }}
                            purchasedBatch={d.purchased_batch}
                          />
                        ))}
                      </>
                    )}
                  </>
                ) : (
                  <EmptyState
                    // emptyStateIcon="ant-design:heart-filled"
                    noExplore
                    message="No Enrolled Courses"
                  />
                )}

                <Flex
                  justifyBetween
                  className="mainHeading"
                  flexWidth="100%"
                  flexMargin="60px 0px 30px"
                >
                  <KiddenText fontSize="32px">
                    <span className="liteFont">My Wishlist</span> (Preschool &
                    Daycare)
                  </KiddenText>
                  <div />
                </Flex>
                {wishList.length > 0 ? (
                  <>
                    <Flex column>
                      {wishList.map(d => (
                        <SchoolCard
                          isLoggedIn={isLoggedIn}
                          category={PROVIDER_MAP.child_care_providers}
                          schoolImage={
                            d.thumb_url || d.url
                              ? (d.thumb_url || d.url).includes('://')
                                ? d.thumb_url || d.url
                                : `https://${d.thumb_url || d.url}`
                              : d.provider_pic &&
                                d.provider_pic[0] &&
                                d.provider_pic[0].media_url
                                ? d.provider_pic[0].media_url.includes('://')
                                  ? d.provider_pic[0].media_url
                                  : `https://${d.provider_pic[0].media_url}`
                                : buildingPlaceholder
                          }
                          schoolName={d.business_name || d.name || ''}
                          schoolBranch={d.area || d.area_name || ''}
                          schoolCardWidth="245px"
                          schoolCardHeight="153px"
                          schoolWrapperMarTop="0px"
                          schoolWrapperMarRight="0px"
                          padding="28px 0px"
                          isWishlisted
                          onWishlistIconClick={() =>
                            deleteWishlist(d.id, 'wishlist')
                          }
                          redirectionLink={`/daycare/${d.state ||
                            null}/${d.city || null}/${d.area ||
                            d.area_name ||
                            ''}/${
                            d.slug
                          }?wishlist=true&name=${d.business_name ||
                            d.name ||
                            ''}&type=${PROVIDER_MAP.child_care_providers ||
                            ''}`}
                          // Temp
                          // schoolRating="4.3"
                          photocardType="liked"
                          cardtype="wishList"
                        />
                      ))}

                      {wishlistCount !== wishList.length && (
                        // {wishList.length > 0 && (
                        <Flex justifyCenter flexMargin="70px 0px 50px 0px">
                          <Button
                            text="Load More"
                            type="outline"
                            onClick={() => onWishlistLoadMore(wishList.length)}
                            isLoading={wishlistLoader}
                          />
                        </Flex>
                      )}
                    </Flex>
                  </>
                ) : (
                  <EmptyState
                    emptyStateIcon="ant-design:heart-filled"
                    noExplore
                  />
                )}

                <Flex
                  justifyBetween
                  flexWidth="100%"
                  flexMargin="60px 0px 30px"
                  className="mainHeading"
                >
                  <KiddenText fontSize="32px">
                    {' '}
                    <span className="liteFont">Scheduled</span> Visits
                  </KiddenText>
                  <div />
                </Flex>
                {scheduledTours.length > 0 || unScheduledTours.length > 0 ? (
                  <>
                    {/* {scheduledTours.length > 0 && (
                      <Flex justifyBetween flexWidth="100%">
                        <KiddenText fontSize="20px">Scheduled Tours</KiddenText>
                        <div />
                      </Flex>
                    )} */}

                    <Flex column>
                      {(Array.isArray(scheduledTours)
                        ? scheduledTours
                        : []
                      ).map(d => (
                        <SchoolCard
                          hasCancelReason={
                            (d.is_admin_schduled &&
                              d.schedule_status !== 'confirmed') ||
                            d.cancel_reason
                          }
                          isLoggedIn={isLoggedIn}
                          category={
                            PROVIDER_MAP[d.provider_detail.child_care_providers]
                          }
                          schoolImage={
                            d.provider_pic[0] && d.provider_pic[0].media_url
                              ? d.provider_pic[0].media_url.includes('://')
                                ? d.provider_pic[0].media_url
                                : `https://${d.provider_pic[0].media_url}`
                              : buildingPlaceholder
                          }
                          schoolName={d.provider_detail.business_name}
                          schoolBranch={d.area_name}
                          tourID={d.tour_id}
                          schedule={`${moment(
                            d[
                              d.is_admin_schduled &&
                              d.schedule_type === 'pending'
                                ? 'old_date'
                                : 'date'
                            ],
                          ).format('MMMM DD')}, ${tConvert(
                            d[
                              d.is_admin_schduled &&
                              d.schedule_type === 'pending'
                                ? 'old_time'
                                : 'time'
                            ]
                              .split(':')
                              .slice(0, 2)
                              .join(':'),
                          )}`}
                          contact={d.provider_detail.director_name}
                          btnCancel="Cancel / Reschedule"
                          btnSchedule="Cancel"
                          btnScheduleConfirm="Confirm"
                          schoolCardWidth="245px"
                          schoolCardHeight="153px"
                          schoolWrapperMarTop="0px"
                          schoolWrapperMarRight="0px"
                          padding="28px 0px"
                          // Temp
                          schoolRating="4.3"
                          photocardType="liked"
                          cardtype="wishList"
                          btnType={scheduleButtonType({
                            type: d.schedule_type,
                            status: d.schedule_status,
                          })}
                          btnText={scheduleButtontext({
                            type: d.schedule_type,
                            status: d.schedule_status,
                            isAdminSchedule: d.is_admin_schduled,
                          })}
                          isWishlisted={d.is_wishlisted}
                          reason={d.cancel_reason}
                          scheduledate={`${moment(d.date).format(
                            'dddd, MMM Do',
                          )} ${d.time}`}
                          scheduleID={d.tour_id}
                          redirectionLink={`/daycare/${d.provider_detail
                            .state || null}/${d.provider_detail.city ||
                            null}/${d.provider_detail.area ||
                            d.area_name ||
                            null}/${d.provider_detail.slug}?wishlist=${
                            d.is_wishlisted
                          }&name=${d.provider_detail.business_name}&type=${
                            PROVIDER_MAP[d.provider_detail.child_care_providers]
                          }`}
                          onWishlistIconClick={() => {
                            if (!d.is_wishlisted)
                              postWishlist(d.id, 'schedule');
                            else deleteWishlist(d.id, 'schedule');
                          }}
                          scheduleActions={(e, type) => {
                            e.stopPropagation();
                            setProviderId(d.provider);
                            setSelectedSchedule(d);
                            if (type === 'cancelSuggestion') {
                              setModalStatus(true);
                              setModalType('forceCancel');
                            }
                            if (type === 'confirmSuggestion') {
                              setModalStatus(true);
                              setModalType('forceConfirm');
                            }
                            if (type === 'option') {
                              setModalStatus(true);
                              setModalType('scheduleOptions');
                            }
                          }}
                          // schoolCardMar={
                          //   unScheduledTours.length > 0 &&
                          //   i === scheduledTours.length - 1
                          //     ? '0px 10px 300px 0px'
                          //     : 0
                          // }
                        />
                      ))}

                      {scheduleCount !== scheduledTours.length && (
                        <Flex justifyCenter flexMargin="70px 0px 50px 0px">
                          <Button
                            text="Load More"
                            type="outline"
                            onClick={() =>
                              onScheduleLoadMore(scheduledTours.length)
                            }
                            isLoading={scheduleLoader}
                          />
                        </Flex>
                      )}

                      {unScheduledTours.length > 0 && (
                        <Flex
                          justifyBetween
                          flexWidth="100%"
                          flexMargin="50px 0px -10px"
                        >
                          <KiddenText fontSize="20px">
                            Canceled Tours
                          </KiddenText>
                          <div />
                        </Flex>
                      )}

                      {unScheduledTours.map(d => (
                        <SchoolCard
                          isLoggedIn={isLoggedIn}
                          category={
                            PROVIDER_MAP[d.provider_detail.child_care_providers]
                          }
                          schoolImage={
                            d.provider_pic[0] && d.provider_pic[0].media_url
                              ? d.provider_pic[0].media_url.includes('://')
                                ? d.provider_pic[0].media_url
                                : `https://${d.provider_pic[0].media_url}`
                              : buildingPlaceholder
                          }
                          schoolName={d.provider_detail.business_name}
                          schoolBranch={d.area_name}
                          schoolRating="4.3"
                          photocardType="liked"
                          cardtype="wishList"
                          schoolCardWidth="245px"
                          schoolCardHeight="153px"
                          schoolWrapperMarTop="0px"
                          schoolWrapperMarRight="0px"
                          padding="30px 0px"
                          btnSchedule="Schedule Tour"
                          isWishlisted={d.is_wishlisted}
                          redirectionLink={`/daycare/${d.provider_detail
                            .state || null}/${d.provider_detail.city ||
                            null}/${d.provider_detail.area ||
                            d.area_name ||
                            null}/${d.provider_detail.slug}?wishlist=${
                            d.is_wishlisted
                          }&name=${d.provider_detail.business_name}&type=${
                            PROVIDER_MAP[d.provider_detail.child_care_providers]
                          }`}
                          onWishlistIconClick={() => {
                            if (!d.is_wishlisted)
                              postWishlist(d.id, 'unschedule');
                            else deleteWishlist(d.id, 'unschedule');
                          }}
                          scheduleCheckLoader={scheduleCheckLoader}
                          scheduleTour={e => {
                            e.stopPropagation();
                            checkTourExists(d.provider);
                          }}
                          // schoolCardMar={
                          //   i === unScheduledTours.length - 1
                          //     ? '0px 10px 300px 0px'
                          //     : 0
                          // }
                        />
                      ))}

                      {unScheduleCount !== unScheduledTours.length && (
                        <Flex justifyCenter flexMargin="70px 0px 50px 0px">
                          <Button
                            text="Load More"
                            type="outline"
                            onClick={() =>
                              onUnscheduleLoadMore(unScheduledTours.length)
                            }
                            isLoading={unScheduleLoader}
                          />
                        </Flex>
                      )}
                    </Flex>
                  </>
                ) : (
                  <EmptyState emptyStateIcon="gridicons:scheduled" noExplore />
                )}
              </Flex>

              <Flex flexWidth="31%" column>
                <Flex
                  justifyBetween
                  flexWidth="100%"
                  flexMargin="0px 0px 30px"
                  className="mainHeading"
                >
                  <KiddenText>Favourite articles</KiddenText>
                  <div />
                </Flex>
                {bookmarks.length > 0 ? (
                  <Flex column>
                    {bookmarks.map(
                      ({
                        post_title: title,
                        post_date: date,
                        post_image: img,
                        viewCount,
                        likeCount,
                        ...article
                      }) => (
                        <SchoolCard
                          isLoggedIn={isLoggedIn}
                          schoolImage={img}
                          schoolCardHeight="62px"
                          schoolCardWidth="100px"
                          schoolWrapperMarTop="0px"
                          mostRead={title}
                          dateNum={moment(date).format('MMM DD, YYYY')}
                          viewNum={viewCount}
                          likesNum={likeCount}
                          cardtype="mostRead"
                          photocardType="bookmark"
                          redirectionLink={`/blog/${
                            article.post_name
                          }?category=${article.category}&id=${
                            article.post_id
                          }&name=${article.post_title}`}
                          isBookmarked
                          onBookmarkIconClick={() =>
                            removeBookmark(article.post_id)
                          }
                        />
                      ),
                    )}
                  </Flex>
                ) : (
                  <EmptyState
                    emptyStateIcon="fa-solid:bookmark"
                    noExplore
                    width="100%"
                  />
                )}
              </Flex>
            </Flex>
          </Container>
        </MainWrapper>
      </Parentprofile>

      {modalType &&
        modalStatus && (
          <Modal
            type={modalType}
            setActive={setModalStatus}
            closeHandler={closeHandler}
            setModalType={setModalType}
            batchInfo={batchInfo}
            {...props}
          />
        )}
    </>
  );
};

export default ParentProfile;

ParentProfile.propTypes = {
  authentication: PropTypes.object,
  successToast: PropTypes.func,
  errorToast: PropTypes.func,
};

const EmptyState = ({
  noExplore,
  emptyStateIcon,
  width = '30%',
  message = 'Nothing added yet',
}) => (
  <ExploreArticle width={width}>
    {emptyStateIcon && <IconCard iconName={emptyStateIcon} type="favourite" />}
    <h4>{message}</h4>
    {!noExplore && <h3>Explore School</h3>}
  </ExploreArticle>
);

EmptyState.propTypes = {
  noExplore: PropTypes.bool,
  emptyStateIcon: PropTypes.string,
  width: PropTypes.string,
  message: PropTypes.string,
};

const tConvert = time => {
  let convertedTime;
  // Check correct time format and split into components
  convertedTime = time
    .toString()
    .match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

  if (convertedTime.length > 1) {
    // If time format correct
    convertedTime = convertedTime.slice(1); // Remove full string match value
    convertedTime[5] = +convertedTime[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
    convertedTime[0] = +convertedTime[0] % 12 || 12; // Adjust hours
  }
  return convertedTime.join(''); // return adjusted time or original string
};

const Parentprofile = styled.div``;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 1180px;
  width: 100%;
  margin: 0px auto;
  padding: 0px 20px;
  padding-top: 114px;
  // justify-content: space-between;
  @media screen and (max-width: 768px) {
    padding-top: 20px;
  }
  .parentDetails {
    .telDisable {
      font-size: 14px;
    }
    .number {
      top: 42px !important;
      @media screen and (max-width: 768px) {
        top: 37px !important;
      }
    }
    @media screen and (max-width: 768px) {
      flex-direction: column;

      & > div {
        width: 100%;
        margin-bottom: 0px;
      }
    }
  }
  .wishList {
    @media screen and (max-width: 768px) {
      width: 100%;
      margin-right: 0px;
      padding-top: 0px;
      padding-bottom: 0px;
    }
    & > div {
      @media screen and (max-width: 768px) {
        flex-direction: column;
      }
    }
    .liked {
      @media screen and (max-width: 768px) {
        width: 100%;
        height: 204px;
      }
    }
  }
  .wishlistContent {
    .name {
      width: 75%;
      @media screen and (max-width: 768px) {
        width: 100%;
      }
    }
    @media screen and (max-width: 768px) {
      margin: 16px 0px 0px;
      width: 90%;
    }
  }
  .mainHeading {
    @media screen and (max-width: 768px) {
      margin: 15px 0;
    }
  }
`;
const KiddenText = styled.div`
  font-family: 'Quicksand', sans-serif;
  // color: #30333b;
  color: #613a95;
  font-size: ${props => props.fontSize || '24px'};
  font-weight: bold;
  line-height: 30px;
  text-align: center;
  @media screen and (max-width: 900px) {
    font-size: 20px;
  }
  @media screen and (max-width: 768px) {
    font-size: 16px;
  }
  .liteFont {
    font-weight: 300;
  }
`;

const ExploreArticle = styled.div`
  width: ${props => props.width || '100%'};
  flex-direction: column;
  align-items: center;
  justify-content: center;
  &:last-child {
    margin-bottom: 300px;
  }
  @media screen and (max-width: 768px) {
    width: 100%;
  }
  &.show {
    display: flex;
  }
  h4 {
    color: #30333b;
    font-size: 16px;
    font-weight: 600;
    line-height: 20px;
    margin: 24px 0px 0px;
    text-align: center;
    @media screen and (max-width: 768px) {
      text-align: left;
    }
  }
  h3 {
    color: #613a95;
    font-size: 12px;
    font-weight: 500;
    line-height: 15px;
    text-align: center;
  }
`;
const AddChild = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 20px;
  width: 100%;
  border-radius: 10px;
  background-color: #f2eff5;
  @media screen and (max-width: 800px) {
    padding: 15px;
  }
  @media screen and (max-width: 768px) {
    flex-direction: column;
    align-items: flex-start;
  }
  h2 {
    color: #30333b;
    font-family: Quicksand;
    font-size: 26px;
    font-weight: 600;
    letter-spacing: 0;
    line-height: 33px;
    margin: 0px 0px 8px;
    @media screen and (max-width: 800px) {
      font-size: 22px;
      line-height: 28px;
    }
    @media screen and (max-width: 768px) {
      font-size: 16px;
      line-height: 20px;
    }
  }
  span {
    font-family: 'Quicksand', sans-serif;
    color: #613a95;
    font-size: 20px;
    font-weight: 600;
    line-height: 25px;
    @media screen and (max-width: 800px) {
      font-size: 16px;
      line-height: 20px;
    }
    @media screen and (max-width: 768px) {
      font-size: 14px;
      line-height: 18px;
    }
  }
  .links {
    padding: 16px 40px;
    border-color: #000;
    &:hover {
      box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2);
    }
    @media screen and (max-width: 800px) {
      padding: 12px 40px;
      font-size: 14px;
    }
    @media screen and (max-width: 768px) {
      padding: 10px 40px;
      margin-top: 16px;
      font-size: 12px;
    }
  }
`;

const ChildrenLink = styled.span`
  cursor: pointer;
`;

const ProgramLoader = styled.div`
  height: calc(100vh - 768px);
  width: 100%;
`;

// NOTE: Temporary
// const NewNotification = styled.div`
//   display: flex;
//   padding: 22px 32px;
//   margin: 0px 0px 50px;
//   background: #e2ffda;
//   padding: 22px 32px;
//   border-radius: 5px;
//   justify-content: space-between;
//   @media screen and (max-width: 768px) {
//     padding: 12px 16px;
//     align-items: center;
//     margin: 0px 0px 30px;
//   }
//   h2 {
//     font-family: Quicksand;
//     font-size: 20px;
//     font-weight: 600;
//     letter-spacing: 0;
//     line-height: 25px;
//     margin: 0px;
//     padding-left: 34px;
//     position: relative;
//     @media screen and (max-width: 768px) {
//       font-size: 14px;
//       line-height: 18px;
//     }
//     &::after {
//       position: absolute;
//       content: '';
//       height: 22px;
//       width: 22px;
//       left: 0px;
//       top: 2px;
//       border-radius: 50%;
//       background-color: #60b947;
//       @media screen and (max-width: 768px) {
//         height: 16px;
//         width: 16px;
//       }
//     }
//   }
//   span {
//     font-family: Roboto;
//     font-size: 18px;
//     letter-spacing: 0;
//     line-height: 21px;
//     text-align: right;
//     cursor: pointer;
//     @media screen and (max-width: 768px) {
//       font-size: 12px;
//       line-height: 16px;
//     }
//     &:hover {
//       color: #60b947;
//       font-weight: 600;
//     }
//   }
// `;
