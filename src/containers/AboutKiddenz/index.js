/* eslint-disable react/prop-types */
// eslint-disable-next-line react/no-unescaped-entities
import React, { useState } from 'react';
// import Slider from 'react-slick';
// import Router from 'next/router';
import Link from 'next/link';
import moment from 'moment';
import styled from 'styled-components';
// import { useIntrestingArticlesHook, useBookmarkHook } from 'shared/hooks';
import { useIntrestingArticlesHook, useBookmarkHook } from 'shared/hooks';
// import { Mainheading } from 'components/MainHeading/styled';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import MainWrapper from '../../components/MainWrapper';
import Button from '../../components/Button';
import background from '../../images/header bg (1).svg';
import Flex from '../../components/common/flex';
import MainHeading from '../../components/MainHeading';
import { Mainheading } from '../../components/MainHeading/styled';

// import KiddenOffer from '../../components/KiddenOffer';
import InterestCard from '../../components/InterestCard';
import character from '../../images/character-png-min.png';
// import mainImage from '../../images/video.svg';
import videoImage from '../../images/1.png';
// import infoImage from '../../images/Detailed-Info.png';
// import leadImage from '../../images/recieve leads.svg';
// import ourImage from '../../images/profileImage.jpg';
import ensureImage1 from '../../images/Kiddenz_illustration_quality-information.svg';
import ensureImage2 from '../../images/Kiddenz_illustration_transperancy.svg';
import ensureImage3 from '../../images/Kiddenz_illustration_trust.svg';
import ensureImage4 from '../../images/Kiddenz_illustration_convenience.svg';
import parentMessage from '../../images/Kiddenz_message for parents.png';

const Aboutkiddenz = props => {
  const { articles } = useIntrestingArticlesHook(props);

  const [modalStatus, setModalStatus] = useState(false);
  const [modalType, setModalType] = useState('email');

  const {
    errorToast,
    authentication: { categoryColors = {}, categoriesConfig = {} } = {},
  } = props;
  const onBookmarkAddError = ({ message }) =>
    errorToast(message || 'Something went wrong.');

  const { bookmarkedItems = [], addBookmark, removeBookmark } = useBookmarkHook(
    props,
    { onBookmarkAddError },
  );
  const { authentication: { isLoggedIn } = {} } = props;

  return (
    <>
      <Header
        {...props}
        type="articles"
        searchBar={false}
        isModalActive={modalStatus}
        activeModalType={modalType}
        setActiveCallback={setModalStatus}
      />
      <MainWrapper background="#fff" paddingTop="0px">
        <TopSection>
          <Image>
            <img src={background} alt="" />
          </Image>
          <Title>About Us</Title>
        </TopSection>
        <MainSection>
          <img className="character" src={character} alt="" />
          <p>
            Kiddenz is a India's most trusted Preschool and Daycare discovery
            platform, designed to help parents find ideal preschool, daycare,
            playschool, creche, for their children. We use advanced algorithms
            and insightful data to suggest ideal preschool & daycare in a
            desired location or en-route to work.
            <br />
            Additionally Kiddenz offers best phonics, maths and extracurricular
            programs for holistic development of children
          </p>
          <div className="main-image">
            <img src={videoImage} alt="" />
          </div>
          <Flex justifyBetween flexWidth="100%" wrap>
            <p>
              Parents use Kiddenz to discover best suited preschools and
              daycares. Kiddenz offers the most comprehensive information about
              every preschool and daycare. The information covers details on
              facilities, fees, teachers, curriculum, pictures, location &
              contact information. Our learning programs have been designed by
              experts and used by thousands of children.
            </p>
            <p>
              At Kiddenz we live by and believe that trust between parents and
              experts is paramount, we address real world problems for new
              parents through our community of childcare providers, experts
              teachers and technology.
            </p>
          </Flex>
          <Flex column alignCenter flexMargin="80px 0px 0px">
            <MainHeading
              text="We Ensure"
              fontSize="32px"
              margin="0px 0px 110px 0px"
            />
            <WeEnsure>
              <img src={ensureImage1} alt="" />
              <Flex column alignStart flex1 flexMargin="0px 0px 0px 56px">
                <h3>Quality</h3>
                <span>
                  We strive to provide unbiased, accurate information about
                  preschools & daycares. We offer all relevant information that
                  is essential to select a preschool or daycare. (Virtual tour,
                  pictures, curriculum, teachers and more)
                </span>
              </Flex>
            </WeEnsure>
            <WeEnsure direction="row-reverse" margin="0px 0px 60px auto">
              <img src={ensureImage2} alt="" />
              <Flex column alignEnd flex1 flexMargin="0px 56px 0px 0px">
                <h3>Care</h3>
                <span style={{ textAlign: 'right' }}>
                  All our preschool partners provide utmost care to every child.
                </span>
              </Flex>
            </WeEnsure>
            <WeEnsure>
              <img src={ensureImage3} alt="" />
              <Flex column alignStart flex1 flexMargin="0px 0px 0px 56px">
                <h3>Trust</h3>
                <span>
                  We honor all our commitments and ensure availability of
                  complete and up-to-date information on all our partner
                  preschools & programs
                </span>
              </Flex>
            </WeEnsure>
            <WeEnsure direction="row-reverse" margin="0px 0px 60px auto">
              <img src={ensureImage4} alt="" />
              <Flex column alignEnd flex1 flexMargin="0px 56px 0px 0px">
                <h3>Convenience</h3>
                <span style={{ textAlign: 'right' }}>
                  Find the ideal childcare & learning programs from the comfort
                  of your home
                </span>
              </Flex>
            </WeEnsure>
          </Flex>
          {/* NOTE: Temporarily commented out */}
          {/* <Flex column alignCenter>
            <MainHeading
              text="Our Team"
              fontSize="32px"
              margin="0px 0px 110px 0px"
            />
            <OurTeam>
              <div className="ourImage">
                <img src={ourImage} alt="" />
              </div>
              <Flex column alignStart flex1 flexMargin="0px 0px 0px 56px">
                <h2>Rahul</h2>
                <span>
                  Kumar Rahul Singh possesses 7 years of work experience in a
                  renowned organisation. He now is giving rise to his ideas and
                  passion to promote Kiddenz with all of his heart. His
                  exceptional problem solving and Team building skills at the
                  workplace have proved to be a gem to the company. Rahul is
                  Adaptable, creative and efficient at what he does. Indifferent
                  from his affection for children, he deems that travelling,
                  playing cricket and listening to music are a few things he
                  loves in his free time.
                </span>
              </Flex>
            </OurTeam>
            <OurTeam>
              <div className="ourImage">
                <img src={ourImage} alt="" />
              </div>
              <Flex column alignStart flex1 flexMargin="0px 0px 0px 56px">
                <h2>Suvarna</h2>
                <span>
                  Suvarna is a multipotentialite who believes that commitment,
                  resilience and determination can work wonders. Besides being
                  humble and curious, she possesses great interpersonal skills.
                  She has been a co-author in a few books and reckons that
                  writing, travelling and blogging are her favourite hobbies.
                </span>
              </Flex>
            </OurTeam>
            <OurTeam>
              <div className="ourImage">
                <img src={ourImage} alt="" />
              </div>
              <Flex column alignStart flex1 flexMargin="0px 0px 0px 56px">
                <h2>Sharanya</h2>
                <span>
                  Sharanya believes mindfulness in the workplace is the key to
                  success. Her background in Marketing, Corporate Strategy and
                  Operations informs her apprehensiveness and competitive
                  approach. She is fueled by her passion for understanding the
                  nuances of customer relationship management and building long
                  term value for the organization. Her hunger for knowledge and
                  determination to turn information into action has contributed
                  to getting through every endeavour. Apart from having a
                  flavour in creativity- a tenet she lives out through her
                  interests in singing, writing and reading, she is also a great
                  listener and can carry a team like a breeze.
                </span>
              </Flex>
            </OurTeam>
          </Flex>
       */}
        </MainSection>
        <ParentsMessage>
          <div className="parentMessage">
            <img src={parentMessage} alt="" />
          </div>
          <div className="parentMessage-content">
            <h2>Message for Parents</h2>
            <Flex column flexWidth="35%" className="parentMessage-right">
              <h3>
                “Kiddenz is your trusted, one stop destination for all early
                education and child care needs.”
              </h3>
              <Flex>
                <Button
                  text="Register Now"
                  type="outline"
                  onClick={() => {
                    setModalType('signup');
                    setModalStatus(true);
                  }}
                />
              </Flex>
            </Flex>
          </div>
        </ParentsMessage>
      </MainWrapper>
      <InterestingRead>
        <MainHeading
          text="Interesting Reads"
          fontSize="32px"
          margin="0px 0px 8px 0px"
        />
        <SubInfo>Read what our experts recommend on parenting.</SubInfo>
        <Flex className="interestContainer">
          {articles.map(article => (
            <Link
              href={`/blog/${article.post_name}-${article.post_id}?category=${
                article.category
              }&name=${article.post_title}`}
            >
              <InterestCard
                isLoggedIn={isLoggedIn}
                categoriesConfig={categoriesConfig}
                categoryColors={categoryColors}
                interestImage={article.post_image}
                heading={article.post_title}
                categoryList={article.category}
                list={article.post_tag}
                dateNum={moment(article.post_modified).format('MMMM DD,YYYY')}
                viewNum={article.viewCount}
                likesNum={article.likeCount}
                isBookmarked={bookmarkedItems.includes(article.post_id)}
                onBookmarkClick={() => {
                  if (bookmarkedItems.includes(article.post_id))
                    removeBookmark(article.post_id);
                  else addBookmark(article.post_id);
                }}
              />
            </Link>
          ))}
        </Flex>
      </InterestingRead>
      <Footer bgColor="#fff" {...props} />
    </>
  );
};

export default Aboutkiddenz;

const TopSection = styled.div`
  width: 100%;
  height: 377px;
  position: relative;
  background-color: #fff;
  @media (max-width: 900px) {
    height: 200px;
  }
  @media (max-width: 500px) {
    height: 115px;
    margin-bottom: 50px;
  }
`;
const MainSection = styled.div`
  position: relative;
  max-width: 1180px;
  padding: 0px 20px;
  margin: 0px auto;
  background-color: #fff;

  .character {
    position: absolute;
    right: 100px;
    top: -200px;
    @media (max-width: 900px) {
      transform: scale(0.8);
      right: 44px;
      top: -219px;
    }
    @media (max-width: 500px) {
      transform: scale(0.3);
      right: -44px;
      top: -300px;
    }
  }
  p {
    width: 48%;
    color: #30333b;
    font-family: Quicksand;
    font-size: 22px;
    letter-spacing: 0;
    line-height: 36px;
    @media (max-width: 900px) {
      font-size: 18px;
      line-height: 28px;
    }
    @media (max-width: 500px) {
      width: 100%;

      font-size: 14px;
      line-height: 24px;
    }
  }
  .main-image {
    width: 100%;
    height: 529px;
    margin: 30px 0px;

    img {
      width: 100%;
      height: 100%;
      object-fit: cover;
    }
    @media screen and (max-width: 500px) {
      height: auto;
    }
  }
  ${Mainheading} {
    @media screen and (max-width: 500px) {
      width: 100%;
    }
  }
`;

const Image = styled.div`
  position: absolute;
  left: 0px;
  right: 0px;
  bottom: 0px;
  top: 0px;
  z-index: 0;
  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
`;

const Title = styled.h1`
  color: #ffffff;
  font-family: Quicksand;
  font-size: 60px;
  letter-spacing: 0;
  line-height: 75px;
  font-weight: 600;
  position: relative;
  max-width: 1180px;
  padding: 0px 20px;
  margin: 120px auto 0px;
  @media (max-width: 900px) {
    margin-top: 36px;
    font-size: 46px;
  }
  @media (max-width: 900px) {
    margin-top: 10px;
    font-size: 22px;
  }
`;

const InterestingRead = styled.div`
  width: 100%;
  max-width: 1180px;
  display: flex;
  //   border-top: 1px solid #e6e8ec;
  padding: 60px 20px 120px;
  margin: 0px auto;
  flex-direction: column;
  .interestContainer {
    @media screen and (max-width: 765px) {
      flex-direction: column;
      align-items: center;
    }
  }
  @media screen and (max-width: 765px) {
    padding: 30px 20px 30px;
  }
`;

const SubInfo = styled.div`
  color: #30333b;
  font-family: 'Quicksand', sans-serif;
  font-size: 16px;
  font-weight: 500;
  line-height: 20px;
  margin-bottom: 30px;
`;

const WeEnsure = styled.div`
  display: flex;
  align-items: center;
  flex-direction: ${props => props.direction || 'row'};
  margin: ${props => props.margin || '0px auto 60px 0px;'};
  width: 60%;
  @media screen and (max-width: 900px) {
    width: 80%;
    margin-bottom: 30px;
  }
  @media screen and (max-width: 500px) {
    flex-direction: column;
    width: 100%;
    margin: 0px 0px 60px;
    align-items: flex-start;
  }
  ${Flex} {
    @media screen and (max-width: 500px) {
      margin: 0px;
      align-items: flex-start;
    }
  }
  img {
    @media screen and (max-width: 900px) {
      transform: scale(0.9);
    }
    @media screen and (max-width: 500px) {
      transform: scale(1);
      margin-bottom: 40px;
    }
  }
  h3 {
    color: #30333b;
    font-family: Quicksand;
    font-size: 22px;
    letter-spacing: 0;
    line-height: 28px;
    margin: 0px 0px 20px;
    @media screen and (max-width: 900px) {
      font-size: 20px;
    }
    @media screen and (max-width: 500px) {
      font-size: 16px;
      line-height: 24px;
      margin: 0px 0px 16px;
    }
  }
  span {
    color: #30333b;
    font-family: Quicksand;
    font-size: 18px;
    letter-spacing: 0;
    line-height: 24px;
    @media screen and (max-width: 900px) {
      font-size: 16px;
    }
    @media screen and (max-width: 500px) {
      text-align: left !important;
      font-size: 14px;
      line-height: 24px;
    }
  }
`;

// const OurTeam = styled.div`
//   display: flex;
//   margin: ${props => props.margin || '0px auto 80px 0px;'};
//   width: 100%;
//   @media screen and (max-width: 500px) {
//     background: #f2eff5;
//     border-radius: 5px;
//     margin-bottom: 24px;
//     padding: 20px;
//     flex-direction: column;
//   }
//   ${Flex} {
//     @media screen and (max-width: 500px) {
//       margin: 0px;
//     }
//   }
//   .ourImage {
//     height: 225px;
//     width: 225px;
//     border-radius: 50%;
//     overflow: hidden;
//     img {
//       width: 100%;
//       height: 100%;
//     }
//     @media screen and (max-width: 500px) {
//       height: 146px;
//       width: 146px;
//       margin-bottom: 24px;
//     }
//   }
//   h2 {
//     color: #30333b;
//     font-family: Quicksand;
//     font-size: 22px;
//     letter-spacing: 0;
//     line-height: 28px;
//     margin: 0px 0px 20px;
//     @media screen and (max-width: 500px) {
//       font-size: 14px;
//       line-height: 24px;
//       margin-bottom: 10px;
//     }
//   }
//   span {
//     color: #30333b;
//     font-family: Quicksand;
//     font-size: 18px;
//     letter-spacing: 0;
//     line-height: 24px;
//     @media screen and (max-width: 500px) {
//       font-size: 12px;
//       line-height: 24px;
//     }
//   }
// `;

const ParentsMessage = styled.div`
  position: relative;
  padding: 253px 0px 333px 0px;
  @media (max-width: 500px) {
    padding: 100px 0px 170px;
  }
  @media (max-width: 375px) {
    padding: 90px 0px 177px;
  }
  .parentMessage {
    position: absolute;
    top: 0px;
    right: 0px;
    left: 0px;
    height: 100%;

    img {
      width: 100%;
      height: 100%;
      object-fit: cover;
    }

    &-content {
      display: flex;
      //   justify-content: space-between;
      height: auto;
      width: 100%;
      max-width: 1180px;
      align-items: baseline;
      padding: 0px 20px 0px;
      margin: 0px auto;
      .outline {
        &:hover {
          background-color: transparent;
          border-color: #fff;
          color: #fff;
        }
      }
      @media (max-width: 500px) {
        flex-direction: column;
        width: 100%;
        align-items: center;
      }
    }
    &-right {
      @media (max-width: 500px) {
        width: 95%;
        align-items: center;
      }
    }
  }
  h2 {
    width: fit-content;
    color: #ffffff;
    font-family: Quicksand;
    font-size: 32px;
    letter-spacing: 0;
    line-height: 40px;
    z-index: 1;
    margin-right: 20%;
    @media (max-width: 500px) {
      margin-right: 0px;
      font-size: 20px;
    }
  }
  h3 {
    color: #ffffff;
    font-family: Quicksand;
    font-size: 26px;
    font-weight: 500;
    letter-spacing: 0;
    line-height: 35px;
    margin-bottom: 30px;
    margin-top: 30px;
    @media (max-width: 500px) {
      font-size: 14px;
      line-height: 26px;
      margin-top: 10px;
      text-align: center;
    }
  }
  .outline {
    padding: 0px 30px;
    @media (max-width: 500px) {
      height: 36px;
      font-size: 12px;
    }
  }
`;
