/* eslint-disable */
// eslint-disable-next-line react/no-unescaped-entities
import React, { useState } from 'react';
// import Slider from 'react-slick';
// import Router from 'next/router';
import Link from 'next/link';
import moment from 'moment';
import styled from 'styled-components';
// import { useIntrestingArticlesHook, useBookmarkHook } from 'shared/hooks';
import { useIntrestingArticlesHook, useBookmarkHook } from 'shared/hooks';
// import { Mainheading } from 'components/MainHeading/styled';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import MainWrapper from '../../components/MainWrapper';
import Button from '../../components/Button';
import background from '../../images/header bg (1).svg';
import Flex from '../../components/common/flex';
import MainHeading from '../../components/MainHeading';
import { Mainheading } from '../../components/MainHeading/styled';

// import KiddenOffer from '../../components/KiddenOffer';
import InterestCard from '../../components/InterestCard';
import character from '../../images/Character1.svg';
// import mainImage from '../../images/video.svg';
import videoImage from '../../images/1.png';
// import infoImage from '../../images/Detailed-Info.png';
// import leadImage from '../../images/recieve leads.svg';
// import ourImage from '../../images/profileImage.jpg';
import ensureImage1 from '../../images/Kiddenz_illustration_quality-information.svg';
import ensureImage2 from '../../images/Kiddenz_illustration_transperancy.svg';
import ensureImage3 from '../../images/Kiddenz_illustration_trust.svg';
import ensureImage4 from '../../images/Kiddenz_illustration_convenience.svg';
import parentMessage from '../../images/Kiddenz_message for parents.png';

const Terms = props => {
  const { articles } = useIntrestingArticlesHook(props);

  const [modalStatus, setModalStatus] = useState(false);
  const [modalType, setModalType] = useState('email');

  const {
    errorToast,
    authentication: { categoryColors = {}, categoriesConfig = {} } = {},
  } = props;
  const onBookmarkAddError = ({ message }) =>
    errorToast(message || 'Something went wrong.');

  const { bookmarkedItems = [], addBookmark, removeBookmark } = useBookmarkHook(
    props,
    { onBookmarkAddError },
  );
  const { authentication: { isLoggedIn } = {} } = props;

  return (
    <>
      <Header
        {...props}
        type="articles"
        searchBar={false}
        isModalActive={modalStatus}
        activeModalType={modalType}
        setActiveCallback={setModalStatus}
      />
      <MainWrapper background="#fff" paddingTop="0px">
        <TopSection>
          <Image>
            <img src={background} alt="" />
          </Image>
          <Title>Terms of Use</Title>
        </TopSection>
        <MainSection>
          <img className="character" src={character} alt="" />
          <p className="left">
            {/* Terms of Use
            <br /> */}
            Effective date: November 07, 2020
            <br />
            Welcome to&nbsp;Kiddenz. Please read on to learn the rules and
            restrictions that govern your use of our website(s), products,
            services and applications (the &ldquo;Services&rdquo;). If you have
            any questions, comments, or concerns regarding these terms or the
            Services, please contact us at{' '}
            <Link href="mailto:hello@kiddenz.com" target="_blank">
              hello@kiddenz.com
            </Link>
            .<br />
            These Terms of Use (the &ldquo;Terms&rdquo;) are a binding contract
            between you and&nbsp;Vollgas Eduventures Private
            Limited.&nbsp;(&ldquo;Kiddenz,&rdquo; &ldquo;we&rdquo; and
            &ldquo;us&rdquo;). You must agree to and accept all of the Terms, or
            you don&rsquo;t have the right to use the Services. Your using the
            Services in any way means that you agree to all of these Terms, and
            these Terms will remain in effect while you use the Services. These
            Terms include the provisions in this document, as well as those in
            the&nbsp;{' '}
            <Link href="/privacy-policy" target="_blank">
              Privacy Policy
            </Link>{' '}
            and&nbsp;
            <Link href="/copyright" target="_blank">
              Copyright Dispute Policy
            </Link>
            .
          </p>
          <br />
          <p class="p3">
            The Children&rsquo;s Online Privacy Protection Act
            (&ldquo;COPPA&rdquo;) requires that online service providers obtain
            parental consent before they knowingly collect personally
            identifiable information online from children who are under 13. We
            do not knowingly collect or solicit personally identifiable
            information from children under 13; if you are a child under 13,
            please do not attempt to register for the Services or send any
            personal information about yourself to us. For clarity, children
            under 13 may not register as users of the Services, and may only
            interact with or receive the benefit of our Services through a
            parent and with parental supervision.
          </p>
          <p class="p3">
            We do, however, collect information about children from their
            parents or guardians who are registered users of the Services and we
            treat such information as &ldquo;Personal Information&rdquo; under
            our <span class="s1">Privacy Policy</span> We ask that our users not
            provide information about a child without first obtaining consent
            from that child&rsquo;s parent or guardian (&ldquo;Parental
            Consent&rdquo;). If we learn we have collected personal information
            directly from a child under 13 or about a child without Parental
            Consent, we will delete that information as quickly as possible. If
            you believe that a child under 13 may have provided us personal
            information or that information about a child was provided without
            Parental Consent, please contact us at{' '}
            <Link href="mailto:hello@kiddenz.com" target="_blank">
              hello@kiddenz.com
            </Link>
            .
          </p>
          <p class="p3">
            We do not require that children disclose, or that parents or
            guardians disclose about their children, any personal information
            about children in order to receive the benefits of our Services.
          </p>
          <p class="p4">California Resident Rights</p>
          <p class="p3">
            If you are a California resident, you have the rights set forth in
            this section. Please see the &quot;Exercising Your Rights&quot;
            section below for instructions regarding how to exercise these
            rights. Please note that we may process Personal Data of our
            customers&apos; end users or employees in connection with our
            provision of certain services to our customers. If we are processing
            your Personal Data as a service provider, you should contact the
            entity that collected your Personal Data in the first instance to
            address your rights with respect to such data.
          </p>
          <p class="p3">
            If there are any conflicts between this section and any other
            provision of this Privacy Policy and you are a California resident,
            the portion that is more protective of Personal Data shall control
            to the extent of such conflict. If you have any questions about this
            section or whether any of the following rights apply to you, please
            contact us at{' '}
            <Link href="mailto:hello@kiddenz.com" target="_blank">
              hello@kiddenz.com
            </Link>
            <span>&nbsp;</span>
          </p>

          <table cellpadding="0" cellspacing="0" class="t1" border="1">
            <tbody>
              <tr>
                <td class="td1" valign="middle">
                  <p class="p6">
                    <strong>Category of Personal Data</strong>
                  </p>
                </td>
                <td class="td1" valign="middle">
                  <p class="p6">
                    <strong>Examples of Personal Data We Collect</strong>
                  </p>
                </td>
                <td class="td1" valign="middle">
                  <p class="p6">
                    <strong>
                      Categories of Third Parties With Whom We Share this
                      Personal Data
                    </strong>
                  </p>
                </td>
              </tr>
              <tr>
                <td class="td1" valign="middle">
                  <p class="p6">Profile or Contact Data</p>
                </td>
                <td class="td1" valign="middle">
                  <p class="p6">
                    First and last name
                    <br />
                    Email
                    <br />
                    Photo
                    <br />
                    Type of Child Care Desired
                    <br />
                    Zip code location desired for child care
                    <br />
                    Phone number
                  </p>
                </td>
                <td class="td1" valign="middle">
                  <p class="p6">
                    Service Providers
                    <br />
                    Analytics Partners
                  </p>
                </td>
              </tr>
              <tr>
                <td class="td1" valign="middle">
                  <p class="p6">Payment Data</p>
                </td>
                <td class="td1" valign="middle">
                  <p class="p6">
                    Payment card type
                    <br />
                    Credit or debit card number (full number)
                    <br />
                    Last 4 digits of payment card
                    <br />
                    Billing email
                  </p>
                </td>
                <td class="td1" valign="middle">
                  <p class="p6">
                    Service Providers (specifically our payment processing
                    partner, currently ICICI, Inc.)
                  </p>
                </td>
              </tr>
              <tr>
                <td class="td1" valign="middle">
                  <p class="p6">Online Identifiers</p>
                </td>
                <td class="td1" valign="middle">
                  <p class="p6">Unique identifiers, such as passwords</p>
                </td>
                <td class="td1" valign="middle">
                  <p class="p7">
                    <br />
                  </p>
                </td>
              </tr>
              <tr>
                <td class="td1" valign="middle">
                  <p class="p6">Device/IP Data</p>
                </td>
                <td class="td1" valign="middle">
                  <p class="p6">
                    IP address
                    <br />
                    Type of device/operating system/browser used to access the
                    Services
                  </p>
                </td>
                <td class="td1" valign="middle">
                  <p class="p6">
                    Service Providers
                    <br />
                    Advertising Partners
                    <br />
                    Analytics Partners
                    <br />
                    Business Partners
                    <br />
                    Parties You Authorize, Access or Authenticate
                  </p>
                </td>
              </tr>
              <tr>
                <td class="td1" valign="middle">
                  <p class="p6">Web Analytics</p>
                </td>
                <td class="td1" valign="middle">
                  <p class="p6">
                    Browsing or search history
                    <br />
                    Web page interactions (including with ads)
                    <br />
                    Referring web page/source through which you accessed the
                    Services
                    <br />
                    Non-identifiable request IDs
                    <br />
                    Statistics associated with the interaction between device or
                    browser and the Services
                  </p>
                </td>
                <td class="td1" valign="middle">
                  <p class="p6">
                    Service Providers
                    <br />
                    Advertising Partners
                    <br />
                    Analytics Partners
                    <br />
                    Parties You Authorize, Access or Authenticate
                  </p>
                </td>
              </tr>
              <tr>
                <td class="td1" valign="middle">
                  <p class="p6">Social Network Data</p>
                </td>
                <td class="td1" valign="middle">
                  <p class="p6">
                    Email
                    <br />
                    User name on social network
                    <br />
                    Info from user&rsquo;s social media profile
                  </p>
                </td>
                <td class="td1" valign="middle">
                  <p class="p6">
                    Service Providers
                    <br />
                    Analytics Providers
                  </p>
                </td>
              </tr>
              <tr>
                <td class="td1" valign="middle">
                  <p class="p6">Consumer Demographic Data</p>
                </td>
                <td class="td1" valign="middle">
                  <p class="p6">Zip code</p>
                </td>
                <td class="td1" valign="middle">
                  <p class="p6">
                    Service Providers
                    <br />
                    Advertising Partners (in aggregated form only)
                    <br />
                    Analytics Partners
                  </p>
                </td>
              </tr>
              <tr>
                <td class="td1" valign="middle">
                  <p class="p6">Geolocation Data</p>
                </td>
                <td class="td1" valign="middle">
                  <p class="p6">IP-address-based location information</p>
                </td>
                <td class="td1" valign="middle">
                  <p class="p6">
                    Service Providers
                    <br />
                    Advertising Partners
                    <br />
                    Analytics Partners
                    <br />
                    Parties You Authorize, Access or Authenticate
                  </p>
                </td>
              </tr>
            </tbody>
          </table>
          <p class="p9">
            <strong>Categories of Sources of Personal Data</strong>
          </p>
          <p class="p3">
            We collect Personal Data about you from the following categories of
            sources:
          </p>
          <ul class="ul1">
            <li class="li10">
              <strong>You</strong>
              <ul class="ul1">
                <li class="li10">
                  When you provide such information directly to us.
                  <ul class="ul1">
                    <li class="li10">
                      When you create an account or use our interactive tools
                      and Services.
                    </li>
                    <li class="li10">
                      When you voluntarily provide information in free-form text
                      boxes through the Services or through responses to surveys
                      or questionnaires.
                    </li>
                    <li class="li10">
                      When you send us an email or otherwise contact us.
                    </li>
                  </ul>
                </li>
                <li class="li10">
                  When you use the Services and such information is collected
                  automatically.
                  <ul class="ul1">
                    <li class="li10">
                      Through Cookies (defined in the &quot;Tracking Tools,
                      Advertising and Opt-Out&quot; section below).
                    </li>
                    <li class="li10">
                      If you download our mobile application or use a
                      location-enabled browser, we may receive information about
                      your location and mobile device, as applicable.
                    </li>
                  </ul>
                </li>
                <li class="li10">
                  If you download and install certain applications and software
                  we make available, we may receive and collect information
                  transmitted from your computing device for the purpose of
                  providing you the relevant Services, such as information
                  regarding when you are logged on and available to receive
                  updates or alert notices.
                </li>
              </ul>
            </li>
            <li class="li10">
              <strong>Public Records</strong>
              <ul class="ul1">
                <li class="li10">
                  From the government or other sources (Providers only)
                </li>
              </ul>
            </li>
            <li class="li10">
              <strong>Third Parties</strong>
              <ul class="ul1">
                <li class="li10">
                  Vendors
                  <ul class="ul1">
                    <li class="li10">
                      We may use vendors to obtain information to generate leads
                      and create user profiles.
                    </li>
                  </ul>
                </li>
                <li class="li10">
                  Social Networks
                  <ul class="ul1">
                    <li class="li10">
                      If you provide your social network account credentials to
                      us or otherwise sign in to the Services through a
                      third-party site or service, some content and/or
                      information in those accounts may be transmitted into your
                      account with us.
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
          </ul>
          <p class="p9">
            <strong>
              Our Commercial or Business Purposes for Collecting Personal Data
            </strong>
          </p>
          <ul class="ul1">
            <li class="li10">
              <strong>Providing, Customizing and Improving the Services</strong>
              <ul class="ul1">
                <li class="li10">
                  Creating and managing your account or other user profiles.
                </li>
                <li class="li10">
                  Processing orders or other transactions; billing.
                </li>
                <li class="li10">
                  Providing you with the products, services or information you
                  request.
                </li>
                <li class="li10">
                  Meeting or fulfilling the reason you provided the information
                  to us.
                </li>
                <li class="li10">
                  Providing support and assistance for the Services.
                </li>
                <li class="li10">
                  Improving the Services, including testing, research, internal
                  analytics and product development.
                </li>
                <li class="li10">
                  Personalizing the Services, website content and communications
                  based on your preferences.
                </li>
                <li class="li10">
                  Doing fraud protection, security and debugging.
                </li>
                <li class="li10">
                  Carrying out other business purposes stated when collecting
                  your Personal Data or as otherwise set forth in applicable
                  data privacy laws, such as the California Consumer Privacy Act
                  (the &quot;CCPA&quot;).
                </li>
              </ul>
            </li>
            <li class="li10">
              <strong>Marketing the Services</strong>
              <ul class="ul1">
                <li class="li10">Marketing and selling the Services.</li>
                <li class="li10">
                  Showing you advertisements, including interest-based or online
                  behavioral advertising.
                </li>
              </ul>
            </li>
            <li class="li10">
              <strong>Corresponding with You</strong>
              <ul class="ul1">
                <li class="li10">
                  Responding to correspondence that we receive from you,
                  contacting you when necessary or requested, and sending you
                  information about Kiddenz or the Services.
                </li>
                <li class="li10">
                  Sending emails and other communications according to your
                  preferences or that display content that we think will
                  interest you.
                </li>
              </ul>
            </li>
            <li class="li10">
              <strong>
                Meeting Legal Requirements and Enforcing Legal Terms
              </strong>
              <ul class="ul1">
                <li class="li10">
                  Fulfilling our legal obligations under applicable law,
                  regulation, court order or other legal process, such as
                  preventing, detecting and investigating security incidents and
                  potentially illegal or prohibited activities.
                </li>
                <li class="li10">
                  Protecting the rights, property or safety of you, Kiddenz or
                  another party.
                </li>
                <li class="li10">Enforcing any agreements with you.</li>
                <li class="li10">
                  Responding to claims that any posting or other content
                  violates third-party rights.
                </li>
                <li class="li10">Resolving disputes.</li>
              </ul>
            </li>
          </ul>
          <p class="p3">
            We will not collect additional categories of Personal Data or use
            the Personal Data we collected for materially different, unrelated
            or incompatible purposes without providing you notice.
          </p>
          <p class="p5">
            <br />
          </p>
          <p class="p11">
            <strong>
              Will these Terms ever change?
              <span>&nbsp;</span>
            </strong>
          </p>
          <p class="p1">
            We are constantly trying to improve our Services, so these Terms may
            need to change along with the Services. We reserve the right to
            change the Terms at any time, but if we do, we will bring it to your
            attention by placing a notice on the&nbsp;Kiddenz.com&nbsp;website,
            by sending you an email, and/or by some other means. If you
            don&rsquo;t agree with the new Terms, you are free to reject them;
            unfortunately, that means you will no longer be able to use the
            Services.
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            If you use the Services in any way after a change to the Terms is
            effective, that means you agree to all of the changes. Except for
            changes by us as described here, no other amendment or modification
            of these Terms will be effective unless in writing and signed by
            both you and us. What about my privacy? Kiddenz takes the privacy of
            its users very seriously. For the current Kiddenz Privacy Policy,
            please click&nbsp;here.
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            WHAT ARE THE BASICS OF USING KIDDENZ?
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            Kiddenz is a network that connects preschool and day-care providers
            (&ldquo;Providers&rdquo;) and those looking to engage childcare
            services (&ldquo;Parents&rdquo;). A person who completes
            Kiddenz&rsquo;s account registration process, including Providers
            and Parents, is a &ldquo;Member.&rdquo; When we use the word
            &ldquo;you&rdquo; in these Terms, it refers to any Member, while if
            we use one of those specific terms, it only applies to that category
            of user.
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            Providers arrange and organize preschool or day-care facilities.
            Providers may list their facility as available via the Services
            (&ldquo;Listings&rdquo;). Parents may select preschool/daycare in
            Listings for their child(ren). You may view Listings as an
            unregistered visitor to the Services; however, if you wish to
            connect with providers in or create a Listing, you must first
            register to create a Kiddenz Account (defined below).
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            While these Terms do place certain restrictions on our
            Members&rsquo; use of our Services, you understand that Kiddenz is
            not a party to the relationship formed between a Provider and a
            Parent. A Parent may use Kiddenz to facilitate its receipt of
            professional services from a Provider (&ldquo;Provider
            Services&rdquo;), but Kiddenz can&rsquo;t and won&rsquo;t be
            responsible for making sure those Provider Services are actually
            provided or are up to a certain standard of quality, or for
            mediating disputes between Providers and/or Parents. Kiddenz
            similarly can&rsquo;t and won&rsquo;t be responsible for ensuring
            that information (including credentials) a Parent or Provider
            provides about himself or herself is accurate or up-to-date. We
            don&rsquo;t control the actions of any Parent or Provider, and
            Providers aren&rsquo;t our employees.
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            A Parent and a Provider may choose to enter into a separate
            agreement outside of Kiddenz regarding the Provider Services, to
            which we are not a party and aren&rsquo;t responsible for enforcing
            (an &ldquo;Outside Agreement&rdquo;). We encourage you to use
            Outside Agreements, but will not review their terms.&nbsp;If you
            choose to enter into an Outside Agreement, it must not, in any
            manner, conflict with any of the terms herein.
          </p>
          <p class="p1">
            <span>&nbsp;</span>
            IF YOU CHOOSE TO CREATE A LISTING ON KIDDENZ, YOU UNDERSTAND AND
            AGREE THAT YOUR RELATIONSHIP WITH KIDDENZ IS LIMITED TO BEING A
            MEMBER AND NOT AN EMPLOYEE, AGENT, JOINT VENTURER OR PARTNER OF
            KIDDENZ FOR ANY REASON, AND YOU ACT EXCLUSIVELY ON YOUR OWN BEHALF
            AND FOR YOUR OWN BENEFIT, AND NOT ON BEHALF OF OR FOR THE BENEFIT OF
            KIDDENZ. KIDDENZ DOES NOT CONTROL, AND HAS NO RIGHT TO CONTROL, YOUR
            LISTING, YOUR OFFLINE ACTIVITIES ASSOCIATED WITH YOUR LISTING, OR
            ANY OTHER MATTERS RELATED TO ANY LISTING, THAT YOU PROVIDE. AS A
            MEMBER YOU AGREE NOT TO DO ANYTHING TO CREATE A FALSE IMPRESSION
            THAT YOU ARE ENDORSED BY, PARTNERING WITH, OR ACTING ON BEHALF OF OR
            FOR THE BENEFIT OF KIDDENZ, INCLUDING BY INAPPROPRIATELY USING ANY
            KIDDENZ INTELLECTUAL PROPERTY.
            <span>&nbsp;</span>
          </p>
          <p class="p1">HOW CAN I CREATE A KIDDENZ ACCOUNT?</p>
          <p class="p1">
            <span>&nbsp;</span>
            In order to access certain features of the Services and to select a
            provider for your child(ren) or create a Listing, you must register
            to create an account (&ldquo;Kiddenz Account&rdquo;), select a
            password and username (&ldquo;User ID&rdquo;) and become a Member.
            You promise to provide us with accurate, complete, and updated
            registration information about yourself. Kiddenz reserves the right
            to terminate your Kiddenz Account and your access to the Services if
            you create more than one Kiddenz Account, or if any information
            provided during the registration process or thereafter proves to be
            inaccurate, fraudulent, not current, incomplete, or otherwise in
            violation of these Terms of Service. You may not select as your User
            ID a name that you don&rsquo;t have the right to use, or another
            person&rsquo;s name with the intent to impersonate that person. You
            may not transfer your account to anyone else without our prior
            written permission.
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            You represent and warrant that you are an individual of legal age to
            form a binding contract. You understand and agree that you must
            provide accurate information. Kiddenz may, for transparency or fraud
            prevention or detection purposes (directly or through third parties)
            ask you to provide a form of government identification (for example,
            your driver&rsquo;s license or passport), your date of birth, and
            other information, or undertake additional checks and processes
            designed to help verify or check the identities or backgrounds of
            Members and/or screen Member information against third party
            databases or other sources. However, we do not make any
            representations about, confirm, or endorse any Member or the
            Member&rsquo;s purported identity or background.
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            You will only use the Services for your own internal, personal use,
            and not on behalf of or for the benefit of any third party, and only
            in a manner that complies with all laws that apply to you. If your
            use of the Services is prohibited by applicable laws, then you
            aren&rsquo;t authorized to use the Services. We can&rsquo;t and
            won&rsquo;t be responsible for your using the Services in a way that
            breaks the law.
          </p>
          <p class="p1">
            <span>&nbsp;</span>
            You will not share your account or password with anyone, and you
            must protect the security of your account and your password.
            You&rsquo;re responsible for any activity associated with your
            Kiddenz Account.
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            HOW CAN I CREATE A LISTING?
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            Only Providers may create Listings. To create a Listing, a Provider
            will be asked a variety of questions about the preschool or daycare
            facilities to be listed, including, but not limited to, the
            location, capacity, size, features, availability, and pricing and
            related rules. All Preschool and day-cares must have valid physical
            addresses. The placement or ranking of Listings in search results
            may depend on a variety of factors, including, Parent and Provider
            preferences, ratings, filters and reviews . A Provider may choose to
            include certain requirements which Parents must meet in order to be
            eligible to request an appointment of the Provider&rsquo;s facility.
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            Providers alone are responsible for any and all Listings they post.
            Accordingly, each Provider represents and warrants that any Listing
            such Provider posts, any Admission thereof, in a Provider&rsquo;s
            Listing (i) will not breach any agreements the Provider has entered
            into with any third parties, such as homeowners association,
            condominium, or other third party agreements, (ii) will be in
            compliance with all applicable laws (such as zoning laws), tax
            requirements, intellectual property laws, and rules and regulations
            that may apply to any preschool/day-care included in a Listing the
            Provider posts (including having all required permits, licenses and
            registrations), and (iii) not conflict with the rights of third
            parties. Please note that Kiddenz assumes no responsibility for a
            Provider&rsquo;s compliance with any agreements with or duties to
            third parties, applicable laws, rules and regulations. Kiddenz
            reserves the right, at any time and without prior notice, to remove
            or disable access to any Listing for any reason, including Listings
            that Kiddenz, in its sole discretion, considers to be objectionable
            for any reason, in violation of these Terms, or otherwise harmful to
            the Services.
            <span>&nbsp;</span>
          </p>
          <p class="p1">HOW DO Admissions WORK?</p>
          <p class="p1">
            <span>&nbsp;</span>
            Providers shall routinely offer tours for Parents of their
            preschool/day-care premises (&ldquo;Tours&rdquo;). A Parent must
            attend a Tour before enrolling his or her child(ren) in an
            preschool/day-care. During his or her attendance at a Tour, a Parent
            may express interest in enrolling his or her child(ren).
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            The applicable Provider will have to confirm the admission to
            Kiddenz. The applicable Provider, not Kiddenz, is solely responsible
            for honoring any confirmed Admission(s) through the Services. The
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            Parent agrees to personally inspect the premises of the
            preschool/daycare via a Tour prior to the Parent&rsquo;s
            child(ren)&rsquo;s admission and acknowledges that Kiddenz is not
            responsible for the nature and/or condition of such ECE Class.
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            YOUR USE OF THE SERVICES IS SUBJECT TO THE FOLLOWING ADDITIONAL
            RESTRICTIONS:
          </p>
          <p class="p1">
            You understand and agree that you will not contribute any Content or
            User Submission (each of those terms is defined below) or otherwise
            use the Services or interact with the Services in a manner that:
            Violates any law or regulation, including, without limitation, any
            applicable export control laws, zoning regulations or tax
            regulations; Infringes or violates the intellectual property rights
            or any other rights of anyone else (including Kiddenz); Is harmful,
            fraudulent, deceptive, threatening, harassing, defamatory, obscene,
            or otherwise objectionable; Jeopardizes the security of your Kiddenz
            Account or anyone else&rsquo;s (such as allowing someone else to log
            in to the Services as you);
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            Attempts, in any manner, to obtain the password, account, or other
            security information from any other user;
          </p>
          <p class="p1">
            <span>&nbsp;</span>
            Violates the security of any computer network, or cracks any
            passwords or security encryption codes; Runs Maillist, Listserv, any
            form of auto-responder or &ldquo;spam&rdquo; on the Services, or any
            processes that run or are activated while you are not logged into
            the Services, or that otherwise interfere with the proper working of
            the Services (including by placing an unreasonable load on the
            Services&rsquo; infrastructure);
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            &ldquo;Crawls,&rdquo; &ldquo;scrapes,&rdquo; or
            &ldquo;spiders&rdquo; any page, data, or portion of or relating to
            the Services or Content (through use of manual or automated means);
            Copies or stores any significant portion of the Content; Decompiles,
            reverse engineers, or otherwise attempts to obtain the source code
            or underlying ideas or information of or relating to the Services.
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            Falsely implies Kiddenz endorsement, partnership or otherwise
            misleads others as to your affiliation with Kiddenz. A violation of
            any of the foregoing is grounds for termination of your right to use
            or access the Services.
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            You also agree to comply with the&nbsp;Kiddenz Rules&nbsp;(which are
            part of these Terms), which include further details on what you can
            and cannot do while using the Service. A violation of any of the
            foregoing is grounds for termination of your right to use or access
            the Services.
            <span>&nbsp;</span>
          </p>
          <p class="p11">
            <strong>
              What are my rights in Kiddenz?
              <span>&nbsp;</span>
            </strong>
          </p>
          <p class="p1">
            The materials displayed or performed or available on or through the
            Services, including, but not limited to, text, graphics, data,
            articles, photos, images, illustrations, User Submissions, and so
            forth (all of the foregoing, the &ldquo;Content&rdquo;) are
            protected by copyright and/or other intellectual property laws. You
            promise to abide by all copyright notices, trademark rules,
            information, and restrictions contained in any Content you access
            through the Services, and you won&rsquo;t use, copy, reproduce,
            modify, translate, publish, broadcast, transmit, distribute,
            perform, upload, display, license, sell or otherwise exploit for any
            purpose any Content not owned by you, (i) without the prior consent
            of the owner of that Content or (ii) in a way that violates someone
            else&rsquo;s (including Kiddenz&rsquo;s) rights.
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            You understand that Kiddenz owns the Services. You won&rsquo;t
            modify, publish, transmit, participate in the transfer or sale of,
            reproduce (except as expressly provided in this Section), create
            derivative works based on, or otherwise exploit any of the Services.
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            The Services may allow you to copy or download certain Content;
            please remember that just because this functionality exists,
            doesn&rsquo;t mean that all the restrictions above don&rsquo;t apply
            &ndash; they do!
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            <strong>
              Do I have to grant any licenses to Kiddenz or to other users?
              <span>&nbsp;</span>
            </strong>
          </p>
          <p class="p1">
            Anything you post, upload, share, store, or otherwise provide
            through the Services is your &ldquo;User Submission.&rdquo; Some
            User Submissions are viewable by other users. In order to display
            your User Submissions on the Services, and to allow other users to
            enjoy them (where applicable), you grant us certain rights in those
            User Submissions. Please note that all of the following licenses are
            subject to our&nbsp;Privacy Policy&nbsp;to the extent they relate to
            User Submissions that are also your personally-identifiable
            information.
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            For all User Submissions, you hereby grant Kiddenz a license to
            translate, edit, modify (for technical purposes, for example making
            sure your content is viewable on an iPhone as well as a computer)
            and reproduce and otherwise act with respect to such User
            Submissions, in each case to enable us to operate the Services, as
            described in more detail below. This is a license only &ndash; your
            ownership in User Submissions is not affected.
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            If you share a User Submission publicly on the Services and/or in a
            manner that more than just you or certain specified users can view,
            or if you provide us (in a direct email or otherwise) with any
            feedback, suggestions, improvements, enhancements, and/or feature
            requests relating to the Services (a &ldquo;Public User
            Submission&rdquo;), then you grant Kiddenz the licenses above, as
            well as a license to display, perform, and distribute your Public
            User Submission for the purpose of making that Public User
            Submission accessible to all Kiddenz users and providing the
            Services necessary to do so, as well as all other rights necessary
            to use and exercise all rights in that Public User Submission in
            connection with the Services and/or otherwise in connection with
            Kiddenz&rsquo;s business for any purpose. Also, you grant all other
            users of the Services a license to access that Public User
            Submission, and to use and exercise all rights in it, as permitted
            by the functionality of the Services.
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            You agree that the licenses you grant are royalty-free, perpetual,
            sublicensable, irrevocable, and worldwide, and you understand and
            agree that we may continue displaying your Public User Submissions
            after you delete your Kiddenz account, that it may not be possible
            to completely delete that content from Kiddenz&rsquo;s records, and
            that your User Submissions may remain viewable elsewhere to the
            extent that they were copied or stored by other users.
          </p>
          <p class="p1">
            Finally, you understand and agree that Kiddenz, in performing the
            required technical steps to provide the Services to our users
            (including you), may need to make changes to your User Submissions
            to conform and adapt those User Submissions to the technical
            requirements of connection networks, devices, services, or media,
            and the foregoing licenses include the rights to do so.
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            <strong>
              What if I see something on the Services that infringes my
              copyright?
            </strong>
          </p>
          <p class="p1">
            <span>&nbsp;</span>
            We respect others&rsquo; intellectual property rights, and we
            reserve the right to delete or disable Content alleged to be
            infringing, and to terminate the accounts of repeat alleged
            infringers.
          </p>
          <p class="p1">
            <span>&nbsp;</span>
            <strong>
              Who is responsible for what I see and do on the Services?
            </strong>
          </p>
          <p class="p1">
            <span>&nbsp;</span>
            Any information or content publicly posted or privately transmitted
            through the Services is the sole responsibility of the person from
            whom such content originated, and you access all such information
            and content at your own risk, and we aren&rsquo;t liable for any
            errors or omissions in that information or content or for any
            damages or loss you might suffer in connection with it. We cannot
            control and have no duty to take any action regarding how you may
            interpret and use the Content or what actions you may take as a
            result of having been exposed to the Content, and you hereby release
            us from all liability for you having acquired or not acquired
            Content through the Services. We can&rsquo;t guarantee the identity
            of any users with whom you interact in using the Services and are
            not responsible for which users gain access to the Services.
          </p>
          <p class="p1">
            <span>&nbsp;</span>
            You are responsible for all Content you contribute, in any manner,
            to the Services, and you represent and warrant you have all rights
            necessary to do so, in the manner in which you contribute it. You
            will keep all your registration information accurate and current.
            You are responsible for all your activity in connection with the
            Services.
          </p>
          <p class="p1">
            <span>&nbsp;</span>
            The Services may contain links or connections to third party
            websites or services that are not owned or controlled by Kiddenz.
            When you access third party websites or use third party services,
            you accept that there are risks in doing so, and that Kiddenz is not
            responsible for such risks. We encourage you to be aware when you
            leave the Services and to read the terms and conditions and privacy
            policy of each third party website or service that you visit or
            utilize.
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            Kiddenz has no control over, and assumes no responsibility for, the
            content, accuracy, privacy policies, or practices of or opinions
            expressed in any third party websites or by any third party that you
            interact with through the Services. In addition, Kiddenz will not
            and cannot monitor, verify, censor or edit the content of any third
            party site or service. By using the Services, you release and hold
            us harmless from any and all liability arising from your use of any
            third party website or service.
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            Your interactions with organizations and/or individuals found on or
            through the Services, including payment and delivery of goods or
            services, and any other terms, conditions, warranties or
            representations associated with such dealings, are solely between
            you and such organizations and/or individuals. You should make
            whatever investigation you feel necessary or appropriate before
            proceeding with any online or offline transaction with any of these
            third parties. You agree that Kiddenz shall not be responsible or
            liable for any loss or damage of any sort incurred as the result of
            any such dealings.
          </p>
          <p class="p1">
            <span>&nbsp;</span>
            If there is a dispute between participants on this site, or between
            users and any third party, you agree that Kiddenz is under no
            obligation to become involved. In the event that you have a dispute
            with one or more other users, you release Kiddenz, its officers,
            employees, agents, and successors from claims, demands, and damages
            of every kind or nature, known or unknown, suspected or unsuspected,
            disclosed or undisclosed, arising out of or in any way related to
            such disputes and/or our Services.
          </p>
          <p class="p1">
            <span>&nbsp;</span>
            <strong>
              Will Kiddenz ever change the Services?
              <span>&nbsp;</span>
            </strong>
          </p>
          <p class="p1">
            We&rsquo;re always trying to improve the Services, so they may
            change over time. We may suspend or discontinue any part of the
            Services, or we may introduce new features or impose limits on
            certain features or restrict access to parts or all of the Services.
            We&rsquo;ll try to give you notice when we make a material change to
            the Services that would adversely affect you, but this isn&rsquo;t
            always practical. Similarly, we reserve the right to remove any
            Content from the Services at any time, for any reason (including,
            but not limited to, if someone alleges you contributed that Content
            in violation of these Terms), in our sole discretion, and without
            notice.
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            <strong>
              Does Kiddenz cost anything?
              <span>&nbsp;</span>
            </strong>
          </p>
          <p class="p1">
            Though many of our Services are currently free, we reserve the right
            to charge for certain or all Services in the future. You shall pay
            all applicable fees, as described on the Kiddenz website in
            connection with the Services selected by you. Kiddenz reserves the
            right to change its price list and to institute new charges at any
            time, upon notice to you, which may be sent by email or posted on
            the website. Your use of the Services following such notification
            constitutes your acceptance of any new or increased charges.
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            Premium Services.&nbsp;If you choose to sign up for our premium
            services (the &ldquo;Premium Services&rdquo;), you will be charged a
            subscription fee based on the length of the subscription you select
            (the &ldquo;Subscription Term&rdquo;). The subscription fee for the
            Premium Services (&ldquo;Subscription Fee&rdquo;) will be charged to
            you in advance as further described below. For details on the
            features and fees for the Premium Services, see&nbsp;our help
            center. All Premium Services Fees are non-refundable.
          </p>
          <p class="p1">
            <span>&nbsp;</span>
            Auto-Renewal for Premium Services.&nbsp;Your Admission in the
            Premium Services will be automatically renewed at the end of each
            Subscription Term and your Payment Method (defined below) will be
            charged in advance for the next Subscription Term. If you wish to
            cancel auto-renewal of your Premium Services subscription for the
            following Subscription Term, you must cancel by&nbsp;following the
            instructions found here&nbsp;prior to the end of your then-current
            Subscription Term. Kiddenz may change the Subscription Fee upon
            notice to you, but such change will only take effect after your
            then-current Subscription Term has ended. If you do not wish to pay
            the new Subscription Fee, your only remedy shall be to cancel your
            Subscription for the following Subscription Term, prior to the
            expiration of your then-current Subscription Term.
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            Billing.&nbsp;We use a third-party payment processor (the
            &quot;Payment Processor&quot;) to bill you through a payment account
            linked to your account on the Services (your &quot;Billing
            Account&quot;) for use of the Premium Services. The processing of
            payments will be subject to the terms, conditions and privacy
            policies of the Payment Processor in addition to these Terms. We are
            not responsible for error by the Payment Processor. By choosing to
            use Premium Services, you agree to pay us, through the Payment
            Processor, all charges at the prices then in effect for any use of
            such Premium Services in accordance with the applicable payment
            terms, and you authorize us, through the Payment Processor, to
            charge your chosen payment provider (for example, a credit or debit
            card you provide) (your &quot;Payment Method&quot;). You agree to
            make payment using that selected Payment Method. We reserve the
            right to correct any errors or mistakes that the Payment Processor
            makes even if it has already requested or received payment.
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            Payment Method.&nbsp;The terms of your payment will be based on your
            Payment Method and may be determined by agreements between you and
            the financial institution, credit card issuer or other provider of
            your chosen Payment Method. If we, through the Payment Processor, do
            not receive payment from you, you agree to pay all amounts due on
            your Billing Account upon demand.
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            Reaffirmation of Authorization.&nbsp;Your non-termination or
            continued use of a Premium Service reaffirms that we are authorized
            to charge your Payment Method for the applicable Subscription Fees
            as described in these Terms. We may submit those charges for payment
            and you will be responsible for such charges. This does not waive
            our right to seek payment directly from you.
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            Current Information Required.&nbsp;You must provide current,
            complete and accurate information for your billing account. You must
            promptly update all information to keep your billing account
            current, complete and accurate (such as a change in billing address,
            credit card number, or credit card expiration date), and you must
            promptly notify us or our Payment Processor if your Payment Method
            is cancelled (e.g. for loss or theft) of if you become aware of a
            potential breach of security, such as the unauthorized disclosure or
            use of your username or password. Changes to such information can be
            made in your account settings. If you fail to provide any of the
            foregoing information, you agree that we may continue charging you
            for any use of Premium Services under your billing account unless
            you have cancelled your subscription for Premium Services as set
            forth above.
          </p>
          <p class="p1">
            <span>&nbsp;</span>
            Promotional Discounts and Free Trials.&nbsp;Kiddenz may offer
            discounted access to certain Premium Services from time to time.
            Unless otherwise noted, the promotional Subscription Fee pricing is
            available only for the period expressly identified in the applicable
            promotion, after which regular Subscription Fee pricing will apply.
            Any free trial or other promotion that provides access to the
            Premium Services must be used within the specified time of the
            trial. You must stop using the Premium Services before the end of
            the trial period in order to avoid being charged Subscription Fees
            for continued usage of such Premium Services.
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            <strong>
              What if I want to stop using Kiddenz?
              <span>&nbsp;</span>
            </strong>
          </p>
          <p class="p1">
            You&rsquo;re free to do that at any time, by contacting us at
            <Link href="mailto:hello@kiddenz.com" target="_blank">
              hello@kiddenz.com
            </Link>
            ; please refer to our&nbsp;Privacy Policy, as well as the licenses
            above, to understand how we treat information you provide to us
            after you have stopped using our Services.
          </p>
          <p class="p1">
            <span>&nbsp;</span>
            Kiddenz is also free to terminate (or suspend access to) your use of
            the Services or your account, for any reason in our discretion,
            including your breach of these Terms. Kiddenz has the sole right to
            decide whether you are in violation of any of the restrictions set
            forth in these Terms.
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            Account termination may result in destruction of any Content
            associated with your account, so keep that in mind before you decide
            to terminate your account. If you have deleted your account by
            mistake, contact us immediately at{' '}
            <Link href="mailto:hello@kiddenz.com" target="_blank">
              hello@kiddenz.com
            </Link>
            &ndash; we will try to help, but unfortunately, we can&rsquo;t
            promise that we can recover or restore anything.
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            Provisions that, by their nature, should survive termination of
            these Terms shall survive termination. By way of example, all of the
            following will survive termination: any obligation you have to pay
            us or indemnify us, any limitations on our liability, any terms
            regarding ownership or intellectual property rights, and terms
            regarding disputes between us.
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            <strong>
              What else do I need to know?
              <span>&nbsp;</span>
            </strong>
          </p>
          <p class="p1">
            Warranty Disclaimer.&nbsp;Neither Kiddenz nor its licensors or
            suppliers makes any representations or warranties concerning any
            content contained in or accessed through the Services, and we will
            not be responsible or liable for the accuracy, copyright compliance,
            legality, or decency of material contained in or accessed through
            the Services. We (and our licensors and suppliers) make no
            representations or warranties regarding suggestions or
            recommendations of services or products offered or purchased through
            the Services. Products and services purchased or offered (whether or
            not following such recommendations and suggestions) through the
            Services are provided &ldquo;AS IS&rdquo; and without any warranty
            of any kind from Kiddenz or others (unless, with respect to such
            others only, provided expressly and unambiguously in writing by a
            designated third party for a specific product). THE SERVICES AND
            CONTENT ARE PROVIDED BY KIDDENZ (AND ITS LICENSORS AND SUPPLIERS) ON
            AN &ldquo;AS-IS&rdquo; BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER
            EXPRESS OR IMPLIED, INCLUDING, WITHOUT LIMITATION, IMPLIED
            WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE,
            NON-INFRINGEMENT, OR THAT USE OF THE SERVICES WILL BE UNINTERRUPTED
            OR ERROR-FREE. SOME STATES DO NOT ALLOW LIMITATIONS ON HOW LONG AN
            IMPLIED WARRANTY LASTS, SO THE ABOVE LIMITATIONS MAY NOT APPLY TO
            YOU.
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            Limitation of Liability.&nbsp;TO THE FULLEST EXTENT ALLOWED BY
            APPLICABLE LAW, UNDER NO CIRCUMSTANCES AND UNDER NO LEGAL THEORY
            (INCLUDING, WITHOUT LIMITATION, TORT, CONTRACT, STRICT LIABILITY, OR
            OTHERWISE) SHALL KIDDENZ (OR ITS LICENSORS OR SUPPLIERS) BE LIABLE
            TO YOU OR TO ANY OTHER PERSON FOR (A) ANY INDIRECT, SPECIAL,
            INCIDENTAL, OR CONSEQUENTIAL DAMAGES OF ANY KIND, INCLUDING DAMAGES
            FOR LOST PROFITS, LOSS OF GOODWILL, WORK STOPPAGE, ACCURACY OF
            RESULTS, OR COMPUTER FAILURE OR MALFUNCTION, OR (B) ANY AMOUNT, IN
            THE AGGREGATE, IN EXCESS OF THE GREATER OF (I) INR 1000 OR (II) THE
            AMOUNTS PAID BY YOU TO KIDDENZ IN CONNECTION WITH THE SERVICES IN
            THE TWELVE (12) MONTH PERIOD PRECEDING THIS APPLICABLE CLAIM, OR
            (III) ANY MATTER BEYOND OUR REASONABLE CONTROL. SOME STATES DO NOT
            ALLOW THE EXCLUSION OR LIMITATION OF CERTAIN DAMAGES, SO THE ABOVE
            LIMITATION AND EXCLUSIONS MAY NOT APPLY TO YOU.
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            Indemnity.&nbsp;To the fullest extent allowed by applicable law, You
            agree to indemnify and hold Kiddenz, its affiliates, officers,
            agents, employees, and partners harmless from and against any and
            all claims, liabilities, damages (actual and consequential), losses
            and expenses (including attorneys&rsquo; fees) arising from or in
            any way related to any third party claims relating to (a) your use
            of the Services (including any actions taken by a third party using
            your account), and (b) your violation of these Terms. In the event
            of such a claim, suit, or action (&ldquo;Claim&rdquo;), we will
            attempt to provide notice of the Claim to the contact information we
            have for your account (provided that failure to deliver such notice
            shall not eliminate or reduce your indemnification obligations
            hereunder).
          </p>
          <p class="p1">
            <span>&nbsp;</span>
            Assignment.&nbsp;You may not assign, delegate or transfer these
            Terms or your rights or obligations hereunder, or your Services
            account, in any way (by operation of law or otherwise) without
            Kiddenz&rsquo;s prior written consent. We may transfer, assign, or
            delegate these Terms and our rights and obligations without consent.
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            Choice of Law; Arbitration.&nbsp;These Terms shall be interpreted
            under the laws of the Republic of India. For any suit or legal
            proceedings arising out of this term the courts at Bengaluru alone
            will have jurisdiction to entertain and try the same.&nbsp;Any
            arbitration under these Terms will take place on an individual
            basis: class arbitrations and class actions are not permitted. YOU
            UNDERSTAND AND AGREE THAT BY ENTERING INTO THESE TERMS, YOU AND
            KIDDENZ ARE EACH WAIVING THE RIGHT TO TRIAL BY JURY OR TO
            PARTICIPATE IN A CLASS ACTION.
            <span>&nbsp;</span>
          </p>
          <p class="p1">
            Miscellaneous. You will be responsible for paying, withholding,
            filing, and reporting all taxes, duties, and other governmental
            assessments associated with your activity in connection with the
            Services, provided that the Kiddenz may, in its sole discretion, do
            any of the foregoing on your behalf or for itself as it sees fit.
            The failure of either you or us to exercise, in any way, any right
            herein shall not be deemed a waiver of any further rights hereunder.
            If any provision of these Terms is found to be unenforceable or
            invalid, that provision will be limited or eliminated, to the
            minimum extent necessary, so that these Terms shall otherwise remain
            in full force and effect and enforceable. You and Kiddenz agree that
            these Terms are the complete and exclusive statement of the mutual
            understanding between you and Kiddenz, and that it supersedes and
            cancels all previous written and oral agreements, communications and
            other understandings relating to the subject matter of these Terms.
            You hereby acknowledge and agree that you are not an employee,
            agent, partner, or joint venture of Kiddenz, and you do not have any
            authority of any kind to bind Kiddenz in any respect whatsoever.
            Except as expressly set forth in the section above regarding the
            Apple Application, you and Kiddenz agree there are no third party
            beneficiaries intended under these Terms.
          </p>
        </MainSection>
      </MainWrapper>
      <Footer bgColor="#fff" {...props} />
    </>
  );
};

export default Terms;

const TopSection = styled.div`
  width: 100%;
  height: 377px;
  position: relative;
  background-color: #fff;
  @media (max-width: 900px) {
    height: 200px;
  }
  @media (max-width: 500px) {
    height: 115px;
    margin-bottom: 50px;
  }
`;
const MainSection = styled.div`
  position: relative;
  max-width: 1180px;
  padding: 0px 20px;
  margin: 0px auto;
  background-color: #fff;
  margin-bottom: 50px;
  .character {
    position: absolute;
    right: 100px;
    top: -200px;
    @media (max-width: 900px) {
      transform: scale(0.8);
      right: 44px;
      top: -219px;
    }
    @media (max-width: 500px) {
      transform: scale(0.3);
      right: -44px;
      top: -300px;
    }
    @media (max-width: 345px) {
      right: -77px;
    }
  }
  p,
  ul li {
    color: #30333b;
    font-family: Quicksand;
    font-size: 22px;
    letter-spacing: 0;
    line-height: 36px;
    @media (max-width: 900px) {
      font-size: 18px;
      line-height: 28px;
    }
    @media (max-width: 500px) {
      width: 100%;

      font-size: 14px;
      line-height: 24px;
    }
  }
  p.left {
    width: 48%;
    @media (max-width: 500px) {
      width: 100%;
    }
  }
  .main-image {
    width: 100%;
    height: 529px;
    margin: 30px 0px;

    img {
      width: 100%;
      height: 100%;
      object-fit: cover;
    }
    @media screen and (max-width: 500px) {
      height: auto;
    }
  }
  ${Mainheading} {
    @media screen and (max-width: 500px) {
      width: 100%;
    }
  }
  table {
    td {
      padding: 10px;
    }
  }
  ul {
    @media screen and (max-width: 768px) {
      padding: 0 0 0 17px;
    }
  }
`;

const Image = styled.div`
  position: absolute;
  left: 0px;
  right: 0px;
  bottom: 0px;
  top: 0px;
  z-index: 0;
  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
`;

const Title = styled.h1`
  color: #ffffff;
  font-family: Quicksand;
  font-size: 60px;
  letter-spacing: 0;
  line-height: 75px;
  font-weight: 600;
  position: relative;
  max-width: 1180px;
  padding: 0px 20px;
  margin: 120px auto 0px;
  @media (max-width: 900px) {
    margin-top: 36px;
    font-size: 46px;
  }
  @media (max-width: 900px) {
    margin-top: 10px;
    font-size: 22px;
  }
`;

const InterestingRead = styled.div`
  width: 100%;
  max-width: 1180px;
  display: flex;
  //   border-top: 1px solid #e6e8ec;
  padding: 60px 20px 120px;
  margin: 0px auto;
  flex-direction: column;
  .interestContainer {
    @media screen and (max-width: 765px) {
      flex-direction: column;
      align-items: center;
    }
  }
  @media screen and (max-width: 765px) {
    padding: 30px 20px 30px;
  }
`;

const SubInfo = styled.div`
  color: #30333b;
  font-family: 'Quicksand', sans-serif;
  font-size: 16px;
  font-weight: 500;
  line-height: 20px;
  margin-bottom: 30px;
`;

const WeEnsure = styled.div`
  display: flex;
  align-items: center;
  flex-direction: ${props => props.direction || 'row'};
  margin: ${props => props.margin || '0px auto 60px 0px;'};
  width: 60%;
  @media screen and (max-width: 900px) {
    width: 80%;
    margin-bottom: 30px;
  }
  @media screen and (max-width: 500px) {
    flex-direction: column;
    width: 100%;
    margin: 0px 0px 60px;
    align-items: flex-start;
  }
  ${Flex} {
    @media screen and (max-width: 500px) {
      margin: 0px;
      align-items: flex-start;
    }
  }
  img {
    @media screen and (max-width: 900px) {
      transform: scale(0.9);
    }
    @media screen and (max-width: 500px) {
      transform: scale(1);
      margin-bottom: 40px;
    }
  }
  h3 {
    color: #30333b;
    font-family: Quicksand;
    font-size: 22px;
    letter-spacing: 0;
    line-height: 28px;
    margin: 0px 0px 20px;
    @media screen and (max-width: 900px) {
      font-size: 20px;
    }
    @media screen and (max-width: 500px) {
      font-size: 16px;
      line-height: 24px;
      margin: 0px 0px 16px;
    }
  }
  span {
    color: #30333b;
    font-family: Quicksand;
    font-size: 18px;
    letter-spacing: 0;
    line-height: 24px;
    @media screen and (max-width: 900px) {
      font-size: 16px;
    }
    @media screen and (max-width: 500px) {
      text-align: left !important;
      font-size: 14px;
      line-height: 24px;
    }
  }
`;

// const OurTeam = styled.div`
//   display: flex;
//   margin: ${props => props.margin || '0px auto 80px 0px;'};
//   width: 100%;
//   @media screen and (max-width: 500px) {
//     background: #f2eff5;
//     border-radius: 5px;
//     margin-bottom: 24px;
//     padding: 20px;
//     flex-direction: column;
//   }
//   ${Flex} {
//     @media screen and (max-width: 500px) {
//       margin: 0px;
//     }
//   }
//   .ourImage {
//     height: 225px;
//     width: 225px;
//     border-radius: 50%;
//     overflow: hidden;
//     img {
//       width: 100%;
//       height: 100%;
//     }
//     @media screen and (max-width: 500px) {
//       height: 146px;
//       width: 146px;
//       margin-bottom: 24px;
//     }
//   }
//   h2 {
//     color: #30333b;
//     font-family: Quicksand;
//     font-size: 22px;
//     letter-spacing: 0;
//     line-height: 28px;
//     margin: 0px 0px 20px;
//     @media screen and (max-width: 500px) {
//       font-size: 14px;
//       line-height: 24px;
//       margin-bottom: 10px;
//     }
//   }
//   span {
//     color: #30333b;
//     font-family: Quicksand;
//     font-size: 18px;
//     letter-spacing: 0;
//     line-height: 24px;
//     @media screen and (max-width: 500px) {
//       font-size: 12px;
//       line-height: 24px;
//     }
//   }
// `;

const ParentsMessage = styled.div`
  position: relative;
  padding: 253px 0px;
  @media (max-width: 500px) {
    padding: 100px 0px 170px;
  }
  @media (max-width: 375px) {
    padding: 90px 0px 177px;
  }
  .parentMessage {
    position: absolute;
    top: 0px;
    right: 0px;
    left: 0px;
    height: 100%;

    img {
      width: 100%;
      height: 100%;
      object-fit: cover;
    }

    &-content {
      display: flex;
      //   justify-content: space-between;
      height: auto;
      width: 100%;
      max-width: 1180px;
      align-items: baseline;
      padding: 0px 20px 0px;
      margin: 0px auto;
      .outline {
        &:hover {
          background-color: transparent;
          border-color: #fff;
          color: #fff;
        }
      }
      @media (max-width: 500px) {
        flex-direction: column;
        width: 100%;
        align-items: center;
      }
    }
    &-right {
      @media (max-width: 500px) {
        width: 95%;
        align-items: center;
      }
    }
  }
  h2 {
    width: fit-content;
    color: #ffffff;
    font-family: Quicksand;
    font-size: 32px;
    letter-spacing: 0;
    line-height: 40px;
    z-index: 1;
    margin-right: 20%;
    @media (max-width: 500px) {
      margin-right: 0px;
      font-size: 20px;
    }
  }
  h3 {
    color: #ffffff;
    font-family: Quicksand;
    font-size: 26px;
    font-weight: 500;
    letter-spacing: 0;
    line-height: 35px;
    margin-bottom: 30px;
    margin-top: 30px;
    @media (max-width: 500px) {
      font-size: 14px;
      line-height: 26px;
      margin-top: 10px;
      text-align: center;
    }
  }
  .outline {
    padding: 0px 30px;
    @media (max-width: 500px) {
      height: 36px;
      font-size: 12px;
    }
  }
`;
