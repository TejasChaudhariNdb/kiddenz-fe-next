/* eslint-disable react/prop-types */
// eslint-disable-next-line react/no-unescaped-entities
import React, { useState } from 'react';
// import Slider from 'react-slick';
// import Router from 'next/router';
import Link from 'next/link';
import moment from 'moment';
import styled from 'styled-components';
// import { useIntrestingArticlesHook, useBookmarkHook } from 'shared/hooks';
import { useIntrestingArticlesHook, useBookmarkHook } from 'shared/hooks';
// import { Mainheading } from 'components/MainHeading/styled';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import MainWrapper from '../../components/MainWrapper';
import Button from '../../components/Button';
import background from '../../images/header bg (1).svg';
import Flex from '../../components/common/flex';
import MainHeading from '../../components/MainHeading';
import { Mainheading } from '../../components/MainHeading/styled';

// import KiddenOffer from '../../components/KiddenOffer';
import InterestCard from '../../components/InterestCard';
import character from '../../images/Character1.svg';
// import mainImage from '../../images/video.svg';
import videoImage from '../../images/1.png';
// import infoImage from '../../images/Detailed-Info.png';
// import leadImage from '../../images/recieve leads.svg';
// import ourImage from '../../images/profileImage.jpg';
import ensureImage1 from '../../images/Kiddenz_illustration_quality-information.svg';
import ensureImage2 from '../../images/Kiddenz_illustration_transperancy.svg';
import ensureImage3 from '../../images/Kiddenz_illustration_trust.svg';
import ensureImage4 from '../../images/Kiddenz_illustration_convenience.svg';
import parentMessage from '../../images/Kiddenz_message for parents.png';

const Terms = props => {
  const { articles } = useIntrestingArticlesHook(props);

  const [modalStatus, setModalStatus] = useState(false);
  const [modalType, setModalType] = useState('email');

  const {
    errorToast,
    authentication: { categoryColors = {}, categoriesConfig = {} } = {},
  } = props;
  const onBookmarkAddError = ({ message }) =>
    errorToast(message || 'Something went wrong.');

  const { bookmarkedItems = [], addBookmark, removeBookmark } = useBookmarkHook(
    props,
    { onBookmarkAddError },
  );
  const { authentication: { isLoggedIn } = {} } = props;

  return (
    <>
      <Header
        {...props}
        type="articles"
        searchBar={false}
        isModalActive={modalStatus}
        activeModalType={modalType}
        setActiveCallback={setModalStatus}
      />
      <MainWrapper background="#fff" paddingTop="0px">
        <TopSection>
          <Image>
            <img src={background} alt="" />
          </Image>
          <Title>Privacy Policy</Title>
        </TopSection>
        <MainSection>
          <img className="character" src={character} alt="" />
          <p className="left">
            Privacy Policy <br />
            Effective date: June 30, 2020
            <br />
            We at Kiddenz know you care about how your personal information is
            used and shared, and we take your privacy seriously. Please read the
            following to learn more about our Privacy Policy.&nbsp;By using or
            accessing the Services in any manner, you acknowledge that you
            accept the practices and policies outlined in this Privacy Policy,
            and you hereby consent that we will collect, use, and share your
            information in the following ways.
            <br />
          </p>
          <p>
            Remember that your use of Kiddenz&rsquo;s Services is at all times
            subject to the&nbsp;Terms of Use, which incorporates this Privacy
            Policy. Any terms we use in this Policy without defining them have
            the definitions given to them in the&nbsp;Terms of Use .
          </p>
          <p>
            <strong>What does this Privacy Policy cover? </strong>
          </p>
          <p>
            This Privacy Policy covers our treatment of personally identifiable
            information ("Personal Information") that we gather when you are
            accessing or using our Services, but not to the practices of
            companies we don&rsquo;t own or control, or people that we
            don&rsquo;t manage. We gather various types of Personal Information
            from our users, as explained in more detail below, and we use this
            Personal Information internally in connection with our Services,
            including to personalize, provide, and improve our services, to
            allow you to set up a user account and profile, to contact you and
            allow other users to contact you, to fulfill your requests for
            certain products and services, and to analyze how you use the
            Services. In certain cases, we may also share some Personal
            Information with third parties, but only as described below.
          </p>
          <p>
            <strong>Will Kiddenz ever change this Privacy Policy? </strong>
          </p>
          <p>
            We&rsquo;re constantly trying to improve our Services, so we may
            need to change this Privacy Policy from time to time as well, but we
            will alert you to changes by placing a notice on the Kiddenz.com
            website, by sending you an email, and/or by some other means. Please
            note that if you&rsquo;ve opted not to receive legal notice emails
            from us (or you haven&rsquo;t provided us with your email address),
            those legal notices will still govern your use of the Services, and
            you are still responsible for reading and understanding them. If you
            use the Services after any changes to the Privacy Policy have been
            posted, that means you agree to all of the changes.
          </p>
          <p>
            <strong>What Information does Kiddenz Collect?</strong>
          </p>
          <p>Information You Provide to Us</p>
          <p>
            We receive and store any information you knowingly provide to us.
            For example, through the registration process and/or through your
            account settings, we may collect Personal Information such as your
            name, email address, children&rsquo;s birthday(s) and gender,
            biography, home city, and your log-in credentials for Facebook or
            other third party sites. If you provide your third-party account
            credentials to us or otherwise sign in to the Services through a
            third party site or service, you understand some content and/or
            information in those accounts (&ldquo;Third Party Account
            Information&rdquo;) may be transmitted into your account with us,
            and that Third Party Account Information transmitted to our Services
            is covered by this Privacy Policy. Certain information may be
            required to register with us or to take advantage of some of our
            features.
          </p>
          <p>
            We may communicate with you if you&rsquo;ve provided us the means to
            do so. For example, if you&rsquo;ve given us your email address, we
            may send you marketing emails, or email you about your use of the
            Services. Also, we may receive a confirmation when you open an email
            from us. This confirmation helps us make our communications with you
            more interesting and improve our services. If we do email you, each
            marketing communication we send you will contain instructions
            permitting you to &ldquo;opt-out&rdquo; of receiving future
            marketing communications. In addition, if at any time you wish not
            to receive any future marketing communications or you wish to have
            your name deleted from our mailing lists, please contact us at
            hello@kiddenz.com I
          </p>
          <p>nformation Collected Automatically</p>
          <p>
            Whenever you interact with our Services, we automatically receive
            and record information on our server logs from your browser or
            device, which may include your IP address, geolocation data, device
            identification, &ldquo;cookie&rdquo; information, the type of
            browser and/or device you&rsquo;re using to access our Services, and
            the page or feature you requested. &ldquo;Cookies&rdquo; are
            identifiers we transfer to your browser or device that allow us to
            recognize your browser or device and tell us how and when pages and
            features in our Services are visited and by how many people. You may
            be able to change the preferences on your browser or device to
            prevent or limit your device&rsquo;s acceptance of cookies, but this
            may prevent you from taking advantage of some of our features.
          </p>
          <p>
            Our advertising partners may also transmit cookies to your browser
            or device, when you click on ads that appear on the Services. Also,
            if you click on a link to a third party website or service, a third
            party may also transmit cookies to you. Again, this Privacy Policy
            does not cover the use of cookies by any third parties, and we
            aren&rsquo;t responsible for their privacy policies and practices.
            Please be aware that cookies placed by third parties may continue to
            track your activities online even after you have left our Services,
            and those third parties may not honor &ldquo;Do Not Track&rdquo;
            requests you have set using your browser or device. We may use this
            data to customize content for you that we think you might like,
            based on your usage patterns.
          </p>
          <p>
            We may also use it to improve the Services &ndash; for example, this
            data can tell us how often users use a particular feature of the
            Services, and we can use that knowledge to make the Services
            interesting to as many users as possible.
          </p>
          <p>Location Information</p>
          <p>
            The Services provide location-based information, so in order to
            work, the Services need to know your location. You will be asked for
            access to geo-location services through the permission system used
            by your mobile operating system or browser. Whenever you open and
            use/interact with our apps on your mobile device or go to one of our
            Sites, we automatically receive and use the precise location
            information from your mobile device or browser (e.g., latitude and
            longitude) to tailor the Services to your current location (i.e.,
            we&rsquo;ll show you a list of nearby places and stories). This
            information is NOT shared with others.
          </p>
          <p>
            Through cookies we place on your browser or device, we may collect
            information about your online activity after you leave our Services.
            Just like any other usage information we collect, this information
            allows us to improve the Services and customize your online
            experience, and otherwise as described in this Privacy Policy. Your
            browser may offer you a &ldquo;Do Not Track&rdquo; option, which
            allows you to signal to operators of websites and web applications
            and services (including behavioral advertising services) that you do
            not wish such operators to track certain of your online activities
            over time and across different websites. Our Services do not support
            Do Not Track requests at this time, which means that we collect
            information about your online activity both while you are using the
            Services and after you leave our Services.
          </p>
          <p>
            <strong>
              Will Kiddenz Share Any of the Personal Information it Receives?{' '}
            </strong>
          </p>
          <p>
            We do not rent or sell your Personal Information in personally
            identifiable form to anyone, except as expressly provided below. We
            may share your Personal Information with third parties as described
            in this section:
          </p>
          <p>
            Information that&rsquo;s been de-identified.&nbsp;We may de-identify
            your Personal Information so that you are not identified as an
            individual, and provide that information to our partners. We may
            also provide aggregate usage information to our partners (or allow
            partners to collect that information from you), who may use such
            information to understand how often and in what ways people use our
            Services, so that they, too, can provide you with an optimal online
            experience. However, we never disclose aggregate usage or
            de-identified information to a partner (or allow a partner to
            collect such information) in a manner that would identify you as an
            individual person.
          </p>
          <p>
            Analytics:&nbsp;We may use third party analytics services, such as
            Google Analytics, to grow our business, to improve and develop our
            Services, to monitor and analyze use of our Services, to aid our
            technical administration, to increase the functionality and
            user-friendliness of our Services, and to verify that users have the
            authorization needed for us to process their requests. These
            services may collect and retain some information about you. Google
            Analytics collects the IP address assigned to you on the date you
            use the Services, but not your name or other personally identifying
            information. We do not combine the information generated through the
            use of Google Analytics with your Personal Information. Although
            Google Analytics plants a persistent cookie on your web browser to
            identify you as a unique user the next time you use the Services,
            the cookie cannot be used by anyone but Google. Google&rsquo;s
            ability to use and share information collected by Google Analytics
            about your use of the Services is restricted by the Google Analytics
            Terms of Use and the Google Privacy Policy. You may find additional
            information about Google Analytics at
            www.google.com/policies/privacy/partners/. Finally, you can opt out
            of Google Analytics by visiting
            https://tools.google.com/dlpage/gaoptout/.
          </p>
          <p>
            Advertisers:&nbsp;We allow advertisers and/or merchant partners
            (&ldquo;Advertisers&rdquo;) to choose the demographic information of
            users who will see their advertisements and/or promotional offers
            and you agree that we may provide any of the information we have
            collected from you in non-personally identifiable form to an
            Advertiser, in order for that Advertiser to select the appropriate
            audience for those advertisements and/or offers. For example, we
            might use the fact you are located in Bangalore to show you ads or
            offers for Bangalore businesses, but we will not tell such
            businesses who you are. Or, we might allow Advertisers to display
            their ads to users with similar usage patterns to yours, but we will
            not disclose usage information to Advertisers except in aggregate
            form, and not in a manner that would identify you personally. Note
            that if an advertiser asks us to show an ad to a certain audience or
            audience segment and you respond to that ad, the advertiser may
            conclude that you fit the description of the audience they were
            trying to reach.
          </p>
          <p>
            Affiliated Businesses:&nbsp;In certain situations, businesses or
            third party websites we&rsquo;re affiliated with may sell or provide
            products or services to you through or in connection with the
            Services (either alone or jointly with us). You can recognize when
            an affiliated business is associated with such a transaction or
            service, and we will share your Personal Information with that
            affiliated business only to the extent that it is related to such
            transaction or service. We have no control over the policies and
            practices of third party websites or businesses as to privacy or
            anything else, so if you choose to take part in any transaction or
            service relating to an affiliated website or business, please review
            all such business&rsquo; or websites&rsquo; policies.
          </p>
          <p>
            Agents:&nbsp;We employ other companies and people to perform tasks
            on our behalf and need to share your information with them to
            provide products or services to you; for example, we may use a
            payment processing company to receive and process your credit card
            transactions for us. Unless we tell you differently, our agents do
            not have any right to use the Personal Information we share with
            them beyond what is necessary to assist us. Note that an
            &ldquo;agent&rdquo; may also be considered a &ldquo;partner&rdquo;
            in certain circumstances, and would be subject to the terms of
            the&nbsp;&ldquo;Information that&rsquo;s been
            de-identified&rdquo;&nbsp;section in that regard.
          </p>
          <p>
            User Profiles and Submissions:&nbsp;Certain user profile
            information, including your name, location, and any video or image
            content that such user has uploaded to the Services, may be
            displayed to other users to facilitate user interaction within the
            Services or address your request for our services. Please remember
            that any content you upload to your public user profile, along with
            any Personal Information or content that you voluntarily disclose
            online in a manner other users can view (on discussion boards, in
            messages and chat areas, etc.) becomes publicly available, and can
            be collected and used by anyone. Your user name may also be
            displayed to other users if and when you send messages or comments
            or upload images or videos through the Services and other users can
            contact you through messages and comments.
          </p>
          <p>
            Business Transfers:&nbsp;We may choose to buy or sell assets, and
            may share and/or transfer customer information in connection with
            the evaluation of and entry into such transactions. Also, if we (or
            our assets) are acquired, or if we go out of business, enter
            bankruptcy, or go through some other change of control, Personal
            Information could be one of the assets transferred to or acquired by
            a third party.
          </p>
          <p>
            Protection of Company and Others:&nbsp;We reserve the right to
            access, read, preserve, and disclose any information that we
            reasonably believe is necessary to comply with law or court order;
            enforce or apply our&nbsp;Terms of Use&nbsp;and other agreements; or
            protect the rights, property, or safety of Company, our employees,
            our users, or others.
          </p>
          <p>
            <strong>Is Personal Information about me secure? </strong>
          </p>
          <p>
            You must prevent unauthorized access to your account and Personal
            Information by selecting and protecting your password and/or other
            sign-on mechanism appropriately and limiting access to your computer
            or device and browser by signing off after you have finished
            accessing your account. We endeavor to protect the privacy of your
            account and other Personal Information we hold in our records, but
            unfortunately, we cannot guarantee complete security. Unauthorized
            entry or use, hardware or software failure, and other factors, may
            compromise the security of user information at any time.
          </p>
          <p>
            <strong>What Personal Information can I access? </strong>
          </p>
          <p>
            Through your profile, you may access, and, in some cases, edit or
            delete the following information you&rsquo;ve provided to us:
          </p>
          <p>
            <span class="s1">●</span> name
          </p>
          <p>
            <span class="s1">●</span> email address
          </p>
          <p>
            <span class="s1">●</span> children&rsquo;s birthday(s) and gender
          </p>
          <p>
            <span class="s1">●</span> Child information
          </p>
          <p>
            <span class="s1">●</span> home city
          </p>
          <p>
            <span class="s1">●</span> stories, including images and videos you
            have uploaded to the site
          </p>
          <p>
            The information you can view, update, and delete may change as the
            Services change. If you have any questions about viewing or updating
            information we have on file about you, please contact us at
            hello@kiddenz.com.
          </p>
          <p>
            <strong>What choices do I have?</strong>
          </p>
          <p>
            You can always opt not to disclose information to us, but keep in
            mind some information may be needed to register with us or to take
            advantage of some of our features.
          </p>
          <p>
            You may be able to add, update, or delete information as explained
            above. When you update information, however, we may maintain a copy
            of the unrevised information in our records. You may request
            deletion of your account by emailing hello@kiddenz.com. Some
            information may remain in our records after your deletion of such
            information from your account. We may use any aggregated data
            derived from or incorporating your Personal Information after you
            update or delete it, but not in a manner that would identify you
            personally.
          </p>
          <p>
            <strong>What if I have questions about this policy</strong>?
          </p>
          <p>
            If you have any questions or concerns regarding our privacy
            policies, please send us a detailed message to hello@kiddenz.com,
            and we will try to resolve your concerns.
          </p>
        </MainSection>
      </MainWrapper>
      <Footer bgColor="#fff" {...props} />
    </>
  );
};

export default Terms;

const TopSection = styled.div`
  width: 100%;
  height: 377px;
  position: relative;
  background-color: #fff;
  @media (max-width: 900px) {
    height: 200px;
  }
  @media (max-width: 500px) {
    height: 115px;
    margin-bottom: 50px;
  }
`;
const MainSection = styled.div`
  position: relative;
  max-width: 1180px;
  padding: 0px 20px;
  margin: 0px auto;
  background-color: #fff;
  margin-bottom: 50px;
  .character {
    position: absolute;
    right: 100px;
    top: -200px;
    @media (max-width: 900px) {
      transform: scale(0.8);
      right: 44px;
      top: -219px;
    }
    @media (max-width: 500px) {
      transform: scale(0.3);
      right: -44px;
      top: -300px;
    }
    @media (max-width: 345px) {
      right: -77px;
    }
  }
  p,
  ul li {
    color: #30333b;
    font-family: Quicksand;
    font-size: 22px;
    letter-spacing: 0;
    line-height: 36px;
    @media (max-width: 900px) {
      font-size: 18px;
      line-height: 28px;
    }
    @media (max-width: 500px) {
      width: 100%;

      font-size: 14px;
      line-height: 24px;
    }
  }
  p.left {
    width: 48%;
    @media screen and (max-width: 500px) {
      width: 100%;
    }
  }
  .main-image {
    width: 100%;
    height: 529px;
    margin: 30px 0px;

    img {
      width: 100%;
      height: 100%;
      object-fit: cover;
    }
    @media screen and (max-width: 500px) {
      height: auto;
    }
  }
  ${Mainheading} {
    @media screen and (max-width: 500px) {
      width: 100%;
    }
  }
  table {
    td {
      padding: 10px;
    }
  }
`;

const Image = styled.div`
  position: absolute;
  left: 0px;
  right: 0px;
  bottom: 0px;
  top: 0px;
  z-index: 0;
  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
`;

const Title = styled.h1`
  color: #ffffff;
  font-family: Quicksand;
  font-size: 60px;
  letter-spacing: 0;
  line-height: 75px;
  font-weight: 600;
  position: relative;
  max-width: 1180px;
  padding: 0px 20px;
  margin: 120px auto 0px;
  @media (max-width: 900px) {
    margin-top: 36px;
    font-size: 46px;
  }
  @media (max-width: 900px) {
    margin-top: 10px;
    font-size: 22px;
  }
`;

const InterestingRead = styled.div`
  width: 100%;
  max-width: 1180px;
  display: flex;
  //   border-top: 1px solid #e6e8ec;
  padding: 60px 20px 120px;
  margin: 0px auto;
  flex-direction: column;
  .interestContainer {
    @media screen and (max-width: 765px) {
      flex-direction: column;
      align-items: center;
    }
  }
  @media screen and (max-width: 765px) {
    padding: 30px 20px 30px;
  }
`;

const SubInfo = styled.div`
  color: #30333b;
  font-family: 'Quicksand', sans-serif;
  font-size: 16px;
  font-weight: 500;
  line-height: 20px;
  margin-bottom: 30px;
`;

const WeEnsure = styled.div`
  display: flex;
  align-items: center;
  flex-direction: ${props => props.direction || 'row'};
  margin: ${props => props.margin || '0px auto 60px 0px;'};
  width: 60%;
  @media screen and (max-width: 900px) {
    width: 80%;
    margin-bottom: 30px;
  }
  @media screen and (max-width: 500px) {
    flex-direction: column;
    width: 100%;
    margin: 0px 0px 60px;
    align-items: flex-start;
  }
  ${Flex} {
    @media screen and (max-width: 500px) {
      margin: 0px;
      align-items: flex-start;
    }
  }
  img {
    @media screen and (max-width: 900px) {
      transform: scale(0.9);
    }
    @media screen and (max-width: 500px) {
      transform: scale(1);
      margin-bottom: 40px;
    }
  }
  h3 {
    color: #30333b;
    font-family: Quicksand;
    font-size: 22px;
    letter-spacing: 0;
    line-height: 28px;
    margin: 0px 0px 20px;
    @media screen and (max-width: 900px) {
      font-size: 20px;
    }
    @media screen and (max-width: 500px) {
      font-size: 16px;
      line-height: 24px;
      margin: 0px 0px 16px;
    }
  }
  span {
    color: #30333b;
    font-family: Quicksand;
    font-size: 18px;
    letter-spacing: 0;
    line-height: 24px;
    @media screen and (max-width: 900px) {
      font-size: 16px;
    }
    @media screen and (max-width: 500px) {
      text-align: left !important;
      font-size: 14px;
      line-height: 24px;
    }
  }
`;

// const OurTeam = styled.div`
//   display: flex;
//   margin: ${props => props.margin || '0px auto 80px 0px;'};
//   width: 100%;
//   @media screen and (max-width: 500px) {
//     background: #f2eff5;
//     border-radius: 5px;
//     margin-bottom: 24px;
//     padding: 20px;
//     flex-direction: column;
//   }
//   ${Flex} {
//     @media screen and (max-width: 500px) {
//       margin: 0px;
//     }
//   }
//   .ourImage {
//     height: 225px;
//     width: 225px;
//     border-radius: 50%;
//     overflow: hidden;
//     img {
//       width: 100%;
//       height: 100%;
//     }
//     @media screen and (max-width: 500px) {
//       height: 146px;
//       width: 146px;
//       margin-bottom: 24px;
//     }
//   }
//   h2 {
//     color: #30333b;
//     font-family: Quicksand;
//     font-size: 22px;
//     letter-spacing: 0;
//     line-height: 28px;
//     margin: 0px 0px 20px;
//     @media screen and (max-width: 500px) {
//       font-size: 14px;
//       line-height: 24px;
//       margin-bottom: 10px;
//     }
//   }
//   span {
//     color: #30333b;
//     font-family: Quicksand;
//     font-size: 18px;
//     letter-spacing: 0;
//     line-height: 24px;
//     @media screen and (max-width: 500px) {
//       font-size: 12px;
//       line-height: 24px;
//     }
//   }
// `;

const ParentsMessage = styled.div`
  position: relative;
  padding: 253px 0px;
  @media (max-width: 500px) {
    padding: 100px 0px 170px;
  }
  @media (max-width: 375px) {
    padding: 90px 0px 177px;
  }
  .parentMessage {
    position: absolute;
    top: 0px;
    right: 0px;
    left: 0px;
    height: 100%;

    img {
      width: 100%;
      height: 100%;
      object-fit: cover;
    }

    &-content {
      display: flex;
      //   justify-content: space-between;
      height: auto;
      width: 100%;
      max-width: 1180px;
      align-items: baseline;
      padding: 0px 20px 0px;
      margin: 0px auto;
      .outline {
        &:hover {
          background-color: transparent;
          border-color: #fff;
          color: #fff;
        }
      }
      @media (max-width: 500px) {
        flex-direction: column;
        width: 100%;
        align-items: center;
      }
    }
    &-right {
      @media (max-width: 500px) {
        width: 95%;
        align-items: center;
      }
    }
  }
  h2 {
    width: fit-content;
    color: #ffffff;
    font-family: Quicksand;
    font-size: 32px;
    letter-spacing: 0;
    line-height: 40px;
    z-index: 1;
    margin-right: 20%;
    @media (max-width: 500px) {
      margin-right: 0px;
      font-size: 20px;
    }
  }
  h3 {
    color: #ffffff;
    font-family: Quicksand;
    font-size: 26px;
    font-weight: 500;
    letter-spacing: 0;
    line-height: 35px;
    margin-bottom: 30px;
    margin-top: 30px;
    @media (max-width: 500px) {
      font-size: 14px;
      line-height: 26px;
      margin-top: 10px;
      text-align: center;
    }
  }
  .outline {
    padding: 0px 30px;
    @media (max-width: 500px) {
      height: 36px;
      font-size: 12px;
    }
  }
`;
