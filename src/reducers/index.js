import { combineReducers } from "redux";

import DashboardReducer from "shared/containers/Dashboard/reducer";
import AuthenticationReducer from "shared/containers/Authentication/reducer";

const rootReducer = combineReducers({
  dashboard: DashboardReducer,
  authentication: AuthenticationReducer
});

export default rootReducer;
