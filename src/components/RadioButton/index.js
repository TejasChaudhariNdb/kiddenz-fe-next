/* eslint-disable react/no-unused-prop-types */
/* eslint-disable jsx-a11y/label-has-associated-control */
/**
 *
 * Button.js
 *
 * A common button, if you pass it a prop "route" it'll render a link to a react-router route
 * otherwise it'll render a link with an onclick
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import colors from 'utils/colors';

// import { css } from 'styled-components';
export const Radio = styled.div`
  position: relative;
  z-index: 0;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  input[type='radio']:checked {
    position: absolute;
    top: 0px;
    z-index: 10;
    // background: red;
    left: 0px;
    width:28px;
    height:30px;
    opacity: 0;
    cursor:pointer;
  }
  input[type='radio']:not(:checked) {
    position: absolute;
    top: 0px;
    z-index: 10;
    // background: red;
    left: 0px;
    width:28px;
    height:30px;
    opacity: 0;
    cursor:pointer;
  }
  input[type='radio']:checked + label {
    position: relative;
    padding: ${props => props.filterPadding || '0px 0px 0px 35px'};
    cursor: pointer;
    margin: ${props => props.RadioMargin};
    color: ${props => props.filterRadio || '#fff'};
    font-family: 'Roboto', sans-serif;
    font-size: ${props => props.filterFont || '16px'};
    font-weight: ${props => props.filterFontWeight || '700'};
    line-height: ${props => props.filterLineheight || '16px'};
    display: inline-block;
    white-space: nowrap;
  }
  input[type='radio']:not(:checked) + label {
    position: relative;
    // padding-left: ${props => props.filterPadding || '35px'};
    padding: ${props => props.filterPadding || '0px 0px 0px 35px'};
    cursor: pointer;
    margin: ${props => props.RadioMargin};
    color: ${props => props.filterRadio || '#fff'};
    font-family: 'Roboto', sans-serif;
    font-size: ${props => props.filterFont || '16px'};
    font-weight: ${props => props.filterFontWeight || '700'};
    line-height: ${props => props.filterLineheight || '16px'};
    display: inline-block;
    white-space: nowrap;
  }
  input[type='radio']:checked + label:before {
    content: '';
    position: absolute;
    left: 0;
    top: 1px;
    width: 20px;
    height: 20px;
    border: 1px solid ${props => props.checkedColor || `${colors.secondary}`};
    border-radius: 100%;
    // background: #fff;
    @media (max-width: 500px) {
      top: 2px;
    }
  }
  input[type='radio']:not(:checked) + label:before {
    content: '';
    position: absolute;
    left: 0;
    top: 1px;
    width: 20px;
    height: 20px;
    border: 1px solid #ddd;
    border-radius: 100%;
    // background: #fff;
    @media (max-width: 500px) {
      top: 2px;
    }
  }
  input[type='radio']:checked + label:after {
    content: '';
    width: 12px;
    height: 12px;
    background: ${props => props.checkedColor || `${colors.secondary}`};
    position: absolute;
    top: 5px;
    left: 4px;
    border-radius: 100%;
    -webkit-transition: all 0.2s ease;
    transition: all 0.2s ease;
    @media (max-width: 500px) {
      top: 6px;
    }
  }
  input[type='radio']:not(:checked) + label:after {
    content: '';
    width: 12px;
    height: 12px;
    background: ${props => props.checkedColor || '#613A95'};
    position: absolute;
    top: 5px;
    left: 4px;
    border-radius: 100%;
    -webkit-transition: all 0.2s ease;
    transition: all 0.2s ease;
    @media (max-width: 500px) {
      top: 6px;
    }
  }
  input[type='radio']:not(:checked) + label:after {
    opacity: 0;
    -webkit-transform: scale(0);
    transform: scale(0);
  }
  input[type='radio']:checked + label:after {
    opacity: 1;
    -webkit-transform: scale(1);
    transform: scale(1);
  }
`;
// const Label = styled.label`
//   color: ${colors.white};
//   font-family: 'Roboto', sans-serif;
//   font-size: 16px;
//   font-weight: bold;
//   line-height: 21px;
// `;
const RadioButton = props => (
  <Radio
    filterRadio={props.filterRadio}
    filterFont={props.filterFont}
    filterFontWeight={props.filterFontWeight}
    filterLineheight={props.filterLineheight}
    filterPadding={props.filterPadding}
    RadioMargin={props.RadioMargin}
    checkedColor={props.checkedColor}
  >
    <input
      type="radio"
      id={props.id}
      name={props.id}
      checked={props.checked}
      onChange={e => props.onClick(e)}
      placeholder={props.text}
      style={{ opacity: 0 }}
    />
    <label forHtml={props.id}>{props.text}</label>
  </Radio>
);

// If the Button has a handleRoute prop, we want to render a button
// if (props.handleRoute) {
//   button = (
//     <StyledButton onClick={props.handleRoute}>
//       {Children.toArray(props.children)}
//     </StyledButton>
//   );
// }

RadioButton.propTypes = {
  // handleRoute: PropTypes.func,
  id: PropTypes.string,
  text: PropTypes.string,
  filterRadio: PropTypes.string,
  filterFont: PropTypes.string,
  filterFontWeight: PropTypes.string,
  filterPadding: PropTypes.string,
  filterLineheight: PropTypes.string,
  RadioMargin: PropTypes.string,
  checkedColor: PropTypes.string,
  checked: PropTypes.bool,
  onClick: PropTypes.func,
};

export default RadioButton;
