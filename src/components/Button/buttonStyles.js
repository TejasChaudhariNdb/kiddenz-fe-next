import { css } from 'styled-components';
// eslint-disable-next-line import/no-named-as-default-member
import colors from 'utils/colors';

const buttonStyles = css`
// height: 56px;
height: ${props => props.height};
width: ${props => (props.fullwidth ? '100%' : 'auto')};
// min-width: 166px;
// display: inline-block;
display: ${props => (props.display ? props.display : 'flex')};
justify-content: center;
align-items: center;
box-sizing: border-box;
padding: ${props => (props.headerButton ? '29px' : '56px')};
padding-top: 0px;
padding-bottom: 0px;
margin-top: ${props => props.marginTop};
margin-right: ${props => props.marginRight};
text-decoration: none;
border-radius: 5px;
-webkit-font-smoothing: antialiased;
-webkit-touch-callout: none;
user-select: none;
cursor: pointer;
outline: 0;
font-family: 'Quicksand', sans-serif;
font-weight: 500;
font-size: 16px;
line-height: 20px;
text-align: center;
// box-shadow: 0 0 20px 0 rgba(0,0,0,0.1);}
border: 1px solid #60B947;
background-color: #60B947;
color:  ${colors.white};
white-space: nowrap;
&:hover
{
  background-color: ${colors.white};
  color:#60B947;

}
&:disabled{
  cursor: default;
  // background: ${colors.lightGrey};
  // border-color:${colors.lightGrey};
}
.icon{
  display: none;
}
&.secondary {
  background-color: ${colors.secondary};
  border-color: ${colors.secondary};
  &:hover
  {
    background-color: ${colors.white};
    color:${colors.secondary};

  }
}
&.filter{
  height: 40px;
  padding:0px 22px;
  padding-right: ${({ paddingRight }) => paddingRight || '11px'};
  font-size: 13px;
  line-height: 18px;
  .iconify
  {
    margin-left: 14px;
    margin-right: -11px;
    width: 20px;
    height: 20px;
  }
}
&.outline {
  border-width: 1px;
  background-color: ${colors.white};
  border-color: ${colors.secondary};
  color: ${colors.secondary};
  &:hover
  {
    box-shadow: 0 0 20px 0 rgba(0,0,0,0.1);
  }
}
&.outlineHover {
  padding: 0 20px;
  font-weight: 600;
  &:hover,&.active
  {
    background-color: rgb(97, 58, 149);
    border-color: rgb(255, 255, 255);
    color: rgb(255, 255, 255);
    box-shadow: none;
  }
}
&.smallOutline{
  height: 30px;	
  width: 50px;
  padding: 0px;
  font-family: 'Roboto', sans-serif;
  font-size: 14px;	
  line-height: 19px;
  background-color: ${colors.white};
  border:0.5px solid rgba(192,200,205,0.9);
  box-shadow:none;
  color: rgba(48,51,59,0.9);
  text-transform: capitalize;
}
&.share
{
  border-width: 0px;
  background-color: transparent;
  color: ${colors.secondary};
  box-shadow:none;
}
&.btnWhite {
  color:#55357F;
  background-color: ${colors.white};
  border-color: ${colors.white};
  &:hover
  {
    border-color: ${colors.secondary};
  }
  &.footer{
    color: ${colors.darkBlue};
  }
}
&.links {
  height: auto;
  padding: 0px;
  background-color: transparent;
  border-color: transparent;
  box-shadow: none;
  color: ${colors.black};
  // &:hover
  // {
  //   box-shadow: 0 0 20px 0 rgba(0,0,0,0.1);
  // }
}
&.subscribe
{
  color: #666C78;
  background-color: ${colors.white};
  border-color: #666C78;
  box-shadow:none;
  &:hover
  {
box-shadow: 0 0 10px 0 rgba(0,0,0,0.1);

  }
}
&.viewArticle
{
  color: #666C78;
  background-color: ${colors.white};
  border: 0px;
  box-shadow: 0 2px 4px 0 rgba(0,0,0,0.26);
  &:hover
  {
box-shadow: 0 0 20px 0 rgba(0,0,0,0.1);
  }
}
&.attach
{
  height: auto;
  font-size:14px;
  padding: 0px;
  background-color: transparent;
  border-color: transparent;
  box-shadow: none;
  color:${colors.secondary};
}
&.dropdown{
  height: 36px;
  width: fit-content;
  padding: 0px 10px;
  border: 1px solid #C0C8CD;
  background-color: transparent;
  color: ${colors.inputPlaceholder};
  font-size: 14px;	line-height: 16px;
  box-shadow: none;
  .icon
  {
    display: inline-block;
    margin-left: 5px;
    transform:rotate(0deg);
    transition:transform 0.2s ease;
    &.active
    {
    transform:rotate(180deg);
    transition:transform 0.2s ease;
    }
  }
}
&.mobile{
  &:hover
  {
    background-color: ${colors.white};
    color:#60B947;

  }
  @media (max-width: 768px) {
    margin-right: 0px;
    margin-bottom: 20px;
  }
  @media (max-width: 500px) {
    padding:0px 25px;
    font-size:14px;
    height:46px;
  }
}
&.profile
{
  height: auto;
  padding:12px 20px;  
  background-color: ${colors.secondary};
  border-color: ${colors.secondary};  
  &:hover
  {
    background-color: ${colors.white};
    color:${colors.secondary};

  }
  @media (max-width: 414px) {
    margin-bottom: 10px;
  }
}
&.viewAll
{
  height: auto;
  padding:0px 10px;
  font-size: 16px;
  font-weight: 700;
  color:${colors.secondary};
  border-radius: 12.5px;
  background-color: rgba(97,58,149,0.1);
  border-color: #EBF1FC;
  box-shadow:none;
  @media screen and (max-width: 768px) {
    font-size: 14px;
  }
  &:hover
  {
    background-color:${colors.secondary};
    color:${colors.white};
  }
}
&.VA {
  display: block;
  margin: 0 auto;
  background: rgba(97, 58, 149, 0.1);
  border-radius: 19.5px;
  width: 160px;
  height: 40px;
  @media screen and (max-width: 768px) {
    width: 130px;
  }
}
&.preCare
{
  border-color:#55357F;
  color: ${colors.white};
   font-size: 20px;	
   font-weight: bold;	
   line-height: 25px;
  height: 65px;	
  // width: 280px;	
  border-radius: 35px;	
  background-color: #55357F;
  box-shadow: 0 20px 40px 0 rgba(0,0,0,0.15) !important;
  @media screen and (max-width: 767px)
  {
    font-size: 14px;
    height: auto;
    width: auto;
    padding: 10px 20px;
    margin-right: 0px;

  }
  @media screen and (max-width: 500px) {
    box-shadow: 0 5px 7px 0 rgba(0,0,0,0.15) !important;
  }
  @media screen and (max-width: 380px) {
    font-size: 14px;
    // height: 50px;
    // width: 300px;	
  }
  @media screen and (max-width: 360px) {
    font-size: 12px;
    // width: 280px;	 
  }
 
}
&.roundedGreen
{
  box-shadow: none;
  border: 1px solid #60B947;
  background-color: #60B947;
  &:hover
  {
    background-color:#fff !important;
    border-color:#60B947 !important; 
    color:#60B947 !important;
  }
}

&.noShadow {
  box-shadow: none;
}

`;

// const headerButton = styled.buttonStyles`
//   padding: 29px;
// `;
export default buttonStyles;
