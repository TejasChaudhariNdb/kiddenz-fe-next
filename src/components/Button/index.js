/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable indent */
/**
 *
 * Button.js
 *
 * A common button, if you pass it a prop "route" it'll render a link to a react-router route
 * otherwise it'll render a link with an onclick
 */

import React from 'react';
import PropTypes from 'prop-types';
import Icon from '@mdi/react';
import { mdiChevronDown, mdiCloseCircle } from '@mdi/js';
// import { css } from 'styled-components';

// import colors from "./utils/colors";
import styled from 'styled-components';
import buttonStyles from './buttonStyles';
import blueSpinner from '../../images/blueSpinner.gif';
// import greenSpinner from '../../images/greenSpinner.gif';
import spinner from '../../images/spinner.gif';
import purpleSpinner from '../../images/purple_spinner.gif';

// import A from './A';
// import StyledButton from './StyledButton';
// import Wrapper from './Wrapper';
const A = styled.button`
  ${buttonStyles};
`;
const Button = props => (
  <A
    // href={props.href}
    onClick={!props.isLoading && !props.disabled ? props.onClick : null}
    fullwidth={props.fullwidth}
    fitContent={props.fitContent}
    height={props.height}
    marginRight={props.marginRight}
    headerButton={props.headerButton}
    marginTop={props.marginTop}
    className={props.type}
    disabled={props.disabled}
    fontWeight={props.fontWeight}
    display={props.display}
    paddingRight={props.displayAcessory ? '11px' : '22px'}
  >
    {!props.isLoading && props.text}
    {props.isLoading &&
      props.type === 'secondary' && (
        <img src={purpleSpinner} height={25} alt="" />
      )}
    {props.isLoading &&
      (props.type === 'outline' || props.type === 'subscribe') && (
        <img src={blueSpinner} height={25} alt="" />
      )}
    {props.isLoading &&
      props.type === 'mobile' && <img src={purpleSpinner} height={25} alt="" />}

    {props.isLoading &&
      props.type === 'profile' && <img src={spinner} height={25} alt="" />}
    {props.isLoading &&
      props.type === 'request' && <img src={spinner} height={25} alt="" />}

    {props.type === 'filter' &&
      props.displayAcessory && (
        // <span
        //   className="iconify"
        //   data-icon="carbon:close-filled"
        //   data-inline="false"
        //   onClick={props.buttonAcessoryClick}
        // />
        <span
          onClick={props.buttonAcessoryClick}
          style={{ paddingLeft: '6px' }}
        >
          <Icon path={mdiCloseCircle} size={0.9} />
        </span>
      )}
    <Icon path={mdiChevronDown} size={1} className="icon" />
  </A>
);

// If the Button has a handleRoute prop, we want to render a button
// if (props.handleRoute) {
//   button = (
//     <StyledButton onClick={props.handleRoute}>
//       {Children.toArray(props.children)}
//     </StyledButton>
//   );
// }

Button.propTypes = {
  // handleRoute: PropTypes.func,
  // href: PropTypes.string,
  marginRight: PropTypes.string,
  marginTop: PropTypes.string,
  type: PropTypes.string,
  text: PropTypes.string,
  height: PropTypes.string,
  onClick: PropTypes.func,
  // children: PropTypes.node.isRequired,
  fitContent: PropTypes.string,
  fullwidth: PropTypes.bool,
  disabled: PropTypes.bool,
  headerButton: PropTypes.bool,
  displayAcessory: PropTypes.bool,
  isLoading: PropTypes.bool,
  buttonAcessoryClick: PropTypes.func,
  fontWeight: PropTypes.string,
  display: PropTypes.string,
};
Button.defaultProps = {
  height: '56px',
};
export default Button;
