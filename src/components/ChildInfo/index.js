import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Flex from '../common/flex';

const Childinfo = styled.div`
  width: 100%;
  display: flex;
  padding: 18px;
  border: 1px solid #f3f5f9;
  border-radius: 10px;
  background-color: rgba(242, 239, 245, 1);
  margin: ${props => props.margin || '0px 0px 20px 0px;'};
  @media (max-width: 400px) {
    padding: 12px;
  }

  &.active {
    border: 1px solid #613a95;
    .tickIcon {
      color: #60b947;
      width: 20px;
      height: 20px;
      position: absolute;
      right: -69px;
      top: 15px;
    }
  }

  h3 {
    color: #666c78;
    font-family: Roboto;
    font-size: 14px;
    line-height: 19px;
    font-weight: 400;
    margin: 0px;
    span {
      color: rgba(48, 51, 59, 1);
    }
  }
`;
const ChildImage = styled.div`
  height: 54px;
  width: 54px;
  border-radius: 50%;
  overflow: hidden;
  img {
    width: 100%;
    height: 100%;
  }
`;
const ChildName = styled.div`
  color: #30333b;
  font-size: 16px;
  line-height: 18px;
`;

const ChildInfo = ({
  margin,
  image,
  name,
  age,
  gender,
  index,
  selectedChildren,
  addChildToSchedule,
}) => (
  <Childinfo
    margin={margin}
    className={selectedChildren.includes(index) ? 'active' : ''}
    onClick={() => addChildToSchedule(index)}
  >
    <ChildImage>
      <img src={image} alt="" />
    </ChildImage>
    <Flex column justifyBetween flexWidth="60%" flexMargin="0px 0px 0px 20px">
      <ChildName>{name}</ChildName>
      <Flex justifyBetween>
        <h3>
          Age: <span>{age}</span>
        </h3>
        <h3>
          Gender: <span>{gender}</span>
        </h3>
      </Flex>
      {/* <span
        className="iconify tickIcon"
        data-icon={selectedChildren.includes(index) ? 'cil:check-alt' : ''}
        data-inline="false"
      /> */}
    </Flex>
  </Childinfo>
);
ChildInfo.propTypes = {
  margin: PropTypes.string,
  image: PropTypes.string,
  name: PropTypes.string,
  age: PropTypes.string,
  gender: PropTypes.string,
  index: PropTypes.number,
  selectedChildren: PropTypes.array,
  addChildToSchedule: PropTypes.func,
};
export default ChildInfo;
