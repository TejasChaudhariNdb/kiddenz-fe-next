/* eslint-disable import/no-unresolved */
import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { useDropdownClose } from 'shared/hooks';
import styled from 'styled-components';
// import InputRange from 'react-input-range';
import colors from 'utils/colors';
// import Flex from '../common/flex';
import Button from '../Button';
// import CheckBox from '../Checkbox';
// import RadioButton from '../RadioButton';
// import 'react-input-range/lib/css/index.css';

const DROPDOWN_CONFIG = {
  sort: [
    {
      id: '01',
      text: 'Distance',
      key: 'distance',
    },
    // {
    //   id: '02',
    //   text: 'Popularity',
    //   key: 'popularity',
    // },
    // {
    //   id: '03',
    //   text: 'Newly Added',
    //   key: 'newly_added',
    // },
    // {
    //   id: '04',
    //   text: 'Price: Low to High',
    //   key: 'price_low_to_high',
    // },
    // {
    //   id: '05',
    //   text: 'Price: High to Low',
    //   key: 'price_high_to_low',
    // },
    // {
    //   id: '06',
    //   text: 'Rating',
    //   key: 'rating',
    // },
    // {
    //   id: '07',
    //   text: 'Discount',
    //   key: 'discount',
    // },
  ],
};

function Dropdown({ type, dropdownText, sortType, setSortType }) {
  const dropdownRef = useRef(null);
  const [status, setStatus] = useState(false);
  useDropdownClose(dropdownRef, setStatus);
  return (
    <DropdownWrapper column>
      <Button
        text={dropdownText}
        type="dropdown"
        onClick={() => setStatus(!status)}
      />
      {status && (
        <DropdownMenu ref={dropdownRef}>
          {DROPDOWN_CONFIG[type].map(items => (
            <DropdownMenuItem
              key={items.id}
              className={sortType === items.key ? 'active' : ''}
              onClick={e => {
                setSortType(e, items.key);
                setStatus(false);
              }}
            >
              {items.text}

              <span
                className="iconify"
                data-icon="mdi-light:check"
                data-inline="false"
              />
            </DropdownMenuItem>
          ))}
        </DropdownMenu>
      )}
    </DropdownWrapper>
  );
}

Dropdown.propTypes = {
  dropdownText: PropTypes.string,
  type: PropTypes.string,
  sortType: PropTypes.string,
  setSortType: PropTypes.func,
};

Dropdown.defaultProps = {
  dropdownText: 'Sort By',
};
export default Dropdown;

const DropdownWrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  z-index: 100;
`;
const DropdownMenu = styled.ul`
  position: absolute;
  top: 80%;
  right: 0;
  // width: 100%;
  width: 220px;
  padding: 10px;
  border-radius: 10px;
  background-color: ${colors.white};
  box-shadow: 0 0 10px 0 rgba(0,0,0,0.1);}
  list-style-type: none;
  @media (max-width:500px)
  {
    right:0px;
    width: 180px;
    border-radius: 6px;
  }
`;
const DropdownMenuItem = styled.li`
  height: 40px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0px 10px;
  border-radius: 5px;
  color: ${colors.inputPlaceholder};
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 40px;
  cursor: pointer;
  list-style-type: none;
  @media (max-width: 500px) {
    height: 24px;
    font-size: 12px;
    line-height: 24px;
  }
  .iconify {
    display: none;
    color: ${colors.secondary};
    font-size: 20px;
  }
  .dropdownArrow {
    transform: rotate(0deg);
    transition: transform 0.2s ease;
    &.active {
      transform: rotate(180deg);
      transition: transform 0.2s ease;
    }
  }
  &:hover {
    background-color: #ebf1fc;
    color: ${colors.secondary};
  }
  &.active {
    .iconify {
      display: inline-block;
    }
  }
`;
