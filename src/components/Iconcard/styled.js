import styled from 'styled-components';
import colors from 'utils/colors';
export const IconCard = styled.div`
  cursor: pointer;
  padding: ${props => props.padding || '0px'};
  margin: ${props => props.margin || '0px'};
  display: flex;
  align-items: center;
  justify-content: center;
  height: 50px;
  width: 50px;
  border-radius: 50%;
  background-color: rgba(255, 255, 255, 1);
  box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.05);
  background: ${props => props.background || '#fff'};
  .iconify {
    color: ${props => props.fill || 'rgba(48, 51, 59, 1)'};
    width: 30px;
    height: 30px;
  }
  &:hover
  {
    box-shadow: 0 0 25px 0 rgba(0, 0, 0, 0.1);
  }
  &.square {
    position: absolute;
    top:  ${props => props.top || '15px'};
    right:  ${props => props.right || '15px'};
    height: 24px;
    width: 24px;
    border-radius: 5px;
    background-color: rgba(0, 0, 0, 0.5);
    .iconify {
      color: ${colors.white};
  }
  &.active
  {
  background-color: #fff;
  }


  @media (max-width: 414px) {
    top: 10px;
    right: 10px;
  }
}
&.heart {
  position: absolute;
  top:  ${props => props.top || '15px'};
  right:  ${props => props.right || '15px'};
  height: 24px;
  width: 24px;
  border-radius: 5px;
  background-color: rgba(0, 0, 0, 0.5);
  .iconify {
    color: ${colors.white};
}
&.active
{
  background-color: #fff;
}

@media (max-width: 414px) {
  top: 10px;
  right: 10px;
}
}
&.favourite
{
  width:100%;
  height: 150px;
  background-color:#F9FAFC;
  border-radius: 5px;
  box-shadow:none;
  .iconify
  {
    height: 77px;	
    width: 58px;
    color:#fff;
  }
  @media (max-width: 500px) {
    height: 240px;
  }
}
&.bookmark
{
  position: absolute;
  top: 5px;
  right: 5px;
  height: 24px;
  width: 24px;
  border-radius: 5px;
  background-color: rgba(0, 0, 0, 0.5);
  @media (max-width: 1000px) {
    height: 20px;
  width: 20px;
  }
  
  .iconify {
    color: ${colors.white};
    width: 20px;
    height: 20px;
    @media (max-width: 1000px) {
      height: 16px;
      width: 16px;
    }
}
&.favourite
{
  width:100%;
  height: 128px;
  background-color:#F9FAFC;
}
`;
