import React from 'react';
import PropTypes from 'prop-types';
import Icon from '@mdi/react';
import { IconCard } from './styled';
const IconButton = ({
  iconName,
  margin,
  type,
  padding,
  onClick,
  top,
  right,
  background,
  isWishlisted,
  fill,
  inconFill,
  mdi = false,
}) => {
  React.useEffect(() => {}, [iconName]);

  return (
    <IconCard
      margin={margin}
      className={`${type} active`}
      padding={padding}
      top={top}
      right={right}
      onClick={onClick}
      background={background}
      fill={fill}
    >
      {mdi ? (
        <Icon
          path={iconName}
          size={1}
          className="icon"
          color={isWishlisted ? '#613A95' : inconFill || '#000'}
          style={{
            stroke: isWishlisted ? 'none' : '#616161',
            strokeWidth: isWishlisted ? '0px' : '1px',
          }}
        />
      ) : (
        <span className="iconify" data-icon={iconName} data-inline="false">
          {' '}
        </span>
      )}
    </IconCard>
  );
};
IconButton.propTypes = {
  iconName: PropTypes.string,
  margin: PropTypes.string,
  type: PropTypes.string,
  padding: PropTypes.string,
  top: PropTypes.string,
  right: PropTypes.string,
  background: PropTypes.string,
  inconFill: PropTypes.string,
  fill: PropTypes.string,
  onClick: PropTypes.func,
  isWishlisted: PropTypes.bool,
  mdi: PropTypes.func,
};
export default IconButton;
