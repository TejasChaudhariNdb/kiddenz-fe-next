import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import colors from 'utils/colors';

const Heading2 = styled.h2`
  width: 100%;
  margin: 0px;
  color: ${colors.darkGrey};
  font-family: 'Quicksand', sans-serif;
  font-weight: 700;
  font-size: 32px;
  line-height: 40px;
  text-align: ${props => props.textAlign};

  @media (max-width: 768px) {
    font-size: 25px;
  }
  @media (max-width: 500px) {
    font-size: 20px;
    line-height: 26px;
  }
`;

const H2 = props => (
  <Heading2 textAlign={props.textAlign}>{props.text}</Heading2>
);
H2.propTypes = {
  text: PropTypes.string,
  textAlign: PropTypes.string,
};
export default H2;
