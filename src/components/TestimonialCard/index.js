/* eslint-disable no-unused-vars */
/* eslint-disable react/no-unused-prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import PhotoCard from '../PhotoCard/index';
// import image2 from '../../images/profileImage.jpg';
// import profile from '../../images/profile.jpg';
import Flex from '../common/flex';

import { TestimonialCard, Name, CommentIcon, Comment, Job } from './styled';

const Testimonial = ({
  name,
  comment,
  type,
  job,
  img,
  width,
  height,
  designation,
}) => (
  <TestimonialCard className={type}>
    {type === 'primary' ? (
      <>
        <div className="testimonialMargin">
          <div className="testimonialShadow">
            <Flex whitebg="#fff" flexHeight="100%" flexBorderRadius="10px">
              <PhotoCard
                height="100%"
                width="30%"
                image={img || null}
                margin="0px"
              />
              <Flex
                column
                flexPadding="45px"
                flexWidth="70%"
                className="testimonialComment"
              >
                <Comment>{comment}</Comment>
                <Name>{name}</Name>
                <Job>{job}</Job>
              </Flex>
            </Flex>
          </div>
        </div>
      </>
    ) : (
      <>
        <Flex alignCenter>
          <PhotoCard
            image={img || null}
            width={width || '40px'}
            height={height || '40px'}
          />
        </Flex>
        <Flex>
          <CommentIcon>
            <span
              className="iconify"
              data-icon="ls:quote"
              data-inline="false"
            />
          </CommentIcon>
          <Comment>{comment}</Comment>
        </Flex>
        <Name>- {name}</Name>
        <Name>{designation}</Name>
      </>
    )}
  </TestimonialCard>
);
Testimonial.propTypes = {
  designation: PropTypes.string,
  name: PropTypes.string,
  comment: PropTypes.string,
  type: PropTypes.string,
  job: PropTypes.string,
  img: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};
export default Testimonial;
