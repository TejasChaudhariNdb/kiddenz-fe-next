import styled from 'styled-components';
import { Photo } from '../PhotoCard/styled';
export const Comment = styled.div`
  color: #30333b;
  font-family: 'Quicksand', sans-serif;
  font-size: 14px;
  margin-left: 14px;
  font-weight: 500;
  line-height: 21px;
  @media (max-width: 1300px) {
    font-size: 13px;
  }
  @media screen and (max-width: 400px) {
    font-size: 12px;
  }
`;
export const Name = styled.div`
  color: #30333b;
  font-family: 'Quicksand', sans-serif;
  margin-left: 5px;
  font-size: 14px;
  line-height: 19px;
  display: flex;
  justify-content: flex-end;
  width: 90%;
  margin-top: 15px;
`;
export const Job = styled.div`
  color: #30333b;
  font-family: 'Quicksand', sans-serif;
  font-size: 16px;
  line-height: 20px;
`;

export const TestimonialCard = styled.div`
  width: 100%;
  padding: 30px;
  margin-bottom: 30px;
  border-radius: 10px;
  font-weight: 300;
  background-color: #f3f5f9;
  &.popup {
    border-radius: 0px;
    border-bottom: 1px solid #e6e8ec;
    background-color: transparent;
  }
  &.primary {
    height: 240px;
    background-color: transparent;
    padding: 0px;
    @media screen and (max-width: 500px) {
      height: 210px;
    }
    .testimonialMargin {
      margin: 30px;
      height: 90%;
      @media screen and (max-width: 500px) {
        margin: 30px 15px;
      }
    }
    .testimonialShadow {
      background-color: #fff;
      box-shadow: rgba(0, 0, 0, 0.12) 0px 0px 30px 0px;
      height: 100%;
      border-radius: 10px;
    }
    .testimonialComment {
      position: relative;
      @media screen and (max-width: 500px) {
        padding: 20px 10px 20px 20px;
        width: 60%;
      }
      &::after {
        position: absolute;
        content: '';
        width: 0;
        height: 0;
        left: -22px;
        top: 45%;
        border-top: 12px solid transparent;
        border-bottom: 12px solid transparent;
        border-right: 22px solid white;
      }
    }
    ${Photo} {
      width: 40%;
    }
    ${Comment} {
      font-size: 16px;
      margin-bottom: 25px;
      margin-left: 0px;
      @media screen and (max-width: 500px) {
        font-size: 12px;
        margin-bottom: 10px;
      }
    }
    ${Name} {
      font-size: 18px;
      font-weight: bold;
      margin-left: 0px;
      justify-content: flex-start;
      @media screen and (max-width: 500px) {
        font-size: 14px;
        margin-top: 0px;
      }
    }
    ${Job} {
      @media screen and (max-width: 500px) {
        font-size: 12px;
      }
    }
  }
  @media screen and (max-width: 767px) {
    padding: 20px;
  }
`;

export const CommentIcon = styled.div`
  .iconify {
    width: 40px;
    height: 40px;
    color: #4bd498;
    opacity: 0.2;
  }
`;
