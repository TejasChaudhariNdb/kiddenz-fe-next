import React from 'react';
import styled from 'styled-components';
import colors from 'utils/colors';
import Flex from '../common/flex';
import SearchBar from '../SearchBar';
import ProfileDropdown from '../ProfileDropdown/index';
import profileImage from '../../images/profileImage.jpg';
// import { FormattedMessage } from 'react-intl';

// import A from './A';
// import Img from './Img';
// import NavBar from './NavBar';
// import HeaderLink from './HeaderLink';
// import Background from './../../images/headerBg';
// import messages from './messages';
// import buttonStyles from '../Button/buttonStyles';
const NavBar = styled.section`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 86px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0px 30px;
  background-color: ${colors.white};
  z-index: 99;
`;
const NavBarItem = styled.section`
  display: flex;
  justify-content: flex-start;
  align-items: center;
`;
const Logo = styled.div`
  color: ${colors.secondary};
  font-family: 'Quicksand', sans-serif;
  font-size: 30px;
  font-weight: bold;
  line-height: 38px;
`;
const Menu = styled.ul`
  // width: 100%;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  margin-right: 80px;
  color: #30333b;
`;
const MenuItem = styled.li`
  margin-right: 40px;
  font-size: 16px;
  line-height: 18px;
  cursor: pointer;
  list-style-type: none;

  &:last-child {
    margin-right: 0px;
  }
`;
function ProfileHeader() {
  return (
    <>
      {/* <A href="https://www.reactboilerplate.com/">
        <Img src={Banner} alt="react-boilerplate - Logo" />
      </A> */}
      <NavBar>
        <Flex>
          <NavBarItem>
            <Logo>Kiddenz</Logo>
            <SearchBar />
          </NavBarItem>
        </Flex>
        <NavBarItem>
          <Menu>
            <MenuItem>About</MenuItem>
            <MenuItem>Parenting Articles</MenuItem>
            <MenuItem>For Providers</MenuItem>
          </Menu>
          <ProfileDropdown name="Hi Patrícia" image={profileImage} />
        </NavBarItem>
      </NavBar>
    </>
  );
}

export default ProfileHeader;
