/* eslint-disable react/prop-types */
import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';

// Styled components
const GroupContainer = styled.div`
  height: 82px;
  position: relative;
  width: 1116px;
`;

const HowCanITake = styled.p`
  font-family: var(--h-3-font-family);
  font-size: var(--h-3-font-size);
  font-style: var(--h-3-font-style);
  font-weight: var(--h-3-font-weight);
  left: 0;
  letter-spacing: var(--h-3-letter-spacing);
  line-height: var(--h-3-line-height);
  position: absolute;
  top: -1px;
  width: 907px;
`;

const YouCanTalkToThe = styled.p`
  color: #2a2a2ab2;
  font-family: var(--h-4-font-family);
  font-size: var(--h-4-font-size);
  font-style: var(--h-4-font-style);
  font-weight: var(--h-4-font-weight);
  left: 70px;
  letter-spacing: var(--h-4-letter-spacing);
  line-height: var(--h-4-line-height);
  position: absolute;
  top: 51px;
  width: 1000px;
`;

const ChevronDown = styled.img`
  height: 24px;
  left: 1070px;
  position: absolute;
  top: 6px;
  width: 46px;
`;

const Group = ({
  property1,
  className,
  text = 'How can I take admission in eurokids?',
  text1 = 'You can talk to the center head and take admission.',
}) => (
  <GroupContainer className={`group ${className}`}>
    <HowCanITake className={`how-can-i-take ${property1}`}>{text}</HowCanITake>
    {property1 === 'default' && (
      <YouCanTalkToThe className="you-can-talk-to-the">{text1}</YouCanTalkToThe>
    )}
    <ChevronDown
      className="chevron-down"
      alt="Chevron down"
      src={
        property1 === 'default'
          ? 'https://c.animaapp.com/QFxCdMhV/img/chevron-down-9.svg'
          : 'https://c.animaapp.com/QFxCdMhV/img/chevron-down-8.svg'
      }
    />
  </GroupContainer>
);

Group.propTypes = {
  property1: PropTypes.oneOf(['variant-2', 'default']),
  text: PropTypes.string,
  text1: PropTypes.string,
};

export default Group;
