import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Flex from 'components/common/flex';
import Button from '../Button';

const Notifications = ({
  notifyImage,
  date,
  text,
  desc,
  viewBtn,
  name,
  phoneNum,
  cancelText,
  contactPerson,
  tourId,
}) => (
  <NotificationList tourId={tourId}>
    {notifyImage && (
      <Image>
        <img src={notifyImage} alt="" />
      </Image>
    )}
    <Flex justifyBetween column flex1>
      <Flex justifyBetween flexWidth="100%">
        <h2>{text}</h2> <Date>{date}</Date>
      </Flex>
      {desc && <p>{desc}</p>}
      {viewBtn && <Button type="attach" text={viewBtn} />}
      {name && (
        <h3>
          Name:
          <span>{name}</span>
        </h3>
      )}
      {phoneNum && (
        <h3>
          Phone Number:
          <span>{phoneNum}</span>
        </h3>
      )}
      {tourId && (
        <ScheduleDetails>
          <h4>
            Date: <span>Monday, Oct 28th 5:30PM</span>
          </h4>
          <h4>
            Tour ID: <span>BKS29018</span>
          </h4>
        </ScheduleDetails>
      )}
      {contactPerson && (
        <h3>
          Contact person:
          <span>{contactPerson}</span>
        </h3>
      )}

      {cancelText && (
        <Flex flexMargin="16px 0px 0px">
          <Button type="viewAll" text={cancelText} height="46px" />
        </Flex>
      )}
    </Flex>
  </NotificationList>
);
Notifications.propTypes = {
  text: PropTypes.string,
  notifyImage: PropTypes.string,
  date: PropTypes.string,
  desc: PropTypes.string,
  viewBtn: PropTypes.string,
  name: PropTypes.string,
  phoneNum: PropTypes.string,
  cancelText: PropTypes.string,
  contactPerson: PropTypes.string,
  tourId: PropTypes.string,
};
export default Notifications;

const NotificationList = styled.div`
  display: flex;
  width: 100%;
  margin-bottom: 32px;
  padding-bottom: 32px;
  border-bottom: 1px solid #dbdfe7;
  h2 {
    color: #666c78;
    font-family: Roboto;
    font-size: 16px;
    letter-spacing: 0;
    line-height: 19px;
    font-weight: 400;
    margin: 0px 0px 10px;
  }
  h3 {
    color: #30333b;
    font-family: Roboto;
    font-size: 16px;
    letter-spacing: 0;
    line-height: 24px;
    font-weight: 400;
    margin: 5px 0px 0px;
    span {
      font-weight: bold;
      margin-left: 5px;
    }
  }
  p {
    color: #30333b;
    font-family: Roboto;
    font-size: 16px;
    letter-spacing: 0;
    font-weight: 400;
    line-height: 24px;
    margin: 0px;
  }
  .attach {
    width: fit-content;
  }
  .viewAll {
    height: 40px;
    border-radius: 20px;
    padding: 0px 18px;
  }
`;

const Date = styled.div`
  color: #666c78;
  font-family: Roboto;
  font-size: 16px;
  letter-spacing: 0;
  line-height: 19px;
`;

const Image = styled.div`
  height: 52px;
  min-width: 52px;
  width: 52px;
  overflow: hidden;
  border-radius: 50%;
  margin-right: 30px;
  img {
    width: 100%;
    height: 100%;
  }
`;

const ScheduleDetails = styled.div`
  padding: 8px 14px;
  width: 346px;
  border-radius: 10px;
  background-color: #f2eff5;
  h4 {
    color: #30333b;
    font-family: Roboto;
    font-size: 16px;
    font-weight: 400;
    letter-spacing: 0;
    line-height: 26px;
    margin: 0px 0px 4px;
    span {
      font-weight: bold;
      margin-left: 5px;
    }
  }
`;
