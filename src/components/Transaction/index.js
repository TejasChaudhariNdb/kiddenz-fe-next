/* eslint-disable indent */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable react/prop-types */
import React from 'react';
import moment from 'moment';
// import Router from 'next/router';
// import PropTypes from 'prop-types';
// import Link from 'next/link';
import styled from 'styled-components';
import colors from 'utils/colors';
import Flex from '../common/flex';
// import BranchRating from '../BranchRating';
import placeholderImage from '../../images/ReadingImage1.png';

const paymentStatusConfig = {
  '1': {
    title: 'Pending',
    color: 'orange',
  },
  '2': {
    title: 'Cancelled',
    color: 'red',
  },
  '3': {
    title: 'Success',
    color: 'green',
  },
  '4': {
    title: 'Failed',
    color: 'red',
  },
};

const Transaction = props => {
  const {
    name,
    image,
    // batchName,
    transaction = {},
    onDownloadInvoice,
    onClick,
    purchasedBatch,
    onShowSession,
  } = props;
  return (
    <TransactionCard onClick={onClick}>
      <div className="left">
        <img src={image || placeholderImage} alt="Card" />
      </div>
      <div className="right">
        {transaction.payment_status ? (
          <span
            className={`ribbon ${
              paymentStatusConfig[transaction.payment_status].color
            }`}
          >
            {paymentStatusConfig[transaction.payment_status].title}
          </span>
        ) : (
          <span
            className={`ribbon 
        }`}
          >
            Online
          </span>
        )}
        <h4>{name}</h4>
        {purchasedBatch && (
          <Flex column flexMargin="10px 0px 0px">
            <AdditionalInfo margin="0px 0px 5px" color="#666C78">
              <span> {purchasedBatch.heading_name}</span>
            </AdditionalInfo>
            {purchasedBatch.start_date && purchasedBatch.end_date ? (
              <AdditionalInfo margin="0px 0px 5px" color="#60B947">
                <span>
                  {moment(purchasedBatch.start_date).format('DD MMM YY')}
                </span>{' '}
                <span style={{ margin: '0 5px' }}>to</span>
                <span>
                  {moment(purchasedBatch.end_date).format('DD MMM YY')}
                </span>
              </AdditionalInfo>
            ) : (
              <AdditionalInfo margin="0px 0px 5px" color="#60B947">
                <span>{purchasedBatch.till_date}</span>
              </AdditionalInfo>
            )}
          </Flex>
        )}

        {/* {batchName && (
          <BranchRating
            branchname={batchName}
            // ratingNum={schoolRating}
            // starImage={star}
          />
        )} */}
        {/* {batchName && <p>{batchName}</p>} */}
        <p>
          Rs {transaction.amount} | {transaction.payment_method}
        </p>

        <p>Paid on : {moment(transaction.created_on).format('DD MMM YY')}</p>
        {transaction.order_slug_id ? (
          <p>Order Id: {transaction.order_slug_id.toUpperCase()}</p>
        ) : null}
        {/* <a
          href="http://www.africau.edu/images/default/sample.pdf"
          download
          target="_blank"
        >
          Download Invoice
        </a> */}
        <div className="flex-link">
          <div
            className="downlaod-invoice"
            onClick={e => onDownloadInvoice(e, transaction.payment_id)}
          >
            Download Invoice
          </div>
          {(purchasedBatch &&
            purchasedBatch.batch_slots &&
            purchasedBatch.batch_slots.length) ||
          (purchasedBatch &&
            purchasedBatch.batch_days &&
            purchasedBatch.batch_days.length) ? (
            <SessionInfo onClick={onShowSession}>See Batches</SessionInfo>
          ) : null}
        </div>
      </div>
    </TransactionCard>
  );
};

export default Transaction;

const TransactionCard = styled.div`
  display: flex;
  font-family: 'Roboto', sans-serif;
  margin-bottom: 20px;
  cursor: pointer;
  @media screen and (max-width: 768px) {
    flex-direction: column;
  }
  .left {
    width: 45%;
    margin-right: 3%;
    max-height: 153px;
    max-width: 245px;
    @media screen and (max-width: 768px) {
      margin-right: 0;
      margin-bottom: 20px;
      width: 100%;
      max-width: 100%;
      max-height: unset;
    }
    img {
      border-radius: 10px;
      width: 100%;
    }
  }
  .right {
    width: 45%;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: space-evenly;
    @media screen and (max-width: 768px) {
      width: 100%;
    }
    .ribbon {
      padding: 4px;
      font-family: 'Roboto', sans-serif;
      font-weight: 500;
      font-size: 12px;
      line-height: 13px;
      color: #fff;
      margin-right: 10px;
      text-transform: uppercase;
      border-radius: 5px;
    }
    .green {
      background: #1db336d1;
      border: 1px solid #1db336d1;
    }
    .orange {
      background: orange;
      border: 1px solid orange;
    }
    .red {
      background: red;
      border: 1px solid red;
    }
    h4 {
      font-family: 'Roboto', sans-serif;
      margin: 10px 0 5px;
      line-height: 1.2;
    }
    p {
      font-family: 'Roboto', sans-serif;
      margin: 0 0 5px;
      font-size: 14px;
      color: rgba(48, 51, 59, 1);
      text-transform: capitalize;
    }
    .downlaod-invoice {
      font-family: 'Roboto', sans-serif;
      color: #613a95;
      font-size: 14px;
      margin: 0 20px 0 0;
      cursor: pointer;
    }
    .flex-link {
      display: flex;
      flex-direction: row;
    }
  }
`;

const AdditionalInfo = styled.div`
  color: ${props => props.color || '#666c78'};
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 22px;
  margin: ${props => props.margin || '0px'};
  span {
    color: ${props => props.color || colors.lightBlack}};
  }
`;

const SessionInfo = styled.div`
  font-family: 'Roboto', sans-serif;
  color: #613a95;
  font-size: 14px;
  margin: 0;
  cursor: pointer;
`;
