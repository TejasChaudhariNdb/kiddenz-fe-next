import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Flex from '../common/flex';
import thinker from '../../images/thinker.png';

const RefreshFilter = ({
  margin,
  // , isFilterApplied = false
}) => (
  <Refresh margin={margin}>
    <Flex flexMargin="0px auto 0px 0px" className="content">
      <img src={thinker} alt="" />
      <Flex column flexWidth="56%" className="content-right">
        <h3>
          Hey sorry! <span>“no matches”</span> were found for your search
          criteria.
        </h3>
        {/* <h3>
          {!isFilterApplied
            ? `Kindly apply new filters`
            : `We're Sorry! No search results were found matching your criteria.`}
        </h3> */}
      </Flex>
    </Flex>
  </Refresh>
);
RefreshFilter.propTypes = {
  margin: PropTypes.string,
  // isFilterApplied: PropTypes.bool,
};
export default RefreshFilter;

const Refresh = styled.div`
  padding: 35px 20px 0px 20px;
  margin: ${props => props.margin || '20px 0px 40px'};
  border-radius: 10px;
  background-color: #f2eff5;
  img {
    margin-right: 30px;
  }
  .content {
    @media (max-width: 768px) {
      align-items: center;
    }
    @media (max-width: 500px) {
      flex-direction: column-reverse;
    }
    &-right {
      @media (max-width: 500px) {
        width: 100%;
      }
    }
  }
  h3 {
    color: #30333b;
    font-family: Quicksand;
    font-size: 24px;
    font-weight: 400;
    letter-spacing: 0;
    line-height: 32px;
    margin: 0px 0px 30px;
    span {
      font-weight: 500;
    }
    @media (max-width: 500px) {
      font-size: 18px;
      line-height: 24px;
      margin: 0px 0px 20px;
    }
  }
`;
