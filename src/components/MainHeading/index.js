import React from 'react';
import PropTypes from 'prop-types';
import { Mainheading } from './styled';
const MainHeading = ({ text, fontSize, margin }) => (
  <Mainheading fontSize={fontSize} margin={margin}>
    {text}
  </Mainheading>
);
MainHeading.propTypes = {
  text: PropTypes.string,
  fontSize: PropTypes.string,
  margin: PropTypes.string,
};
export default MainHeading;
