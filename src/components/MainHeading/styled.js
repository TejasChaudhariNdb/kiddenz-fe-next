import styled from 'styled-components';

export const Mainheading = styled.h2`
  font-family: 'Quicksand', sans-serif;
  margin: ${props => props.margin || '0px 0px 50px 0px;'};
  font-size: ${props => props.fontSize || '36px'};
  color: #2d3438;
  font-weight: bold;
  line-height: 45px;
  z-index: 1;
  @media screen and (max-width: 767px) {
    font-size: 28px;
  }
  @media screen and (max-width: 767px) {
    // font-size: 24px;
    // line-height: 30px;
    // margin-bottom: 20px;
    font-size: 18px;
    line-height: 1.2;
    margin-bottom: 20px;
  }
`;
