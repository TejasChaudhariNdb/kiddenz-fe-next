import styled from 'styled-components';
import colors from 'utils/colors';
import Flex from '../common/flex';
export const IconWrapper = styled(Flex)`
  & > div {
    @media (max-width: 425px) {
      margin-right: 0px;
      margin-bottom: 20px;
      height: 30px;
      width: 30px;
      .iconify {
        height: 20px;
        width: 20px;
      }
    }
  }
  @media (max-width: 425px) {
    margin: 0px 20px 0px auto;
    padding: 0px;
    flex-direction: column;
  }
  @media (max-width: 380px) {
    margin: 0px 20px 0px auto;
    padding: 0px;
  }
`;
export const TitleName = styled.h1`
  color: ${colors.lightBlack};
  font-family: Quicksand;
  font-size: 32px;
  font-weight: bold;
  line-height: 40px;
  margin: 0px;

  @media (max-width: 425px) {
    font-size: 20px;
    line-height: 20px;
    margin-bottom: 7px;
  }
`;
export const TagLine = styled.div`
  font-family: 'Roboto', sans-serif;
  font-weight: 400;
  font-size: 14px;
  line-height: 25px;
  margin: 4px 0px 10px;
  @media (max-width: 425px) {
    font-size: 14px;
    line-height: 18px;
  }
`;
