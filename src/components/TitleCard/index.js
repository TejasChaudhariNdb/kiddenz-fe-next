import React from 'react';
import PropTypes from 'prop-types';
// import {
//   mdiHeartOutline,
//   // mdiShareVariant
// } from '@mdi/js';
import { mdiHeart } from '@mdi/js';
import IconButton from '../Iconcard';
import BranchRating from '../BranchRating/index';
import { TitleName, IconWrapper, TagLine } from './styled';
import Flex from '../common/flex';

const TitleCard = ({
  title,
  branchName,
  tagline,
  isWishlisted,
  onWishlistIconClick,
  isLoggedIn,
}) => (
  <Flex alignStart>
    <Flex column flexWidth="fit-content" style={{ maxWidth: '70%' }}>
      <TitleName>{title} </TitleName>
      <TagLine>{tagline}</TagLine>
      <BranchRating
        branchname={branchName || 'Koramangala'}
        // starImage={starImage}
        // ratingNum="4.3"
        margin="10px 0px"
      />
    </Flex>
    {isLoggedIn ? (
      <IconWrapper alignEnd flexMargin="0px 0px 0px 50px">
        <IconButton
          iconName={isWishlisted ? mdiHeart : mdiHeart}
          margin="0px 20px 0px 0px"
          isWishlisted={isWishlisted}
          onClick={e => {
            e.stopPropagation();
            onWishlistIconClick(e);
          }}
          inconFill="#fff"
          mdi
        />
        {/* <IconButton iconName="ei:share-google" /> */}
      </IconWrapper>
    ) : null}
  </Flex>
);
TitleCard.propTypes = {
  title: PropTypes.string,
  tagline: PropTypes.string,
  branchName: PropTypes.string,
  onWishlistIconClick: PropTypes.func,
  isWishlisted: PropTypes.bool,
  isLoggedIn: PropTypes.bool,
};
export default TitleCard;
