import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

// import Button from 'components/Button';
// import colors from 'utils/colors';
// import Flex from '../common/flex';

const SearchInput = styled.section`
  position: relative;
  width: ${props => props.width || '401px'};
  margin: ${props => props.margin || '0px 0px 0px 54px;'};

  input {
    width: 100%;
    border: 1px solid #dbdee5;
    border-radius: 5px;
    background-color: #ffffff;
    padding: 16px;
  }
  input::placeholder {
    color: #000;
    font-size: 14px;
    line-height: 17px;
  }
  .iconify {
    position: absolute;
    width: 22px;
    height: 22px;
    color: #4b5256;
    right: 15px;
    top: 14px;
  }
`;

function SearchBar({ margin, width }) {
  return (
    <>
      <SearchInput margin={margin} width={width}>
        <input type="text" placeholder="Search by location or route" />
        <span
          className="iconify"
          data-icon="bytesize:search"
          data-inline="false"
        />
      </SearchInput>
    </>
  );
}

SearchBar.propTypes = {
  margin: PropTypes.string,
  width: PropTypes.string,
};
export default SearchBar;
