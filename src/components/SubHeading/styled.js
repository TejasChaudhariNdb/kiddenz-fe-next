import styled from 'styled-components';
import colors from 'utils/colors';
export const Subheading = styled.h2`
  color: ${colors.lightBlack};
  font-family: 'Quicksand', sans-serif;
  line-height: 23px;
  font-size: ${props => props.fontSize || '18px'};
  margin: ${props => props.margin || '0px'};
  font-weight: 800;

  @media (max-width: 780px) {
    font-size: 18px;
  }

  @media (max-width: 380px) {
    font-size: 16px;
    margin-bottom: 20px;
  }
`;
