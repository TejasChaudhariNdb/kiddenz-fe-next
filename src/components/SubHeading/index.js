import React from 'react';
import PropTypes from 'prop-types';
import { Subheading } from './styled';
const SubHeading = ({ text, marginBottomXs, margin, fontSize, ...rest }) => (
  <Subheading
    marginBottomXs={marginBottomXs}
    margin={margin}
    fontSize={fontSize}
    {...rest}
  >
    {text}
  </Subheading>
);
SubHeading.propTypes = {
  text: PropTypes.string,
  marginBottomXs: PropTypes.string,
  margin: PropTypes.string,
  fontSize: PropTypes.string,
};
export default SubHeading;
