import styled from 'styled-components';
import colors from 'utils/colors';
import { Photo } from '../PhotoCard/styled';
export const TeacherDetails = styled.div`
  width: ${props => props.width || '100%'};
  margin: ${props => props.margin || '0px 15px 0px 0px'};
  &.popup {
    display: flex;
    ${Photo} {
      min-width: 162px;
    }
    .teacherContent {
      width: 85%;
    }
  }
  .teacherContent {
    display: flex;
    width: 100%;
    flex-direction: column;
  }
  &:last-child {
    margin-right: 0px;
    // @media (max-width: 425px) {
    //   margin: 0px auto 30px;
    // }
  }
  .viewAll {
    width: fit-content;
    font-size: 11px;
    border-radius: 0px;
  }
  @media (max-width: 425px) {
    min-width: 55%;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    margin: 0px 20px 30px 0px;
    padding-right: 20px;
    border-right: 1px solid #e6e8ec;

    ${Photo} {
      margin-right: 0px;
      width: 100%;
    }
  }
`;
export const Language = styled.div`
  color: #666c78;
  font-family: Roboto;
  font-size: 12px;
  line-height: 16px;
`;
export const Name = styled.div`
  color: #30333b;
  font-family: Quicksand;
  font-size: 16px;
  font-weight: 500;
  line-height: 20px;
  text-transform: capitalize;
  margin-bottom: 10px;
`;
export const Experience = styled.div`
  color: #30333b;
  font-family: Quicksand;
  font-size: 14px;
  line-height: 18px;
  margin: 10px 0px;
  font-weight: 500;
  span {
    font-weight: 400;
    color: #666c78;
  }
`;
export const Desc = styled.div`
  white-space: pre-line;
  color: #666c78;
  font-family: Roboto;
  font-size: 14px;
  line-height: 18px;
  font-weight: 400;
  span {
    margin-left: 4px;
    font-weight: 500;
    color: ${colors.secondary};
    cursor: pointer;
    &.showLess {
    }
  }
`;
