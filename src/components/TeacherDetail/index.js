/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Button from 'components/Button';
import { TeacherDetails, Language, Name, Experience, Desc } from './styled';
import Flex from '../common/flex';
import PhotoCard from '../PhotoCard';

const TeacherDetail = ({
  image,
  name,
  years,
  desc,
  languages,
  type,
  classType,
  margin,
  width,
  photoWidth = '100%',
}) => {
  const [isShowMore, setShowMore] = useState(false);

  return (
    <TeacherDetails
      className={classType}
      margin={margin}
      width={width}
      photoWidth={photoWidth}
    >
      <PhotoCard image={image} width={photoWidth} height="190px" type={type} />
      <div className="teacherContent">
        <Name>{name}</Name>
        <Button text="TEACHER" type="viewAll" marginRight="10px" />
        {years && (
          <Experience>
            Experience: <span>{years}</span>
          </Experience>
        )}
        <Desc>
          {desc.slice(0, isShowMore ? desc.length : 107)}
          {desc.length > 107 && (
            <>
              {!isShowMore && (
                <span className="showMore" onClick={() => setShowMore(true)}>
                  ... Show more
                </span>
              )}
              {isShowMore && (
                <span className="showLess" onClick={() => setShowMore(false)}>
                  Show less
                </span>
              )}
            </>
          )}
        </Desc>
        {languages && (
          <Flex column flexMargin="10px 0px 0px">
            <Experience>Language</Experience>
            <Language>{languages}</Language>
          </Flex>
        )}
      </div>
    </TeacherDetails>
  );
};
TeacherDetail.propTypes = {
  image: PropTypes.string,
  name: PropTypes.string,
  years: PropTypes.string,
  desc: PropTypes.string,
  languages: PropTypes.string,
  type: PropTypes.string,
  classType: PropTypes.string,
  margin: PropTypes.string,
  width: PropTypes.string,
  photoWidth: PropTypes.string,
};
export default TeacherDetail;
