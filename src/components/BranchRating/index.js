import React from 'react';
import PropTypes from 'prop-types';
import { BranchName, TotalRating } from './styled';
import Flex from '../common/flex';

const BranchRating = ({ branchname, starImage, ratingNum = 0, margin }) => (
  <Flex alignCenter flexMargin={margin}>
    <BranchName isRightBorder={ratingNum}> {branchname}</BranchName>
    {ratingNum && +ratingNum > 0 ? (
      <TotalRating>
        <img src={starImage} alt="" />
        <span>
          {' '}
          {ratingNum && +ratingNum > 0 && parseFloat(ratingNum).toFixed(1)}
        </span>
      </TotalRating>
    ) : null}
  </Flex>
);
BranchRating.propTypes = {
  branchname: PropTypes.string,
  starImage: PropTypes.string,
  ratingNum: PropTypes.string,
  margin: PropTypes.string,
};
export default BranchRating;
