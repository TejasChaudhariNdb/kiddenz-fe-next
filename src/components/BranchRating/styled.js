import styled from 'styled-components';

export const BranchName = styled.div`
  position: relative;
  color: rgba(48, 51, 59, 1);
  font-family: Roboto;
  font-size: 14px;
  line-height: 14px;
  padding-right: 10px;
  margin-right: 10px;
  width: ${props => props.isRightBorder || '1px solid'};
  @media (max-width: 425px) {
    font-size: 13px;
  }
  @media (max-width: 380px) {
    font-size: 12px;
    text-align: center;
  }
`;
export const TotalRating = styled.div`
  display: flex;
  min-width: fit-content;
  span {
    color: rgba(48, 51, 59, 1);
    font-family: Roboto;
    font-size: 14px;
    line-height: 14px;
    margin-left: 5px;
  }
`;
