/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';
import { Vector } from '../Vector';

// Styled components
const BreadcrumbsContainer = styled.div`
  align-items: center;
  display: inline-flex;
  gap: 9px;
  position: relative;
`;

const TextWrapper = styled.div`
  color: #00000080;
  font-family: 'Quicksand', Helvetica;
  font-size: 18px;
  font-weight: 600;
  letter-spacing: 0;
  line-height: normal;
  margin-top: -1px;
  position: relative;
  text-align: center;
  width: fit-content;
`;

const VectorInstance = styled(Vector)`
  left: unset !important;
  position: relative !important;
  top: unset !important;
`;

const Breadcrumbs = ({
  showL2 = true,
  showL3 = true,
  showL4 = true,
  showL5 = true,
  className,
}) => (
  <BreadcrumbsContainer className={`breadcrumbs ${className}`}>
    <TextWrapper>Home</TextWrapper>
    {showL2 && <VectorInstance className="vector-instance" />}
    {showL2 && <TextWrapper>Brand index page</TextWrapper>}
    {showL3 && <VectorInstance className="vector-instance" />}
    {showL3 && <TextWrapper>Eurokids brand page</TextWrapper>}
  </BreadcrumbsContainer>
);

Breadcrumbs.propTypes = {
  showL2: PropTypes.bool,
  showL3: PropTypes.bool,
  showL4: PropTypes.bool,
  showL5: PropTypes.bool,
};

export default Breadcrumbs;
