import { Breadcrumbs } from '.';

export default {
  title: 'Components/Breadcrumbs',
  component: Breadcrumbs,
};

export const Default = {
  args: {
    showL2: true,
    showL3: true,
    showL4: true,
    showL5: true,
    className: {},
  },
};
