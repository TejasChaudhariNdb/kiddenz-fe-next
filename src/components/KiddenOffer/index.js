import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const KiddenzOffer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: ${props => props.width || '28%'};

  @media screen and (max-width: 1100px) {
    width: 34%;
  }
  &:first-child,
  &:last-child {
    @media screen and (max-width: 1100px) {
      width: 25%;
    }
  }
  @media screen and (max-width: 900px) {
    width: 36%;
  }
  &:first-child,
  &:last-child {
    @media screen and (max-width: 900px) {
      width: 27%;
    }
  }
  @media screen and (max-width: 767px) {
    width: 100%;
    margin-bottom: 30px;
    max-width: 300px;
  }
  @media screen and (max-width: 500px) {
    margin-bottom: 10px;
    width: 100% !important;
  }
  @media screen and (max-width: 380px) {
    margin-bottom: 0px;
  }
`;

const KiddenImage = styled.div`
  width: 250px;
  height: 250px;
  overflow: hidden;
  img {
    transform: scale(0.9);
    width: 100%;
    height: 100%;
  }

  @media screen and (max-width: 500px) {
    width: 240px;
    height: 240px;
    img {
      transform: scale(1);
    }
  }
`;

const KiddenText = styled.h3`
  font-family: 'Quicksand', sans-serif;
  margin: ${props => props.textmargin || '0px 0px 22px 0px;'};
  color: #30333b;
  font-size: 24px;
  font-weight: bold;
  line-height: 30px;
  text-align: center;
  @media screen and (max-width: 900px) {
    font-size: 20px;
    margin-bottom: 10px;
  }
  @media screen and (max-width: 500px) {
    font-size: 16px;
    line-height: 24px;
  }
`;

const KiddenDesc = styled.div`
  font-family: 'Roboto', sans-serif;
  color: #666c78;
  font-family: Roboto;
  font-size: 18px;
  line-height: 27px;
  text-align: center;
  padding: 0px 7px;
  @media screen and (max-width: 900px) {
    font-size: 14px;
  }
  @media screen and (max-width: 500px) {
    font-size: 14px;
    line-height: 22px;
  }
`;
const KiddenOffer = ({ text, image, desc, width, subText }) => (
  <KiddenzOffer width={width}>
    <KiddenImage>
      <img src={image} alt="offerImage" />
    </KiddenImage>
    <KiddenText>
      {text}
      <span className="liteFont">{subText}</span>
    </KiddenText>
    <KiddenDesc>{desc}</KiddenDesc>
  </KiddenzOffer>
);
KiddenOffer.propTypes = {
  text: PropTypes.string,
  image: PropTypes.string,
  desc: PropTypes.string,
  width: PropTypes.string,
  subText: PropTypes.string,
};
export default KiddenOffer;
