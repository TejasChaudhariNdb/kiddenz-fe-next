import styled from 'styled-components';
export const LocationMap = styled.div`
  position: relative;
  height: 350px;
  width: 100%;
  border-radius: 10px;
`;
export const ContactUs = styled.div`
  position: absolute;
  bottom: 20px;
  left: 20px;
  right: 20px;
  padding: 20px;
  height: auto;
  border-radius: 10px;
  background-color: #ffffff;
  font-weight: 300;
  box-shadow: 2px 2px 20px 0 rgba(74, 67, 67, 0.1);
`;
export const Address = styled.div`
  color: #30333b;
  font-family: 'Roboto', sans-serif;
  font-size: 16px;
  line-height: 24px;
`;
export const Email = styled.div`
  color: #3b74e7;
  font-family: 'Roboto', sans-serif;
  font-size: 16px;
  line-height: 24px;
`;
