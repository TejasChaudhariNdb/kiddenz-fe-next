import React from 'react';
import PropTypes from 'prop-types';
import { GoogleMap, Marker } from '@react-google-maps/api';
import { MAPS_API_KEY } from 'utils/google';
import markerIcon from '../../images/mapMarker.svg';
import { LocationMap } from './styled';

const Location = ({ latitude, longitude }) => (
  <LocationMap>
    <Map
      googleMapURL={`https://maps.googleapis.com/maps/api/js?key=${MAPS_API_KEY}&v=3.exp&libraries=geometry,drawing,places`}
      loadingElement={<div style={{ height: `100%` }} />}
      containerElement={<div style={{ height: `100%` }} />}
      mapElement={<div style={{ height: `100%` }} />}
      latitude={latitude}
      longitude={longitude}
    />
    {/* <ContactUs>
      <Address>{address}</Address>
      <Address>{number}</Address>
      <Email>{email}</Email>
    </ContactUs> */}
  </LocationMap>
);

Location.propTypes = {
  latitude: PropTypes.number,
  longitude: PropTypes.number,
  // address: PropTypes.string,
  // number: PropTypes.string,
  // email: PropTypes.string,
};

export default Location;

const Map = ({ latitude, longitude }) => {
  const renderMap = () => {
    const options = {
      streetViewControl: false,
      fullscreenControl: false,
      mapTypeControl: false,
      zoomControl: false,
    };
    return (
      <GoogleMap
        options={options}
        mapContainerStyle={{
          height: '350px',
          width: '100%',
          border: '0px solid #613A95',
        }}
        zoom={13}
        center={{
          lat: +latitude,
          lng: +longitude,
        }}
      >
        <Marker
          position={{ lat: +latitude, lng: +longitude }}
          icon={markerIcon}
        />
      </GoogleMap>
    );
  };

  return renderMap();
};

Map.propTypes = {
  latitude: PropTypes.number,
  longitude: PropTypes.number,
};
