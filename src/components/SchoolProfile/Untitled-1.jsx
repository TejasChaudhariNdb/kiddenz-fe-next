/* eslint-disable */
/* eslint-disable no-unused-expressions */
/* eslint-disable react/prop-types */
// eslint-disable-next-line react/no-unescaped-entities
import React, { useEffect, useState } from 'react';
import { withRouter } from 'react-router';
import groupBy from 'lodash/groupBy';
import axios from 'config/axios';
import styled from 'styled-components';
// import { Pano } from 'react-vr';

// import PropTypes from 'prop-types';
// import { mdiHeartOutline, mdiShareVariant } from '@mdi/js';
import parent5 from 'images/user5.svg';
import Footer from 'components/Footer';
import Button from 'components/Button';
import Header from 'components/Header';
import moment from 'moment';
import history from 'utils/history';
import MainWrapper from 'components/MainWrapper';
import ImageBanner from 'components/ImageBanner/index';
import bannerImage from 'images/school-banner.svg';
import jumboImage from 'images/JumboLogo.png';
import fullscreen from 'images/fullscreen.png';
import exitFullscreen from 'images/exit-fullscreen.png';
import TitleCard from 'components/TitleCard';
import Modal from 'components/Modal';
import { useWishlistHooks } from 'shared/hooks';
import Flex from 'components/common/flex';
import qs from 'querystring';
import ProfileDetails from 'components/ProfileDetails/index';
import PhotoCard from 'components/PhotoCard/index';
import Location from 'components/LocationMap/index';
import SubHeading from 'components/SubHeading/index';
import Syllabus from 'components/SyllabusCard';
import Testimonial from 'components/TestimonialCard/index';
import ParentReview from 'components/ParentReview/index';
import TeacherDetail from 'components/TeacherDetail/index';
import SchoolCard from 'components/SchoolCard';
import image1 from '../../../images/g1.svg';
import image2 from '../../../images/g2.svg';
import image3 from '../../../images/g3.svg';
import image4 from '../../../images/g4.svg';
import image5 from '../../../images/g5.svg';
import locationImage from '../../../images/queensroad.png';
import teacherImage from '../../../images/teacher2.svg';
import parent1 from '../../../images/useer1.svg';
import parent2 from '../../../images/user2.svg';
import parent3 from '../../../images/user3.svg';
import parent4 from '../../../images/user4.svg';
import facilityIcon from '../../../images/CCTV.png';
import schoolImage1 from '../../../images/monterssori.svg';
import schoolImage2 from '../../../images/elle.png';
import schoolImage3 from '../../../images/jumbo.png';
import VirtualTour from '../../../images/360_view.jpg';
import VirtualTourPlaceholder from '../../../images/360_placeholder.jpg';
import schoolImage4 from '../../../images/tiny.png';
import loader from '../../../images/mascot.gif';
import schoolImage5 from '../../../images/oi.png';
import star3 from '../../../images/staar_3.png';
import Verfied from 'components/Verfied';
import year from '../../../images/Artboard 16.png';
import join from '../../../images/Artboard 19 (2).png';
import capacity from '../../../images/capacity.png';
import license from '../../../images/license number (4).png';
import achieve1 from '../../../images/Group 4.svg';
import achieve2 from '../../../images/Group 12.svg';
import coverPicPlaceholder from '../../../images/coverPicPlaceholder.jpg';
import Profile from '../../../images/profile.jpg';
import testImg from '../../../images/test_img.jpg';

import {
  ProfileSection,
  ProfileGrid,
  AdditionalInfo,
  Schedule,
  ParaGraph,
  Facility,
  BranchName,
  FacilitiesItem,
  GalleryWrapper,
  TeacherWrapper,
  Name,
  Text,
  Email,
} from './styled';

const UNWANTED_FACILITIES = [
  'id',
  'provider',
  'languages_spoken',
  'emergency_details',
  'restrictions',
  'emergency_options',
  'extended_time',
  'parent_teacher_app_name',
  'courses',
  'custom_course_text',
];

const Model = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  height: 100vh;
  width: 100%;
`;

const MAJOR_FACILITIES = [
  'indoor_play_area',
  'outdoor_play_area',
  'provide_food_to_day_care',
  'transportation',
  'child_proof',
  'emergency_details',
  'emergency_options',
  'cctv',
  'live_streaming',
  'parent_teacher_app',
  'ac_rooms',
];

const MAJOR_FACILITIES_ICONS = {
  indoor_play_area: 'bx:bxs-tennis-ball',
  outdoor_play_area: 'ic-baseline-outdoor-grill',
  provide_food_to_day_care: 'emojione-monotone:pot-of-food',
  transportation: 'ic-round-emoji-transportation',
  child_proof: 'mdi-account-child',
  emergency_options: 'whh-emergency',
  cctv: 'ant-design:video-camera-outline',
  live_streaming: 'ic-baseline-live-tv',
  parent_teacher_app: 'raphael-parent',
  ac_rooms: 'ic-baseline-ac-unit',
};

const Home = props => {
  const [schoolname, setSchoolName] = useState('');
  const [schoolbranch, setSchoolBranch] = useState('');
  const [active, setActive] = useState(false);
  const [mediaType, setMediaType] = useState(null);
  const [type, setType] = useState('');
  const [isFullScreen, setIsFullScreen] = useState(false);

  // if (history.location.search) {
  const { id = null } = qs.parse(history.location.search.slice(1));
  // }
  if (id) axios.defaults.baseURL = 'https://adminbe.kiddenz.com/api/providers/'; //prod
  // 'http://13.235.13.205/api/providers/'; // staging

  //https://kzadminapi.cartoonmango.com/api/providers/
  console.log(id, 'id One');
  const {
    location: { data: locationData = [], loader: locationLoader },
    provider: { data: providerData = {}, loader: providerLoader },
    yearlyCalender: {
      data: yearlyCalenderData = [],
      loader: yearlyCalenderLoader,
    },
    scheduleForDay: {
      data: scheduleForDayData = [],
      loader: scheduleForDayLoader,
    },
    workingDay: { data: workingDayData = [], loader: workingDayLoader },
    facilities: { data: facilitiesData = [], loader: facilitiesLoader },
    faculty: { data: facultyData = [], loader: facultyLoader },
    language: { data: languageData = [], loader: languageLoader },
    contact: { data: contactData = [], loader: contactLoader },
    media: { data: mediaData = [], loader: mediaLoader },
    basicInfo: { data: basicInfoData = [], loader: basicInfoLoader },
    emergecyServices: {
      data: emergecyServicesData = [],
      loader: emergecyServicesLoader,
    },
    scheduleChoice: {
      data: scheduleChoiceData = [],
      loader: scheduleChoiceLoader,
    },
  } = useWishlistHooks(props, { id });

  const { safe } = props;

  const mediaImages = mediaData.filter(
    d => d.media_type !== 'logo' && d.media_type !== 'cover_pic',
  );

  const includedMajorFacilities = [[], []];

  Object.entries(safe(facilitiesData, '[0]', {}))
    .filter(([key, value]) =>
      MAJOR_FACILITIES.includes(key) && key !== 'emergency_details'
        ? value
        : false,
    )
    .map(([key], i) => {
      if (i !== 4) {
        includedMajorFacilities[0].push(key);
      } else {
        includedMajorFacilities[1].push(key);
      }
    });

  const routerWillLeave = nextState => {
    // return false to block navigation, true to allow
    if (nextState.action === 'POP') {
      // handle "Back" button clicks here
    }
  };

  useEffect(() => {
    window.addEventListener('popstate', e => {
      // Nope, go back to your page
      console.log('object');
      props.history.go(1);
    });

    return () => {};
  }, []);

  useEffect(() => {
    if (active) {
      document.body.style.overflow = 'hidden';
    } else {
      document.body.style.overflow = 'unset';
    }
  }, [active]);

  const onSubmitSuccess = () => {
    setActive(true);
    setType('approval');
    console.log('hi');
  };

  const onDecline = () => {
    setActive(false);
  };
  const onAccept = () => {
    setActive(false);
    // // PROD1
    window.location.href =
      'https://caretaker.kiddenz.com/#/profile?page=success';

    // PROD2
    // window.location.href =
    // 'https://kiddenzdc.cartoonmango.com/#/profile?page=success';

    // DEV
    // window.location.href =
    //   'http://kiddenz-admin.s3-website.ap-south-1.amazonaws.com/#/profile?page=success';
    // ('https://kiddenzdc.cartoonmango.com/#/profile?page=success');
  };

  const dayCareData = workingDayData
    .filter(d => d.school_type === 'day_care')
    .filter(
      d =>
        d.weekdays === 'mon_fri' ||
        d.weekdays === 'sun' ||
        d.weekdays === 'sat',
    );

  const preschoolData = Object.keys(
    groupBy(
      workingDayData.filter(
        d =>
          d.school_type === 'preschool' &&
          (d.weekdays === 'mon_fri' ||
            d.weekdays === 'sun' ||
            d.weekdays === 'sat') &&
          d.batch_slug_name,
      ),
      o => o.batch_slug_name,
    ),
  )
    .map(function(a) {
      return { key: a, value: +a.split('batch')[1] };
    })
    .sort(function(a, b) {
      return a.value - b.value;
    })
    .map(function(a) {
      return a.key;
    });

  const pararomicMedia = mediaImages.filter(d => d.media_type === 'photo360');

  // safe(yearlyCalenderData, '[0].year', '')
  const dayCareCalenderData = yearlyCalenderData.filter(
    d => d.school_type && d.school_type === 'day_care',
  );

  const preschoolCalenderData = yearlyCalenderData.filter(
    d => d.school_type && d.school_type === 'preschool',
  );

  const image = `https://cdn.pannellum.org/2.5/pannellum.htm#panorama=${safe(
    mediaImages.filter(d => d.media_type === 'photo360'),
    '[0].media_url',
  )}&autoLoad=true`;

  // `https://cdn.pannellum.org/2.5/pannellum.htm#panorama=${safe(
  //   mediaImages.filter(d => d.media_type === 'photo360'),
  //   '[0].media_url',
  // )}&amp;amp;autoLoad=true`;

  // const test = () => {
  //   return {
  //     __html: `<iframe
  //   width="600"
  //   height="400"
  //   allowfullscreen
  //   style="border-style:none;"
  //   src=${image}
  // />`,
  //   };
  // };

  const iframeSrc = () => {
    if (
      safe(pararomicMedia, '[0]', {}).media_url &&
      safe(pararomicMedia, '[0]', {}).media_url.includes("'data': ")
    ) {
      if (
        safe(pararomicMedia, '[0]', {})
          .media_url.split("'data': '")[1]
          .split("'}")[0] &&
        safe(pararomicMedia, '[0]', {})
          .media_url.split("'data': '")[1]
          .split("'}")[0]
          .includes('index.htm')
      ) {
        return safe(pararomicMedia, '[0]', {})
          .media_url.split("'data': '")[1]
          .split("'}")[0];
      } else {
        return 'https://virtualtour.kiddenz.com/dc-90-ECC0F5E26B/index.html';
      }
    } else {
      if (
        safe(pararomicMedia, '[0]', {}).media_url &&
        safe(pararomicMedia, '[0]', {}).media_url.includes('index.htm')
      ) {
        return safe(pararomicMedia, '[0]', {}).media_url;
      } else {
        return 'https://virtualtour.kiddenz.com/dc-90-ECC0F5E26B/index.html';
      }
    }
  };

  return (
    <>
      {!locationLoader &&
      !providerLoader &&
      !yearlyCalenderLoader &&
      !scheduleForDayLoader &&
      !workingDayLoader &&
      !facilitiesLoader &&
      !facultyLoader &&
      !languageLoader &&
      !contactLoader &&
      !mediaLoader &&
      !scheduleChoiceLoader &&
      !emergecyServicesLoader &&
      !basicInfoLoader ? (
        !isFullScreen && (
          <>
            <MainWrapper background="#fff">
              <>
                <span
                  // id="myEnterVRButton"
                  className="iconify"
                  data-icon="si-glyph:screen-scale"
                  data-inline="false"
                  style={{
                    position: 'absolute',
                    // top: '10px',
                    // left: '13px',
                    color: 'yellow',
                    fontSize: '22px',
                  }}
                />
                <ImageBanner
                  imgpath={
                    safe(
                      mediaData.filter(d => d.media_type === 'cover_pic'),
                      '[0].media_url',
                    )
                      ? safe(
                          mediaData.filter(d => d.media_type === 'cover_pic'),
                          '[0].media_url',
                        )
                      : coverPicPlaceholder
                  }
                  jumboImage={
                    safe(
                      mediaData.filter(d => d.media_type === 'logo'),
                      '[0].media_url',
                    )
                      ? safe(
                          mediaData.filter(d => d.media_type === 'logo'),
                          '[0].media_url',
                        )
                      : Profile
                  }
                />
                <ProfileSection>
                  <Flex column flexWidth="70%">
                    <Verfied name="Verified" />
                    <TitleCard
                      title={providerData.business_name || ''}
                      branchName={safe(locationData, '[0].area')}
                      tagline={safe(basicInfoData, '[0].tag_line') || ''}
                    />
                    <Flex wrap flexWidth="100%">
                      <ProfileDetails
                        heading="ORGANISATION"
                        data1={'Play School'}
                        data2={'Day Care'}
                        margin="0px 50px 0px 0px"
                        isSingle={
                          (providerData.child_care_providers === '1' ||
                            providerData.child_care_providers === '3') &&
                          (providerData.child_care_providers === '2' ||
                            providerData.child_care_providers === '3')
                        }
                        isPreschool={
                          providerData.child_care_providers === '1' ||
                          providerData.child_care_providers === '3'
                        }
                        isDaycare={
                          providerData.child_care_providers === '2' ||
                          providerData.child_care_providers === '3'
                        }
                      />
                      <ProfileDetails
                        isPreschool={
                          providerData.child_care_providers === '1' ||
                          providerData.child_care_providers === '3'
                        }
                        isDaycare={
                          providerData.child_care_providers === '2' ||
                          providerData.child_care_providers === '3'
                        }
                        isSingle={
                          (providerData.child_care_providers === '1' ||
                            providerData.child_care_providers === '3') &&
                          (providerData.child_care_providers === '2' ||
                            providerData.child_care_providers === '3')
                        }
                        heading="AGE GROUP"
                        data1={
                          providerData.child_care_providers === '1' ||
                          providerData.child_care_providers === '3'
                            ? `${Math.floor(
                                safe(
                                  basicInfoData,
                                  '[0].preschool_children_min_age',
                                ) / 12,
                              )}yrs ${safe(
                                basicInfoData,
                                '[0].preschool_children_min_age',
                              ) % 12}m - ${Math.floor(
                                safe(
                                  basicInfoData,
                                  '[0].preschool_children_max_age',
                                ) / 12,
                              )}yrs ${safe(
                                basicInfoData,
                                '[0].preschool_children_max_age',
                              ) % 12}m`
                            : '---'
                        }
                        data2={
                          providerData.child_care_providers === '2' ||
                          providerData.child_care_providers === '3'
                            ? `${Math.floor(
                                safe(basicInfoData, '[0].children_min_age') /
                                  12,
                              )}yrs ${safe(
                                basicInfoData,
                                '[0].children_min_age',
                              ) % 12}m - ${Math.floor(
                                safe(basicInfoData, '[0].children_max_age') /
                                  12,
                              )}yrs ${safe(
                                basicInfoData,
                                '[0].children_max_age',
                              ) % 12}m`
                            : '---'
                        }
                        margin="0px 50px 0px 0px"
                      />

                      <ProfileDetails
                        isPreschool={
                          providerData.child_care_providers === '1' ||
                          providerData.child_care_providers === '3'
                        }
                        isDaycare={
                          providerData.child_care_providers === '2' ||
                          providerData.child_care_providers === '3'
                        }
                        isSingle={
                          (providerData.child_care_providers === '1' ||
                            providerData.child_care_providers === '3') &&
                          (providerData.child_care_providers === '2' ||
                            providerData.child_care_providers === '3')
                        }
                        heading="TIMING"
                        data1={
                          (providerData.child_care_providers === '1' ||
                            providerData.child_care_providers === '3') &&
                          workingDayData.filter(
                            d => d.school_type === 'preschool',
                          ).length > 0
                            ? `Mon - Fri | ${moment(
                                safe(
                                  workingDayData.filter(
                                    d => d.school_type === 'preschool',
                                  ),
                                  '[0].from_hour',
                                ),
                                'hh:mm:ss',
                              ).format('hh:mm A')} -
                    ${moment(
                      safe(
                        workingDayData.filter(
                          d => d.school_type === 'preschool',
                        ),
                        '[0].to_hour',
                      ),
                      'hh:mm:ss',
                    ).format('hh:mm A')} `
                            : '---'
                        }
                        data2={
                          (providerData.child_care_providers === '2' ||
                            providerData.child_care_providers === '3') &&
                          workingDayData.filter(
                            d => d.school_type === 'day_care',
                          ).length > 0
                            ? ` Mon - Fri | ${moment(
                                safe(
                                  workingDayData.filter(
                                    d => d.school_type === 'day_care',
                                  ),
                                  '[0].from_hour',
                                ),
                                'hh:mm:ss',
                              ).format('hh:mm A')} -
                    ${moment(
                      safe(
                        workingDayData.filter(
                          d => d.school_type === 'day_care',
                        ),
                        '[0].to_hour',
                      ),
                      'hh:mm:ss',
                    ).format('hh:mm A')}`
                            : '---'
                        }
                        margin="0px 50px 0px 0px"
                      />

                      <ProfileDetails
                        isPreschool={
                          providerData.child_care_providers === '1' ||
                          providerData.child_care_providers === '3'
                        }
                        isDaycare={
                          providerData.child_care_providers === '2' ||
                          providerData.child_care_providers === '3'
                        }
                        isSingle={
                          (providerData.child_care_providers === '1' ||
                            providerData.child_care_providers === '3') &&
                          (providerData.child_care_providers === '2' ||
                            providerData.child_care_providers === '3')
                        }
                        margin="0px"
                        data1={
                          providerData.child_care_providers === '1' ||
                          providerData.child_care_providers === '3'
                            ? basicInfoData &&
                              basicInfoData[0] &&
                              basicInfoData[0].preschool_schedule.length > 0
                              ? scheduleChoiceData
                                  .map(
                                    d =>
                                      (
                                        safe(
                                          basicInfoData,
                                          '[0].preschool_schedule',
                                        ) || []
                                      ).includes(d.id) && d.name,
                                  )
                                  .filter(d => d)
                                  .toString()
                                  .split(',')
                                  .join('/')
                              : '---'
                            : '---'
                        }
                        data2={
                          providerData.child_care_providers === '2' ||
                          providerData.child_care_providers === '3'
                            ? basicInfoData &&
                              basicInfoData[0] &&
                              basicInfoData[0].schedule.length > 0
                              ? scheduleChoiceData
                                  .map(
                                    d =>
                                      (
                                        safe(basicInfoData, '[0].schedule') ||
                                        []
                                      ).includes(d.id) && d.name,
                                  )
                                  .filter(d => d)
                                  .toString()
                                  .split(',')
                                  .join('/')
                              : '---'
                            : '---'
                        }
                        heading="SCHEDULE"
                      />
                    </Flex>
                    <Flex wrap flexWidth="100%" className="enquiryButton">
                      <Button
                        text="Schedule Tour"
                        type="profile"
                        marginRight="10px"
                        marginTop="10px"
                        onClick={() => {
                          setType('tour');
                          setActive(true);
                        }}
                      />
                      <Button
                        text="Enquire"
                        type="profile"
                        marginRight="10px"
                        marginTop="10px"
                        onClick={() => {
                          setType('email');
                          setActive(true);
                        }}
                      />
                      <Button
                        text="Connect with Counselor"
                        type="profile"
                        marginRight="10px"
                        marginTop="10px"
                        onClick={() => {
                          setType('confirm message');
                          setActive(true);
                        }}
                      />
                      <Button
                        text="Connect with Parent"
                        type="profile"
                        marginRight="10px"
                        marginTop="10px"
                        onClick={() => {
                          setType('enquiry');
                          setActive(true);
                        }}
                      />
                      {/* <Button
              text="Thank u"
              type="profile"
              marginRight="10px"
              onClick={() =>{ setType('thankyou'); setActive(true);}}
            /> */}
                    </Flex>
                  </Flex>
                  {/* TEMP */}
                  {/* {safe(
                mediaImages.filter(d => d.media_type === 'photo360'),
                '[0].media_url',
              ) ? (
                <Flex column flexWidth="30%">
                  <SubHeading text="Virtual Tour" margin="0px 0px 20px" />
                  <div
                    id="myEmbeddedScene"
                    style={{
                      display: 'flex',
                    }}
                    dangerouslySetInnerHTML={{
                      __html: `<iframe
                    width="450"
                    height="220"
                    allowfullscreen
                    style="border-style:none;"
                    // src=${image}
                    src='http://kiddenz-360.s3-website.ap-south-1.amazonaws.com/'
                  />`,
                    }}
                  />
                </Flex>
              ) : (
                <Flex column flexWidth="30%">
                  <SubHeading text="Virtual Tour" margin="0px 0px 20px" />
                  <PhotoCard
                    width="100%"
                    height="220px"
                    type="virtual"
                    image={VirtualTourPlaceholder}
                  />
                </Flex>
              )} */}
                  <Flex column flexWidth="30%">
                    <SubHeading text="Virtual Tour" margin="0px 0px 20px" />
                    <button
                      style={{
                        width: '30px',
                        height: '30px',
                        position: 'relative',
                        top: '40px',
                        left: '10px',
                      }}
                      onClick={() => setIsFullScreen(true)}
                    >
                      <img
                        style={{
                          height: '15px',
                          width: '15px',
                          position: 'relative',
                          top: '-1px',
                        }}
                        src={fullscreen}
                        alt=""
                      />
                    </button>
                    <iframe
                      width="340"
                      height="220"
                      allowfullscreen
                      style={{ borderStyle: 'none' }}
                      src={iframeSrc()}
                    />
                  </Flex>
                </ProfileSection>
                <ProfileGrid>
                  <div className="gridColumn">
                    <div className="gridItem">
                      <SubHeading text="Additional Information" />
                      <Flex column flexMargin="20px 0px">
                        <AdditionalInfo>
                          <img src={year} alt="" />
                          Year of Establishment :
                          <span>
                            {' '}
                            {safe(basicInfoData, '[0].year_of_establishment')}
                          </span>
                        </AdditionalInfo>
                        {providerData.license_number && (
                          <AdditionalInfo>
                            <img src={license} alt="" />
                            License Number :
                            <span> {providerData.license_number}</span>
                            <span
                              class="iconify license"
                              data-icon="ic:outline-verified-user"
                              data-inline="false"
                            />
                          </AdditionalInfo>
                        )}
                        {/* TODO: TEMPORARY */}
                        {/* <AdditionalInfo>
                      <img src={join} alt="" />
                      Joined Kiddenz on:<span> 28th Nov 2019</span>
                    </AdditionalInfo> */}
                        {(providerData.child_care_providers === '2' ||
                          providerData.child_care_providers === '3') && (
                          <AdditionalInfo>
                            <img src={capacity} alt="" />
                            Day Care Capacity :
                            <span>
                              {' '}
                              {providerData.child_care_providers === '2' ||
                              providerData.child_care_providers === '3'
                                ? safe(
                                    basicInfoData,
                                    '[0].day_care_total_capacity',
                                  )
                                : ''}
                            </span>
                            <span
                              class="iconify license"
                              data-icon="ic:outline-verified-user"
                              data-inline="false"
                            />
                          </AdditionalInfo>
                        )}
                        {(providerData.child_care_providers === '1' ||
                          providerData.child_care_providers === '3') && (
                          <AdditionalInfo>
                            <img src={capacity} alt="" />
                            Preschool Capacity{' : '}
                            <span>
                              {' '}
                              {providerData.child_care_providers === '1' ||
                              providerData.child_care_providers === '3'
                                ? safe(
                                    basicInfoData,
                                    '[0].preschool_total_capacity',
                                  )
                                : ''}
                            </span>
                          </AdditionalInfo>
                        )}
                      </Flex>
                    </div>
                    {safe(basicInfoData, '[0].achievement', '') && (
                      <div className="gridItem">
                        <SubHeading text="Achievements" />
                        <Flex column flexMargin="30px 0px">
                          {safe(basicInfoData, '[0].achievement', '')
                            .split(',')
                            .map(d => (
                              <Schedule>
                                <img src={achieve1} alt="" />
                                <span>{d}</span>
                              </Schedule>
                            ))}
                        </Flex>
                      </div>
                    )}

                    <div className="gridItem">
                      <SubHeading text="Syllabus and activities" />
                      <Flex column flexMargin="30px 0px">
                        {safe(basicInfoData, '[0].activity', '') &&
                        safe(basicInfoData, '[0].activity', '').split(',')
                          .length > 0 ? (
                          safe(basicInfoData, '[0].activity', '')
                            .split(',')
                            .map(d => <Facility>{d}</Facility>)
                        ) : (
                          <Facility>
                            {safe(basicInfoData, '[0].activity', '')}
                          </Facility>
                        )}
                        <Flex justifyEnd>
                          <Button text="Show more" type="viewAll" />
                        </Flex>
                        <Flex justifyEnd>
                          <Button text="Show less" type="viewAll" />
                        </Flex>
                      </Flex>
                    </div>
                    {safe(facilitiesData, '[0].courses', '') === 'custom' && (
                      <>
                        <div className="gridItem">
                          <SubHeading text="Courses" />
                          <Flex column flexMargin="30px 0px">
                            {safe(
                              facilitiesData,
                              '[0].custom_course_text',
                              '',
                            ) &&
                            safe(
                              facilitiesData,
                              '[0].custom_course_text',
                              '',
                            ).split(',').length > 0 ? (
                              safe(facilitiesData, '[0].custom_course_text', '')
                                .split(',')
                                .map(d => <Facility>{d}</Facility>)
                            ) : (
                              <Facility>
                                {safe(
                                  facilitiesData,
                                  '[0].custom_course_text',
                                  '',
                                )}
                              </Facility>
                            )}
                            <Flex justifyEnd>
                              <Button text="Show more" type="viewAll" />
                            </Flex>
                            <Flex justifyEnd>
                              <Button text="Show less" type="viewAll" />
                            </Flex>
                          </Flex>
                        </div>
                      </>
                    )}

                    {safe(facilitiesData, '[0].courses', '') !== 'custom' && (
                      <>
                        <div className="gridItem">
                          <SubHeading text="Courses" />
                          <Flex column flexMargin="30px 0px">
                            <Facility className="provide">
                              {safe(facilitiesData, '[0].courses', '')}
                            </Facility>
                          </Flex>
                        </div>
                      </>
                    )}

                    {scheduleForDayData.sort((a, b) => {
                      const first = +a.start_time.split(':')[0];
                      const second = +b.start_time.split(':')[0];
                      return first - second;
                    }).length > 0 && (
                      <div className="gridItem">
                        <SubHeading text="Schedule for the day" />
                        <Flex column flexMargin="30px 0px">
                          {scheduleForDayData
                            .sort((a, b) => {
                              const first = +a.start_time.split(':')[0];
                              const second = +b.start_time.split(':')[0];
                              return first - second;
                            })
                            .map(d => (
                              <Schedule>
                                <span>
                                  {moment(d.start_time, 'hh:mm:ss').format(
                                    'hh:mm A',
                                  )}
                                </span>
                                <span>{d.programm}</span>
                              </Schedule>
                            ))}
                          <Flex justifyEnd>
                            <Button text="Show more" type="viewAll" />
                          </Flex>
                          <Flex justifyEnd>
                            <Button text="Show less" type="viewAll" />
                          </Flex>
                        </Flex>
                      </div>
                    )}
                    {(dayCareCalenderData.length > 0 ||
                      preschoolCalenderData.length > 0) && (
                      <>
                        {(providerData.child_care_providers === '2' ||
                          providerData.child_care_providers === '3') &&
                          dayCareCalenderData.length > 0 && (
                            <>
                              <div className="gridItem">
                                <SubHeading
                                  text={`Day Care Calendar (${
                                    dayCareCalenderData[0].year
                                  })`}
                                />
                                <Flex column flexMargin="30px 0px">
                                  {dayCareCalenderData
                                    .sort((a, b) => {
                                      const first = new Date(
                                        a.start_date,
                                      ).getTime();
                                      const second = new Date(
                                        b.start_date,
                                      ).getTime();
                                      console.log(
                                        new Date(a.start_date).getTime(),
                                      );
                                      return first - second;
                                    })
                                    .map(d => (
                                      <Schedule className="calendar">
                                        {console.log(
                                          d.start_date === d.to_date,
                                          'd.start_date === d.to_date',
                                        )}
                                        <span>
                                          {d.start_date === d.to_date
                                            ? moment(d.start_date).format(
                                                'MMM DD',
                                              )
                                            : `${moment(d.start_date).format(
                                                'MMM DD',
                                              )}
                                        - ${moment(d.to_date).format(
                                          'MMM DD',
                                        )}`}
                                        </span>
                                        <span>{d.event}</span>
                                      </Schedule>
                                    ))}
                                  <Flex justifyEnd>
                                    <Button text="Show more" type="viewAll" />
                                  </Flex>
                                  <Flex justifyEnd>
                                    <Button text="Show less" type="viewAll" />
                                  </Flex>
                                </Flex>
                              </div>
                            </>
                          )}
                        {(providerData.child_care_providers === '1' ||
                          providerData.child_care_providers === '3') &&
                          preschoolCalenderData.length > 0 && (
                            <>
                              <div className="gridItem">
                                <SubHeading
                                  text={`Preschool Calendar (${
                                    preschoolCalenderData[0].year
                                  })`}
                                />
                                <Flex column flexMargin="30px 0px">
                                  {preschoolCalenderData
                                    .sort((a, b) => {
                                      const first = new Date(
                                        a.start_date,
                                      ).getTime();
                                      const second = new Date(
                                        b.start_date,
                                      ).getTime();
                                      console.log(
                                        new Date(a.start_date).getTime(),
                                      );
                                      return first - second;
                                    })
                                    .map(d => (
                                      <Schedule className="calendar">
                                        <span>
                                          {d.start_date === d.to_date
                                            ? moment(d.start_date).format(
                                                'MMM DD',
                                              )
                                            : `${moment(d.start_date).format(
                                                'MMM DD',
                                              )}
                                        - ${moment(d.to_date).format(
                                          'MMM DD',
                                        )}`}
                                        </span>
                                        <span>{d.event}</span>
                                      </Schedule>
                                    ))}
                                  <Flex justifyEnd>
                                    <Button text="Show more" type="viewAll" />
                                  </Flex>
                                  <Flex justifyEnd>
                                    <Button text="Show less" type="viewAll" />
                                  </Flex>
                                </Flex>
                              </div>
                            </>
                          )}
                      </>
                    )}
                    {(dayCareData.length > 0 || preschoolData.length > 0) && (
                      <div className="gridItem">
                        {(providerData.child_care_providers === '2' ||
                          providerData.child_care_providers === '3') &&
                          dayCareData.length > 0 && (
                            <>
                              <SubHeading text="Day Care Working days" />
                              <Flex column flexMargin="30px 0px">
                                {dayCareData.map(d => (
                                  <Schedule>
                                    <span>
                                      {(d.weekdays || '').split('_').join('-')}
                                    </span>
                                    <span>
                                      {moment(d.from_hour, 'hh:mm:ss').format(
                                        'hh:mm A',
                                      )}{' '}
                                      -{' '}
                                      {moment(d.to_hour, 'hh:mm:ss').format(
                                        'hh:mm A',
                                      )}
                                    </span>
                                  </Schedule>
                                ))}
                              </Flex>
                            </>
                          )}
                        {(providerData.child_care_providers === '1' ||
                          providerData.child_care_providers === '3') &&
                          preschoolData.length > 0 && (
                            <>
                              <SubHeading text="Pre School Working days" />
                              {preschoolData.map(d => (
                                <>
                                  <p className="batchName">{d || ''}</p>
                                  <Flex column flexMargin="30px 0px">
                                    {groupBy(
                                      workingDayData.filter(
                                        d => d.school_type === 'preschool',
                                      ),
                                      o => o.batch_slug_name,
                                    )[d].map(d => (
                                      <Schedule>
                                        <span>
                                          {(d.weekdays || '')
                                            .split('_')
                                            .join('-')}
                                        </span>
                                        <span>
                                          {moment(
                                            d.from_hour,
                                            'hh:mm:ss',
                                          ).format('hh:mm A')}{' '}
                                          -{' '}
                                          {moment(d.to_hour, 'hh:mm:ss').format(
                                            'hh:mm A',
                                          )}
                                        </span>
                                      </Schedule>
                                    ))}
                                  </Flex>
                                </>
                              ))}
                            </>
                          )}
                      </div>
                    )}
                    <div className="gridItem">
                      <SubHeading text="Gallery" />
                      <GalleryWrapper wrap flexMargin="30px 0px">
                        {(mediaImages.length > 0 ? mediaImages : mediaResponse)
                          .slice(0, 6)
                          .map((d, i) => (
                            <PhotoCard
                              image={d.media_url}
                              width="48%"
                              type={i === 5 ? 'viewall' : ''}
                              onClick={
                                i === 5
                                  ? () => {
                                      setType('gallery');
                                      setActive(true);
                                    }
                                  : () => {
                                      setType('gallery');
                                      setActive(true);
                                      setMediaType(d.media_type);
                                    }
                              }
                              totalPhotos={
                                i === 5
                                  ? (mediaImages.length > 0
                                      ? mediaImages
                                      : mediaResponse
                                    ).length
                                  : ''
                              }
                            />
                          ))}
                        {/* <PhotoCard image={image2} />
                    <PhotoCard image={image3} />
                    <PhotoCard image={image4} />
                    <PhotoCard image={image5} />
                    <PhotoCard
                      image={image3}
                      type="viewall"
                      onClick={() => {
                        setType('gallery');
                        setActive(true);
                      }}
                    /> */}
                      </GalleryWrapper>
                    </div>
                  </div>

                  <div className="gridColumn">
                    <div className="gridItem">
                      <SubHeading
                        text="Words from Director/Centre Head"
                        margin="0px 0px 20px"
                        fontSize="22px"
                      />
                      <Testimonial
                        name={providerData.director_name || ''}
                        comment={`${providerData.words_from_director ||
                          "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s"}`}
                        img={providerData.director_photo_url}
                        width="162px"
                        height="190px"
                      />
                    </div>

                    <div className="gridItem">
                      <SubHeading text="About our program" fontSize="22px" />
                      <Flex>
                        <ParaGraph>
                          {providerData.about_school ||
                            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s"}
                        </ParaGraph>
                      </Flex>
                    </div>

                    <div className="gridItem">
                      <SubHeading
                        text="Facilities"
                        marginBottomXs="10px"
                        fontSize="22px"
                      />
                      <Flex wrap flexMargin="20px 0px" justifyBetween>
                        {includedMajorFacilities.map(d => (
                          <FacilitiesItem column>
                            {d.map((key, value) =>
                              key !== 'emergency_options' ? (
                                <Facility
                                  padding="0px 0px 0px 34px"
                                  style={{ textTransform: 'capitalize' }}
                                >
                                  {/* <span
                                    className="iconify"
                                    data-icon={MAJOR_FACILITIES_ICONS[key]}
                                    data-inline="false"
                                  /> */}
                                  <img src={facilityIcon} alt="" />
                                  {key.split('_').join(' ')}
                                </Facility>
                              ) : (
                                emergecyServicesData
                                  .filter(d =>
                                    safe(
                                      facilitiesData,
                                      '[0].emergency_options',
                                    ).includes(d.id),
                                  )
                                  .map(d => (
                                    <Facility
                                      padding="0px 0px 0px 34px"
                                      style={{ textTransform: 'capitalize' }}
                                    >
                                      <img src={facilityIcon} alt="" />
                                      {d.name}
                                    </Facility>
                                  ))
                              ),
                            )}
                          </FacilitiesItem>
                        ))}
                      </Flex>
                    </div>

                    <div className="gridItem">
                      <SubHeading
                        text="Additional Facilities"
                        marginBottomXs="10px"
                        fontSize="22px"
                      />
                      <Flex wrap flexMargin="20px 0px" justifyBetween>
                        <Flex column flexWidth="45%">
                          <span
                            style={{
                              marginBottom: 20,
                              fontWeight: 500,
                              fontSize: 14,
                            }}
                          >
                            Yes, we provide
                          </span>
                          {Object.entries(safe(facilitiesData, '[0]', {}))
                            .filter(([key, value]) =>
                              UNWANTED_FACILITIES.includes(key) ||
                              MAJOR_FACILITIES.includes(key)
                                ? false
                                : value,
                            )
                            .map(([key, value]) =>
                              key !== 'additional_facilities' ? (
                                <Facility className="provide">
                                  {key.split('_').join(' ')}
                                </Facility>
                              ) : (
                                value
                                  .split(',')
                                  .map(d => (
                                    <Facility className="provide">{d}</Facility>
                                  ))
                              ),
                            )}
                          <Flex justifyEnd>
                            <Button text="Show more" type="viewAll" />
                          </Flex>
                          <Flex justifyEnd>
                            <Button text="Show less" type="viewAll" />
                          </Flex>
                        </Flex>

                        <Flex
                          column
                          flexWidth="40%"
                          flexMargin="0px 0px 0px 30px"
                          className="dontProvide"
                        >
                          <span
                            style={{
                              marginBottom: 20,
                              fontWeight: 500,
                              fontSize: 14,
                            }}
                          >
                            No, We Don’t Provide
                          </span>
                          {Object.entries(safe(facilitiesData, '[0]', {}))
                            .filter(([key, value]) =>
                              UNWANTED_FACILITIES.includes(key)
                                ? false
                                : !value,
                            )
                            .map(([key]) => (
                              <Facility className="notProvide">
                                {key.split('_').join(' ')}
                              </Facility>
                            ))}
                          {safe(facilitiesData, '[0]', {}).restrictions ? (
                            safe(facilitiesData, '[0]', {}).restrictions.split(
                              ',',
                            ).length > 0 ? (
                              safe(facilitiesData, '[0].restrictions', '')
                                .split(',')
                                .map(d => (
                                  <Facility className="notProvide">
                                    {d}
                                  </Facility>
                                ))
                            ) : (
                              <Facility className="notProvide">
                                {safe(facilitiesData, '[0].restrictions', {})}
                              </Facility>
                            )
                          ) : null}
                          <Flex justifyEnd>
                            <Button text="Show more" type="viewAll" />
                          </Flex>
                          <Flex justifyEnd>
                            <Button text="Show less" type="viewAll" />
                          </Flex>
                        </Flex>
                      </Flex>
                    </div>
                    {safe(basicInfoData, '[0].summer_classes_offered') && (
                      <div className="gridItem">
                        <SubHeading
                          text="Summer classes offered"
                          marginBottomXs="10px"
                          fontSize="22px"
                        />
                        <Flex wrap justifyBetween flexMargin="30px 0px">
                          {safe(
                            basicInfoData,
                            '[0].summer_classes_offered_description',
                          )
                            .split(',')
                            .map(a => (
                              <Facility width="40%" className="provide">
                                {a}{' '}
                              </Facility>
                            ))}
                        </Flex>
                      </div>
                    )}
                    {
                      <div className="gridItem mobileonly">
                        <Flex justifyBetween flexMarginXs="0px 0px 20px 0px">
                          <SubHeading text="Our Teachers" fontSize="22px" />
                          <Button
                            text="View all"
                            type="viewAll"
                            marginRight="10px"
                            onClick={() => {
                              setType('teacher & staff');
                              setActive(true);
                            }}
                          />
                        </Flex>
                        <TeacherWrapper flexMargin="30px 0px">
                          {facultyData &&
                          facultyData.filter(d => d.faculty_type === 'teacher')
                            .length > 0 ? (
                            facultyData &&
                            facultyData
                              .filter(d => d.faculty_type === 'teacher')
                              .map((teach, id) => {
                                if (id < 3) {
                                  const langFilter = languageData.filter(a => {
                                    if (teach.languages_spoken.includes(a.id)) {
                                      return a.name;
                                    }
                                  });
                                  const lang = langFilter.map(a => a.name);
                                  return (
                                    <TeacherDetail
                                      name={teach.name}
                                      image={teach.photo_url}
                                      years={`${teach.experience} yrs`}
                                      desc={teach.words_from_teacher}
                                      languages={lang && lang.join(', ')}
                                      type="verified"
                                      width="33%"
                                    />
                                  );
                                }
                              })
                          ) : (
                            <>
                              <TeacherDetail
                                name="Sebastian Bennett"
                                image={Profile}
                                years="5yrs"
                                desc="Hello, my name is Sebastian and I am the founder of Angie's Daycare & Wonderschool."
                                languages="English, Hindi, Kannada"
                                type="verified"
                                width="33%"
                              />
                              <TeacherDetail
                                name="Sebastian Bennett"
                                image={Profile}
                                years="5yrs"
                                desc="Hello, my name is Sebastian and I am the founder of Angie's Daycare & Wonderschool."
                                languages="English, Hindi, Kannada"
                                width="33%"
                              />
                              <TeacherDetail
                                name="Sebastian Bennett"
                                image={Profile}
                                years="5yrs"
                                desc="Hello, my name is Sebastian and I am the founder of Angie's Daycare & Wonderschool."
                                languages="English, Hindi, Kannada"
                                type="verified"
                                width="33%"
                              />
                            </>
                          )}
                        </TeacherWrapper>
                      </div>
                    }
                  </div>

                  <div className="gridColumn">
                    <div className="gridItem mobileonly">
                      <SubHeading text="Contact Us" />
                      <Flex column flexMargin="30px 0px">
                        {safe(
                          locationData,
                          '[0]',
                          {},
                          ({
                            area = '',
                            city = '',
                            state = '',
                            pincode = '',
                            lattitude = 0.0,
                            longitude = 0.0,
                            ...location
                          }) => (
                            <Location
                              latitude={+lattitude}
                              longitude={+longitude}
                              address={`${location.address_line_1 ||
                                ''} ${location.address_line_2 ||
                                ''} ${area} ${city} ${state} ${pincode}`}

                              // TODO: Confim if needed
                              // number={safe(
                              //   contactData,
                              //   '[0]',
                              //   {},
                              //   contact =>
                              //     `Call the ${providerData.business_name ||
                              //       ''} hotline ${contact.mobile_number || ''}`,
                              // )}
                              // email={safe(
                              //   contactData,
                              //   '[0]',
                              //   {},
                              //   contact => contact.email_id,
                              // )}
                            />
                          ),
                        )}
                        {/* {contactData.map(
                      a =>
                        a.contact_type === 'director' &&
                        a.name && (
                          <>
                            <SubHeading
                              text="Director Details"
                              margin="20px 0px"
                            />
                            <Name>{a.name}</Name>
                            {(a.phone_number || a.alt_phone_number) && (
                              <Text>
                                Contact :{' '}
                                {a.phone_number ? `${a.phone_number},` : ''}{' '}
                                {a.alt_phone_number}
                              </Text>
                            )}
                            <Email>{a.email_id}</Email>
                          </>
                        ),
                    )} */}
                        <Button
                          text="Request for price"
                          // type="profile"
                          marginTop="10px"
                          onClick={() => {
                            setActive(true);
                            setType('enquiry');
                          }}
                        />
                        <Button
                          text="Contact us"
                          // type="profile"
                          marginTop="10px"
                          onClick={() => {
                            setActive(true);
                            setType('enquiry');
                          }}
                        />
                        {/* <Button
                    text="View Parent Handbook"
                    type="profile"
                    marginTop="10px"
                    onClick={() => {
                      setType('handbook');
                      setActive(true);
                    }}
                  /> */}
                      </Flex>
                    </div>
                  </div>
                </ProfileGrid>
              </>
            </MainWrapper>

            <Footer {...{ ...props, id, onSubmitSuccess }} />
          </>
        )
      ) : (
        <div
          style={{
            display: 'flex',
            justifyContent: 'center',
            marginTop: '20%',
          }}
        >
          <img src={loader} height={200} width={200} alt="" />
        </div>
      )}

      {isFullScreen && (
        <>
          <button
            style={{
              width: '30px',
              height: '30px',
              position: 'absolute',
              top: '40px',
              left: '40px',
              zIndex: 9,
            }}
            onClick={() => {
              setIsFullScreen(false);
            }}
          >
            <img
              style={{
                height: '15px',
                width: '15px',
                position: 'relative',
                top: '-1px',
              }}
              src={exitFullscreen}
              alt=""
            />
          </button>
          <Model
            id="myEmbeddedSceneFull"
            dangerouslySetInnerHTML={{
              __html: `<iframe
                    allowfullscreen
                    style="border-style:none;width:100%;height:100%;"
                    src=${iframeSrc()}
                  />`,
            }}
          />
        </>
      )}

      {active && (
        <Modal
          type={type}
          setActive={setActive}
          faculty={facultyData}
          languages={languageData}
          mediaResponse={
            type === 'gallery' &&
            (mediaImages.length > 0 ? mediaImages : mediaResponse)
          }
          mediaType={mediaType}
          setMediaType={() => setMediaType(null)}
          onDecline={onDecline}
          onAccept={onAccept}
        />
      )}
    </>
  );
};

// index.propTypes = {};

export default withRouter(Home);

const mediaResponse = [
  {
    media_id: 213,
    media_type: 'Decor',
    media_url:
      'https://productimages.withfloats.com/actual/5c7e6c80b5c197000185f1dc.jpg',
  },
  {
    media_id: 214,
    media_type: 'Play',
    media_url:
      'https://www.roundaboutharlow.co.uk/wp-content/uploads/Pre-schoolsNurseries-in-Harlow-1052x640.jpeg',
  },
  {
    media_id: 215,
    media_type: 'reception',
    media_url:
      'https://www.lilypondcountrydayschool.com/wp-content/uploads/2018/12/preschoolsforyoungchildren2-848x520.jpg',
  },
  {
    media_id: 216,
    media_type: 'photo360',
    media_url:
      'https://akm-img-a-in.tosshub.com/sites/btmt/images/stories/priya--preschool-owner-(1)_660_041119044845.jpg',
  },
  {
    media_id: 217,
    media_type: 'other',
    media_url:
      'https://s3-ap-southeast-1.amazonaws.com/tv-prod/school/photo/1599-large.jpg',
  },
  {
    media_id: 218,
    media_type: 'photo360',
    media_url:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcShjXHX0bp_xneLSLCw-1Se72KZ8IUOH9aeUGWice1cdoMEdM_pfA&s',
  },
  {
    media_id: 219,
    media_type: 'photo360',
    media_url:
      'https://www.stoodnt.com/blog/wp-content/uploads/2018/04/Preschool-Teaching-Career-and-Jobs-in-India-Asian-College-of-Teachers.jpg',
  },
  {
    media_id: 220,
    media_type: 'reception',
    media_url:
      'https://news24.sgp1.digitaloceanspaces.com/static_dev/static_root/media/2019/07/22/082a84e4-7500-4614-a7db-6c4619140fcb.jpg',
  },
  {
    media_id: 221,
    media_type: 'reception',
    media_url:
      'http://www.asianews.it/files/img/INDIA_-_0609_-_Ges%C3%B9_demone.jpg',
  },
  {
    media_id: 222,
    media_type: 'reception',
    media_url:
      'http://alfenoun.com/wp-content/uploads/2017/05/preschool-kids-130806.jpg',
  },
  {
    media_id: 223,
    media_type: 'reception',
    media_url:
      'https://abkfun.com/wp-content/uploads/2016/06/preschool-boy.jpg',
  },
];
