import styled from 'styled-components';
import Flex from 'components/common/flex';
import { Photo } from 'components/PhotoCard/styled';
import colors from 'utils/colors';

export const GalleryWrapper = styled(Flex)`
${Photo} {
&:nth-child(2n) {
  margin: 0px 0px 10px 0px;
}
}
  @media (max-width: 1152px) {
    ${Photo} {
      width: 48%;
    }
  }
  @media (max-width: 500px) {
    justify-content:space-between;
    ${Photo} {
      height: 125px;
      width: 172px;

    &:nth-child(2n) {
      margin: 0px 0px 10px 0px;
    }
    }
  }
  @media (max-width: 395px) {
    ${Photo} {
    height: 115px;
    width: 150px;
    }
  
  }
  @media (max-width: 350px) {
    ${Photo} {
    height: 115px;
    width: 140px;
    }
  
  }
  @media (max-width: 330px) {
    ${Photo} {
      height: 101px;
      width: 135px;
    }
    }
  }
`;
export const ProfileSection = styled.div`
width:100%;
display:flex;
justify-content:space-between;
padding: 30px 30px 30px 170px;
// background-color: ${colors.white};
.enquiryButton
{
  .profile
  {
    @media (max-width: 1280px) {
      padding:12px 17px;
    }
    @media (max-width: 425px) {
      padding:10px 15px;
      font-size: 13px;
      width: 47%;
    }
  }
}
.profileDetails
{
  &::-moz-scrollbars
  {
    display:none;
  }
}
@media (max-width: 1280px) {
  padding-left:130px;
}

@media (max-width: 768px) {
  width: 100%;
  margin-left: 0px;
  flex-direction:column;
  padding: 75px 0px 50px 20px;
  &>div:nth-child(2)
  {
    margin-top:20px;
    width:50%;
  }
}
@media (max-width: 425px) {
  padding: 45px 0px 30px 24px;
  &>div:nth-child(2)
  {
    width:100%;
    padding-right:20px;
  }
}
.profileContent
{
  @media (max-width: 768px) {
    width: 100%;
  }
 
}
`;

export const ProfileGrid = styled.div`
// background-color: ${colors.white};
display: flex;
margin:0px 30px;
border-top: 1px solid #E6E8EC;
@media (max-width: 768px) {
  flex-direction: column;
}
@media (max-width: 500px) {
  margin:0px;
}

.gridColumn
{
width:30%;
padding-top:50px;
padding-right:50px;
margin-right:50px;
border-right: 1px solid #E6E8EC;

@media (max-width: 1366px) {
  padding-right: 30px;
    margin-right: 30px;
}
@media (max-width: 1152px) {
  padding-right: 10px;
    margin-right: 10px;
}
@media (max-width: 768px) {
  display: flex;
  flex-wrap: wrap;
  width: 100%;
  padding-right: 0px;
  padding-top: 30px;
  margin-right: 0px;
  border-width: 0px;
  justify-content:space-between;
}

&:nth-child(2)
{
    width:46%;
    @media (max-width: 1280px) {
      width: 40%;
    }
    @media (max-width: 768px) {
      width: 100%;
      justify-content:space-between;
    }
    @media (max-width: 425px) {
      padding-top:0px;
    }
    
}
&:last-child
{
    width:24%;
    padding-right:0px;
    margin-right:0px;
    border-right: 0px solid #E6E8EC;
    @media (max-width: 1280px) {
      width: 28%;
    }
    @media (max-width: 768px) {
      width: 100%;
    }
}
}
.gridItem
{
border-bottom: 1px solid #E6E8EC;
margin-bottom:30px;
&.mobileonly{
  @media (max-width: 768px) {
    width: 100%;
    padding-right: 20px;
  }
}
&.teachers
{
  @media (max-width: 500px) {
    width: 100%;
    padding-right: 0px !important;
  }
}
@media (max-width: 768px) {
  width: 48%;
}
@media (max-width: 425px) {
  width: 100%;
  padding:0px 20px;
}
}
.galleryItem:last-child
{
   opacity:0.4;
}
.weProvide
{
  span
  {
   margin-bottom: 20px;
   font-weight: 500;
   font-size: 18px;
 @media (max-width: 500px) {
   font-size:14px;
 }
}
}
.dontProvide
{
  width:45%;
   span
   {
    margin-bottom: 20px;
    font-weight: 500;
    font-size: 18px;
  @media (max-width: 500px) {
    font-size:14px;
  }
   }
  @media (max-width: 1280px) {
    margin:0px;
  }
}
.batchName
{
  margin:30px 0px 10px;
  text-transform: capitalize;
  color:#30333B;
  font-weight: 500;
  font-family: Roboto, sans-serif;
  font-size: 17px;
  text-transform: Uppercase;
}
`;
export const AdditionalInfo = styled.h3`
  color: #666c78;
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  font-weight: 400;
  line-height: 22px;
  display: flex;
  align-items: center;
  margin: 0px 0px 12px;
  span {
    color: ${colors.lightBlack};
  }
  img {
    margin-right: 15px;
    transform: scale(0.9);
  }
  .license {
    margin-left: 4px;
    margin-right: 0px;
  }
  @media (max-width: 425px) {
    font-size: 12px;
  }
`;

export const TeacherWrapper = styled(Flex)`
  @media (max-width: 425px) {
    flex-direction: row;
    overflow-x: scroll;
  }
`;
export const Schedule = styled.h3`
  display: flex;
  padding: ${props => props.padding || '0px'};
  align-items: center;
  color: #666c78;
  font-family: 'Roboto', sans-serif;
  font-size: 16px;
  line-height: 26px;
  margin: 0px 0px 10px;
  font-weight: 400;
  & span:first-child {
    /* margin-right: 20px; */
    min-width: 150px;
    color: #30333b;
    font-weight: 500;
  }
  @media (max-width: 1420px) {
    font-size: 13px;
  }
  @media (max-width: 425px) {
    margin-bottom: 0px;
  }
  &:last-child {
    margin-bottom: 0px;
    padding-bottom: 0px;
  }
  span {
    text-transform: Capitalize;
  }
  img {
    transform: scale(0.9);
    margin-right: 15px;
  }
`;

export const ParaGraph = styled.div`
  white-space: pre-line;
  color: #666c78;
  font-family: 'Roboto', sans-serif;
  font-size: 16px;
  line-height: 25px;
  margin: 25px 0px;
  @media (max-width: 425px) {
    font-size: 14px;
  }
  ul {
    padding: 0px;
  }
  li {
    list-style: none;
  }
`;

export const Facility = styled.h3`
  position: relative;
  width: ${props => props.width || 'auto'};
  min-height: ${props => props.height || 'auto'};
  padding: ${props => props.padding || '0px'};
  color: #666c78;
  font-family: 'Roboto', sans-serif;
  font-size: 16px;
  font-weight: 400;
  margin: 0px 0px 12px;
  line-height: 21px;
  text-transform: Capitalize;
  img {
    position: absolute;
    left: 0px;
    top: -1px;
    transform: scale(0.9);
    @media (max-width: 425px) {
      transform: scale(0.8);
    }
  }
  &:last-child {
    margin-bottom: 0px;
  }
  @media (max-width: 1300px) {
    font-size: 13px;
  }
  @media (max-width: 768px) {
    font-size: 12px;
  }
  @media (max-width: 320px) {
    font-size: 11px;
  }
  &.provide {
    position: relative;
    padding-left: 20px;
    text-transform: capitalize;
    &::before {
      position: absolute;
      content: '';
      height: 8px;
      width: 8px;
      left: 0px;
      top: 6px;
      background-color: #8a909d;
      border-radius: 50%;
    }
  }
  &.notProvide {
    position: relative;
    padding-left: 20px;
    text-transform: capitalize;
    &::before {
      position: absolute;
      content: '';
      height: 9px;
      width: 9px;
      left: 0px;
      top: 7px;
      border: 2px solid #8a909d;
      border-radius: 50%;
    }
  }
  .iconify {
    position: absolute;
    color: #30333b;
    height: 23px;
    width: 26px;
    margin-right: 18px;
    left: 0px;
    top: -1px;
  }
`;

export const FacilitiesItem = styled(Flex)`
  padding: 0px;
  width: 45%;
  // &:last-child {
  //   margin: 0px 0px 0px 10px;
  //   @media (max-width: 320px) {
  //     margin: 0px 0px 0px 0px;
  //   }
  // }
`;
export const Name = styled.div`
  color: #30333b;
  font-family: 'Roboto', sans-serif;
  font-size: 18px;
  line-height: 21px;
  margin-bottom: 2px;
`;
export const Text = styled.div`
  color: rgba(0, 0, 0, 0.5);
  font-family: 'Roboto', sans-serif;
  font-size: 18px;
  line-height: 21px;
  margin-bottom: 2px;
`;
export const Email = styled.div`
  color: #60b947;
  font-family: 'Roboto', sans-serif;
  font-size: 18px;
  line-height: 21px;
  margin-bottom: 40px;
  margin-bottom: 2px;
`;
export const BranchName = styled.div`
  color: #666c78;
  font-family: 'Roboto', sans-serif;
  font-size: 18px;
  font-weight: 500;
  line-height: 32px;
  text-align: justify;
  cursor: pointer;
  &:hover {
    color: #613a95;
  }
`;

export const Image360 = styled.div`
  // position: relative;
  width: 100%;
  iframe {
    width: 100%;
    @media (max-width: 425px) {
      width: 100%;
    }
  }
  @media (max-width: 425px) {
    margin-top: -25px;
  }
`;

export const ImageTabs = styled.div`
  position: absolute;
  bottom: 15px;
  left: 20px;
  display: flex;
  max-width: 87%;
  overflow-x: scroll;
  .profile {
    padding: 6px 12px;
  }
  &::-webkit-scrollbar {
    visibility: hidden;
    height: 0px;
    width: 0px;
  }
`;

export const FullScreenImageTabs = styled.div`
  height: 100vh;
  bottom: 15px;
  left: 20px;
  display: flex;
  flex-direction: column;
  max-width: 87%;
  overflow-x: scroll;
  .profile {
    z-index: 10;
    padding: 6px 12px;
    // position: absolute;
    bottom: 15px;
    left: 20px;
  }
  &::-webkit-scrollbar {
    visibility: hidden;
    height: 0px;
    width: 0px;
  }
`;

export const MobileTabs = styled.div`
  display: none;
  height: 40px;
  align-items: center;
  border-top: 1px solid #e6e8ec;
  border-bottom: 1px solid #e6e8ec;
  padding-left: 24px;

  li {
    display: flex;
    align-items: center;
    height: 100%;
    font-family: 'Quicksand', sans-serif;
    font-weight: 400;
    padding: 0px 12px;
    color: #666c78;
    font-size: 14px;
    letter-spacing: 0;
    line-height: 17px;
    list-style-type: none;
    cursor: pointer;
    &:hover,
    &.active {
      color: #fff;
      background-color: #60b947;
    }
  }
  &.fixed {
    position: fixed;
    top: 52px;
    left: 0px;
    background-color: #fff;
    right: 0px;
    z-index: 9;
  }
  @media (max-width: 500px) {
    display: flex;
  }
`;

export const displayNone = {
  display: 'none',
};
