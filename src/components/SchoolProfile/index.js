/* eslint-disable */
/* eslint-disable no-unused-expressions */
/* eslint-disable react/prop-types */
// eslint-disable-next-line react/no-unescaped-entities
import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Router from 'next/router';
import groupBy from 'lodash/groupBy';
import flatten from 'lodash/flatten';
// import axios from 'config/axios';
import styled from 'styled-components';
import FlexBottomFixed from 'components/common/flex_bottom_fixed';
// import { Pano } from 'react-vr';

// import PropTypes from 'prop-types';
// import { mdiHeartOutline, mdiShareVariant } from '@mdi/js';
// import parent5 from 'images/user5.svg';
import Footer from 'components/Footer';
import Button from 'components/Button';
import Header from 'components/Header';
import moment from 'moment';
import Director from 'components/DirectorCard';
// import history from 'utils/history';
import MainWrapper from 'components/MainWrapper';
import ImageBanner from 'components/ImageBanner/index';
import TitleCard from 'components/TitleCard';
import {
  useDayCareDetailHook,
  useWishlistHook,
  useScheduleHook,
} from 'shared/hooks';
import Flex from 'components/common/flex';
import ProfileDetails from 'components/ProfileDetails/index';
import PhotoCard from 'components/PhotoCard/index';
import Location from 'components/LocationMap/index';
import SubHeading from 'components/SubHeading/index';
import Syllabus from 'components/SyllabusCard';
// import Testimonial from 'components/TestimonialCard/index';
// import ParentReview from 'components/ParentReview/index';
import TeacherDetail from 'components/TeacherDetail/index';
// import SchoolCard from 'components/SchoolCard';
import SimpleBar from 'simplebar-react';
// import parent1 from 'images/useer1.svg';
// import parent2 from 'images/user2.svg';
// import parent3 from 'images/user3.svg';
// import parent4 from 'images/user4.svg';
// import schoolImage1 from 'images/monterssori.svg';
// import schoolImage2 from 'images/elle.png';
// import schoolImage3 from 'images/jumbo.png';
// import VirtualTour from 'images/360_view.jpg';
// import VirtualTourPlaceholder from 'images/360_placeholder.jpg';
// import schoolImage4 from 'images/tiny.png';
// import schoolImage5 from 'images/oi.png';
// import star3 from 'images/staar_3.png';
import Verfied from 'components/Verfied';
import year from 'images/year of establishment.svg';
// import capacity from 'images/capacity.png';
import license from 'images/license number (4).png';
import achieve1 from 'images/Group 4.svg';
// import achieve2 from 'images/Group 12.svg';
import achieve3 from 'images/Group 14.svg';
import welcomeImage from 'images/mascot1.png';
import coverPicPlaceholder from 'images/coverPicPlaceholder.jpg';
import Profile from 'images/profile.jpg';
import fullscreen from 'images/fullscreen.png';
import exitFullscreen from 'images/exit-fullscreen.png';

import cctv from 'images/CCTV.svg';
import ac from 'images/AC Rooms-1.svg';
import car from 'images/car parking-1.svg';
import childProof from 'images/child proof-1.svg';
import emergency from 'images/emergency services-1.svg';
import hours from 'images/extended hours-1.svg';
import fire from 'images/fire emergency-1.svg';
import food from 'images/food-1.svg';
import indoor from 'images/indoor play area-1.svg';
import liveStreaming from 'images/live streaming-1.svg';
import outdoor from 'images/outdoor play area-1.svg';
import app from 'images/parent teacher app-1.svg';
import report from 'images/report.svg';
import restricitons from 'images/restricitons.svg';
import sleeping from 'images/sleeping area.svg';
import snacks from 'images/snacks-1.svg';
import specialCare from 'images/special care.svg';
import sick from 'images/tc of sick kids.svg';
import transport from 'images/transport-1.svg';
import waiting from 'images/waiting room.svg';
import washroom from 'images/washroom-1.svg';
import weekend from 'images/weekend care-1.svg';
import general from 'images/general.svg';
import preschoolCap from 'images/preschool cap.svg';
import daycareCap from 'images/daycare cap.svg';
import licenseTag from 'images/license label.svg';
import doctor from 'images/doctors-1.svg';
import nurse from 'images/nurses-1.svg';
import profilePlaceholder from 'images/school-profile.jpg';
import colors from '../../utils/colors';
import teacher1 from 'images/teacher-female-ph1.png';
import teacher2 from 'images/teacher-female-ph2.png';
import teacher3 from 'images/teacher-female-ph3.png';

import staff1 from 'images/staff-female-ph1.png';
import staff2 from 'images/staff-female-ph2.png';
import { virtualTourlBaseUrl } from 'utils/BaseUrl';
import Link from 'next/link';
const TEACHER_PLACEHOLDERS = [teacher1, teacher2, teacher3];
const STAFF_PLACEHOLDERS = [staff1, staff2];

const FACILITIES_ICON_CONFIG = {
  'ac rooms': ac,
  reports: report,
  cctv: cctv,
  'live streaming': liveStreaming,
  'snacks provided': snacks,
  'sleeping area facility': sleeping,
  'waiting area for parents': waiting,
  'fire emergency contact': fire,
  'extended time': hours,
  transportation: transport,
  'provide food to day care': food,
  'child proof': childProof,
  'parent teacher app': app,
  'washroom for children staff': washroom,
  'take care of sick kids': sick,
  'outdoor play area': outdoor,
  'indoor play area': indoor,
  'weekend care': weekend,
  'car parking': car,
  'facility for special care kids': specialCare,
  'hourly basis': hours,
  Doctor: doctor,
  Nurse: nurse,
};

const CUSTOM_FACILITY_TEXT = {
  'provide food to day care': 'provide food to day care children',
  'washroom for children staff': 'washroom for children and staff',
  'ac rooms': 'AC Rooms',
  'facility for special care kids': 'Facility for Special Care',
  'waiting area for parents': 'Waiting Area for Parents',
  'parent teacher app': 'Parent-Teacher App',
  'hourly basis': 'Daycare on Hourly Basis',
};

const ACHIEVEMENT_ICONS = [achieve1, achieve3];

import {
  ProfileSection,
  ProfileGrid,
  AdditionalInfo,
  Schedule,
  ParaGraph,
  Facility,
  BranchName,
  FacilitiesItem,
  GalleryWrapper,
  TeacherWrapper,
  Image360,
  ImageTabs,
  Name,
  Text,
  Email,
  FullScreenImageTabs,
  MobileTabs,
  displayNone,
} from './styled';

const UNWANTED_FACILITIES = [
  'id',
  'provider',
  'languages_spoken',
  'emergency_details',
  'restrictions',
  'emergency_options',
  'extended_time',
  'parent_teacher_app_name',
  ,
  'courses',
  'custom_course_text',
];

const Model = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  height: 100vh;
  width: 100%;
`;

const MAJOR_FACILITIES = [
  'indoor_play_area',
  'outdoor_play_area',
  'provide_food_to_day_care',
  'transportation',
  'child_proof',
  'emergency_details',
  'emergency_options',
  'cctv',
  'live_streaming',
  'parent_teacher_app',
  'ac_rooms',
];

const MAJOR_FACILITIES_ICONS = {
  indoor_play_area: 'bx:bxs-tennis-ball',
  outdoor_play_area: 'ic-baseline-outdoor-grill',
  provide_food_to_day_care: 'emojione-monotone:pot-of-food',
  transportation: 'ic-round-emoji-transportation',
  child_proof: 'mdi-account-child',
  emergency_options: 'whh-emergency',
  cctv: 'ant-design:video-camera-outline',
  live_streaming: 'ic-baseline-live-tv',
  parent_teacher_app: 'raphael-parent',
  ac_rooms: 'ic-baseline-ac-unit',
};

const BY_PASS_SLUGS = [
  'koala-preschool-214598',
  'gurukul-mont-international-preschool-and-daycare-287459',
  'junior-dps-preschool-278980',
  'republick-high-pre-school-232109',
  'koala-preschool-179368',
  'cambridge-montessori-preschool-and-daycare-76899',
  'zion-kidz-preschool-273142',
  'little-millennium-preschool-252142',
];

const scrollTo = postion => {
  window.scrollTo({
    top: postion,
    behavior: 'smooth',
  });
};

const SchoolProfile = ({
  successToast,
  errorToast,
  USER_CONNECT_API_CALL,
  ...props
}) => {
  const [schoolname, setSchoolName] = useState('');
  const [schoolbranch, setSchoolBranch] = useState('');
  const [active, setActive] = useState(false);
  const [mediaType, setMediaType] = useState(null);
  const [type, setType] = useState('');
  const [isFullScreen, setIsFullScreen] = useState(false);
  const [activityShowMore, setActivityShowMore] = useState(false);
  const [coursesShowMore, setCoursesShowMore] = useState(false);
  const [scheduleShowMore, setScheduleShowMore] = useState(false);
  const [dayCareCalShowMore, setDayCareCalShowMore] = useState(false);
  const [preschoolCalShowMore, setPreschoolCalShowMore] = useState(false);
  const [yesWeProvideShowMore, setYesWeProvideShowMore] = useState(false);
  const [noWeProvideShowMore, setNoWeProvideShowMore] = useState(false);
  const [currentTourSlide, setCurrentTourSlide] = useState(0);
  const [selectedSchedule, setSelectedSchedule] = useState({});
  const [isAccessBlockerModalFixed, setIsAccessBlockerModalFixed] = useState(
    false,
  );
  const [enquiryModalTitle, setEnquiryModalTitle] = useState('');
  const [activeMobileTab, setActiveMobileTab] = useState('about');
  const [mobileView, setMobileView] = useState(false);
  const [mobileNavFixed, setMobileNavFixed] = useState(false);
  const [isWishlisted, setIsWishlisted] = useState(null);
  // const contetOffset =
  //   global.window && window.getElementById('app').getBoundingClientRect();

  const handleScroll = e => {
    if (!isLoggedIn) {
      if (window.pageYOffset > 630 && !isAccessBlockerModalFixed)
        setIsAccessBlockerModalFixed(true);
      else setIsAccessBlockerModalFixed(false);
    }

    // const mobileNav = document.getElementById('mobileTab');
    const mobileNav = 748;

    if (window.scrollY > mobileNav) {
      setMobileNavFixed(true);
    }

    if (window.scrollY < mobileNav) {
      setMobileNavFixed(false);
    }
  };

  useEffect(() => {
    // add when mounted
    document.addEventListener('scroll', handleScroll);
    // return function to be called when unmounted
    return () => {
      document.removeEventListener('scroll', handleScroll);
    };
  }, []);

  const {
    PROVIDER_FACULTY_API_CALL,
    PROVIDER_CONTACT_DETAILS_API_CALL,
    PROVIDER_FACILITY_API_CALL,
    PROVIDER_LANGUAGES_API_CALL,
    PROVIDER_YEARLY_CALENDER_API_CALL,
    PROVIDER_EMERGENCY_SERVICES_API_CALL,
    GET_USERS_CHILDREN_API_CALL,
    GET_USERS_TOUR_SCHEDULE_API_CALL,
    USER_ENQUIRY_API_CALL,
    authentication: { isLoggedIn } = {},
    query: { 'daycare-id': id } = {},
  } = props;

  // const { wishlist, name: schoolName, type: schoolType } = props.router.query;
  const { isSchoolWishlisted } = props;
  useEffect(
    () => {
      setIsWishlisted(isSchoolWishlisted);
    },
    [isSchoolWishlisted],
  );

  const origin =
    typeof window !== 'undefined' && window.location.origin
      ? window.location.origin
      : '';

  const routerPath = useRouter();

  const {
    location: { data: locationData = [], loader: locationLoader },
    provider: { data: providerData = {}, loader: providerLoader },
    yearlyCalender: {
      data: yearlyCalenderData = [],
      loader: yearlyCalenderLoader,
    },
    scheduleForDay: {
      data: scheduleForDayData = [],
      loader: scheduleForDayLoader,
    },
    workingDay: { data: workingDayData = [], loader: workingDayLoader },
    facilities: { data: facilitiesData = [], loader: facilitiesLoader },
    faculty: { data: facultyData = [], loader: facultyLoader },
    language: { data: languageData = [], loader: languageLoader },
    contact: { data: contactData = [], loader: contactLoader },
    media: { data: mediaData = [], loader: mediaLoader },
    basicInfo: { data: basicInfoData = [], loader: basicInfoLoader },
    emergecyServices: {
      data: emergecyServicesData = [],
      loader: emergecyServicesLoader,
    },
    scheduleChoice: {
      data: scheduleChoiceData = [],
      loader: scheduleChoiceLoader,
    },
    contentSchedule: {
      data: {
        photo_360: {
          // choice: virtualTourService = 'no'
        } = {},
      } = {},
      loader: contentScheduleLoader,
    },
  } = useDayCareDetailHook(props);

  const { postWishlist, deleteWishlist } = useWishlistHook(props, {
    onWishlistDelError,
  });

  const scheduleCheckCallback = ({
    data: {
      data: { schedule_tour_created: isTourScheduled, data = [] } = {},
    } = {},
    provId,
  }) => {
    const schduleDetails = data[0];
    if (isTourScheduled) {
      // setProviderId(provId);
      setSelectedSchedule(schduleDetails);
      setType('forceReschule');
      setActive(true);
      // onRescheduleClick
    }
    if (!isTourScheduled) {
      setType('tour');
      setActive(true);
    }
  };

  const onScheduleEditSuccess = () => {
    successToast('Re-schedules successfully.');
  };
  const onScheduleEditError = ({ message }) =>
    errorToast(message || 'Something went worng.');

  const {
    checkTourExists,
    scheduleCheckLoader,
    userConnectLoader,
    userEnquiryLoader,
  } = useScheduleHook(props, {
    onScheduleEditSuccess,
    onScheduleEditError,
    scheduleCheckCallback,
  });

  const { safe } = props;

  const resize = () => {
    setMobileView(window.innerWidth <= 500);
  };

  useEffect(
    () => {
      if (global.window && window.screen.width < 500) {
        setMobileView(true);
      } else {
        setMobileView(false);
      }
    },
    [global.window && global.window.screen.width],
  );

  const mediaImages = mediaData.filter(
    d => d.media_type !== 'logo' && d.media_type !== 'cover_pic',
    // &&
    // d.media_type !== 'photo360',
  );
  const mainFacilities = Object.entries(safe(facilitiesData, '[0]', {})).filter(
    ([key, value]) =>
      MAJOR_FACILITIES.includes(key) && key !== 'emergency_details'
        ? value
        : false,
  );

  const facilitiesFlattened = flatten(
    mainFacilities.map(([key, val]) => {
      if (key === 'emergency_options') {
        return emergecyServicesData
          .map(d => {
            if (safe(facilitiesData, '[0].emergency_options').includes(d.id))
              return d.name;
          })
          .filter(d => d);
      } else {
        return key;
      }
    }),
  );

  const manjorFacilitiesArrayOne = facilitiesFlattened.slice(
    0,
    facilitiesFlattened.length % 2 === 0
      ? facilitiesFlattened.length / 2
      : facilitiesFlattened.length / 2 + 1,
  );

  const manjorFacilitiesArrayTwo = facilitiesFlattened.slice(
    facilitiesFlattened.length % 2 === 0
      ? facilitiesFlattened.length / 2
      : facilitiesFlattened.length / 2 + 1,
  );

  const routerWillLeave = nextState => {
    // return false to block navigation, true to allow
    if (nextState.action === 'POP') {
      // handle "Back" button clicks here
    }
  };

  useEffect(
    () => {
      if (active) {
        document.body.style.overflow = 'hidden';
      } else {
        document.body.style.overflow = 'unset';
      }
    },
    [active],
  );

  // CHANGING URL WITHOUT RELOAD
  const queryChange = (e, val) => {
    const { area = '', city = '', state = '', pincode = '' } = safe(
      locationData,
      '[0]',
      {},
    );
    Router.replace(
      `/daycare/[state]/[city]/[area]/[daycare-id]?wishlist=${val}&name=${providerData.business_name ||
        ''}&type=${safe(locationData, '[0].area')}`,
      `/daycare/${state}/${city}/${area}/${id}?wishlist=${val}&name=${providerData.business_name ||
        ''}&type=${safe(locationData, '[0].area')}`,
      {
        shallow: true,
      },
    );
  };
  const onWishlistDelError = message =>
    errorToast(message || 'Something went wrong.');

  const onSubmitSuccess = () => {
    setActive(true);
    setType('approval');
  };

  const onDecline = () => {
    setActive(false);
  };
  const onAccept = () => {
    setActive(false);
    // PROD
    // window.location.href =
    //   'https://kiddenzdc.cartoonmango.com/#/profile?page=success';

    // DEV
    window.location.href =
      'http://kiddenz-admin.s3-website.ap-south-1.amazonaws.com/#/profile?page=success';
    ('https://kiddenzdc.cartoonmango.com/#/profile?page=success');
  };

  const dayCareData = workingDayData
    .filter(d => d.school_type === 'day_care')
    .filter(
      d =>
        d.weekdays === 'mon_fri' ||
        d.weekdays === 'sun' ||
        d.weekdays === 'sat',
    );

  const preschoolData = Object.keys(
    groupBy(
      workingDayData.filter(
        d =>
          d.school_type === 'preschool' &&
          (d.weekdays === 'mon_fri' ||
            d.weekdays === 'sun' ||
            d.weekdays === 'sat') &&
          d.batch_slug_name,
      ),
      o => o.batch_slug_name,
    ),
  )
    .map(function(a) {
      return { key: a, value: +a.split('batch')[1] };
    })
    .sort(function(a, b) {
      return a.value - b.value;
    })
    .map(function(a) {
      return a.key;
    });

  const pararomicMedia = mediaImages.filter(d => d.media_type === 'photo360');

  // safe(yearlyCalenderData, '[0].year', '')
  const dayCareCalenderData = yearlyCalenderData.filter(
    d => d.school_type && d.school_type === 'day_care',
  );

  const preschoolCalenderData = yearlyCalenderData.filter(
    d => d.school_type && d.school_type === 'preschool',
  );

  const additionalFacilities = Object.entries(safe(facilitiesData, '[0]', {}))
    .filter(
      ([key, value]) =>
        UNWANTED_FACILITIES.includes(key) || MAJOR_FACILITIES.includes(key)
          ? false
          : value,
    )
    .filter(([key]) => key !== 'additional_facilities')
    .map(([key]) => key.split('_').join(' '));

  const additionalFacilitiesCustom = Object.entries(
    safe(facilitiesData, '[0]', {}),
  )
    .filter(
      ([key, value]) =>
        UNWANTED_FACILITIES.includes(key) || MAJOR_FACILITIES.includes(key)
          ? false
          : value,
    )
    .filter(([key]) => key === 'additional_facilities')
    .map(([key, value]) => value);

  const yesWeProvideFacilities = [
    ...additionalFacilities,
    ...(additionalFacilitiesCustom.length > 0
      ? additionalFacilitiesCustom[0].split(',')
      : []),
  ];

  const unwantedFacilities = Object.entries(safe(facilitiesData, '[0]', {}))
    .filter(
      ([key, value]) => (UNWANTED_FACILITIES.includes(key) ? false : !value),
    )
    .map(([key]) => key.split('_').join(' '));

  const restrictedFacilities = safe(facilitiesData, '[0]', {}).restrictions
    ? safe(facilitiesData, '[0]', {}).restrictions.split(',')
    : [];

  const nowDontProvide = [...unwantedFacilities, ...restrictedFacilities];

  const summerClassesOffered = (safe(
    basicInfoData,
    '[0].summer_classes_offered_description',
  )
    ? safe(basicInfoData, '[0].summer_classes_offered_description')
    : ''
  ).split(',');

  const summerClassesOfferedArrayOne = summerClassesOffered.slice(
    0,
    summerClassesOffered.length % 2 === 0
      ? summerClassesOffered.length / 2
      : summerClassesOffered.length / 2 + 1,
  );

  const summerClassesOfferedArrayTwo = summerClassesOffered.slice(
    summerClassesOffered.length % 2 === 0
      ? summerClassesOffered.length / 2
      : summerClassesOffered.length / 2 + 1,
  );

  const image = `https://cdn.pannellum.org/2.5/pannellum.htm#panorama=${safe(
    mediaImages.filter(d => d.media_type === 'photo360'),
    `[${currentTourSlide}].media_url`,
  )}&autoLoad=true`;

  const iframeSrc = () => {
    if (
      safe(pararomicMedia, '[0]', {}).media_url &&
      safe(pararomicMedia, '[0]', {}).media_url.includes("'data': ")
    ) {
      if (
        safe(pararomicMedia, '[0]', {})
          .media_url.split("'data': '")[1]
          .split("'}")[0] &&
        safe(pararomicMedia, '[0]', {})
          .media_url.split("'data': '")[1]
          .split("'}")[0]
          .includes('index.htm')
      ) {
        return safe(pararomicMedia, '[0]', {})
          .media_url.split("'data': '")[1]
          .split("'}")[0];
      } else {
        // return 'https://virtualtour.kiddenz.com/dc-90-ECC0F5E26B/index.html';
        return null;
      }
    } else {
      if (
        safe(pararomicMedia, '[0]', {}).media_url &&
        safe(pararomicMedia, '[0]', {}).media_url.includes('index.htm')
      ) {
        return safe(pararomicMedia, '[0]', {}).media_url;
      } else {
        // return 'https://virtualtour.kiddenz.com/dc-90-ECC0F5E26B/index.html';
        return null;
      }
    }
  };

  const iframeRenderer = () => {
    if (virtualTourService === 'yes') {
      return (
        <iframe
          width="340"
          height="220"
          allowFullScreen
          style={{ borderStyle: 'none' }}
          src={iframeSrc()}
        />
      );
    } else {
      if (
        safe(
          mediaImages.filter(d => d.media_type === 'photo360'),
          '[0].media_url',
        )
      ) {
        return (
          <div
            key={`myEmbeddedScene_${currentTourSlide}`}
            id="myEmbeddedScene"
            style={{ display: 'flex' }}
          >
            <iframe
              width="450"
              height="220"
              allowFullScreen
              src={image}
              style={{ borderStyle: 'none' }}
            />
          </div>
          // <div
          //   key={`myEmbeddedScene_${currentTourSlide}`}
          //   id="myEmbeddedScene"
          //   style={{
          //     display: 'flex',
          //   }}
          //   dangerouslySetInnerHTML={{
          //     __html: `<iframe
          //     width="450"
          //     height="220"
          //           allowfullscreen
          //           style="border-style:none;"
          //           src=${image}
          //           />`,
          //   }}
          // />
        );
      } else {
        return null;
        // return (
        // <iframe
        //   width="340"
        //   height="220"
        //   allowfullscreen
        //   style={{ borderStyle: 'none' }}
        //   src="https://virtualtour.kiddenz.com/dc-90-ECC0F5E26B/index.html"
        // />
        // );
      }
    }
  };

  const fullScreenIframeRenderer = () => {
    if (virtualTourService === 'yes') {
      return (
        <Model
          id="myEmbeddedSceneFull"
          dangerouslySetInnerHTML={{
            __html: `<iframe
                    allowfullscreen
                    style="border-style:none;width:100%;height:100%;"
                    src=${iframeSrc()}
                  />`,
          }}
        />
      );
    } else {
      if (
        safe(
          mediaImages.filter(d => d.media_type === 'photo360'),
          '[0].media_url',
        )
      ) {
        return (
          <Model
            id="myEmbeddedSceneFull"
            dangerouslySetInnerHTML={{
              __html: `<iframe
                    allowfullscreen
                    style="border-style:none;width:100%;height:100%;"
                    src=${image}
                  />`,
            }}
          />
        );
      } else {
        return null;
        // return (
        // <Model
        //   id="myEmbeddedSceneFull"
        //   dangerouslySetInnerHTML={{
        //     __html: `<iframe
        //           allowfullscreen
        //           style="border-style:none;width:100%;height:100%;"
        //           src='https://virtualtour.kiddenz.com/dc-90-ECC0F5E26B/index.html'
        //         />`,
        //   }}
        // />
        // );
      }
    }
  };

  const buttonNameGen = media_url => {
    const splitArr = media_url.split('/');
    const roomName = splitArr[splitArr.length - 1]
      .split('-')[0]
      .split('_')
      .join(' ');

    return roomName.charAt(0).toUpperCase() + roomName.slice(1);
  };

  const onAuthSuccess = () => {
    PROVIDER_FACULTY_API_CALL({ params: { id } });
    PROVIDER_CONTACT_DETAILS_API_CALL({ params: { id } });
    PROVIDER_FACILITY_API_CALL({ params: { id } });
    PROVIDER_LANGUAGES_API_CALL({ params: {} });
    PROVIDER_YEARLY_CALENDER_API_CALL({ params: { id } });
    PROVIDER_EMERGENCY_SERVICES_API_CALL({ params: {} });
    GET_USERS_CHILDREN_API_CALL();
    GET_USERS_TOUR_SCHEDULE_API_CALL({
      query: {
        limit: 3,
        skip: 0,
      },
    });
  };

  const virtualTourService = (() => {
    const vitutalTourImagesArr = mediaData.filter(
      d => d.media_type === 'photo360',
    );
    if (vitutalTourImagesArr.length === 1) {
      if (vitutalTourImagesArr[0].media_url.includes(virtualTourlBaseUrl)) {
        return 'yes';
      }
      return 'no';
    } else {
      return 'no';
    }
  })();

  const allImages = mediaData.filter(
    element => element.media_type !== 'photo360',
  );

  const allImageHref = allImages.map(element => {
    return element.media_url;
  });

  const logoImage = allImages.find(element => element.media_type === 'logo');
  const coverpic = allImages.find(
    element => element.media_type === 'cover_pic',
  );

  return (
    <>
      <script
        type="application/ld+json"
        dangerouslySetInnerHTML={{
          __html: `
        {
            "@context": "http://schema.org",
            "@type": "Preschool",
            "name": "${providerData.business_name || ''}",
            "description": "${
              providerData.words_from_director
                ? providerData.words_from_director.charAt(0).toUpperCase() +
                  providerData.words_from_director.slice(1)
                : '' || ''
            }",
            "address": {
                  "@type": "PostalAddress",
                  "streetAddress": "${safe(
                    locationData,
                    '[0].address_line_1',
                  ) || ''} ${safe(locationData, '[0].address_line_2') || ''}",
                  "addressLocality": "${safe(locationData, '[0].area')}",
                  "addressRegion": "${safe(locationData, '[0].city')}",
                  "postalCode": "${safe(locationData, '[0].pincode')}",
                  "addressCountry": "India"
            },
            "geo": {
                "@type": "GeoCoordinates",
                "latitude": "${safe(locationData, '[0].lattitude')}",
                "longitude": "${safe(locationData, '[0].longitude')}"
            },
            "image": "[${allImageHref}]",
            "logo": "${logoImage.media_url}",
            "url": "https://kiddenz.com${routerPath.asPath.slice(
              0,
              routerPath.asPath.indexOf('?') >= 0 ? routerPath.asPath.indexOf('?') : undefined,
            )}",
            "foundingDate": "${safe(
              basicInfoData,
              '[0].year_of_establishment',
            )}",
            "founder": {
                  "@type": "Person",
                  "name": "${
                    providerData.director_designation === 'central_head' ||
                    providerData.director_designation === 'center_head'
                      ? providerData.name || ''
                      : providerData.director_name || providerData.name
                  }"
            },
            "numberOfEmployees": {
                  "@type": "QuantitativeValue",
                  "value": "${safe(facultyData.length) + 3}"
            },
            "contactPoint": {
                  "@type": "ContactPoint",
                  "telephone": "${providerData.phone_number}",
                  "contactType": "Customer Service",
                  "areaServed": {
                      "@type": "Place",
                      "name": "${safe(locationData, '[0].area')}",
                      "branchCode": "",
                      "hasMap": "",
                      "geo": {
                            "@type": "GeoCoordinates",
                            "latitude": "${safe(
                              locationData,
                              '[0].lattitude',
                            )}",
                            "longitude": "${safe(
                              locationData,
                              '[0].longitude',
                            )}"
                      }
                  }
            }
          }`,
        }}
      />
      {!isFullScreen && (
        <>
          <MainWrapper
            background="#fff"
            // paddingTop={66}
            className="schoolProfile"
          >
            <Header
              type="articles"
              screenWidth
              isModalActive={active}
              activeModalType={type}
              setActiveCallback={setActive}
              successToast={successToast}
              errorToast={errorToast}
              dayCareName={providerData.business_name || ''}
              dayCareArea={safe(locationData, '[0].area')}
              providerId={providerData.id}
              mediaType={mediaType}
              mediaResponse={
                mediaImages.length > 0
                  ? mediaImages.filter(d => d.media_type !== 'photo360')
                  : mediaResponse
              }
              faculty={facultyData}
              languages={languageData}
              onOtpSuccessCallback={onAuthSuccess}
              selectedSchedule={selectedSchedule}
              onOnboardingSuccessCallback={onAuthSuccess}
              modalTitle={enquiryModalTitle}
              {...props}
            />
            <ImageBanner
              checkHttpForBanner={safe(
                mediaData.filter(d => d.media_type === 'cover_pic'),
                '[0].media_url',
              )}
              checkHttpForLogo={safe(
                mediaData.filter(d => d.media_type === 'logo'),
                '[0].media_url',
              )}
              imgpath={
                safe(
                  mediaData.filter(d => d.media_type === 'cover_pic'),
                  '[0].media_url',
                )
                  ? safe(
                      mediaData.filter(d => d.media_type === 'cover_pic'),
                      '[0].media_url',
                    )
                  : coverPicPlaceholder
              }
              jumboImage={
                safe(
                  mediaData.filter(d => d.media_type === 'logo'),
                  '[0].media_url',
                )
                  ? safe(
                      mediaData.filter(d => d.media_type === 'logo'),
                      '[0].media_url',
                    )
                  : Profile
              }
            />

            <ProfileSection>
              <Flex column flexWidth="70%" className="profileContent">
                <Verfied name="Verified" />
                <TitleCard
                  title={providerData.business_name || ''}
                  branchName={safe(locationData, '[0].area')}
                  tagline={safe(basicInfoData, '[0].tag_line') || ''}
                  isWishlisted={isWishlisted === 'true'}
                  isLoggedIn={isLoggedIn}
                  onWishlistIconClick={e => {
                    if (isWishlisted !== 'true') {
                      postWishlist(providerData.id, 'search');
                      queryChange(e, true);
                      setIsWishlisted('true');
                    } else {
                      deleteWishlist(providerData.id, 'search');
                      queryChange(e, false);
                      setIsWishlisted('false');
                    }
                  }}
                />
                <SimpleBar style={{ maxWidth: '100%' }}>
                  <Flex
                    flexWidth="100%"
                    // flexoverflow="scroll"
                    className="profileDetails"
                  >
                    <ProfileDetails
                      heading="ORGANISATION"
                      data1={'Preschool'}
                      data2={'Day Care'}
                      margin="0px 50px 0px 0px"
                      isSingle={
                        (providerData.child_care_providers === '1' ||
                          providerData.child_care_providers === '3') &&
                        (providerData.child_care_providers === '2' ||
                          providerData.child_care_providers === '3')
                      }
                      isPreschool={
                        providerData.child_care_providers === '1' ||
                        providerData.child_care_providers === '3'
                      }
                      isDaycare={
                        providerData.child_care_providers === '2' ||
                        providerData.child_care_providers === '3'
                      }
                    />
                    <ProfileDetails
                      isPreschool={
                        providerData.child_care_providers === '1' ||
                        providerData.child_care_providers === '3'
                      }
                      isDaycare={
                        providerData.child_care_providers === '2' ||
                        providerData.child_care_providers === '3'
                      }
                      isSingle={
                        (providerData.child_care_providers === '1' ||
                          providerData.child_care_providers === '3') &&
                        (providerData.child_care_providers === '2' ||
                          providerData.child_care_providers === '3')
                      }
                      heading="AGE GROUP"
                      data1={
                        providerData.child_care_providers === '1' ||
                        providerData.child_care_providers === '3'
                          ? `${Math.floor(
                              safe(
                                basicInfoData,
                                '[0].preschool_children_min_age',
                              ) / 12,
                            )}yrs ${safe(
                              basicInfoData,
                              '[0].preschool_children_min_age',
                            ) % 12}m - ${Math.floor(
                              safe(
                                basicInfoData,
                                '[0].preschool_children_max_age',
                              ) / 12,
                            )}yrs ${safe(
                              basicInfoData,
                              '[0].preschool_children_max_age',
                            ) % 12}m`
                          : '---'
                      }
                      data2={
                        providerData.child_care_providers === '2' ||
                        providerData.child_care_providers === '3'
                          ? `${Math.floor(
                              safe(basicInfoData, '[0].children_min_age') / 12,
                            )}yrs ${safe(
                              basicInfoData,
                              '[0].children_min_age',
                            ) % 12}m - ${Math.floor(
                              safe(basicInfoData, '[0].children_max_age') / 12,
                            )}yrs ${safe(
                              basicInfoData,
                              '[0].children_max_age',
                            ) % 12}m`
                          : '---'
                      }
                      margin="0px 50px 0px 0px"
                    />

                    <ProfileDetails
                      isPreschool={
                        providerData.child_care_providers === '1' ||
                        providerData.child_care_providers === '3'
                      }
                      isDaycare={
                        providerData.child_care_providers === '2' ||
                        providerData.child_care_providers === '3'
                      }
                      isSingle={
                        (providerData.child_care_providers === '1' ||
                          providerData.child_care_providers === '3') &&
                        (providerData.child_care_providers === '2' ||
                          providerData.child_care_providers === '3')
                      }
                      heading="TIMING"
                      data1={
                        (providerData.child_care_providers === '1' ||
                          providerData.child_care_providers === '3') &&
                        workingDayData.filter(
                          d => d.school_type === 'preschool',
                        ).length > 0
                          ? `Mon - Fri | ${moment(
                              safe(
                                workingDayData.filter(
                                  d => d.school_type === 'preschool',
                                ),
                                '[0].from_hour',
                              ),
                              'hh:mm:ss',
                            ).format('hh:mm A')} -
                    ${moment(
                      safe(
                        workingDayData.filter(
                          d => d.school_type === 'preschool',
                        ),
                        '[0].to_hour',
                      ),
                      'hh:mm:ss',
                    ).format('hh:mm A')} `
                          : '---'
                      }
                      data2={
                        (providerData.child_care_providers === '2' ||
                          providerData.child_care_providers === '3') &&
                        workingDayData.filter(d => d.school_type === 'day_care')
                          .length > 0
                          ? ` Mon - Fri | ${moment(
                              safe(
                                workingDayData.filter(
                                  d => d.school_type === 'day_care',
                                ),
                                '[0].from_hour',
                              ),
                              'hh:mm:ss',
                            ).format('hh:mm A')} -
                    ${moment(
                      safe(
                        workingDayData.filter(
                          d => d.school_type === 'day_care',
                        ),
                        '[0].to_hour',
                      ),
                      'hh:mm:ss',
                    ).format('hh:mm A')}`
                          : '---'
                      }
                      margin="0px 50px 0px 0px"
                    />

                    <ProfileDetails
                      isPreschool={
                        providerData.child_care_providers === '1' ||
                        providerData.child_care_providers === '3'
                      }
                      isDaycare={
                        providerData.child_care_providers === '2' ||
                        providerData.child_care_providers === '3'
                      }
                      isSingle={
                        (providerData.child_care_providers === '1' ||
                          providerData.child_care_providers === '3') &&
                        (providerData.child_care_providers === '2' ||
                          providerData.child_care_providers === '3')
                      }
                      margin="0px"
                      data1={
                        providerData.child_care_providers === '1' ||
                        providerData.child_care_providers === '3'
                          ? basicInfoData &&
                            basicInfoData[0] &&
                            basicInfoData[0].preschool_schedule.length > 0
                            ? scheduleChoiceData
                                .map(
                                  d =>
                                    (
                                      safe(
                                        basicInfoData,
                                        '[0].preschool_schedule',
                                      ) || []
                                    ).includes(d.id) && d.name,
                                )
                                .filter(d => d)
                                .toString()
                                .split(',')
                                .join('/')
                            : '---'
                          : '---'
                      }
                      data2={
                        providerData.child_care_providers === '2' ||
                        providerData.child_care_providers === '3'
                          ? basicInfoData &&
                            basicInfoData[0] &&
                            basicInfoData[0].schedule.length > 0
                            ? scheduleChoiceData
                                .map(
                                  d =>
                                    (
                                      safe(basicInfoData, '[0].schedule') || []
                                    ).includes(d.id) && d.name,
                                )
                                .filter(d => d)
                                .toString()
                                .split(',')
                                .join('/')
                            : '---'
                          : '---'
                      }
                      heading="SCHEDULE"
                    />
                  </Flex>
                </SimpleBar>
                {global.window && window.innerWidth > 500 ? (
                  <Flex wrap flexWidth="100%" className="enquiryButton">
                    <Button
                      text="Schedule Tour"
                      type="profile"
                      marginRight="10px"
                      marginTop="10px"
                      // isLoading={scheduleCheckLoader}
                      onClick={() => {
                        if (isLoggedIn) {
                          checkTourExists(providerData.id);
                        } else {
                          setType('login');
                          setActive(true);
                        }
                      }}
                    />
                    <Button
                      text="Enquire"
                      type="profile"
                      marginRight="10px"
                      marginTop="10px"
                      onClick={() => {
                        if (isLoggedIn) {
                          setType('thankyou');
                          setActive(true);
                        } else {
                          setType('login');
                          setActive(true);
                        }
                      }}
                    />
                    <Button
                      text="Connect with Counsellor"
                      type="profile"
                      marginRight="10px"
                      marginTop="10px"
                      onClick={() => {
                        if (isLoggedIn) {
                          USER_CONNECT_API_CALL({
                            payload: {
                              provider: providerData.id,
                              connect_type: 'counselor',
                            },
                            errorCallback: ({ message }) => {
                              errorToast(message || 'Something Went wrong');
                            },
                            successCallback: () => {
                              if (isLoggedIn) {
                                setType('enquiry');
                                setEnquiryModalTitle(
                                  'Thank you! Your request to connect with counsellor has been sent',
                                );
                                setActive(true);
                              } else {
                                setType('login');
                                setActive(true);
                              }
                            },
                          });
                        } else {
                          setType('login');
                          setActive(true);
                        }
                      }}
                    />
                    <Button
                      text="Connect with Parent"
                      type="profile"
                      marginRight="10px"
                      marginTop="10px"
                      onClick={() => {
                        if (isLoggedIn) {
                          USER_CONNECT_API_CALL({
                            payload: {
                              provider: providerData.id,
                              connect_type: 'parent',
                            },
                            errorCallback: ({ message }) => {
                              errorToast(message || 'Something Went wrong');
                            },
                            successCallback: () => {
                              if (isLoggedIn) {
                                setType('enquiry');
                                setEnquiryModalTitle(
                                  'Thank you! Your request to connect with parent has been sent',
                                );
                                setActive(true);
                              } else {
                                setType('login');
                                setActive(true);
                              }
                            },
                          });
                        } else {
                          setType('login');
                          setActive(true);
                        }
                      }}
                    />
                  </Flex>
                ) : (
                  <FlexBottomFixed
                    left="9px"
                    wrap
                    flexWidth="100%"
                    className="enquiryButton"
                  >
                    <Button
                      text="Call"
                      type="profile"
                      marginRight="5px"
                      display="inline"
                      onClick={() => {
                        if (isLoggedIn) {
                          USER_ENQUIRY_API_CALL({
                            payload: {
                              provider: providerData.id,
                              enquire: 'Call Lead.',
                            },
                            errorCallback: ({ message }) => {
                              // errorToast(message || 'Something Went wrong');
                            },
                            successCallback: () => {
                              successToast('Successfully Connected.');
                              closeHandler();
                            },
                          });
                          window.parent.location.href =
                            'tel:+91' + contactData.mobile_number;
                          // 'tel:+91' + providerData.phone_number;
                        } else {
                          setType('login');
                          setActive(true);
                        }
                      }}
                    />
                    <Button
                      text="Schedule Tour"
                      type="profile"
                      display="inline"
                      onClick={() => {
                        if (isLoggedIn) {
                          checkTourExists(providerData.id);
                        } else {
                          setType('login');
                          setActive(true);
                        }
                      }}
                    />
                  </FlexBottomFixed>
                )}
              </Flex>

              {(isLoggedIn || BY_PASS_SLUGS.includes(id)) &&
              iframeRenderer() &&
              iframeSrc() ? (
                <Flex column flexWidth="30%">
                  <SubHeading text="Virtual Tour" margin="0px 0px 20px" />
                  <Image360>
                    <button
                      className="buttonVirtual"
                      style={{
                        width: '30px',
                        height: '30px',
                        position: 'relative',
                        top:
                          virtualTourService === 'yes' ||
                          (virtualTourService === 'no' &&
                            !safe(
                              mediaImages.filter(
                                d => d.media_type === 'photo360',
                              ),
                              '[0].media_url',
                            ))
                            ? '40px'
                            : '30%',
                        left:
                          virtualTourService === 'yes' ||
                          (virtualTourService === 'no' &&
                            !safe(
                              mediaImages.filter(
                                d => d.media_type === 'photo360',
                              ),
                              '[0].media_url',
                            ))
                            ? '10px'
                            : '4px',
                      }}
                      onClick={() => setIsFullScreen(true)}
                    >
                      <img
                        style={{
                          height: '15px',
                          width: '15px',
                          position: 'relative',
                          top: '-1px',
                        }}
                        src={fullscreen}
                        alt=""
                      />
                    </button>
                    {virtualTourService === 'no' && (
                      <ImageTabs>
                        {mediaImages
                          .filter(d => d.media_type === 'photo360')
                          .map((d, i) => (
                            <Button
                              text={buttonNameGen(d.media_url)}
                              type="profile"
                              marginRight="10px"
                              onClick={() => setCurrentTourSlide(i)}
                            />
                          ))}
                      </ImageTabs>
                    )}
                    {iframeRenderer()}
                  </Image360>
                </Flex>
              ) : null}
            </ProfileSection>
            <MobileTabs
              className={mobileNavFixed ? 'fixed' : ''}
              id="mobileTab"
            >
              <li
                className={activeMobileTab === 'about' ? 'active' : ''}
                onClick={() => {
                  setActiveMobileTab('about');
                  scrollTo(748);
                }}
              >
                About us
              </li>
              <li
                className={activeMobileTab === 'basic' ? 'active' : ''}
                onClick={() => {
                  setActiveMobileTab('basic');
                  scrollTo(748);
                }}
              >
                Basic Info
              </li>
              <li
                className={activeMobileTab === 'photos' ? 'active' : ''}
                onClick={() => {
                  setActiveMobileTab('photos');
                  scrollTo(748);
                }}
              >
                Gallery
              </li>
              <li
                className={activeMobileTab === 'contactInfo' ? 'active' : ''}
                onClick={() => {
                  setActiveMobileTab('contactInfo');
                  scrollTo(748);
                }}
              >
                Contact us
              </li>
            </MobileTabs>

            <ProfileGrid id="content-conatiner">
              {!isLoggedIn && !BY_PASS_SLUGS.includes(id) ? (
                <>
                  <div style={{ width: '78%' }}>
                    <img
                      src={profilePlaceholder}
                      width="100%"
                      style={{ filter: `blur(4px)` }}
                    />
                  </div>
                  {/* <div style={{ ...signInPrompt }}> */}
                  <AdditionalFilterWrapper
                    className={isAccessBlockerModalFixed ? 'fixed' : ''}
                  >
                    <ModalLeft>
                      <ModalTitle className="creatAccount">
                        Help us serve you better!
                      </ModalTitle>
                      <ModalSubTitle>
                        By logging in you will be able to get in touch with the
                        school and get exclusive information regarding the
                        provider.
                      </ModalSubTitle>
                      <div className="scheduleTour">
                        <Button
                          text="Login"
                          type="mobile"
                          onClick={() => {
                            setType('login');
                            setActive(true);
                          }}
                        />
                      </div>
                    </ModalLeft>
                    <ModalRight>
                      <img src={welcomeImage} alt="" height={215} />
                    </ModalRight>
                  </AdditionalFilterWrapper>
                  {/* </div> */}
                </>
              ) : (
                <>
                  <div className="gridColumn">
                    <div
                      className="gridItem"
                      style={
                        activeMobileTab !== 'basic' && mobileView
                          ? displayNone
                          : {}
                      }
                    >
                      <SubHeading text="Additional Information" />
                      <Flex column flexMargin="20px 0px">
                        <AdditionalInfo>
                          <img src={year} alt="" />
                          Year of Establishment :
                          <span>
                            {' '}
                            {safe(basicInfoData, '[0].year_of_establishment')}
                          </span>
                        </AdditionalInfo>
                        {providerData.license_number && (
                          <AdditionalInfo>
                            <img src={license} alt="" />
                            License Number :
                            <span> {providerData.license_number}</span>
                            <img className="license" src={licenseTag} alt="" />
                          </AdditionalInfo>
                        )}
                        {/* TODO: TEMPORARY */}
                        {/* <AdditionalInfo>
      <img src={join} alt="" />
      Joined Kiddenz on:<span> 28th Nov 2019</span>
    </AdditionalInfo> */}
                        {(providerData.child_care_providers === '2' ||
                          providerData.child_care_providers === '3') && (
                          <AdditionalInfo>
                            <img src={daycareCap} alt="" />
                            Day Care Capacity :
                            <span>
                              {' '}
                              {providerData.child_care_providers === '2' ||
                              providerData.child_care_providers === '3'
                                ? safe(
                                    basicInfoData,
                                    '[0].day_care_total_capacity',
                                  )
                                : ''}
                            </span>
                          </AdditionalInfo>
                        )}
                        {(providerData.child_care_providers === '1' ||
                          providerData.child_care_providers === '3') && (
                          <AdditionalInfo>
                            <img src={preschoolCap} alt="" />
                            Preschool Capacity
                            {' : '}
                            <span>
                              {' '}
                              {providerData.child_care_providers === '1' ||
                              providerData.child_care_providers === '3'
                                ? safe(
                                    basicInfoData,
                                    '[0].preschool_total_capacity',
                                  )
                                : ''}
                            </span>
                          </AdditionalInfo>
                        )}
                      </Flex>
                    </div>

                    {safe(basicInfoData, '[0].achievement', '') &&
                      safe(basicInfoData, '[0].achievement', '') !== '' && (
                        <div
                          className="gridItem"
                          style={
                            activeMobileTab !== 'basic' && mobileView
                              ? displayNone
                              : {}
                          }
                        >
                          <SubHeading text="Achievements" />
                          <Flex column flexMargin="30px 0px">
                            {safe(basicInfoData, '[0].achievement', '')
                              .split(',')
                              .map((d, i) => (
                                <Schedule padding="0px 0px 10px 0px">
                                  <img src={ACHIEVEMENT_ICONS[i % 2]} alt="" />
                                  <span>{d}</span>
                                </Schedule>
                              ))}
                          </Flex>
                        </div>
                      )}

                    {safe(basicInfoData, '[0].preschool_programme', '') &&
                      safe(basicInfoData, '[0].preschool_programme', '') !==
                        '' && (
                        <>
                          <div
                            className="gridItem"
                            style={
                              activeMobileTab !== 'basic' && mobileView
                                ? displayNone
                                : {}
                            }
                          >
                            <SubHeading text="Programms offered" />
                            <Flex column flexMargin="30px 0px">
                              {safe(
                                basicInfoData,
                                '[0].preschool_programme',
                                '',
                              )
                                .split(',')
                                .map(d => (
                                  <Facility>{d}</Facility>
                                ))}
                            </Flex>
                          </div>
                        </>
                      )}

                    {safe(facilitiesData, '[0].courses', '') === 'custom' && (
                      <>
                        <div
                          className="gridItem"
                          style={
                            activeMobileTab !== 'basic' && mobileView
                              ? displayNone
                              : {}
                          }
                        >
                          <SubHeading text="Curriculum followed" />
                          <Flex column flexMargin="30px 0px">
                            {safe(
                              facilitiesData,
                              '[0].custom_course_text',
                              '',
                            ) &&
                            safe(
                              facilitiesData,
                              '[0].custom_course_text',
                              '',
                            ).split(',').length > 0 ? (
                              safe(facilitiesData, '[0].custom_course_text', '')
                                .split(',')
                                .slice(
                                  0,
                                  coursesShowMore
                                    ? safe(
                                        facilitiesData,
                                        '[0].custom_course_text',
                                        '',
                                      ).split(',').length
                                    : 6,
                                )
                                .map(d => <Facility>{d}</Facility>)
                            ) : (
                              <Facility>
                                {safe(
                                  facilitiesData,
                                  '[0].custom_course_text',
                                  '',
                                )}
                              </Facility>
                            )}

                            {safe(
                              facilitiesData,
                              '[0].custom_course_text',
                              '',
                            ) &&
                              safe(
                                facilitiesData,
                                '[0].custom_course_text',
                                '',
                              ).split(',').length > 6 && (
                                <>
                                  {!coursesShowMore && (
                                    <Flex flexMargin="10px 0px 0px">
                                      <Button
                                        text="Show more"
                                        type="viewAll"
                                        onClick={() => setCoursesShowMore(true)}
                                      />
                                    </Flex>
                                  )}
                                  {coursesShowMore && (
                                    <Flex flexMargin="10px 0px 0px">
                                      <Button
                                        text="Show less"
                                        type="viewAll"
                                        onClick={() =>
                                          setCoursesShowMore(false)
                                        }
                                      />
                                    </Flex>
                                  )}
                                </>
                              )}
                          </Flex>
                        </div>
                      </>
                    )}
                    {safe(facilitiesData, '[0].courses', '') !== 'custom' && (
                      <>
                        <div
                          className="gridItem"
                          style={
                            activeMobileTab !== 'basic' && mobileView
                              ? displayNone
                              : {}
                          }
                        >
                          <SubHeading text="Curriculum" />
                          <Flex column flexMargin="30px 0px">
                            <Facility>
                              {safe(facilitiesData, '[0].courses', '')}
                            </Facility>
                          </Flex>
                        </div>
                      </>
                    )}

                    <div
                      className="gridItem"
                      style={
                        activeMobileTab !== 'basic' && mobileView
                          ? displayNone
                          : {}
                      }
                    >
                      <SubHeading text="Syllabus and Activities" />
                      <Flex column flexMargin="30px 0px">
                        <Flex wrap alignCenter>
                          {safe(basicInfoData, '[0].activity', '') &&
                          safe(basicInfoData, '[0].activity', '').split(',')
                            .length > 0 ? (
                            safe(basicInfoData, '[0].activity', '')
                              .split(',')
                              .slice(
                                0,
                                activityShowMore
                                  ? safe(
                                      basicInfoData,
                                      '[0].activity',
                                      '',
                                    ).split(',').length
                                  : 6,
                              )
                              .map(d => <Syllabus text={d} />)
                          ) : (
                            <Syllabus
                              text={safe(basicInfoData, '[0].activity', '')}
                            />
                          )}
                        </Flex>
                        {safe(basicInfoData, '[0].activity', '') &&
                          safe(basicInfoData, '[0].activity', '').split(',')
                            .length > 6 && (
                            <>
                              {!activityShowMore && (
                                <Flex flexMargin="10px 0px 0px">
                                  <Button
                                    text="Show more"
                                    type="viewAll"
                                    onClick={() => setActivityShowMore(true)}
                                  />
                                </Flex>
                              )}
                              {activityShowMore && (
                                <Flex
                                  flexHeight="fit-content"
                                  flexMargin="10px 0px 0px"
                                >
                                  <Button
                                    text="Show less"
                                    type="viewAll"
                                    onClick={() => setActivityShowMore(false)}
                                  />
                                </Flex>
                              )}
                            </>
                          )}
                      </Flex>
                    </div>

                    {scheduleForDayData.sort((a, b) => {
                      const first = +a.start_time.split(':')[0];
                      const second = +b.start_time.split(':')[0];
                      return first - second;
                    }).length > 0 && (
                      <div
                        className="gridItem"
                        style={
                          activeMobileTab !== 'basic' && mobileView
                            ? displayNone
                            : {}
                        }
                      >
                        <SubHeading text="Schedule for the Day" />
                        <Flex column flexMargin="30px 0px">
                          {scheduleForDayData
                            .sort((a, b) => {
                              const first = +a.start_time.split(':')[0];
                              const second = +b.start_time.split(':')[0];
                              return first - second;
                            })
                            .slice(
                              0,
                              scheduleShowMore ? scheduleForDayData.length : 6,
                            )
                            .map(d => (
                              <Schedule>
                                <span>
                                  {moment(d.start_time, 'hh:mm:ss').format(
                                    'hh:mm A',
                                  )}
                                </span>
                                <span>{d.programm}</span>
                              </Schedule>
                            ))}
                          {scheduleForDayData.length > 6 && (
                            <>
                              {!scheduleShowMore && (
                                <Flex flexMargin="10px 0px 0px">
                                  <Button
                                    text="Show more"
                                    type="viewAll"
                                    onClick={() => setScheduleShowMore(true)}
                                  />
                                </Flex>
                              )}
                              {scheduleShowMore && (
                                <Flex flexMargin="10px 0px 0px">
                                  <Button
                                    text="Show less"
                                    type="viewAll"
                                    onClick={() => setScheduleShowMore(false)}
                                  />
                                </Flex>
                              )}
                            </>
                          )}
                        </Flex>
                      </div>
                    )}
                    {(dayCareCalenderData.length > 0 ||
                      preschoolCalenderData.length > 0) && (
                      <>
                        {(providerData.child_care_providers === '2' ||
                          providerData.child_care_providers === '3') &&
                          dayCareCalenderData.length > 0 && (
                            <>
                              <div
                                className="gridItem"
                                style={
                                  activeMobileTab !== 'basic' && mobileView
                                    ? displayNone
                                    : {}
                                }
                              >
                                <SubHeading
                                  text={`Day Care Calendar (${
                                    dayCareCalenderData[0].year
                                  })`}
                                />
                                <Flex column flexMargin="30px 0px">
                                  {dayCareCalenderData
                                    .sort((a, b) => {
                                      const first = new Date(
                                        a.start_date,
                                      ).getTime();
                                      const second = new Date(
                                        b.start_date,
                                      ).getTime();
                                      return first - second;
                                    })
                                    .slice(
                                      0,
                                      dayCareCalShowMore
                                        ? dayCareCalenderData.length
                                        : 6,
                                    )
                                    .map(d => (
                                      <Schedule className="calendar">
                                        <span>
                                          {d.start_date === d.to_date
                                            ? moment(d.start_date).format(
                                                'MMM DD',
                                              )
                                            : moment(d.start_date).format(
                                                'MMM',
                                              ) ===
                                              moment(d.to_date).format('MMM')
                                              ? `${moment(d.start_date).format(
                                                  'MMM DD',
                                                )}
                        - ${moment(d.to_date).format('DD')}`
                                              : `${moment(d.start_date).format(
                                                  'MMM DD',
                                                )}
                        - ${moment(d.to_date).format('MMM DD')}`}
                                        </span>
                                        <span>{d.event}</span>
                                      </Schedule>
                                    ))}
                                  {dayCareCalenderData.length > 6 && (
                                    <>
                                      {!dayCareCalShowMore && (
                                        <Flex flexMargin="10px 0px 0px">
                                          <Button
                                            text="Show more"
                                            type="viewAll"
                                            onClick={() =>
                                              setDayCareCalShowMore(true)
                                            }
                                          />
                                        </Flex>
                                      )}
                                      {dayCareCalShowMore && (
                                        <Flex flexMargin="10px 0px 0px">
                                          <Button
                                            text="Show less"
                                            type="viewAll"
                                            onClick={() =>
                                              setDayCareCalShowMore(false)
                                            }
                                          />
                                        </Flex>
                                      )}
                                    </>
                                  )}
                                </Flex>
                              </div>
                            </>
                          )}
                        {(providerData.child_care_providers === '1' ||
                          providerData.child_care_providers === '3') &&
                          preschoolCalenderData.length > 0 && (
                            <>
                              <div
                                className="gridItem"
                                style={
                                  activeMobileTab !== 'basic' && mobileView
                                    ? displayNone
                                    : {}
                                }
                              >
                                <SubHeading
                                  text={`Preschool Calendar (${
                                    preschoolCalenderData[0].year
                                  })`}
                                />
                                <Flex column flexMargin="30px 0px">
                                  {preschoolCalenderData
                                    .sort((a, b) => {
                                      const first = new Date(
                                        a.start_date,
                                      ).getTime();
                                      const second = new Date(
                                        b.start_date,
                                      ).getTime();
                                      return first - second;
                                    })
                                    .slice(
                                      0,
                                      preschoolCalShowMore
                                        ? preschoolCalenderData.length
                                        : 6,
                                    )
                                    .map(d => (
                                      <Schedule className="calendar">
                                        <span>
                                          {d.start_date === d.to_date
                                            ? moment(d.start_date).format(
                                                'MMM DD',
                                              )
                                            : moment(d.start_date).format(
                                                'MMM',
                                              ) ===
                                              moment(d.to_date).format('MMM')
                                              ? `${moment(d.start_date).format(
                                                  'MMM DD',
                                                )}
                      - ${moment(d.to_date).format('DD')}`
                                              : `${moment(d.start_date).format(
                                                  'MMM DD',
                                                )}
                        - ${moment(d.to_date).format('MMM DD')}`}
                                        </span>
                                        <span>{d.event}</span>
                                      </Schedule>
                                    ))}
                                  {preschoolCalenderData.length > 6 && (
                                    <>
                                      {!preschoolCalShowMore && (
                                        <Flex flexMargin="10px 0px 0px">
                                          <Button
                                            text="Show more"
                                            type="viewAll"
                                            onClick={() =>
                                              setPreschoolCalShowMore(true)
                                            }
                                          />
                                        </Flex>
                                      )}
                                      {preschoolCalShowMore && (
                                        <Flex flexMargin="10px 0px 0px">
                                          <Button
                                            text="Show less"
                                            type="viewAll"
                                            onClick={() =>
                                              setPreschoolCalShowMore(false)
                                            }
                                          />
                                        </Flex>
                                      )}
                                    </>
                                  )}
                                </Flex>
                              </div>
                            </>
                          )}
                      </>
                    )}

                    {/* {(dayCareData.length > 0 || preschoolData.length > 0) && (
  <div className="gridItem">
    {(providerData.child_care_providers === '2' ||
      providerData.child_care_providers === '3') &&
      dayCareData.length > 0 && (
        <>
          <SubHeading text="Day Care Working days" />
          <Flex column flexMargin="30px 0px">
            {dayCareData.map(d => (
              <Schedule>
                <span>
                  {(d.weekdays || '').split('_').join('-')}
                </span>
                <span>
                  {moment(d.from_hour, 'hh:mm:ss').format(
                    'hh:mm A',
                  )}{' '}
                  -{' '}
                  {moment(d.to_hour, 'hh:mm:ss').format(
                    'hh:mm A',
                  )}
                </span>
              </Schedule>
            ))}
          </Flex>
        </>
      )}
    {(providerData.child_care_providers === '1' ||
      providerData.child_care_providers === '3') &&
      preschoolData.length > 0 && (
        <>
          <SubHeading text="Pre School Working days" />
          {preschoolData.map(d => (
            <>
              <p className="batchName">{d || ''}</p>
              <Flex column flexMargin="30px 0px">
                {groupBy(
                  workingDayData.filter(
                    d => d.school_type === 'preschool',
                  ),
                  o => o.batch_slug_name,
                )[d].map(d => (
                  <Schedule>
                    <span>
                      {(d.weekdays || '').split('_').join('-')}
                    </span>
                    <span>
                      {moment(d.from_hour, 'hh:mm:ss').format(
                        'hh:mm A',
                      )}{' '}
                      -{' '}
                      {moment(d.to_hour, 'hh:mm:ss').format(
                        'hh:mm A',
                      )}
                    </span>
                  </Schedule>
                ))}
              </Flex>
            </>
          ))}
        </>
      )}
  </div>
)} */}

                    {mediaImages.length > 0 ? (
                      <div
                        className="gridItem"
                        style={
                          activeMobileTab !== 'photos' && mobileView
                            ? displayNone
                            : {}
                        }
                      >
                        <SubHeading text="Gallery" />
                        <GalleryWrapper wrap flexMargin="30px 0px">
                          {mediaImages
                            .filter(d => d.media_type !== 'photo360')
                            .slice(0, 6)
                            .map((d, i) => (
                              <PhotoCard
                                image={d.media_url}
                                width="48%"
                                type={i === 5 ? 'viewall' : ''}
                                imageAlt={
                                  providerData.business_name +
                                  '-' +
                                  safe(locationData, '[0].area')
                                }
                                onClick={
                                  i === 5
                                    ? () => {
                                        setType('gallery');
                                        setActive(true);
                                      }
                                    : () => {
                                        setType('gallery');
                                        setActive(true);
                                        setMediaType(d.media_type);
                                      }
                                }
                                totalPhotos={
                                  i === 5
                                    ? (mediaImages.length > 0
                                        ? mediaImages.filter(
                                            d => d.media_type !== 'photo360',
                                          )
                                        : mediaResponse.filter(
                                            d => d.media_type !== 'photo360',
                                          )
                                      ).length
                                    : ''
                                }
                              />
                            ))}
                        </GalleryWrapper>
                        <div>
                        {mediaImages
                            .filter(d => d.media_type !== 'photo360')
                            .map((d, i) => (
                              <img src={d.media_url} alt={
                                providerData.business_name +
                                '-' +
                                safe(locationData, '[0].area')
                              }  style={{ display: 'none' }}/>
                            ))}
                        </div>
                      </div>
                    ) : null}
                  </div>



                  <div className="gridColumn">
                    <div
                      className="gridItem"
                      style={
                        activeMobileTab !== 'about' && mobileView
                          ? displayNone
                          : {}
                      }
                    >
                      <SubHeading
                        text={`Words from ${
                          providerData.director_designation
                            ? (providerData.director_designation ===
                              'central_head'
                                ? 'Center_head'
                                : providerData.director_designation ===
                                  'center_head'
                                  ? 'Center_head'
                                  : providerData.director_designation ===
                                    'director'
                                    ? 'Director'
                                    : ''
                              )
                                .split('_')
                                .join(' ')
                            : ''
                        }`}
                        margin="0px 0px 20px"
                        fontSize="22px"
                      />
                      <Director
                        name={
                          providerData.director_designation ===
                            'central_head' ||
                          providerData.director_designation === 'center_head'
                            ? providerData.name || ''
                            : providerData.director_name || providerData.name
                        }
                        img={providerData.director_photo_url}
                        job={
                          providerData.director_designation
                            ? (providerData.director_designation ===
                              'central_head'
                                ? 'Center_head'
                                : providerData.director_designation ===
                                  'center_head'
                                  ? 'Center_head'
                                  : providerData.director_designation ===
                                    'director'
                                    ? 'Director'
                                    : ''
                              )
                                .split('_')
                                .join(' ')
                            : ''
                        }
                        comment={`${
                          providerData.words_from_director
                            ? providerData.words_from_director
                                .charAt(0)
                                .toUpperCase() +
                              providerData.words_from_director.slice(1)
                            : '' || ''
                        }`}
                      />
                    </div>

                    <div
                      className="gridItem"
                      style={
                        activeMobileTab !== 'about' && mobileView
                          ? displayNone
                          : {}
                      }
                    >
                      <SubHeading text="About our Program" fontSize="22px" />
                      <Flex>
                        <ParaGraph>
                          {providerData.about_school
                            ? providerData.about_school
                                .charAt(0)
                                .toUpperCase() +
                              providerData.about_school.slice(1)
                            : '' || ''}
                        </ParaGraph>
                      </Flex>
                    </div>

                    <div
                      className="gridItem"
                      style={
                        activeMobileTab !== 'about' && mobileView
                          ? displayNone
                          : {}
                      }
                    >
                      <SubHeading
                        text="Facilities"
                        marginBottomXs="10px"
                        fontSize="22px"
                      />
                      <Flex wrap flexMargin="20px 0px" justifyBetween>
                        <FacilitiesItem column>
                          {manjorFacilitiesArrayOne.map(d => (
                            <Facility
                              height="42px"
                              padding="0px 0px 0px 34px"
                              style={{ textTransform: 'capitalize' }}
                            >
                              <img
                                src={
                                  FACILITIES_ICON_CONFIG[d.split('_').join(' ')]
                                    ? FACILITIES_ICON_CONFIG[
                                        d.split('_').join(' ')
                                      ]
                                    : emergency
                                }
                                alt=""
                              />
                              {d.split('_').join(' ')}
                            </Facility>
                          ))}
                        </FacilitiesItem>

                        <FacilitiesItem column>
                          {manjorFacilitiesArrayTwo.map(d => (
                            <Facility
                              height="42px"
                              padding="0px 0px 0px 34px"
                              style={{ textTransform: 'capitalize' }}
                            >
                              <img
                                src={
                                  FACILITIES_ICON_CONFIG[d.split('_').join(' ')]
                                    ? FACILITIES_ICON_CONFIG[
                                        d.split('_').join(' ')
                                      ]
                                    : emergency
                                }
                                alt=""
                              />
                              {d.split('_').join(' ')}
                            </Facility>
                          ))}
                        </FacilitiesItem>
                      </Flex>
                    </div>

                    <div
                      className="gridItem"
                      style={
                        activeMobileTab !== 'about' && mobileView
                          ? displayNone
                          : {}
                      }
                    >
                      <SubHeading
                        text="Additional Facilities"
                        marginBottomXs="10px"
                        fontSize="22px"
                      />
                      <Flex wrap flexMargin="30px 0px" justifyBetween>
                        <Flex column flexWidth="45%" className="weProvide">
                          <span
                            style={{
                              color: '#5fb947',
                            }}
                          >
                            Yes, we provide
                          </span>
                          {yesWeProvideFacilities
                            .slice(
                              0,
                              yesWeProvideShowMore
                                ? yesWeProvideFacilities.length
                                : 6,
                            )
                            .map(d => (
                              <Facility
                                height="42px"
                                padding="0px 0px 0px 34px"
                                style={{ textTransform: 'capitalize' }}
                              >
                                <img
                                  src={
                                    FACILITIES_ICON_CONFIG[d]
                                      ? FACILITIES_ICON_CONFIG[d]
                                      : general
                                  }
                                  alt=""
                                />
                                {CUSTOM_FACILITY_TEXT[d]
                                  ? CUSTOM_FACILITY_TEXT[d]
                                  : d}
                              </Facility>
                            ))}
                          {yesWeProvideFacilities.length > 6 && (
                            <>
                              {!yesWeProvideShowMore && (
                                <Flex>
                                  <Button
                                    text="Show more"
                                    type="viewAll"
                                    onClick={() =>
                                      setYesWeProvideShowMore(true)
                                    }
                                  />
                                </Flex>
                              )}
                              {yesWeProvideShowMore && (
                                <Flex>
                                  <Button
                                    text="Show less"
                                    type="viewAll"
                                    onClick={() =>
                                      setYesWeProvideShowMore(false)
                                    }
                                  />
                                </Flex>
                              )}
                            </>
                          )}
                        </Flex>

                        <Flex
                          column
                          flexWidth="45%"
                          flexMargin="0px 0px 0px 30px"
                          className="dontProvide"
                        >
                          <span
                            style={{
                              color: '#ee5757',
                            }}
                          >
                            No, we don't provide
                          </span>
                          {nowDontProvide.length > 0
                            ? nowDontProvide
                                .slice(
                                  0,
                                  noWeProvideShowMore
                                    ? nowDontProvide.length
                                    : 6,
                                )
                                .map(d => (
                                  <Facility
                                    height="42px"
                                    padding="0px 0px 0px 34px"
                                    style={{ textTransform: 'capitalize' }}
                                  >
                                    <img
                                      src={
                                        FACILITIES_ICON_CONFIG[d]
                                          ? FACILITIES_ICON_CONFIG[d]
                                          : restricitons
                                      }
                                      alt=""
                                    />
                                    {CUSTOM_FACILITY_TEXT[d]
                                      ? CUSTOM_FACILITY_TEXT[d]
                                      : d}
                                  </Facility>
                                ))
                            : ''}

                          {nowDontProvide.length > 6 && (
                            <>
                              {!noWeProvideShowMore && (
                                <Flex>
                                  <Button
                                    text="Show more"
                                    type="viewAll"
                                    onClick={() => setNoWeProvideShowMore(true)}
                                  />
                                </Flex>
                              )}
                              {noWeProvideShowMore && (
                                <Flex>
                                  <Button
                                    text="Show less"
                                    type="viewAll"
                                    onClick={() =>
                                      setNoWeProvideShowMore(false)
                                    }
                                  />
                                </Flex>
                              )}
                            </>
                          )}
                        </Flex>
                      </Flex>
                    </div>
                    {safe(basicInfoData, '[0].summer_classes_offered') &&
                      safe(
                        basicInfoData,
                        '[0].summer_classes_offered_description',
                      ) && (
                        <div
                          className="gridItem"
                          style={
                            activeMobileTab !== 'about' && mobileView
                              ? displayNone
                              : {}
                          }
                        >
                          <SubHeading
                            text="Summer Classes Offered"
                            marginBottomXs="10px"
                            fontSize="22px"
                          />
                          <Flex justifyBetween flexMargin="30px 0px">
                            <Flex flexWidth="48%" column>
                              {summerClassesOfferedArrayOne.map(a => (
                                <Syllabus width="100%" text={a} />
                              ))}
                            </Flex>
                            <Flex flexWidth="48%" column>
                              {summerClassesOfferedArrayTwo.map(a => (
                                <Syllabus width="100%" text={a} />
                              ))}
                            </Flex>
                          </Flex>
                        </div>
                      )}
                    {facultyData.length > 0 ? (
                      <div
                        className="gridItem teachers"
                        style={
                          activeMobileTab !== 'about' && mobileView
                            ? displayNone
                            : {}
                        }
                      >
                        <Flex justifyBetween flexMarginXs="0px 0px 20px 0px">
                          <SubHeading
                            text={
                              facultyData.filter(
                                d => d.faculty_type === 'teacher',
                              ).length > 0
                                ? 'Our Teachers'
                                : facultyData.filter(
                                    d => d.faculty_type === 'staff',
                                  ).length > 0
                                  ? 'Our Staffs'
                                  : ''
                            }
                            fontSize="22px"
                          />
                          <Button
                            text="View all"
                            type="viewAll"
                            marginRight="10px"
                            onClick={() => {
                              setType('teacher & staff');
                              setActive(true);
                            }}
                          />
                        </Flex>
                        <TeacherWrapper flexMargin="30px 0px">
                          {facultyData &&
                          facultyData.filter(d => d.faculty_type === 'teacher')
                            .length > 0
                            ? facultyData &&
                              facultyData
                                .filter(d => d.faculty_type === 'teacher')
                                .map((teach, id) => {
                                  if (id < 3) {
                                    const langFilter = languageData.filter(
                                      a => {
                                        if (
                                          teach.languages_spoken.includes(a.id)
                                        ) {
                                          return a.name;
                                        }
                                      },
                                    );
                                    const lang = langFilter.map(a => a.name);
                                    return (
                                      <TeacherDetail
                                        name={teach.name}
                                        image={
                                          teach.photo_url ||
                                          TEACHER_PLACEHOLDERS[id]
                                        }
                                        years={`${teach.experience} yrs`}
                                        desc={teach.words_from_teacher}
                                        languages={lang && lang.join(', ')}
                                        type="verified"
                                        width="33%"
                                      />
                                    );
                                  }
                                })
                            : facultyData.filter(
                                d => d.faculty_type === 'staff',
                              ).length > 0
                              ? facultyData &&
                                facultyData
                                  .filter(d => d.faculty_type === 'staff')
                                  .map((teach, id) => {
                                    if (id < 3) {
                                      const langFilter = languageData.filter(
                                        a => {
                                          if (
                                            teach.languages_spoken.includes(
                                              a.id,
                                            )
                                          ) {
                                            return a.name;
                                          }
                                        },
                                      );
                                      const lang = langFilter.map(a => a.name);
                                      return (
                                        <TeacherDetail
                                          name={teach.name}
                                          image={
                                            teach.photo_url ||
                                            STAFF_PLACEHOLDERS[id]
                                          }
                                          years={`${teach.experience} yrs`}
                                          desc={teach.words_from_teacher}
                                          languages={lang && lang.join(', ')}
                                          type="verified"
                                          width="33%"
                                        />
                                      );
                                    }
                                  })
                              : null}
                        </TeacherWrapper>
                      </div>
                    ) : null}
                  </div>
                </>
              )}
              <div
                className="gridColumn"
                style={
                  activeMobileTab !== 'contactInfo' && mobileView
                    ? displayNone
                    : {}
                }
              >
                <div className="gridItem mobileonly">
                  <SubHeading text="Contact Us" />
                  <Flex column flexMargin="30px 0px">
                    {safe(
                      locationData,
                      '[0]',
                      {},
                      ({
                        area = '',
                        city = '',
                        state = '',
                        pincode = '',
                        lattitude = 0.0,
                        longitude = 0.0,
                        ...location
                      }) => (
                        <Location
                          latitude={+lattitude}
                          longitude={+longitude}
                          address={`${location.address_line_1 ||
                            ''} ${location.address_line_2 ||
                            ''} ${area} ${city} ${state} ${pincode}`}

                          // TODO: Confim if needed
                          // number={safe(
                          //   contactData,
                          //   '[0]',
                          //   {},
                          //   contact =>
                          //     `Call the ${providerData.business_name ||
                          //       ''} hotline ${contact.mobile_number || ''}`,
                          // )}
                          // email={safe(
                          //   contactData,
                          //   '[0]',
                          //   {},
                          //   contact => contact.email_id,
                          // )}
                        />
                      ),
                    )}
                    {/* {contactData.map(
      a =>
        a.contact_type === 'director' &&
        a.name && (
          <>
            <SubHeading
              text="Director Details"
              margin="20px 0px"
            />
            <Name>{a.name}</Name>
            {(a.phone_number || a.alt_phone_number) && (
              <Text>
                Contact :{' '}
                {a.phone_number ? `${a.phone_number},` : ''}{' '}
                {a.alt_phone_number}
              </Text>
            )}
            <Email>{a.email_id}</Email>
          </>
        ),
    )} */}
                    <Button
                      text="Request for price"
                      // type="profile"
                      marginTop="10px"
                      disabled={userEnquiryLoader}
                      onClick={() => {
                        if (isLoggedIn) {
                          USER_CONNECT_API_CALL({
                            payload: {
                              provider: providerData.id,
                              connect_type: 'price',
                            },
                            errorCallback: ({ message }) => {
                              errorToast(message || 'Something Went wrong');
                            },
                            successCallback: () => {
                              setType('enquiry');
                              setEnquiryModalTitle(
                                'Your request for fee detail, has been sent ',
                              );
                              setActive(true);
                            },
                          });
                        } else {
                          setType('login');
                          setActive(true);
                        }
                      }}
                    />
                    <Button
                      text="Contact us"
                      // type="profile"
                      marginTop="10px"
                      disabled={userEnquiryLoader}
                      onClick={() => {
                        if (isLoggedIn) {
                          USER_ENQUIRY_API_CALL({
                            payload: {
                              provider: providerData.id,
                              enquire: 'Would like to contact.',
                            },
                            errorCallback: ({ message }) => {
                              errorToast(message || 'Something Went wrong');
                            },
                            successCallback: () => {
                              setType('contactUsSuccess');
                              setActive(true);
                            },
                          });
                        } else {
                          setType('login');
                          setActive(true);
                        }
                      }}
                    />

                    {isLoggedIn ? (
                      <>
                        <SubHeading text="Address" margin="20px 0px" />

                        <Name>{safe(locationData, '[0].address_line_1')}</Name>
                        <Name>{safe(locationData, '[0].address_line_2')}</Name>
                        <Name>{safe(locationData, '[0].area')}</Name>
                        <Name>
                          {safe(locationData, '[0].city')} -{' '}
                          {safe(locationData, '[0].pincode')}
                        </Name>
                        <Name>{safe(locationData, '[0].state')}</Name>
                      </>
                    ) : null}
                  </Flex>
                </div>
                {/* <div className="gridItem mobileonly">
                  <SubHeading text="Featured Pre-school" />
                  <Flex
                    column
                    flexPadding="40px 0px"
                    flexBorderdashed="1px solid #E6E8EC"
                  >
                    <SchoolCard
                      schoolImage={schoolImage1}
                      schoolName="Montessori"
                      schoolBranch={schoolbranch || 'Koramangala'}
                      schoolRating="4.3"
                      schoolDesc="8:30AM - 3:00PM"
                      photocardType="preSchool"
                      cardtype="small"
                    />
                  </Flex>
                  <Flex
                    column
                    flexPadding="40px 0px"
                    flexBorderdashed="1px solid #E6E8EC"
                  >
                    <SchoolCard
                      schoolImage={schoolImage2}
                      schoolName="Waldorf"
                      schoolBranch={schoolbranch || 'Koramangala'}
                      schoolRating="4.3"
                      schoolDesc="8:30AM - 3:00PM"
                      photocardType="preSchool"
                      cardtype="small"
                    />
                  </Flex>
                  <Flex
                    column
                    flexPadding="40px 0px"
                    flexBorderdashed="1px solid #E6E8EC"
                  >
                    <SchoolCard
                      schoolImage={schoolImage3}
                      schoolName="Reggio Emilia"
                      schoolBranch={schoolbranch || 'Koramangala'}
                      schoolRating="4.3"
                      schoolDesc="8:30AM - 3:00PM"
                      photocardType="preSchool"
                      cardtype="small"
                    />
                  </Flex>
                  <Flex
                    column
                    flexPadding="40px 0px"
                    flexBorderdashed="1px solid #E6E8EC"
                  >
                    <SchoolCard
                      schoolImage={schoolImage4}
                      schoolName="Bank Street"
                      schoolBranch={schoolbranch || 'Koramangala'}
                      schoolRating="4.3"
                      schoolDesc="8:30AM - 3:00PM"
                      photocardType="preSchool"
                      cardtype="small"
                    />
                  </Flex>
                  <Flex
                    column
                    flexPadding="40px 0px"
                    flexBorderdashed="1px solid #E6E8EC"
                  >
                    <SchoolCard
                      schoolImage={schoolImage5}
                      schoolName="HighScope"
                      schoolBranch={schoolbranch || 'Koramangala'}
                      schoolRating="4.3"
                      schoolDesc="8:30AM - 3:00PM"
                      photocardType="preSchool"
                      cardtype="small"
                    />
                  </Flex>
                </div>
                <div className="gridItem mobileonly">
                  <SubHeading text="Our other branches" />
                  <Flex column flexMargin="30px 0px">
                    <BranchName>Klay kids - Marathahalli</BranchName>
                    <BranchName>Jumpo kids - RT Nagar</BranchName>
                    <BranchName>Euro kids - KR Puram</BranchName>
                    <BranchName>Oi kids - Jayanagar</BranchName>
                  </Flex>
                </div>
               */}
              </div>
            </ProfileGrid>
            <FatFooterContainer>
          <div>
            <CityTitle>Top Cities: </CityTitle>
            <GridAutoWraper>
              <Link href="/bengaluru" className="schoolLink">
                Daycares in Bengaluru
              </Link>
              <Link href="/Bengaluru" className="schoolLink">
              Preschools in Bengaluru
              </Link>
              <Link href="/pune" className="schoolLink">
                Preschools in Pune
              </Link>
              <Link href="/pune" className="schoolLink">
                Daycares in Pune
              </Link>
              <Link href="/mumbai" className="schoolLink">
                Preschools in Mumbai
              </Link>
              <Link href="/mumbai" className="schoolLink">
                Daycares in Mumbai
              </Link>
              <Link href="/noida" className="schoolLink">
                Preschools in Noida
              </Link>
              <Link href="/noida" className="schoolLink">
                Daycares in Noida
              </Link>
              <Link href="/new delhi" className="schoolLink">
                Preschools in New Delhi
              </Link>
              <Link href="/new delhi" className="schoolLink">
                Daycares in New Delhi
              </Link>
              <Link href="/hyderabad" className="schoolLink">
                Preschools in Hyderabad
              </Link>
              <Link href="/hyderabad" className="schoolLink">
                Daycares in Hyderabad
              </Link>
              <Link href="/chennai" className="schoolLink">
                Preschools in Chennai
              </Link>
              <Link href="/chennai" className="schoolLink">
                Daycares in Chennai
              </Link>
              <Link href="/gurugram" className="schoolLink">
                Preschools in Gurugram
              </Link>
              <Link href="/gurugram" className="schoolLink">
                Daycares in Gurugram
              </Link>
              <Link href="/kolkata" className="schoolLink">
                Preschools in Kolkata
              </Link>
              <Link href="/kolkata" className="schoolLink">
                Daycares in Kolkata
              </Link>
            </GridAutoWraper>
          </div>
          <br />
          <div>
            <CityTitle>Preschools in popular areas, Bengaluru: </CityTitle>
            <GridAutoWraper>
              <Link href="/bengaluru/bannerghetta" className="schoolLink">
                Preschools in Bannerghetta
              </Link>
              <Link href="/bengaluru/gottigere" className="schoolLink">
                Preschools in Gottigere
              </Link>
              <Link href="/bengaluru/btm-layout" className="schoolLink">
                Preschools in BTM Layout
              </Link>
              <Link href="/bengaluru/hsr-layout" className="schoolLink">
                Preschools in HSR Layout
              </Link>
              <Link href="/bengaluru/indiranagar" className="schoolLink">
                Preschools in Indiranagar
              </Link>
              <Link href="/bengaluru/akshayanagar" className="schoolLink">
                Preschools in Akshayanagar
              </Link>
              <Link href="/bengaluru/whitefield" className="schoolLink">
                Preschools in Whitefield
              </Link>
              <Link href="/bengaluru/ananthnagar" className="schoolLink">
                Preschools in Ananthnagar
              </Link>
              <Link href="/bengaluru/electronic-city" className="schoolLink">
                Preschools in Electronic City
              </Link>
              <Link href="/bengaluru/varthur" className="schoolLink">
                Preschools in Varthur
              </Link>
              <Link href="/bengaluru/kadugodi" className="schoolLink">
                Preschools in Kadugodi
              </Link>
              <Link href="/bengaluru/frazer-town" className="schoolLink">
                Preschools in Frazer Town
              </Link>
              <Link href="/bengaluru/marathahalli" className="schoolLink">
                Preschools in Marathahalli
              </Link>
              <Link href="/bengaluru/haralur" className="schoolLink">
                Preschools in Haralur
              </Link>
              <Link href="/bengaluru/sarjapur" className="schoolLink">
                Preschools in Sarjapur
              </Link>
              <Link href="/bengaluru/bellandur" className="schoolLink">
                Preschools in Bellandur
              </Link>
              <Link href="/bengaluru/basavanagudi" className="schoolLink">
                Preschools in Basavanagudi
              </Link>
              <Link href="/bengaluru/govindraj-nagar" className="schoolLink">
                Preschools in Govindraj Nagar
              </Link>
              <Link href="/bengaluru/koramangala" className="schoolLink">
                Preschools in Koramangala
              </Link>
              <Link href="/bengaluru/chandra-layout" className="schoolLink">
                Preschools in Chandra Layout
              </Link>
              <Link href="/bengaluru/mathikere" className="schoolLink">
                Preschools in Mathikere
              </Link>
              <Link href="/bengaluru/kr-puram" className="schoolLink">
                Preschools in KR Puram
              </Link>
              <Link href="/bengaluru/vijayanagar" className="schoolLink">
                Preschools in Vijayanagar
              </Link>
              <Link href="/bengaluru/yelahanka" className="schoolLink">
                Preschools in Yelahanka
              </Link>
              <Link href="/bengaluru/jp-nagar" className="schoolLink">
                Preschools in JP Nagar
              </Link>
              <Link href="/bengaluru/jayanagar" className="schoolLink">
                Preschools in Jayanagar
              </Link>
              <Link href="/bengaluru/nagarbhavi" className="schoolLink">
                Preschools in Nagarbhavi
              </Link>
              <Link href="/bengaluru/singasandra" className="schoolLink">
                Preschools in Singasandra
              </Link>
              <Link href="/bengaluru/Vidyaranyapura" className="schoolLink">
                Preschools in Vidyaranyapura
              </Link>
            </GridAutoWraper>
          </div>
          <br />
          <div>
            <CityTitle>Daycares in popular areas, Bengaluru: </CityTitle>
            <GridAutoWraper>
              <Link
                href="/bengaluru/bannerghetta?&organisation=2"
                className="schoolLink"
              >
                Daycares in Bannerghetta
              </Link>
              <Link
                href="/bengaluru/gottigere?&organisation=2"
                className="schoolLink"
              >
                Daycares in Gottigere
              </Link>
              <Link
                href="/bengaluru/btm-layout?&organisation=2"
                className="schoolLink"
              >
                Daycares in BTM Layout
              </Link>
              <Link
                href="/bengaluru/hsr-layout?&organisation=2"
                className="schoolLink"
              >
                Daycares in HSR Layout
              </Link>
              <Link
                href="/bengaluru/indiranagar?&organisation=2"
                className="schoolLink"
              >
                Daycares in Indiranagar
              </Link>
              <Link
                href="/bengaluru/akshayanagar?&organisation=2"
                className="schoolLink"
              >
                Daycares in Akshayanagar
              </Link>
              <Link
                href="/bengaluru/whitefield?&organisation=2"
                className="schoolLink"
              >
                Daycares in Whitefield
              </Link>
              <Link
                href="/bengaluru/ananthnagar?&organisation=2"
                className="schoolLink"
              >
                Daycares in Ananthnagar
              </Link>
              <Link
                href="/bengaluru/electronic-city?&organisation=2"
                className="schoolLink"
              >
                Daycares in Electronic City
              </Link>
              <Link
                href="/bengaluru/varthur?&organisation=2"
                className="schoolLink"
              >
                Daycares in Varthur
              </Link>
              <Link
                href="/bengaluru/kadugodi?&organisation=2"
                className="schoolLink"
              >
                Daycares in Kadugodi
              </Link>
              <Link
                href="/bengaluru/frazer-town?&organisation=2"
                className="schoolLink"
              >
                Daycares in Frazer Town
              </Link>
              <Link
                href="/bengaluru/marathahalli?&organisation=2"
                className="schoolLink"
              >
                Daycares in Marathahalli
              </Link>
              <Link
                href="/bengaluru/haralur?&organisation=2"
                className="schoolLink"
              >
                Daycares in Haralur
              </Link>
              <Link
                href="/bengaluru/sarjapur?&organisation=2"
                className="schoolLink"
              >
                Daycares in Sarjapur
              </Link>
              <Link
                href="/bengaluru/bellandur?&organisation=2"
                className="schoolLink"
              >
                Daycares in Bellandur
              </Link>
              <Link
                href="/bengaluru/basavanagudi?&organisation=2"
                className="schoolLink"
              >
                Daycares in Basavanagudi
              </Link>
              <Link
                href="/bengaluru/govindraj-nagar?&organisation=2"
                className="schoolLink"
              >
                Daycares in Govindraj Nagar
              </Link>
              <Link
                href="/bengaluru/koramangala?&organisation=2"
                className="schoolLink"
              >
                Daycares in Koramangala
              </Link>
              <Link
                href="/bengaluru/chandra-layout?&organisation=2"
                className="schoolLink"
              >
                Daycares in Chandra Layout
              </Link>
              <Link
                href="/bengaluru/mathikere?&organisation=2"
                className="schoolLink"
              >
                Daycares in Mathikere
              </Link>
              <Link
                href="/bengaluru/kr-puram?&organisation=2"
                className="schoolLink"
              >
                Daycares in KR Puram
              </Link>
              <Link
                href="/bengaluru/vijayanagar?&organisation=2"
                className="schoolLink"
              >
                Daycares in Vijayanagar
              </Link>
              <Link
                href="/bengaluru/yelahanka?&organisation=2"
                className="schoolLink"
              >
                Daycares in Yelahanka
              </Link>
              <Link
                href="/bengaluru/jp-nagar?&organisation=2"
                className="schoolLink"
              >
                Daycares in JP Nagar
              </Link>
              <Link
                href="/bengaluru/nagarbhavi?&organisation=2"
                className="schoolLink"
              >
                Daycares in Nagarbhavi
              </Link>
              <Link
                href="/bengaluru/jayanagar?&organisation=2"
                className="schoolLink"
              >
                Daycares in Jayanagar
              </Link>
              <Link
                href="/bengaluru/singsandra?&organisation=2"
                className="schoolLink"
              >
                Daycares in Singsandra
              </Link>
              <Link
                href="/bengaluru/Vidyaranyapura?&organisation=2"
                className="schoolLink"
              >
                Preschools in Vidyaranyapura
              </Link>
            </GridAutoWraper>
          </div>
          <br />
          <div>
            <CityTitle>Preschools in popular areas, Pune </CityTitle>
            <GridAutoWraper>
              <Link href="/pune/kharadi" className="schoolLink">
                Preschools in Kharadi
              </Link>
              <Link href="/pune/baner" className="schoolLink">
                Preschools in Baner
              </Link>
              <Link href="/pune/hadapsar" className="schoolLink">
                Preschools in Hadapsar
              </Link>
              <Link href="/pune/kothrud" className="schoolLink">
                Preschools in Kothrud
              </Link>
              <Link href="/pune/wagholi" className="schoolLink">
                Preschools in Wagholi
              </Link>
              <Link href="/pune/aundh" className="schoolLink">
                Preschools in Aundh
              </Link>
              <Link href="/pune/pimpri-chinchwad" className="schoolLink">
                Preschools in Pimpri Chinchwad
              </Link>
              <Link href="/pune/wakad" className="schoolLink">
                Preschools in Wakad
              </Link>
              <Link href="/pune/magarpatta" className="schoolLink">
                Preschools in Magarpatta
              </Link>
              <Link href="/pune/warje" className="schoolLink">
                Preschools in Warje
              </Link>
              <Link href="/pune/lohegaon" className="schoolLink">
                Preschools in Lohegaon
              </Link>
              <Link href="/pune/kondhwa" className="schoolLink">
                Preschools in Kondhwa
              </Link>
              <Link href="/pune/talegaon" className="schoolLink">
                Preschools in Talegaon
              </Link>
              <Link href="/pune/pimple-saudagar" className="schoolLink">
                Preschools in Pimple Saudagar
              </Link>
              <Link href="/pune/bavdhan" className="schoolLink">
                Preschools in Bavdhan
              </Link>
              <Link href="/pune/kalyani-nagar" className="schoolLink">
                Preschools in Kalyani Nagar
              </Link>
              <Link href="/pune/moshi" className="schoolLink">
                Preschools in Moshi
              </Link>
              <Link href="/pune/hinjawadi" className="schoolLink">
                Preschools in Hinjawadi
              </Link>
            </GridAutoWraper>
          </div>
          <br />
          <div>
            <CityTitle>Daycares in popular areas, Pune </CityTitle>
            <GridAutoWraper>
              <Link href="/pune/kharadi?&organisation=2" className="schoolLink">
                Daycares in Kharadi
              </Link>
              <Link href="/pune/baner?&organisation=2" className="schoolLink">
                Daycares in Baner
              </Link>
              <Link
                href="/pune/hadapsar?&organisation=2"
                className="schoolLink"
              >
                Daycares in Hadapsar
              </Link>
              <Link href="/pune/kothrud?&organisation=2" className="schoolLink">
                Daycares in Kothrud
              </Link>
              <Link href="/pune/wagholi?&organisation=2" className="schoolLink">
                Daycares in Wagholi
              </Link>
              <Link href="/pune/aundh?&organisation=2" className="schoolLink">
                Daycares in Aundh
              </Link>
              <Link
                href="/pune/pimpri-chinchwad?&organisation=2"
                className="schoolLink"
              >
                Daycares in Pimpri Chinchwad
              </Link>
              <Link href="/pune/wakad?&organisation=2" className="schoolLink">
                Daycares in Wakad
              </Link>
              <Link
                href="/pune/magarpatta?&organisation=2"
                className="schoolLink"
              >
                Daycares in Magarpatta
              </Link>
              <Link href="/pune/warje?&organisation=2" className="schoolLink">
                Daycares in Warje
              </Link>
              <Link
                href="/pune/lohegoan?&organisation=2"
                className="schoolLink"
              >
                Daycares in Lohegoan
              </Link>
              <Link href="/pune/kondhwa?&organisation=2" className="schoolLink">
                Daycares in Kondhwa
              </Link>
              <Link
                href="/pune/talegaon?&organisation=2"
                className="schoolLink"
              >
                Daycares in Talegaon
              </Link>
              <Link
                href="/pune/pimple-saudagar?&organisation=2"
                className="schoolLink"
              >
                Daycares in Pimple Saudagar
              </Link>
              <Link href="/pune/bavdhan?&organisation=2" className="schoolLink">
                Daycares in Bavdhan
              </Link>
              <Link
                href="/pune/kalyani-nagar?&organisation=2"
                className="schoolLink"
              >
                Daycares in Kalyani Nagar
              </Link>
              <Link href="/pune/moshi?&organisation=2" className="schoolLink">
                Daycares in Moshi
              </Link>
              <Link
                href="/pune/hinjawadi?&organisation=2"
                className="schoolLink"
              >
                Daycares in Hinjawadi
              </Link>
            </GridAutoWraper>
          </div>
          <br />
          <div>
            <CityTitle>Popular Preschools & Daycares</CityTitle>
            <GridAutoWraper>
              <Link
                href="/bengaluru/vidyaranyapura/cherish-kidz-an-indi-international-preschool-and-daycare-1820952"
                className="school_link"
              >
                Cherish Kids
              </Link>
              <Link
                href="/bengaluru/k-r-puram/discovery-montessori-usa-1785493"
                className="school_link"
              >
                Discovery Montessori USA
              </Link>
              <Link
                href="/bangalore/padmanabhanagar/ace-montessori-1783520"
                className="school_link"
              >
                Ace Montessori
              </Link>
              <Link
                href="/bengaluru/vijaynagar/step-by-step-the-learning-center-1787911"
                className="school_link"
              >
                Step by Step The Learning Center
              </Link>
              <Link
                href="/bangalore/gottigere/aura-montessori-house-of-children-1788833"
                className="school_link"
              >
                Aura Montessori
              </Link>
              <Link
                href="/bangalore/gottigere/the-pearls-montessori-1795828"
                className="school_link"
              >
                The Pearls Montessori
              </Link>
              <Link
                href="/bangalore/gottigere-banerghatta/greenwood-high-preschool-227730"
                className="school_link"
              >
                Greenwood High
              </Link>
              <Link
                href="/bengaluru/bohra-layout-gottigere/kalpaa-pre-school-1739888"
                className="school_link"
              >
                Kalpaa Preschools
              </Link>
              <Link
                href="/bengaluru/koramangala/united-world-academy-1713483"
                className="school_link"
              >
                United World Academy
              </Link>
              <Link
                href="/bengaluru/koramangala-1st-block/koala-preschool-181624"
                className="school_link"
              >
                Koala
              </Link>
              <Link
                href="/bengaluru/koramangala/podar-jumbo-kids-393240"
                className="school_link"
              >
                Podar Jumbo Kids
              </Link>
              <Link
                href="/bangalore/rpc-layout/st-michaels-kindergarten-1790584"
                className="school_link"
              >
                St. Michaels Kindergarten
              </Link>
              <Link
                href="/bangalore/moodalapalya/kidsbee-play-home-1793959"
                className="school_link"
              >
                Kidsbee Playhome
              </Link>
              <Link
                href="/bengaluru/govindraj-nagar/akshara-little-bees-pre-school-1786216"
                className="school_link"
              >
                Akshara Little Bees
              </Link>
              <Link
                href="/bengaluru/v-v-puram/edumeta-the-i-school-1794859"
                className="school_link"
              >
                Edumeta the Ischool
              </Link>
              <Link
                href="/bengaluru/vidyaranyapura/kidzee-vidyaranyapura-1798722"
                className="school_link"
              >
                Kidzee Vidyaranyapura
              </Link>
              <Link
                href="/bengaluru/vidyaranyapura/gurukull-vidyalaya-1797820"
                className="school_link"
              >
                Gurukull Vidyalaya
              </Link>
              <Link
                href="/bengaluru/phase-1/rainbow-kids-international-preschool-and-daycare-ananth-nagar-1799426"
                className="school_link"
              >
                Rainbow Kids International
              </Link>
              <Link
                href="/bengaluru/kadugodi/vedavihaan-the-global-school-best-pre-school-day-care-belathur-campus-3-1815367"
                className="school_link"
              >
                Vedavihaan
              </Link>
              <Link
                href="/bangalore/bannerghatta-road,/inspiro-preschool-daycare-1814585"
                className="school_link"
              >
                Inspiro Preschools
              </Link>
              <Link
                href="/pune/kharadi/euro-kids-kharadi-1809387"
                className="school_link"
              >
                Eurokids Kharadi
              </Link>
              <Link
                href="/bengaluru/vijaynagar/jaanvi-preschool-1807442"
                className="school_link"
              >
                Jaanvi Preschools
              </Link>
              <Link
                href="/bengaluru/rmv-2nd-stage/20-1806636"
                className="school_link"
              >
                Kids Castle
              </Link>
              <Link
                href="/bengaluru/jayanagar/podar-jumbo-kids-plus-1816916"
                className="school_link"
              >
                Podar Jumbo
              </Link>
            </GridAutoWraper>
          </div>
        </FatFooterContainer>
          </MainWrapper>
          <Footer {...props} />
        </>
      )}

      {isFullScreen && (
        <>
          <Image360>
            <button
              style={{
                width: '30px',
                height: '30px',
                position: 'absolute',
                top:
                  virtualTourService === 'yes' ||
                  (virtualTourService === 'no' &&
                    !safe(
                      mediaImages.filter(d => d.media_type === 'photo360'),
                      '[0].media_url',
                    ))
                    ? 'auto'
                    : 'auto',
                left:
                  virtualTourService === 'yes' ||
                  (virtualTourService === 'no' &&
                    !safe(
                      mediaImages.filter(d => d.media_type === 'photo360'),
                      '[0].media_url',
                    ))
                    ? '20px'
                    : '4px',
                bottom:
                  virtualTourService === 'yes' ||
                  (virtualTourService === 'no' &&
                    !safe(
                      mediaImages.filter(d => d.media_type === 'photo360'),
                      '[0].media_url',
                    ))
                    ? '21%'
                    : '4px',
                zIndex: 9,
              }}
              onClick={() => {
                setIsFullScreen(false);
              }}
            >
              <img
                style={{
                  height: '15px',
                  width: '15px',
                  position: 'relative',
                  top: '-1px',
                }}
                src={exitFullscreen}
                alt=""
              />
            </button>
            {virtualTourService === 'no' && (
              <FullScreenImageTabs>
                <div
                  className="wrap"
                  style={{
                    position: 'absolute',
                    zIndex: 10,
                    bottom: '15px',
                    left: '20px',
                    display: 'inherit',
                  }}
                >
                  {mediaImages
                    .filter(d => d.media_type === 'photo360')
                    .map((d, i) => (
                      <Button
                        text={buttonNameGen(d.media_url)}
                        type="profile"
                        marginRight="10px"
                        onClick={() => setCurrentTourSlide(i)}
                      />
                    ))}
                </div>
              </FullScreenImageTabs>
            )}
            {fullScreenIframeRenderer()}
          </Image360>
        </>
      )}
    </>
  );
};

// index.propTypes = {};

export default SchoolProfile;

const mediaResponse = [
  {
    media_id: 213,
    media_type: 'Decor',
    media_url:
      'https://productimages.withfloats.com/actual/5c7e6c80b5c197000185f1dc.jpg',
  },
  {
    media_id: 214,
    media_type: 'Play',
    media_url:
      'https://www.roundaboutharlow.co.uk/wp-content/uploads/Pre-schoolsNurseries-in-Harlow-1052x640.jpeg',
  },
  {
    media_id: 215,
    media_type: 'reception',
    media_url:
      'https://www.lilypondcountrydayschool.com/wp-content/uploads/2018/12/preschoolsforyoungchildren2-848x520.jpg',
  },
  {
    media_id: 216,
    media_type: 'photo360',
    media_url:
      'https://akm-img-a-in.tosshub.com/sites/btmt/images/stories/priya--preschool-owner-(1)_660_041119044845.jpg',
  },
  {
    media_id: 217,
    media_type: 'other',
    media_url:
      'https://s3-ap-southeast-1.amazonaws.com/tv-prod/school/photo/1599-large.jpg',
  },
  {
    media_id: 218,
    media_type: 'photo360',
    media_url:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcShjXHX0bp_xneLSLCw-1Se72KZ8IUOH9aeUGWice1cdoMEdM_pfA&s',
  },
  {
    media_id: 219,
    media_type: 'photo360',
    media_url:
      'https://www.stoodnt.com/blog/wp-content/uploads/2018/04/Preschool-Teaching-Career-and-Jobs-in-India-Asian-College-of-Teachers.jpg',
  },
  {
    media_id: 220,
    media_type: 'reception',
    media_url:
      'https://news24.sgp1.digitaloceanspaces.com/static_dev/static_root/media/2019/07/22/082a84e4-7500-4614-a7db-6c4619140fcb.jpg',
  },
  {
    media_id: 221,
    media_type: 'reception',
    media_url:
      'http://www.asianews.it/files/img/INDIA_-_0609_-_Ges%C3%B9_demone.jpg',
  },
  {
    media_id: 222,
    media_type: 'reception',
    media_url:
      'http://alfenoun.com/wp-content/uploads/2017/05/preschool-kids-130806.jpg',
  },
  {
    media_id: 223,
    media_type: 'reception',
    media_url:
      'https://abkfun.com/wp-content/uploads/2016/06/preschool-boy.jpg',
  },
];

const AdditionalFilterWrapper = styled.div`
  position: absolute;
  max-width: 699px;
  width: 100%;
  padding: 50px 60px;
  margin: 100px auto 20px;
  display: flex;
  background-color: #fff;
  z-index: 9;
  left: 0px;
  right: 0px;
  box-shadow: rgba(0, 0, 0, 0.5) 0px 2px 30px 0px;

  @media (max-width: 500px) {
    max-width: 320px;
    padding: 30px 20px;
    height: 357px;
  }
  @media (max-width: 359px) {
    max-width: 300px;
  }
  &.fixed {
    position: fixed;
    top: 100px;
  }
  .mobile {
    @media (max-width: 500px) {
      margin: 0px auto 20px;
      height: 46px;
      font-size: 14px;
      padding: 0px 40px;
    }
  }
`;
const CityTitle = styled.div`
  font-family: 'Quicksand', sans-serif;
  color: #2d3438;
  font-weight: bold;
  font-size: 0.9rem;
  padding-bottom: 5px;
`;
const FatFooterContainer = styled.div`
  padding: 30px 20px 100px;
`;
const GridAutoWraper = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(167px, 1fr));
  row-gap: 4px;
  color: #616161 !important;
  font-size: 0.75rem;
  font-family: 'Roboto', sans-serif;

  @media screen and (max-width: 767px) {
    grid-template-columns: repeat(2, 1fr);
  }

  a {
    color: #30333b !important;
    text-decoration: none !important;
  }
`;
export const GalleryHeading = styled.div`
  margin: ${props => props.margin || '0px'};
  color: ${props => props.color || '#30333B'};
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 500;
  line-height: 28px;
`;

export const ScheduleDetail = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  border-radius: 10px;
  background-color: #f3f5f9;
  padding: 18px 24px;
  margin-bottom: 10px;
  h2 {
    color: #30333b;
    font-family: 'Quicksand', sans-serif;
    font-size: 16px;
    font-weight: bold;
    line-height: 26px;
    text-align: center;
    margin: 0px;
    span {
      font-weight: 300;
      margin-right: 4px;
    }
  }
`;

const ModalTitle = styled.div`
  margin-bottom: 10px;
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 600;
  line-height: 28px;

  &.creatAccount {
    color: #30333b;
    font-size: 20px;
    // font-weight: 400;
    line-height: 23px;
    @media (max-width: 500px) {
      font-size: 18px;
      text-align: center;
    }
  }
  &.loginQues {
    font-family: 'Roboto', sans-serif;
    font-weight: 400;
    text-align: center;
    span {
      margin: 0px 5px;
    }
    &__title {
      margin-bottom: 20px;
      font-family: 'Roboto', sans-serif;
      font-weight: 400;
      text-align: center;
      font-size: 16px;
      line-height: 21px;
    }
  }
`;

// const TermsLink = styled.a`
//   margin-left: 5px;
//   color: ${props => props.color || '#613A95'};
//   font-family: 'Quicksand', sans-serif;
//   font-weight: 400;
//   font-size: 14px;
//   line-height: 19px;
//   text-decoration: none;
//   cursor: pointer;
// `;
// const SpanText = styled.span`
//   margin-left: 5px;
//   color: #666c78;
//   font-family: 'Roboto', sans-serif;
//   font-size: 14px;
//   line-height: 19px;
// `;
const ModalSubTitle = styled.div`
  margin-left: 5px;
  margin-bottom: 20px;
  color: #30333b;
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 19px;
  @media (max-width: 500px) {
    text-align: center;
  }
`;
const CloseModal = styled.div`
  position: absolute;
  top: 20px;
  right: 20px;
  cursor: pointer;
  @media screen and (max-width: 768px) {
    top: 1px;
    right: 7px;
  }
  .iconify {
    height: 12px;
    width: 12px;
    color: #666c78;
  }
`;

// const signInPrompt = {
//   position: 'absolute',
//   width: '50%',
//   height: '8%',
//   backgroundColor: 'rgb(255, 255, 255)',
//   boxShadow: 'rgba(0, 0, 0, 0.5) 0px 2px 30px 0px',
//   display: 'flex',
//   flexDirection: 'column',
//   justifyContent: 'center',
//   alignItems: 'center',
//   padding: '30px',
//   borderRadius: '2px',
//   margin: '0 10%',
// };

const ModalRight = styled.div`
  position: absolute;
  left: 0px;
  bottom: 0px;
  img {
    @media (max-width: 500px) {
      position: initial;
      margin-bottom: -33px;
      margin-left: -34px;
      transform: scale(0.7);
    }
  }
`;

const ModalLeft = styled.div`
  margin-left: auto;
  max-width: 66%;
  .alreadySigned {
    color: #666c78;
    font-family: Roboto;
    font-size: 14px;
    letter-spacing: 0;
    line-height: 14px;
    font-weight: 400;
    text-align: center;
    @media (max-width: 500px) {
      font-size: 12px;
    }
    span {
      color: #613a95;
      margin-left: 5px;
      cursor: pointer;
    }
  }
  @media (max-width: 500px) {
    margin-left: 0px;
    max-width: 100%;
  }
`;
