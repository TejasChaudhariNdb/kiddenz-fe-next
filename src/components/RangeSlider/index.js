/* eslint-disable react/jsx-no-undef */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
// import colors from 'utils/colors';

const SliderWrapper = styled.form`
  margin: 0 auto;
  padding: 100px 30px 0;

  @media (min-width: 800px) {
    max-width: 60%;
  }
`;

function RangeSlider(props) {
  const min = 5;
  const max = 10;
  const [values, setValues] = useState([min, max]);
  return (
    <SliderWrapper type={props.type}>
      <InputRange
        maxValue={20}
        minValue={0}
        value={values}
        onChange={value => setValues({ values: value })}
      />
    </SliderWrapper>
  );
}
RangeSlider.propTypes = {
  type: PropTypes.string,
  // textAlign: PropTypes.string,
};
export default RangeSlider;
