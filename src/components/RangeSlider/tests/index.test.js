import React from 'react';
import { render } from 'react-testing-library';

import RangeSlider from '../index';

describe('<RangeSlider />', () => {
  it('should render a prop', () => {
    const id = 'testId';
    const { container } = render(<RangeSlider id={id} />);
    expect(container.querySelector('h1').id).toEqual(id);
  });

  it('should render its text', () => {
    const children = 'Text';
    const { container, queryByText } = render(
      <RangeSlider>{children}</RangeSlider>,
    );
    const { childNodes } = container.querySelector('h1');
    expect(childNodes).toHaveLength(1);
    expect(queryByText(children)).not.toBeNull();
  });
});
