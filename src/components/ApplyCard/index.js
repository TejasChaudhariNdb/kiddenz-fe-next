import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import colors from 'utils/colors';
import Button from '../Button/index';
const Name = styled.div`
  color: rgba(48, 51, 59, 1);
  font-family: 'Quicksand', sans-serif;
  font-size: 18px;
  line-height: 23px;
  font-weight: 600;
  margin: 0px 0px 20px;
`;
const ApplyCard = styled.div`
display:flex;
width: ${props => props.width || '24%'}
flex-direction: column;
padding:36px 24px; 
border-radius: 10px;
background-color: rgba(255,255,255,1);
box-shadow: 0 0 30px 0 rgba(0,0,0,0.05);
margin-bottom:20px;
transition:transform 0.3s ease;
cursor:pointer;
&:last-child
{
    margin-left:15px;
    @media (max-width:900px)
{
  margin-left:0px;
}
}
@media (max-width:900px)
{
  width:32%;
}
@media (max-width:500px)
{
  width:100%;
}
&:hover
{
    box-shadow: 0 0 35px 0 rgba(0,0,0,0.1);
    transform:scale(1.03);
    transition:transform 0.3s ease;
    ${Name}
    {
        color:${colors.secondary};
    }
}
h2
{
    color: rgba(102,108,120,1);
    font-family: 'Roboto', sans-serif;
    font-size: 14px;
    line-height: 16px;
    font-weight:400;
    margin:0px 0px 5px;
    span
    {
     color:rgba(48,51,59,1);
     margin-left:4px;
    }
}
p
{
    color: rgba(102,108,120,1);
    font-family: 'Roboto', sans-serif;
    font-size: 12px;
    line-height: 19px;
    font-weight:400;
    display: -webkit-box;
  -webkit-line-clamp: 5;
  -webkit-box-orient: vertical;  
  overflow: hidden;
}
.applyBtn
{
    width:30%;
    .subscribe
    {
        padding:0px 34px;
        &:hover
        {
            border-color:${colors.secondary};
            color:${colors.secondary};
        }
    }
}
`;

const Apply = ({
  name,
  fontSize,
  margin,
  opening,
  experience,
  Desc,
  width,
}) => (
  <ApplyCard fontSize={fontSize} margin={margin} width={width}>
    <Name>{name}</Name>
    <h2>
      Opening:
      <span>{opening}</span>
    </h2>
    <h2>
      Experience:
      <span>{experience}</span>
    </h2>
    <p>{Desc}</p>
    <div className="applyBtn">
      <Button text="Apply" type="subscribe" height="40px" />
    </div>
  </ApplyCard>
);
Apply.propTypes = {
  name: PropTypes.string,
  fontSize: PropTypes.string,
  margin: PropTypes.string,
  opening: PropTypes.string,
  experience: PropTypes.string,
  Desc: PropTypes.string,
  width: PropTypes.string,
};
export default Apply;
