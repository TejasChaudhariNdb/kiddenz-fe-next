import React from 'react';
import PropTypes from 'prop-types';
import DatePicker from 'react-datepicker';
import styled from 'styled-components';
import Button from '../Button';
import targetIcon from '../../images/target.svg';

const InputBox = ({
  displayName = false,
  name,
  type = 'text',
  margin,
  label,
  placeholder,
  value,
  onChange,
  onBlur,
  error,
  rows,
  maxDate,
  minDate,
  disabled,
  onKeyPress,
  ...rest
}) => (
  <Inputbox margin={margin}>
    {displayName && name && <h2>{name}</h2>}
    {label && <h2>{label}</h2>}

    {type === 'text' && (
      <>
        <InputField
          placeholder={placeholder}
          type={type}
          value={value}
          onChange={e => onChange(e, name)}
          onBlur={e => onBlur && onBlur(e, name)}
          onKeyPress={onKeyPress}
          {...rest}
        />
        <Error>{error}</Error>
      </>
    )}

    {type === 'number' && (
      <>
        <InputField
          placeholder={placeholder}
          type={type}
          value={value}
          onChange={e => onChange(e, name)}
          onBlur={e => onBlur && onBlur(e, name)}
          onKeyPress={onKeyPress}
          {...rest}
        />
        <Error>{error}</Error>
      </>
    )}

    {type === 'tel' && (
      <>
        <InputField
          placeholder={placeholder}
          type="number"
          className={type}
          value={value}
          onChange={e => onChange(e, name)}
          onBlur={e => onBlur(e, name)}
          disabled={disabled}
          onKeyPress={onKeyPress}
        />
        <span className="number">+91- </span>
        <Error>{error}</Error>
      </>
    )}
    {type === 'search' && (
      <>
        <InputField
          placeholder={placeholder}
          className={type}
          value={value}
          // onChange={e => onChange(e, name)}
          // onBlur={e => onBlur(e, name)}
          // disabled={disabled}
          // onKeyPress={onKeyPress}
        />
        <img className="targetIcon" src={targetIcon} alt="" />
      </>
    )}
    {type === 'telDisable' && (
      <>
        <InputField
          placeholder={placeholder}
          className={type}
          value={value}
          onChange={e => onChange(e, name)}
          onBlur={e => onBlur(e, name)}
          disabled={disabled}
          onKeyPress={onKeyPress}
        />
        <span className="number">+91- </span>
        <span
          className="iconify tickIcon"
          data-icon="cil:check-alt"
          data-inline="false"
        />
        <Error>{error}</Error>
      </>
    )}

    {type === 'datepicker' && (
      <>
        <DatePickerInput>
          <DatePicker
            placeholderText="DD-MM-YY"
            className="datepicker"
            value={value}
            // iconName="feather:calendar"
            onChange={e => onChange(e, name)}
            onBlur={e => onBlur(e, name)}
            maxDate={maxDate}
            minDate={minDate}
            showYearDropdown
            showMonthDropdown
          />
        </DatePickerInput>
        <Error>{error}</Error>
      </>
    )}

    {type === 'textarea' && (
      <>
        <textarea
          cols="4"
          rows={rows}
          placeholder={placeholder}
          className="textArea"
          type={type}
          style={{ resize: 'none' }}
          value={value}
          maxLength={1000}
          onChange={e => onChange(e, name)}
        />
        <p>{rest.charLimit}</p>
      </>
    )}
    {type === 'subscribe' && (
      <>
        <InputField
          placeholder="Enter your email to subcribe our articles"
          type={type}
        />
        <Button type="mobile" text="subscribe" height="36px" />
      </>
    )}
    {type === 'childAge' && (
      <>
        <InputField
          placeholder={placeholder}
          className={type}
          value={value}
          maxLength={2}
          onChange={e => onChange(e, name)}
        />
      </>
    )}
  </Inputbox>
);
InputBox.propTypes = {
  displayName: PropTypes.bool,
  disabled: PropTypes.bool,
  name: PropTypes.string,
  type: PropTypes.string,
  margin: PropTypes.string,
  value: PropTypes.string,
  placeholder: PropTypes.string,
  maxDate: PropTypes.object,
  minDate: PropTypes.object,
  error: PropTypes.string,
  rows: PropTypes.string,
  label: PropTypes.string,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  onKeyPress: PropTypes.func,
  charLimit: PropTypes.string,
};
export default InputBox;

const Inputbox = styled.div`
  position: relative;
  width: 100%;
  font-family: 'Quicksand', sans-serif;
  margin: ${props => props.margin || '0px 0px 30px 0px;'};

  h2 {
    margin: 0px 0px 7px 0px;
    font-weight: 400;
    color: #666c78;
    font-family: 'Roboto', sans-serif;
    font-size: 14px;
    line-height: 19px;
  }

  .mobile {
    position: absolute;
    padding: 0px 20px;
    right: 15px;
    top: 6px;
  }
  .textArea {
    width: 100%;
    border-radius: 5px;
    border: 1px solid #c0c8cd;
    padding: 17px;
    outline: 0px;
    font-size: 14px;
    color: rgba(48, 51, 59, 1);
    font-family: 'Roboto', sans-serif;
    &::-webkit-input-placeholder {
      color: #666c78;
      font-family: 'Roboto', sans-serif;
      font-size: 14px;
      line-height: 21px;
      opacity: 0.5;
    }
  }
  & > .number {
    font-size: 14px;
    position: absolute;
    left: 10px;
    top: 16px;
    font-family: 'Roboto', sans-serif;
  }
  .tickIcon {
    color: #60b947;
    width: 20px;
    height: 20px;
    position: absolute;
    right: 10px;
    top: 17px;
  }
  .targetIcon {
    position: absolute;
    right: 10px;
    top: 9px;
    @media (max-width: 500px) {
      transform: scale(0.9) !important;
    }
  }
`;
const InputField = styled.input`
  width: 100%;
  border: 1px solid #c0c8cd;
  border-radius: 5px;
  font-size: 14px;
  color: rgba(48, 51, 59, 1);
  background-color: ${({ disabled }) => (disabled ? '#F2EFF5' : '#ffffff')};
  padding: 17px;
  outline: 0px;
  @media (max-width: 500px) {
    padding: 12px;
  }
  &.childAge {
    padding: 6px 10px;
    font-size: 14px;
    font-family: 'Roboto', sans-serif;
    color: rgba(48, 51, 59, 1);
    text-align: center;
    &::-webkit-input-placeholder {
      font-family: 'Roboto', sans-serif;
      font-size: 14px;
      color: rgba(48, 51, 59, 0.9);
      opacity: 1;
    }
  }
  &.tel {
    padding: 17px 17px 17px 42px;
  }
  &.telDisable {
    padding-left: 42px;
    position: relative;
    font-size: 16px;
    font-family: 'Roboto', sans-serif;
    & + .number {
      top: 40px !important;
      & + .tickIcon {
        top: 46px !important;
        @media (max-width: 500px) {
          top: 37px !important;
        }
      }
    }

    &::-webkit-input-placeholder {
      color: #666c78;
      font-family: 'Roboto', sans-serif;
      font-size: 16px;
      line-height: 21px;
      opacity: 0.5;
    }
    &::placeholder {
      color: #666c78;
      font-family: 'Roboto', sans-serif;
      font-size: 16px;
      line-height: 21px;
      opacity: 0.5;
    }
    :-ms-input-placeholder {
      color: #666c78;
      font-family: 'Roboto', sans-serif;
      font-size: 16px;
      line-height: 21px;
      opacity: 0.5;
    }
  }
`;
const Error = styled.div`
  font-size: 12px;
  color: red;
`;

const DatePickerInput = styled.div`
  position: relative;
  input[type='text'] {
    width: 100%;
    border: 1px solid #c0c8cd;
    border-radius: 5px;
    font-size: 14px;
    background-color: #ffffff;
    padding: 17px;
    outline: 0px;

    &:focus {
      outline: 0px;
    }
  }
  .iconify {
    position: absolute;
    top: 18px;
    right: 10px;
    width: 22px;
    height: 22px;
    color: #666c78;
  }
`;
