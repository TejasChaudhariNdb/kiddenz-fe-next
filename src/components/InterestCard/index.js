/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable no-param-reassign */
/* eslint-disable no-multi-assign */
import React from 'react';
import PropTypes from 'prop-types';
import Router from 'next/router';
import { mdiBookmark, mdiBookmarkOutline } from '@mdi/js';
import styled from 'styled-components';
import Flex from '../common/flex';
import IconCard from '../Iconcard';

const InterestCard = ({
  interestImage,
  heading,
  list = '',
  categoryList = '',
  dateNum,
  viewNum,
  likesNum,
  margin,
  width,
  type,
  imageHeight,
  imageWidth,
  onClick,
  isBookmarked,
  isLoggedIn,
  onBookmarkClick,
  categoryColors = {},
  categoriesConfig = {},
}) => {
  // const handelCategoryClick = (slug, categoryId, category) =>
  //   Router.push(
  //     `/blog/category/${slug}?id=${categoryId}&name=${category}`,
  //   ).then(() => window.scrollTo(0, 0));

  const handelHashTagClick = hashtag =>
    Router.push(`/blog/search?key=%23${hashtag}`).then(() =>
      window.scrollTo(0, 0),
    );

  return (
    <Interest
      // href={`/blog/${29}`}
      margin={margin}
      width={width}
      className={type}
      onClick={onClick}
    >
      <InterestImage imageHeight={imageHeight} imageWidth={imageWidth}>
        <img src={interestImage} alt="" />
        {isLoggedIn && (
          <>
            {isBookmarked ? (
              <IconCard
                isWishlisted={isBookmarked}
                iconName={mdiBookmark}
                type="square"
                padding="3px"
                onClick={e => {
                  e.stopPropagation();
                  e.preventDefault();
                  onBookmarkClick();
                }}
                mdi
              />
            ) : (
              <IconCard
                isWishlisted={isBookmarked}
                iconName={mdiBookmarkOutline}
                type="square"
                padding="3px"
                onClick={e => {
                  e.stopPropagation();
                  e.preventDefault();
                  onBookmarkClick();
                }}
                mdi
              />
            )}
          </>
        )}
      </InterestImage>
      <InterestContent>
        {(categoryList || '').length > 0 && (
          <Flex wrap flexMargin="0px 0px 15px">
            {(categoryList || '').split(',').map(cat => (
              <CaregoryItem
                color={categoryColors[cat] && categoryColors[cat][0]}
                bgColor={categoryColors[cat] && categoryColors[cat][1]}
                // onClick={e => {
                //   e.preventDefault();
                //   e.stopPropagation();
                //   handelCategoryClick(
                //     categoriesConfig[cat].slugName,
                //     categoriesConfig[cat].id,
                //     cat,
                //   );
                // }}

                // NOTE: FROM HARDCODED categoriesConfig OBJ Auth Reducer
                onClick={e => {
                  e.preventDefault();
                  e.stopPropagation();
                  Router.push(categoriesConfig[cat].redirection).then(() =>
                    window.scrollTo(0, 0),
                  );
                }}
              >
                {cat.toUpperCase()}
              </CaregoryItem>
            ))}
          </Flex>
        )}
        <h3>{heading}</h3>
        <HashTag>
          {(list || '').split(',').map(cat => (
            <span
              onClick={e => {
                e.preventDefault();
                e.stopPropagation();
                handelHashTagClick(cat);
              }}
            >
              {(list || '').length > 0 ? `#${cat}` : ''}
            </span>
          ))}{' '}
        </HashTag>

        <Flex flexMargin="20px 0px 0px 0px">
          <Likes>
            <span
              className="iconify"
              data-icon="feather:calendar"
              data-inline="false"
            />
            {dateNum}
          </Likes>
          <Likes>
            <span
              className="iconify"
              data-icon="ion:eye-outline"
              data-inline="false"
            />
            {viewNum || 0}
          </Likes>
          <Likes>
            <span
              className="iconify"
              data-icon="ant-design:like-outline"
              data-inline="false"
            />
            {likesNum || 0}
          </Likes>
        </Flex>
      </InterestContent>
    </Interest>
  );
};
InterestCard.propTypes = {
  heading: PropTypes.string,
  categoryColors: PropTypes.object,
  categoriesConfig: PropTypes.object,
  interestImage: PropTypes.string,
  list: PropTypes.string,
  dateNum: PropTypes.string,
  viewNum: PropTypes.string,
  likesNum: PropTypes.string,
  margin: PropTypes.string,
  width: PropTypes.string,
  imageHeight: PropTypes.string,
  imageWidth: PropTypes.string,
  type: PropTypes.string,
  onClick: PropTypes.func,
  onBookmarkClick: PropTypes.func,
  categoryList: PropTypes.array,
  isBookmarked: PropTypes.bool,
  isLoggedIn: PropTypes.bool,
};
export default InterestCard;

const CaregoryItem = styled.div`
  color: ${props => props.color || 'rgb(97,58,149)'};
  background-color: ${props => props.bgColor || 'rgba(97,58,149,0.1)'};
  display: flex;
  align-items: center;
  justify-content: center;
  font-family: 'Roboto', sans-serif;
  font-size: 11px;
  line-height: 15px;
  height: 22px;
  width: fint-content;
  // border: 1px solid #a3a9b7;
  border-radius: 3px;
  margin-bottom: 10px;
  padding: 0px 8px;
  margin-right: 20px;
  &:hover {
    color: #fff;
    background-color: ${props => props.color || 'rgb(97,58,149)'};
  }
  @media screen and (max-width: 1100px) {
    margin-bottom: 10px;
  }
`;

const InterestContent = styled.div`
  padding: 20px 20px 50px;
  @media screen and (max-width: 1100px) {
    padding: 20px 20px 50px;
  }
  @media screen and (max-width: 790px) and (min-width: 766px) {
    padding: 20px 10px 30px;
  }
  @media screen and (max-width: 400px) {
    padding: 20px 15px 30px;
  }
  .bond {
    display: flex;
    align-items: center;
    justify-content: center;
    font-family: 'Roboto', sans-serif;
    color: ${props => props.color || 'rgb(97,58,149)'};
    font-size: 11px;
    line-height: 15px;
    height: 22px;
    width: fint-content;
    // border: 1px solid #a3a9b7;
    border-radius: 3px;
    margin-bottom: 30px;
    padding: 0px 8px;
    background-color: ${props => props.bgColor || 'rgba(97,58,149,0.1)'};
    margin-right: 20px;
    @media screen and (max-width: 1100px) {
      margin-bottom: 10px;
    }
    &:hover {
      color: ${props => props.bgColor || 'rgba(97,58,149,0.1)'};
      background-color: ${props => props.color || 'rgb(97,58,149)'};
    }
  }
  h3 {
    margin: 0px 0px 20px;
    color: #30333b;
    font-family: 'Quicksand', sans-serif;
    font-size: 18px;
    font-weight: bold;
    line-height: 23px;
    @media screen and (max-width: 1100px) {
      font-size: 16px;
      margin-bottom: 10px;
    }
  }
`;
const InterestImage = styled.div`
  position: relative;
  width: ${props => props.imageWidth || '100%'};
  height: ${props => props.imageHeight || '215px'};
  border-radius: 5px;
  overflow: hidden;
  img {
    width: 100%;
    height: 100%;
  }
  @media screen and (max-width: 500px) {
    height: 300px;
  }
`;
const Interest = styled.a`
  width: ${props => props.width || '30.5%'};
  margin: ${props => props.margin || '0px 6% 0px 0px'};
  text-decoration: none;
  border-radius: 10px;
  background-color: #ffffff;
  box-shadow: 0 0 30px 0 rgba(0, 0, 0, 0.05);
  cursor: pointer;
  transition: all 0.2s;
&:last-child
{
  margin-right:0px;
}
&.article
{
  cursor:default;
  &:hover {
    transform: scale(1);
    h3 {
      color: #30333b;
  }
  }
}
@media screen and (max-width: 900px) {
  width:32%
  margin: 0px 2% 0px 0px;
}
  @media screen and (max-width: 765px) {
    max-width: 400px;
    width: 100%;
    margin: 0px auto 30px;
  }
  @media screen and (max-width: 500px) {
    max-width:100%;
  }
  &:hover {
    transform: scale(1.02);
    box-shadow: 0 0 30px 0 rgba(0,0,0,0.2);
    transition: all 0.2s;
    h3 {
      color: #613a95;
    }
  }
  .bookmark {
    top: 15px;
    right: 15px;
  }
  &.primary {
    display: flex;
    box-shadow: none;
    width: 100%;
    ${InterestContent} {
      padding: 0px 20px 0px;
      width:90%;
      @media screen and (max-width: 500px) {
        padding: 20px 0px 0px 0px;
      }
    }
    @media screen and (max-width: 500px) {
      flex-direction: column;
      margin: 0px 0px 50px;
    }
    ${InterestImage}
    {
      @media screen and (max-width: 500px) {
        width: 100%;
        height: 280px;
      }
    }
 
    .bond {
      margin-bottom: 20px;
    }
  }
  &.secondary {
    box-shadow: none;

    ${InterestImage} {
      display: none;
    }
    .bookmark {
      display: none;
    }
    ${InterestContent} {
      @media screen and (max-width: 500px) {
        padding: 20px 0px;
      }
      h3 {
        font-size: 40px;
        line-height: 50px;
        margin-bottom: 30px;
        @media screen and (max-width: 500px) {
          font-size: 26px;
          line-height: 32px;
          margin-bottom: 12px;
        }
      }
    }
  }
`;

const HashTag = styled.div`
  display: flex;
  flex-wrap: wrap;
  // height: 16px;
  span {
    color: rgb(97, 58, 149);
    font-family: 'Roboto', sans-serif;
    font-size: 12px;
    line-height: 16px;
    padding-right: 5px;
    &:hover {
      font-weight: 600;
    }
  }
`;

const Likes = styled.div`
  display: flex;
  align-items: end;
  color: #666c78;
  font-family: 'Roboto', sans-serif;
  font-size: 12px;
  line-height: 19px;
  margin-right: 20px;
  @media screen and (max-width: 1100px) and (min-width: 789px) {
    font-size: 10px;
    line-height: 15px;
    margin-right: 10px;
  }
  @media screen and (max-width: 790px) and (min-width: 766px) {
    margin-right: 3px;
  }
  @media screen and (max-width: 400px) {
    font-size: 10px;
  }
  .iconify {
    width: 18px;
    height: 18px;
    color: #666c78;
    margin-right: 4px;
    @media screen and (max-width: 1100px) {
      width: 16px;
      height: 16px;
    }
    @media screen and (max-width: 790px) and (min-width: 766px) {
      margin-right: 3px;
    }
    @media screen and (max-width: 400px) {
      margin-right: 4px;
    }
  }
`;
