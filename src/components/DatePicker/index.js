/* eslint-disable indent */
import React from 'react';
import PropTypes from 'prop-types';
import DatePicker from 'react-datepicker';
import styled from 'styled-components';
// import 'react-datepicker/dist/react-datepicker.css';
import { setMinutes, setHours } from 'date-fns';

const SelectDate = styled.div`
  display: flex;
  align-items: center;
  height: 56px;
  width: ${({ width }) => width || '190px'};
  border: 1px solid #c0c8cd;
  border-radius: 5px;
  background-color: #ffffff;
  padding-left: 17px;
  input[type='text'] {
    border: 0px;
    width: 100%;
    &:focus {
      outline: 0px;
    }
  }
  .iconify {
    min-width: 17px;
    height: 17px;
    color: #30333b;
    margin-right: 10px;
  }
  .react-datepicker__time-list-item--disabled {
    display: none;
  }
`;

const CustomDatePicker = ({
  iconName,
  placeholder,
  isTime,
  onChange,
  name,
  value,
  disabled,
  minDate,
  maxDate,
  width,
  ...rest
}) => (
  <SelectDate width={width} className="datepick">
    <span className="iconify" data-icon={iconName} data-inline="false" />
    <DatePicker
      {...rest}
      value={value}
      placeholderText={placeholder}
      onChange={e => onChange(e, name)}
      showDisabledMonthNavigation
      minDate={minDate || new Date()}
      maxDate={maxDate}
      minTime={setHours(setMinutes(new Date(), 0), 9)}
      maxTime={setHours(setMinutes(new Date(), 0), 18)}
      disabled={disabled}
      {...(!isTime
        ? {}
        : {
            showTimeSelect: true,
            showTimeSelectOnly: true,
            timeIntervals: 15,
            timeFormat: 'HH:mm',
          })}
    />
  </SelectDate>
);

CustomDatePicker.propTypes = {
  placeholder: PropTypes.string,
  iconName: PropTypes.string,
  isTime: PropTypes.bool,
  disabled: PropTypes.bool,
  onChange: PropTypes.func,
  name: PropTypes.string,
  width: PropTypes.string,
  value: PropTypes.object,
  minDate: PropTypes.object,
  maxDate: PropTypes.object,
};
export default CustomDatePicker;
