/* eslint-disable indent */
import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';
import Button from 'components/Button';
// import colors from 'utils/colors';
import {
  mdiHeart,
  mdiHeartOutline,
  mdiBookmark,
  mdiBookmarkOutline,
} from '@mdi/js';

// eslint-disable-next-line import/named
import { Photo, ViewAll } from './styled';
import IconCard from '../Iconcard';
import Verfied from '../Verfied';
import Profile from '../../images/profile.jpg';

// const Featured = styled.div`
//   position: absolute;
//   top: 20px;
//   left: 20px;
//   display: flex;
//   justify-content: center;
//   align-items: center;
//   height: 24px;
//   width: 68px;
//   border-radius: 5px;
//   background-color: ${colors.white};
//   color: ${colors.secondary};
//   font-family: 'Roboto', sans-serif;
//   font-size: 11px;
//   font-weight: 500;
//   line-height: 15px;
//   text-transform: uppercase;
//   box-shadow: 3px 3px 8px 0 rgba(0, 0, 0, 0.2);

//   @media (max-width: 414px) {
//     top: 10px;
//     left: 10px;
//   }
// `;
const PhotoCard = ({
  image,
  width,
  height,
  margin,
  type,
  onClick,
  isWishlisted,
  isBookmarked,
  onWishlistIconClick,
  onBookmarkIconClick,
  isLoggedIn,
  totalPhotos,
  hasBoxShadow,
  imageAlt,
}) => (
  <Photo
    width={width}
    height={height}
    margin={margin}
    // eslint-disable-next-line react/jsx-no-duplicate-props
    className={type}
    onClick={onClick}
    hasBoxShadow={hasBoxShadow}
  >
    {image ? (
      <img src={image} alt={imageAlt} />
    ) : (
      <img src={Profile} alt="profileImage" />
    )}
    {type === 'featured' && <>{/* <Featured>Featured</Featured> */}</>}
    {type === 'liked' &&
      isLoggedIn && (
        <>
          {isWishlisted ? (
            <IconCard
              iconName={mdiHeart}
              type="heart"
              padding="3px"
              isWishlisted
              onClick={e => {
                e.stopPropagation();
                e.preventDefault();
                onWishlistIconClick();
              }}
              mdi
            />
          ) : (
            <IconCard
              iconName={mdiHeartOutline}
              type="heart"
              padding="3px"
              onClick={e => {
                e.stopPropagation();
                e.preventDefault();
                onWishlistIconClick();
              }}
              mdi
            />
          )}
        </>
      )}
    {type === 'both' && (
      <>
        {/* <Featured>Featured</Featured> */}
        {isLoggedIn && (
          <>
            {isWishlisted ? (
              <IconCard
                iconName="emojione-red-heart"
                type="square"
                padding="3px"
                onClick={e => {
                  e.stopPropagation();
                  e.preventDefault();
                  onWishlistIconClick();
                }}
              />
            ) : (
              <IconCard
                iconName="ei-heart"
                type="square"
                onClick={e => {
                  e.stopPropagation();
                  e.preventDefault();
                  onWishlistIconClick();
                }}
              />
            )}
          </>
        )}
      </>
    )}

    {type === 'bookmark' && (
      <>
        {isLoggedIn && (
          <>
            {isBookmarked ? (
              <IconCard
                iconName={mdiBookmark}
                type="square"
                padding="3px"
                top="4px"
                right="6px"
                isWishlisted={isBookmarked}
                onClick={e => {
                  e.stopPropagation();
                  e.preventDefault();
                  onBookmarkIconClick();
                }}
                mdi
              />
            ) : (
              <IconCard
                iconName={mdiBookmarkOutline}
                isWishlisted={isBookmarked}
                type="square"
                padding="3px"
                top="4px"
                right="6px"
                onClick={e => {
                  e.stopPropagation();
                  e.preventDefault();
                  onBookmarkIconClick();
                }}
                mdi
              />
            )}
          </>
        )}
      </>
    )}
    {type === 'viewall' && (
      <ViewAll className="viewall">
        <span>{totalPhotos} Photos</span>
        <Button text="View all" type="viewAll" marginRight="10px" />
      </ViewAll>
    )}
    {type === 'verified' && (
      <div className="verifiedBtn">
        <Verfied name="Verified" />
      </div>
    )}
  </Photo>
);
PhotoCard.propTypes = {
  image: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  margin: PropTypes.string,
  type: PropTypes.string,
  onClick: PropTypes.func,
  onWishlistIconClick: PropTypes.func,
  onBookmarkIconClick: PropTypes.func,
  isWishlisted: PropTypes.bool,
  isBookmarked: PropTypes.bool,
  isLoggedIn: PropTypes.bool,
  hasBoxShadow: PropTypes.bool,
  totalPhotos: PropTypes.number,
  imageAlt: PropTypes.string,
};
export default PhotoCard;
