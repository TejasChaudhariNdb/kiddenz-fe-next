import styled from 'styled-components';
import colors from 'utils/colors';
export const Photo = styled.div`
  position: relative;
  height: ${props => props.height || '110px'};
  width: ${props => props.width || '144px'};
  min-width: ${props => props.minWidth};
  margin: ${props => props.margin || '0px 10px 10px 0px;'};
  box-shadow: ${({ hasBoxShadow }) =>
    hasBoxShadow ? '0px 0px 11px 8px rgba(156,154,158,1);' : ''}
  border-radius: 5px;
  & > img {
    width: 100%;
    height: 100%;
    border-radius: 10px;
  }
  &.viewall {
    position: relative;
    & > img {
      opacity: 0.4;
    }
    .viewall {
      display: flex;
    }
  }
  &.preSchool {
    min-width: 126px;
    height: 76px;
  }
  .verifiedBtn {
    position: absolute;
    right: 10px;
    bottom: -10px;
  }
  &.active {
    border: 2px solid #fff;
  }
  // &.bookmark
  // {
  //   @media (max-width: 1280px) {
  //       height: 100px !important;
  //       width: 62px !important;
  //     }
  // }
`;
export const ViewAll = styled.div`
  display: none;
  left: 0px;
  top: 0px;
  bottom: 0px;
  right: 0px;
  position: absolute;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  .viewAll {
    background-color: ${colors.secondary};
    border-color: ${colors.secondary};
    color: #fff;
  }
`;
