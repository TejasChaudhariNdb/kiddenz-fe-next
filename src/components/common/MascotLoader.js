import React from 'react';
import loader from 'images/mascot.gif';

export default function MascotLoader({ height = null } = {}) {
  return (
    <div
      style={{
        margin: '10px auto 20px',
        display: 'table-cell',
        textAlign: 'center',
        verticalAlign: 'middle',
        height: height || '100vh',
        width: '100vw',
      }}
    >
      <img src={loader} alt="" height={150} width={150} />
    </div>
  );
}
