import styled from 'styled-components';

const Ruppee = styled.strong`
  display: inline;
  font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
  font-weight: 500;
`;

export default Ruppee;
