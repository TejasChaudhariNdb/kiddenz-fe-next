/* eslint-disable react/prop-types */
import React from 'react';
import styled from 'styled-components';

// Styled component for the vector image
const VectorImage = styled.img`
  height: 9px;
  left: 0;
  position: absolute;
  top: 0;
  width: 7px;
`;

const Vector = ({ className }) => (
  <VectorImage
    className={`vector ${className}`}
    alt="Vector"
    src="https://c.animaapp.com/QFxCdMhV/img/vector-4.svg"
  />
);

export default Vector;
