import styled from 'styled-components';
import colors from 'utils/colors';
export const Checkbox = styled.div`
margin: ${props => props.margin || '0px'};
  display: flex;
  width:${props => props.width || 'auto'};
  align-items:center;
  &:last-child
  {
    margin-bottom: 10px;
  }


  label {
    position: relative;
    color: #666C78;	  
    font-family: 'Roboto', sans-serif;	
    font-size: ${props => props.filterFont || '14px'};	
    line-height: ${props => props.filterLineheight || '19px'};
    padding-left:28px;
    // white-space: nowrap;	
    cursor:pointer;
    @media screen and (max-width: 768px) {
      font-size: 12px;
      line-height: 15px;
      color: #666C78;
      display: flex;
      align-items: center;
    }
  }

  input {
    padding: 0;
    height: initial;
    width: initial;
    margin-bottom: 0;
    display: none;
    cursor: pointer;
    
  }


label:before {
  content: '';
  -webkit-appearance: none;
  // background-color: #3B74E7; 
  background-color: #fff;
  border: 1px solid #C0C8CD;
  // border: 1px solid #3B74E7;
  left:0px;
  border-radius: 5px;
  margin-bottom: 2px;
  padding: 5px;
  height: 20px;	
  width: 20px;
  display: inline-block;
  position: absolute;
  vertical-align: middle;
  cursor: pointer;
  margin-right: ${props => props.filterChekbox || '12px'};
  @media screen and (max-width: 768px) {
    height: 16px;
    width: 16px;
  }
}
input:checked + label:before {
  background: ${colors.secondary};
  border-width: 0px;
}
input:checked + label:after {
  content: '';
  display: block;
  position: absolute;
  top: 3px;
  left: 7px;
  width: 6px;
  height: 11px;
  border: solid #FFF;
  border-width: 0 1.5px 1.5px 0;
  transform: rotate(45deg);
  @media screen and (max-width: 768px) {
    top: 33%;
    left: 6px;
    width: 4px;
    height: 8px;
    border-width: 0 0.5px 0.5px 0;
  }
}

.scheduleTour {
  input:checked + label::after {
    @media screen and (max-width: 768px) {
      top: 8px !important;
      left: 6px;
      width: 4px;
      height: 8px;
      border-width: 0 0.5px 0.5px 0;
    }
  }
}
 input:disabled {
}
 input:disabled + label:after {
  background-color: #3B74E7;
  border: solid #FFF;
  border-width: 0 1px 1px 0;
  transform: rotate(45deg);
  cursor: default;
}
input:disabled + label:before {
  border-color: #FFF;
  cursor: default;
}
input:disabled + label {
  color: #FFF;
  cursor: default;
}
}
&.checkboxwrap{
  label{
    white-space: pre-wrap;
    padding-left: 25px;

    &:before{
      margin-left: -25px;
    }
  }
}
`;
