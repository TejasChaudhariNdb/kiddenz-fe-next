/* eslint-disable */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Checkbox } from './styled';
const CheckBox = ({
  label,
  id,
  onClickFunction,
  checked,
  disabled,
  margin,
  width,
  classtype,
  filterChekbox,
  filterFont,
  filterLineheight,
}) => {
  // useEffect(() =>{}, [checked]);
  return (
    <Checkbox
      className={`checkbox ${classtype}`}
      margin={margin}
      width={width}
      filterChekbox={filterChekbox}
      filterFont={filterFont}
      filterLineheight={filterLineheight}
      onClick={val => onClickFunction(val)}
    >
      <input
        type="checkbox"
        id={id}
        name={id}
        checked={checked}
        disabled={disabled}
      />
      <label htmlFor={id} dangerouslySetInnerHTML={{ __html: label }} />
    </Checkbox>
  );
};

CheckBox.propTypes = {
  label: PropTypes.string,
  margin: PropTypes.string,
  width: PropTypes.string,
  classtype: PropTypes.string,
  filterChekbox: PropTypes.string,
  filterFont: PropTypes.string,
  filterLineheight: PropTypes.string,
};
export default CheckBox;
