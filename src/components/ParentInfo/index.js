/* eslint-disable indent */
import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useDropdownClose } from 'shared/hooks';
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';
import colors from 'utils/colors';
import Button from '../Button';
import InputBox from '../InputBox';
import profile from '../../images/profile.jpg';
// import profile from '../../images/parents-placeholder.png';
import Flex from '../common/flex';
import Form from '../../containers/HomePage/Form';
import purpleSpinner from '../../images/purple_spinner.gif';
import poweredByGoogle from '../../images/powered_by_google_on_white.png';
// eslint-disable-next-line import/named

const ParentInfo = ({
  isUpdate,
  setIsUpdate,
  image,
  name,
  email,
  mobile,
  isVerified,
  margin,
  type,
  onClick,
  onChange,
  onBlur,
  onCancel,
  onVerify,
  onSubmit,
  loader,
  handleImageChnage,
  tempImage,
  setTransportCords,
  setAreaCords,
  locationSearchSting,
  setLocationSearchSting,
  area,
  setArea,
  locationSearchError,
  setLocationSearchError,
  transportLocationSearchError,
  setTransportLocationSearchError,
  ...rest
}) => {
  const fileUploadRef = React.useRef();

  const [locationItemClicked, setLocationItemClicked] = useState(false);
  const [
    locationSearchDropdownState,
    setLocationSearchDropdownState,
  ] = useState(false);

  const [transportLocationClicked, setTransportLocationClicked] = useState(
    false,
  );
  const [
    transportLocationSearchDropdownState,
    setTransportLocationSearchDropdownState,
  ] = useState(false);

  const handleSelect = (selected, locationType) => {
    if (locationType === 'transport') {
      setLocationItemClicked(true);
      setLocationSearchSting(selected);
      setLocationSearchError(false);
    } else {
      setTransportLocationClicked(true);
      setArea(selected);
      setTransportLocationSearchError(false);
    }

    setLocationSearchDropdownState(false);

    geocodeByAddress(selected)
      .then(res => getLatLng(res[0]))
      .then(({ lat, lng }) => {
        if (locationType === 'transport') {
          setTransportCords({
            transport_lattitude: lat,
            transport_longitude: lng,
          });
        } else {
          setAreaCords({
            preferred_lattitude: lat,
            preferred_longitude: lng,
          });
        }
      })
      .catch(() => {
        // this.setState({ isGeocoding: false });
      });
  };

  return (
    <ParentCard
      margin={margin}
      // eslint-disable-next-line react/jsx-no-duplicate-props
      className={type}
      onClick={onClick}
    >
      <Flex
        alignStart
        justifyBetween
        flexPadding="30px"
        className={`afterEdit ${isUpdate ? 'hide' : ''}`}
      >
        <div style={{ width: '100%', display: 'flex' }}>
          <ParentPhoto>
            <img src={image || profile} alt="" />
            <span className="uploadProfileImage">
              <span
                className="iconify"
                data-icon="bytesize:camera"
                data-inline="false"
              />
            </span>
          </ParentPhoto>
          <Flex
            className="detailContent"
            column
            flexMargin="0px 0px 0px 30px"
            flexWidth="430px"
          >
            <Name>
              {name}{' '}
              <Flex className="update">
                <Button
                  type="attach"
                  text="Update your profile"
                  onClick={() => setIsUpdate(true)}
                />
              </Flex>
            </Name>
            <Number>
              +91 {mobile}
              <span
                className="iconify"
                data-icon="feather:check"
                data-inline="false"
              />
            </Number>
            <Email>
              {email}
              {isVerified ? (
                <VerfiedMail>
                  <span
                    className="iconify"
                    data-icon="feather:check"
                    data-inline="false"
                  />
                </VerfiedMail>
              ) : (
                <Button
                  type="attach"
                  text="Verify"
                  onClick={() => onVerify(email)}
                />
              )}
            </Email>
            {locationSearchSting && (
              <Address>
                <div className="text">
                  Transporation location (your home address)
                </div>
                <div style={{ color: 'rgba(0,0,0,0.8)', fontSize: '14px' }}>
                  {locationSearchSting}
                </div>
              </Address>
            )}
            <Address>
              <div className="text">
                Preferred location (your preferred location for school )
              </div>
              <div style={{ color: 'rgba(0,0,0,0.8)', fontSize: '14px' }}>
                {area}
              </div>
            </Address>
          </Flex>
        </div>
        <div
          style={{
            position: 'absolute',
            right: '16px',
          }}
          className="desktopUpdate"
        >
          <Button
            type="attach"
            text="Update your profile"
            onClick={() => setIsUpdate(true)}
          />
        </div>
      </Flex>

      <Flex
        alignStart
        justifyBetween
        className={`beforeEdit ${isUpdate ? 'show' : ''}`}
        flexPadding="30px"
      >
        <Flex flexWidth="100%" className="parentDetails">
          <Flex>
            <UploadPhoto onClick={() => fileUploadRef.current.click()}>
              {tempImage || image ? (
                <ParentPhoto>
                  <img src={tempImage || image} alt="" />
                  {rest.isImageUploading ? (
                    <img src={purpleSpinner} alt="" style={imageLoaderStyles} />
                  ) : null}
                </ParentPhoto>
              ) : (
                'Upload Photo'
              )}
              <div className="uploadIcon">
                <span
                  className="iconify"
                  data-icon="mdi-light:camera"
                  data-inline="false"
                />
              </div>
              <input
                type="file"
                name="file"
                ref={fileUploadRef}
                style={{ display: 'none' }}
                onChange={e => handleImageChnage(e)}
                accept=".jpg,.jpeg,.png"
              />
            </UploadPhoto>
          </Flex>
          <Flex column flexMargin="0px 0px 0px 30px" flexWidth="70%">
            <InputBox
              name="user_name"
              placeholder="Enter the name"
              margin="0px 0px 10px 0px"
              value={name}
              label="Name"
              onChange={e => onChange(e, 'name')}
              onBlur={e => onBlur(e, 'name')}
            />
            <InputBox
              name="user_mobile"
              type="telDisable"
              placeholder="0000000000"
              margin="0px 0px 10px 0px"
              label="Phone number"
              value={mobile}
              disabled
            />
            <InputBox
              name="user_email"
              placeholder="Enter the Email Id"
              margin="0px 0px 10px 0px"
              value={email}
              onChange={e => onChange(e, 'email')}
              onBlur={e => onBlur(e, 'email')}
              label="Email ID"
            />
            <Places
              setLocationItemClicked={setLocationItemClicked}
              setLocationSearchSting={setLocationSearchSting}
              handleSelect={e => handleSelect(e, 'transport')}
              setLocationSearchDropdownState={setLocationSearchDropdownState}
              setLocationSearchError={setLocationSearchError}
              locationSearchSting={locationSearchSting}
              locationSearchError={locationSearchError}
              locationSearchDropdownState={locationSearchDropdownState}
              locationItemClicked={locationItemClicked}
              label="If you like to get transportation for your child, please fill in your complete address below"
              topValue={350}
            />
            <Places
              setLocationItemClicked={setTransportLocationClicked}
              setLocationSearchSting={setArea}
              handleSelect={e => handleSelect(e, 'preferred')}
              setLocationSearchDropdownState={
                setTransportLocationSearchDropdownState
              }
              setLocationSearchError={setTransportLocationSearchError}
              locationSearchSting={area}
              locationSearchError={transportLocationSearchError}
              locationSearchDropdownState={transportLocationSearchDropdownState}
              locationItemClicked={transportLocationClicked}
              label="In which area do you need childcare?"
              topValue={473}
            />

            <Button
              type="mobile"
              text="Update Profile"
              onClick={onSubmit}
              isLoading={loader}
            />
          </Flex>
        </Flex>
        <Flex className="update">
          <Button
            type="attach"
            text="Cancel"
            onClick={() => {
              setIsUpdate(false);
              onCancel();
            }}
          />
        </Flex>
      </Flex>
    </ParentCard>
  );
};

ParentInfo.propTypes = {
  image: PropTypes.string,
  margin: PropTypes.string,
  tempImage: PropTypes.string,
  type: PropTypes.string,
  onClick: PropTypes.func,
  handleImageChnage: PropTypes.func,
  name: PropTypes.string,
  email: PropTypes.string,
  mobile: PropTypes.string,
  isVerified: PropTypes.bool,
  loader: PropTypes.bool,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  onCancel: PropTypes.func,
  onVerify: PropTypes.func,
  onSubmit: PropTypes.bool,
  isUpdate: PropTypes.bool,
  setIsUpdate: PropTypes.bool,
  setTransportCords: PropTypes.func,
  setAreaCords: PropTypes.func,
  locationSearchSting: PropTypes.string,
  setLocationSearchSting: PropTypes.func,
  area: PropTypes.string,
  setArea: PropTypes.func,
  locationSearchError: PropTypes.string,
  setLocationSearchError: PropTypes.func,
  transportLocationSearchError: PropTypes.string,
  setTransportLocationSearchError: PropTypes.func,
};

export default ParentInfo;

const Places = ({
  setLocationItemClicked,
  setLocationSearchSting,
  handleSelect,
  setLocationSearchDropdownState,
  locationSearchDropdownState,
  setLocationSearchError,
  locationSearchError,
  locationSearchSting,
  locationItemClicked,
  label,
  topValue,
}) => {
  const locationSearchDropdown = useRef(null);
  useDropdownClose(locationSearchDropdown, setLocationSearchDropdownState);

  return (
    <Form>
      <PlacesAutocomplete
        value={locationSearchSting}
        onChange={address => {
          setLocationItemClicked(false);

          setLocationSearchSting(address);
        }}
        onSelect={address => handleSelect(address)}
        onError={() => {}}
        clearItemsOnError
        shouldFetchSuggestions
        searchOptions={{
          componentRestrictions: { country: ['in'] },
        }}
      >
        {({ getInputProps, suggestions, getSuggestionItemProps }) => {
          if (suggestions.length > 0) setLocationSearchDropdownState(true);
          else setLocationSearchDropdownState(false);

          return (
            <>
              <InputBox
                {...getInputProps({
                  onBlur: () => {
                    if (locationSearchSting) setLocationSearchError(false);
                    else setLocationSearchError(true);

                    if (locationItemClicked) setLocationSearchError(false);
                    else setLocationSearchError(true);
                  },
                })}
                // name="childcare_area"
                margin="0px 0px 10px 0px"
                value={locationSearchSting}
                label={label}
              />
              {/* <img className="targetIcon" src={targetIcon} alt="" /> */}
              {locationSearchError && (
                <DestinationError>Location is Required</DestinationError>
              )}

              {locationSearchDropdownState &&
                suggestions.length > 0 && (
                  <SearchOption
                    column
                    ref={locationSearchDropdown}
                    topValue={topValue}
                  >
                    {suggestions.map(suggestion => (
                      <li {...getSuggestionItemProps(suggestion, {})}>
                        {suggestion.description}
                      </li>
                    ))}
                    <li className="google">
                      <img src={poweredByGoogle} alt="googleImage" />
                    </li>
                  </SearchOption>
                )}
            </>
          );
        }}
      </PlacesAutocomplete>
    </Form>
  );
};

Places.propTypes = {
  setLocationItemClicked: PropTypes.func,
  setLocationSearchSting: PropTypes.func,
  handleSelect: PropTypes.func,
  setLocationSearchDropdownState: PropTypes.func,
  locationSearchDropdownState: PropTypes.string,
  setLocationSearchError: PropTypes.func,
  locationSearchError: PropTypes.string,
  locationSearchSting: PropTypes.string,
  locationItemClicked: PropTypes.bool,
  label: PropTypes.string,
  topValue: PropTypes.number,
};

// <====Styles====>

const ParentCard = styled.div`
  width: 100%;
  border: 1px solid #dbdee5;
  border-radius: 10px;
  background-color: ${colors.white};
  color: ${colors.secondary};
  margin: ${props => props.margin || '0px 0px 20px'};
  position: relative;
  .afterEdit {
    & > div {
      @media screen and (max-width: 500px) {
        flex-direction: column;
        width: 100%;
      }
    }
    & > .update {
      display: flex;
      @media screen and (max-width: 500px) {
        display: none;
      }
    }
    &.hide {
      display: none;
    }
  }
  .beforeEdit {
    display: none;
    @media screen and (max-width: 800px) {
      padding: 15px;
    }
    @media screen and (max-width: 500px) {
      padding: 20px;
    }
    &.show {
      display: flex;
    }
    .update {
      display: flex;
      position: absolute;
      right: 10px;
    }
    .parentDetails {
      @media screen and (max-width: 500px) {
        flex-direction: column;
      }
      & > div {
        @media screen and (max-width: 500px) {
          margin: 0px;
        }
      }
    }
  }
  .detailContent {
    @media screen and (max-width: 951px) {
      width: 280px;
    }
    @media screen and (max-width: 500px) {
      width: 100%;
    }

    @media screen and (max-width: 500px) {
      margin: 24px 0px 0px;
    }
  }
  .desktopUpdate {
    display: block;
    @media screen and (max-width: 500px) {
      display: none;
    }
  }
`;

const ParentPhoto = styled.div`
  position: relative;
  height: 100px;
  min-width: 100px;
  max-width: 100px;
  border-radius: 50%;
  .uploadProfileImage {
    position: absolute;
    width: 30px;
    height: 30px;
    border-radius: 50%;
    display: flex;
    justify-content: center;
    align-items: center;
    border: 1px dashed rgba(0, 0, 0, 0.4);
    cursor: pointer;
    bottom: -10px;
    left: 35px;
    background: #fff;
    &:hover {
      background: #613a95;
      .iconify {
        color: #fff;
      }
    }
    @media screen and (max-width: 500px) {
      width: 24px;
      height: 24px;
      left: 20px;
    }
    .iconify {
      width: 15px;
      height: 15px;
      @media screen and (max-width: 500px) {
        width: 12px;
        height: 12px;
      }
    }
  }
  img {
    width: 100%;
    height: 100%;
    border-radius: 50%;
  }
  @media screen and (max-width: 800px) {
    height: 86px;
    min-width: 86px;
    max-width: 86px;
  }
  @media screen and (max-width: 500px) {
    height: 64px;
    min-width: 64px;
    max-width: 64px;
  }
`;
const Name = styled.div`
  color: #30333b;
  font-size: 20px;
  line-height: 24px;
  margin-bottom: 20px;
  @media screen and (max-width: 500px) {
    display: flex;
    justify-content: space-between;
    font-size: 16px;
    line-height: 20px;
    margin-bottom: 10px;
    flex-direction: column;
  }
  .update {
    display: none;
    @media screen and (max-width: 500px) {
      display: flex;
    }
  }
`;
const Number = styled.div`
  width: 60%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  color: #30333b;
  font-size: 16px;
  line-height: 19px;
  margin-bottom: 10px;
  @media screen and (max-width: 500px) {
    width: 100%;
    font-size: 14px;
    line-height: 19px;
    margin-bottom: 5px;
  }
  .iconify {
    color: #60b947;
    width: 22px;
    height: 22px;
  }
`;
const Email = styled.div`
  width: 60%;
  color: #30333b;
  font-size: 16px;
  line-height: 19px;
  display: flex;
  margin: 0px 0px 10px;
  justify-content: space-between;
  align-items: center;
  @media screen and (max-width: 951px) {
    width: 100%;
  }
  .attach {
    @media screen and (max-width: 500px) {
      font-size: 12px;
    }
  }
  @media screen and (max-width: 500px) {
    width: 100%;
    font-size: 12px;
    line-height: 16px;
  }
`;

const Address = styled.div`
  width: 100%;
  color: #30333b;
  font-size: 16px;
  line-height: 19px;
  display: flex;
  flex-direction: column;
  margin: 0px 0px 15px;
  .text {
    font-size: 16px;
    margin-bottom: 5px;
    font-family: 'Quicksand', sans-serif;
    font-weight: 700;
    color: #613a95;
    @media screen and (max-width: 500px) {
      font-size: 14px;
      width: 95%;
    }
  }

  .attach {
    @media screen and (max-width: 500px) {
      font-size: 12px;
    }
  }
  @media screen and (max-width: 500px) {
    font-size: 12px;
    line-height: 16px;
  }
`;

const VerfiedMail = styled.div`
  font-size: 22px;
  color: #60b947;
`;

const UploadPhoto = styled.div`
  position: relative;
  display: flex;
  text-align: center;
  align-items: center;
  height: 100px;
  width: 100px;
  border: 1px dashed #c0c8cd;
  border-radius: 50px;
  background-color: #f2eff5;
  font-size: 14px;
  font-weight: bold;
  line-height: 16px;
  color: #a5abb9;
  text-transform: UPPERCASE;
  @media screen and (max-width: 800px) {
    height: 86px;
    width: 86px;
  }
  @media screen and (max-width: 500px) {
    height: 64px;
    width: 64px;
    margin-bottom: 20px;
  }
  .uploadIcon {
    position: absolute;
    display: flex;
    text-align: center;
    justify-content: center;
    align-items: center;
    min-width: 34px;
    width: 34px;
    height: 34px;
    background-color: #613a95;
    border-radius: 50px;
    bottom: 0px;
    right: 0px;
    @media screen and (max-width: 500px) {
      width: 28px;
      height: 28px;
      min-width: 28px;
    }
    .iconify {
      color: #fff;
      width: 19px;
      height: 19px;
      @media screen and (max-width: 500px) {
        width: 16px;
        height: 16px;
      }
    }
  }
`;

const DestinationError = styled.span`
  margin: 10px 0px 0px 0px;
  color: red;
  font-size: 12px;
  line-height: 14px;
`;

const SearchOption = styled(Flex)`
  position: absolute;
  top: ${({ topValue }) => `${topValue}px`};
  width: 100%;
  max-width: 442px;
  padding: 20px 20px 40px;
  border-radius: 5px;
  background: ${colors.white};
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.3);
  z-index: 1000000;
  .google {
    position: absolute;
    right: 20px;
    bottom: 0px;
    // padding:20px 0px;
  }
  li {
    font-size: 14px;
    line-height: 30px;
    border-bottom: 1px solid rgba(0, 0, 0, 0.1);
    list-style-type: none;
    padding: 5px 0px;
    color: #666c78;
    cursor: pointer;
    &:last-child {
      border-bottom: 0px solid rgba(0, 0, 0, 0.1);
    }
    &:hover {
      color: #000;
    }
  }
`;

const imageLoaderStyles = {
  height: 30,
  width: 30,
  position: 'absolute',
  left: 37,
  top: 30,
};
