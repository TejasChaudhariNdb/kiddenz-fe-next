import React, { useRef, useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Button from '../Button';
import colors from '../../utils/colors';
import InputBox from '../InputBox';
import { useScheduleHook } from '../../shared/hooks';

const ThankYouModal = props => {
  const {
    closeHandler,
    USER_ENQUIRY_API_CALL,
    providerId,
    errorToast,
    successToast,
  } = props;
  const currentRef = useRef();
  const [value, setValue] = useState('');
  const [error, setError] = useState('');

  const { userEnquiryLoader } = useScheduleHook(props);
  console.log(userEnquiryLoader, 'userEnquiryLoader');

  return (
    <AdditionalFilterWrapper className="tour" ref={currentRef}>
      <CloseModal onClick={closeHandler}>
        {' '}
        <span
          className="iconify"
          data-icon="bytesize:close"
          data-inline="false"
        />
      </CloseModal>

      <GalleryHeading margin="0px 0px 10px">
        Thank you for your interest
      </GalleryHeading>

      <OrgName margin="0px 0px 20px">
        Our team will get in touch with you shortly
      </OrgName>
      <div className="scheduleTour">
        <InputBox
          name="Let us know you’re query"
          type="textarea"
          value={value}
          onChange={e => setValue(e.target.value)}
          onBlur={e =>
            e.target.value ? setError('') : setError('Field is required.')
          }
        />
        <Error>{error}</Error>
        <Button
          text="Send"
          type="mobile"
          marginRight="10px"
          isLoading={userEnquiryLoader}
          onClick={() => {
            if (value) {
              USER_ENQUIRY_API_CALL({
                payload: {
                  provider: providerId,
                  enquire: value,
                },
                errorCallback: ({ message }) => {
                  errorToast(message || 'Something Went wrong');
                },
                successCallback: () => {
                  successToast('Successfully posted.');
                  closeHandler();
                },
              });
            } else {
              setError('Field is required.');
            }
          }}
        />
      </div>
    </AdditionalFilterWrapper>
  );
};

ThankYouModal.propTypes = {
  closeHandler: PropTypes.func,
  USER_ENQUIRY_API_CALL: PropTypes.func,
  providerId: PropTypes.number,
  errorToast: PropTypes.func,
  successToast: PropTypes.func,
};

export default ThankYouModal;

const AdditionalFilterWrapper = styled.div`
  width: 710px;
  height: ${props => props.height};
  margin: 5% auto;
  padding: 25px;
  padding-left: 45px;
  border-radius: 10px;
  background: ${colors.white};
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
  position: relative;

  &.tour {
    max-width: 500px;
    width: 100%;
    padding: 50px 48px;
    margin: 5% auto 20px;
    @media screen and (max-width: 768px) {
      max-width: 290px;
      width: 100%;
      padding: 20px;
      margin: 20% auto;
    }
    .scheduleTour {
      display: flex;
      flex-direction: column;
    }
    @media (max-width: 500px) {
      max-width: 380px !important;
      width: 100% !important;
      padding: 50px 20px;
    }
    @media (max-width: 380px) {
      max-width: 320px !important;
    }
    @media (max-width: 330px) {
      max-width: 300px !important;
    }
  }
`;

export const GalleryHeading = styled.div`
  margin: ${props => props.margin || '0px'};
  color: ${props => props.color || '#30333B'};
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 500;
  line-height: 28px;
  @media (max-width: 500px) {
    font-size: 16px;
    line-height: 25px;
    margin-bottom: 30px;
  }
`;

const CloseModal = styled.div`
  position: absolute;
  top: 20px;
  right: 20px;
  cursor: pointer;
  @media screen and (max-width: 768px) {
    top: 1px;
    right: 7px;
  }
  .iconify {
    height: 12px;
    width: 12px;
    color: #666c78;
  }
`;

const OrgName = styled.div`
  color: #30333b;
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 19px;
  margin: ${props => props.margin || '0px'};
`;

export const ScheduleDetail = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  border-radius: 10px;
  background-color: #f3f5f9;
  padding: 18px 24px;
  margin-bottom: 10px;
  h2 {
    color: #30333b;
    font-family: 'Quicksand', sans-serif;
    font-size: 16px;
    font-weight: bold;
    line-height: 26px;
    text-align: center;
    margin: 0px;
    span {
      font-weight: 300;
      margin-right: 4px;
    }
  }
`;

const Error = styled.div`
  font-size: 12px;
  color: red;
`;
