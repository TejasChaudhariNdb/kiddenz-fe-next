/* eslint-disable */
import React, { useRef, useState, useEffect } from 'react';
import Router from 'next/router';
import styled from 'styled-components';
import Button from '../Button';
import RadioButton from '../RadioButton';
import colors from '../../utils/colors';
import Flex from '../common/flex';
import InputBox from '../InputBox';
// import Form from '../Form';
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';
import { useDropdownClose, useDayCareSearchHook } from 'shared/hooks';
import Input from '../../containers/HomePage/Input';
import Form from '../../containers/HomePage/Form';

import child from '../../images/teddy_icon.svg';
import targetIcon from '../../images/target.svg';

import transportIcon from '../../images/transport_icon.svg';
import FoodIcon from '../../images/food_icon.svg';
import joiningIcon from '../../images/joining time_icon.svg';
import careIcon from '../../images/care_icon.svg';
import scheduleIcon from '../../images/schedule_icon.svg';
import locationIcon from '../../images/Kiddenz_icon-location-06.svg';
import modalImage from '../../images/transport_icon.svg';
import { useOnboardingHook } from '../../shared/hooks';
import poweredByGoogle from '../../images/powered_by_google_on_white.png';
const ONBOARDING_DATA = {
  child_care_type: '1',
  ideal_joining_time: 'immediate',
  schedule_type: 'full time',
};

const CARE_TYPE_MAP = ['pre school', 'day care', 'both'];

const OnboardingModal = ({
  setActive,
  setModalType,
  onOnboardingSuccessCallback,
  ...props
}) => {
  const {
    router: { query: { form } = {} } = {},
    authentication: {
      isLoggedIn,
      profile: { data: { name = '' } = {} } = {},
    } = {},
  } = props;

  const currentRef = useRef();
  const { successToast, errorToast } = props;
  const [currentStep, setCurrentStep] = useState(0);
  const [activeCareType, setActiveCareType] = useState('1');
  const [activeJoiningTime, setActiveJoiningTime] = useState('immediate');
  const [activeSchedule, setActiveSchedule] = useState('full time');
  const [activeTransportFacility, setActiveTransportFacility] = useState(
    'Pick Up',
  );
  const [transportFacilityStatus, setTransportFacilityStatus] = useState(true);
  const [transportCords, setTransportCords] = useState({
    transport_lattitude: 0.0,
    transport_longitude: 0.0,
  });

  const [areaCords, setAreaCords] = useState({
    preferred_lattitude: 0.0,
    preferred_longitude: 0.0,
  });

  const [activeFoodFacility, setActiveFoodFacility] = useState('Breakfast');
  const [foodFacilityStatus, setFoodFacilityStatus] = useState(true);

  const [locationSearchSting, setLocationSearchSting] = useState('');
  const [locationItemClicked, setLocationItemClicked] = useState(false);
  const [
    locationSearchDropdownState,
    setLocationSearchDropdownState,
  ] = useState(false);
  const [locationSearchError, setLocationSearchError] = useState(false);

  const [area, setArea] = useState('');
  const [areaErr, setAreaErr] = useState('');

  const [pincode, setPincode] = useState('');
  const [pincodeErr, setPincodeErr] = useState('');

  const [children, setChildren] = useState([]);
  const [hoveredChildren, setHoveredChildren] = useState(0);
  const [isChildrenHovered, setIsChildrenHovered] = useState(false);
  const [totalChildren, setTotalChildren] = useState(0);
  const [error, setError] = useState('');

  const [requirements, setRequirements] = useState('');

  const handleSelect = (selected, locationType) => {
    setLocationItemClicked(true);
    locationType === 'transport'
      ? setLocationSearchSting(selected)
      : setArea(selected);

    setLocationSearchDropdownState(false);
    setLocationSearchError(false);

    geocodeByAddress(selected)
      .then(res => getLatLng(res[0]))
      .then(({ lat, lng }) => {
        if (locationType === 'transport') {
          setTransportCords({
            transport_lattitude: lat,
            transport_longitude: lng,
          });
        } else {
          setAreaCords({
            preferred_lattitude: lat,
            preferred_longitude: lng,
          });
        }
      })
      .catch(error => {
        // this.setState({ isGeocoding: false });
      });
  };

  const onSuccess = ({ data }) => {
    if (form === 'review_form') {
      Router.push({
        pathname: '/review',
      }).then(() => window.scrollTo(0, 0));
    }
    const { data: { question_serializer: defFilters } = {} } = data;
    if (onOnboardingSuccessCallback) onOnboardingSuccessCallback(defFilters);
    setActive(false);
    setModalType('');
  };
  const onError = ({ message }) => {
    errorToast(message || 'Something went wrong.');
  };

  const { onSubmit, onboardingLoader } = useOnboardingHook(props, {
    onSuccess,
    onError,
  });

  const prevStepHandler = e => {
    e.preventDefault();
    setCurrentStep(d => d - 1);
  };

  const nextStepHandler = e => {
    e.preventDefault();
    if (
      currentStep === 0 &&
      (children.length === 0 ||
        children.includes(undefined) ||
        children.includes(''))
    ) {
      setError('Fill in children details.');
    } else if (
      currentStep === 0 &&
      (children.length > 0 && children.filter(dt => dt > 15).length > 0)
    ) {
      setError('Age should be less than 15 years.');
    } else if (
      currentStep === 4 &&
      transportFacilityStatus === true &&
      !locationSearchSting
    ) {
      setLocationSearchError(true);
    } else if (
      currentStep === 6 &&
      (!area || !pincode || pincodeErr || areaErr)
    ) {
      console.log(area, 'area');
      if (!pincode) {
        setPincodeErr('Enter Pincode');
      }
      if (!area) {
        setLocationSearchError(true);
      }
    } else {
      onSubmit({
        // child_care_type: activeCareType,
        // ideal_joining_time: activeJoiningTime,
        // schedule_type: activeSchedule,
        // transport_facility: transportFacilityStatus,
        // food_facility: foodFacilityStatus,
        children: children.map(d => ({
          age: d,
        })),
        // ...(transportFacilityStatus === true
        //   ? transportCords
        //   : {}),
        // ...areaCords,
        // preferred_pincode: pincode,
        // specific_requirement: requirements,
        // prefered_location_string: area,
        // transport_location_string: locationSearchSting,
      });
      setError('');
      // setPincodeErr('');
      // setAreaErr('');
      // setLocationSearchError(false);
      // setCurrentStep(d => d + 1);
    }
  };

  return (
    <AdditionalFilterWrapper className="tour" ref={currentRef}>
      <Flex column flexHeight="fit-content">
        {(currentStep === 0 || currentStep === 7) && (
          <ModalTitle className="loginQues">
            <div>
              Hi, <span>{name}</span>
            </div>
            {currentStep === 0
              ? 'Let us know a bit about your child'
              : 'All set! You are good to go'}
          </ModalTitle>
        )}
        <ModalTitle
          className="loginQues__title"
          style={
            currentStep === 7
              ? { padding: '0px 40px', fontSize: '16px', lineHeight: '18px' }
              : {}
          }
        >
          {/* {currentStep !== 7 ? `${currentStep + 1}.` : ''}{' '} */}
          {STEPS[currentStep].title}
        </ModalTitle>
        {currentStep === 6 && (
          <ModalSubInfo>To suggest preschools near your location</ModalSubInfo>
        )}
        {STEPS[currentStep].icon && (
          <ModalLoginImage>
            <img src={STEPS[currentStep].icon} alt="" />
          </ModalLoginImage>
        )}

        {currentStep === 0 && (
          <div>
            <Flex
              justifyCenter
              flexMargin="86px 0px 0px"
              className="ageOfchild"
            >
              {[...Array(5)].map((d, i) => (
                <ChildAge
                  column
                  alignCenter
                  className={
                    (isChildrenHovered && i <= hoveredChildren) ||
                    (!isChildrenHovered && i <= totalChildren)
                      ? 'active'
                      : ''
                  }
                >
                  <ChildAgeImg
                    onMouseEnter={() => {
                      setHoveredChildren(i);
                      setIsChildrenHovered(true);
                    }}
                    onMouseLeave={() => {
                      setHoveredChildren(0);
                      setIsChildrenHovered(false);
                    }}
                    onClick={() => {
                      setTotalChildren(i);
                      const temp = new Array(i + 1);
                      const newArr =
                        children.length > 0
                          ? [...children, ...temp].splice(0, i + 1)
                          : temp;
                      setChildren(newArr);
                    }}
                  >
                    <img src={child} alt="" />
                  </ChildAgeImg>
                  <InputBox
                    key={`age_${i}`}
                    name={`age_${i}`}
                    placeholder="Age"
                    type="childAge"
                    value={children[i] ? children[i] : ''}
                    onChange={e => {
                      if (
                        Number(e.target.value) ||
                        Number(e.target.value) === 0
                      ) {
                        const temp = [...children];
                        temp[i] = e.target.value;
                        setChildren(temp);
                      }
                      return 0;
                    }}
                  />
                </ChildAge>
              ))}
            </Flex>
            <ErrorMsg>{error}</ErrorMsg>
          </div>
        )}

        {currentStep === 1 && (
          <Flex column>
            <CareType
              className={activeCareType === '1' ? 'active' : ''}
              onClick={() => {
                ONBOARDING_DATA.child_care_type = '1';
                setActiveCareType('1');
                return 0;
              }}
            >
              Pre-School
              <span
                className="iconify"
                data-icon="feather:check"
                data-inline="false"
              />
            </CareType>
            <CareType
              className={activeCareType === '2' ? 'active' : ''}
              onClick={() => {
                ONBOARDING_DATA.child_care_type = '2';
                setActiveCareType('2');
                return 0;
              }}
            >
              Day Care
              <span
                className="iconify"
                data-icon="feather:check"
                data-inline="false"
              />
            </CareType>
            <CareType
              className={activeCareType === '3' ? 'active' : ''}
              onClick={() => {
                ONBOARDING_DATA.child_care_type = '3';
                setActiveCareType('3');
                return 0;
              }}
            >
              Both
              <span
                className="iconify"
                data-icon="feather:check"
                data-inline="false"
              />
            </CareType>
          </Flex>
        )}

        {currentStep === 2 && (
          <Flex column>
            <CareType
              className={activeJoiningTime === 'immediate' ? 'active' : ''}
              onClick={() => {
                ONBOARDING_DATA.ideal_joining_time = 'immediate';
                setActiveJoiningTime('immediate');
                return 0;
              }}
            >
              Immediate
              <span
                className="iconify"
                data-icon="feather:check"
                data-inline="false"
              />
            </CareType>

            <CareType
              className={
                activeJoiningTime === 'less than a month' ? 'active' : ''
              }
              onClick={() => {
                ONBOARDING_DATA.ideal_joining_time = 'less than a month';
                setActiveJoiningTime('less than a month');
                return 0;
              }}
            >
              Less than a month
              <span
                className="iconify"
                data-icon="feather:check"
                data-inline="false"
              />
            </CareType>
            <CareType
              className={activeJoiningTime === '2 Weeks' ? 'active' : ''}
              onClick={() => {
                ONBOARDING_DATA.ideal_joining_time = '2 Weeks';
                setActiveJoiningTime('2 Weeks');
                return 0;
              }}
            >
              More than a month
              <span
                className="iconify"
                data-icon="feather:check"
                data-inline="false"
              />
            </CareType>
          </Flex>
        )}

        {currentStep === 3 && (
          <Flex column>
            <CareType
              className={activeSchedule === 'full time' ? 'active' : ''}
              onClick={() => {
                ONBOARDING_DATA.schedule_type = 'full time';
                setActiveSchedule('full time');
                return 0;
              }}
            >
              Full Time
              <span
                className="iconify"
                data-icon="feather:check"
                data-inline="false"
              />
            </CareType>
            <CareType
              className={activeSchedule === 'weekend' ? 'active' : ''}
              onClick={() => {
                ONBOARDING_DATA.schedule_type = 'weekend';
                setActiveSchedule('weekend');
                return 0;
              }}
            >
              Weekend
              <span
                className="iconify"
                data-icon="feather:check"
                data-inline="false"
              />
            </CareType>
            <CareType
              className={activeSchedule === 'flexible' ? 'active' : ''}
              onClick={() => {
                ONBOARDING_DATA.schedule_type = 'flexible';
                setActiveSchedule('flexible');
                return 0;
              }}
            >
              Flexible
              <span
                className="iconify"
                data-icon="feather:check"
                data-inline="false"
              />
            </CareType>
          </Flex>
        )}

        {currentStep === 4 && (
          <Flex column>
            {/* <Flex flexMargin="0px 0px 20px 0px" justifyCenter>
              {/* <RadioButton
                id="trans1"
                text="Yes"
                filterRadio="#4B5256"
                filterFont="13px"
                filterFontWeight="400"
                filterLineheight="21px"
                filterPadding="0px 0px 0px 30px"
                RadioMargin="0px 25px 0px 0px"
                checked={transportFacilityStatus === true}
                onClick={() => {
                  setTransportFacilityStatus(true);
                  // setActiveTransportFacility('Pick Up');
                }}
              />
              <RadioButton
                id="trans2"
                text="No"
                filterRadio="#4B5256"
                filterFont="13px"
                filterFontWeight="400"
                filterLineheight="21px"
                filterPadding="0px 0px 0px 30px"
                RadioMargin="0px 25px 0px 0px"
                checked={transportFacilityStatus === false}
                onClick={() => {
                  setTransportFacilityStatus(false);
                  // setActiveTransportFacility(null);
                }}
              /> 
            </Flex> */}
            <CareType
              className={transportFacilityStatus === true ? 'active' : ''}
              onClick={() => {
                setTransportFacilityStatus(true);
              }}
            >
              Yes
              <span
                className="iconify"
                data-icon="feather:check"
                data-inline="false"
              />
            </CareType>
            <CareType
              className={transportFacilityStatus === false ? 'active' : ''}
              onClick={() => {
                setTransportFacilityStatus(false);
                setLocationSearchError(false);
              }}
            >
              No
              <span
                className="iconify"
                data-icon="feather:check"
                data-inline="false"
              />
            </CareType>
            {transportFacilityStatus === true ? (
              <>
                <ModalTitle
                  className="loginQues small"
                  style={{ fontSize: 18, margin: '10px 0px 20px' }}
                >
                  For which location do you need the transport?
                </ModalTitle>
                <PlacesAutoComplete
                  setLocationItemClicked={setLocationItemClicked}
                  setLocationSearchSting={setLocationSearchSting}
                  handleSelect={e => handleSelect(e, 'transport')}
                  setLocationSearchDropdownState={
                    setLocationSearchDropdownState
                  }
                  setLocationSearchError={setLocationSearchError}
                  locationSearchSting={locationSearchSting}
                  locationSearchError={locationSearchError}
                  locationSearchDropdownState={locationSearchDropdownState}
                  locationItemClicked={locationItemClicked}
                />
              </>
            ) : null}
          </Flex>
        )}

        {currentStep === 5 && (
          <Flex column>
            <Flex justifyCenter flexMargin="0px 0px 20px 0px">
              {/* <RadioButton
                id="cook1"
                text="Yes"
                filterRadio="#4B5256"
                filterFont="13px"
                filterFontWeight="400"
                filterLineheight="21px"
                filterPadding="0px 0px 0px 30px"
                RadioMargin="0px 25px 0px 0px"
                checked={foodFacilityStatus === true}
                onClick={() => {
                  setFoodFacilityStatus(true);
                  // setActiveFoodFacility('Breakfast');
                }}
              />
              <RadioButton
                id="cook2"
                text="No"
                filterRadio="#4B5256"
                filterFont="13px"
                filterFontWeight="400"
                filterLineheight="21px"
                filterPadding="0px 0px 0px 30px"
                RadioMargin="0px 25px 0px 0px"
                checked={foodFacilityStatus === false}
                onClick={() => {
                  setFoodFacilityStatus(false);
                  // setActiveFoodFacility(null);
                }}
              /> */}
            </Flex>
            <CareType
              className={activeFoodFacility === 'Breakfast' ? 'active' : ''}
              onClick={() => {
                ONBOARDING_DATA.care = 'breakfast';
                setActiveFoodFacility('Breakfast');
                return 0;
              }}
            >
              Yes
              <span
                className="iconify"
                data-icon="feather:check"
                data-inline="false"
              />
            </CareType>
            <CareType
              className={activeFoodFacility === 'Lunch' ? 'active' : ''}
              onClick={() => {
                ONBOARDING_DATA.care = 'lunch';
                setActiveFoodFacility('Lunch');
                return 0;
              }}
            >
              No
              <span
                className="iconify"
                data-icon="feather:check"
                data-inline="false"
              />
            </CareType>
          </Flex>
        )}

        {currentStep === 6 && (
          <Flex column>
            <PlacesAutoComplete
              setLocationItemClicked={setLocationItemClicked}
              setLocationSearchSting={setArea}
              handleSelect={e => handleSelect(e, 'preferred')}
              setLocationSearchDropdownState={setLocationSearchDropdownState}
              setLocationSearchError={setLocationSearchError}
              locationSearchSting={area}
              locationSearchError={locationSearchError}
              locationSearchDropdownState={locationSearchDropdownState}
              locationItemClicked={locationItemClicked}
            />
            <InputBox
              placeholder="Enter pincode"
              type="text"
              name="pincode"
              value={pincode}
              onChange={e => {
                setPincode(e.target.value);
                if (Number(e.target.value)) {
                  if (e.target.value.length === 6) {
                    setPincodeErr('');
                  } else setPincodeErr('Enter valid pincode');
                }
              }}
              error={pincodeErr}
            />
          </Flex>
        )}

        {currentStep === 7 && (
          <>
            <Flex>
              <SummaryReport>
                <li>
                  {' '}
                  1. I have{' '}
                  <span>
                    {' '}
                    {children.length}{' '}
                    {children.length > 1 ? 'children' : 'child'}{' '}
                    {(() => {
                      let temp = [...children];
                      if (temp.length > 1) {
                        let firstHalf = temp.slice(0, temp.length - 1);
                        firstHalf.push('and');

                        const secondHalf = temp.slice(
                          temp.length - 1,
                          temp.length,
                        );
                        temp = [...firstHalf, ...secondHalf];
                      }
                      return temp.join(' ');
                    })()}{' '}
                    years old {children.length > 1 ? 'respectively' : ''}
                  </span>
                </li>
                <li>
                  2. Type of care:{' '}
                  <CapitalizedSpan>
                    {' '}
                    {CARE_TYPE_MAP[Number(activeCareType) - 1]}
                  </CapitalizedSpan>
                </li>
                <li>
                  3. Joining Time:{' '}
                  <CapitalizedSpan> {activeJoiningTime}</CapitalizedSpan>
                </li>
                <li>
                  4. My schedule:
                  <CapitalizedSpan> {activeSchedule}</CapitalizedSpan>
                </li>
                <li>
                  5. Transportation Facility:
                  <span> {transportFacilityStatus ? 'Yes' : 'No'}</span>
                </li>
                <li>
                  6. Freshly Cooked Food:
                  <span>
                    {' '}
                    {activeFoodFacility === 'Breakfast' ? 'Yes' : 'No'}
                  </span>
                </li>
                <li>
                  7. Preferred location:
                  <span>
                    {' '}
                    {area}, {pincode}
                  </span>
                </li>
              </SummaryReport>
            </Flex>
            <Flex>
              <InputBox
                name={`textarea`}
                rows="4"
                margin="0px 0px 10px"
                placeholder="Requirements"
                type="textarea"
                label="Any specific requirements? Please mention below"
                value={requirements}
                onChange={e => setRequirements(e.target.value)}
              />
            </Flex>
          </>
        )}
        <Flex
          justifyBetween
          flexWidth={STEPS[currentStep].footerFlexWidth}
          flexMargin={STEPS[currentStep].footerFlexMargin}
        >
          {currentStep !== 0 &&
            currentStep !== 7 && (
              <TermsLink color="#666C78" onClick={prevStepHandler}>
                Back
              </TermsLink>
            )}
          <Flex flexWidth="100%">
            <Button
              href="/"
              text="Done"
              type="mobile"
              height="36px !important"
              headerButton
              isLoading={onboardingLoader}
              onClick={nextStepHandler}
            />
          </Flex>
          {/* {currentStep !== 7 && (
            <Indicator>
              {[...Array(7)].map((_, i) => (
                <span className={i <= currentStep ? 'active' : ''} />
              ))}
            </Indicator>
          )}
          {currentStep < 7 && (
            <TermsLink onClick={nextStepHandler}>Next</TermsLink>
          )} */}
          {currentStep === 7 && (
            <>
              <Flex justifyCenter flexWidth="100%">
                <Button
                  href="/"
                  text="Back"
                  type="subscribe"
                  height="36px"
                  headerButton
                  marginRight="10px"
                  onClick={prevStepHandler}
                />

                <Button
                  href="/"
                  text="Done"
                  type="mobile"
                  height="36px !important"
                  headerButton
                  isLoading={onboardingLoader}
                  onClick={() =>
                    onSubmit({
                      child_care_type: activeCareType,
                      ideal_joining_time: activeJoiningTime,
                      schedule_type: activeSchedule,
                      transport_facility: transportFacilityStatus,
                      food_facility: foodFacilityStatus,
                      children: children.map(d => ({
                        age: d,
                      })),
                      // ...(transportFacilityStatus === true
                      //   ? transportCords
                      //   : {}),
                      // ...areaCords,
                      // preferred_pincode: pincode,
                      // specific_requirement: requirements,
                      // prefered_location_string: area,
                      // transport_location_string: locationSearchSting,
                    })
                  }
                />
              </Flex>
            </>
          )}
        </Flex>
      </Flex>
    </AdditionalFilterWrapper>
  );
};

export default OnboardingModal;

const PlacesAutoComplete = ({
  setLocationItemClicked,
  setLocationSearchSting,
  handleSelect,
  setLocationSearchDropdownState,
  locationSearchDropdownState,
  setLocationSearchError,
  locationSearchError,
  locationSearchSting,
  locationItemClicked,
}) => {
  const locationSearchDropdown = useRef(null);
  useDropdownClose(locationSearchDropdown, setLocationSearchDropdownState);

  return (
    <Form className="locationForm">
      <PlacesAutocomplete
        value={locationSearchSting}
        onChange={address => {
          setLocationItemClicked(false);
          // if (address) {
          //   setLocationSearchError(false);
          // } else {
          //   setLocationSearchError(true);
          // }
          setLocationSearchSting(address);
        }}
        onSelect={address => handleSelect(address)}
        onError={() => {}}
        clearItemsOnError
        shouldFetchSuggestions
        searchOptions={{
          componentRestrictions: { country: ['in'] },
        }}
      >
        {({ getInputProps, suggestions, getSuggestionItemProps }) => {
          if (suggestions.length > 0) setLocationSearchDropdownState(true);
          else setLocationSearchDropdownState(false);

          return (
            <>
              <label htmlFor="username">
                <div className="searchInputField two">
                  <Input
                    {...getInputProps({
                      onBlur: () => {
                        if (locationSearchSting) setLocationSearchError(false);
                        else setLocationSearchError(true);

                        if (locationItemClicked) setLocationSearchError(false);
                        else setLocationSearchError(true);
                      },
                    })}
                    type="text"
                    placeholder="Enter complete address"
                    value={locationSearchSting}
                    // disabled={activeTransportFacility === 'Drop'}
                    autoComplete="off"
                    className="search"
                  />
                </div>
                <img className="targetIcon" src={targetIcon} alt="" />
              </label>
              {locationSearchError && (
                <DestinationError>Location is Required</DestinationError>
              )}
              <div style={{ width: '166px' }} className="buttonWrapper">
                {/* <Button
                 text="Search"
                 headerButton
                 marginTop="10px"
                 onClick={e => {
                   e.preventDefault();
                   if (!locationSearchSting)
                     setLocationSearchError(true);
                   else {
                     setLocationSearchError(false);
 
                     if (isLoggedIn)
                       Router.push(
                         `/daycare/search?locationString=${locationSearchSting}&latitude=${locationLat}&longitude=${locationLng}&organisation=${organisation}&transportation=${transportation}&food=${food}&schedule_type=${
                           SCHEDULE_CONFIG[schedule]
                         }&type=radius&sort_by=distance`,
                       ).then(() => window.scrollTo(0, 0));
                     else
                       Router.push(
                         `/daycare/search?locationString=${locationSearchSting}&latitude=${locationLat}&longitude=${locationLng}&type=radius`,
                       ).then(() => window.scrollTo(0, 0));
                   }
                 }}
                 disabled={searchType !== 'location'}
               /> */}
              </div>

              {locationSearchDropdownState &&
                suggestions.length > 0 && (
                  <SearchOption column ref={locationSearchDropdown}>
                    {suggestions.map(suggestion => (
                      <li {...getSuggestionItemProps(suggestion, {})}>
                        {suggestion.description}
                      </li>
                    ))}
                    <li className="google">
                      <img src={poweredByGoogle} alt="googleImage" />
                    </li>
                  </SearchOption>
                )}
            </>
          );
        }}
      </PlacesAutocomplete>
    </Form>
  );
};

const STEPS = [
  {
    title: 'How many children need care?',
    footerFlexWidth: '60%',
    footerFlexMargin: '60px 0px 0px auto',
  },
  {
    title: 'What kind of care do you need?',
    footerFlexWidth: '100%',
    footerFlexMargin: '60px 0px 0px 0px',
    icon: careIcon,
  },
  {
    title: 'When is your ideal joining time?',
    footerFlexWidth: '100%',
    footerFlexMargin: '60px 0px 0px 0px',
    icon: joiningIcon,
  },
  {
    title: 'Which is your preferred schedule?',
    footerFlexWidth: '100%',
    footerFlexMargin: '60px 0px 0px 0px',
    icon: scheduleIcon,
  },
  {
    title: 'Do you need transport facility for your child?',
    footerFlexWidth: '100%',
    footerFlexMargin: '60px 0px 0px 0px',
    icon: transportIcon,
  },
  {
    title: 'Require freshly cooked food at childcare?',
    footerFlexWidth: '100%',
    footerFlexMargin: '60px 0px 0px 0px',
    icon: FoodIcon,
  },
  {
    title: 'What is your preferred location?',
    footerFlexWidth: '100%',
    footerFlexMargin: '60px 0px 0px 0px',
    icon: locationIcon,
  },
  {
    title: 'We are excited to help you find the best place for your child',
    footerFlexWidth: '100%',
    footerFlexMargin: 'auto 0px 0px 0px',
    icon: '',
  },
];

const AdditionalFilterWrapper = styled.div`
  width: 710px;
  height: ${props => props.height};
  max-height: fit-content;
  margin: 5% auto;
  padding: 25px;
  padding-left: 45px;
  border-radius: 10px;
  background: ${colors.white};
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
  position: relative;
  &.testimonials {
    max-width: 665px;
    width: 100%;
    padding: 50px 25px 50px 36px;
    margin: 4% auto 20px;
  }
  &.teachers {
    max-width: 1022px;
    width: 100%;
    padding: 62px 58px;
    margin: 5% auto;
  }
  &.tour {
    max-width: 500px;
    width: 100%;
    padding: 38px 48px;
    margin: 5% auto 20px;
    @media screen and (max-width: 768px) {
      max-width: 290px;
      width: 100%;
      padding: 20px;
      margin: 20% auto;
    }
    @media (max-width: 500px) {
      max-width: 330px;
      padding: 38px 16px;
    }
    .scheduleTour {
      display: flex;
      flex-direction: column;
      .personalDetail {
        color: #30333b;
        font-family: 'Roboto', sans-serif;
        font-size: 16px;
        line-height: 22px;
        margin-bottom: 20px;
      }
    }
  }
  .confirmMessage {
    //   display:flex;
    //   flex-direction:column;
    //   align-items:center;
    text-align: center;
  }
  .popupScroll {
    max-height: 590px;
    overflow: scroll;
  }
  .ageOfchild {
    @media (max-width: 500px) {
      margin: 40px 0px 0px;
    }
  }
`;

export const GalleryHeading = styled.div`
  margin: ${props => props.margin || '0px'};
  color: ${props => props.color || '#30333B'};
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 500;
  line-height: 28px;
`;

export const ScheduleDetail = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  border-radius: 10px;
  background-color: #f3f5f9;
  padding: 18px 24px;
  margin-bottom: 10px;
  h2 {
    color: #30333b;
    font-family: 'Quicksand', sans-serif;
    font-size: 16px;
    font-weight: bold;
    line-height: 26px;
    text-align: center;
    margin: 0px;
    span {
      font-weight: 300;
      margin-right: 4px;
    }
  }
`;

const ModalTitle = styled.div`
  margin-bottom: 30px;
  font-family: 'Quicksand', sans-serif;
  font-size: 20px;
  font-weight: 400;
  line-height: 28px;
  color: #30333b;
  @media (max-width: 500px) {
    font-size: 18px;
  }
  &.small {
    font-size: 16px;
    line-height: 18px;
  }
  &.creatAccount {
    color: #30333b;
    font-size: 20px;
    // font-weight: 400;
    line-height: 23px;
  }
  &.small {
    @media (max-width: 500px) {
      font-size: 14px !important;
      line-height: 20px !important;
    }
  }
  &.loginQues {
    font-family: 'Roboto', sans-serif;
    font-weight: 400;
    text-align: center;

    span {
      margin: 0px 5px;
    }
    &__title {
      margin-bottom: 20px;
      font-family: 'Roboto', sans-serif;
      font-weight: 400;
      text-align: center;
      font-size: 20px;
      line-height: 21px;
      @media (max-width: 500px) {
        font-size: 18px;
      }
    }
  }
`;

const ModalLoginImage = styled.div`
  width: 104px;
  height: 104px;
  margin: 30px auto 30px;
  min-height: 104px;
  @media (max-width: 500px) {
    margin: 0px auto 30px;
  }
  img {
    width: 100%;
    height: 100%;
  }
`;
const SearchOption = styled(Flex)`
  position: absolute;
  top: 60px;
  width: 100%;
  max-width: 442px;
  padding: 20px 20px 40px;
  border-radius: 5px;
  background: ${colors.white};
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.3);
  z-index: 1000000;
  .google {
    position: absolute;
    right: 20px;
    bottom: 0px;
    // @media (max-width: 500px) {
    //   right: 100px;
    // }
  }
  li {
    font-size: 14px;
    line-height: 30px;
    border-bottom: 1px solid rgba(0, 0, 0, 0.1);
    list-style-type: none;
    padding: 5px 0px;
    color: #666c78;
    cursor: pointer;
    &:last-child {
      border-bottom: 0px solid rgba(0, 0, 0, 0.1);
    }
    &:hover {
      color: #000;
    }
  }
`;
const TermsLink = styled.a`
  margin-left: 5px;
  color: ${props => props.color || '#613A95'};
  font-family: 'Quicksand', sans-serif;
  font-weight: 400;
  font-size: 14px;
  line-height: 19px;
  text-decoration: none;
  cursor: pointer;
  width: 33px;
  &:hover,
  &.active {
    font-weight: 600;
  }
`;
const ChildAgeImg = styled.div`
  height: 84px;
  width: 40px;
  margin-bottom: 10px;
  opacity: 0.4;
  cursor: pointer;
  @media (max-width: 500px) {
    height: 60px;
  }
  img {
    @media (max-width: 500px) {
      width: 100%;
    }
  }
`;

const ChildAge = styled(Flex)`
  margin-right: 20px;

  width: 12%;
  &:last-child {
    margin-right: 0px;
  }
  &:hover,
  &.active {
    .childAge {
      display: block;
    }
    ${ChildAgeImg} {
      opacity: 1;
    }
  }
  .childAge {
    display: none;
    @media (max-width: 500px) {
      padding: 3px;
      font-size: 12px;
    }
  }
`;

const Indicator = styled.div`
  width: 85px;
  display: flex;
  align-items: center;

  span {
    height: 10px;
    min-width: 10px;
    margin-right: 5px;
    border-radius: 50px;
    background-color: #d6dae2;
    &.active {
      background-color: ${colors.secondary};
    }
  }
`;

const CareType = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 46px;
  width: 100%;
  border-radius: 5px;
  background-color: #f3f5f9;
  border: 1px solid #f3f5f9;
  color: #4b5256;
  font-family: 'Roboto', sans-serif;
  font-size: 16px;
  line-height: 21px;
  text-align: center;
  margin-bottom: 10px;
  cursor: pointer;
  position: relative;
  @media (max-width: 500px) {
    height: 40px;
    font-size: 14px;
  }
  &:hover,
  &.active {
    color: ${colors.secondary};
    border-color: ${colors.secondary};
    .iconify {
      display: block;
    }
  }
  .iconify {
    display: none;
    position: absolute;
    top: 15px;
    right: 15px;
    color: ${colors.secondary};
  }
`;
const DestinationError = styled.span`
  margin: 10px 0px 0px 0px;
  color: red;
  font-size: 12px;
  line-height: 14px;
  font-family: 'Quicksand', sans-serif;
`;
const SummaryReport = styled.div`
  padding: 20px;
  width: 100%;
  border-radius: 10px;
  background-color: #f3f5f9;
  margin-bottom: 20px;
  li {
    list-style-type: none;
    color: #666c78;
    font-family: 'Roboto', sans-serif;
    font-size: 14px;
    font-weight: 400;
    line-height: 20px;
    span {
      font-weight: 500;
    }
  }
`;

const ErrorMsg = styled.div`
  font-size: 12px;
  color: red;
  margin-left: 35px;
  @media (max-width: 500px) {
    margin-left: 18px;
  }
`;

const ModalSubInfo = styled.div`
  font-family: Roboto;
  font-size: 18px;
  letter-spacing: 0;
  line-height: 26px;
  text-align: center;
  opacity: 0.5;
  @media (max-width: 500px) {
    font-size: 14px;
    line-height: 20px;
    margin-bottom: 30px;
  }
`;

const CapitalizedSpan = styled.span`
  text-transform: capitalize;
`;
