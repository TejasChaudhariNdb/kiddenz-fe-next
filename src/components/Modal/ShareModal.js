/* eslint-disable indent */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable react/prop-types */
import React from 'react';
import styled from 'styled-components';
// import PropTypes from 'prop-types';
import colors from 'utils/colors';
import {
  EmailShareButton,
  EmailIcon,
  FacebookShareButton,
  FacebookIcon,
  TwitterShareButton,
  TwitterIcon,
  TelegramShareButton,
  TelegramIcon,
  WhatsappShareButton,
  WhatsappIcon,
  LinkedinShareButton,
  LinkedinIcon,
  PinterestShareButton,
  PinterestIcon,
  RedditShareButton,
  RedditIcon,
  InstapaperShareButton,
  InstapaperIcon,
  LineShareButton,
  LineIcon,
  TumblrShareButton,
  TumblrIcon,
  ViberShareButton,
  ViberIcon,
} from 'react-share';
import Flex from '../common/flex';

const ShareModal = props => {
  const { closeHandler, shareUrl } = props;

  return (
    <AdditionalFilterWrapper className="tour">
      <CloseModal onClick={closeHandler}>
        {' '}
        <span
          className="iconify"
          data-icon="bytesize:close"
          data-inline="false"
        />
      </CloseModal>
      <Flex column flexHeight="fit-content">
        <ModalTitle>Share this program with your friends</ModalTitle>
        <ModalLoginImage>
          <EmailShareButton url={shareUrl} body="body">
            <EmailIcon size={32} round />
          </EmailShareButton>
          <FacebookShareButton url={shareUrl}>
            <FacebookIcon size={32} round />
          </FacebookShareButton>
          <TwitterShareButton url={shareUrl}>
            <TwitterIcon size={32} round />
          </TwitterShareButton>
          <InstapaperShareButton url={shareUrl}>
            <InstapaperIcon size={32} round />
          </InstapaperShareButton>
          <TelegramShareButton url={shareUrl}>
            <TelegramIcon size={32} round />
          </TelegramShareButton>
          <WhatsappShareButton url={shareUrl} separator=":: ">
            <WhatsappIcon size={32} round />
          </WhatsappShareButton>
          <LinkedinShareButton url={shareUrl}>
            <LinkedinIcon size={32} round />
          </LinkedinShareButton>
          <PinterestShareButton url={String(window.location)}>
            <PinterestIcon size={32} round />
          </PinterestShareButton>
          <RedditShareButton
            url={shareUrl}
            windowWidth={660}
            windowHeight={460}
          >
            <RedditIcon size={32} round />
          </RedditShareButton>
          <LineShareButton url={shareUrl}>
            <LineIcon size={32} round />
          </LineShareButton>
          <TumblrShareButton url={shareUrl}>
            <TumblrIcon size={32} round />
          </TumblrShareButton>
          <ViberShareButton url={shareUrl}>
            <ViberIcon size={32} round />
          </ViberShareButton>
        </ModalLoginImage>
      </Flex>
    </AdditionalFilterWrapper>
  );
};
ShareModal.propTypes = {};
export default ShareModal;

export const Mainheading = styled.div``;

const AdditionalFilterWrapper = styled.div`
  width: 710px;
  height: ${props => props.height};
  margin: 5% auto;
  padding: 25px;
  padding-left: 45px;
  border-radius: 10px;
  background: ${colors.white};
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
  position: relative;

  &.tour {
    max-width: 540px;
    width: 100%;
    padding: 38px 48px;
    margin: 5% auto 20px;
    @media screen and (max-width: 768px) {
      max-width: 290px;
      width: 100%;
      padding: 20px;
      margin: 20% auto;
    }
    @media (max-width: 500px) {
      max-width: 330px;
      padding: 38px 16px;
    }
  }
`;

const ModalTitle = styled.div`
  margin-bottom: 30px;
  font-family: 'Quicksand', sans-serif;
  font-size: 20px;
  font-weight: 400;
  line-height: 28px;
  color: #30333b;
`;

const ModalLoginImage = styled.div`
  margin: 30px 0px;
  justify-content: flex-start;
  display: flex;
  align-items: flex-start;
  flex-wrap: wrap;
  @media (max-width: 500px) {
    margin: 0 0px;
  }
  button {
    margin: 0 5px 5px 0;
  }
`;

const CloseModal = styled.div`
  position: absolute;
  top: 20px;
  right: 20px;
  cursor: pointer;
  @media screen and (max-width: 768px) {
    top: 1px;
    right: 7px;
  }
  .iconify {
    height: 12px;
    width: 12px;
    color: #666c78;
  }
`;
