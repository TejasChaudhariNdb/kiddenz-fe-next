// import React, { useRef } from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
// import CheckBox from 'components/Checkbox';
// import Button from '../Button';
// import colors from '../../utils/colors';
// import InputBox from '../InputBox';
// import Flex from '../common/flex';
// // import CheckBox from '../Checkbox';
// import welcomeImage from '../../images/mascot1.png';
// import { useLoginHook } from '../../shared/hooks';

// const WelcomeModalCopy = () => (
//   //   closeHandler,
//   //   setModalType,
//   //   isEdit,
//   //   setIsMobileEdit,
//   //   setActiveAuthType,
//   //   authActionSelected = 'login',
//   //   ...props

//   //   const currentRef = useRef();

//   //   const onError = ({ message }) => {
//   //     props.errorToast(message);
//   //   };

//   //   const onSuccess = () => {
//   //     props.successToast('OTP sent successfully.');
//   //     setModalType('verifyMobile');
//   //   };

//   //   const {
//   //     mobile: { value, onChange, onBlur, error },
//   //     onSubmit,
//   //     loginLoader,
//   //   } = useLoginHook(props, { onError, onSuccess, isEdit });

//   //   const onEnterButtonPressed = e => {
//   //     if (e.key === 'Enter') {
//   //       e.preventDefault();
//   //       setActiveAuthType();
//   //       onSubmit();
//   //     }
//   //   };

//   <>
//     <AdditionalFilterWrapper className="welcome">
//       {/* {!props.disableLoginModalCloseIcon && ( */}
//       <CloseModal onClick={closeHandler}>
//         {' '}
//         <span
//           className="iconify"
//           data-icon="bytesize:close"
//           data-inline="false"
//         />
//       </CloseModal>
//       {/* )} */}
//       <ModalRight>
//         <img src={welcomeImage} alt="" />
//       </ModalRight>
//       <ModalLeft>
//         <ModalTitle className="creatAccount">Welcome to Kiddenz!</ModalTitle>
//         <ModalSubTitle>
//           Let's get to know you in order to suggest best schools to you
//         </ModalSubTitle>
//         <div className="scheduleTour">
//           <InputBox
//             placeholder="Your Name"
//             type="text"
//             margin="0px 0px 20px 0px"
//           />
//           <InputBox
//             name="mobile"
//             placeholder="0000000000"
//             type="tel"
//             margin="0px 0px 20px 0px"
//           />
//           <Flex flexMargin="0px 0px 20px 0px">
//             {/* <SpanText>
//               By signing {authActionSelected === 'login' ? 'in' : 'up'} you
//               agree to
//             </SpanText>
//             <TermsLink>Terms of use</TermsLink>
//             <SpanText> and </SpanText>
//             <TermsLink> Policy</TermsLink> */}
//             <CheckBox label="I agree to the Terms of use and Privacy Policy" />
//           </Flex>

//           <Button
//             text="Send OTP"
//             isLoading={loginLoader}
//             type="mobile"
//             //   onClick={() => {
//             //     setActiveAuthType();
//             //     onSubmit();
//             //   }}
//           />
//         </div>
//       </ModalLeft>
//     </AdditionalFilterWrapper>
//   </>
// );

// // WelcomeModalCopy.propTypes = {
// //   authActionSelected: PropTypes.string,
// //   closeHandler: PropTypes.func,
// //   setModalType: PropTypes.func,
// //   setIsMobileEdit: PropTypes.func,
// //   isEdit: PropTypes.bool,
// //   disableLoginModalCloseIcon: PropTypes.bool,
// //   router: PropTypes.object,
// //   successToast: PropTypes.func,
// //   errorToast: PropTypes.func,
// //   setActiveAuthType: PropTypes.func,
// // };

// export default WelcomeModalCopy;

// const AdditionalFilterWrapper = styled.div`
//   width: 710px;
//   height: ${props => props.height};
//   margin: 5% auto;
//   padding: 25px;
//   padding-left: 45px;
//   border-radius: 10px;
//   background: ${colors.white};
//   box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
//   position: relative;

//   &.welcome {
//     max-width: 828px;
//     width: 100%;
//     padding: 50px 48px;
//     margin: 15% auto 20px;
//     display: flex;
//     .scheduleTour {
//       display: flex;
//       flex-direction: column;
//       .personalDetail {
//         color: #30333b;
//         font-family: 'Roboto', sans-serif;
//         font-size: 16px;
//         line-height: 22px;
//         margin-bottom: 20px;
//       }
//     }
//   }
// `;

// export const GalleryHeading = styled.div`
//   margin: ${props => props.margin || '0px'};
//   color: ${props => props.color || '#30333B'};
//   font-family: 'Quicksand', sans-serif;
//   font-size: 22px;
//   font-weight: 500;
//   line-height: 28px;
// `;

// export const ScheduleDetail = styled.div`
//   display: flex;
//   flex-direction: column;
//   align-items: center;
//   border-radius: 10px;
//   background-color: #f3f5f9;
//   padding: 18px 24px;
//   margin-bottom: 10px;
//   h2 {
//     color: #30333b;
//     font-family: 'Quicksand', sans-serif;
//     font-size: 16px;
//     font-weight: bold;
//     line-height: 26px;
//     text-align: center;
//     margin: 0px;
//     span {
//       font-weight: 300;
//       margin-right: 4px;
//     }
//   }
// `;

// const ModalTitle = styled.div`
//   margin-bottom: 10px;
//   font-family: 'Quicksand', sans-serif;
//   font-size: 22px;
//   font-weight: 600;
//   line-height: 28px;

//   &.creatAccount {
//     color: #30333b;
//     font-size: 20px;
//     // font-weight: 400;
//     line-height: 23px;
//   }
//   &.loginQues {
//     font-family: 'Roboto', sans-serif;
//     font-weight: 400;
//     text-align: center;
//     span {
//       margin: 0px 5px;
//     }
//     &__title {
//       margin-bottom: 20px;
//       font-family: 'Roboto', sans-serif;
//       font-weight: 400;
//       text-align: center;
//       font-size: 16px;
//       line-height: 21px;
//     }
//   }
// `;

// const TermsLink = styled.a`
//   margin-left: 5px;
//   color: ${props => props.color || '#613A95'};
//   font-family: 'Quicksand', sans-serif;
//   font-weight: 400;
//   font-size: 14px;
//   line-height: 19px;
//   text-decoration: none;
//   cursor: pointer;
// `;
// const SpanText = styled.span`
//   margin-left: 5px;
//   color: #666c78;
//   font-family: 'Roboto', sans-serif;
//   font-size: 14px;
//   line-height: 19px;
// `;
// const ModalSubTitle = styled.div`
//   margin-left: 5px;
//   margin-bottom: 20px;
//   color: #30333b;
//   font-family: 'Roboto', sans-serif;
//   font-size: 14px;
//   line-height: 19px;
// `;
// const CloseModal = styled.div`
//   position: absolute;
//   top: 20px;
//   right: 20px;
//   cursor: pointer;
//   .iconify {
//     height: 12px;
//     width: 12px;
//     color: #666c78;
//   }
// `;

// const ModalRight = styled.div``;

// const ModalLeft = styled.div``;
