/* eslint-disable no-nested-ternary */
/* eslint-disable no-constant-condition */
import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Button from '../Button';
import { Radio } from '../RadioButton/index';
import colors from '../../utils/colors';
import thinker from '../../images/thinker.svg';

const RestrictionModal = ({ closeHandler, clickType }) => (
  <>
    <div style={{ width: '78%' }}>
      <img
        // src={profilePlaceholder}
        width="100%"
        style={{ filter: `blur(4px)` }}
        alt=""
      />
    </div>
    {/* <div style={{ ...signInPrompt }}> */}
    <AdditionalFilterWrapper className="addChild">
      <ModalLeft>
        <ModalTitle className="creatAccount">
          Oops! search credits exhausted.
        </ModalTitle>
        <ModalSubTitle>
          You have exhausted your {clickType || 'search'} credits. Our team will
          get in touch with you to serve you better.
        </ModalSubTitle>
        <div className="scheduleTour">
          <Button text="Got it!" type="mobile" onClick={() => closeHandler()} />
        </div>
      </ModalLeft>
      <ModalRight>
        <img src={thinker} alt="" height={215} />
      </ModalRight>
    </AdditionalFilterWrapper>
    {/* </div> */}
  </>
);

RestrictionModal.propTypes = {
  closeHandler: PropTypes.func,
  clickType: PropTypes.string,
};

// RestrictionModal.propTypes = {
//   closeHandler: PropTypes.func,
//   setModalType: PropTypes.func,
//   successToast: PropTypes.func,
//   errorToast: PropTypes.func,
//   isChildEdit: PropTypes.bool,
//   editChildId: PropTypes.number,
//   router: PropTypes.object,
// };

export default RestrictionModal;

const AdditionalFilterWrapper = styled.div`
  width: 710px;
  height: ${props => props.height};
  margin: 15% auto;
  padding: 45px;
  border-radius: 10px;
  background: ${colors.white};
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
  position: relative;
  &.addChild {
    width: 800px;

    @media (max-width: 500px) {
      max-width: 340px;
      width: 100%;
      padding: 20px;
    }
    @media (max-width: 380px) {
      max-width: 320px;
    }
    @media (max-width: 330px) {
      max-width: 300px;
    }
    .mobile {
      @media (max-width: 500px) {
        width: 100%;
      }
    }
  }
  .addChild-input {
    @media (max-width: 500px) {
      margin-bottom: 0px;
      width: 100%;
    }

    p {
      color: #666c78;
      font-family: 'Roboto', sans-serif;
      font-size: 14px;
      line-height: 16px;
      margin: 0px 0px 10px;
    }
    .tel {
      @media (max-width: 500px) {
        padding: 12px 12px 12px 40px !important;
      }
    }
    .number {
      @media (max-width: 500px) {
        top: 12px;
        font-size: 13px;
      }
    }
    input {
      @media (max-width: 500px) {
        padding: 12px;
      }
    }
  }
  .addChild-check {
    .checkbox {
      width: 20%;
      @media (max-width: 500px) {
        width: 50%;
      }
    }
    ${Radio} {
      @media (max-width: 500px) {
        width: 100%;
      }
      label {
        @media (max-width: 500px) {
          //   white-space: normal !important;
        }
      }
    }
    input[type='radio']:not(:checked) + label,
    input[type='radio']:checked + label {
      color: #666c78;
      font-family: 'Roboto', sans-serif;
      font-size: 14px;
      line-height: 25px;
      font-weight: 400;
    }
  }
  .childrenDetails {
    @media (max-width: 500px) {
      margin: 0px;
    }
  }
  .btnWrapper {
    @media (max-width: 500px) {
      .mobile {
        padding: 10px 12px;
        height: 40px;
        font-size: 14px;
      }
      .subscribe {
        padding: 10px 12px;
        height: 40px;
        font-size: 14px;
      }
    }
  }
`;

export const GalleryHeading = styled.div`
  margin: ${props => props.margin || '0px'};
  color: ${props => props.color || '#30333B'};
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 500;
  line-height: 28px;
  @media (max-width: 500px) {
    font-size: 18px;
    line-height: 24px;
  }
`;

const ModalSubTitle = styled.div`
  margin-left: 5px;
  margin-bottom: 20px;
  color: #30333b;
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 19px;
  @media (max-width: 500px) {
    text-align: center;
  }
`;

const ModalLeft = styled.div`
  margin-left: auto;
  max-width: 66%;
  .alreadySigned {
    color: #666c78;
    font-family: Roboto;
    font-size: 14px;
    letter-spacing: 0;
    line-height: 14px;
    font-weight: 400;
    text-align: center;
    @media (max-width: 500px) {
      font-size: 12px;
    }
    span {
      color: #613a95;
      margin-left: 5px;
      cursor: pointer;
    }
  }
  @media (max-width: 500px) {
    margin-left: 0px;
    max-width: 100%;
  }
`;

const ModalTitle = styled.div`
  margin-bottom: 10px;
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 600;
  line-height: 28px;

  &.creatAccount {
    color: #30333b;
    font-size: 20px;
    // font-weight: 400;
    line-height: 23px;
    @media (max-width: 500px) {
      font-size: 18px;
      text-align: center;
    }
  }
  &.loginQues {
    font-family: 'Roboto', sans-serif;
    font-weight: 400;
    text-align: center;
    span {
      margin: 0px 5px;
    }
    &__title {
      margin-bottom: 20px;
      font-family: 'Roboto', sans-serif;
      font-weight: 400;
      text-align: center;
      font-size: 16px;
      line-height: 21px;
    }
  }
`;

const ModalRight = styled.div`
  position: absolute;
  left: 0px;
  bottom: 0px;
  img {
    @media (max-width: 500px) {
      margin-bottom: -52px;
      margin-top: -28px;
      margin-left: -31px;
      transform: scale(0.7);
    }
  }
  @media (max-width: 500px) {
    position: relative;
  }
`;
