/* eslint-disable react/prop-types */
/* eslint-disable indent */
/* eslint-disable no-nested-ternary */
/* eslint-disable no-constant-condition */
import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Flex from 'components/common/flex';
import getAgeText from 'shared/utils/getAgeText';
import Button from '../Button';
import colors from '../../utils/colors';
import Ruppee from '../common/Ruppee';

const CourseTypeConfig = [
  {
    label: 'Full Course',
    key: 'Full_Course',
    costKey: 'full_course_cost',
    dicountKey: 'full_course_discount',
  },
  {
    label: 'Single Session',
    key: 'Single_Session',
    costKey: 'single_session_cost',
    dicountKey: 'single_session_discount',
  },
  {
    label: 'Monthly',
    key: 'Monthly',
    costKey: 'monthly_cost',
    dicountKey: 'monthly_discount',
  },
  {
    label: 'Trial Class',
    key: 'Trial_Class',
    costKey: 'trial_cost',
    dicountKey: 'trial_discount',
  },
];

const OnlineProgramModal = ({
  closeHandler,
  onlineTag,
  // clickType,
  programDetail = {},
  courseType = '',
  selectedBatch = {},
  onBuyNow,
  isLoading,
  noOfSession,
}) => {
  const currentRef = useRef();
  return (
    <>
      <AdditionalFilterWrapper className="tour" ref={currentRef}>
        <CloseModal onClick={closeHandler}>
          {' '}
          <span
            className="iconify"
            data-icon="bytesize:close"
            data-inline="false"
          />
        </CloseModal>

        <BuyNow>
          {programDetail.program_detail &&
            programDetail.program_detail.start_age && (
              <li>
                <h2>
                  Age:
                  <span>
                    {getAgeText(
                      programDetail.program_detail &&
                        programDetail.program_detail.start_age,
                      programDetail.program_detail &&
                        programDetail.program_detail.end_age,
                    )}
                  </span>
                </h2>
              </li>
            )}
          {courseType === 'Single_Session' &&
            noOfSession && (
              <li>
                <h2>
                  No of sessions:
                  <span>{noOfSession}</span>
                </h2>
              </li>
            )}

          {CourseTypeConfig.map(course => (
            <>
              {course.key === courseType && (
                <li>
                  <h2>
                    Course type:
                    <span>
                      {onlineTag === 'home_school'
                        ? 'Home School'
                        : course.label}
                    </span>
                  </h2>
                </li>
              )}
            </>
          ))}
          {courseType !== 'Trial_Class' &&
            courseType !== 'Single_Session' &&
            selectedBatch &&
            selectedBatch.heading_name && (
              <>
                <li>
                  <h2>
                    Selected Batch:
                    <span>{selectedBatch.heading_name}</span>
                  </h2>
                </li>
                {selectedBatch.duration && (
                  <li>
                    <h2>
                      Duration:
                      <span>{selectedBatch.duration}</span>
                    </h2>
                  </li>
                )}
                {selectedBatch.no_of_seats && (
                  <li>
                    <h2>
                      No of sessions:
                      <span>{selectedBatch.no_of_seats}</span>
                    </h2>
                  </li>
                )}
              </>
            )}
          {CourseTypeConfig.map(course => (
            <>
              {course.key === courseType && (
                <Flex alignCenter flexMargin="36px 0px 8px">
                  {programDetail.program_cost &&
                  programDetail.program_cost[course.costKey] ? (
                    <>
                      {programDetail.program_cost &&
                      programDetail.program_cost[course.dicountKey] ? (
                        <h3>
                          <Ruppee>₹</Ruppee>
                          {(programDetail.program_cost[course.costKey] -
                            programDetail.program_cost[course.dicountKey]) *
                            (+noOfSession || 1)}
                        </h3>
                      ) : (
                        <h3>
                          <Ruppee>₹</Ruppee>
                          {programDetail.program_cost[course.costKey] *
                            (+noOfSession || 1)}
                        </h3>
                      )}
                    </>
                  ) : null}
                  {programDetail.program_cost &&
                  programDetail.program_cost[course.dicountKey] ? (
                    <h6>
                      <span>
                        <Ruppee>₹</Ruppee>
                        {programDetail.program_cost[course.costKey]}
                      </span>
                      <Ruppee>₹</Ruppee>
                      {programDetail.program_cost[course.dicountKey]} Off
                    </h6>
                  ) : null}
                </Flex>
              )}
            </>
          ))}
          {programDetail.program_cost &&
            programDetail.program_cost.money_back_gurantee && (
              <p>Money back Guarantee</p>
            )}
          <Button
            onClick={onBuyNow}
            text={courseType !== 'Trial_Class' ? 'Buy Now' : 'Book Now'}
            isLoading={isLoading}
            type="profile"
          />
        </BuyNow>
      </AdditionalFilterWrapper>
    </>
  );
};

OnlineProgramModal.propTypes = {
  closeHandler: PropTypes.func,
  // clickType: PropTypes.string,
};

// RestrictionModal.propTypes = {
//   closeHandler: PropTypes.func,
//   setModalType: PropTypes.func,
//   successToast: PropTypes.func,
//   errorToast: PropTypes.func,
//   isChildEdit: PropTypes.bool,
//   editChildId: PropTypes.number,
//   router: PropTypes.object,
// };

export default OnlineProgramModal;

const CloseModal = styled.div`
  position: absolute;
  top: 25px;
  right: 25px;
  cursor: pointer;
  @media screen and (max-width: 768px) {
    top: 1px;
    right: 7px;
  }
  .iconify {
    height: 12px;
    width: 12px;
    color: #666c78;
  }
`;

const AdditionalFilterWrapper = styled.div`
  width: 710px;
  height: ${props => props.height};
  margin: 5% auto;
  padding: 25px;
  padding-left: 45px;
  border-radius: 10px;
  background: ${colors.white};
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
  position: relative;

  &.tour {
    max-width: 500px;
    width: 100%;
    padding: 50px 48px;
    margin: auto;
    @media screen and (max-width: 768px) {
      max-width: 290px;
      width: 100%;
      padding: 20px;
      margin: 20% auto;
    }
    .scheduleTour {
      display: flex;
      flex-direction: column;
      .personalDetail {
        color: #30333b;
        font-family: 'Roboto', sans-serif;
        font-size: 16px;
        line-height: 22px;
        margin-bottom: 20px;
      }
    }
  }
`;

const BuyNow = styled.ul`
  width: 100%;
  padding: 0px 10px 20px;
  border: 1px solid #bfc8ce;
  border-radius: 10px;
  margin: 0;
  li {
    padding: 20px 0px;
    border-bottom: 1px solid #ddd9e1;
    list-style-type: none;
    @media screen and (max-width: 768px) {
      padding: 10px 0px;
    }
    h2 {
      display: flex;
      align-items: center;
      justify-content: space-between;
      font-family: Quicksand;
      font-weight: bold;
      font-size: 18px;
      line-height: 22px;
      color: #4b5256;
      margin: 0px;
      @media screen and (max-width: 768px) {
        font-size: 13px;
        line-height: 1.2;
      }
      span {
        font-family: 'Roboto', sans-serif;
        font-weight: 400;
        font-size: 18px;
        line-height: 21px;
        text-align: right;
        color: #4b5256;
        @media screen and (max-width: 768px) {
          font-size: 11px;
          line-height: 1.2;
        }
      }
    }
  }
  h3 {
    font-family: Quicksand;
    font-weight: bold;
    font-size: 24px;
    line-height: 30px;
    color: #000000;
    margin: 0px 8px 0px 0px;
    @media screen and (max-width: 768px) {
      font-size: 16px;
      line-height: 1.2;
    }
  }
  h6 {
    margin: 0px;
    font-family: Quicksand;
    font-weight: bold;
    font-size: 14px;
    line-height: 17px;
    color: #000000;
    @media screen and (max-width: 768px) {
      font-size: 10px;
      line-height: 1.2;
    }
    span {
      font-weight: 400;
      text-decoration-line: line-through;
      opacity: 0.5;
      margin-right: 4px;
    }
  }
  p {
    font-family: Quicksand;
    font-weight: 400;
    font-size: 16px;
    line-height: 20px;
    color: #000000;
    margin: 0px 0px 18px;
    @media screen and (max-width: 768px) {
      font-size: 12px;
      line-height: 1.2;
      color: #000000;
      margin: 0px 0px 10px;
    }
  }
  button {
    @media screen and (max-width: 768px) {
      font-size: 14px;
      height: auto;
      width: auto;
      padding: 10px 20px;
    }
  }
`;

// const ModalSubTitle = styled.div`
//   margin-left: 5px;
//   margin-bottom: 20px;
//   color: #30333b;
//   font-family: 'Roboto', sans-serif;
//   font-size: 14px;
//   line-height: 19px;
//   @media (max-width: 500px) {
//     text-align: center;
//   }
// `;

// const ModalLeft = styled.div`
//   margin-left: auto;
//   max-width: 66%;
//   .alreadySigned {
//     color: #666c78;
//     font-family: Roboto;
//     font-size: 14px;
//     letter-spacing: 0;
//     line-height: 14px;
//     font-weight: 400;
//     text-align: center;
//     @media (max-width: 500px) {
//       font-size: 12px;
//     }
//     span {
//       color: #613a95;
//       margin-left: 5px;
//       cursor: pointer;
//     }
//   }
//   @media (max-width: 500px) {
//     margin-left: 0px;
//     max-width: 100%;
//   }
// `;

// const ModalTitle = styled.div`
//   margin-bottom: 10px;
//   font-family: 'Quicksand', sans-serif;
//   font-size: 22px;
//   font-weight: 600;
//   line-height: 28px;

//   &.creatAccount {
//     color: #30333b;
//     font-size: 20px;
//     // font-weight: 400;
//     line-height: 23px;
//     @media (max-width: 500px) {
//       font-size: 18px;
//       text-align: center;
//     }
//   }
//   &.loginQues {
//     font-family: 'Roboto', sans-serif;
//     font-weight: 400;
//     text-align: center;
//     span {
//       margin: 0px 5px;
//     }
//     &__title {
//       margin-bottom: 20px;
//       font-family: 'Roboto', sans-serif;
//       font-weight: 400;
//       text-align: center;
//       font-size: 16px;
//       line-height: 21px;
//     }
//   }
// `;

// const ModalRight = styled.div`
//   position: absolute;
//   left: 0px;
//   bottom: 0px;
//   img {
//     @media (max-width: 500px) {
//       margin-bottom: -52px;
//       margin-top: -28px;
//       margin-left: -31px;
//       transform: scale(0.7);
//     }
//   }
//   @media (max-width: 500px) {
//     position: relative;
//   }
// `;
