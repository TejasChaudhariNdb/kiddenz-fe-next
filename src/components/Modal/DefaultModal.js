/* eslint-disable camelcase */
/* eslint-disable indent */
/* eslint-disable no-lonely-if */
/* eslint-disable eqeqeq */
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import cloneDeep from 'lodash/cloneDeep';
import styled from 'styled-components';
import InputRange from 'react-input-range';
import Button from '../Button';
import Flex from '../common/flex';
import colors from '../../utils/colors';
// import Badge from '../Badge';
import RadioButton from '../RadioButton';
import CheckBox from '../Checkbox';

const DefaultModal = ({
  modalType = 'primary',
  closeHandler,
  onApplyFilter,
  sentFilters,
  resetFilter,
  queryChange,
  searchMethod,
  blockSearch,
  setFilterSectionActive,
}) => {
  const [appliedFilters, setAppliedFilters] = useState(FILTERS_INITIAL_STATE);
  const [extendedHoursSliderValues, setExtendedHoursSliderValues] = useState({
    min: 0,
    max: 24,
  });
  const [daycarePriceSliderValues, setDaycarePriceSliderValues] = useState({
    min: 500,
    max: 20000,
  });

  const filtersOnChange = (key, value, actionType) => {
    const tempFilters = cloneDeep(appliedFilters);

    // // make live streaming null when cctv options there
    // if (key === 'cctv' && (value == 0 || value == 1))
    //   tempFilters.live_streaming = null;

    // // make cctv null when live fees options there
    // if (key === 'live_streaming' && (value == 0 || value == 1))
    //   tempFilters.cctv = null;

    if (key !== 'schedule_type') {
      tempFilters[key] = value;
    } else {
      tempFilters.schedule_type = tempFilters.schedule_type
        ? tempFilters.schedule_type
        : [];
      const scheduleList = tempFilters.schedule_type;
      if (actionType === 'add') scheduleList.push(value);
      else {
        const index = tempFilters.schedule_type.indexOf(value);
        if (index > -1) {
          tempFilters.schedule_type.splice(index, 1);
        }
      }
    }
    if (key === 'extended_hours' && !value) {
      tempFilters.max_extended_hours = null;
      tempFilters.min_extended_hours = null;
    }
    setAppliedFilters(tempFilters);
  };

  useEffect(
    () => {
      const filtersApplied = {};
      Object.keys(FILTERS_INITIAL_STATE).map(d => {
        filtersApplied[d] = sentFilters[d] || null;
        return 0;
      });
      setAppliedFilters({
        ...FILTERS_INITIAL_STATE,
        ...filtersApplied,
      });
      setExtendedHoursSliderValues({
        min: sentFilters.min_extended_hours
          ? sentFilters.min_extended_hours
          : 0,
        max: sentFilters.max_extended_hours
          ? sentFilters.max_extended_hours
          : 24,
      });
      setDaycarePriceSliderValues({
        min: sentFilters.daycare_min_price
          ? sentFilters.daycare_min_price
          : 500,
        max: sentFilters.daycare_max_price
          ? sentFilters.daycare_max_price
          : 20000,
      });
    },
    [sentFilters],
  );

  const onSliderChange = val => {
    const temp = cloneDeep(appliedFilters);
    setAppliedFilters({ ...temp, ...val });
  };

  return (
    <AdditionalFilterWrapper className="default">
      <Flex justifyEnd>
        {/* <Button onClick={closeHandler} text="X" type="links" /> */}
        <CloseModal onClick={closeHandler}>
          {' '}
          <span
            className="iconify"
            data-icon="bytesize:close"
            data-inline="false"
          />
        </CloseModal>
      </Flex>
      <ModalTitle>Additional Filters</ModalTitle>
      {modalType === 'default' && (
        <>
          <Flex
            whitebg="#F3F5F9"
            flexPadding="20px"
            flexBorderRadius="10px"
            flexMargin="0px 0px 25px 0px"
          >
            <Flex column flexMargin="0px 60px 0px 0px">
              <TypeOrg>Special Service</TypeOrg>
              <OrgName>Weekend childcare</OrgName>
            </Flex>
            <RadioButton
              id="weekend_yes"
              text="yes"
              filterRadio="#4B5256"
              filterFont="13px"
              filterFontWeight="400"
              filterLineheight="21px"
              filterPadding="0px 0px 0px 30px"
              RadioMargin="0px 65px 0px 0px"
              onClick={() => filtersOnChange('schedule_type', 3, 'add')}
              checked={appliedFilters.schedule_type.includes(3)}
            />
            <RadioButton
              id="weekend_no"
              text="No"
              filterRadio="#4B5256"
              filterFont="13px"
              filterFontWeight="400"
              filterLineheight="21px"
              filterPadding="0px 0px 0px 30px"
              onClick={() => filtersOnChange('schedule_type', 3, 'remove')}
              checked={!appliedFilters.schedule_type.includes(3)}
            />
          </Flex>
          <Flex flexMargin="0px 0px 30px 0px" alignCenter>
            <Title>Schedule Type</Title>

            <CheckBox
              label="Full Time"
              id="schedule_full_time"
              margin="0px 30px 0px 0px"
              filterFont="13px"
              filterChekbox="5px"
              filterLineheight="18px"
              onClickFunction={e => {
                e.preventDefault();
                if (appliedFilters.schedule_type.includes(1))
                  filtersOnChange('schedule_type', 1, 'remove');
                else filtersOnChange('schedule_type', 1, 'add');
              }}
              checked={appliedFilters.schedule_type.includes(1)}
            />
            <CheckBox
              label="Flexible"
              id="schedule_flexible"
              margin="0px 30px 0px 0px"
              filterFont="13px"
              filterChekbox="5px"
              filterLineheight="18px"
              onClickFunction={e => {
                e.preventDefault();
                if (appliedFilters.schedule_type.includes(2))
                  filtersOnChange('schedule_type', 2, 'remove');
                else filtersOnChange('schedule_type', 2, 'add');
              }}
              checked={appliedFilters.schedule_type.includes(2)}
            />
            <CheckBox
              label="24 Hours"
              id="schedule_all_day"
              margin="0px"
              filterFont="13px"
              filterChekbox="5px"
              filterLineheight="18px"
              onClickFunction={e => {
                e.preventDefault();
                if (appliedFilters.schedule_type.includes(4))
                  filtersOnChange('schedule_type', 4, 'remove');
                else filtersOnChange('schedule_type', 4, 'add');
              }}
              checked={appliedFilters.schedule_type.includes(4)}
            />
          </Flex>
          <Flex flexMargin="0px 0px 30px 0px" alignCenter>
            <Title>CCTV</Title>
            <RadioButton
              id="cctv_yes"
              text="Yes"
              filterRadio="#4B5256"
              filterFont="13px"
              filterFontWeight="400"
              filterLineheight="21px"
              filterPadding="0px 0px 0px 30px"
              RadioMargin="0px 60px 0px 0px"
              onClick={() => {
                filtersOnChange('cctv', 1);
              }}
              checked={appliedFilters.cctv == 1}
            />
            <RadioButton
              id="cctv_no"
              text="No"
              filterRadio="#4B5256"
              filterFont="13px"
              filterFontWeight="400"
              filterLineheight="21px"
              filterPadding="0px 50px 0px 30px"
              RadioMargin="0px"
              onClick={() => {
                filtersOnChange('cctv', 0);
              }}
              checked={appliedFilters.cctv == 0}
            />
            <RadioButton
              id="live_tv"
              text="Live feed available"
              filterRadio="#4B5256"
              filterFont="13px"
              filterFontWeight="400"
              filterLineheight="21px"
              filterPadding="0px 0px 0px 30px"
              RadioMargin="0px 0px 0px 0px"
              onClick={() => {
                filtersOnChange('live_streaming', 1);
              }}
              checked={appliedFilters.live_streaming == 1}
            />
          </Flex>
          {/* TEMP -- W8 for client comments */}
          {/* <Flex flexMargin="0px 0px 30px 0px" alignCenter>
          <Title>Free Trials</Title>
          <RadioButton
            id="CCTV1"
            text="Yes"
            filterRadio="#4B5256"
            filterFont="13px"
            filterFontWeight="400"
            filterLineheight="21px"
            filterPadding="0px 0px 0px 30px"
            RadioMargin="0px 60px 0px 0px"
          />
          <RadioButton
            id="CCTV2"
            text="No"
            filterRadio="#4B5256"
            filterFont="13px"
            filterFontWeight="400"
            filterLineheight="21px"
            filterPadding="0px 0px 0px 30px"
            RadioMargin="0px"
          />
        </Flex>
         */}
          <Flex flexMargin="0px 0px 30px 0px" alignCenter>
            <Title>Outdoor playing area</Title>
            <RadioButton
              id="outdoor_yes"
              text="Yes"
              filterRadio="#4B5256"
              filterFont="13px"
              filterFontWeight="400"
              filterLineheight="21px"
              filterPadding="0px 0px 0px 30px"
              RadioMargin="0px 60px 0px 0px"
              onClick={() => filtersOnChange('outdoor_play', 1)}
              checked={appliedFilters.outdoor_play == 1}
            />
            <RadioButton
              id="outdoor_no"
              text="No"
              filterRadio="#4B5256"
              filterFont="13px"
              filterFontWeight="400"
              filterLineheight="21px"
              filterPadding="0px 0px 0px 30px"
              RadioMargin="0px"
              onClick={() => filtersOnChange('outdoor_play', 0)}
              checked={appliedFilters.outdoor_play == 0}
            />
          </Flex>
          <Flex flexMargin="0px 0px 30px 0px" alignCenter>
            <Title>Parent teacher app</Title>
            <RadioButton
              id="parent_app_yes"
              text="Yes"
              filterRadio="#4B5256"
              filterFont="13px"
              filterFontWeight="400"
              filterLineheight="21px"
              filterPadding="0px 0px 0px 30px"
              RadioMargin="0px 60px 0px 0px"
              onClick={() => filtersOnChange('parent_app', 1)}
              checked={appliedFilters.parent_app == 1}
            />
            <RadioButton
              id="parent_app_no"
              text="No"
              filterRadio="#4B5256"
              filterFont="13px"
              filterFontWeight="400"
              filterLineheight="21px"
              filterPadding="0px 0px 0px 30px"
              RadioMargin="0px"
              onClick={() => filtersOnChange('parent_app', 0)}
              checked={appliedFilters.parent_app == 0}
            />
          </Flex>
          {/* TEMP -- W8 for client comments */}
          {/* <Flex flexMargin="0px 0px 30px 0px" alignCenter>
          <Title>Massagin & bathing experts</Title>
          <RadioButton
            id="special1"
            text="Yes"
            filterRadio="#4B5256"
            filterFont="13px"
            filterFontWeight="400"
            filterLineheight="21px"
            filterPadding="0px 0px 0px 30px"
            RadioMargin="0px 60px 0px 0px"
          />
          <RadioButton
            id="special2"
            text="No"
            filterRadio="#4B5256"
            filterFont="13px"
            filterFontWeight="400"
            filterLineheight="21px"
            filterPadding="0px 0px 0px 30px"
            RadioMargin="0px"
          />
        </Flex>
         */}
          <Flex flexMargin="0px 0px 30px 0px" alignStart>
            <Title>Extended Hours</Title>
            <Flex column flexWidth="40%">
              <Flex>
                <>
                  <RadioButton
                    id="extended_hours_yes"
                    text="Yes"
                    filterRadio="#4B5256"
                    filterFont="13px"
                    filterFontWeight="400"
                    filterLineheight="21px"
                    filterPadding="0px 0px 0px 30px"
                    RadioMargin="0px 60px 0px 0px"
                    onClick={() => filtersOnChange('extended_hours', 1)}
                    checked={appliedFilters.extended_hours == 1}
                  />
                  <RadioButton
                    id="extended_hours_no"
                    text="No"
                    filterRadio="#4B5256"
                    filterFont="13px"
                    filterFontWeight="400"
                    filterLineheight="21px"
                    filterPadding="0px 0px 0px 30px"
                    RadioMargin="0px"
                    onClick={() => filtersOnChange('extended_hours', 0)}
                    checked={appliedFilters.extended_hours == 0}
                  />
                </>
              </Flex>
              {appliedFilters.extended_hours == 1 && (
                <FilterItem
                  className="rangeSlider"
                  width="100%"
                  margin="40px 0px 0px"
                  padding="0px 0px 0px 10px"
                >
                  <InputRange
                    maxValue={24}
                    minValue={0}
                    value={extendedHoursSliderValues}
                    formatLabel={d => `${d} hrs`}
                    onChange={val => setExtendedHoursSliderValues(val)}
                    allowSameValues={false}
                  />
                </FilterItem>
              )}
            </Flex>
          </Flex>
          <Flex flexMargin="0px 0px 30px 0px" alignCenter>
            <Title>Special Care</Title>
            <RadioButton
              id="special_care_yes"
              text="Yes"
              filterRadio="#4B5256"
              filterFont="13px"
              filterFontWeight="400"
              filterLineheight="21px"
              filterPadding="0px 0px 0px 30px"
              RadioMargin="0px 60px 0px 0px"
              onClick={() => filtersOnChange('special_care', 1)}
              checked={appliedFilters.special_care == 1}
            />
            <RadioButton
              id="special_care_no"
              text="No"
              filterRadio="#4B5256"
              filterFont="13px"
              filterFontWeight="400"
              filterLineheight="21px"
              filterPadding="0px 0px 0px 30px"
              RadioMargin="0px"
              onClick={() => filtersOnChange('special_care', 0)}
              checked={appliedFilters.special_care == 0}
            />
          </Flex>
          {/* TEMP -- W8 for client comments */}
          {/* <Flex flexMargin="0px 0px 30px 0px" alignCenter>
          <Title>Activities</Title>
          <RadioButton
            id="Tution1"
            text="Yes"
            filterRadio="#4B5256"
            filterFont="13px"
            filterFontWeight="400"
            filterLineheight="21px"
            filterPadding="0px 0px 0px 30px"
            RadioMargin="0px 60px 0px 0px"
          />
          <RadioButton
            id="Tutio2"
            text="No"
            filterRadio="#4B5256"
            filterFont="13px"
            filterFontWeight="400"
            filterLineheight="21px"
            filterPadding="0px 0px 0px 30px"
            RadioMargin="0px"
          />
        </Flex>
        <Flex flexMargin="0px 0px 30px 0px" alignCenter>
          <Title>Tuition</Title>
          <RadioButton
            id="Tution1"
            text="Yes"
            filterRadio="#4B5256"
            filterFont="13px"
            filterFontWeight="400"
            filterLineheight="21px"
            filterPadding="0px 0px 0px 30px"
            RadioMargin="0px 60px 0px 0px"
          />
          <RadioButton
            id="Tutio2"
            text="No"
            filterRadio="#4B5256"
            filterFont="13px"
            filterFontWeight="400"
            filterLineheight="21px"
            filterPadding="0px 0px 0px 30px"
            RadioMargin="0px"
          />
        </Flex>
         */}
          {/* <Flex flexMargin="0px 0px 30px 0px" alignCenter wrap>
          <Title>Tuition</Title>
          <Badge
            badgeActive
            text="Montessari"
            marginBottom="10px"
            tooltipText="This comprehensive program developed by physician and educator 
  Maria Montessori takes a developmental approach to learning. 
  All teachers must have an early childhood undergraduate or graduate 
  degree and Montessori certification. The Montessori approach 
  emphasizes nature, creativity, and hands-on learning with gentle 
  guidance provided by the teachers. The goal of the Montessori method 
  is to develop a child's senses, character, practical life skills, 
  and academic ability."
          />
          <Badge text="Academic" marginBottom="10px" />
          <Badge text="Play-based" marginBottom="10px" />
          <Badge text="Waldorf" marginBottom="10px" />
          <Badge text="Reggio Emilia" marginLeft="200px" />
          <Badge text="Bank Street" />
          <Badge text="HighScope" />
        </Flex>
         */}
        </>
      )}

      {modalType === 'primary' && (
        <>
          <Flex
            column
            whitebg="#F2EFF5"
            flexPadding="16px 20px"
            flexBorderRadius="10px"
            flexMargin="0px 0px 25px 0px"
            className="specialService"
          >
            {/* <TypeOrg></TypeOrg> */}
            <OrgName>Special Service</OrgName>

            <Flex flexMargin="12px 0px 0px">
              <CheckBox
                label="Weekend Childcare"
                id="schedule_full_time"
                margin="0px 30px 0px 0px"
                filterFont="13px"
                filterChekbox="5px"
                filterLineheight="18px"
                onClickFunction={e => {
                  e.preventDefault();
                  if (
                    appliedFilters &&
                    appliedFilters.schedule_type &&
                    appliedFilters.schedule_type.includes(3)
                  )
                    filtersOnChange('schedule_type', 3, 'remove');
                  else filtersOnChange('schedule_type', 3, 'add');
                }}
                checked={
                  appliedFilters &&
                  appliedFilters.schedule_type &&
                  appliedFilters.schedule_type.includes(3)
                }
              />
              <CheckBox
                label="24/7 Care"
                id="schedule_full_time_2"
                margin="0px 30px 0px 0px"
                filterFont="13px"
                filterChekbox="5px"
                filterLineheight="18px"
                onClickFunction={e => {
                  e.preventDefault();
                  if (
                    appliedFilters &&
                    appliedFilters.schedule_type &&
                    appliedFilters.schedule_type.includes(4)
                  )
                    filtersOnChange('schedule_type', 4, 'remove');
                  else filtersOnChange('schedule_type', 4, 'add');
                }}
                checked={
                  appliedFilters &&
                  appliedFilters.schedule_type &&
                  appliedFilters.schedule_type.includes(4)
                }
              />
            </Flex>
          </Flex>
          <Flex
            column
            flexPadding="0px 20px 0px 20px"
            flexBorderdashed="1px solid #EAEDF2"
            flexMargin="0px 0px 20px 0px"
            className="checklists"
          >
            <Flex
              column
              flexMargin="0px 0px 20px 0px"
              flexPadding="0px 0px 20px"
              flexBorderdashed="1px solid #EAEDF2"
            >
              <Title>Schedule Type</Title>
              <Flex flexMargin="20px 0px 0px 0px" alignCenter>
                <CheckBox
                  label="Full Time"
                  id="schedule_full_time"
                  margin="0px 30px 0px 0px"
                  filterFont="13px"
                  filterChekbox="5px"
                  filterLineheight="18px"
                  onClickFunction={e => {
                    e.preventDefault();
                    if (
                      appliedFilters &&
                      appliedFilters.schedule_type &&
                      appliedFilters.schedule_type.includes(1)
                    )
                      filtersOnChange('schedule_type', 1, 'remove');
                    else filtersOnChange('schedule_type', 1, 'add');
                  }}
                  checked={
                    appliedFilters &&
                    appliedFilters.schedule_type &&
                    appliedFilters.schedule_type.includes(1)
                  }
                />

                <Flex flexMargin="0px 30px 0px 0px" alignCenter>
                  <CheckBox
                    label="Flexible"
                    id="schedule_flexible"
                    margin="0px 10px 0px 0px"
                    filterFont="13px"
                    filterChekbox="5px"
                    filterLineheight="18px"
                    onClickFunction={e => {
                      e.preventDefault();
                      if (
                        appliedFilters &&
                        appliedFilters.schedule_type &&
                        appliedFilters.schedule_type.includes(2)
                      )
                        filtersOnChange('schedule_type', 2, 'remove');
                      else filtersOnChange('schedule_type', 2, 'add');
                    }}
                    checked={
                      appliedFilters &&
                      appliedFilters.schedule_type &&
                      appliedFilters.schedule_type.includes(2)
                    }
                  />
                  <div className="infoIcon">
                    <span
                      className="iconify"
                      data-icon="jam:info"
                      data-inline="false"
                    />
                  </div>
                  <ToolTipInfo>Daycare available at hourly basis.</ToolTipInfo>
                </Flex>
              </Flex>
            </Flex>
            <Flex wrap justifyBetween className="checklists">
              <CheckBox
                width="44%"
                label="CCTV"
                id="schedule_full_time_3"
                margin="0px 30px 25px 0px"
                filterFont="13px"
                filterChekbox="5px"
                filterLineheight="18px"
                onClickFunction={e => {
                  e.preventDefault();
                  if (appliedFilters.cctv == 1) filtersOnChange('cctv', null);
                  else filtersOnChange('cctv', 1);
                }}
                checked={appliedFilters.cctv == 1}
              />
              <Flex
                flexWidth="44%"
                flexMargin="0px 30px 0px 0px"
                alignCenter
                flexHeight="fit-content"
              >
                <CheckBox
                  // width="100%"
                  label="Special Care"
                  id="schedule_full_time_4"
                  margin="0px 10px 0px 0px"
                  filterFont="13px"
                  filterChekbox="5px"
                  filterLineheight="18px"
                  onClickFunction={e => {
                    e.preventDefault();
                    if (appliedFilters.special_care == 1)
                      filtersOnChange('special_care', null);
                    else filtersOnChange('special_care', 1);
                  }}
                  checked={appliedFilters.special_care == 1}
                />
                <div className="infoIcon">
                  <span
                    className="iconify"
                    data-icon="jam:info"
                    data-inline="false"
                  />
                </div>
                <ToolTipInfo>
                  Qualified to care for differently able kids.
                </ToolTipInfo>
              </Flex>
              <CheckBox
                width="44%"
                label="Live Streaming Available "
                id="schedule_full_time_5"
                margin="0px 30px 25px 0px"
                filterFont="13px"
                filterChekbox="5px"
                filterLineheight="18px"
                onClickFunction={e => {
                  e.preventDefault();
                  if (appliedFilters.live_streaming == 1)
                    filtersOnChange('live_streaming', null);
                  else filtersOnChange('live_streaming', 1);
                }}
                checked={appliedFilters.live_streaming == 1}
              />
              <CheckBox
                width="44%"
                label="Parent Connect App"
                id="schedule_full_time_6"
                margin="0px 30px 25px 0px"
                filterFont="13px"
                filterChekbox="5px"
                filterLineheight="18px"
                onClickFunction={e => {
                  e.preventDefault();
                  if (appliedFilters.parent_app == 1)
                    filtersOnChange('parent_app', null);
                  else filtersOnChange('parent_app', 1);
                }}
                checked={appliedFilters.parent_app == 1}
              />
              <CheckBox
                width="44%"
                label="Outdoor Play Area"
                id="schedule_full_time_8"
                margin="0px 30px 25px 0px"
                filterFont="13px"
                filterChekbox="5px"
                filterLineheight="18px"
                onClickFunction={e => {
                  e.preventDefault();
                  if (appliedFilters.outdoor_play == 1)
                    filtersOnChange('outdoor_play', null);
                  else filtersOnChange('outdoor_play', 1);
                }}
                checked={appliedFilters.outdoor_play == 1}
              />
              <CheckBox
                width="44%"
                label="AC Rooms"
                id="schedule_full_time_8"
                margin="0px 30px 25px 0px"
                filterFont="13px"
                filterChekbox="5px"
                filterLineheight="18px"
                onClickFunction={e => {
                  e.preventDefault();
                  if (appliedFilters.ac_room == 1)
                    filtersOnChange('ac_room', null);
                  else filtersOnChange('ac_room', 1);
                }}
                checked={appliedFilters.ac_room == 1}
              />
              <Flex column flexWidth="44%" flexMargin="0px 30px 25px 0px">
                <CheckBox
                  width="100%"
                  label="Extended Hours"
                  id="schedule_full_time_9"
                  filterFont="13px"
                  filterChekbox="5px"
                  filterLineheight="18px"
                  onClickFunction={e => {
                    e.preventDefault();
                    if (appliedFilters.extended_hours == 1)
                      filtersOnChange('extended_hours', null);
                    else filtersOnChange('extended_hours', 1);
                  }}
                  checked={appliedFilters.extended_hours == 1}
                />
                {appliedFilters.extended_hours == 1 && (
                  <FilterItem
                    className="rangeSlider"
                    width="100%"
                    margin="40px 0px 0px"
                    // padding="0px 0px 0px 10px"
                  >
                    <InputRange
                      maxValue={24}
                      minValue={0}
                      value={extendedHoursSliderValues}
                      formatLabel={d => `${d} hrs`}
                      onChange={val => setExtendedHoursSliderValues(val)}
                      onChangeComplete={val =>
                        onSliderChange({
                          min_extended_hours: val.min,
                          max_extended_hours: val.max,
                        })
                      }
                      allowSameValues={false}
                    />
                  </FilterItem>
                )}
              </Flex>
              {sentFilters.organisation == 3 && (
                <Flex column flexWidth="44%" flexMargin="0px 30px 25px 0px">
                  <CheckBox
                    width="100%"
                    label="Daycare cost(Price/Month)"
                    id="schedule_full_time_10"
                    filterFont="13px"
                    filterChekbox="5px"
                    filterLineheight="18px"
                    onClickFunction={e => {
                      e.preventDefault();
                      if (appliedFilters.daycareprice == 1)
                        filtersOnChange('daycareprice', null);
                      else filtersOnChange('daycareprice', 1);
                    }}
                    checked={appliedFilters.daycareprice == 1}
                  />
                  {appliedFilters.daycareprice === 1 && (
                    <FilterItem
                      className="rangeSlider"
                      width="100%"
                      margin="40px 0px 0px"
                      // padding="0px 0px 0px 10px"
                    >
                      <InputRange
                        maxValue={20000}
                        minValue={500}
                        value={daycarePriceSliderValues}
                        onChange={val => setDaycarePriceSliderValues(val)}
                        onChangeComplete={val =>
                          onSliderChange({
                            daycare_min_price: val.min,
                            daycare_max_price: val.max,
                          })
                        }
                        allowSameValues={false}
                      />
                    </FilterItem>
                  )}
                </Flex>
              )}
            </Flex>
          </Flex>
        </>
      )}

      <Flex flexMargin="0px" justifyEnd>
        <Button
          text="Reset Filter"
          marginRight="10px"
          type="subscribe"
          headerButton
          onClick={resetFilter}
          disabled={checkIfFiltersChnaged(
            appliedFilters.extended_hours == 0
              ? (() => {
                  const {
                    min_extended_hours,
                    max_extended_hours,
                    ...filters
                  } = appliedFilters;

                  return filters;
                })()
              : appliedFilters,
          )}
        />
        <Button
          text="Apply Filter"
          type="mobile"
          headerButton
          onClick={e => {
            setFilterSectionActive(false);
            onApplyFilter(
              e,
              {
                ...sentFilters,
                ...appliedFilters,
              },
              searchMethod,
            );
            closeHandler();
            if (!blockSearch) {
              queryChange(e, { ...sentFilters, ...appliedFilters });
            }
          }}
        />
      </Flex>
    </AdditionalFilterWrapper>
  );
};

DefaultModal.propTypes = {
  modalType: PropTypes.string,
  closeHandler: PropTypes.func,
  sentFilters: PropTypes.object,
  resetFilter: PropTypes.func,
  onApplyFilter: PropTypes.func,
  queryChange: PropTypes.func,
  searchMethod: PropTypes.string,
  blockSearch: PropTypes.bool,
  setFilterSectionActive: PropTypes.func,
};

export default DefaultModal;

const checkIfFiltersChnaged = obj =>
  !Object.entries(obj)
    .map(([key, value]) => {
      if (key === 'schedule_type') {
        if ((value || []).length > 0) return false;
        return true;
        // eslint-disable-next-line no-else-return
      } else {
        if (value === null) return true;
        return false;
      }
    })
    .includes(false);

const FILTERS_INITIAL_STATE = {
  schedule_type: [],
  cctv: null,
  live_streaming: null,
  ac_room: null,
  free_trial: null,
  outdoor_play: null,
  parent_app: null,
  massaging_bathing: null,
  extended_hours: null,
  special_care: null,
  activities: null,
  tution: null,
  min_extended_hours: null,
  max_extended_hours: null,
  daycareprice: null,
  daycare_min_price: null,
  daycare_max_price: null,
};
const ToolTipInfo = styled.div`
  display: none;
  position: absolute;
  width: max-content;
  padding: 10px;
  background-color: #fff;
  color: #4b5256;
  font-family: Roboto;
  font-size: 12px;
  letter-spacing: 0;
  line-height: 19px;
  border-radius: 10px;
  top: -70px;
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
  &::before {
    content: '';
    display: block;
    width: 0;
    height: 0;
    position: absolute;
    width: 15px;
    height: 20px;
    transform: rotate(45deg);
    background: #fff;
    left: 33%;
    bottom: -7px;
  }
`;
const AdditionalFilterWrapper = styled.div`
  width: 710px;
  height: ${props => props.height};
  margin: 5% auto;
  padding: 25px;
  padding-left: 45px;
  border-radius: 10px;
  background: ${colors.white};
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
  position: relative;
  @media (max-width: 500px) {
    max-width: 380px;
    padding: 20px;
  }
  @media (max-width: 380px) {
    max-width: 310px;
    padding: 20px;
  }
  .specialService {
    @media (max-width: 500px) {
      padding: 10px 10px;
    }
    .checkbox {
      @media (max-width: 500px) {
        margin: 0px 8px 0px 0px;
      }
    }
  }
  .checklists {
    @media (max-width: 500px) {
      padding: 0px;
    }
    & > div {
      @media (max-width: 500px) {
        width: 100%;
        margin-bottom: 24px;
      }
    }
  }

  .subscribe,
  .mobile {
    @media (max-width: 500px) {
      height: 40px;
      font-size: 14px;
    }
  }

  .infoIcon {
    display: flex;
    align-items: center;
    cursor: pointer;
    .iconify {
      width: 17px;
      height: 17px;
      color: #666c78;
    }
    &:hover {
      .iconify {
        color: #60b947;
      }
      & + ${ToolTipInfo} {
        display: block;
      }
    }
  }
`;

const Title = styled.div`
  width: 200px;
  color: #7e888e;
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 19px;
  white-space: nowrap;
`;

const OrgName = styled.div`
  color: #30333b;
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 19px;
  margin: ${props => props.margin || '0px'};
`;

const TypeOrg = styled.div`
  font-family: 'Roboto', sans-serif;
  color: #7e888e;
  font-family: Roboto;
  font-size: 12px;
  line-height: 16px;
`;

const ModalTitle = styled.div`
  margin-bottom: 30px;
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 500;
  line-height: 28px;

  &.creatAccount {
    color: #30333b;
    font-size: 20px;
    // font-weight: 400;
    line-height: 23px;
  }
  &.loginQues {
    font-family: 'Roboto', sans-serif;
    font-weight: 400;
    text-align: center;
    span {
      margin: 0px 5px;
    }
    &__title {
      margin-bottom: 20px;
      font-family: 'Roboto', sans-serif;
      font-weight: 400;
      text-align: center;
      font-size: 16px;
      line-height: 21px;
    }
  }
`;
const FilterItem = styled.div`
  // flex-grow: 1;
  width: ${props => props.width || 'fit-content'};
  margin: ${props => props.margin || '0px'};
  padding: ${props => props.padding || '0px 18px'};
  border-right: 1px solid ${colors.filterBorder};
  white-space: nowrap;
  &:first-child {
    padding-left: 0px;
  }
  &:nth-last-child(2) {
    flex-grow: 1;
    border-width: 0px;
    padding-right: 0px;
  }
  .input-range__slider {
    background: ${colors.secondary};
    border: 5px solid #fff;
    box-shadow: 0px 0px 2px #000;
  }
  .input-range__track--background {
    margin-top: -0.05rem;
    height: 6px;
  }
  .input-range__label--min,
  .input-range__label--max {
    .input-range__label-container {
      display: none;
    }
  }
  &.smallerDevices {
    display: none;
    @media (max-width: 768px) {
      display: flex;
      flex-direction: column;
      align-items: flex-start;
    }
  }
  &.rangeSlider {
    flex-grow: 1;
    min-width: 11%;
  }
  @media (max-width: 768px) {
    width: 100%;
    display: flex;
    align-items: center;
    margin-top: 20px;
    padding-right: 0px;
    padding-left: 0px;
    border-width: 0px;
  }
  @media (max-width: 414px) {
    flex-wrap: wrap;
  }
  &.mobileonly {
    @media (max-width: 768px) {
      display: none;
    }
  }
`;

const CloseModal = styled.div`
  position: absolute;
  top: -15px;
  right: -10px;
  cursor: pointer;
  &:hover {
    .iconify {
      color: #000;
    }
  }
  .iconify {
    height: 12px;
    width: 12px;
    color: #666c78;
  }
`;
