/* eslint-disable indent */
import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Button from '../Button';
import colors from '../../utils/colors';
import Flex from '../common/flex';

const EnquiryModal = ({
  closeHandler,
  authentication: {
    profile: { data: { email = '', mobile_number: mobile = '' } } = {},
  } = {},
  dashboard: {
    PROVIDER_CONTACT_DETAILS_API: { data: contactData = [] } = {},
  } = {},

  modalTitle,
  type,
}) => {
  const currentRef = useRef();

  return (
    <AdditionalFilterWrapper className="tour" ref={currentRef}>
      <CloseModal onClick={closeHandler}>
        {' '}
        <span
          className="iconify"
          data-icon="bytesize:close"
          data-inline="false"
        />
      </CloseModal>

      <GalleryHeading margin="0px 0px 10px">
        {type === 'contactUsSuccess'
          ? 'Contact Information'
          : modalTitle || 'Your Enquiry has been sent'}
      </GalleryHeading>

      <OrgName margin="0px 0px 20px">
        {type !== 'contactUsSuccess'
          ? 'Our team will get in touch with you at'
          : 'You can reach out to'}
      </OrgName>

      <div className="scheduleTour">
        <Flex column flexPadding="0px 0px 0px 0px">
          {type === 'contactUsSuccess' ? (
            <Flex alignCenter flexMargin="0px 0px 20px 0px">
              {/* <span
                className="iconify"
                data-icon="feather:mail"
                data-inline="false"
              /> */}
              <span className="" style={{ color: '#30333b' }}>
                {contactData.length > 0 ? contactData[0].name : ''}
              </span>
            </Flex>
          ) : null}
          <Flex alignCenter flexMargin="0px 0px 20px 0px">
            <span
              className="iconify"
              data-icon="feather:mail"
              data-inline="false"
            />
            {type === 'contactUsSuccess' ? (
              <span className="personalDetail">
                {contactData.length > 0 ? contactData[0].email_id : ''}
              </span>
            ) : (
              <span className="personalDetail">{email}</span>
            )}
          </Flex>
          <Flex alignCenter flexMargin="0px 0px 20px 0px">
            <span
              className="iconify"
              data-icon="ant-design:phone-outlined"
              data-inline="false"
            />
            {type === 'contactUsSuccess' ? (
              <span className="personalDetail">
                {contactData.length > 0
                  ? `${contactData[0].phone_number}, ${
                      contactData[0].alt_phone_number
                    }`
                  : ''}
              </span>
            ) : (
              <span className="personalDetail">{mobile}</span>
            )}
          </Flex>
        </Flex>
        <Button
          text="Ok"
          type="mobile"
          marginRight="10px"
          onClick={closeHandler}
        />
      </div>
    </AdditionalFilterWrapper>
  );
};

EnquiryModal.propTypes = {
  closeHandler: PropTypes.func,
  authentication: PropTypes.object,
  dashboard: PropTypes.object,
  modalTitle: PropTypes.string,
  type: PropTypes.string,
};

export default EnquiryModal;

const AdditionalFilterWrapper = styled.div`
  width: 710px;
  height: ${props => props.height};
  margin: 5% auto;
  padding: 25px;
  padding-left: 45px;
  border-radius: 10px;
  background: ${colors.white};
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
  position: relative;

  // &.testimonials {
  //   max-width: 665px;
  //   width: 100%;
  //   padding: 50px 25px 50px 36px;
  //   margin: 4% auto 20px;
  // }
  // &.teachers {
  //   max-width: 1022px;
  //   width: 100%;
  //   padding: 62px 58px;
  //   margin: 5% auto;
  // }
  &.tour {
    max-width: 500px;
    width: 100%;
    padding: 50px 48px;
    margin: 15% auto 20px;
    @media screen and (max-width: 768px) {
      max-width: 290px;
      width: 100%;
      padding: 20px;
      margin: 20% auto;
    }
    .scheduleTour {
      display: flex;
      flex-direction: column;
      .personalDetail {
        color: #30333b;
        font-family: 'Roboto', sans-serif;
        font-size: 16px;
        line-height: 22px;
        // margin-bottom: 20px;
        padding-left: 15px;
      }
    }
    @media (max-width: 500px) {
      max-width: 380px !important;
      width: 100% !important;
      padding: 50px 20px;
    }
    @media (max-width: 380px) {
      max-width: 320px !important;
    }
    @media (max-width: 330px) {
      max-width: 300px !important;
    }
  }
`;

export const GalleryHeading = styled.div`
  margin: ${props => props.margin || '0px'};
  color: ${props => props.color || '#30333B'};
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 500;
  line-height: 28px;
  @media (max-width: 500px) {
    font-size: 16px;
    line-height: 25px;
    margin-bottom: 30px;
  }
`;

const CloseModal = styled.div`
  position: absolute;
  top: 20px;
  right: 20px;
  cursor: pointer;
  @media screen and (max-width: 768px) {
    top: 1px;
    right: 7px;
  }
  .iconify {
    height: 12px;
    width: 12px;
    color: #666c78;
  }
`;

const OrgName = styled.div`
  color: #30333b;
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 19px;
  margin: ${props => props.margin || '0px'};
`;

export const ScheduleDetail = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  border-radius: 10px;
  background-color: #f3f5f9;
  padding: 18px 24px;
  margin-bottom: 10px;
  h2 {
    color: #30333b;
    font-family: 'Quicksand', sans-serif;
    font-size: 16px;
    font-weight: bold;
    line-height: 26px;
    text-align: center;
    margin: 0px;
    span {
      font-weight: 300;
      margin-right: 4px;
    }
  }
`;
