/* eslint-disable react/prop-types */
/* eslint-disable indent */
/* eslint-disable no-nested-ternary */
/* eslint-disable no-constant-condition */
import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import moment from 'moment';
import Flex from 'components/common/flex';
import colors from '../../utils/colors';

const OnlineProgramBatches = ({
  closeHandler,
  // clickType,
  batchInfo = {},
}) => {
  const currentRef = useRef();
  return (
    <>
      <AdditionalFilterWrapper className="tour" ref={currentRef}>
        <ModalTitle className="creatAccount">Batches</ModalTitle>
        <CloseModal onClick={closeHandler}>
          {' '}
          <span
            className="iconify"
            data-icon="bytesize:close"
            data-inline="false"
          />
        </CloseModal>

        {batchInfo && (
          <Flex column flexMargin="10px 0px 0px">
            <AdditionalInfo margin="0px 0px 5px" color="#666C78">
              <span> {batchInfo.heading_name}</span>
            </AdditionalInfo>
            {batchInfo.start_date && batchInfo.end_date ? (
              <AdditionalInfo margin="0px 0px 5px" color="#60B947">
                <span>{moment(batchInfo.start_date).format('DD MMM YY')}</span>{' '}
                <span style={{ margin: '0 5px' }}>to</span>
                <span>{moment(batchInfo.end_date).format('DD MMM YY')}</span>
              </AdditionalInfo>
            ) : (
              <AdditionalInfo margin="0px 0px 5px" color="#60B947">
                <span>{batchInfo.till_date}</span>
              </AdditionalInfo>
            )}
          </Flex>
        )}

        <BuyNow>
          {batchInfo.batch_slots && batchInfo.batch_slots.length ? (
            <ul className="batch-dateList">
              {batchInfo.batch_slots.map(d => (
                <li className="batch-dateItem">
                  <Flex column>
                    <p>{moment(d.selected_date).format('DD MMM')}</p>
                    {/* <span>Mon-thu</span> */}
                  </Flex>
                  {/* <div className="batch-time">
                                  {d.time.split(":") &&
                                  d.time.split(":")[0] > 12
                                    ? `${d.time.split(":")[0] - 12}:${
                                        d.time.split(":")[1]
                                      } PM`
                                    : `${d.time.split(":")[0]}:${
                                        d.time.split(":")[1]
                                      } AM`}
                                </div> */}
                  {batchInfo.time && (
                    <div className="batch-time">
                      {moment(batchInfo.time, 'H:mm:ss').format('h:mm a')}
                    </div>
                  )}
                  {/* <div>{batchInfo.time}</div> */}
                </li>
              ))}
            </ul>
          ) : null}
          {batchInfo.batch_days && batchInfo.batch_days.length ? (
            <ul className="batch-dateList">
              {batchInfo.batch_days.map(d => (
                <li className="batch-dateItem">
                  <Flex column>
                    <p>{d.selected_day}</p>
                    {/* <span>Mon-thu</span> */}
                  </Flex>
                  {/* <div className="batch-time">
                                  {d.time.split(":") &&
                                  d.time.split(":")[0] > 12
                                    ? `${d.time.split(":")[0] - 12}:${
                                        d.time.split(":")[1]
                                      } PM`
                                    : `${d.time.split(":")[0]}:${
                                        d.time.split(":")[1]
                                      } AM`}
                                </div> */}
                  {d.time && (
                    <div className="batch-time">
                      {moment(d.time, 'H:mm:ss').format('h:mm a')}
                    </div>
                  )}
                  {/* <div>{batchInfo.time}</div> */}
                </li>
              ))}
            </ul>
          ) : null}
        </BuyNow>
      </AdditionalFilterWrapper>
    </>
  );
};

OnlineProgramBatches.propTypes = {
  closeHandler: PropTypes.func,
  // clickType: PropTypes.string,
};

// RestrictionModal.propTypes = {
//   closeHandler: PropTypes.func,
//   setModalType: PropTypes.func,
//   successToast: PropTypes.func,
//   errorToast: PropTypes.func,
//   isChildEdit: PropTypes.bool,
//   editChildId: PropTypes.number,
//   router: PropTypes.object,
// };

export default OnlineProgramBatches;

const CloseModal = styled.div`
  position: absolute;
  top: 20px;
  right: 20px;
  cursor: pointer;
  @media screen and (max-width: 768px) {
    top: 1px;
    right: 7px;
  }
  .iconify {
    height: 12px;
    width: 12px;
    color: #666c78;
  }
`;

const ModalTitle = styled.div`
  margin-bottom: 20px;
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 500;
  line-height: 28px;

  &.creatAccount {
    color: #30333b;
    font-size: 20px;
    // font-weight: 400;
    line-height: 23px;
  }
  &.loginQues {
    font-family: 'Roboto', sans-serif;
    font-weight: 400;
    text-align: center;
    span {
      margin: 0px 5px;
    }
    &__title {
      margin-bottom: 20px;
      font-family: 'Roboto', sans-serif;
      font-weight: 400;
      text-align: center;
      font-size: 16px;
      line-height: 21px;
    }
  }
`;

const AdditionalFilterWrapper = styled.div`
  width: 710px;
  height: ${props => props.height};
  margin: 5% auto;
  padding: 25px;
  padding-left: 45px;
  border-radius: 10px;
  background: ${colors.white};
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
  position: relative;
  max-height: 90vh;
  overflow-y: scroll;

  &.tour {
    max-width: 500px;
    width: 100%;
    padding: 30px;
    margin: 20% auto;
    @media screen and (max-width: 768px) {
      max-width: 290px;
      width: 100%;
      padding: 20px;
      margin: 20% auto;
    }
    .scheduleTour {
      display: flex;
      flex-direction: column;
      .personalDetail {
        color: #30333b;
        font-family: 'Roboto', sans-serif;
        font-size: 16px;
        line-height: 22px;
        margin-bottom: 20px;
      }
    }
  }
`;

const BuyNow = styled.ul`
  width: 100%;
  padding: 0px 0;
  border: 1px solid #bfc8ce;
  border-radius: 10px;
  margin: 15px 0 0 0;
  padding: 0;
  .batch-dateList {
    padding: 0;
  }
  li {
    padding: 20px;
    border-bottom: 1px solid #ddd9e1;
    list-style-type: none;
    // @media screen and (max-width: 768px) {
    //   padding: 10px 0px;
    // }
    p {
      margin: 0 0 5px;
    }
    h2 {
      display: flex;
      align-items: center;
      justify-content: space-between;
      font-family: Quicksand;
      font-weight: bold;
      font-size: 18px;
      line-height: 22px;
      color: #4b5256;
      margin: 0px;
      @media screen and (max-width: 768px) {
        font-size: 13px;
        line-height: 1.2;
      }
      span {
        font-family: 'Roboto', sans-serif;
        font-weight: 400;
        font-size: 18px;
        line-height: 21px;
        text-align: right;
        color: #4b5256;
        @media screen and (max-width: 768px) {
          font-size: 11px;
          line-height: 1.2;
        }
      }
    }
  }
  h3 {
    font-family: Quicksand;
    font-weight: bold;
    font-size: 24px;
    line-height: 30px;
    color: #000000;
    margin: 0px 8px 0px 0px;
    @media screen and (max-width: 768px) {
      font-size: 16px;
      line-height: 1.2;
    }
  }
  h6 {
    margin: 0px;
    font-family: Quicksand;
    font-weight: bold;
    font-size: 14px;
    line-height: 17px;
    color: #000000;
    @media screen and (max-width: 768px) {
      font-size: 10px;
      line-height: 1.2;
    }
    span {
      font-weight: 400;
      text-decoration-line: line-through;
      opacity: 0.5;
      margin-right: 4px;
    }
  }
  p {
    font-family: Quicksand;
    font-weight: 400;
    font-size: 16px;
    line-height: 20px;
    color: #000000;
    margin: 0px 0px 18px;
    @media screen and (max-width: 768px) {
      font-size: 12px;
      line-height: 1.2;
      color: #000000;
      margin: 0px 0px 10px;
    }
  }
  button {
    @media screen and (max-width: 768px) {
      font-size: 14px;
      height: auto;
      width: auto;
      padding: 10px 20px;
    }
  }
`;

const AdditionalInfo = styled.div`
  color: ${props => props.color || '#666c78'};
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 22px;
  margin: ${props => props.margin || '0px'};
  span {
    color: ${props => props.color || colors.lightBlack}};
  }
`;

// const ModalSubTitle = styled.div`
//   margin-left: 5px;
//   margin-bottom: 20px;
//   color: #30333b;
//   font-family: 'Roboto', sans-serif;
//   font-size: 14px;
//   line-height: 19px;
//   @media (max-width: 500px) {
//     text-align: center;
//   }
// `;

// const ModalLeft = styled.div`
//   margin-left: auto;
//   max-width: 66%;
//   .alreadySigned {
//     color: #666c78;
//     font-family: Roboto;
//     font-size: 14px;
//     letter-spacing: 0;
//     line-height: 14px;
//     font-weight: 400;
//     text-align: center;
//     @media (max-width: 500px) {
//       font-size: 12px;
//     }
//     span {
//       color: #613a95;
//       margin-left: 5px;
//       cursor: pointer;
//     }
//   }
//   @media (max-width: 500px) {
//     margin-left: 0px;
//     max-width: 100%;
//   }
// `;

// const ModalTitle = styled.div`
//   margin-bottom: 10px;
//   font-family: 'Quicksand', sans-serif;
//   font-size: 22px;
//   font-weight: 600;
//   line-height: 28px;

//   &.creatAccount {
//     color: #30333b;
//     font-size: 20px;
//     // font-weight: 400;
//     line-height: 23px;
//     @media (max-width: 500px) {
//       font-size: 18px;
//       text-align: center;
//     }
//   }
//   &.loginQues {
//     font-family: 'Roboto', sans-serif;
//     font-weight: 400;
//     text-align: center;
//     span {
//       margin: 0px 5px;
//     }
//     &__title {
//       margin-bottom: 20px;
//       font-family: 'Roboto', sans-serif;
//       font-weight: 400;
//       text-align: center;
//       font-size: 16px;
//       line-height: 21px;
//     }
//   }
// `;

// const ModalRight = styled.div`
//   position: absolute;
//   left: 0px;
//   bottom: 0px;
//   img {
//     @media (max-width: 500px) {
//       margin-bottom: -52px;
//       margin-top: -28px;
//       margin-left: -31px;
//       transform: scale(0.7);
//     }
//   }
//   @media (max-width: 500px) {
//     position: relative;
//   }
// `;
