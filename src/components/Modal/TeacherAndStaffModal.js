/* eslint-disable consistent-return */
/* eslint-disable array-callback-return */
/* eslint-disable no-nested-ternary */
import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import teacher1 from 'images/teacher-female-ph1.png';
import teacher2 from 'images/teacher-female-ph2.png';
import teacher3 from 'images/teacher-female-ph3.png';
import staff1 from 'images/staff-female-ph1.png';
import staff2 from 'images/staff-female-ph2.png';
import colors from '../../utils/colors';
import TeacherDetail from '../TeacherDetail/index';
import SchoolCard from '../SchoolCard';
import Flex from '../common/flex';
import { Photo } from '../PhotoCard/styled';

const TEACHER_PLACEHOLDERS = [teacher1, teacher2, teacher3];
const STAFF_PLACEHOLDERS = [staff1, staff2];

const teacherPlaceholderImageMapper = id => {
  if (id % 3 === 0) {
    return teacher1;
  }
  if (id % 3 === 1) {
    return teacher2;
  }
  if (id % 3 === 2) {
    return teacher3;
  }
};
const staffPlaceholderImageMapper = id => {
  if (id % 2 === 0) {
    return staff1;
  }
  if (id % 2 === 1) {
    return staff2;
  }
};

const TeacherAndStaffModal = ({ closeHandler, faculty = [], languages }) => {
  const currentRef = useRef();
  const teachers = faculty.filter(a => a.faculty_type === 'teacher');
  const staffs = faculty.filter(a => a.faculty_type === 'staff');
  return (
    <AdditionalFilterWrapper className="teachers" ref={currentRef}>
      <CloseModal onClick={closeHandler}>
        {' '}
        <span
          className="iconify"
          data-icon="bytesize:close"
          data-inline="false"
        />
      </CloseModal>

      {teachers.length > 0 ? (
        <>
          <GalleryHeading>Our Teachers</GalleryHeading>
          <TeacherWrapper flexMargin="30px 0px">
            {teachers &&
              teachers.map((teach, i) => {
                const langFilter = languages.filter(a => {
                  if (teach.languages_spoken.includes(a.id)) {
                    return a.name;
                  }
                });
                const lang = langFilter.map(a => a.name);
                return (
                  <TeacherDetail
                    name={teach.name}
                    image={
                      teach.photo_url
                        ? teach.photo_url
                        : i < 3
                          ? TEACHER_PLACEHOLDERS[i]
                          : teacherPlaceholderImageMapper(i)
                    }
                    years={teach.experience ? `${teach.experience} yrs` : null}
                    desc={
                      teach.words_from_teacher
                        ? teach.words_from_teacher
                        : "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s"
                    }
                    languages={lang ? lang.join(', ') : null}
                    type="verified"
                    classType="popup"
                    margin="0px 0px 20px"
                    photoWidth="192px"
                  />
                );
              })}
          </TeacherWrapper>
        </>
      ) : (
        ''
      )}

      {staffs.length > 0 ? (
        <>
          <OurStaff>
            <GalleryHeading>Our Staff</GalleryHeading>
            <div className="ourStaff">
              {staffs &&
                staffs.map((staff, i) => (
                  <SchoolCard
                    schoolImage={
                      staff.photo_url
                        ? staff.photo_url
                        : i < 2
                          ? STAFF_PLACEHOLDERS[i]
                          : staffPlaceholderImageMapper(i)
                    }
                    schoolCardHeight="100px"
                    schoolCardWidth="100px"
                    staffName={staff.name}
                    additionalComment={
                      staff.additional_comments
                        ? staff.additional_comments
                        : "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s"
                    }
                    years={staff.experience ? `${staff.experience} yrs` : null}
                    cardtype="staff"
                    // designation="care taker"
                  />
                ))}
            </div>
          </OurStaff>
        </>
      ) : (
        ''
      )}
    </AdditionalFilterWrapper>
  );
};

TeacherAndStaffModal.propTypes = {
  closeHandler: PropTypes.func,
  faculty: PropTypes.array,
  languages: PropTypes.array,
};

export default TeacherAndStaffModal;

const AdditionalFilterWrapper = styled.div`
  width: 710px;
  height: ${props => props.height};
  margin: 5% auto;
  padding: 25px;
  padding-left: 45px;
  border-radius: 10px;
  background: ${colors.white};
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
  position: relative;

  &.teachers {
    max-width: 1022px;
    width: 100%;
    padding: 62px 58px;
    margin: 5% auto;
    @media (max-width: 768px) {
      padding: 40px 24px;
      max-width: 380px;
      position: absolute;
      top: 0;
    }
    @media (max-width: 380px) {
      max-width: 320px;
    }
  }
  .popup {
    @media (max-width: 425px) {
      min-width: 55%;
      display: flex;
      flex-direction: column;
      align-items: flex-start;
      margin: 0px 20px 30px 0px;
      padding-right: 20px;
      border-right: 0px solid #e6e8ec !important;
      .teacherContent {
        width: 100%;
      }
      ${Photo} {
        margin-right: 0px;
        width: 60%;
        height: 200px;
      }
    }
  }
`;

export const GalleryHeading = styled.div`
  margin: ${props => props.margin || '0px'};
  color: ${props => props.color || '#30333B'};
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 500;
  line-height: 28px;
`;

const CloseModal = styled.div`
  position: absolute;
  top: 20px;
  right: 20px;
  cursor: pointer;
  @media screen and (max-width: 768px) {
    top: 1px;
    right: 7px;
  }
  .iconify {
    height: 12px;
    width: 12px;
    color: #666c78;
  }
`;

export const TeacherWrapper = styled(Flex)`
  flex-direction: column;
  @media (max-width: 414px) {
    flex-direction: column;
  }
`;

export const OurStaff = styled.div`
  padding: 42px 0px;
  border-top: 1px solid #e6e8ec;
  margin-top: 20px;
  .ourStaff {
    display: flex;
    flex-direction: column;
    flex-wrap: wrap;
    justify-content: space-between;
    & > div {
      width: 90%;
    }
  }
`;
export const OurSecurity = styled.div`
  padding: 42px 0px;
  .ourStaff {
    display: flex;
    flex-wrap: wrap;
  }
`;
