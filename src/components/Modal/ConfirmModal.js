import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import moment from 'moment';
import Button from '../Button';
import colors from '../../utils/colors';
import confirmImage from '../../images/confirmation icon.png';

const ConfirmModal = ({
  closeHandler,
  setScheduleRes,
  scheduleRes: { tour_id: tourId, date, time } = {},
}) => {
  const currentRef = useRef();

  return (
    <AdditionalFilterWrapper className="tour" ref={currentRef}>
      <CloseModal
        onClick={() => {
          closeHandler();
          setScheduleRes({});
        }}
      >
        {' '}
        <span
          className="iconify"
          data-icon="bytesize:close"
          data-inline="false"
        />
      </CloseModal>

      <div className="confirmMessage">
        <div>
          <img src={confirmImage} alt="" />
        </div>
        <OrgName margin="30px 0px 30px" style={{ textAlign: 'center' }}>
          <b>Thank you!</b>
          <br />
          We will update you through you’re registered email ID/Mobile No., once
          you’re schedule is confirmed
        </OrgName>

        <ScheduleDetail>
          <h2>
            <span>Date:</span>
            {moment(date).format('dddd, MMM DD')}, {time}
          </h2>
          <h2>
            <span>Tour Id:</span>
            {tourId}
          </h2>
        </ScheduleDetail>
        <Button
          text="Share on Whatsapp"
          type="share"
          fullwidth
          onClick={() => {}}
        />
        <Button
          text="Done"
          type="mobile"
          marginTop="10px"
          fullwidth
          onClick={() => {
            closeHandler();
            setScheduleRes({});
          }}
        />
      </div>
    </AdditionalFilterWrapper>
  );
};

ConfirmModal.propTypes = {
  closeHandler: PropTypes.func,
  setScheduleRes: PropTypes.func,
  scheduleRes: PropTypes.object,
};

export default ConfirmModal;

const AdditionalFilterWrapper = styled.div`
  width: 710px;
  height: ${props => props.height};
  margin: 5% auto;
  padding: 25px;
  padding-left: 45px;
  border-radius: 10px;
  background: ${colors.white};
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
  position: relative;

  &.tour {
    max-width: 500px;
    width: 100%;
    padding: 50px 48px;
    margin: 5% auto 20px;
    @media screen and (max-width: 768px) {
      max-width: 290px;
      width: 100%;
      padding: 20px;
      margin: 20% auto;
    }
  }
  .confirmMessage {
    //   display:flex;
    //   flex-direction:column;
    //   align-items:center;
    text-align: center;
  }
`;

export const GalleryHeading = styled.div`
  margin: ${props => props.margin || '0px'};
  color: ${props => props.color || '#30333B'};
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 500;
  line-height: 28px;
`;

const CloseModal = styled.div`
  position: absolute;
  top: 20px;
  right: 20px;
  cursor: pointer;
  @media screen and (max-width: 768px) {
    top: 1px;
    right: 7px;
  }
  .iconify {
    height: 12px;
    width: 12px;
    color: #666c78;
  }
`;

const OrgName = styled.div`
  color: #30333b;
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 19px;
  margin: ${props => props.margin || '0px'};
`;

export const ScheduleDetail = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  border-radius: 10px;
  background-color: #f3f5f9;
  padding: 18px 24px;
  margin-bottom: 10px;
  h2 {
    color: #30333b;
    font-family: 'Quicksand', sans-serif;
    font-size: 16px;
    font-weight: bold;
    line-height: 26px;
    text-align: center;
    margin: 0px;
    span {
      font-weight: 300;
      margin-right: 4px;
    }
  }
`;
