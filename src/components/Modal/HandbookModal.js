import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import colors from '../../utils/colors';
import Flex from '../common/flex';
import jumboImage from '../../images/JumboLogo.png';

const HandbookModal = ({ closeHandler }) => {
  const currentRef = useRef();
  return (
    <AdditionalFilterWrapper className="teachers" ref={currentRef}>
      <CloseModal onClick={closeHandler}>
        {' '}
        <span
          className="iconify"
          data-icon="bytesize:close"
          data-inline="false"
        />
      </CloseModal>

      <HandbookHeading>Parents Handbook</HandbookHeading>
      <Flex alignCenter justifyBetween flexMargin="36px 0px 0px">
        <Flex alignCenter>
          <JumboImage>
            <img src={jumboImage} alt="" />
          </JumboImage>
          <HandbookTitle>Jumbo Kids</HandbookTitle>
        </Flex>
        <HandBookBtn>Download Handbook</HandBookBtn>
      </Flex>
      <Flex column flexPadding="0px 120px">
        <Flex
          column
          flexPadding="40px 0px 45px"
          flexBorderdashed="1px solid #E6E8EC"
        >
          <ListHeading>Rules and Regulations</ListHeading>
          <ListDesc>
            Lorem ipsum dolor sit amet, mel ex ipsum dolore. Sit commodo eripuit
            an, has et natum invenire. An vix nostro accusata omittantur. Lorem
            ipsum dolor sit amet, mel ex ipsum dolore. Sit commodo eripuit an,
            has et natum invenire. An vix nostro accusata omittantur. Lorem
            ipsum dolor sit amet, mel ex ipsum dolore. Sit commodo eripuit an,
            has et natum invenire. An vix nostro accusata omittantur
          </ListDesc>
        </Flex>
        <Flex
          column
          flexPadding="40px 0px 45px"
          flexBorderdashed="1px solid #E6E8EC"
        >
          <ListHeading>Child Safety</ListHeading>
          <ListDesc>
            Lorem ipsum dolor sit amet, mel ex ipsum dolore. Sit commodo eripuit
            an, has et natum invenire. An vix nostro accusata omittantur. Lorem
            ipsum dolor sit amet, mel ex ipsum dolore. Sit commodo eripuit an,
            has et natum invenire. An vix nostro accusata omittantur. Lorem
            ipsum dolor sit amet, mel ex ipsum dolore. Sit commodo eripuit an,
            has et natum invenire. An vix nostro accusata omittantur
          </ListDesc>
        </Flex>
        <Flex
          column
          flexPadding="40px 0px 45px"
          flexBorderdashed="1px solid #E6E8EC"
        >
          <HandbookTitle margin="0px 0px 30px 0px">Enrollment</HandbookTitle>
          <ListHeading>Payment</ListHeading>
          <ListDesc>
            Lorem ipsum dolor sit amet, mel ex ipsum dolore. Sit commodo eripuit
            an, has et natum invenire. An vix nostro accusata omittantur. Lorem
            ipsum dolor sit amet, mel ex ipsum dolore. Sit commodo eripuit an,
            has et natum invenire. An vix nostro accusata omittantur. Lorem
            ipsum dolor sit amet, mel ex ipsum dolore. Sit commodo eripuit an,
            has et natum invenire. An vix nostro accusata omittantur
          </ListDesc>
        </Flex>
        <Flex
          column
          flexPadding="40px 0px 45px"
          // flexBorderdashed="1px solid #E6E8EC"
        >
          <ListHeading>Cancellation of admission</ListHeading>
          <ListDesc>
            Lorem ipsum dolor sit amet, mel ex ipsum dolore. Sit commodo eripuit
            an, has et natum invenire. An vix nostro accusata omittantur. Lorem
            ipsum dolor sit amet, mel ex ipsum dolore. Sit commodo eripuit an,
            has et natum invenire. An vix nostro accusata omittantur. Lorem
            ipsum dolor sit amet, mel ex ipsum dolore. Sit commodo eripuit an,
            has et natum invenire. An vix nostro accusata omittantur
          </ListDesc>
        </Flex>
      </Flex>
      <Flex />
    </AdditionalFilterWrapper>
  );
};

HandbookModal.propTypes = {
  closeHandler: PropTypes.func,
};

export default HandbookModal;

const AdditionalFilterWrapper = styled.div`
  width: 710px;
  height: ${props => props.height};
  margin: 5% auto;
  padding: 25px;
  padding-left: 45px;
  border-radius: 10px;
  background: ${colors.white};
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
  position: relative;
  &.testimonials {
    max-width: 665px;
    width: 100%;
    padding: 50px 25px 50px 36px;
    margin: 4% auto 20px;
  }
  &.teachers {
    max-width: 1022px;
    width: 100%;
    padding: 62px 58px;
    margin: 5% auto;
  }
  &.tour {
    max-width: 500px;
    width: 100%;
    padding: 50px 48px;
    margin: 5% auto 20px;
    @media screen and (max-width: 768px) {
      max-width: 290px;
      width: 100%;
      padding: 20px;
      margin: 20% auto;
    }
    .scheduleTour {
      display: flex;
      flex-direction: column;
      .personalDetail {
        color: #30333b;
        font-family: 'Roboto', sans-serif;
        font-size: 16px;
        line-height: 22px;
        margin-bottom: 20px;
      }
    }
  }
  .confirmMessage {
    //   display:flex;
    //   flex-direction:column;
    //   align-items:center;
    text-align: center;
  }
  .popupScroll {
    max-height: 590px;
    overflow: scroll;
  }
`;

export const GalleryHeading = styled.div`
  margin: ${props => props.margin || '0px'};
  color: ${props => props.color || '#30333B'};
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 500;
  line-height: 28px;
`;

const CloseModal = styled.div`
  position: absolute;
  top: 20px;
  right: 20px;
  cursor: pointer;
  @media screen and (max-width: 768px) {
    top: 1px;
    right: 7px;
  }
  .iconify {
    height: 12px;
    width: 12px;
    color: #666c78;
  }
`;

export const TeacherWrapper = styled(Flex)`
  @media (max-width: 414px) {
    flex-direction: column;
  }
`;

export const OurStaff = styled.div`
  padding: 42px 0px;
  border-top: 1px solid #e6e8ec;
  border-bottom: 1px solid #e6e8ec;

  margin-top: 20px;
  .ourStaff {
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
  }
`;

export const OurSecurity = styled.div`
  padding: 42px 0px;
  .ourStaff {
    display: flex;
    flex-wrap: wrap;
  }
`;

export const HandbookHeading = styled.div`
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 500;
  line-height: 28px;
`;

export const JumboImage = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 86px;
  width: 86px;
  border-radius: 50%;
  border: 3px solid #fff;
  background-color: rgba(255, 238, 79, 1);
  margin-right: 30px;
  & > img {
    width: 90%;
    height: 70%;
  }
`;

export const HandbookTitle = styled.div`
  color: #30333b;
  font-family: 'Quicksand', sans-serif;
  font-size: 20px;
  font-weight: bold;
  line-height: 25px;
  margin: ${props => props.margin || '0px'};
`;

export const HandBookBtn = styled.div`
  padding: 18px 20px;
  border: 1px solid #666c78;
  border-radius: 5px;
  background-color: #ffffff;
  color: #30333b;
  font-family: 'Quicksand', sans-serif;
  font-size: 16px;
  font-weight: 500;
  line-height: 20px;
`;

export const ListHeading = styled.div`
  color: #30333b;
  font-family: 'Quicksand', sans-serif;
  font-size: 18px;
  font-weight: bold;
  line-height: 23px;
  margin-bottom: 18px;
`;

export const ListDesc = styled.div`
  color: #666c78;
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 21px;
  padding-right: 70px;
`;
