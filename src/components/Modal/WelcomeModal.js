/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* react/no-unescaped-entities */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import CheckBox from 'components/Checkbox';
import { toast } from 'react-toastify';
import Button from '../Button';
import colors from '../../utils/colors';
import InputBox from '../InputBox';
import Flex from '../common/flex';
// import CheckBox from '../Checkbox';
import welcomeImage from '../../images/mascot1.png';
import { useLoginHook } from '../../shared/hooks';

const warningToast = msg => toast.warn(msg);

const WelcomeModal = ({
  closeHandler,
  setModalType,
  isEdit,
  setIsMobileEdit,
  setActiveAuthType,
  name,
  setName,
  ...props
}) => {
  const [isTermsAgreed, setTermsAggreedState] = useState(true);
  const [nameError, setNameErrorState] = useState('');
  const { errorToast, successToast } = props;

  const onError = ({ message }) => {
    errorToast(message);
  };

  const onSuccess = ({
    data: { restData: { already_registered: alreadyRegistered } = {} } = {},
  }) => {
    setModalType('verifyMobile');
    if (alreadyRegistered) {
      warningToast(
        'Mobile number is already registered. Please Enter OTP to login.',
      );
    } else {
      successToast('OTP sent successfully.');
    }
  };

  const {
    mobile: { value, onChange, onBlur, error },
    onSubmit,
    loginLoader,
  } = useLoginHook(props, { onError, onSuccess, isEdit });

  const onEnterButtonPressed = e => {
    if (e.key === 'Enter') {
      e.preventDefault();
      setActiveAuthType();
      let isError = null;
      if (!name.length) {
        isError = 'Please enter your name';
      } else {
        isError = '';
      }
      setNameErrorState(error);
      onSubmit(isError, { auth_type: 'register', name });
    }
  };

  const onInputChange = e => {
    e.preventDefault();
    setNameErrorState('');
    setName(e.target.value);
  };

  const onInputBlur = e => {
    e.preventDefault();
    if (e.target.value.length === 0) {
      setNameErrorState('Please enter your name');
    } else {
      setNameErrorState('');
    }
  };

  const submitHandler = () => {
    let isError = null;
    if (!name.length) {
      isError = 'Please enter your name';
    } else {
      isError = '';
    }
    setNameErrorState(error);
    onSubmit(isError, { auth_type: 'register', name });
  };

  return (
    <>
      <AdditionalFilterWrapper className="welcome">
        {/* {!props.disableLoginModalCloseIcon && ( */}
        <CloseModal onClick={closeHandler}>
          {' '}
          <span
            className="iconify"
            data-icon="bytesize:close"
            data-inline="false"
          />
        </CloseModal>
        {/* )} */}

        <ModalLeft>
          <ModalTitle className="creatAccount">Welcome to Kiddenz!</ModalTitle>
          <ModalSubTitle>
            Let&apos;s get to know you in order to suggest the best schools
          </ModalSubTitle>
          <div className="scheduleTour">
            <InputBox
              placeholder="Your Name"
              type="text"
              margin="0px 0px 20px 0px"
              value={name}
              onChange={onInputChange}
              onBlur={onInputBlur}
              error={nameError}
            />
            <InputBox
              name="mobile"
              placeholder="0000000000"
              type="tel"
              margin="0px 0px 20px 0px"
              value={value}
              onChange={onChange}
              onBlur={onBlur}
              error={error}
              onKeyPress={onEnterButtonPressed}
            />
            <Flex flexMargin="0px 0px 20px 0px">
              <CheckBox
                label={`I agree to the <a href="/terms-of-use" target="_blank">Terms of use</a> and <a href="/privacy-policy" target="_blank">Privacy Policy</a>`}
                onClickFunction={() =>
                  isTermsAgreed
                    ? setTermsAggreedState(false)
                    : setTermsAggreedState(true)
                }
                checked={isTermsAgreed === true}
              />
            </Flex>

            <Button
              text="Send OTP"
              type="mobile"
              disabled={!isTermsAgreed}
              onClick={() => {
                setActiveAuthType();
                submitHandler();
              }}
              isLoading={loginLoader}
            />
            <h2 className="alreadySigned">
              Already have an account?{' '}
              <span onClick={() => setModalType('login')}>Login</span>{' '}
            </h2>
          </div>
        </ModalLeft>
        <ModalRight>
          <img src={welcomeImage} alt="" />
        </ModalRight>
      </AdditionalFilterWrapper>
    </>
  );
};

export default WelcomeModal;

WelcomeModal.propTypes = {
  closeHandler: PropTypes.func,
  setModalType: PropTypes.func,
  setIsMobileEdit: PropTypes.func,
  isEdit: PropTypes.bool,
  disableLoginModalCloseIcon: PropTypes.bool,
  router: PropTypes.object,
  successToast: PropTypes.func,
  errorToast: PropTypes.func,
  setActiveAuthType: PropTypes.func,
  name: PropTypes.string,
  setName: PropTypes.func,
};

const AdditionalFilterWrapper = styled.div`
  width: 710px;
  height: ${props => props.height};
  margin: 5% auto;
  padding: 25px;
  padding-left: 45px;
  border-radius: 10px;
  background: ${colors.white};
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
  position: relative;

  &.welcome {
    position: relative;
    max-width: 828px;
    width: 100%;
    padding: 50px 48px;
    margin: 15% auto 20px;
    display: flex;
    @media (max-width: 1000px) {
      max-width: 750px;
      padding: 50px 30px;
    }
    @media (max-width: 500px) {
      max-width: 320px;
      padding: 50px 20px;
      flex-direction: column;
      overflow: hidden;
    }

    @media (max-width: 390px) {
      max-width: 300px;
    }

    .scheduleTour {
      display: flex;
      flex-direction: column;
      .personalDetail {
        color: #30333b;
        font-family: 'Roboto', sans-serif;
        font-size: 16px;
        line-height: 22px;
        margin-bottom: 20px;
      }
      .tel {
        @media (max-width: 500px) {
          padding: 10px 10px 10px 35px;
          font-size: 12px;
        }
      }
      .number {
        @media (max-width: 500px) {
          font-size: 12px;
          left: 10px;
          top: 9px;
        }
      }
      .checkbox label {
        @media (max-width: 500px) {
          font-size: 12px;
          flex-wrap: wrap;
          a {
            margin: 0 5px;
          }
        }
      }
      .mobile {
        @media (max-width: 500px) {
          height: 46px;
          font-size: 14px;
        }
      }
    }
  }
`;

export const GalleryHeading = styled.div`
  margin: ${props => props.margin || '0px'};
  color: ${props => props.color || '#30333B'};
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 500;
  line-height: 28px;
`;

export const ScheduleDetail = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  border-radius: 10px;
  background-color: #f3f5f9;
  padding: 18px 24px;
  margin-bottom: 10px;
  h2 {
    color: #30333b;
    font-family: 'Quicksand', sans-serif;
    font-size: 16px;
    font-weight: bold;
    line-height: 26px;
    text-align: center;
    margin: 0px;
    span {
      font-weight: 300;
      margin-right: 4px;
    }
  }
`;

const ModalTitle = styled.div`
  margin-bottom: 10px;
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 600;
  line-height: 28px;

  &.creatAccount {
    color: #30333b;
    font-size: 20px;
    // font-weight: 400;
    line-height: 23px;
    @media (max-width: 500px) {
      font-size: 18px;
    }
  }
  &.loginQues {
    font-family: 'Roboto', sans-serif;
    font-weight: 400;
    text-align: center;
    span {
      margin: 0px 5px;
    }
    &__title {
      margin-bottom: 20px;
      font-family: 'Roboto', sans-serif;
      font-weight: 400;
      text-align: center;
      font-size: 16px;
      line-height: 21px;
    }
  }
`;

const ModalSubTitle = styled.div`
  margin-left: 5px;
  margin-bottom: 20px;
  color: #30333b;
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 19px;
`;
const CloseModal = styled.div`
  position: absolute;
  top: 20px;
  right: 20px;
  cursor: pointer;
  @media screen and (max-width: 768px) {
    top: 1px;
    right: 7px;
  }
  .iconify {
    height: 12px;
    width: 12px;
    color: #666c78;
  }
`;

const ModalRight = styled.div`
  position: absolute;
  left: 0px;
  bottom: 0px;
  @media (max-width: 500px) {
    position: initial;
    margin-top: -39px;
    margin-bottom: -101px;
    margin-left: -73px;
    transform: scale(0.7);
  }
`;

const ModalLeft = styled.div`
  margin-left: auto;
  .alreadySigned {
    color: #666c78;
    font-family: Roboto;
    font-size: 14px;
    letter-spacing: 0;
    line-height: 14px;
    font-weight: 400;
    text-align: center;
    @media (max-width: 500px) {
      font-size: 12px;
    }
    span {
      color: #613a95;
      margin-left: 5px;
      cursor: pointer;
    }
  }
  @media (max-width: 500px) {
    margin-left: 0px;
  }
`;
