import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Button from '../Button';
import colors from '../../utils/colors';
import InputBox from '../InputBox';

const EmailModal = ({ closeHandler, emailType = 'profile', email }) => {
  const currentRef = useRef();

  return (
    <AdditionalFilterWrapper className="tour" ref={currentRef}>
      <CloseModal onClick={closeHandler}>
        {' '}
        <span
          className="iconify"
          data-icon="bytesize:close"
          data-inline="false"
        />
      </CloseModal>

      {emailType === 'profile' ? (
        <>
          <GalleryHeading margin="0px 0px 20px">
            E-mail Verification
          </GalleryHeading>
          <OrgName margin="0px 0px 24px">
            Please click on the link sent to your email address{' '}
            <span>{email}</span> to verify your email address.
          </OrgName>
          <div className="scheduleTour">
            <Button
              text="Ok, Got it."
              type="mobile"
              marginRight="10px"
              onClick={closeHandler}
            />
          </div>
        </>
      ) : (
        <>
          <GalleryHeading margin="0px 0px 10px">
            E-mail Verification
          </GalleryHeading>
          <OrgName margin="0px 0px 20px">
            We will send a verification mail to your E-mail ID
          </OrgName>
          <div className="scheduleTour">
            <InputBox name="Email Id" type="text" />
            <Button text="Send Verification" type="mobile" marginRight="10px" />
          </div>
        </>
      )}
    </AdditionalFilterWrapper>
  );
};

EmailModal.propTypes = {
  closeHandler: PropTypes.func,
  emailType: PropTypes.string,
  email: PropTypes.string,
};

export default EmailModal;

const AdditionalFilterWrapper = styled.div`
  width: 710px;
  height: ${props => props.height};
  margin: 5% auto;
  padding: 25px;
  padding-left: 45px;
  border-radius: 10px;
  background: ${colors.white};
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
  position: relative;

  &.tour {
    max-width: 500px;
    width: 100%;
    padding: 50px 48px;
    margin: 20% auto;
    @media screen and (max-width: 768px) {
      max-width: 290px;
      width: 100%;
      padding: 20px;
      margin: 20% auto;
    }
    .scheduleTour {
      display: flex;
      flex-direction: column;
      .personalDetail {
        color: #30333b;
        font-family: 'Roboto', sans-serif;
        font-size: 16px;
        line-height: 22px;
        margin-bottom: 20px;
      }
    }
  }
`;

export const GalleryHeading = styled.div`
  margin: ${props => props.margin || '0px'};
  color: ${props => props.color || '#30333B'};
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 500;
  line-height: 28px;
`;

const CloseModal = styled.div`
  position: absolute;
  top: 20px;
  right: 20px;
  cursor: pointer;
  @media screen and (max-width: 768px) {
    top: 1px;
    right: 7px;
  }
  .iconify {
    height: 12px;
    width: 12px;
    color: #666c78;
  }
`;

const OrgName = styled.div`
  color: #30333b;
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 19px;
  font-weight: 400;
  margin: ${props => props.margin || '0px'};
  span {
    font-weight: 600;
  }
`;
