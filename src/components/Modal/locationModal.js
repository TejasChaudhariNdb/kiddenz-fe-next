import React from 'react';
import styled from 'styled-components';
// import PropTypes from 'prop-types';
import Flex from '../common/flex';
import InputBox from '../InputBox';
import locationImage from '../../images/Kiddenz_icon-location-06.svg';

const LocationModal = () => (
  <AdditionalFilterWrapper className="tour">
    <Flex column flexHeight="fit-content">
      <ModalTitle>What is your preferred location?</ModalTitle>
      <ModalSubInfo>To suggest preschools near your location</ModalSubInfo>
      <ModalLoginImage>
        <img src={locationImage} alt="" />
      </ModalLoginImage>
      <Flex column>
        <InputBox
          placeholder="Enter Your Location"
          type="text"
          name="pincode"
        />
        <StateList>
          {' '}
          <li>Delhi</li>
          <li>Mumbai</li>
          <li>Bangalore</li>
          <li>Kochi</li>
          <li>Delhi</li>
          <li>Mumbai</li>
          <li>Bangalore</li>
          <li>Kochi</li>
          <li>Mumbai</li>
        </StateList>
      </Flex>
    </Flex>
  </AdditionalFilterWrapper>
);
LocationModal.propTypes = {};
export default LocationModal;

export const Mainheading = styled.div``;

const AdditionalFilterWrapper = styled.div`
  width: 710px;
  height: ${props => props.height};
  max-height: fit-content;
  margin: 5% auto;
  padding: 25px;
  padding-left: 45px;
  border-radius: 10px;
  background: #fffl
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
  position: relative;

  &.tour {
    max-width: 500px;
    width: 100%;
    padding: 38px 48px;
    margin: 5% auto 20px;
    @media screen and (max-width: 768px) {
      max-width: 290px;
      width: 100%;
      padding: 20px;
      margin: 20% auto;
    }
    @media (max-width: 500px) {
      max-width: 330px;
      padding: 38px 16px;
    }
 

`;

const ModalTitle = styled.div`
  margin-bottom: 30px;
  font-family: 'Quicksand', sans-serif;
  font-size: 20px;
  font-weight: 400;
  line-height: 28px;
  color: #30333b;
`;

const ModalLoginImage = styled.div`
  width: 104px;
  height: 104px;
  margin: 30px auto 30px;
  min-height: 104px;
  @media (max-width: 500px) {
    margin: 0px auto 30px;
  }
  img {
    width: 100%;
    height: 100%;
  }
`;

const ModalSubInfo = styled.div`
  font-family: Roboto;
  font-size: 18px;
  letter-spacing: 0;
  line-height: 26px;
  text-align: center;
  opacity: 0.5;
  @media (max-width: 500px) {
    font-size: 14px;
    line-height: 20px;
    margin-bottom: 30px;
  }
`;

const StateList = styled.div`
  display: flex;
  align-items: center;
  flex-wrap: wrap;
  li {
    display: flex;
    align-items: center;
    justify-content: center;
    height: 35px;
    border: 1px solid #613a95;
    border-radius: 5px;
    padding: 0px 14px;
    font-weight: 400;
    font-size: 16px;
    line-height: 20px;
    color: #613a95;
    font-family: 'Quicksand', sans-serif;
    margin: 0px 12px 12px 0px;
    cursor: pointer;
    &:hover {
      background: #613a95;
      color: #613a95;
    }
  }
`;
