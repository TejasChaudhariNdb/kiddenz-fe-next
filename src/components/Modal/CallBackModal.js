import React, {
  useRef,
  // useState
} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import welcomeImage from 'images/mascot1.png';
// import Button from '../Button';
// import colors from '../../utils/colors';
// import InputBox from '../InputBox';

const CallBackModal = ({ closeHandler }) => {
  const currentRef = useRef();
  return (
    <AdditionalFilterWrapper className="fixed" ref={currentRef}>
      <CloseModal onClick={closeHandler}>
        {' '}
        <span
          className="iconify"
          data-icon="bytesize:close"
          data-inline="false"
        />
      </CloseModal>
      <ModalLeft>
        <ModalTitle className="creatAccount">
          Thank you for your interest
        </ModalTitle>
        <ModalSubTitle>
          Our team will get in touch with you shortly
        </ModalSubTitle>
      </ModalLeft>
      <ModalRight>
        <img src={welcomeImage} alt="" height={215} />
      </ModalRight>
    </AdditionalFilterWrapper>
  );
};

CallBackModal.propTypes = {
  closeHandler: PropTypes.func,
  // ONLINE_PROGRAM_CALLBACK_REQUEST_API_CALL: PropTypes.func,
  // programId: PropTypes.number,
  // errorToast: PropTypes.func,
  // successToast: PropTypes.func,
};

export default CallBackModal;

const AdditionalFilterWrapper = styled.div`
  width: 710px;
  margin: 5% auto;
  padding: 19px 8% 0px 0;
  background: #ffffff;
  box-shadow: 0 0 10px 0 rgb(0 0 0 / 10%);
  position: relative;
  display: flex;
  flex-direction: row-reverse;
  align-items: center;
  justify-content: space-between;
  border-radius: 5px;
  @media screen and (max-width: 768px) {
    max-width: 710px;
    margin: 5%;
  }
  @media screen and (max-width: 500px) {
    flex-direction: column;
    align-items: flex-start;
    padding: 20px 20px 0 0;
  }
  &.tour {
    max-width: 500px;
    width: 100%;
    padding: 50px 48px;
    margin: 5% auto 20px;
    @media screen and (max-width: 768px) {
      max-width: 290px;
      width: 100%;
      padding: 20px;
      margin: 20% auto;
    }
    .scheduleTour {
      display: flex;
      flex-direction: column;
    }
    @media (max-width: 768px) {
      max-width: 380px !important;
      width: 100% !important;
      padding: 50px 20px;
    }
    @media (max-width: 380px) {
      max-width: 320px !important;
    }
    @media (max-width: 330px) {
      max-width: 300px !important;
    }
  }
`;

export const GalleryHeading = styled.div`
  margin: ${props => props.margin || '0px'};
  color: ${props => props.color || '#30333B'};
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 500;
  line-height: 28px;
  @media (max-width: 768px) {
    font-size: 16px;
    line-height: 25px;
    margin-bottom: 30px;
  }
`;
const ModalRight = styled.div`
  // position: absolute;
  left: 0px;
  bottom: 0px;
  img {
    @media (max-width: 768px) {
      position: initial;
      margin-bottom: -33px;
      margin-left: -34px;
      transform: scale(0.7);
    }
  }
`;

const CloseModal = styled.div`
  position: absolute;
  top: 20px;
  right: 20px;
  cursor: pointer;
  @media screen and (max-width: 768px) {
    top: 1px;
    right: 7px;
  }
  .iconify {
    height: 12px;
    width: 12px;
    color: #666c78;
  }
`;

// const OrgName = styled.div`
//   color: #30333b;
//   font-family: 'Roboto', sans-serif;
//   font-size: 14px;
//   line-height: 19px;
//   margin: ${props => props.margin || '0px'};
// `;

const ModalLeft = styled.div`
  margin-left: auto;
  max-width: 66%;
  .alreadySigned {
    color: #666c78;
    font-family: Roboto;
    font-size: 14px;
    letter-spacing: 0;
    line-height: 14px;
    font-weight: 400;
    text-align: center;
    @media (max-width: 500px) {
      font-size: 12px;
    }
    span {
      color: #613a95;
      margin-left: 5px;
      cursor: pointer;
    }
  }
  @media (max-width: 500px) {
    margin: 0px auto;
    max-width: 100%;
    padding-left: 20px;
    padding-top: 20px;
  }
`;

const ModalTitle = styled.div`
  margin-bottom: 10px;
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 600;
  line-height: 28px;

  &.creatAccount {
    color: #30333b;
    font-size: 20px;
    // font-weight: 400;
    line-height: 23px;
    @media (max-width: 500px) {
      font-size: 18px;
      text-align: center;
    }
  }
  &.loginQues {
    font-family: 'Roboto', sans-serif;
    font-weight: 400;
    text-align: center;
    span {
      margin: 0px 5px;
    }
    &__title {
      margin-bottom: 20px;
      font-family: 'Roboto', sans-serif;
      font-weight: 400;
      text-align: center;
      font-size: 16px;
      line-height: 21px;
    }
  }
`;

const ModalSubTitle = styled.div`
  margin-left: 5px;
  margin-bottom: 20px;
  color: #30333b;
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 19px;
  @media (max-width: 500px) {
    text-align: center;
  }
`;

export const ScheduleDetail = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  border-radius: 10px;
  background-color: #f3f5f9;
  padding: 18px 24px;
  margin-bottom: 10px;
  h2 {
    color: #30333b;
    font-family: 'Quicksand', sans-serif;
    font-size: 16px;
    font-weight: bold;
    line-height: 26px;
    text-align: center;
    margin: 0px;
    span {
      font-weight: 300;
      margin-right: 4px;
    }
  }
`;

// const Error = styled.div`
//   font-size: 12px;
//   color: red;
// `;
