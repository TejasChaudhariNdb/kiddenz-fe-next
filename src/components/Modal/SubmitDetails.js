/* eslint-disable no-nested-ternary */
/* eslint-disable no-constant-condition */
import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useProviderHook } from 'shared/hooks';
import Button from '../Button';
import RadioButton, { Radio } from '../RadioButton/index';
import colors from '../../utils/colors';
import InputBox from '../InputBox';
import Flex from '../common/flex';

const SubmitDetailsModal = ({
  closeHandler,
  successToast,
  errorToast,
  ...props
}) => {
  const onPostSuccess = () => {
    successToast('Successfully submitted.');
    closeHandler();
  };
  const onPostError = ({ message }) =>
    errorToast(message || 'Something went wrong.');

  const {
    onChange,
    onBlur,
    onSubmit,
    formValues,
    error,
    loader,
  } = useProviderHook(props, {
    onPostSuccess,
    onPostError,
  });

  return (
    <AdditionalFilterWrapper className="addChild">
      <CloseModal onClick={closeHandler}>
        {' '}
        <span
          className="iconify"
          data-icon="bytesize:close"
          data-inline="false"
        />
      </CloseModal>
      <GalleryHeading margin="0px 0px 25px">Add details</GalleryHeading>

      <ProviderForm>
        <Flex
          justifyBetween
          flexMargin="30px 0px"
          wrap
          className="childrenDetails"
        >
          <Flex column flexWidth="48%" className="addChild-input">
            <p>Contact person name*</p>
            <InputBox
              name="Contact person name"
              type="text"
              margin="0px 0px 20px 0px"
              placeholder="Contact person name"
              onChange={e => onChange(e, 'name')}
              onBlur={e => onBlur(e, 'name')}
              error={error.name}
            />
          </Flex>
          <Flex column flexWidth="48%" className="addChild-input">
            <p>Name of Preschool/Daycare/Extracurricular Programs*</p>
            <InputBox
              name="Name of Preschool"
              type="text"
              margin="0px 0px 20px 0px"
              placeholder="Name of Preschool"
              onChange={e => onChange(e, 'business_name')}
              onBlur={e => onBlur(e, 'business_name')}
              error={error.business_name}
            />
          </Flex>
        </Flex>
        <Flex
          justifyBetween
          flexMargin="30px 0px"
          wrap
          className="childrenDetails"
        >
          <Flex column flexWidth="48%" className="addChild-input">
            <p>Location Address*</p>

            <InputBox
              name="Location Address"
              type="text"
              margin="0px 0px 20px 0px"
              placeholder="Location Address"
              onChange={e => onChange(e, 'location_address')}
              onBlur={e => onBlur(e, 'location_address')}
              error={error.location_address}
            />
          </Flex>
          <Flex column flexWidth="48%" className="addChild-input">
            <p>City*</p>

            <InputBox
              name="City"
              type="text"
              margin="0px 0px 20px 0px"
              placeholder="City"
              onChange={e => onChange(e, 'city')}
              onBlur={e => onBlur(e, 'city')}
              error={error.city}
            />
          </Flex>
        </Flex>
        <Flex
          justifyBetween
          flexMargin="30px 0px"
          wrap
          className="childrenDetails"
        >
          <Flex column flexWidth="48%" className="addChild-input">
            <p>State*</p>

            <InputBox
              name="State"
              type="text"
              margin="0px 0px 20px 0px"
              placeholder="State"
              onChange={e => onChange(e, 'state')}
              onBlur={e => onBlur(e, 'state')}
              error={error.state}
            />
          </Flex>
          <Flex column flexWidth="48%" className="addChild-input">
            <p>Pincode*</p>

            <InputBox
              name="Pincode"
              type="text"
              margin="0px 0px 20px 0px"
              placeholder="Pincode"
              onChange={e => onChange(e, 'pincode', 'number')}
              onBlur={e => onBlur(e, 'pincode')}
              error={error.pincode}
            />
          </Flex>
        </Flex>
        <Flex
          justifyBetween
          flexMargin="30px 0px"
          wrap
          className="childrenDetails"
        >
          <Flex column flexWidth="48%" className="addChild-input">
            <p>Mobile Number*</p>

            <InputBox
              name="Mobile Number"
              type="tel"
              margin="0px 0px 20px 0px"
              placeholder="Mobile Number"
              onChange={e => onChange(e, 'phone_number', 'number')}
              onBlur={e => onBlur(e, 'phone_number')}
              error={error.phone_number}
            />
          </Flex>
          <Flex column flexWidth="48%" className="addChild-input">
            <p>Alternate number</p>

            <InputBox
              name="Alternate number"
              type="tel"
              margin="0px 0px 20px 0px"
              placeholder="Alternate number"
              onChange={e => onChange(e, 'additional_phone_number', 'number')}
              onBlur={e => onBlur(e, 'additional_phone_number')}
              error={error.additional_phone_number}
            />
          </Flex>
        </Flex>
        <Flex
          justifyBetween
          flexMargin="30px 0px"
          wrap
          className="childrenDetails"
        >
          <Flex column flexWidth="48%" className="addChild-input">
            <p>Email*</p>

            <InputBox
              name="Email"
              type="text"
              margin="0px 0px 20px 0px"
              placeholder="Email"
              onChange={e => onChange(e, 'email')}
              onBlur={e => onBlur(e, 'email')}
              error={error.email}
            />
          </Flex>
          <Flex column flexWidth="48%" className="addChild-input" />
        </Flex>
        <Flex column className="addChild-input">
          <p>Do you have website ?</p>
          <Flex className="addChild-check" flexMargin="0px 0px 40px">
            <RadioButton
              text="Yes"
              // checkedColor="#fff"
              filterRadio="#666C78"
              RadioMargin="0px 106px 0px 0px"
              checked={formValues.website === true}
              onClick={e => onChange(e, 'website', 'radio', true)}
            />
            <RadioButton
              text="No"
              filterRadio="#666C78"
              checked={formValues.website === false}
              onClick={e => onChange(e, 'website', 'radio', false)}
            />
          </Flex>
          <p>Is you business available on google ? </p>
          <Flex flexMargin="0px 0px 40px" className="addChild-check">
            <RadioButton
              text="Yes"
              // checkedColor="#fff"
              filterRadio="#666C78"
              RadioMargin="0px 98px 0px 0px"
              checked={formValues.google_list === true}
              onClick={e => onChange(e, 'google_list', 'radio', true)}
            />
            <RadioButton
              text="No"
              // checkedColor="#fff"
              filterRadio="#666C78"
              RadioMargin="0px 98px 0px 0px"
              checked={formValues.google_list === false}
              onClick={e => onChange(e, 'google_list', 'radio', false)}
            />
          </Flex>

          <p>How did you hear about kiddenz</p>
          <Flex
            flexMargin="0px 0px 40px"
            className="addChild-check abtKiddenz"
            wrap
          >
            <RadioButton
              text="Saw on google"
              // checkedColor="#fff"
              filterRadio="#666C78"
              RadioMargin="0px 20px 0px 0px"
              checked={formValues.hear_about_kiddenz === 'Saw on google'}
              onClick={e =>
                onChange(e, 'hear_about_kiddenz', 'radio', 'Saw on google')
              }
            />
            <RadioButton
              text="Marketing officer from Kiddenz contacted us"
              // checkedColor="#fff"
              filterRadio="#666C78"
              RadioMargin="0px 20px 0px 0px"
              checked={
                formValues.hear_about_kiddenz ===
                'Marketing officer from Kiddenz contacted us'
              }
              onClick={e =>
                onChange(
                  e,
                  'hear_about_kiddenz',
                  'radio',
                  'Marketing officer from Kiddenz contacted us',
                )
              }
            />
            <RadioButton
              text="Email from Kiddenz"
              // checkedColor="#fff"
              filterRadio="#666C78"
              RadioMargin="0px 20px 0px 0px"
              checked={formValues.hear_about_kiddenz === 'Email from Kiddenz'}
              onClick={e =>
                onChange(e, 'hear_about_kiddenz', 'radio', 'Email from Kiddenz')
              }
            />
            <RadioButton
              text="Word of mouth"
              // checkedColor="#fff"
              filterRadio="#666C78"
              RadioMargin="0px 20px 0px 0px"
              checked={formValues.hear_about_kiddenz === 'Word of mouth'}
              onClick={e =>
                onChange(e, 'hear_about_kiddenz', 'radio', 'Word of mouth')
              }
            />
            <RadioButton
              text="Facebook or other social media"
              // checkedColor="#fff"
              filterRadio="#666C78"
              checked={
                formValues.hear_about_kiddenz ===
                'Facebook or other social media'
              }
              onClick={e =>
                onChange(
                  e,
                  'hear_about_kiddenz',
                  'radio',
                  'Facebook or other social media',
                )
              }
            />
          </Flex>

          <p>Anything you would like to mention ?</p>
          <Flex flexMargin="0px 0px 40px" className="addChild-check">
            <InputBox
              type="textarea"
              margin="0px"
              rows={4}
              onChange={e => onChange(e, 'misc_info')}
              onBlur={e => onBlur(e, 'misc_info')}
              error={error.misc_info}
            />
          </Flex>
        </Flex>
      </ProviderForm>
      <Flex justifyEnd className="btnWrapper">
        <Button
          type="subscribe"
          text="Cancel"
          marginRight="20px"
          onClick={closeHandler}
        />
        <Button
          type="mobile"
          text="Submit"
          onClick={onSubmit}
          isLoading={loader}
        />
      </Flex>
    </AdditionalFilterWrapper>
  );
};

SubmitDetailsModal.propTypes = {
  closeHandler: PropTypes.func,
  successToast: PropTypes.func,
  errorToast: PropTypes.func,
};

// SubmitDetailsModal.propTypes = {
//   closeHandler: PropTypes.func,
//   setModalType: PropTypes.func,
//   successToast: PropTypes.func,
//   errorToast: PropTypes.func,
//   isChildEdit: PropTypes.bool,
//   editChildId: PropTypes.number,
//   router: PropTypes.object,
// };

export default SubmitDetailsModal;

const ProviderForm = styled.div`
  // @media (max-width: 768px) {
  height: calc(90vh - 171px);
  overflow: scroll;
  overflow-x: hidden;
  // }
`;

const AdditionalFilterWrapper = styled.div`
  width: 710px;
  height: ${props => props.height};
  margin: 5% auto;
  padding: 45px;
  border-radius: 10px;
  background: ${colors.white};
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
  position: relative;
  &.addChild {
    width: 800px;

    @media (max-width: 768px) {
      max-width: 340px;
      width: 100%;
      padding: 20px;
    }
    @media (max-width: 380px) {
      max-width: 320px;
    }
    @media (max-width: 330px) {
      max-width: 300px;
    }
  }
  .addChild-input {
    @media (max-width: 768px) {
      margin-bottom: 0px;
      width: 100%;
    }

    p {
      color: #666c78;
      font-family: 'Roboto', sans-serif;
      font-size: 14px;
      line-height: 16px;
      margin: 0px 0px 10px;
    }
    .tel {
      @media (max-width: 768px) {
        padding: 12px 12px 12px 40px !important;
      }
    }
    .number {
      @media (max-width: 768px) {
        top: 12px;
        font-size: 13px;
      }
    }
    input {
      @media (max-width: 768px) {
        padding: 12px;
      }
    }
  }
  .addChild-check {
    &.abtKiddenz {
      input[type='radio']:not(:checked) + label,
      input[type='radio']:checked + label {
        white-space: normal !important;
      }
    }
    .checkbox {
      width: 20%;
      @media (max-width: 768px) {
        width: 50%;
      }
    }
    ${Radio} {
      @media (max-width: 768px) {
        width: 100%;
      }
      label {
        @media (max-width: 768px) {
          //   white-space: normal !important;
        }
      }
    }
    input[type='radio']:not(:checked) + label,
    input[type='radio']:checked + label {
      color: #666c78;
      font-family: 'Roboto', sans-serif;
      font-size: 14px;
      line-height: 25px;
      font-weight: 400;
    }
  }

  .childrenDetails {
    @media (max-width: 768px) {
      margin: 0px;
    }
  }
  .btnWrapper {
    margin-top: 30px;
    @media (max-width: 768px) {
      .mobile {
        padding: 10px 12px;
        height: 40px;
        font-size: 14px;
      }
      .subscribe {
        padding: 10px 12px;
        height: 40px;
        font-size: 14px;
      }
    }
  }
`;

export const GalleryHeading = styled.div`
  margin: ${props => props.margin || '0px'};
  color: ${props => props.color || '#30333B'};
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 500;
  line-height: 28px;
  @media (max-width: 768px) {
    font-size: 18px;
    line-height: 24px;
  }
`;

const CloseModal = styled.div`
  position: absolute;
  top: 20px;
  right: 20px;
  cursor: pointer;
  @media screen and (max-width: 768px) {
    top: 1px;
    right: 7px;
  }
  .iconify {
    height: 12px;
    width: 12px;
    color: #666c78;
  }
`;
