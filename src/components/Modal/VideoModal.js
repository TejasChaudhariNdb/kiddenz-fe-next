/* eslint-disable jsx-a11y/media-has-caption */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Flex from '../common/flex';

const VideoModal = ({ closeHandler }) => {
  const currentRef = useRef();

  return (
    <>
      <div className="gallerySection">
        <div
          className="closeGallery"
          onClick={() => {
            closeHandler();
          }}
        >
          <span
            className="iconify"
            data-icon="bytesize:close"
            data-inline="false"
          />
        </div>
        <div className="galleryContent" ref={currentRef}>
          <div className="galleryContainer">
            <video width="100%" autoPlay>
              <source
                src="https://s3.ap-south-1.amazonaws.com/dev.maplytiks.org/api/v1/general/website/primary.mp4"
                type="video/mp4"
              />
            </video>
          </div>
        </div>
      </div>
    </>
  );
};

VideoModal.propTypes = {
  closeHandler: PropTypes.func,
};

export default VideoModal;

export const NavBar = styled.div`
  display: flex;
  color: #ffffff;
  padding: 10px 0px;
  border-bottom: 1px solid #e6e8ec;
  li {
    color: #ffffff;
    font-family: 'Roboto', sans-serif;
    font-size: 14px;
    line-height: 22px;
    list-style-type: none;
    margin-right: 20px;
    position: relative;
    cursor: pointer;
    &:hover::after,
    &.active::after {
      content: '';
      position: absolute;
      border-bottom: 1px solid #e6e8ec;
      width: 100%;
      height: 2px;
      background-color: #fff;
      left: 0px;
      bottom: -10px;
    }
  }
`;

export const GalleryHeading = styled.div`
  margin: ${props => props.margin || '0px'};
  color: ${props => props.color || '#30333B'};
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 500;
  line-height: 28px;
`;

export const TeacherWrapper = styled(Flex)`
  @media (max-width: 414px) {
    flex-direction: column;
  }
`;

export const OurStaff = styled.div`
  padding: 42px 0px;
  border-top: 1px solid #e6e8ec;
  border-bottom: 1px solid #e6e8ec;

  margin-top: 20px;
  .ourStaff {
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
  }
`;

export const OurSecurity = styled.div`
  padding: 42px 0px;
  .ourStaff {
    display: flex;
    flex-wrap: wrap;
  }
`;

export const HandbookHeading = styled.div`
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 500;
  line-height: 28px;
`;

export const JumboImage = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 86px;
  width: 86px;
  border-radius: 50%;
  border: 3px solid #fff;
  background-color: rgba(255, 238, 79, 1);
  margin-right: 30px;
  & > img {
    width: 90%;
    height: 70%;
  }
`;

export const HandbookTitle = styled.div`
  color: #30333b;
  font-family: 'Quicksand', sans-serif;
  font-size: 20px;
  font-weight: bold;
  line-height: 25px;
  margin: ${props => props.margin || '0px'};
`;

export const HandBookBtn = styled.div`
  padding: 18px 20px;
  border: 1px solid #666c78;
  border-radius: 5px;
  background-color: #ffffff;
  color: #30333b;
  font-family: 'Quicksand', sans-serif;
  font-size: 16px;
  font-weight: 500;
  line-height: 20px;
`;

export const ListHeading = styled.div`
  color: #30333b;
  font-family: 'Quicksand', sans-serif;
  font-size: 18px;
  font-weight: bold;
  line-height: 23px;
  margin-bottom: 18px;
`;

export const ListDesc = styled.div`
  color: #666c78;
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 21px;
  padding-right: 70px;
`;
