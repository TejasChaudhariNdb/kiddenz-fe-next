import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import styled from 'styled-components';
import { useScheduleHook } from 'shared/hooks';
import Button from '../Button';
import colors from '../../utils/colors';
import Flex from '../common/flex';

import DatePicker from '../DatePicker';

const TourModal = ({
  closeHandler,
  setModalType,
  setActive,
  dayCareName,
  dayCareArea,
  authentication: { profile: { data: { name = '' } = {} } = {} } = {},
  providerId,
  errorToast,
  successToast,
  setScheduleRes,
  isForceSchedule = false,
  setIsForceSchedule = () => {},
  ...props
}) => {
  const currentRef = useRef();
  const onScheduleAddError = ({ message }) => {
    errorToast(message);
  };

  const onScheduleAddSuccess = res => {
    successToast('Successfully Added.');
    setModalType('confirm message');
    setScheduleRes(res);
  };

  const {
    onChange,
    error,
    onScheduleSubmit,
    date,
    time,
    setProviderId,
    addScheduleLoader,
  } = useScheduleHook(props, { onScheduleAddSuccess, onScheduleAddError });

  React.useEffect(
    () => {
      setProviderId(providerId);
    },
    [providerId],
  );

  return (
    <>
      <AdditionalFilterWrapper className="tour" ref={currentRef}>
        <CloseModal
          onClick={() => {
            closeHandler();
            setIsForceSchedule(false);
            setProviderId(null);
          }}
        >
          {' '}
          <span
            className="iconify"
            data-icon="bytesize:close"
            data-inline="false"
          />
        </CloseModal>
        <div className="scheduleTour">
          <GalleryHeading margin="0px 0px 26px">Schedule a tour</GalleryHeading>
          <OrgName margin="0px 0px 36px">
            Hi {name},<br />A great choice. We are glad we could help. Sharing
            some details about your child will help {`"${dayCareName}"`} prepare
            for your visit.
            {/* <br />
            <br />
            Please select the child whose information should be shared. */}
          </OrgName>

          <div className="childInfo">
            {/* <SimpleBar style={{ maxHeight: '208px' }}>
              {childList.length > 0 ? (
                childList.map(d => (
                  <ChildInfo
                    name={`${d.first_name} ${d.last_name}`}
                    age={`${d.age} years`}
                    gender={d.gender_type}
                    image={d.gender_type === 'male' ? maleKid : femaleKid}
                    selectedChildren={selectedChildren}
                    addChildToSchedule={addChildToSchedule}
                    index={d.id}
                  />
                ))
              ) : (
                <Flex justifyCenter>
                  <ExploreArticle width="30%">
                    <IconCard iconName="fa-solid:child" type="favourite" />
                    <h4>Nothing added yet</h4>
                  </ExploreArticle>
                </Flex>
              )}
            </SimpleBar> */}

            {/* <div
              style={{
                display: 'flex',
                justifyContent: 'flex-end',
                marginTop: '13px',
              }}
            >
              <Flex justifyBetween flexWidth="100%">
                <Error>{error.child}</Error>
                <Button
                  text="Add new Child"
                  type="viewAll"
                  onClick={() => setModalType('addChild')}
                />
              </Flex>
            </div> */}
            <OrgName margin="50px 0px 20px">
              Please select the date and time of your visit
              <br />
              to {dayCareName}, {dayCareArea}
            </OrgName>
            <Flex
              flexMargin="0px 0px 20px"
              justifyBetween
              className="datePickerContent"
            >
              <Flex column>
                <DatePicker
                  value={date ? moment(date).format('DD/MM/YYYY') : ''}
                  iconName="feather:calendar"
                  placeholder="Select Date"
                  onChange={e =>
                    onChange('date', moment(e).format('YYYY-MM-DD'))
                  }
                />
                <Error>{error.date}</Error>
              </Flex>
              <Flex column>
                <DatePicker
                  value={time}
                  iconName="bytesize:clock"
                  placeholder="Select Time"
                  isTime
                  onChange={e => onChange('time', moment(e).format('HH:mm'))}
                />
                <Error>{error.time}</Error>
              </Flex>
            </Flex>
          </div>
          <Button
            text="Confirm"
            type="mobile"
            onClick={() => onScheduleSubmit(providerId, isForceSchedule)}
            isLoading={addScheduleLoader}
          />
        </div>
      </AdditionalFilterWrapper>
    </>
  );
};

TourModal.propTypes = {
  closeHandler: PropTypes.func,
  setModalType: PropTypes.func,
  authentication: PropTypes.object,
  dashboard: PropTypes.object,
  dayCareName: PropTypes.string,
  dayCareArea: PropTypes.string,
  providerId: PropTypes.number,
  errorToast: PropTypes.func,
  successToast: PropTypes.func,
  setActive: PropTypes.func,
  setScheduleRes: PropTypes.func,
  isForceSchedule: PropTypes.bool,
  setIsForceSchedule: PropTypes.func,
};

export default TourModal;

const AdditionalFilterWrapper = styled.div`
  width: 710px;
  height: ${props => props.height};
  margin: 5% auto;
  padding: 25px;
  padding-left: 45px;
  border-radius: 10px;
  background: ${colors.white};
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
  position: relative;

  &.tour {
    max-width: 500px;
    width: 100%;
    padding: 50px 48px;
    margin: 5% auto 20px;
    @media screen and (max-width: 768px) {
      max-width: 290px;
      width: 100%;
      padding: 20px;
      margin: 20% auto;
    }
    .scheduleTour {
      display: flex;
      flex-direction: column;
      .personalDetail {
        color: #30333b;
        font-family: 'Roboto', sans-serif;
        font-size: 16px;
        line-height: 22px;
        margin-bottom: 20px;
      }
    }
  }
  .confirmMessage {
    //   display:flex;
    //   flex-direction:column;
    //   align-items:center;
    text-align: center;
  }
  .datePickerContent {
    @media (max-width: 500px) {
      flex-direction: column;
    }
    .datepick {
      @media (max-width: 500px) {
        height: 40px;
        margin-bottom: 10px;
      }
    }
  }
  @media (max-width: 500px) {
    padding: 50px 20px !important;
    max-width: 380px !important;
  }
  @media (max-width: 400px) {
    max-width: 340px;
  }
  @media (max-width: 330px) {
    max-width: 300px;
  }
`;

export const GalleryHeading = styled.div`
  margin: ${props => props.margin || '0px'};
  color: ${props => props.color || '#30333B'};
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 500;
  line-height: 28px;
`;

const CloseModal = styled.div`
  position: absolute;
  top: 20px;
  right: 20px;
  cursor: pointer;
  @media screen and (max-width: 768px) {
    top: 1px;
    right: 7px;
  }
  .iconify {
    height: 12px;
    width: 12px;
    color: #666c78;
  }
`;

const OrgName = styled.div`
  color: #30333b;
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 19px;
  margin: ${props => props.margin || '0px'};
`;

// const ChildWrapper = styled.div`
//   max-height: 208px;
//   overflow: scroll;
// `;

const Error = styled.div`
  font-size: 12px;
  color: red;
`;
