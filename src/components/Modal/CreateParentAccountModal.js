import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Button from '../Button';
import colors from '../../utils/colors';
import InputBox from '../InputBox';
import Flex from '../common/flex';
import CheckBox from '../Checkbox';
import { useRegisterHook } from '../../shared/hooks';

const CreateParentAccountModal = ({
  closeHandler,
  setModalType,
  isEdit,
  setIsMobileEdit,
  setActiveAuthType,
  ...props
}) => {
  const currentRef = useRef();

  const onError = ({ message }) => {
    props.errorToast(message || 'Something went wrong,');
  };

  const onSuccess = () => {
    props.successToast('OTP sent successfully.');
    setModalType('verifyMobile');
  };

  const {
    name: { value: name },
    mobile: { value: mobileNo },
    email: { value: emailAddress },
    subscription: { onChange: onChangeSubscribeEmail, value: isSubscribed },
    onChange,
    onBlur,
    error,
    onSubmit,
    registerLoader,
  } = useRegisterHook(props, { onError, onSuccess, isEdit });

  const onEnterButtonPressed = e => {
    if (e.key === 'Enter') {
      e.preventDefault();
      setActiveAuthType();
      onSubmit();
    }
  };

  return (
    <>
      <AdditionalFilterWrapper className="tour" ref={currentRef}>
        {/* <CloseModal onClick={closeHandler}>
          {' '}
          <span
            className="iconify"
            data-icon="bytesize:close"
            data-inline="false"
          />
        </CloseModal> */}

        <ModalTitle className="creatAccount">Allow us to help you</ModalTitle>
        <div className="scheduleTour">
          <InputBox
            name="name"
            placeholder="Your Name"
            type="text"
            margin="0px 0px 20px 0px"
            value={name}
            onChange={onChange}
            onBlur={onBlur}
            error={error.name}
            onKeyPress={onEnterButtonPressed}
          />
          <InputBox
            name="mobile"
            placeholder="0000000000"
            type="tel"
            margin="0px 0px 20px 0px"
            value={mobileNo}
            onChange={onChange}
            onBlur={onBlur}
            error={error.mobile}
            onKeyPress={onEnterButtonPressed}
          />
          <InputBox
            name="email"
            type="text"
            placeholder="Email Address"
            margin="0px 0px 20px 0px"
            value={emailAddress}
            onChange={onChange}
            onBlur={onBlur}
            error={error.email}
            onKeyPress={onEnterButtonPressed}
          />
          <Flex flexMargin="0px 0px 20px 0px">
            <SpanText>By signing up you agree to</SpanText>
            <TermsLink>Terms of use</TermsLink>
            <SpanText> and </SpanText>
            <TermsLink>Privacy Policy</TermsLink>
          </Flex>

          <CheckBox
            label="I would like to subscribe for the articles and receive updates from Kiddenz"
            id="agree1"
            margin="0px 0px 20px 0px"
            filterFont="13px"
            filterChekbox="5px"
            filterLineheight="18px"
            classtype="checkboxwrap"
            checked={isSubscribed}
            onClickFunction={onChangeSubscribeEmail}
          />

          <Button
            text="Send OTP"
            isLoading={registerLoader}
            type="secondary"
            onClick={() => {
              setActiveAuthType();
              onSubmit();
            }}
          />

          <Flex justifyCenter flexMargin="50px 0px 0px 0px">
            <SpanText style={{ marginLeft: '0px' }}>
              Already have an account?
            </SpanText>
            <TermsLink
              onClick={e => {
                e.preventDefault();
                setModalType('login');
                setIsMobileEdit();
              }}
            >
              Login
            </TermsLink>
          </Flex>
        </div>
      </AdditionalFilterWrapper>
    </>
  );
};

CreateParentAccountModal.propTypes = {
  closeHandler: PropTypes.func,
  setModalType: PropTypes.func,
  setIsMobileEdit: PropTypes.func,
  isEdit: PropTypes.bool,
  router: PropTypes.object,
  successToast: PropTypes.func,
  errorToast: PropTypes.func,
  setActiveAuthType: PropTypes.func,
};

export default CreateParentAccountModal;

const AdditionalFilterWrapper = styled.div`
  width: 710px;
  height: ${props => props.height};
  margin: 5% auto;
  padding: 25px;
  padding-left: 45px;
  border-radius: 10px;
  background: ${colors.white};
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
  position: relative;
  &.testimonials {
    max-width: 665px;
    width: 100%;
    padding: 50px 25px 50px 36px;
    margin: 4% auto 20px;
  }
  &.teachers {
    max-width: 1022px;
    width: 100%;
    padding: 62px 58px;
    margin: 5% auto;
  }
  &.tour {
    max-width: 500px;
    width: 100%;
    padding: 50px 48px;
    margin: 5% auto 20px;
    @media screen and (max-width: 768px) {
      max-width: 290px;
      width: 100%;
      padding: 20px;
      margin: 20% auto;
    }
    .scheduleTour {
      display: flex;
      flex-direction: column;
      .personalDetail {
        color: #30333b;
        font-family: 'Roboto', sans-serif;
        font-size: 16px;
        line-height: 22px;
        margin-bottom: 20px;
      }
    }
  }
  .confirmMessage {
    //   display:flex;
    //   flex-direction:column;
    //   align-items:center;
    text-align: center;
  }
  .popupScroll {
    max-height: 590px;
    overflow: scroll;
  }
`;

export const GalleryHeading = styled.div`
  margin: ${props => props.margin || '0px'};
  color: ${props => props.color || '#30333B'};
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 500;
  line-height: 28px;
`;

export const ScheduleDetail = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  border-radius: 10px;
  background-color: #f3f5f9;
  padding: 18px 24px;
  margin-bottom: 10px;
  h2 {
    color: #30333b;
    font-family: 'Quicksand', sans-serif;
    font-size: 16px;
    font-weight: bold;
    line-height: 26px;
    text-align: center;
    margin: 0px;
    span {
      font-weight: 300;
      margin-right: 4px;
    }
  }
`;

const ModalTitle = styled.div`
  margin-bottom: 30px;
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 500;
  line-height: 28px;

  &.creatAccount {
    color: #30333b;
    font-size: 20px;
    // font-weight: 400;
    line-height: 23px;
  }
  &.loginQues {
    font-family: 'Roboto', sans-serif;
    font-weight: 400;
    text-align: center;
    span {
      margin: 0px 5px;
    }
    &__title {
      margin-bottom: 20px;
      font-family: 'Roboto', sans-serif;
      font-weight: 400;
      text-align: center;
      font-size: 16px;
      line-height: 21px;
    }
  }
`;

const TermsLink = styled.a`
  margin-left: 5px;
  color: ${props => props.color || '#613A95'};
  font-family: 'Quicksand', sans-serif;
  font-weight: 400;
  font-size: 14px;
  line-height: 19px;
  text-decoration: none;
`;
const SpanText = styled.span`
  margin-left: 5px;
  color: #666c78;
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 19px;
`;
