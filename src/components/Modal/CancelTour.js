/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* react/no-unescaped-entities */
import React from 'react';
import PropTypes from 'prop-types';
import { useScheduleHook } from 'shared/hooks';
import styled from 'styled-components';
import colors from '../../utils/colors';
import Button from '../Button';
import Flex from '../common/flex';
import cancelImage from '../../images/Sad (1).png';

const CancelTour = ({
  closeHandler,
  schduleTempData: { name, date, tourid, reason, id } = {},
  setScheduleTempDataRes,
  setModalType,
  errorToast,
  successToast,
  scheduleAction,
  setScheduleAction,
  ...props
}) => {
  const onScheduleDeleteSuccess = () => {
    successToast('Tour cancelled successfully.');
    setScheduleTempDataRes({});
    closeHandler();
  };
  const onScheduleDeleteError = ({ message }) =>
    errorToast(message || 'Something went worng.');

  const { onScheduleCancel, scheduleDeleteLoader } = useScheduleHook(props, {
    onScheduleDeleteSuccess,
    onScheduleDeleteError,
  });

  return (
    <AdditionalFilterWrapper className="welcome">
      {/* {!props.disableLoginModalCloseIcon && ( */}
      <CloseModal onClick={closeHandler}>
        {' '}
        <span
          className="iconify"
          data-icon="bytesize:close"
          data-inline="false"
        />
      </CloseModal>
      {/* )} */}

      <ModalLeft>
        <ModalTitle className="creatAccount">
          {' '}
          Your tour is cancelled with
        </ModalTitle>

        <CancelDetails>
          <Flex
            column
            className="cancelDate"
            flexPadding="20px"
            flexBorderdashed="1px solid #fff"
          >
            <h2>{name}</h2>
            <h2>
              <span>Date:</span>
              {date}
            </h2>
            <h2>
              <span>Tour Id:</span>
              {tourid}
            </h2>
            <h2>
              <span>Reason</span>
              {reason}
            </h2>
          </Flex>
        </CancelDetails>
        <ModalSubTitle className="creatAccount">
          {' '}
          Would you like to schedule for a different date and time?
        </ModalSubTitle>
        <Flex flexMargin="20px 0px 0px">
          <Button
            type="outline"
            text="No, maybe later"
            marginRight="10px"
            height="50px"
            onClick={() => {
              onScheduleCancel({
                reason,
                id,
              });
            }}
            isLoading={scheduleDeleteLoader}
          />
          <Button
            type="primary"
            text="Yes, schedule"
            height="50px"
            fullwidth
            onClick={() => {
              setScheduleTempDataRes({});
              // setScheduleAction('edit');
              setModalType('scheduleOptions');
              // setScheduleAction(null);
            }}
          />
        </Flex>
      </ModalLeft>
      <ModalRight>
        <img src={cancelImage} alt="" />
      </ModalRight>
    </AdditionalFilterWrapper>
  );
};

CancelTour.propTypes = {
  closeHandler: PropTypes.func,
  setScheduleRes: PropTypes.func,
  scheduleRes: PropTypes.object,
  schduleTempData: PropTypes.object,
  setScheduleTempDataRes: PropTypes.func,
  errorToast: PropTypes.func,
  successToast: PropTypes.func,
  setModalType: PropTypes.func,
  scheduleAction: PropTypes.string,
  setScheduleAction: PropTypes.func,
};

export default CancelTour;

const AdditionalFilterWrapper = styled.div`
  width: 840px;
  height: ${props => props.height};
  margin: 25vh auto;
  padding: 50px;
  border-radius: 10px;
  background: ${colors.white};
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
  position: relative;
  @media (max-width: 500px) {
    padding: 50px 24px;
    max-width: 380px;
    margin: 2vh auto;
  }
  @media (max-width: 400px) {
    max-width: 340px;
  }
  @media (max-width: 330px) {
    max-width: 300px;
  }
`;

export const GalleryHeading = styled.div`
  margin: ${props => props.margin || '0px'};
  color: ${props => props.color || '#30333B'};
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 500;
  line-height: 28px;
`;

const ModalTitle = styled.div`
  margin-bottom: 20px;
  color: #30333B;
  font-family: Quicksand;
  font-size: 22px;
  font-weight: 500;
  letter-spacing: 0;
  line-height: 28px;
  text-align: center;
  &.creatAccount {
    color: #30333b;
    font-size: 20px;
    // font-weight: 400;
    line-height: 23px;
    @media (max-width: 500px) {
      font-size: 18px;
    }
  }

    &__title {
      margin-bottom: 20px;
      font-family: 'Roboto', sans-serif;
      font-weight: 400;
      text-align: center;
      font-size: 16px;
      line-height: 21px;
    }
  }
`;

const ModalSubTitle = styled.div`
  margin-left: 5px;
  color: #30333b;
  font-family: Roboto;
  font-size: 16px;
  letter-spacing: 0;
  line-height: 22px;
`;
const CloseModal = styled.div`
  position: absolute;
  top: 20px;
  right: 20px;
  cursor: pointer;
  @media screen and (max-width: 768px) {
    top: 1px;
    right: 7px;
  }
  .iconify {
    height: 12px;
    width: 12px;
    color: #666c78;
  }
`;

const ModalRight = styled.div`
  position: absolute;
  left: 0px;
  bottom: 0px;
  @media (max-width: 500px) {
    position: initial;
    margin-top: -39px;
    margin-bottom: -108px;
    margin-left: -73px;
    transform: scale(0.7);
  }
`;

const ModalLeft = styled.div`
  margin-left: auto;
  width: fit-content;
  .alreadySigned {
    color: #666c78;
    font-family: Roboto;
    font-size: 14px;
    letter-spacing: 0;
    line-height: 14px;
    font-weight: 400;
    text-align: center;
    @media (max-width: 500px) {
      font-size: 12px;
    }
    span {
      color: #613a95;
      margin-left: 5px;
      cursor: pointer;
    }
  }
  @media (max-width: 500px) {
    margin-left: 0px;
    width: 100%;
  }
  .outline {
    @media (max-width: 500px) {
      height: 46px;
      padding: 0px 22px;
      font-size: 14px;
    }
    @media (max-width: 330px) {
      height: 40px;
      padding: 0px 18px;
      font-size: 13px;
    }
  }
  .primary {
    @media (max-width: 500px) {
      height: 46px;
      padding: 0px 22px;
      font-size: 14px;
    }
    @media (max-width: 330px) {
      height: 40px;
      padding: 0px 18px;
      font-size: 13px;
    }
  }
`;

export const CancelDetails = styled.div`
  width: 100%;
  border-radius: 10px;
  background-color: #f2eff5;
  margin-bottom: 20px;
  .cancelDate {
    h2 {
      width: 100%;
      color: #30333b;
      font-family: 'Quicksand', sans-serif;
      font-size: 16px;
      font-weight: bold;
      line-height: 26px;
      //   text-align: center;
      margin: 5px 0px;
      span {
        font-weight: 300;
        margin-right: 4px;
      }
      @media (max-width: 500px) {
        font-size: 14px;
        line-height: 22px;
      }
    }
  }
`;
