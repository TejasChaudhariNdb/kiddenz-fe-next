import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import colors from '../../utils/colors';
import Testimonial from '../TestimonialCard/index';

const TestimonialModal = ({ closeHandler }) => {
  const currentRef = useRef();
  return (
    <AdditionalFilterWrapper className="testimonials" ref={currentRef}>
      <CloseModal onClick={closeHandler}>
        {' '}
        <span
          className="iconify"
          data-icon="bytesize:close"
          data-inline="false"
        />
      </CloseModal>
      <GalleryHeading>4 Testimonials</GalleryHeading>
      <div className="popupScroll">
        <Testimonial
          name="Yvonne Thompson"
          type="popup"
          comment="“I love the program at Bright Beginnings Family Daycare because it is a program that not only shows children how to interact with others of their age but also shows them how to be disciplined. ”"
        />
        <Testimonial
          name="Heather Clark"
          type="popup"
          comment="“Maria and her team are great, super loving. I love that they are always teaching her something new.”"
        />
        <Testimonial
          name="Heather Clark"
          type="popup"
          comment="“Maria and her team are great, super loving. I love that they are always teaching her something new.”"
        />
        <Testimonial
          name="Yvonne Thompson"
          type="popup"
          comment="“I love the program at Bright Beginnings Family Daycare because it is a program that not only shows children how to interact with others of their age but also shows them how to be disciplined. ”"
        />
        <Testimonial
          name="Yvonne Thompson"
          type="popup"
          comment="“Maria and her team are great, super loving. I love that they are always teaching her something new.”"
        />
      </div>
    </AdditionalFilterWrapper>
  );
};

TestimonialModal.propTypes = {
  closeHandler: PropTypes.func,
};

export default TestimonialModal;

const AdditionalFilterWrapper = styled.div`
  width: 710px;
  height: ${props => props.height};
  margin: 5% auto;
  padding: 25px;
  padding-left: 45px;
  border-radius: 10px;
  background: ${colors.white};
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
  position: relative;
  &.testimonials {
    max-width: 665px;
    width: 100%;
    padding: 50px 25px 50px 36px;
    margin: 4% auto 20px;
  }
  &.teachers {
    max-width: 1022px;
    width: 100%;
    padding: 62px 58px;
    margin: 5% auto;
  }
  &.tour {
    max-width: 500px;
    width: 100%;
    padding: 50px 48px;
    margin: 5% auto 20px;
    @media screen and (max-width: 768px) {
      max-width: 290px;
      width: 100%;
      padding: 20px;
      margin: 20% auto;
    }
    .scheduleTour {
      display: flex;
      flex-direction: column;
      .personalDetail {
        color: #30333b;
        font-family: 'Roboto', sans-serif;
        font-size: 16px;
        line-height: 22px;
        margin-bottom: 20px;
      }
    }
  }
  .confirmMessage {
    //   display:flex;
    //   flex-direction:column;
    //   align-items:center;
    text-align: center;
  }
  .popupScroll {
    max-height: 590px;
    overflow: scroll;
  }
`;

export const GalleryHeading = styled.div`
  margin: ${props => props.margin || '0px'};
  color: ${props => props.color || '#30333B'};
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 500;
  line-height: 28px;
`;

const CloseModal = styled.div`
  position: absolute;
  top: 20px;
  right: 20px;
  cursor: pointer;
  @media screen and (max-width: 768px) {
    top: 1px;
    right: 7px;
  }
  .iconify {
    height: 12px;
    width: 12px;
    color: #666c78;
  }
`;
