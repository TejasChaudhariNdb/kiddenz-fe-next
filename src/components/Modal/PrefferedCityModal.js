/* eslint-disable indent */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable react/prop-types */
import React, { useState, useRef } from 'react';
import styled from 'styled-components';
// import PropTypes from 'prop-types';
import colors from 'utils/colors';
import PlacesAutocomplete, {
  geocodeByAddress,
} from 'react-places-autocomplete';
import Input from 'containers/HomePage/Input';
import Form from 'containers/HomePage/Form';
import { useDropdownClose } from 'shared/hooks';
import Flex from '../common/flex';
import locationImage from '../../images/Kiddenz_icon-location-06.svg';
import pulseLoader from '../../images/pulse_loader.gif';
// import Input from 'containers/Input';
// import InputBox from '../InputBox';
// import Button from '../Button';

const PrefferedCityModal = props => {
  const {
    closeHandler,
    onSubmit,
    cities = [],
    // onSearch,
    // searchKey,
    loader,
    // errorToast,
    selectedCity,
    onCurrentLocation,
    locationLoader,
    onSelectResult,
  } = props;
  const locationSearchDropdown = useRef(null);
  // const [selectedCity, setSelectedCity] = useState('');
  const [locationSearchSting, setLocationSearchSting] = useState('');

  const [
    locationSearchDropdownState,
    setLocationSearchDropdownState,
  ] = useState(false);
  useDropdownClose(locationSearchDropdown, setLocationSearchDropdownState);

  const handleSelect = address => {
    geocodeByAddress(address)
      .then(results => {
        onSelectResult(results[0]);
        console.log(results[0].address_components, 'address comp');
      })
      .catch(error => console.error('Error', error));
  };

  // const onSelectCity = city => {
  //   if (city !== selectedCity) {
  //     setSelectedCity(city);
  //   } else {
  //     setSelectedCity('');
  //   }
  // };
  // const handleSubmit = () => {
  //   if (selectedCity) {
  //     onSubmit(selectedCity);
  //   } else {
  //     errorToast('Please select location');
  //   }
  // };
  return (
    <AdditionalFilterWrapper className="tour">
      {selectedCity && (
        <CloseModal onClick={closeHandler}>
          {' '}
          <span
            className="iconify"
            data-icon="bytesize:close"
            data-inline="false"
          />
        </CloseModal>
      )}
      <Flex column flexHeight="fit-content">
        <ModalTitle>What is your preferred location?</ModalTitle>
        <ModalSubInfo>
          To suggest online programs near your location
        </ModalSubInfo>
        <ModalLoginImage>
          <img src={locationImage} alt="" />
        </ModalLoginImage>
        <Flex column>
          {/* <InputBox
            placeholder="Enter Your Location"
            type="text"
            name="pincode"
            onChange={onSearch}
            value={searchKey}
          /> */}
          <Form
          // className="locationForm"
          >
            <PlacesAutocomplete
              value={locationSearchSting}
              onChange={address => {
                setLocationSearchSting(address);
              }}
              onSelect={address => handleSelect(address)}
              onError={() => {}}
              clearItemsOnError
              shouldFetchSuggestions
              searchOptions={{
                // types: ['locality', 'administrative_area_level_2'],
                componentRestrictions: { country: ['in'] },
              }}
            >
              {({ getInputProps, suggestions, getSuggestionItemProps }) => {
                if (suggestions.length > 0)
                  setLocationSearchDropdownState(true);
                else setLocationSearchDropdownState(false);

                return (
                  <>
                    <label htmlFor="username">
                      <div className="searchInputField">
                        <Input
                          {...getInputProps({
                            onBlur: () => {},
                            // onFocus: () => {
                            //   setShowCureentLocation(true);
                            //   setLocationSearchError(false);
                            // },
                          })}
                          id="username"
                          type="text"
                          placeholder="Enter your location"
                          value={locationSearchSting}
                          autoComplete="off"
                          inputBorder="	1px solid #C0C8CD"
                        />
                      </div>
                      <div
                        className="myCurrentLocation active"
                        onClick={!locationLoader ? onCurrentLocation : null}
                        isLoading={locationLoader}
                      >
                        <span
                          className="iconify"
                          data-icon="ic:outline-location-searching"
                          data-inline="false"
                        />
                        <span>
                          {locationLoader ? 'Detecting' : 'Detect my location'}
                        </span>
                      </div>
                      {/* )} */}
                    </label>

                    {locationSearchDropdownState &&
                      suggestions.length > 0 && (
                        <SearchOption
                          column
                          ref={locationSearchDropdown}
                          top="0px"
                          className="single"
                        >
                          {suggestions.map(suggestion => (
                            <li
                              {...getSuggestionItemProps(suggestion, {
                                onClick: () => {
                                  // onSubmit(
                                  //   suggestion.formattedSuggestion.mainText,
                                  // );
                                  setLocationSearchSting(
                                    suggestion.formattedSuggestion.mainText,
                                  );
                                },
                              })}
                            >
                              {suggestion.description}
                            </li>
                          ))}
                        </SearchOption>
                      )}
                  </>
                );
              }}
            </PlacesAutocomplete>
          </Form>
          {/* <Flex justifyCenter flexMargin="0px 0px 20px 0px">
            <Button
              text="Current Location"
              type="mobile"
              onClick={onCurrentLocation}
              isLoading={locationLoader}
            />
          </Flex> */}
          {loader ? (
            <FilterLoader>
              <Flex
                alignCenter
                justifyCenter
                flexWidth="100%"
                flexHeight="100%"
              >
                <img src={pulseLoader} alt="" height={75} width={75} />
              </Flex>
            </FilterLoader>
          ) : (
            <StateList>
              {' '}
              {cities.map(city => (
                <li
                  onClick={() => {
                    onSubmit(city);
                    // if (city !== selectedCity) {
                    //   setSelectedCity(city);
                    // } else {
                    //   setSelectedCity('');
                    // }
                  }}
                  className={city === selectedCity && 'active'}
                >
                  {city}
                </li>
              ))}
            </StateList>
          )}
        </Flex>
        {/* <Button onClick={handleSubmit} text="Done" /> */}
      </Flex>
    </AdditionalFilterWrapper>
  );
};
PrefferedCityModal.propTypes = {};
export default PrefferedCityModal;

export const Mainheading = styled.div``;

const AdditionalFilterWrapper = styled.div`
  width: 710px;
  height: ${props => props.height};
  margin: 5% auto;
  padding: 25px;
  padding-left: 45px;
  border-radius: 10px;
  background: ${colors.white};
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
  position: relative;

  &.tour {
    max-width: 500px;
    width: 100%;
    padding: 38px 48px;
    margin: 5% auto 20px;
    @media screen and (max-width: 768px) {
      max-width: 290px;
      width: 100%;
      padding: 20px;
      margin: 20% auto;
    }
    @media (max-width: 500px) {
      max-width: 330px;
      padding: 38px 16px;
    }
  }
`;

const ModalTitle = styled.div`
  margin-bottom: 30px;
  font-family: 'Quicksand', sans-serif;
  font-size: 20px;
  font-weight: 400;
  line-height: 28px;
  color: #30333b;
`;

const ModalLoginImage = styled.div`
  width: 104px;
  height: 104px;
  margin: 30px auto 30px;
  min-height: 104px;
  @media (max-width: 500px) {
    margin: 0px auto 30px;
  }
  img {
    width: 100%;
    height: 100%;
  }
`;

const ModalSubInfo = styled.div`
  font-family: Roboto;
  font-size: 18px;
  letter-spacing: 0;
  line-height: 26px;
  text-align: center;
  opacity: 0.5;
  @media (max-width: 500px) {
    font-size: 14px;
    line-height: 20px;
    margin-bottom: 30px;
  }
`;

const CloseModal = styled.div`
  position: absolute;
  top: 20px;
  right: 20px;
  cursor: pointer;
  @media screen and (max-width: 768px) {
    top: 1px;
    right: 7px;
  }
  .iconify {
    height: 12px;
    width: 12px;
    color: #666c78;
  }
`;

const StateList = styled.div`
  display: flex;
  align-items: center;
  flex-wrap: wrap;
  li {
    display: flex;
    align-items: center;
    justify-content: center;
    height: 35px;
    border: 1px solid #613a95;
    border-radius: 5px;
    padding: 0px 14px;
    font-weight: 400;
    font-size: 16px;
    line-height: 20px;
    color: #613a95;
    font-family: 'Quicksand', sans-serif;
    margin: 0px 12px 12px 0px;
    cursor: pointer;
    &:hover {
      background: #613a95;
      color: #fff;
    }
    &.active {
      background: #613a95;
      color: #fff;
    }
  }
`;

const FilterLoader = styled.div`
  height: calc(100vh - 500px);
  width: 100%;
`;

const SearchOption = styled(Flex)`
  position: absolute;
  top: ${props => props.top || '115px'};
  left: 0px;
  width: 100%;
  max-width: 442px;
  padding: 20px 20px 40px;
  border-radius: 5px;
  background: ${colors.white};
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.3);
  z-index: 1000000;
  .google {
    position: absolute;
    right: 20px;
    bottom: 0px;
    // padding:20px 0px;
    @media (max-width: 500px) {
      right: 20px;
    }
    @media (min-width: 1400px) {
      display: block !important;
    }
  }
  @media (max-width: 500px) {
    left: 0px;
    top: 90px;
  }
  .google {
    position: absolute;
    right: 20px;
    bottom: 0px;
    // padding:20px 0px;
    @media (max-width: 500px) {
      right: 20px;
    }
    @media (min-width: 1400px) {
      display: block !important;
    }
  }
  &.single {
    top: 90px;
    @media (max-width: 500px) {
      top: 100px;
    }
  }

  li {
    font-size: 14px;
    line-height: 30px;
    border-bottom: 1px solid rgba(0, 0, 0, 0.1);
    list-style-type: none;
    padding: 5px 0px;
    color: #666c78;
    cursor: pointer;
    &:last-child {
      border-bottom: 0px solid rgba(0, 0, 0, 0.1);
    }
    &:hover {
      color: #000;
    }
  }
`;
