/* eslint-disable no-console */
import React, { useRef, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import axios from 'config/axios';
import Router from 'next/router';
import styled from 'styled-components';
import Button from '../Button';
import colors from '../../utils/colors';
import CheckBox from '../Checkbox';
import InputBox from '../InputBox';
import Flex from '../common/flex';
import { useVerifyOTPHook, useProfileUpdateHook } from '../../shared/hooks';
import { setCookie } from '../../shared/utils/commonHelpers';

const VerifyMobileModal = ({
  closeHandler,
  setModalType,
  setIsMobileEdit,
  setActive,
  activeAuthType,
  onOtpSuccessCallback,
  signUpName,
  ...props
}) => {
  const [isProfileUpdate, setProfileUpdate] = useState(false);
  const [mailSubscription, setMailSubscriptionState] = useState(true);
  const [accessToken, setAccessToken] = useState('');
  const [minutes, setMinutes] = useState(0);
  const [seconds, setSeconds] = useState(59);

  const { router: { query: { form, provider_slug: slug } = {} } = {} } = props;
  const currentRef = useRef();
  const { successToast, errorToast } = props;
  const onSuccess = ({ data, showEmailPopUp }) => {
    /* Check if we need update mail popup
      yes - open the profile update and store token in state
      no - close the modal and set cookie
    */

    if (!showEmailPopUp) {
      setModalType('');
      setActive(false);
      setIsMobileEdit(false);
      successToast('OTP verified successfully.');
      const token = data.data['x-access-token'];
      setCookie('x-access-token', token);
      axios.defaults.headers.common.Authorization = token
        ? `Bearer ${token}`
        : '';

      /* Check if we need onboarding popup
      yes - open the onboarding popup
      no - close the modal 
    */
      if (!data.data.is_onboarded) {
        setModalType('onboarding');
        setActive(true);
      } else {
        if (form === 'review_form') {
          Router.push(`/review?slug=${slug}`).then(() => window.scrollTo(0, 0));
        }
        setModalType('');
        setActive(false);

        const {
          data: {
            data: { selected_filter: defFilters },
          },
        } = data;

        // Things to do after verify OTP if the user is onboarded
        if (onOtpSuccessCallback) onOtpSuccessCallback(defFilters);
      }
    } else {
      setProfileUpdate(true);
      const token = data.data['x-access-token'];
      setAccessToken(token);
      axios.defaults.headers.common.Authorization = token
        ? `Bearer ${token}`
        : '';
    }
  };

  const {
    otp: { value, onChange, onBlur, error, onSubmit, resend, loader, otpTimer },
    mobileNo,
  } = useVerifyOTPHook(props, { onSuccess, successToast, errorToast });

  const onProfileUpdateSuccess = () => {
    setIsMobileEdit(false);
    successToast('Successfully updated.');
    setCookie('x-access-token', accessToken);
    axios.defaults.headers.common.Authorization = accessToken
      ? `Bearer ${accessToken}`
      : '';

    if (form === 'review_form') {
      if (form === 'review_form') {
        Router.push({
          pathname: '/review',
        }).then(() => window.scrollTo(0, 0));
      }
      setModalType('');
      setActive(false);
    } else {
      setModalType('onboarding');
      setActive(true);
    }
  };

  const onProfileUpdateError = ({ message }) => {
    successToast(message || 'Something went wrong.');
  };

  const {
    email: { value: emailAddress },
    onBlur: profileItemOnBlur,
    onChange: profileItemOnChange,
    error: profileItemOnError,
    onSubmit: profileDataSubmit,
    profileLoader,
  } = useProfileUpdateHook(props, {
    onSuccess: onProfileUpdateSuccess,
    onError: onProfileUpdateError,
    signUpName,
  });

  const onEnterButtonPressed = e => {
    if (e.key === 'Enter') {
      e.preventDefault();
      if (isProfileUpdate) profileDataSubmit();
      else onSubmit();
    }
  };

  useEffect(
    () => {
      if (otpTimer) {
        setMinutes(Number(otpTimer.split(':')[0]));
        setSeconds(Number(otpTimer.split(':')[1]));
      }
    },
    [otpTimer],
  );

  useEffect(
    () => {
      const intervalId = setInterval(() => {
        if (seconds > 0) {
          setSeconds(prevTime => prevTime - 1);
        }
        if (seconds === 0) {
          if (minutes === 0) {
            clearInterval(intervalId);
          } else if (seconds === 0) {
            setMinutes(prevTime => prevTime - 1);
            setSeconds(59);
          }
        }
      }, 1000);

      return () => clearInterval(intervalId);
    },
    [seconds, minutes],
  );

  return (
    <>
      <AdditionalFilterWrapper className="tour" ref={currentRef}>
        <ModalTitle className="creatAccount">
          {isProfileUpdate ? 'Your Email Address' : 'Verify Your Mobile Number'}
        </ModalTitle>
        {!isProfileUpdate && (
          <>
            <SpanText>Please enter the OTP sent to your mobile number</SpanText>
            <NumberEdit alignCenter>
              <span
                className="iconify"
                data-icon="simple-line-icons:screen-smartphone"
                data-inline="false"
              />
              <div className="number">+91 {mobileNo}</div>
              <TermsLink
                style={{ marginLeft: 'auto' }}
                onClick={() => {
                  setIsMobileEdit();
                  setModalType(activeAuthType);
                }}
              >
                Edit
              </TermsLink>
            </NumberEdit>
          </>
        )}
        <div className="scheduleTour">
          {!isProfileUpdate && (
            <>
              <div className="otpEnter">
                <InputBox
                  name="otp"
                  placeholder="Enter OTP"
                  type="number"
                  margin="15px 0px 10px 0px"
                  value={value}
                  onChange={onChange}
                  onBlur={onBlur}
                  error={error}
                  onKeyPress={onEnterButtonPressed}
                />
                <TermsLink
                  onClick={+seconds > 0 ? null : resend}
                  disabled={+seconds > 0}
                >
                  Resend OTP
                </TermsLink>
                {+seconds > 0 ? (
                  <p>
                    {minutes}:{seconds < 10 ? `0${seconds}` : seconds}
                  </p>
                ) : null}
              </div>
            </>
          )}
          {isProfileUpdate && (
            <InputBox
              name="email"
              placeholder="Email Address"
              type="text"
              margin="15px 0px 10px 0px"
              value={emailAddress}
              onChange={profileItemOnChange}
              onBlur={profileItemOnBlur}
              error={profileItemOnError.email}
              onKeyPress={onEnterButtonPressed}
            />
          )}
          {isProfileUpdate ? (
            <Flex flexMargin="10px 0px 0px 0px">
              <CheckBox
                label="I would like to subscribe for the articles and receive updates from Kiddenz"
                onClickFunction={() =>
                  mailSubscription
                    ? setMailSubscriptionState(false)
                    : setMailSubscriptionState(true)
                }
                checked={mailSubscription === true}
              />
            </Flex>
          ) : null}
          <Button
            text={isProfileUpdate ? 'Next' : 'Verify Number'}
            type="mobile"
            marginTop="30px"
            onClick={isProfileUpdate ? profileDataSubmit : onSubmit}
            isLoading={isProfileUpdate ? profileLoader : loader}
          />
        </div>
      </AdditionalFilterWrapper>
    </>
  );
};

VerifyMobileModal.propTypes = {
  closeHandler: PropTypes.func,
  setIsMobileEdit: PropTypes.func,
  setModalType: PropTypes.func,
  successToast: PropTypes.func,
  errorToast: PropTypes.func,
  setActive: PropTypes.func,
  onOtpSuccessCallback: PropTypes.func,
  activeAuthType: PropTypes.string,
  signUpName: PropTypes.string,
  router: PropTypes.object,
};

export default VerifyMobileModal;

const AdditionalFilterWrapper = styled.div`
  width: 710px;
  height: ${props => props.height};
  margin: 5% auto;
  padding: 25px;
  padding-left: 45px;
  border-radius: 10px;
  background: ${colors.white};
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
  position: relative;
  @media (max-width: 500px) {
    padding: 20px 16px !important;
  }
  &.testimonials {
    max-width: 665px;
    width: 100%;
    padding: 50px 25px 50px 36px;
    margin: 4% auto 20px;
  }
  &.teachers {
    max-width: 1022px;
    width: 100%;
    padding: 62px 58px;
    margin: 5% auto;
  }
  &.tour {
    max-width: 500px;
    width: 100%;
    padding: 50px 48px;
    margin: 12% auto 20px;
    @media screen and (max-width: 768px) {
      max-width: 290px;
      width: 100%;
      padding: 20px;
      margin: 20% auto;
    }
    .scheduleTour {
      display: flex;
      flex-direction: column;
      .personalDetail {
        color: #30333b;
        font-family: 'Roboto', sans-serif;
        font-size: 16px;
        line-height: 22px;
        margin-bottom: 20px;
      }
      .mobile {
        @media (max-width: 500px) {
          height: 46px;
        }
      }
      .otpEnter {
        position: relative;
        p {
          position: absolute;
          top: 15px;
          right: 15px;
          font-family: 'Roboto', sans-serif;
          font-size: 16px;
          color: rgba(48, 51, 59, 1);
          @media (max-width: 500px) {
            font-size: 14px;
          }
        }
      }
    }
    @media (max-width: 500px) {
      max-width: 320px;
      padding: 50px 20px;
    }
    @media (max-width: 390px) {
      max-width: 300px;
    }
  }
  .confirmMessage {
    //   display:flex;
    //   flex-direction:column;
    //   align-items:center;
    text-align: center;
  }
  .popupScroll {
    max-height: 590px;
    overflow: scroll;
  }
`;

export const GalleryHeading = styled.div`
  margin: ${props => props.margin || '0px'};
  color: ${props => props.color || '#30333B'};
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 500;
  line-height: 28px;
`;

export const ScheduleDetail = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  border-radius: 10px;
  background-color: #f3f5f9;
  padding: 18px 24px;
  margin-bottom: 10px;
  h2 {
    color: #30333b;
    font-family: 'Quicksand', sans-serif;
    font-size: 16px;
    font-weight: bold;
    line-height: 26px;
    text-align: center;
    margin: 0px;
    span {
      font-weight: 300;
      margin-right: 4px;
    }
  }
`;

const ModalTitle = styled.div`
  margin-bottom: 30px;
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 500;
  line-height: 28px;

  &.creatAccount {
    color: #30333b;
    font-size: 20px;
    // font-weight: 400;
    line-height: 23px;
    @media (max-width: 500px) {
      font-size: 18px;
      margin-bottom: 10px;
    }
  }
  &.loginQues {
    font-family: 'Roboto', sans-serif;
    font-weight: 400;
    text-align: center;
    span {
      margin: 0px 5px;
    }
    &__title {
      margin-bottom: 20px;
      font-family: 'Roboto', sans-serif;
      font-weight: 400;
      text-align: center;
      font-size: 16px;
      line-height: 21px;
    }
  }
`;

const TermsLink = styled.a`
  margin-left: 5px;
  color: ${props => props.color || '#613A95'};
  font-family: 'Quicksand', sans-serif;
  font-weight: 400;
  font-size: 14px;
  line-height: 19px;
  text-decoration: none;
  cursor: ${props => (props.disabled ? 'disabled' : 'pointer')};
`;
const SpanText = styled.span`
  margin-left: 5px;
  color: #666c78;
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 19px;
`;

const NumberEdit = styled(Flex)`
  height: 40px;
  padding: 0px 15px;
  margin-top: 15px;
  border-radius: 5px;
  background-color: #f3f5f9;

  .iconify {
    color: #666c78;
    font-size: 20px;
  }
  .number {
    font-family: 'Roboto', sans-serif;
    margin-left: 20px;
    color: #30333b;
    font-size: 16px;
    line-height: 21px;
    @media (max-width: 500px) {
      font-size: 14px;
    }
  }
`;
