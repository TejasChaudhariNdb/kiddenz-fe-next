/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* react/no-unescaped-entities */
import React from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import colors from '../../utils/colors';
import Button from '../Button';
import Flex from '../common/flex';
import welcomeImage from '../../images/thumbsup.png';

const ReScheduleModal = ({
  closeHandler,
  setScheduleRes,
  scheduleRes: { tour_id: tourId, date, time } = {},
}) => (
  <AdditionalFilterWrapper className="welcome">
    {/* {!props.disableLoginModalCloseIcon && ( */}
    <CloseModal onClick={closeHandler}>
      {' '}
      <span
        className="iconify"
        data-icon="bytesize:close"
        data-inline="false"
      />
    </CloseModal>
    {/* )} */}

    <ModalLeft>
      <ModalTitle className="creatAccount"> Thank you!</ModalTitle>
      <ModalSubTitle>
        We will update you once your schedule is confirmed
      </ModalSubTitle>
      <CancelDetails>
        <Flex
          column
          className="cancelDate"
          flexPadding="20px"
          flexBorderdashed="1px solid #fff"
        >
          <h2>
            <span>Date:</span>
            {moment(date).format('dddd, MMM DD')}, {time}
          </h2>
          <h2>
            <span>Tour Id:</span>
            {tourId}
          </h2>
        </Flex>
      </CancelDetails>
      <Flex column alignCenter>
        {/* <Button type="attach" text="Send on Whatsapp" onClick={() => {}} /> */}
        <Button
          type="primary"
          text="Done"
          height="50px"
          fullwidth
          marginTop="20px"
          onClick={() => {
            closeHandler();
            setScheduleRes({});
          }}
        />
      </Flex>
    </ModalLeft>
    <ModalRight>
      <img src={welcomeImage} alt="" />
    </ModalRight>
  </AdditionalFilterWrapper>
);

ReScheduleModal.propTypes = {
  closeHandler: PropTypes.func,
  //   USER_ENQUIRY_API_CALL: PropTypes.func,
  //   providerId: PropTypes.number,
  //   errorToast: PropTypes.func,
  //   successToast: PropTypes.func,
  setScheduleRes: PropTypes.func,
  scheduleRes: PropTypes.object,
};

export default ReScheduleModal;

const AdditionalFilterWrapper = styled.div`
  width: 830px;
  height: ${props => props.height};
  margin: 25vh auto;
  padding: 84px 56px 84px 56px;
  border-radius: 10px;
  background: ${colors.white};
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
  position: relative;

  @media (max-width: 500px) {
    max-width: 380px;
    padding: 45px 24px 40px 24px;
    overflow: hidden;
    margin: 10vh auto;
  }
  @media (max-width: 400px) {
    max-width: 340px;
  }
  @media (max-width: 330px) {
    max-width: 300px;
  }
`;

export const GalleryHeading = styled.div`
  margin: ${props => props.margin || '0px'};
  color: ${props => props.color || '#30333B'};
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 500;
  line-height: 28px;
`;

export const ScheduleDetail = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  border-radius: 10px;
  background-color: #f3f5f9;
  padding: 18px 24px;
  margin-bottom: 10px;
  h2 {
    color: #30333b;
    font-family: 'Quicksand', sans-serif;
    font-size: 16px;
    font-weight: bold;
    line-height: 26px;
    text-align: center;
    margin: 0px;
    span {
      font-weight: 300;
      margin-right: 4px;
    }
  }
`;

const ModalTitle = styled.div`
  margin-bottom: 10px;
  color: #30333b;
  font-family: Roboto;
  font-size: 22px;
  font-weight: 500;
  letter-spacing: 0;
  line-height: 22px;
  text-align: center;
  &.creatAccount {
    color: #30333b;
    font-size: 20px;
    // font-weight: 400;
    line-height: 23px;
    @media (max-width: 500px) {
      font-size: 18px;
    }
  }
  &.loginQues {
    font-family: 'Roboto', sans-serif;
    font-weight: 400;
    text-align: center;
    span {
      margin: 0px 5px;
    }
    &__title {
      margin-bottom: 20px;
      font-family: 'Roboto', sans-serif;
      font-weight: 400;
      text-align: center;
      font-size: 16px;
      line-height: 21px;
    }
  }
`;

const ModalSubTitle = styled.div`
  margin-left: 5px;
  margin-bottom: 20px;
  color: #30333b;
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 19px;
`;
const CloseModal = styled.div`
  position: absolute;
  top: 20px;
  right: 20px;
  cursor: pointer;
  @media screen and (max-width: 768px) {
    top: 1px;
    right: 7px;
  }
  .iconify {
    height: 12px;
    width: 12px;
    color: #666c78;
  }
`;

const ModalRight = styled.div`
  position: absolute;
  left: 50px;
  bottom: 0px;
  @media (max-width: 500px) {
    position: initial;
    margin-top: -39px;
    margin-bottom: -100px;
    margin-left: -88px;
    transform: scale(0.7);
  }
`;

const ModalLeft = styled.div`
  margin-left: auto;
  width: fit-content;
  .alreadySigned {
    color: #666c78;
    font-family: Roboto;
    font-size: 14px;
    letter-spacing: 0;
    line-height: 14px;
    font-weight: 400;
    text-align: center;
    @media (max-width: 500px) {
      font-size: 12px;
    }
    span {
      color: #613a95;
      margin-left: 5px;
      cursor: pointer;
    }
  }
  @media (max-width: 500px) {
    margin-left: 0px;
  }
`;

export const CancelDetails = styled.div`
  width: 100%;
  border-radius: 10px;
  background-color: #f2eff5;
  margin-bottom: 20px;
  .cancelDate {
    h2 {
      width: 100%;
      color: #30333b;
      font-family: 'Quicksand', sans-serif;
      font-size: 16px;
      font-weight: bold;
      line-height: 26px;
      text-align: center;
      margin: 0px;
      span {
        font-weight: 300;
        margin-right: 4px;
      }
      @media (max-width: 500px) {
        font-size: 14px;
      }
    }
  }
`;
