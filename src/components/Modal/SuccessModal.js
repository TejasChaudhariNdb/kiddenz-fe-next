import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import colors from '../../utils/colors';
import welcomeImage from 'images/mascot1.png';

const SuccessModal = ({ closeHandler }) => {
  const currentRef = useRef();

  return (
    <AdditionalFilterWrapper className="tour" ref={currentRef}>
      <ModalLeft>
        <CloseModal onClick={closeHandler}>
          {' '}
          <span
            className="iconify"
            data-icon="bytesize:close"
            data-inline="false"
          />
        </CloseModal>

        <GalleryHeading margin="0px 0px 10px">
          Your trial class is booked successfully
        </GalleryHeading>

        <OrgName margin="0px 0px 20px">
          You will be notified with the date and time of the trial class in 24
          hours.
        </OrgName>
      </ModalLeft>
      <ModalRight>
        <img src={welcomeImage} alt="" height={123} />
      </ModalRight>
    </AdditionalFilterWrapper>
  );
};

SuccessModal.propTypes = {
  closeHandler: PropTypes.func,
};

export default SuccessModal;

const AdditionalFilterWrapper = styled.div`
  width: 710px;
  height: ${props => props.height};
  margin: 5% auto;
  padding: 25px;
  padding-left: 45px;
  border-radius: 10px;
  background: ${colors.white};
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
  position: relative;

  &.tour {
    max-width: 500px;
    width: 100%;
    padding: 50px 48px;
    margin: 5% auto 20px;
    @media screen and (max-width: 768px) {
      max-width: 290px;
      width: 100%;
      padding: 20px;
      margin: 20% auto;
    }
    .scheduleTour {
      display: flex;
      flex-direction: column;
    }
    @media (max-width: 500px) {
      max-width: 380px !important;
      width: 100% !important;
      padding: 30px 20px;
    }
    @media (max-width: 380px) {
      max-width: 320px !important;
    }
    @media (max-width: 330px) {
      max-width: 300px !important;
    }
  }
`;

const ModalLeft = styled.div`
  width: 80%;
  margin: 0 0 0 auto;
`;

const ModalRight = styled.div`
  left: 0;
  bottom: 0;
  position: absolute;
  overflow: hidden;
  z-index: 0;
  border-radius: 10px;
  img {
    @media (max-width: 768px) {
      position: initial;
      margin-bottom: -33px;
      margin-left: -34px;
      transform: scale(0.7);
    }
  }
`;

export const GalleryHeading = styled.div`
  margin: ${props => props.margin || '0px'};
  color: ${props => props.color || '#30333B'};
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 500;
  line-height: 28px;
  @media (max-width: 768px) {
    font-size: 15px;
    line-height: 1.2;
    margin-bottom: 10px;
  }
`;

const CloseModal = styled.div`
  position: absolute;
  top: 20px;
  right: 20px;
  cursor: pointer;
  @media screen and (max-width: 768px) {
    top: 1px;
    right: 7px;
  }
  .iconify {
    height: 12px;
    width: 12px;
    color: #666c78;
  }
`;

const OrgName = styled.div`
  color: #30333b;
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 19px;
  margin: ${props => props.margin || '0px'};
  @media screen and (max-width: 768px) {
    font-size: 12px;
    line-height: 1.4;
  }
`;

export const ScheduleDetail = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  border-radius: 10px;
  background-color: #f3f5f9;
  padding: 18px 24px;
  margin-bottom: 10px;
  h2 {
    color: #30333b;
    font-family: 'Quicksand', sans-serif;
    font-size: 16px;
    font-weight: bold;
    line-height: 26px;
    text-align: center;
    margin: 0px;
    span {
      font-weight: 300;
      margin-right: 4px;
    }
  }
`;

// const Error = styled.div`
//   font-size: 12px;
//   color: red;
// `;
