/* eslint-disable no-unused-vars */
/* eslint-disable indent */
/* eslint-disable no-nested-ternary */
/* eslint-disable no-constant-condition */
import React, { useRef, useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import moment from 'moment';
import { useChildrenHook } from 'shared/hooks';
// import SimpleBar from 'simplebar-react';
import Button from '../Button';
import RadioButton, { Radio } from '../RadioButton/index';
import DatePicker from '../DatePicker';

import colors from '../../utils/colors';
import InputBox from '../InputBox';
import CheckBox from '../Checkbox';
import Flex from '../common/flex';

import Badge from '../Badge';

const TEXTAREA_MAX_CHARS = 500;

const AddChildModal = ({
  childModalTitle,
  closeHandler,
  successToast,
  errorToast,
  isChildEdit,
  setModalType,
  editChildId,
  ...props
}) => {
  const [allergiesWordCount, setAllergiesWordCount] = useState(0);
  const [medicalInstWordCount, setMedicalInstWordCount] = useState(0);
  const [specificNeedsWordCount, setSpecificNeedsWordCount] = useState(0);
  const [othersWordCount, setOthersWordCount] = useState(0);

  // const fileUploadRef = React.useRef();
  const { router: { route = '' } = {} } = props;
  const isDayCareDetail = route.indexOf('/[daycare-id]') !== -1;

  const currentRef = useRef();

  const onChildEditSuccess = () => {
    // successToast('Successfully updated.');
    setModalType(null);
    closeHandler();
  };
  const onChildEditError = ({ message }) => {
    // errorToast(message || 'Something went wrong.');
  };

  const onChildAddSuccess = () => {
    // successToast('Successfully added.');
    if (isDayCareDetail) setModalType('tour');
    else closeHandler();
  };
  const onChildAddError = ({ message }) => {
    // errorToast(message || 'Something went wrong.');
  };

  const {
    onChange,
    onBlur,
    formValues,
    error,
    onChildSubmit,
    resetForm,
    addChildLoader,
    // uploadedImage,
    // handleImageChnage,
  } = useChildrenHook(props, {
    isEdit: isChildEdit,
    childId: editChildId,
    onChildAddSuccess,
    onChildAddError,
    onChildEditSuccess,
    onChildEditError,
  });

  const TUTION_DAYS = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
  const COURSES = ['Montessori', 'International', 'Integrated', 'Custom'];

  const tution = formValues.tution
    ? typeof formValues.tution === 'string'
      ? formValues.tution.split(',')
      : formValues.tution
    : [];

  const courses = formValues.courses
    ? typeof formValues.courses === 'string'
      ? formValues.courses.split(',')
      : formValues.courses
    : [];

  const handleWordCount = (e, stateVar, setState) => {
    const charCount = e.target.value.length;
    setState(charCount);
  };

  return (
    <AdditionalFilterWrapper className="addChild" ref={currentRef}>
      <CloseModal
        onClick={() => {
          if (isDayCareDetail) setModalType('tour');
          else closeHandler();

          resetForm();
        }}
      >
        {' '}
        <span
          className="iconify"
          data-icon="bytesize:close"
          data-inline="false"
        />
      </CloseModal>
      <GalleryHeading margin="0px 0px 25px">
        {childModalTitle || 'Add a child'}
      </GalleryHeading>

      <Flex justifyBetween flexMargin="30px 0px" wrap>
        <Flex column flexWidth="48%" className="addChild-input" id="childFName">
          <p>Child first name*</p>
          <InputBox
            name="Child first name"
            type="text"
            margin="0px 0px 20px 0px"
            placeholder="First Name"
            value={formValues.first_name}
            onChange={e => onChange(e, 'first_name')}
            onBlur={e => onBlur(e, 'first_name')}
            error={error.first_name ? 'First name is required' : ''}
          />
        </Flex>
        <Flex column flexWidth="48%" className="addChild-input">
          <p>Child last name*</p>
          <InputBox
            name="Child Last name"
            type="text"
            margin="0px 0px 20px 0px"
            placeholder="Last Name"
            value={formValues.last_name}
            onChange={e => onChange(e, 'last_name')}
            onBlur={e => onBlur(e, 'last_name')}
            error={error.last_name ? 'Last name is required' : ''}
          />
        </Flex>
      </Flex>
      <Flex justifyBetween flexMargin="30px 0px" className="childrenDetails">
        <Flex column flexWidth="48%" className="addChild-input">
          <p>Date of birth*</p>
          <DatePicker
            value={
              formValues.date_of_birth
                ? moment(formValues.date_of_birth).format('DD/MM/YYYY')
                : ''
            }
            iconName="feather:calendar"
            placeholder="Select Date"
            onChange={e =>
              onChange(
                {},
                'date_of_birth',
                'text',
                moment(e).format('YYYY-MM-DD'),
              )
            }
            minDate={new Date().setFullYear(
              new Date().getFullYear() - 15,
              0,
              1,
            )}
            maxDate={new Date()}
            width="100%"
            showMonthDropdown
            showYearDropdown
          />
          <Error>{error.date_of_birth ? 'DOB is required' : ''}</Error>
          {/* <InputBox
            name="Child first name"
            type="datepicker"
            margin="0px 0px 20px 0px"
            placeholder="DD-MM-YYY"
            value={formValues.date_of_birth}
            minDate={new Date().setFullYear(
              new Date().getFullYear() - 15,
              0,
              1,
            )}
            maxDate={new Date()}
            onChange={e =>
              onChange(
                {},
                'date_of_birth',
                'text',
                moment(e).format('YYYY-MM-DD'),
              )
            }
            onBlur={e => onBlur(e, 'date_of_birth')}
            error={error.date_of_birth}
          /> */}
        </Flex>
        <Flex column flexWidth="48%" className="addChild-input" />
      </Flex>

      <Flex column className="addChild-input">
        <p>Gender</p>
        <Flex className="addChild-check" flexMargin="0px 0px 40px">
          <RadioButton
            id="add_child_male"
            text="Male"
            // checkedColor="#fff"
            filterRadio="#666C78"
            RadioMargin="0px 106px 0px 0px"
            checked={formValues.gender_type === 'male'}
            onClick={e => onChange(e, 'gender_type', 'radio', 'male')}
          />
          <RadioButton
            id="add_child_female"
            text="Female"
            filterRadio="#666C78"
            checked={formValues.gender_type === 'female'}
            onClick={e => onChange(e, 'gender_type', 'radio', 'female')}
          />
        </Flex>
        <p>Previous child care experiences</p>
        <Flex flexMargin="0px 0px 40px" className="addChild-check" wrap>
          <RadioButton
            id="nanny"
            text="Nanny"
            // checkedColor="#fff"
            filterRadio="#666C78"
            RadioMargin="0px 98px 0px 0px"
            checked={formValues.child_care_experience === 'nanny'}
            onClick={e =>
              onChange(e, 'child_care_experience', 'radio', 'nanny')
            }
          />
          <RadioButton
            id="Day_Care"
            text="Day Care"
            // checkedColor="#fff"
            filterRadio="#666C78"
            RadioMargin="0px 98px 0px 0px"
            checked={formValues.child_care_experience === 'daycare'}
            onClick={e =>
              onChange(e, 'child_care_experience', 'radio', 'daycare')
            }
          />
          <RadioButton
            id="none"
            text="None"
            // checkedColor="#fff"
            filterRadio="#666C78"
            checked={formValues.child_care_experience === 'none'}
            onClick={e => onChange(e, 'child_care_experience', 'radio', 'none')}
          />
        </Flex>
        <p>Serving type</p>
        <Flex flexMargin="0px 0px 40px" className="addChild-check">
          <CheckBox
            label="Solid"
            id="id08"
            margin="0px 30px 0px 0px"
            checked={formValues.food_type.includes('solid')}
            onClickFunction={e => {
              e.preventDefault();
              if (formValues.food_type.includes('solid')) {
                onChange(e, 'food_type', 'checkbox', 'solid', 'remove');
              } else onChange(e, 'food_type', 'checkbox', 'solid', 'add');
            }}
          />
          <CheckBox
            label="Liquid"
            id="id09"
            margin="0px 30px 0px 0px"
            checked={formValues.food_type.includes('liquid')}
            onClickFunction={e => {
              e.preventDefault();
              if (formValues.food_type.includes('liquid')) {
                onChange(e, 'food_type', 'checkbox', 'liquid', 'remove');
              } else onChange(e, 'food_type', 'checkbox', 'liquid', 'add');
            }}
          />
        </Flex>
        <p>Type of food</p>
        <Flex flexMargin="0px 0px 40px" className="addChild-check">
          <CheckBox
            label="Veg"
            id="id08"
            margin="0px 30px 0px 0px"
            checked={formValues.food_choice.includes('veg')}
            onClickFunction={e => {
              e.preventDefault();
              if (formValues.food_choice.includes('veg')) {
                onChange(e, 'food_choice', 'checkbox', 'veg', 'remove');
              } else onChange(e, 'food_choice', 'checkbox', 'veg', 'add');
            }}
          />
          <CheckBox
            label="Non-veg"
            id="id09"
            margin="0px 30px 0px 0px"
            checked={formValues.food_choice.includes('non veg')}
            onClickFunction={e => {
              e.preventDefault();
              if (formValues.food_choice.includes('non veg')) {
                onChange(e, 'food_choice', 'checkbox', 'non veg', 'remove');
              } else onChange(e, 'food_choice', 'checkbox', 'non veg', 'add');
            }}
          />
        </Flex>

        <Flex flexMargin="0px 0px 30px 0px" alignCenter wrap>
          <Title>Tuition requirement</Title>
          {TUTION_DAYS.map(d => (
            <Badge
              badgeActive={tution.includes(d)}
              onClick={e => {
                e.preventDefault();
                if (tution.includes(d)) {
                  onChange(e, 'tution', 'checkbox', d, 'remove');
                } else onChange(e, 'tution', 'checkbox', d, 'add');
              }}
              text={d}
              marginBottom="10px"
            />
          ))}
        </Flex>
        <Flex column flexWidth="100%" className="addChild-input">
          <p>Type of Tuition</p>
          <InputBox
            name="Child Last name"
            type="text"
            margin="0px 0px 20px 0px"
            placeholder="For example: Music, Dance, Maths, Social"
            value={formValues.tution_type}
            onChange={e => onChange(e, 'tution_type')}
            onBlur={e => onBlur(e, 'tution_type')}
          />
        </Flex>
        <Flex flexMargin="0px 0px 30px 0px" alignCenter wrap>
          <Title className="type">
            Type of course preferred{' '}
            <div className="infoIcon">
              <span
                className="iconify"
                data-icon="jam:info"
                data-inline="false"
              />
            </div>
            <ToolTipInfo className="tooltip-child">
              {/* <SimpleBar style={{ maxHeight: 400 }}> */}
              <div className="tooltip-info-wrapper">
                <span>
                  <p style={{ color: '#000', textDecoration: 'underline' }}>
                    Montessori
                  </p>
                  This comprehensive program developed by physician and educator
                  Maria Montessori takes a developmental approach to learning.
                  All teachers must have an early childhood undergraduate or
                  graduate degree and Montessori certification. The Montessori
                  approach emphasizes nature, creativity, and hands-on learning
                  with gentle guidance provided by the teachers. The goal of the
                  Montessori method is to develop a child senses, character,
                  practical life skills, and academic ability.
                </span>
                <span>
                  <p style={{ color: '#000', textDecoration: 'underline' }}>
                    International
                  </p>
                  International Preschool Curriculum (IPC) is aligned to meet or
                  exceed the standards of all major education systems making it
                  the most internationally accepted ECE curriculum in the world.
                </span>
                <span>
                  <p style={{ color: '#000', textDecoration: 'underline' }}>
                    Integrated
                  </p>
                  An integrated curriculum allows children to pursue learning in
                  a holistic way, without the restrictions often imposed by
                  subject boundaries. In early childhood programs it focuses
                  upon the inter-relatedness of all curricular areas in helping
                  children acquire basic learning tools. It recognizes that the
                  curriculum for the primary grades includes reading, writing,
                  listening, speaking, literature, drama, social studies, math,
                  science, health, physical education, music, and visual arts.
                  The curriculum also incorporates investigative processes and
                  technology. It emphasizes the importance of maintaining
                  partnerships with families; having knowledge of children and
                  how they learn; and building upon the community and cultural
                  context. Integrated teaching and learning processes enable
                  children to acquire and use basic skills in all the content
                  areas and to develop positive attitudes for continued
                  successful learning throughout the elementary grades.
                </span>
                <span>
                  <p style={{ color: '#000', textDecoration: 'underline' }}>
                    Customs
                  </p>
                  Custom preschool curriculum is derived from variety of
                  individual curriculums, generally different schools have their
                  own curriculum.
                </span>
              </div>
              {/* </SimpleBar> */}
            </ToolTipInfo>
          </Title>
          {COURSES.map(d => (
            <Badge
              badgeActive={courses.includes(d)}
              onClick={e => {
                e.preventDefault();
                if (courses.includes(d)) {
                  onChange(e, 'courses', 'checkbox', d, 'remove');
                } else onChange(e, 'courses', 'checkbox', d, 'add');
              }}
              text={d}
              marginBottom="10px"
            />
          ))}
        </Flex>
        <p>Allergies</p>
        <Flex flexMargin="0px 0px 40px" className="addChild-check">
          <InputBox
            type="textarea"
            margin="0px"
            rows={4}
            value={formValues.allergies}
            onChange={e => {
              if (e.target.value.length <= TEXTAREA_MAX_CHARS) {
                onChange(e, 'allergies');
                handleWordCount(e, allergiesWordCount, setAllergiesWordCount);
              }
            }}
            charLimit={`${allergiesWordCount}/${TEXTAREA_MAX_CHARS}`}
          />
        </Flex>
        <p>Medical instructions</p>
        <Flex flexMargin="0px 0px 40px" className="addChild-check">
          <InputBox
            type="textarea"
            margin="0px"
            rows={4}
            value={formValues.medication_details}
            onChange={e => {
              if (e.target.value.length <= TEXTAREA_MAX_CHARS) {
                onChange(e, 'medication_details');
                handleWordCount(
                  e,
                  medicalInstWordCount,
                  setMedicalInstWordCount,
                );
              }
            }}
            charLimit={`${medicalInstWordCount}/${TEXTAREA_MAX_CHARS}`}
          />
        </Flex>

        <Title>
          Special care needed?
          <div className="infoIcon">
            <span
              className="iconify"
              data-icon="jam:info"
              data-inline="false"
            />
          </div>
          <ToolTipInfo>
            <span>
              {' '}
              Is your child differently abled and needs special care?
            </span>
          </ToolTipInfo>
        </Title>
        <Flex flexMargin="0px 0px 40px" className="addChild-check">
          <RadioButton
            id="add_child_specialcare_yes"
            text="Yes"
            // checkedColor="#fff"
            filterRadio="#666C78"
            RadioMargin="0px 106px 0px 0px"
            checked={formValues.special_care}
            onClick={e => onChange(e, 'special_care', 'radio', true)}
          />
          <RadioButton
            id="add_child_specialcare_no"
            text="No"
            // checkedColor="#fff"
            filterRadio="#666C78"
            checked={!formValues.special_care}
            onClick={e => onChange(e, 'special_care', 'radio', false)}
          />
        </Flex>
        <p>Please let us know about any other specific needs</p>
        <Flex flexMargin="0px 0px 40px" className="addChild-check">
          <InputBox
            type="textarea"
            margin="0px"
            rows={4}
            placeholder="For example: My child needs assistance during eating"
            value={formValues.other_specific_needs}
            onChange={e => {
              if (e.target.value.length <= TEXTAREA_MAX_CHARS) {
                onChange(e, 'other_specific_needs');
                handleWordCount(
                  e,
                  specificNeedsWordCount,
                  setSpecificNeedsWordCount,
                );
              }
            }}
            charLimit={`${specificNeedsWordCount}/${TEXTAREA_MAX_CHARS}`}
          />
        </Flex>
        <p>In case we have missed anything important, Please mention below</p>
        <Flex flexMargin="0px 0px 40px" className="addChild-check">
          <InputBox
            type="textarea"
            margin="0px"
            rows={4}
            placeholder="For example: My child needs atleast 3hours of sleep"
            value={formValues.comments}
            onChange={e => {
              if (e.target.value.length <= TEXTAREA_MAX_CHARS) {
                onChange(e, 'comments');
                handleWordCount(e, othersWordCount, setOthersWordCount);
              }
            }}
            charLimit={`${othersWordCount}/${TEXTAREA_MAX_CHARS}`}
          />
        </Flex>
        <Flex justifyEnd column>
          {error.isError ? (
            <ErrorMain>Please fill the required fields</ErrorMain>
          ) : null}
          <Flex justifyEnd className="btnWrapper">
            <Button
              type="subscribe"
              text="Cancel"
              marginRight="20px"
              onClick={() => {
                if (isDayCareDetail) setModalType('tour');
                else closeHandler();

                resetForm();
              }}
            />
            <Button
              type="mobile"
              text={`${isChildEdit ? 'Save' : 'Create'} Profile`}
              onClick={() =>
                isChildEdit ? onChildSubmit(editChildId) : onChildSubmit()
              }
              isLoading={addChildLoader}
            />
          </Flex>
        </Flex>
      </Flex>
    </AdditionalFilterWrapper>
  );
};

AddChildModal.propTypes = {
  closeHandler: PropTypes.func,
  setModalType: PropTypes.func,
  successToast: PropTypes.func,
  errorToast: PropTypes.func,
  isChildEdit: PropTypes.bool,
  editChildId: PropTypes.number,
  router: PropTypes.object,
  childModalTitle: PropTypes.string,
};

export default AddChildModal;
const ToolTipInfo = styled.div`
  display: none;
  position: absolute;
  width: 200px;
  // max-height: 200px;
  padding: 10px;
  background-color: #fff;
  color: #4b5256;
  font-family: Roboto;
  font-size: 12px;
  letter-spacing: 0;
  line-height: 19px;
  border-radius: 10px;
  bottom: 90%;
  left: -18px;
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
  overflow-y: scroll;
  .tooltip-info-wrapper {
    max-height: 400px;
    overflow-y: scroll;
    padding: 10px 0px;
  }
  &.tooltip-child {
    overflow: initial;
    width: 300px;
    bottom: 32px;
    padding: 10px 0px 10px 10px;
    @media (max-width: 500px) {
      width: 250px;
      bottom: 28px;
    }

    &::before {
      left: 59%;
      @media (max-width: 500px) {
        left: 200px;
      }
    }
  }
  &::before {
    content: '';
    display: block;
    width: 0;
    height: 0;
    position: absolute;
    width: 15px;
    height: 20px;
    transform: rotate(45deg);
    background: #fff;
    left: 85%;
    bottom: -7px;
  }
  span {
    white-space: normal;
  }
`;
const AdditionalFilterWrapper = styled.div`
  width: 710px;
  height: ${props => props.height};
  margin: auto;
  padding: 50px 25px;
  padding-left: 45px;
  border-radius: 10px;
  background: ${colors.white};
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
  position: relative;
  max-height: 90vh;
  overflow-y: scroll;

  &.addChild {
    width: 800px;

    @media (max-width: 500px) {
      max-width: 380px;
      width: 100%;
      padding: 20px;
    }
    @media (max-width: 380px) {
      max-width: 320px;
    }
    @media (max-width: 330px) {
      max-width: 300px;
    }
  }
  .addChild-input {
    @media (max-width: 500px) {
      margin-bottom: 0px;
      width: 100%;
    }

    // &>div
    // {
    //   @media (max-width: 500px) {
    //     margin-bottom:0px;
    //   }
    // }
    p {
      color: #666c78;
      font-family: 'Roboto', sans-serif;
      font-size: 14px;
      line-height: 16px;
      margin: 0px 0px 10px;
    }
    input {
      @media (max-width: 500px) {
        padding: 12px;
      }
    }
  }
  .addChild-check {
    .checkbox {
      width: 20%;
      @media (max-width: 500px) {
        width: 50%;
      }
    }
    ${Radio} {
      @media (max-width: 500px) {
        width: 50%;
      }
    }
    input[type='radio']:not(:checked) + label,
    input[type='radio']:checked + label {
      color: #666c78;
      font-family: 'Roboto', sans-serif;
      font-size: 14px;
      line-height: 25px;
      font-weight: 400;
    }
  }
  .childrenDetails {
    input {
      padding: 0px !important;
    }
    .datepick {
      @media (max-width: 500px) {
        height: 42px;
        padding-left: 10px;
      }
    }
    @media (max-width: 500px) {
      margin: 20x 0px 0px;
    }
  }
  .btnWrapper {
    @media (max-width: 500px) {
      .mobile {
        padding: 10px 12px;
        height: 40px;
        font-size: 14px;
      }
      .subscribe {
        padding: 10px 12px;
        height: 40px;
        font-size: 14px;
      }
    }
  }
`;

export const GalleryHeading = styled.div`
  margin: ${props => props.margin || '0px'};
  color: ${props => props.color || '#30333B'};
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 500;
  line-height: 28px;
  @media (max-width: 500px) {
    font-size: 18px;
    line-height: 24px;
  }
`;

const CloseModal = styled.div`
  position: absolute;
  top: 20px;
  right: 20px;
  cursor: pointer;
  @media screen and (max-width: 768px) {
    top: 1px;
    right: 7px;
  }
  .iconify {
    height: 12px;
    width: 12px;
    color: #666c78;
  }
`;

const Title = styled.div`
  position: relative;
  width: 178px;
  color: #7e888e;
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 19px;
  white-space: nowrap;
  display: flex;
  &.type {
    padding: 8px 0px;
    &:hover {
      .iconify {
        color: #60b947;
      }
      ${ToolTipInfo} {
        display: block;
      }
    }
  }
  @media (max-width: 414px) {
    width: 100%;
    margin-bottom: 20px;
    font-size: 16px;
  }
  .infoIcon {
    display: flex;
    align-items: center;
    cursor: pointer;
    margin-left: 5px;
    .iconify {
      width: 17px;
      height: 17px;
      color: #666c78;
    }
    &:hover {
      .iconify {
        color: #60b947;
      }
      & + ${ToolTipInfo} {
        display: block;
      }
    }
  }
`;

// const UploadPhoto = styled.div`
//   position: relative;
//   display: flex;
//   text-align: center;
//   align-items: center;
//   height: 100px;
//   width: 100px;
//   border: 1px dashed #c0c8cd;
//   border-radius: 50px;
//   background-color: #f2eff5;
//   font-size: 14px;
//   font-weight: bold;
//   line-height: 16px;
//   color: #a5abb9;
//   text-transform: UPPERCASE;
//   @media (max-width: 500px) {
//     height: 80px;
//     width: 80px;
//     font-size: 12px;
//   }
//   .uploadIcon {
//     position: absolute;
//     display: flex;
//     text-align: center;
//     justify-content: center;
//     align-items: center;
//     min-width: 34px;
//     width: 34px;
//     height: 34px;
//     background-color: #613a95;
//     border-radius: 50px;
//     bottom: 0px;
//     right: 0px;
//     @media (max-width: 500px) {
//       width: 28px;
//       min-width: 28px;
//       height: 28px;
//     }
//     .iconify {
//       color: #fff;
//       width: 19px;
//       height: 19px;
//       @media (max-width: 500px) {
//         width: 16px;
//         height: 16px;
//       }
//     }
//   }
// `;

// const ParentPhoto = styled.div`
//   height: 100px;
//   min-width: 100px;
//   max-width: 100px;

//   border-radius: 50%;
//   img {
//     width: 100%;
//     height: 100%;
//     border-radius: 50%;
//   }
// `;

const Error = styled.div`
  font-size: 12px;
  color: #ef6663;
`;

const ErrorMain = styled.div`
  font-size: 12px;
  color: #ef6663;
  margin-bottom: 20px;
  margin-right: 10px;
`;
