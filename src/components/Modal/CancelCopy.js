/* eslint-disable no-nested-ternary */
/* eslint-disable indent */
import React, { useRef, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import styled from 'styled-components';
import { useScheduleHook } from 'shared/hooks';
import Button from '../Button';
import colors from '../../utils/colors';
import Flex from '../common/flex';
import BranchRating from '../BranchRating';
import DatePicker from '../DatePicker';
import InputBox from '../InputBox';
import star from '../../images/star.svg';

const CancelCopy = ({
  closeHandler,
  setModalType,
  setActive,
  type,
  selectedSchedule,
  providerId,
  errorToast,
  successToast,
  setScheduleTempDataRes,
  setScheduleRes,
  formAction,
  ...props
}) => {
  const [actionType, setActionType] = useState(null);
  const [cancelReason, setCancelReason] = useState('');

  const onScheduleDeleteSuccess = () => {
    successToast('Tour cancelled successfully.');
    setActionType(null);
    setCancelReason('');
    closeHandler();
  };
  const onScheduleDeleteError = ({ message }) =>
    errorToast(message || 'Something went worng.');

  const onScheduleEditSuccess = res => {
    successToast('Re-Scheduled Successfully.');
    setActionType(null);
    setCancelReason('');
    // closeHandler();
    setModalType('confirm message');
    setScheduleRes(res);
  };
  const onScheduleEditError = ({ message }) =>
    errorToast(message || 'Something went worng.');

  const {
    // onScheduleCancel,
    // scheduleDeleteLoader,
    scheduleEditLoader,
    date,
    time,
    onChange,
    error,
    onRescheduleClick,
    onScheduleSubmit,
    setProviderId,
  } = useScheduleHook(props, {
    onScheduleDeleteSuccess,
    onScheduleDeleteError,
    onScheduleEditSuccess,
    onScheduleEditError,
  });

  useEffect(
    () => () => {
      setProviderId(null);
    },
    [],
  );

  useEffect(
    () => {
      if (selectedSchedule) {
        setProviderId(selectedSchedule.id);
      }
    },
    [selectedSchedule],
  );

  const currentRef = useRef();
  const innerRef = useRef();
  console.log(selectedSchedule, 'selectedSchedule');
  return (
    <AdditionalFilterWrapper className="tour" ref={currentRef}>
      <CloseModal
        onClick={() => {
          closeHandler();
        }}
      >
        {' '}
        <span
          className="iconify"
          data-icon="bytesize:close"
          data-inline="false"
        />
      </CloseModal>
      {formAction === 'forceCancel' ? (
        <>
          <GalleryHeading margin="0px 0px 25px">
            {(!actionType || actionType === 'edit') &&
              'Your tour scheduled for'}
            {actionType === 'cancel' &&
              'Are you sure you want to cancel the tour?'}
          </GalleryHeading>
          <GallerySubHeading margin="0px 0px 15px">
            {actionType === 'cancel' && 'We will notify the director.'}
          </GallerySubHeading>
          {(!actionType || actionType === 'edit') && (
            <CancelDetails>
              <Flex
                column
                className="cancelDate"
                flexPadding="20px"
                flexBorderdashed="1px solid #fff"
              >
                <h2>
                  <span>Date:</span>
                  {moment(selectedSchedule.date).format('dddd, MMM DD')},{' '}
                  {selectedSchedule.time}
                </h2>
                <h2>
                  <span>Tour Id:</span>
                  {selectedSchedule.tour_id}
                </h2>
              </Flex>
              <Flex
                column
                className="cancelDate"
                flexPadding="20px"
                alignCenter
              >
                <h2>{selectedSchedule.provider_detail.business_name}</h2>
                <BranchRating
                  branchname={selectedSchedule.provider_detail.location_address}
                  ratingNum="4.3"
                  starImage={star}
                />
              </Flex>
            </CancelDetails>
          )}
          {!actionType ? (
            <GalleryHeading fontSize="19px" margin="0px 0px 25px">
              Would you like to cancel trip?
            </GalleryHeading>
          ) : null}
          {type === 'schedule' &&
            !actionType && (
              <Flex>
                <Button
                  type="mobile"
                  text="Cancel Tour"
                  fullwidth
                  marginRight="20px"
                  onClick={() => setActionType('cancel')}
                />
              </Flex>
            )}
          {actionType === 'cancel' && (
            <Flex flexMargin="0px 0px 15px">
              <InputBox
                type="textarea"
                margin="0px"
                rows={4}
                value={cancelReason}
                onChange={e => setCancelReason(e.target.value)}
              />
            </Flex>
          )}
          {actionType === 'edit' && (
            <DateHeader>Please select time and date for your visit.</DateHeader>
          )}
          {actionType === 'edit' && (
            <Flex
              flexMargin="0px 0px 20px"
              justifyBetween
              className="datePickerContent"
            >
              <Flex column>
                <DatePicker
                  value={date ? moment(date).format('DD/MM/YYYY') : ''}
                  iconName="feather:calendar"
                  placeholder="Select Date"
                  onChange={e =>
                    onChange('date', moment(e).format('YYYY-MM-DD'))
                  }
                />
                <Error>{error.date}</Error>
              </Flex>
              <Flex column>
                <DatePicker
                  value={time}
                  iconName="bytesize:clock"
                  placeholder="Select Time"
                  isTime
                  onChange={e => onChange('time', moment(e).format('HH:mm'))}
                />
                <Error>{error.time}</Error>
              </Flex>
            </Flex>
          )}
          {actionType === 'cancel' && (
            <Flex>
              <Button
                type="subscribe"
                text="Back"
                marginRight="20px"
                fullwidth
                onClick={() => setActionType(null)}
              />
              <Button
                type="mobile"
                text="Cancel Tour"
                onClick={() => {
                  // onScheduleCancel({
                  //   reason: cancelReason,
                  //   id: selectedSchedule.id,
                  // })
                  setModalType('cancelConfirmation');
                  setScheduleTempDataRes({
                    name: selectedSchedule.provider_detail.business_name,
                    date: `${moment(selectedSchedule.date).format(
                      'dddd, MMM DD',
                    )}, ${selectedSchedule.time}`,
                    tourid: selectedSchedule.tour_id,
                    reason: cancelReason,
                    id: selectedSchedule.id,
                  });

                  setActionType(null);
                  setCancelReason('');
                }}
                // isLoading={scheduleDeleteLoader}
              />
            </Flex>
          )}
        </>
      ) : formAction === 'forceConfirm' ? (
        <>
          <GalleryHeading margin="0px 0px 25px">
            {(!actionType || actionType === 'edit') &&
              'Your tour scheduled for'}
            {actionType === 'cancel' &&
              'Are you sure you want to cancel the tour?'}
          </GalleryHeading>
          <GallerySubHeading margin="0px 0px 15px">
            {actionType === 'cancel' && 'We will notify the director.'}
          </GallerySubHeading>
          {(!actionType || actionType === 'edit') && (
            <CancelDetails>
              <Flex
                column
                className="cancelDate"
                flexPadding="20px"
                flexBorderdashed="1px solid #fff"
              >
                <h2>
                  <span>Date:</span>
                  {moment(selectedSchedule.date).format('dddd, MMM DD')},{' '}
                  {selectedSchedule.time}
                </h2>
                <h2>
                  <span>Tour Id:</span>
                  {selectedSchedule.tour_id}
                </h2>
              </Flex>
              <Flex
                column
                className="cancelDate"
                flexPadding="20px"
                alignCenter
              >
                <h2>{selectedSchedule.provider_detail.business_name}</h2>
                <BranchRating
                  branchname={selectedSchedule.provider_detail.location_address}
                  ratingNum="4.3"
                  starImage={star}
                />
              </Flex>
            </CancelDetails>
          )}
          {!actionType ? (
            <GalleryHeading fontSize="19px" margin="0px 0px 25px">
              Would you like to re-schedule your visit ?
            </GalleryHeading>
          ) : null}
          {type === 'schedule' &&
            !actionType && (
              <Flex>
                <Button
                  type="mobile"
                  text="Re-Schedule"
                  fullwidth
                  onClick={() => {
                    setActionType('edit');
                    onRescheduleClick(selectedSchedule.id);
                  }}
                />
              </Flex>
            )}

          {actionType === 'edit' && (
            <DateHeader>Please select time and date for your visit.</DateHeader>
          )}
          {actionType === 'edit' && (
            <Flex
              flexMargin="0px 0px 20px"
              justifyBetween
              className="datePickerContent"
            >
              <Flex column>
                <DatePicker
                  value={date ? moment(date).format('DD/MM/YYYY') : ''}
                  iconName="feather:calendar"
                  placeholder="Select Date"
                  disabled
                />
                <Error>{error.date}</Error>
              </Flex>
              <Flex column>
                <DatePicker
                  value={time}
                  iconName="bytesize:clock"
                  placeholder="Select Time"
                  isTime
                  disabled
                />
                <Error>{error.time}</Error>
              </Flex>
            </Flex>
          )}

          {actionType === 'edit' && (
            <Flex>
              <Button
                type="mobile"
                text="Confirm Re-Schedule"
                fullwidth
                onClick={() =>
                  onScheduleSubmit(selectedSchedule.id, false, {
                    extraPayload: {
                      old_date: moment(selectedSchedule.old_date).format(
                        'YYYY-MM-DD',
                      ),
                      old_time: selectedSchedule.old_time,
                      schedule_type: 'schedule',
                      schedule_status: 'confirmed',
                      is_admin_schduled: false,
                      provider: selectedSchedule.provider,
                    },
                  })
                }
                isLoading={scheduleEditLoader}
              />
            </Flex>
          )}
          {type === 'confirmSchedule' && (
            <>
              <OrgName margin="50px 0px 20px">
                Please select the date and time of you’re visit
              </OrgName>
              <Flex
                flexMargin="0px 0px 20px"
                justifyBetween
                className="datePicker"
              >
                <DatePicker innerRef={innerRef} iconName="feather:calendar" />
                <DatePicker innerRef={innerRef} iconName="bytesize:clock" />
              </Flex>
              <Flex>
                <Button type="mobile" text="Confirm Re-Schedule" fullwidth />
              </Flex>
            </>
          )}
        </>
      ) : formAction === 'forceReschule' ? (
        <>
          <GalleryHeading margin="0px 0px 25px">
            You already have an active schedule
          </GalleryHeading>
          <GallerySubHeading margin="0px 0px 15px">
            {actionType === 'cancel' && 'We will notify the director.'}
          </GallerySubHeading>
          {(!actionType || actionType === 'edit') && (
            <CancelDetails>
              <Flex
                column
                className="cancelDate"
                flexPadding="20px"
                flexBorderdashed="1px solid #fff"
              >
                <h2>
                  <span>Date:</span>
                  {moment(selectedSchedule.date).format('dddd, MMM DD')},{' '}
                  {selectedSchedule.time}
                </h2>
                <h2>
                  <span>Tour Id:</span>
                  {selectedSchedule.tour_id}
                </h2>
              </Flex>
              <Flex
                column
                className="cancelDate"
                flexPadding="20px"
                alignCenter
              >
                <h2>{selectedSchedule.provider_detail.business_name}</h2>
                <BranchRating
                  branchname={selectedSchedule.provider_detail.location_address}
                  ratingNum="4.3"
                  starImage={star}
                />
              </Flex>
            </CancelDetails>
          )}
          {!actionType ? (
            <GalleryHeading fontSize="19px" margin="0px 0px 25px">
              Would you like to re-schedule your visit ?
            </GalleryHeading>
          ) : null}
          {type === 'schedule' &&
            !actionType && (
              <Flex>
                <Button
                  type="mobile"
                  text="Re-Schedule"
                  fullwidth
                  onClick={() => {
                    setActionType('edit');
                    onRescheduleClick(selectedSchedule.id);
                  }}
                />
              </Flex>
            )}

          {actionType === 'edit' && (
            <DateHeader>Please select time and date for your visit.</DateHeader>
          )}
          {actionType === 'edit' && (
            <Flex
              flexMargin="0px 0px 20px"
              justifyBetween
              className="datePickerContent"
            >
              <Flex column>
                <DatePicker
                  value={date ? moment(date).format('DD/MM/YYYY') : ''}
                  iconName="feather:calendar"
                  placeholder="Select Date"
                  onChange={e =>
                    onChange('date', moment(e).format('YYYY-MM-DD'))
                  }
                />
                <Error>{error.date}</Error>
              </Flex>
              <Flex column>
                <DatePicker
                  value={time}
                  iconName="bytesize:clock"
                  placeholder="Select Time"
                  isTime
                  onChange={e => onChange('time', moment(e).format('HH:mm'))}
                />
                <Error>{error.time}</Error>
              </Flex>
            </Flex>
          )}

          {actionType === 'edit' && (
            <Flex>
              <Button
                type="mobile"
                text="Confirm Re-Schedule"
                fullwidth
                onClick={() => {
                  onScheduleSubmit(selectedSchedule.id, false, {
                    extraPayload: {
                      old_date: moment(selectedSchedule.date).format(
                        'YYYY-MM-DD',
                      ),
                      old_time: selectedSchedule.time,
                      // schedule_type: 'schedule',
                      // schedule_status: 'confirmed',
                      provider: selectedSchedule.provider,
                      is_admin_schduled: false,
                    },
                  });
                }}
                isLoading={scheduleEditLoader}
              />
            </Flex>
          )}
          {type === 'confirmSchedule' && (
            <>
              <OrgName margin="50px 0px 20px">
                Please select the date and time of you’re visit
              </OrgName>
              <Flex
                flexMargin="0px 0px 20px"
                justifyBetween
                className="datePicker"
              >
                <DatePicker innerRef={innerRef} iconName="feather:calendar" />
                <DatePicker innerRef={innerRef} iconName="bytesize:clock" />
              </Flex>
              <Flex>
                <Button type="mobile" text="Confirm Re-Schedule" fullwidth />
              </Flex>
            </>
          )}
        </>
      ) : (
        <>
          <GalleryHeading margin="0px 0px 25px">
            {(!actionType || actionType === 'edit') &&
              'Your tour scheduled for'}
            {actionType === 'cancel' &&
              'Are you sure you want to cancel the tour?'}
          </GalleryHeading>
          <GallerySubHeading margin="0px 0px 15px">
            {actionType === 'cancel' && 'We will notify the director.'}
          </GallerySubHeading>
          {(!actionType || actionType === 'edit') &&
            selectedSchedule && (
              <CancelDetails>
                <Flex
                  column
                  className="cancelDate"
                  flexPadding="20px"
                  flexBorderdashed="1px solid #fff"
                >
                  <h2>
                    <span>Date:</span>
                    {moment(selectedSchedule.date).format('dddd, MMM DD')},{' '}
                    {selectedSchedule.time}
                  </h2>
                  <h2>
                    <span>Tour Id:</span>
                    {selectedSchedule.tour_id}
                  </h2>
                </Flex>
                <Flex
                  column
                  className="cancelDate"
                  flexPadding="20px"
                  alignCenter
                >
                  <h2>{selectedSchedule.provider_detail.business_name}</h2>
                  <BranchRating
                    branchname={
                      selectedSchedule.provider_detail.location_address
                    }
                    ratingNum="4.3"
                    starImage={star}
                  />
                </Flex>
              </CancelDetails>
            )}
          {!actionType ? (
            <GalleryHeading fontSize="19px" margin="0px 0px 25px">
              Would you like to re-schedule your visit or cancel trip?
            </GalleryHeading>
          ) : null}
          {type === 'schedule' &&
            !actionType && (
              <Flex>
                <Button
                  type="subscribe"
                  text="Cancel Tour"
                  marginRight="20px"
                  onClick={() => setActionType('cancel')}
                />
                <Button
                  type="mobile"
                  text="Re-Schedule"
                  onClick={() => {
                    setActionType('edit');
                    onRescheduleClick(selectedSchedule.id);
                  }}
                />
              </Flex>
            )}
          {actionType === 'cancel' && (
            <Flex flexMargin="0px 0px 15px">
              <InputBox
                type="textarea"
                margin="0px"
                rows={4}
                value={cancelReason}
                onChange={e => setCancelReason(e.target.value)}
              />
            </Flex>
          )}
          {actionType === 'edit' && (
            <DateHeader>Please select time and date for your visit.</DateHeader>
          )}
          {actionType === 'edit' && (
            <Flex
              flexMargin="0px 0px 20px"
              justifyBetween
              className="datePickerContent"
            >
              <Flex column>
                <DatePicker
                  value={date ? moment(date).format('DD/MM/YYYY') : ''}
                  iconName="feather:calendar"
                  placeholder="Select Date"
                  onChange={e =>
                    onChange('date', moment(e).format('YYYY-MM-DD'))
                  }
                />
                <Error>{error.date}</Error>
              </Flex>
              <Flex column>
                <DatePicker
                  value={time}
                  iconName="bytesize:clock"
                  placeholder="Select Time"
                  isTime
                  onChange={e => onChange('time', moment(e).format('HH:mm'))}
                />
                <Error>{error.time}</Error>
              </Flex>
            </Flex>
          )}
          {actionType === 'cancel' && (
            <Flex>
              <Button
                type="subscribe"
                text="Back"
                marginRight="20px"
                fullwidth
                onClick={() => setActionType(null)}
              />
              <Button
                type="mobile"
                text="Cancel Tour"
                onClick={() => {
                  // onScheduleCancel({
                  //   reason: cancelReason,
                  //   id: selectedSchedule.id,
                  // })
                  setModalType('cancelConfirmation');
                  setScheduleTempDataRes({
                    name: selectedSchedule.provider_detail.business_name,
                    date: `${moment(selectedSchedule.date).format(
                      'dddd, MMM DD',
                    )}, ${selectedSchedule.time}`,
                    tourid: selectedSchedule.tour_id,
                    reason: cancelReason,
                    id: selectedSchedule.id,
                  });

                  setActionType(null);
                  setCancelReason('');
                }}
                // isLoading={scheduleDeleteLoader}
              />
            </Flex>
          )}
          {actionType === 'edit' && (
            <Flex>
              <Button
                type="mobile"
                text="Confirm Re-Schedule"
                fullwidth
                onClick={() =>
                  onScheduleSubmit(selectedSchedule.id, false, {
                    extraPayload: {
                      old_date: moment(selectedSchedule.date).format(
                        'YYYY-MM-DD',
                      ),
                      old_time: selectedSchedule.time,
                      provider: selectedSchedule.provider,
                      is_admin_schduled: false,
                    },
                  })
                }
                isLoading={scheduleEditLoader}
              />
            </Flex>
          )}
          {type === 'confirmSchedule' && (
            <>
              <OrgName margin="50px 0px 20px">
                Please select the date and time of you’re visit
              </OrgName>
              <Flex
                flexMargin="0px 0px 20px"
                justifyBetween
                className="datePicker"
              >
                <DatePicker innerRef={innerRef} iconName="feather:calendar" />
                <DatePicker innerRef={innerRef} iconName="bytesize:clock" />
              </Flex>
              <Flex>
                <Button type="mobile" text="Confirm Re-Schedule" fullwidth />
              </Flex>
            </>
          )}
        </>
      )}
    </AdditionalFilterWrapper>
  );
};

CancelCopy.propTypes = {
  closeHandler: PropTypes.func,
  type: PropTypes.string,
  formAction: PropTypes.string,
  setModalType: PropTypes.func,
  setActive: PropTypes.func,
  errorToast: PropTypes.func,
  successToast: PropTypes.func,
  selectedSchedule: PropTypes.object,
  providerId: PropTypes.number,
  setScheduleTempDataRes: PropTypes.func,
  setScheduleRes: PropTypes.func,
};

export default CancelCopy;

const AdditionalFilterWrapper = styled.div`
  width: 710px;
  height: ${props => props.height};
  margin: 5% auto;
  padding: 25px;
  padding-left: 45px;
  border-radius: 10px;
  background: ${colors.white};
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
  position: relative;

  &.tour {
    max-width: 500px;
    width: 100%;
    padding: 50px 48px;
    margin: 25vh auto 0px;
    @media screen and (max-width: 768px) {
      max-width: 290px;
      width: 100%;
      padding: 20px;
      margin: 20% auto;
    }
    @media (max-width: 500px) {
      max-width: 380px;
      margin: 10vh auto 0px;
      padding: 30px 24px;
    }
    @media (max-width: 400px) {
      max-width: 310px;
    }
  }

  .subscribe {
    @media (max-width: 500px) {
      font-size: 14px;
      height: 46px;
      padding: 0px 22px;
      width: 40%;
    }
  }
  .datePicker .react-datepicker__input-container {
    input[type='text'] {
      font-size: 16px;
    }
    input[type='text']::-webkit-input-placeholder {
      font-size: 16px;
    }
  }
  .datePickerContent {
    @media (max-width: 500px) {
      flex-direction: column;
    }
    .datepick {
      @media (max-width: 500px) {
        height: 40px;
        margin-bottom: 10px;
      }
    }
  }
`;

export const GalleryHeading = styled.div`
  margin: ${props => props.margin || '0px'};
  color: ${props => props.color || '#30333B'};
  font-family: 'Quicksand', sans-serif;
  font-size: ${({ fontSize }) => fontSize || '22px'};
  font-weight: 500;
  line-height: 28px;
  @media (max-width: 500px) {
    font-size: 16px !important;
    line-height: 24px;
  }
`;

export const GallerySubHeading = styled.div`
  margin: ${props => props.margin || '0px'};
  color: ${props => props.color || '#30333B'};
  font-family: 'Quicksand', sans-serif;
  font-size: 16px;
  font-weight: 300;
  line-height: 28px;
`;

const CloseModal = styled.div`
  position: absolute;
  top: 20px;
  right: 20px;
  cursor: pointer;
  @media screen and (max-width: 768px) {
    top: 1px;
    right: 7px;
  }
  .iconify {
    height: 12px;
    width: 12px;
    color: #666c78;
  }
`;

export const CancelDetails = styled.div`
  width: 100%;
  border-radius: 10px;
  background-color: #f2eff5;
  margin-bottom: 20px;
  .cancelDate {
    @media (max-width: 500px) {
      padding: 12px;
    }
    h2 {
      width: 100%;
      color: #30333b;
      font-family: 'Quicksand', sans-serif;
      font-size: 16px;
      font-weight: bold;
      line-height: 26px;
      text-align: center;
      margin: 0px;
      span {
        font-weight: 300;
        margin-right: 4px;
      }
      @media (max-width: 500px) {
        font-size: 14px !important;
        line-height: 24px;
      }
      &:first-child {
        @media (max-width: 500px) {
          margin-bottom: 10px;
        }
      }
    }
  }
`;
const OrgName = styled.div`
  color: #30333b;
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 19px;
  margin: ${props => props.margin || '0px'};
`;

const Error = styled.div`
  font-size: 12px;
  color: red;
`;

const DateHeader = styled.div`
  margin-bottom: 10px;
`;
