/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Default from './DefaultModal';
import TestimonialModal from './TestimonialModal';
import TeacherAndStaffModal from './TeacherAndStaffModal';
import HandbookModal from './HandbookModal';
import GalleryModal from './GalleryModal';
import TourModal from './TourModal';
import EmailModal from './EmailModal';
import CancelTour from './CancelTour';
// import ConfirmModal from './ConfirmModal';ReScheduleModal
import ReScheduleModal from './ReSchedule';
import ThankYouModal from './ThankYouModal';
import CostModal from './CostModal';
import VerifyMobileModal from './VerifyMobileModal';
import EnterMailModal from './EnterMailModal';
import SignUpModal from './WelcomeModal';
import LoginModal from './LoginModal';
import OnboardingModal from './OnboardingModal';
import CreateParentAccountModal from './CreateParentAccountModal';
import EnquiryModal from './EnquiryModal';
import AddChildModal from './AddChild';
import CancelCopy from './CancelCopy';
import ConfirmationModal from './ConfirmationModal';
import VideoModal from './VideoModal';
import SubmitDetailsModal from './SubmitDetails';
import RestrictionModal from './RestrictionModal';
import OnlineProgramModal from './OnlineProgramModal';
import OnlineProgramBatches from './OnlineProgramBatches';
import PrefferedCityModal from './PrefferedCityModal';
import ShareModal from './ShareModal';
import CallBackModal from './CallBackModal';
import SuccessModal from './SuccessModal';
import AdmissionOpenModal from './AdmissionOpenModal';
import { Photo } from '../PhotoCard/styled';

// function useOnClick(ref, handler) {
//   useEffect(() => {
//     const listener = event => {
//       // Do nothing if clicking ref's element or descendent elements
//       if (!ref.current || ref.current.contains(event.target)) {
//         return;
//       }

//       handler(event);
//     };

//     document.addEventListener('mousedown', listener);

//     return () => {
//       document.removeEventListener('mousedown', listener);
//     };
//   }, []); // Empty array ensures that effect is only run on mount and unmount
// }

function Modal({
  type,
  setActive,
  emailType,
  setModalType,
  closeHandler,
  ...props
}) {
  const ref = useRef();
  const commonProps = { closeHandler, ...props };
  const [isMobileEdit, setIsMobileEdit] = useState(false);
  const [signUpName, setSignUpName] = useState('');
  const [activeAuthType, setActiveAuthType] = useState('');
  const [scheduleRes, setScheduleRes] = useState({});

  const [schduleTempData, setScheduleTempDataRes] = useState({});

  React.useEffect(() => {
    document.body.style.overflow = 'hidden';
    return () => {
      document.body.style.overflow = 'unset';
    };
  }, []);

  // useOnClick(ref, () => closeHandler());

  return (
    <>
      <ModalWrapper ref={ref}>
        <AdditionalFilterModal>
          {type === 'default' && <Default {...commonProps} />}
          {type === 'testimonial' && <TestimonialModal {...commonProps} />}
          {type === 'teacher & staff' && (
            <TeacherAndStaffModal {...commonProps} />
          )}
          {type === 'handbook' && <HandbookModal {...commonProps} />}
          {type === 'gallery' && (
            <GalleryModal
              mediaResponse={props.mediaResponse}
              {...commonProps}
              {...props}
            />
          )}
          {type === 'tour' && (
            <TourModal
              {...commonProps}
              setActive={setActive}
              setModalType={setModalType}
              setScheduleRes={setScheduleRes}
              {...props}
            />
          )}
          {type === 'email' && (
            <EmailModal emailType={emailType} {...commonProps} />
          )}
          {type === 'confirm message' && (
            // <ConfirmModal
            //   {...commonProps}
            //   setActive={setActive}
            //   setModalType={setModalType}
            //   scheduleRes={scheduleRes}
            //   setScheduleRes={setScheduleRes}
            // />
            <ReScheduleModal
              {...commonProps}
              setActive={setActive}
              setModalType={setModalType}
              scheduleRes={scheduleRes}
              setScheduleRes={setScheduleRes}
            />
          )}
          {type === 'enquiry' && (
            <EnquiryModal
              {...commonProps}
              setActive={setActive}
              setModalType={setModalType}
              {...props}
            />
          )}
          {type === 'contactUsSuccess' && (
            <EnquiryModal
              {...commonProps}
              setActive={setActive}
              setModalType={setModalType}
              type={type}
              {...props}
            />
          )}
          {type === 'thankyou' && (
            <ThankYouModal
              {...commonProps}
              setActive={setActive}
              setModalType={setModalType}
              {...props}
            />
          )}
          {type === 'callback' && (
            <CallBackModal
              {...commonProps}
              setActive={setActive}
              setModalType={setModalType}
              {...props}
            />
          )}
          {type === 'bookSuccess' && (
            <SuccessModal
              {...commonProps}
              setActive={setActive}
              setModalType={setModalType}
              {...props}
            />
          )}
          {type === 'cost' && <CostModal {...commonProps} />}
          {type === 'creatParentAccount' && (
            <CreateParentAccountModal
              {...commonProps}
              setModalType={setModalType}
              setIsMobileEdit={() => setIsMobileEdit(false)}
              isEdit={isMobileEdit}
              setActiveAuthType={() => setActiveAuthType('creatParentAccount')}
            />
          )}
          {type === 'verifyMobile' && (
            <VerifyMobileModal
              {...commonProps}
              setModalType={setModalType}
              setActive={setActive}
              setIsMobileEdit={() => setIsMobileEdit(true)}
              activeAuthType={activeAuthType}
              signUpName={signUpName}
            />
          )}
          {type === 'enterEmail' && <EnterMailModal {...commonProps} />}
          {type === 'login' && (
            <LoginModal
              {...commonProps}
              setModalType={setModalType}
              isEdit={isMobileEdit}
              setActiveAuthType={() => setActiveAuthType('login')}
              {...props}
            />
          )}
          {/* {type === 'signup' && (
            <SignUpModal
              {...commonProps}
              setModalType={setModalType}
              isEdit={isMobileEdit}
              setActiveAuthType={() => setActiveAuthType('signup')}
              name={signUpName}
              setName={setSignUpName}
              {...props}
            />
          )} */}
          {type === 'onboarding' && (
            <OnboardingModal
              setActive={setActive}
              setModalType={setModalType}
              {...props}
            />
          )}
          {type === 'addChild' && (
            <AddChildModal
              {...commonProps}
              setActive={setActive}
              setModalType={setModalType}
              {...props}
            />
          )}
          {type === 'scheduleOptions' && (
            <CancelCopy
              {...commonProps}
              setActive={setActive}
              setModalType={setModalType}
              schduleTempData={schduleTempData}
              setScheduleTempDataRes={setScheduleTempDataRes}
              setScheduleRes={setScheduleRes}
              type="schedule"
              {...props}
            />
          )}
          {type === 'reSchedule' && (
            <CancelCopy
              {...commonProps}
              setActive={setActive}
              setModalType={setModalType}
              type="confirmSchedule"
              schduleTempData={schduleTempData}
              setScheduleTempDataRes={setScheduleTempDataRes}
              setScheduleRes={setScheduleRes}
              {...props}
            />
          )}
          {type === 'forceCancel' && (
            <CancelCopy
              {...commonProps}
              setActive={setActive}
              setModalType={setModalType}
              type="schedule"
              formAction="forceCancel"
              schduleTempData={schduleTempData}
              setScheduleTempDataRes={setScheduleTempDataRes}
              setScheduleRes={setScheduleRes}
              {...props}
            />
          )}
          {type === 'forceConfirm' && (
            <CancelCopy
              {...commonProps}
              setActive={setActive}
              setModalType={setModalType}
              type="schedule"
              formAction="forceConfirm"
              schduleTempData={schduleTempData}
              setScheduleTempDataRes={setScheduleTempDataRes}
              setScheduleRes={setScheduleRes}
              {...props}
            />
          )}
          {type === 'forceReschule' && (
            <CancelCopy
              {...commonProps}
              setActive={setActive}
              setModalType={setModalType}
              type="schedule"
              formAction="forceReschule"
              schduleTempData={schduleTempData}
              setScheduleTempDataRes={setScheduleTempDataRes}
              setScheduleRes={setScheduleRes}
              {...props}
            />
          )}
          {type === 'confirmation' && (
            <ConfirmationModal
              {...commonProps}
              setActive={setActive}
              setModalType={setModalType}
              type="confirmSchedule"
              {...props}
            />
          )}
          {type === 'video' && (
            <VideoModal
              {...commonProps}
              setActive={setActive}
              setModalType={setModalType}
              {...props}
            />
          )}
          {type === 'cancelConfirmation' && (
            <CancelTour
              {...commonProps}
              setActive={setActive}
              setModalType={setModalType}
              schduleTempData={schduleTempData}
              setScheduleTempDataRes={setScheduleTempDataRes}
              {...props}
            />
          )}
          {type === 'provider' && (
            <SubmitDetailsModal
              {...commonProps}
              setActive={setActive}
              setModalType={setModalType}
              {...props}
            />
          )}
          {type === 'restriction' && (
            <RestrictionModal
              {...commonProps}
              setActive={setActive}
              setModalType={setModalType}
              {...props}
            />
          )}
          {type === 'onlineProgram' && (
            <OnlineProgramModal
              {...commonProps}
              setActive={setActive}
              setModalType={setModalType}
              {...props}
            />
          )}
          {type === 'onlineProgramBatches' && (
            <OnlineProgramBatches
              {...commonProps}
              setActive={setActive}
              setModalType={setModalType}
              {...props}
            />
          )}
          {type === 'prefferedCity' && (
            <PrefferedCityModal
              {...commonProps}
              setActive={setActive}
              setModalType={setModalType}
              {...props}
            />
          )}
          {type === 'shareModal' && (
            <ShareModal
              {...commonProps}
              setActive={setActive}
              setModalType={setModalType}
              {...props}
            />
          )}
          {type === 'signup' && (
            <AdmissionOpenModal
            {...commonProps}
            setModalType={setModalType}
            isEdit={isMobileEdit}
            setActiveAuthType={() => setActiveAuthType('signup')}
            name={signUpName}
            setName={setSignUpName}
            {...props}
            />
          )}
        </AdditionalFilterModal>
      </ModalWrapper>
    </>
  );
}
Modal.propTypes = {
  type: PropTypes.string,
  setActive: PropTypes.func,
  setModalType: PropTypes.func,
  closeHandler: PropTypes.func,
  emailType: PropTypes.string,
  mediaResponse: PropTypes.object,
};
export default Modal;

const ModalWrapper = styled.section``;
const AdditionalFilterModal = styled.div`
  position: fixed;
  // display:flex;
  top: 0;
  left: 0;
  width: 100%;
  min-height: 100vh;
  height: 100%;
  background: rgba(0, 0, 0, 0.7);
  z-index: 10000000;
  overflow: auto;
  display: flex;
  align-items: center;
  justify-content: center;
  // transition: opacity 500ms;
  // visibility: hidden;
  // opacity: 0;
  // &:target
  // {
  //   visibility: visible;
  //   opacity: 1;
  // }
  // .tour , .welcome , .addChild
  // {
  //   transition: all 5s ease-in-out;
  // }
  // &:target .tour {
  //   animation: popup 0.7s;
  // }
  // &:target .welcome {
  //   animation: popup 0.7s;
  // }
  // &:target .addChild {
  //   animation: popup 0.7s;
  // }
  // @keyframes popup {
  //   0% {
  //     transform: scale(1);
  //   }
  //   50% {
  //     transform: scale(1.4);
  //   }
  //   60% {
  //     transform: scale(1.1);
  //   }
  //   70% {
  //     transform: scale(1.2);
  //   }
  //   80% {
  //     transform: scale(1);
  //   }
  //   90% {
  //     transform: scale(1.1);
  //   }
  //   100% {
  //     transform: scale(1);
  //   }
  // }
  .gallerySection {
    background-color: rgba(0, 0, 0, 0.9);
    min-height: 100%;
    max-height: 100vh;
    padding: 20px 0px;
    overflow: hidden;
    position: relative;
    .closeGallery .iconify {
      position: absolute;
      height: 22px;
      width: 22px;
      top: 30px;
      right: 30px;
      color: #fff;
      cursor: pointer;
      @media (max-width: 500px) {
        height: 14px;
        width: 14px;
      }
    }
  }
  .galleryContent {
    width: 100vw;
    max-width: 1220px;
    margin: 0px auto;
    padding: 0px 20px;
  }
  .galleryContainer {
    padding-top: 23px;
    display: flex;
    justify-content: space-between;
    @media (max-width: 1080px) {
      padding-left: 20px;
    }
    @media (max-width: 500px) {
      padding: 24px;
      flex-wrap: wrap;
    }
    @media (max-width: 361px) {
      padding: 15px;
    }
    &-left {
      width: 83%;
      position: relative;
      @media (max-width: 800px) {
        width: 78%;
      }
      @media (max-width: 500px) {
        width: 100%;
        height: 225px;
      }
    }
    &-right {
      width: 15%;
      @media (max-width: 800px) {
        width: 20%;
      }
      @media (max-width: 500px) {
        width: 100%;
        margin-top: 60px;
        overflow: auto;
      }
      .popupScroll {
        width: 100%;
        max-height: 87vh;
        overflow-y: scroll;
        padding-right: 20px;
        @media (max-width: 500px) {
          padding-right: 0px;
        }
      }
    }
    &-pagination {
      position: absolute;
      left: 10px;
      margin-top: 15px;
      font-family: 'Roboto', sans-serif;
      color: #ffffff;
      font-size: 14px;
      line-height: 22px;
    }

    &-slider {
      width: 100%;
      height: 582px;
      @media (max-width: 500px) {
        height: 100%;
      }
      .slick-arrow {
        top: 104%;
        height: 30px;
        @media (max-width: 500px) {
          top: 112%;
        }
      }
      .slick-prev {
        left: 93%;
        @media (max-width: 500px) {
          left: 84%;
        }
      }
      .slick-prev:before {
        font-family: FontAwesome;
        content: '\f104';
        font-size: 32px;
        opacity: 1;
      }
      .slick-next {
        right: 0px;
      }
      .slick-next:before {
        font-family: FontAwesome;
        content: '\f105';
        font-size: 32px;
        opacity: 1;
      }
      .slick-slider {
        @media (max-width: 500px) {
          height: 100%;
        }
      }
      .slick-list {
        @media (max-width: 500px) {
          height: 100%;
          border-radius: 12px;
        }
      }
      .slick-track {
        @media (max-width: 500px) {
          height: 100%;
        }
      }
      .slick-slide {
        height: calc(100vh - 180px);
        @media (max-width: 500px) {
          height: 100%;
        }
        & > div {
          height: 100%;
        }
      }
    }
  }
  .galleryWrapper {
    display: flex;
    flex-wrap: wrap;
    @media (max-width: 500px) {
      flex-wrap: nowrap;
      overflow-x: auto;
    }
    ${Photo} {
      @media (max-width: 500px) {
        height: 74px;
        min-width: 100px;
        width: 100px;
        margin: 0px 10px 20px 0px;
      }
    }
  }
`;
