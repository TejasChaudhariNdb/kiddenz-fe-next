/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
/* eslint-disable no-nested-ternary */
/* eslint-disable no-constant-condition */
import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Button from '../Button';
import colors from '../../utils/colors';
import InputBox from '../InputBox';
import Flex from '../common/flex';
import welcomeImage from '../../images/thumbsup.png';
import { useRegisterHook } from '../../shared/hooks';

const AdmissionOpenModal = ({
  closeHandler,
  setModalType,
  isEdit,
  setIsMobileEdit,
  setActiveAuthType,
  ...props
}) => {
  const currentRef = useRef();

  const onError = ({ message }) => {
    props.errorToast(message || 'Something went wrong,');
  };

  const onSuccess = () => {
    props.successToast('OTP sent successfully.');
    setModalType('verifyMobile');
  };

  const {
    name: { value: name },
    mobile: { value: mobileNo },
    email: { value: emailAddress },
    locationAddress: { value: locationAddress },
    subscription: { onChange: onChangeSubscribeEmail, value: isSubscribed },
    onChange,
    onBlur,
    error,
    onSubmit,
    registerLoader,
  } = useRegisterHook(props, { onError, onSuccess, isEdit });

  const onEnterButtonPressed = e => {
    if (e.key === 'Enter') {
      e.preventDefault();
      setActiveAuthType();
      onSubmit();
    }
  };
  return (
    <AdditionalFilterWrapper className="welcome">
      {/* {!props.disableLoginModalCloseIcon && ( */}
      <CloseModal onClick={closeHandler}>
        {' '}
        <span
          className="iconify"
          data-icon="bytesize:close"
          data-inline="false"
        />
      </CloseModal>
      {/* )} */}

      <ModalLeft>
        <ModalTitle className="creatAccount">ADMISSION OPEN</ModalTitle>
        <ModalSubTitle>
          Register below to get attractive discounts of upto ₹8000, Fee details,
          & personal counselling for Preschool & Daycare admissions.
        </ModalSubTitle>
        <div className="scheduleTour">
          <InputBox
            name="name"
            placeholder="Your Name"
            type="text"
            margin="0px 0px 20px 0px"
            value={name}
            onChange={onChange}
            onBlur={onBlur}
            error={error.name}
            onKeyPress={onEnterButtonPressed}
          />
          <InputBox
            name="mobile"
            placeholder="0000000000"
            type="tel"
            margin="0px 0px 20px 0px"
            value={mobileNo}
            onChange={onChange}
            onBlur={onBlur}
            error={error.mobile}
            onKeyPress={onEnterButtonPressed}
          />
          <InputBox
            name="locationAddress"
            type="text"
            placeholder="Address (Your Location)"
            margin="0px 0px 20px 0px"
            value={locationAddress}
            onChange={onChange}
            onBlur={onBlur}
            error={error.locationAddress}
            onKeyPress={onEnterButtonPressed}
          />
          <Button
            text="Submit"
            isLoading={registerLoader}
            type="secondary"
            onClick={() => {
              setActiveAuthType();
              onSubmit();
            }}
          />
        </div>
      </ModalLeft>
      <ModalRight>
        <img src={welcomeImage} alt="" />
      </ModalRight>
    </AdditionalFilterWrapper>
  );
};

AdmissionOpenModal.propTypes = {
  closeHandler: PropTypes.func,
  successToast: PropTypes.func,
  errorToast: PropTypes.func,
};

// AdmissionOpenModal.propTypes = {
//   closeHandler: PropTypes.func,
//   setModalType: PropTypes.func,
//   successToast: PropTypes.func,
//   errorToast: PropTypes.func,
//   isChildEdit: PropTypes.bool,
//   editChildId: PropTypes.number,
//   router: PropTypes.object,
// };

export default AdmissionOpenModal;
const AdditionalFilterWrapper = styled.div`
  width: 710px;
  height: ${props => props.height};
  margin: 5% auto;
  padding: 25px;
  padding-left: 45px;
  border-radius: 10px;
  background: ${colors.white};
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
  position: relative;

  &.welcome {
    position: relative;
    max-width: 828px;
    width: 100%;
    padding: 50px 48px;
    margin: 15% auto 20px;
    display: flex;
    @media (max-width: 1000px) {
      max-width: 750px;
      padding: 50px 30px;
    }
    @media (max-width: 500px) {
      max-width: 320px;
      padding: 50px 20px;
      flex-direction: column;
      overflow: hidden;
    }

    @media (max-width: 390px) {
      max-width: 300px;
    }

    .scheduleTour {
      display: flex;
      flex-direction: column;
      .personalDetail {
        color: #30333b;
        font-family: 'Roboto', sans-serif;
        font-size: 16px;
        line-height: 22px;
        margin-bottom: 20px;
      }
      .tel {
        @media (max-width: 500px) {
          padding: 10px 10px 10px 35px;
          font-size: 12px;
        }
      }
      .number {
        @media (max-width: 500px) {
          font-size: 12px;
          left: 10px;
          top: 9px;
        }
      }
      .checkbox label {
        @media (max-width: 500px) {
          font-size: 12px;
          flex-wrap: wrap;
          a {
            margin: 0 5px;
          }
        }
      }
      .mobile {
        @media (max-width: 500px) {
          height: 46px;
          font-size: 14px;
        }
      }
    }
  }
`;

export const GalleryHeading = styled.div`
  margin: ${props => props.margin || '0px'};
  color: ${props => props.color || '#30333B'};
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 500;
  line-height: 28px;
`;

export const ScheduleDetail = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  border-radius: 10px;
  background-color: #f3f5f9;
  padding: 18px 24px;
  margin-bottom: 10px;
  h2 {
    color: #30333b;
    font-family: 'Quicksand', sans-serif;
    font-size: 16px;
    font-weight: bold;
    line-height: 26px;
    text-align: center;
    margin: 0px;
    span {
      font-weight: 300;
      margin-right: 4px;
    }
  }
`;

const ModalTitle = styled.div`
  margin-bottom: 10px;
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 600;
  line-height: 28px;

  &.creatAccount {
    color: #55357f;
    font-size: 30px;
    // font-weight: 400;
    line-height: 23px;
    @media (max-width: 500px) {
      font-size: 18px;
    }
  }
  &.loginQues {
    font-family: 'Roboto', sans-serif;
    font-weight: 400;
    text-align: center;
    span {
      margin: 0px 5px;
    }
    &__title {
      margin-bottom: 20px;
      font-family: 'Roboto', sans-serif;
      font-weight: 400;
      text-align: center;
      font-size: 16px;
      line-height: 21px;
    }
  }
`;

const ModalSubTitle = styled.div`
  margin-left: 5px;
  margin-bottom: 20px;
  color: #30333b;
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 19px;
  width: 380px;
  @media screen and (max-width: 768px) {
    width: 100%;
  }
`;
const CloseModal = styled.div`
  position: absolute;
  top: 20px;
  right: 20px;
  cursor: pointer;
  @media screen and (max-width: 768px) {
    top: 10px;
    right: 15px;
  }
  .iconify {
    height: 12px;
    width: 12px;
    color: #666c78;
  }
`;

const ModalRight = styled.div`
  position: absolute;
  left: 0px;
  bottom: 0px;
  @media (max-width: 500px) {
    display: none;
  }
`;

const ModalLeft = styled.div`
  margin-left: auto;
  .alreadySigned {
    color: #666c78;
    font-family: Roboto;
    font-size: 14px;
    letter-spacing: 0;
    line-height: 14px;
    font-weight: 400;
    text-align: center;
    @media (max-width: 500px) {
      font-size: 12px;
    }
    span {
      color: #613a95;
      margin-left: 5px;
      cursor: pointer;
    }
  }
  @media (max-width: 500px) {
    margin-left: 0px;
  }
`;
