import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Button from '../Button';
import Flex from '../common/flex';
import colors from '../../utils/colors';

const EmailModal = ({ closeHandler, confirmationText, onConfirmClick }) => {
  const currentRef = useRef();

  return (
    <AdditionalFilterWrapper className="tour" ref={currentRef}>
      <CloseModal onClick={closeHandler}>
        <span
          className="iconify"
          data-icon="bytesize:close"
          data-inline="false"
        />
      </CloseModal>

      <>
        <GalleryHeading margin="0px 0px 10px">Confirm</GalleryHeading>
        <OrgName margin="0px 0px 20px">{confirmationText}</OrgName>
        <div className="scheduleTour">
          <Flex>
            <Button
              text="Cancel"
              type="subscribe"
              marginRight="10px"
              fullwidth
              onClick={closeHandler}
            />
            <Button
              fullwidth
              text="Yes"
              type="mobile"
              marginRight="10px"
              onClick={onConfirmClick}
            />
          </Flex>
        </div>
      </>
    </AdditionalFilterWrapper>
  );
};

EmailModal.propTypes = {
  closeHandler: PropTypes.func,
  onConfirmClick: PropTypes.func,
  confirmationText: PropTypes.string,
};

export default EmailModal;

const AdditionalFilterWrapper = styled.div`
  width: 710px;
  height: ${props => props.height};
  margin: 5% auto;
  padding: 25px;
  padding-left: 45px;
  border-radius: 10px;
  background: ${colors.white};
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
  position: relative;

  &.tour {
    max-width: 500px;
    width: 100%;
    padding: 50px 48px;
    margin: 15% auto 20px;
    @media screen and (max-width: 768px) {
      max-width: 290px;
      width: 100%;
      padding: 20px;
      margin: 20% auto;
    }
    .scheduleTour {
      display: flex;
      flex-direction: column;
      .mobile {
        @media (max-width: 500px) {
          height: 46px;
          padding: 0px 20px;
        }
      }
      .subscribe {
        @media (max-width: 500px) {
          height: 46px;
          margin-right: 10px;
          padding: 0px 20px;
          font-size: 14px;
        }
      }
    }
    @media (max-width: 500px) {
      max-width: 310px;
      padding: 40px 20px;
    }
  }
`;

export const GalleryHeading = styled.div`
  margin: ${props => props.margin || '0px'};
  color: ${props => props.color || '#30333B'};
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 500;
  line-height: 28px;
`;

const CloseModal = styled.div`
  position: absolute;
  top: 20px;
  right: 20px;
  cursor: pointer;
  @media screen and (max-width: 768px) {
    top: 1px;
    right: 7px;
  }
  .iconify {
    height: 12px;
    width: 12px;
    color: #666c78;
  }
`;

const OrgName = styled.div`
  color: #30333b;
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 19px;
  margin: ${props => props.margin || '0px'};
`;
