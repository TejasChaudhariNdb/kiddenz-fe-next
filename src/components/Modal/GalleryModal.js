/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useRef, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-slick';
import { Swipeable } from 'react-swipeable';
import styled from 'styled-components';
import startCase from 'lodash/startCase';
import groupBy from 'lodash/groupBy';
import SimpleBar from 'simplebar-react';
import Flex from '../common/flex';
// import image1 from '../../images/Rectangle Copy 9.svg';
// import image2 from '../../images/Rectangle Copy 11.svg';
// import image3 from '../../images/Rectangle Copy 13.svg';
// import image4 from '../../images/Rectangle Copy 14.svg';
// import image5 from '../../images/Rectangle Copy 4-1.svg';
// import image6 from '../../images/Rectangle Copy 4-2.svg';
// import image7 from '../../images/Rectangle Copy 4-3.svg';
// import image8 from '../../images/Rectangle Copy 4-4.svg';
// import image9 from '../../images/Rectangle Copy 4-7.svg';
// import image10 from '../../images/Rectangle Copy 4-6.svg';
import PhotoCard from '../PhotoCard';

const GalleryModal = ({ closeHandler, mediaResponse, mediaType }) => {
  const currentRef = useRef();
  const galleryRef = useRef();

  const [gallery, setGallery] = useState({});
  const [activeGalleryName, setActiveGalleryName] = useState('');
  const [activeGalleryImages, setActiveGalleryImages] = useState([]);
  const [activeTabIndex, setActiveTabIndex] = useState(0);
  const [activeImageIndex, setActiveImageIndex] = useState(0);
  const [swipeActivate, setSwipeActivate] = useState(false);

  const [randomNumber, setRandomNumber] = useState(
    Math.floor(100000 + Math.random() * 900000),
  );

  const settings = {
    infinite: false,
    speed: 900,
    autoplaySpeed: 2000,
    initialSlide: 0,
    slidesToShow: 1,
    slidesToScroll: 1,
    // waitForAnimate: false,
    beforeChange: current => {
      if (activeGalleryImages.length - 1 !== activeImageIndex) {
        setActiveImageIndex(current);
      }
    },
    afterChange: current => {
      setActiveImageIndex(current);
    },
    swipeToSlide: true,
  };

  const leftPress = useKeyPress('ArrowLeft');
  const rightPress = useKeyPress('ArrowRight');

  // Move Pictures on pressing Left arrow key
  useEffect(
    () => {
      setSwipeActivate(false);
      if (leftPress) {
        // If not the fist tab then on left press navigate to prev tab else to previous pic
        if (activeImageIndex === 0) {
          if (activeTabIndex === 0) {
            // Do Nothing
            const prevTabIndex = Object.entries(gallery).length - 1;
            setActiveTabIndex(prevTabIndex);
            setActiveGalleryName(Object.entries(gallery)[prevTabIndex][0]);
            setActiveGalleryImages(Object.entries(gallery)[prevTabIndex][1]);
          }
          if (activeTabIndex > 0) {
            const prevTabIndex = activeTabIndex - 1;
            setActiveTabIndex(prevTabIndex);
            setActiveGalleryName(Object.entries(gallery)[prevTabIndex][0]);
            setActiveGalleryImages(Object.entries(gallery)[prevTabIndex][1]);
            galleryRef.current.slickGoTo(
              Object.entries(gallery)[prevTabIndex][1].length - 1,
            );
            setActiveImageIndex(
              Object.entries(gallery)[prevTabIndex][1].length - 1,
            );
          }
        } else {
          galleryRef.current.slickPrev();
        }
      }
      return () => {};
    },
    [leftPress],
  );

  // Move Pictures on pressing Right arrow key
  useEffect(
    () => {
      setSwipeActivate(false);
      if (rightPress) {
        // If not the fist tab then on left press navigate to prev tab else to previous pic
        if (activeImageIndex === activeGalleryImages.length - 1) {
          if (activeTabIndex === Object.keys(gallery).length - 1) {
            // Do Nothing
            const nextTabIndex = 0;
            setActiveTabIndex(nextTabIndex);
            setActiveGalleryName(Object.entries(gallery)[nextTabIndex][0]);
            setActiveGalleryImages(Object.entries(gallery)[nextTabIndex][1]);
          }
          if (activeTabIndex < Object.keys(gallery).length - 1) {
            const nextTabIndex = activeTabIndex + 1;
            setActiveTabIndex(nextTabIndex);
            setActiveGalleryName(Object.entries(gallery)[nextTabIndex][0]);
            setActiveGalleryImages(Object.entries(gallery)[nextTabIndex][1]);
            setRandomNumber(Math.floor(100000 + Math.random() * 900000));
            // galleryRef.current.slickGoTo(0);
            setActiveImageIndex(0);
          }
        } else {
          galleryRef.current.slickNext();
        }
      }
      return () => {};
    },
    [rightPress],
  );

  useEffect(
    () => {
      const groupedGalleries = groupBy(mediaResponse, 'media_type');
      const galleryList = { ...groupedGalleries };
      delete galleryList.logo;
      delete galleryList.cover_pic;
      setGallery(galleryList);
    },
    [mediaResponse],
  );

  useEffect(
    () => {
      // if (mediaType) {
      //   setActiveGalleryName(mediaType);
      // } else {
      if (!mediaType && Object.entries(gallery).length) {
        setActiveGalleryName(Object.entries(gallery)[0][0]);
        setActiveGalleryImages(Object.entries(gallery)[0][1]);
      } else {
        setActiveGalleryName(mediaType);
        Object.entries(gallery).map(
          d => d[0] === mediaType && setActiveGalleryImages(d[1]),
        );
        // setActiveGalleryImages(Object.entries(gallery)[0][1]);
      }
    },
    [Object.entries(gallery).length],
    mediaType,
  );

  const handleTabClick = (tabName, tabValues, index) => {
    setActiveGalleryName(tabName);
    setActiveGalleryImages(tabValues);
    galleryRef.current.slickGoTo(0);
    setActiveTabIndex(index);
    setActiveImageIndex(0);
  };

  const onSwipeActionMobile = ({ dir }) => {
    setSwipeActivate(true);
    if (dir === 'Left') {
      // If not the fist tab then on left press navigate to prev tab else to previous pic
      if (activeImageIndex === activeGalleryImages.length - 1) {
        if (activeTabIndex === Object.keys(gallery).length - 1) {
          // Do Nothing
          const nextTabIndex = 0;
          setActiveTabIndex(nextTabIndex);
          setActiveGalleryName(Object.entries(gallery)[nextTabIndex][0]);
          setActiveGalleryImages(Object.entries(gallery)[nextTabIndex][1]);
        }
        if (activeTabIndex < Object.keys(gallery).length - 1) {
          const nextTabIndex = activeTabIndex + 1;
          setActiveTabIndex(nextTabIndex);
          setActiveGalleryName(Object.entries(gallery)[nextTabIndex][0]);
          setActiveGalleryImages(Object.entries(gallery)[nextTabIndex][1]);
          setRandomNumber(Math.floor(100000 + Math.random() * 900000));
          // galleryRef.current.slickGoTo(0);
          setActiveImageIndex(0);
        }
      } else {
        galleryRef.current.slickNext();
      }
    }

    if (dir === 'Right') {
      // If not the fist tab then on left press navigate to prev tab else to previous pic
      if (activeImageIndex === 0) {
        if (activeTabIndex === 0) {
          // Do Nothing
          const prevTabIndex = Object.entries(gallery).length - 1;
          setActiveTabIndex(prevTabIndex);
          setActiveGalleryName(Object.entries(gallery)[prevTabIndex][0]);
          setActiveGalleryImages(Object.entries(gallery)[prevTabIndex][1]);
        }
        if (activeTabIndex > 0) {
          const prevTabIndex = activeTabIndex - 1;
          setActiveTabIndex(prevTabIndex);
          setActiveGalleryName(Object.entries(gallery)[prevTabIndex][0]);
          setActiveGalleryImages(Object.entries(gallery)[prevTabIndex][1]);
          galleryRef.current.slickGoTo(
            Object.entries(gallery)[prevTabIndex][1].length - 1,
          );
          setActiveImageIndex(
            Object.entries(gallery)[prevTabIndex][1].length - 1,
          );
        }
      } else {
        galleryRef.current.slickPrev();
      }
    }
  };

  return (
    <>
      <Swipeable
        onSwiped={e => onSwipeActionMobile(e)}
        // {...config}
        className="gallerySection"
      >
        <div
          className="closeGallery"
          onClick={() => {
            closeHandler();
            setActiveGalleryImages(Object.entries(gallery)[0][1]);
          }}
        >
          <span
            className="iconify"
            data-icon="bytesize:close"
            data-inline="false"
          />
        </div>
        <div className="galleryContent" ref={currentRef}>
          <GalleryHeading color="#fff">Gallery</GalleryHeading>
          <NavBar>
            <SimpleBar style={{ maxHeight: '100%', height: '100%' }}>
              {Object.entries(gallery).length &&
                Object.entries(gallery).map(([key, value], index) => (
                  <li
                    className={activeGalleryName === key ? 'active' : null}
                    onClick={
                      activeGalleryName !== key
                        ? () => handleTabClick(key, value, index)
                        : null
                    }
                  >
                    {startCase(key)}
                  </li>
                ))}
            </SimpleBar>
          </NavBar>
          <div className="galleryContainer">
            <div className="galleryContainer-left">
              <div className="galleryContainer-slider" key={randomNumber}>
                <Slider
                  {...settings}
                  ref={galleryRef}
                  key={swipeActivate && activeGalleryImages.length}
                >
                  {activeGalleryImages.length &&
                    activeGalleryImages.map(galleryImage => (
                      <PhotoCard
                        key={galleryImage.media_url}
                        width="100%"
                        height="100%"
                        image={galleryImage.media_url}
                        imageAlt={activeGalleryName}
                      />
                    ))}
                </Slider>
                <div className="galleryContainer-pagination">
                  <span>{activeImageIndex + 1}</span>/
                  <span>{activeGalleryImages.length}</span>
                </div>
              </div>
            </div>
            <div className="galleryContainer-right">
              <SimpleBar style={{ maxHeight: '88vh' }}>
                <div className="galleryWrapper">
                  {activeGalleryImages.length &&
                    activeGalleryImages.map((galleryImage, index) => (
                      <PhotoCard
                        image={galleryImage.media_url}
                        margin="0px 10px 20px 0px"
                        width="100%"
                        imageAlt={activeGalleryName}
                        onClick={() => {
                          galleryRef.current.slickGoTo(index);
                        }}
                      />
                    ))}
                </div>
              </SimpleBar>
            </div>
          </div>
        </div>
      </Swipeable>
    </>
  );
};

GalleryModal.propTypes = {
  mediaType: PropTypes.string,
  closeHandler: PropTypes.func,
  mediaResponse: PropTypes.object,
};

export default GalleryModal;

// Key Press Detector
const useKeyPress = targetKey => {
  const [keyPressed, setKeyPressed] = useState(false);

  function downHandler({ key }) {
    if (key === targetKey) {
      setKeyPressed(true);
    }
  }

  const upHandler = ({ key }) => {
    if (key === targetKey) {
      setKeyPressed(false);
    }
  };

  useEffect(() => {
    window.addEventListener('keydown', downHandler);
    window.addEventListener('keyup', upHandler);

    return () => {
      window.removeEventListener('keydown', downHandler);
      window.removeEventListener('keyup', upHandler);
    };
  });

  return keyPressed;
};

export const NavBar = styled.div`
  // overflow-x: scroll;
  width: 100%;
  height: 40px;
  &::-webkit-scrollbar {
    visibility: hidden;
  }
  .simplebar-content {
    display: flex;
  }
  .simplebar-mask {
    height: 100% !important;
    border-bottom: 1px solid #e6e8ec;
  }
  .simplebar-content-wrapper {
    display: flex;
    max-height: fit-content !important;
    padding: 10px 0px;
    height: 100% !important;
  }

  li {
    min-width: fit-content;
    color: #ffffff;
    font-family: 'Roboto', sans-serif;
    font-size: 14px;
    line-height: 22px;
    list-style-type: none;
    margin-right: 20px;
    position: relative;
    cursor: pointer;
    @media (max-width: 500px) {
      min-width: fit-content;
    }
    &:first-child {
      @media (max-width: 500px) {
        margin-left: 15px;
      }
    }
    &:last-child {
      @media (max-width: 500px) {
        margin-right: 15px;
      }
    }
    &:hover::after,
    &.active::after {
      content: '';
      position: absolute;
      border-bottom: 1px solid #e6e8ec;
      width: 100%;
      height: 2px;
      background-color: #fff;
      left: 0px;
      bottom: -10px;
    }
  }
`;

export const GalleryHeading = styled.div`
  margin: ${props => props.margin || '0px'};
  color: ${props => props.color || '#30333B'};
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 500;
  line-height: 28px;
  @media (max-width: 500px) {
    padding-left: 25px;
    font-size: 20px;
    line-height: 25px;
  }
  @media (max-width: 361px) {
    padding-left: 15px;
  }
`;

export const TeacherWrapper = styled(Flex)`
  @media (max-width: 414px) {
    flex-direction: column;
  }
`;

export const OurStaff = styled.div`
  padding: 42px 0px;
  border-top: 1px solid #e6e8ec;
  border-bottom: 1px solid #e6e8ec;

  margin-top: 20px;
  .ourStaff {
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
  }
`;

export const OurSecurity = styled.div`
  padding: 42px 0px;
  .ourStaff {
    display: flex;
    flex-wrap: wrap;
  }
`;

export const HandbookHeading = styled.div`
  font-family: 'Quicksand', sans-serif;
  font-size: 22px;
  font-weight: 500;
  line-height: 28px;
`;

export const JumboImage = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 86px;
  width: 86px;
  border-radius: 50%;
  border: 3px solid #fff;
  background-color: rgba(255, 238, 79, 1);
  margin-right: 30px;
  & > img {
    width: 90%;
    height: 70%;
  }
`;

export const HandbookTitle = styled.div`
  color: #30333b;
  font-family: 'Quicksand', sans-serif;
  font-size: 20px;
  font-weight: bold;
  line-height: 25px;
  margin: ${props => props.margin || '0px'};
`;

export const HandBookBtn = styled.div`
  padding: 18px 20px;
  border: 1px solid #666c78;
  border-radius: 5px;
  background-color: #ffffff;
  color: #30333b;
  font-family: 'Quicksand', sans-serif;
  font-size: 16px;
  font-weight: 500;
  line-height: 20px;
`;

export const ListHeading = styled.div`
  color: #30333b;
  font-family: 'Quicksand', sans-serif;
  font-size: 18px;
  font-weight: bold;
  line-height: 23px;
  margin-bottom: 18px;
`;

export const ListDesc = styled.div`
  color: #666c78;
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 21px;
  padding-right: 70px;
`;
