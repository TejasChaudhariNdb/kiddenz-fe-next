import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import colors from 'utils/colors';

const Heading1 = styled.h1`
  width: 100%;
  color: ${colors.white};
  font-family: 'Quicksand', sans-serif;
  font-size: 40px;
  line-height: 47px;
  text-align: ${props => props.textAlign};
  margin: ${props => props.margin || '0px'};
  @media (max-width: 1200px) {
    font-size: 36px;
    margin-top: 40px;
  }
  @media (max-width: 900px) {
    font-size: 26px;
    line-height: 36px;
    margin-top: 20px;
  }
  @media (max-width: 500px) {
    font-size: 20px;
    line-height: 25px;
    padding: 0px 20px;
    margin-top: 20px;
  }
`;

const H1 = props => (
  <Heading1 textAlign={props.textAlign} margin={props.margin}>
    {props.text}
  </Heading1>
);
H1.propTypes = {
  text: PropTypes.string,
  textAlign: PropTypes.string,
  margin: PropTypes.string,
};
export default H1;
