/* eslint-disable no-nested-ternary */
import React from 'react';
import PropTypes from 'prop-types';
import { ImageBanner, JumboImage } from './styled';
const Imagebanner = ({
  imgpath,
  jumboImage,
  checkHttpForBanner = false,
  checkHttpForLogo = false,
}) => (
  <ImageBanner>
    {imgpath && (
      <img
        src={
          checkHttpForBanner
            ? imgpath.includes('://')
              ? imgpath
              : `https://${imgpath}`
            : imgpath
        }
        alt="imagebanner"
      />
    )}
    <JumboImage>
      {jumboImage && (
        <img
          src={
            checkHttpForLogo
              ? jumboImage.includes('://')
                ? jumboImage
                : `https://${jumboImage}`
              : jumboImage
          }
          alt=""
          style={{
            maxWidth: '100%',
            width: '100%',
            height: '100%',
            borderRadius: '50%',
          }}
        />
      )}
    </JumboImage>
  </ImageBanner>
);
Imagebanner.propTypes = {
  imgpath: PropTypes.string,
  jumboImage: PropTypes.string,
  checkHttpForBanner: PropTypes.bool,
  checkHttpForLogo: PropTypes.bool,
};
export default Imagebanner;
