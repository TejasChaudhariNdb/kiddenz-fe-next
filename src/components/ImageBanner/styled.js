import styled from 'styled-components';
export const ImageBanner = styled.div`
  position: relative;
  max-height: ${props => props.height || '556px'};
  height: 556px;
  margin-top: 76px;
  min-height: 556px;
  width: 100%;
  & > img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
  @media (max-width: 768px) {
    height: 255px;
    margin-top: 52px;
  }
  @media (max-width: 425px) {
    min-height: 300px;
    height: 300px;
    max-height: 300px;
  }
`;

export const JumboImage = styled.div`
  position: absolute;
  left: 25px;
  bottom: -60px;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 130px;
  width: 130px;
  border-radius: 50%;
  border: 3px solid #fff;
  background-color: #fff;
  @media (max-width: 1280px) {
    height: 118px;
    width: 118px;
  }
  @media (max-width: 768px) {
    height: 100px;
    width: 100px;
  }
  @media (max-width: 425px) {
    height: 70px;
    width: 70px;
    bottom: -35px;
  }

  & > img {
    width: 90%;
    height: 70%;
  }
`;
