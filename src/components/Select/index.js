/* eslint-disable react/prop-types */
/* eslint-disable indent */
import React from 'react';
// import Select from 'react-select';
import AsyncSelect from 'react-select/async';

export const creatableMenu = {
  menu: provided => ({
    ...provided,
    zIndex: 22,
  }),
  container: provided => ({
    ...provided,
    border: 0,
    borderRadius: 4,
  }),
  control: provided => ({
    ...provided,
    bordercolor: 'transparent',
    borderWidth: 0,
    borderRadius: 4,
  }),
  dropdownIndicator: provided => ({
    ...provided,
    padding: '0.8rem 0.75rem',
  }),
};

function Selects({
  isResponsive,
  type,
  label,
  value,
  multiple,
  disabled,
  onChange,
  sendData,
  options,
  onInputChange,
  defaultValue,
  selectedOption,
  isResponsivePadding,
  noPad = false,
  noFormGroup = false,
  error,
  ...rest
}) {
  return (
    <div
      xs={
        isResponsive && window.screen.width < 1250 ? '5' : rest.colSpan || '12'
      }
      style={{
        padding: noPad && 0,
      }}
    >
      <div className={`${noFormGroup ? '' : 'formFlex'} ${rest.classtype}`}>
        {label && (
          <div
            xs={rest.labelSpan || '4'}
            style={{
              paddingLeft: rest.paddingLeft,
              display: 'block',
              padding: noPad && 0,
            }}
          >
            <label htmlFor={rest.name}>{label}</label>
          </div>
        )}
        <div
          style={{
            padding: noPad
              ? 0
              : isResponsivePadding && window.screen.width < 1250 && 0,
            fontSize: 14,
            marginRight: rest.marginRight,
            paddingLeft: noPad ? '0px !important' : rest.paddingLeft,
            marginLeft: rest.marginLeft,
          }}
        >
          <AsyncSelect
            placeholder="Start / Enter..."
            styles={creatableMenu}
            className="form-control-alternative"
            options={options}
            isDisabled={disabled}
            isMulti={multiple || false}
            onInputChange={onInputChange}
            // {...form.getFieldProps(rest.name, {
            //   validateTrigger: 'onChange',
            //   initialValue: defaultValue && {
            //     value: defaultValue,
            //     label: defaultValue,
            //   },
            //   // value: value,
            // })}
            onChange={onChange}
            invalid={!!error}
            defaultValue={selectedOption}
          />
          {error && (
            <div style={{ color: 'red', fontSize: 11, fontWeight: 300 }}>
              {error}
            </div>
          )}
          {/* {error && (
            <div style={{ color: 'red', fontSize: 11, fontWeight: 300 }}>
              {error[0] && error[0].includes('_')
                ? error[0].charAt(0).toUpperCase() + error[0].slice(1)
                : error}
            </div>
          )} */}
        </div>

        <div>
          {error && error[0] && error[0].includes('_')
            ? error[0].charAt(0).toUpperCase() + error[0].slice(1)
            : error}
        </div>
      </div>
    </div>
  );
}

Selects.propTypes = {};

export default Selects;
