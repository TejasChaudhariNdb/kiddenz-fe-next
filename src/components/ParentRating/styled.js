import styled from 'styled-components';
export const ParentRatings = styled.div`
  .starIcon {
    width: 20px;
    height: 20px;
    margin-right: 3px;
  }
  .commentDate {
    margin-left: 10px;
    color: #666c78;
    font-family: 'Roboto', sans-serif;
    font-size: 12px;
    line-height: 16px;
    align-self: flex-end;
  }
`;
export const Name = styled.h3`
  color: #30333b;
  font-family: 'Quicksand', sans-serif;
  font-weight: 500;
  font-size: 16px;
  line-height: 18px;
  margin: 0px;
`;
