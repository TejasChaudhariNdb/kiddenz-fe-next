import React from 'react';
import Rating from 'react-rating';
import PropTypes from 'prop-types';
import { ParentRatings, Name } from './styled';
import Flex from '../common/flex';
import PhotoCard from '../PhotoCard/index';
import star from '../../images/star.svg';
import star1 from '../../images/star.1.svg';
const ParentRating = ({
  name,
  image,
  initialRating,
  date,
  readonly,
  star3,
}) => (
  <ParentRatings>
    <Flex wrap>
      <PhotoCard
        image={image}
        width="50px"
        height="50px"
        margin="0px 10px 0px 0px"
      />
      <Flex column justifyBetween flexMarginXs="10px 0px 0px 0px">
        <Name>{name}</Name>
        <Flex flexMarginXs="0px 0px 10px 0px">
          <Rating
            emptySymbol={
              <img
                src={star3 || star1}
                alt={star3 ? 'star3' : 'star1'}
                className="starIcon"
              />
            }
            fullSymbol={<img src={star} alt="star" className="starIcon" />}
            className="star_Rating"
            initialRating={initialRating}
            readonly={readonly}
          />
          {date && <span className="commentDate">{date}</span>}
        </Flex>
      </Flex>
    </Flex>
  </ParentRatings>
);
ParentRating.propTypes = {
  name: PropTypes.string,
  image: PropTypes.string,
  date: PropTypes.string,
  initialRating: PropTypes.string,
  readonly: PropTypes.string,
  star3: PropTypes.string,
};
export default ParentRating;
