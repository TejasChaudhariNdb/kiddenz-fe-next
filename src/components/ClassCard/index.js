/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable indent */
/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable no-nested-ternary */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Link from 'next/link';
import moment from 'moment';
import getCardAgeText from 'shared/utils/getCardAgeText';
import Flex from '../common/flex';
import Button from '../Button';
import classImage from '../../images/classImage.png';
import teddy from '../../images/Login   questions   age.png';
import cake from '../../images/blue-cake.svg';
import tv from '../../images/Group 378.png';
import purpleTag from '../../images/purpleTag.png';
import greenTag from '../../images/greenTag.png';
// import redTag from '../../images/redTag.png';
import star from '../../images/Symbols.png';
import Ruppee from '../common/Ruppee';

const getMinimumCost = (cost = {}) => {
  let discountedCost = null;
  let originalCost = null;
  let discount = null;
  if (cost.full_course_cost) {
    originalCost = cost.full_course_cost;
    // if (cost.full_course_discount <= 100) {
    if (cost.full_course_discount > 100) {
      discount = cost.full_course_discount;
      discountedCost = originalCost - cost.full_course_discount;
      // discountedCost =
      //   originalCost - (originalCost * cost.full_course_discount) / 100;
    } else {
      discount = 0;
      discountedCost = originalCost;
    }
  }
  if (cost.monthly_cost) {
    if (!originalCost || (originalCost && cost.monthly_cost < originalCost)) {
      originalCost = cost.monthly_cost;
      // if (cost.monthly_discount <= 100) {
      if (cost.monthly_discount > 0) {
        discount = cost.monthly_discount;
        discountedCost = originalCost - cost.monthly_discount;
        // discountedCost =
        //   originalCost - (originalCost * cost.monthly_discount) / 100;
      } else {
        discount = 0;
        discountedCost = originalCost;
      }
    }
  }
  if (cost.single_session_cost) {
    if (
      !originalCost ||
      (originalCost && cost.single_session_cost < originalCost)
    ) {
      originalCost = cost.single_session_cost;
      // if (cost.monthly_discount <= 100) {
      if (cost.monthly_discount > 0) {
        discount = cost.monthly_discount;
        discountedCost = originalCost - cost.monthly_discount;
        // discountedCost =
        //   originalCost - (originalCost * cost.monthly_discount) / 100;
      } else {
        discount = 0;
        discountedCost = originalCost;
      }
    }
  }
  if (cost.trial_cost > 0) {
    if (!originalCost || (originalCost && cost.trial_cost < originalCost)) {
      originalCost = cost.trial_cost;
      // if (cost.monthly_discount <= 100) {
      if (cost.trial_discount > 0) {
        discount = cost.monthly_discount;
        discountedCost = originalCost - cost.trial_discount;
        // discountedCost =
        //   originalCost - (originalCost * cost.trial_discount) / 100;
      } else {
        discount = 0;
        discountedCost = originalCost;
      }
    }
  }
  return { discountedCost, originalCost, discount };
};

function ClassCard(props) {
  const {
    isOneToOne,
    onBookmark,
    onDeleteBookmark,
    isBookMarked,
    // onCardClick,
    name,
    rating,
    providerName,
    // age,
    startAge,
    endAge,
    isSingleSession,
    price,
    width,
    moneyBack,
    isSponsered,
    media = [],
    isTrial,
    className,
    batches,
    // isLoggedIn,
    href,
    programId,
  } = props;
  const { discountedCost, originalCost, discount } = getMinimumCost(price);
  const programImage = media.filter(img => img.media_type === 'image');
  let batchWithDays = {};
  if (batches && batches.length)
    batchWithDays = batches.find(
      batch =>
        batch.batch_type === 'Full_Course' || batch.batch_type !== 'Monthly',
    );
  return (
    <Classcard
      width={width}
      className={className}
      // onClick={onCardClick}
    >
      <Link
        href={{
          pathname: '/online-program/[program-name]',
          query: { id: programId },
        }}
        as={href}
        // passHref
      >
        <a>
          <ClassImage>
            <img
              src={programImage.length ? programImage[0].media_url : classImage}
              alt=""
            />
            {(isOneToOne || isSponsered) && (
              <div className={isSponsered ? 'tag green' : 'tag purple'}>
                <div className={isSponsered ? 'greenTag' : 'purpleTag'}>
                  <img src={isSponsered ? greenTag : purpleTag} alt="" />
                  <h5>
                    {isSponsered
                      ? 'Sponsored'
                      : isOneToOne
                        ? 'one to one'
                        : null}
                  </h5>
                </div>
                {/* <div className="greenTag">
            <img src={greenTag} alt="" />
            <h5> sponsored</h5>
          </div> */}
                {/* <div className="redTag">
            <img src={redTag} alt="" />
            <h5>2 Days left</h5>
          </div> */}
              </div>
            )}
            {/* {isLoggedIn && ( */}
            <div
              className="bookmark"
              onClick={e => {
                e.stopPropagation();
                e.preventDefault();
                if (isBookMarked) {
                  onDeleteBookmark();
                } else {
                  onBookmark();
                }
              }}
            >
              <div className={!isBookMarked ? 'hide-icon' : ''}>
                <span
                  className="iconify"
                  data-icon="bi:bookmark-check-fill"
                  data-inline="false"
                />
              </div>
              <div className={isBookMarked ? 'hide-icon' : ''}>
                <span
                  className="iconify"
                  data-icon="fa:bookmark-o"
                  data-inline="false"
                />
              </div>
            </div>
            {/* )} */}
          </ClassImage>
          <ClassContent>
            <h2>{name}</h2>
            <div className="profileWrap">
              {providerName && <p>By {providerName}</p>}
              {batchWithDays &&
              batchWithDays.start_date &&
              batchWithDays.end_date ? (
                <h4>
                  {moment(batchWithDays.start_date).format('DD MMM YY')} -{' '}
                  {moment(batchWithDays.end_date).format('DD MMM YY')}
                </h4>
              ) : (
                <h4>{batchWithDays && batchWithDays.till_date}</h4>
              )}
              <ProfileDetail>
                <div className="profile-name">online</div>
                {rating && +rating > 0 ? (
                  <div className="profile-rating">
                    <img src={star} alt="" />
                    {rating && +rating > 0 && parseFloat(rating).toFixed(1)}
                  </div>
                ) : null}
                {isTrial && (
                  <div className="trailClass">Trial Class Available</div>
                )}
              </ProfileDetail>
            </div>
          </ClassContent>
          <ClassContent>
            <Flex
              flexPadding="4px 0px 4px"
              justifyBetween
              className="classSession"
            >
              {/* {age && (
            <Session>
              <div className="icon">
                {' '}
                <img src={teddy} alt="" />
              </div>
              <span>Age Group {age}</span>
            </Session>
          )} */}
              {startAge &&
                endAge && (
                  <Session>
                    <div className="icon">
                      {' '}
                      <img src={cake} alt="" style={{ marginBottom: '7px' }} />
                    </div>
                    <span>Age {getCardAgeText(startAge, endAge)}</span>
                  </Session>
                )}
              {isSingleSession && (
                <Session>
                  <div className="icon">
                    {' '}
                    <img src={tv} alt="" />
                  </div>
                  <span>Single Session Available</span>
                </Session>
              )}
            </Flex>
          </ClassContent>
          <ClassContent>
            <Flex
              justifyBetween
              alignCenter
              flexWidth="100%"
              flexMargin="4px 0px 10px"
            >
              {originalCost ? (
                <div className="viewWrap">
                  <Price>
                    {moneyBack && <span>Money back Guarantee</span>}
                    <p>Price starts from</p>
                    <h4>
                      <Ruppee>₹</Ruppee>
                      {discount ? discountedCost : originalCost}{' '}
                      {discount ? (
                        <span>
                          <Ruppee>₹</Ruppee>
                          {`${originalCost}  ( ₹${discount}  OFF )`}
                        </span>
                      ) : null}
                    </h4>
                  </Price>
                  {/* <Date className="cardDate">
              <span>Mon-wed</span>
              <h3>10 - 20</h3>
              <h5>Sept, 2020</h5>
            </Date> */}
                  <Link
                    href={{
                      pathname: '/online-program/[program-name]',
                      query: { id: programId },
                    }}
                    as={href}
                    // passHref
                  >
                    {/* <a> */}
                    <Button
                      text="View"
                      height="40px"
                      headerButton
                      // onClick={onCardClick}
                    />
                    {/* </a> */}
                  </Link>
                </div>
              ) : (
                <div className="viewWrap flexEnd">
                  <Link
                    href={{
                      pathname: '/online-program/[program-name]',
                      query: { id: programId },
                    }}
                    as={href}
                    // passHref
                  >
                    {/* <a> */}
                    <Button
                      text="View"
                      height="40px"
                      headerButton
                      // onClick={onCardClick}
                    />
                    {/* </a> */}
                  </Link>
                </div>
              )}
            </Flex>
          </ClassContent>
        </a>
      </Link>
    </Classcard>
  );
}

ClassCard.propTypes = {
  isOneToOne: PropTypes.string,
  onBookmark: PropTypes.func,
  onDeleteBookmark: PropTypes.func,
  isBookMarked: PropTypes.bool,
  // onCardClick: PropTypes.func,
  name: PropTypes.string,
  // age: PropTypes.string,
  providerName: PropTypes.string,
  isSingleSession: PropTypes.bool,
  price: PropTypes.string,
  moneyBack: PropTypes.bool,
  isSponsered: PropTypes.bool,
  isTrial: PropTypes.bool,
  media: PropTypes.array,
  width: PropTypes.string,
  rating: PropTypes.string,
  className: PropTypes.string,
  batches: PropTypes.array,
};

export default ClassCard;
const Classcard = styled.div`
  display: flex;
  flex-direction: column;
  width: ${props => props.width || '30%'};
  background: #ffffff;
  box-shadow: 20px 6px 30px #eeeeee;
  border-radius: 5px;
  margin: 10px auto 40px;
  cursor: pointer;
  // min-height: 525px;
  min-height: 537px;
  transition: 0.3s;
  @media screen and (max-width: 500px) {
    min-height: unset;
  }
  &:hover {
    cursor: pointer;
    transform: scale(1.02);
    box-shadow: 3px 3px 8px #ccc;
    transition: 0.3s;
  }
  a {
    text-decoration: none;
  }
`;

const ClassImage = styled.div`
  width: 100%;
  height: 196px;
  position: relative;
  @media screen and (max-width: 768px) {
    height: auto;
  }
  & > img {
    z-index: 99;
    width: 100%;
    height: 100%;
  }
  .bookmark {
    z-index: 9999;
    position: absolute;
    top: 20px;
    right: 16px;
    background: rgba(0, 0, 0, 0.5);
    border-radius: 5px;
    width: 24px;
    height: 24px;
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
    .iconify {
      width: 14px;
      height: 14px;
      color: #fff;
      margin-top: -2px;
    }
  }
  .hide-icon {
    display: none;
  }
  .tag {
    position: absolute;
    top: 20px;
    & > div {
      display: none;
      height: 24px;
      width: fit-content;
      img {
        width: 120%;
        height: 100%;
        position: absolute;
      }
      h5 {
        margin: 0px;
        z-index: 1;
        font-family: Quicksand;
        font-weight: bold;
        font-size: 14px;
        line-height: 17px;
        color: #ffffff;
        padding-left: 10px;
        padding-top: 3px;
      }
    }
    &.purple {
      .purpleTag {
        display: flex;
      }
    }
    &.green {
      .greenTag {
        display: flex;
      }
    }
    &.red {
      .redTag {
        display: flex;
      }
    }
  }
`;

const ClassContent = styled.div`
  padding: 10px 12px;
  border-bottom: 1px dashed #ececec;
  display: flex;
  flex-direction: column;
  min-height: 46.7px;
  .classSession {
    display: flex;
    flex-flow: row wrap;
    justify-content: space-between;
    // ::after {
    //   content: '';
    //   width: 30%;
    // }
  }

  &:last-child {
    border-bottom: 0px solid #ececec;
    margin-top: auto;
    padding: 5px 12px;
  }
  .profileWrap {
    min-height: 90px;
    @media screen and (max-width: 500px) {
      min-height: unset;
    }
    & > p {
      font-family: Quicksand;
      font-weight: 400;
      font-size: 13px;
      line-height: 26px;
      color: #666c78;
      margin-bottom: 6px;
      margin-top: 0px;
      text-align: left;
    }
    & > h4 {
      font-family: Quicksand;
      font-weight: bold;
      font-size: 15px;
      line-height: 21px;
      color: #613a94;
      margin: 0px 0px 10px;
      text-align: left;
    }
  }
  .viewWrap {
    min-height: 78px;
    display: flex;
    align-items: flex-end;
    justify-content: space-between;
    width: 100%;
    margin-bottom: 20px;
    @media screen and (max-width: 500px) {
      min-height: unset;
      margin-bottom: 0;
    }
  }
  .flexEnd {
    width: 100%;
    justify-content: flex-end;
  }
  h2 {
    width: 100%;
    font-family: Quicksand;
    font-weight: 600;
    font-size: 18px !important;
    line-height: 24px !important;
    text-align: left;
    color: #000000;
    display: flex;
    flex-direction: column;
    min-height: 48px;
    margin: 0px 0px 10px !important;
    @media screen and (max-width: 500px) {
      min-height: unset;
    }
  }
  button {
    @media screen and (max-width: 768px) {
      height: 35px;
      padding: 0 18px;
      font-size: 13px;
    }
  }
`;

const Session = styled.div`
  width: 50%;
  display: flex;
  flex-direction: row;
  align-items: center;
  // margin-right: 50px;
  // min-height: 45px;
  // max-height: 45px;

  .icon {
    width: 15px;
    height: auto;
    // margin-bottom: 9px;
    margin-right: 10px;
    img {
      width: 100%;
      height: 100%;
    }
  }

  span {
    font-family: 'Roboto', sans-serif;
    font-weight: 400;
    font-size: 10px;
    line-height: 12px;
    color: #60b947;
    text-align: center;
  }
`;

const Price = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  & > h4 {
    font-family: Quicksand;
    font-weight: 500;
    font-size: 18px;
    line-height: 26px;
    color: #000000;
    margin: 3px 0px 0px;
    @media screen and (max-width: 768px) {
      font-size: 15px;
      line-height: 1.2;
    }
    span {
      font-family: 'Roboto', sans-serif;
      font-weight: 400;
      font-size: 10px;
      line-height: 12px;
      color: #666c78;
    }
  }
  & > p {
    font-family: Quicksand;
    font-style: normal;
    font-weight: 400;
    font-size: 12px;
    line-height: 14px;
    color: #000000;
    margin: 0px;
    opacity: 0.62;
  }
  & > span {
    font-family: Quicksand;
    font-weight: 500;
    font-size: 13px;
    line-height: 16px;
    color: #613a94;
    margin: 0px 0px 5px;
  }
`;

// const Date = styled.div`
//   display: flex;
//   flex-direction: column;
//   align-items: center;
//   & > span {
//     font-family: 'Roboto', sans-serif;
//     font-style: normal;
//     font-weight: 400;
//     font-size: 10px;
//     line-height: 12px;
//     color: #666c78;
//   }
//   & > h3 {
//     font-family: 'Roboto', sans-serif;
//     font-weight: 400;
//     font-size: 16px;
//     line-height: 19px;
//     color: #000000;
//     margin: 0px;
//   }
//   & > h5 {
//     font-family: 'Roboto', sans-serif;
//     font-weight: 400;
//     font-size: 10px;
//     line-height: 12px;
//     color: #000000;
//     margin: 0px;
//   }
// `;

const ProfileDetail = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 4px;
  .profile-name {
    padding: 4px;
    font-family: Quicksand;
    font-weight: 500;
    font-size: 12px;
    line-height: 13px;
    color: #10cc3b;
    margin-right: 10px;
    text-transform: uppercase;
    border: 1px solid #10cc3b;
    border-radius: 5px;
  }
  .profile-rating {
    display: flex;
    align-items: center;
    padding-left: 12px;
    border-left: 1px solid #000000;
    line-height: 1;
    font-family: Quicksand;
    font-weight: 500;
    font-size: 12px;
    color: #666c78;
    img {
      margin-right: 12px;
      width: 18px;
    }
  }
  .trailClass {
    font-family: Roboto;
    font-style: normal;
    font-weight: normal;
    font-size: 11px;
    line-height: 13px;
    color: #ffffff;
    padding: 5px;
    border-radius: 5px;
    background: #60b947;
    margin-left: 15px;
  }
`;
