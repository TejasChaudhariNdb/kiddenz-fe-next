/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Router from 'next/router';
import Button from 'components/Button';
// import colors from 'utils/colors';
import axios from 'config/axios';
import Flex from '../common/flex';
import profile from '../../images/Group 18@1.5x.svg';
import myChild from '../../images/Group 23@1.5x.svg';
// import notification from '../../images/bell@1.5x.svg';
import logOut from '../../images/Group 21@1.5x.svg';
import article from '../../images/Group 22@1.5x.svg';
import provider from '../../images/Group 20@1.5x.svg';
import extracurriculars from '../../images/extracurriculars.svg';
import preschoolCap from '../../images/preschool cap.svg';

import { useDropdownClose } from '../../shared/hooks';
import { deleteCookie } from '../../shared/utils/commonHelpers';

function ProfileDropdown({ type, name, image, ...props }) {
  const [dropdownActive, setDropdownActive] = useState(false);
  const dropdownRef = useRef(null);
  const {
    LOGOUT_API_CALL,
    errorToast,
    successToast,
    router: { pathname } = {},
    // isLoggedIn,
    authActionClick,
    isMobile = false,
  } = props;

  const { authentication: { isLoggedIn } = {} } = props;

  useDropdownClose(dropdownRef, setDropdownActive);

  const ROUTES_WITH_AUTH_MANDATORY = ['/child', '/profile'];

  const logout = () => {
    LOGOUT_API_CALL({
      successCallback: () => {
        deleteCookie('x-access-token');
        axios.defaults.headers.common.Authorization = '';
        successToast('Successfully logged out.');
        Router.push('/').then(() => window.scrollTo(0, 0));

        if (ROUTES_WITH_AUTH_MANDATORY.includes(pathname)) {
          Router.push('/').then(() => window.scrollTo(0, 0));
        }
      },
      errorCallback: ({ message }) => {
        errorToast(message || 'Something went wrong.');
      },
    });
  };

  return isMobile ? (
    <>
      {!isLoggedIn && (
        <Button
          type="links"
          text={
            <span
              className="iconify"
              data-icon="mdi-light:menu"
              data-inline="false"
            />
          }
          onClick={() => setDropdownActive(!dropdownActive)}
        />
      )}
      <Dropdown
        ref={dropdownRef}
        className={type}
        onClick={() => setDropdownActive(!dropdownActive)}
      >
        {isLoggedIn && (
          <DropdownHeader>
            <Flex alignCenter>
              <DropdownImage>
                <img src={image} alt="" />
              </DropdownImage>
              <h3>{name}</h3>
            </Flex>
          </DropdownHeader>
        )}

        {dropdownActive && (
          <DropdownList>
            <div className="menuheading">Menu</div>
            {isLoggedIn && (
              <>
                <MenuProfile>
                  <div className="image">
                    <img src={image} alt="" />
                  </div>
                  <span>{name}</span>
                </MenuProfile>
                <li
                  onClick={() =>
                    Router.push({
                      pathname: '/profile',
                    }).then(() => window.scrollTo(0, 0))
                  }
                >
                  {' '}
                  <img src={profile} alt="" />
                  Profile
                </li>
                {/* <li>
                  {' '}
                  <img src={notification} alt="" />
                  Notification
                </li> */}
                <li
                  onClick={() =>
                    Router.push({
                      pathname: '/child',
                    }).then(() => window.scrollTo(0, 0))
                  }
                >
                  <img src={myChild} alt="" />
                  My Child
                </li>
              </>
            )}
            <li
              onClick={() =>
                Router.push({
                  pathname: '/about-us',
                }).then(() => window.scrollTo(0, 0))
              }
            >
              {' '}
              <img src={preschoolCap} alt="" />
              About us
            </li>
            <li
              onClick={() =>
                Router.push({
                  pathname: '/blog',
                }).then(() => window.scrollTo(0, 0))
              }
            >
              {' '}
              <img src={article} alt="" />
              Parenting Articles
            </li>
            <li
              onClick={() =>
                Router.push({
                  pathname: '/provider',
                }).then(() => window.scrollTo(0, 0))
              }
            >
              {' '}
              <img src={provider} alt="" />
              For Providers
            </li>{' '}
            <li
              onClick={() =>
                Router.push({
                  pathname: '/online-program/search',
                }).then(() => window.scrollTo(0, 0))
              }
            >
              {' '}
              <img src={extracurriculars} alt="" />
              Extracurriculars
            </li>{' '}
            {isLoggedIn ? (
              <li onClick={logout}>
                {' '}
                <img src={logOut} alt="" />
                Logout
              </li>
            ) : null}
            {!isLoggedIn && (
              <DropDownButton>
                <Button
                  text="Login"
                  height="36px"
                  marginRight="10px"
                  onClick={() => authActionClick('login')}
                />
                <Button
                  text="Register"
                  height="36px"
                  type="outline"
                  onClick={() => authActionClick('register')}
                />
              </DropDownButton>
            )}
          </DropdownList>
        )}
      </Dropdown>
    </>
  ) : (
    <>
      <Dropdown
        ref={dropdownRef}
        className={type}
        onClick={() => setDropdownActive(!dropdownActive)}
      >
        {isLoggedIn && (
          <DropdownHeader>
            <Flex alignCenter>
              <DropdownImage>
                <img src={image} alt="" />
              </DropdownImage>
              <h3>{name}</h3>
            </Flex>
            <div className={`dropdownArrow ${dropdownActive && 'active'}`}>
              <span
                className="iconify"
                data-icon="simple-line-icons:arrow-down"
                data-inline="false"
              />
            </div>
          </DropdownHeader>
        )}

        {dropdownActive && (
          <DropdownContent>
            <li
              onClick={() =>
                Router.push({
                  pathname: '/profile',
                }).then(() => window.scrollTo(0, 0))
              }
            >
              {' '}
              <img src={profile} alt="" />
              Profile
            </li>
            {/* <li>
              {' '}
              <img src={notification} alt="" />
              Notification
            </li> */}
            <li
              onClick={() =>
                Router.push({
                  pathname: '/child',
                }).then(() => window.scrollTo(0, 0))
              }
            >
              <img src={myChild} alt="" />
              My Child
            </li>
            <li onClick={logout}>
              {' '}
              <img src={logOut} alt="" />
              Logout
            </li>
          </DropdownContent>
        )}
      </Dropdown>
    </>
  );
}

ProfileDropdown.propTypes = {
  image: PropTypes.string,
  name: PropTypes.string,
  type: PropTypes.string,
  LOGOUT_API_CALL: PropTypes.func,
  errorToast: PropTypes.func,
  successToast: PropTypes.func,
  authActionClick: PropTypes.func,
  router: PropTypes.object,
  authentication: PropTypes.object,
  isLoggedIn: PropTypes.bool,
  isMobile: PropTypes.bool,
};

export default ProfileDropdown;

const Dropdown = styled.div`
  position: relative;
  cursor: pointer;
  &.onmobile {
    @media (max-width: 768px) {
      display: none;
    }
  }
`;
const DropdownHeader = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 8px 10px;
  width: 230px;
  border-radius: 5px;
  background-color: #f3f5f9;
  @media (max-width: 1152px) {
    width: 160px;
  }
  @media (max-width: 768px) {
    width: auto;
    padding: 0px;
    border-radius: 50px;
    // margin-right: 15px;
    margin-left: 15px;
  }
  .dropdownArrow {
    transform: rotate(0deg);
    transition: transform 0.2s ease;
    &.active {
      transform: rotate(180deg);
      transition: transform 0.2s ease;
    }
  }
  h3 {
    color: #30333b;
    font-size: 16px;
    font-weight: 400;
    line-height: 18px;
    margin: 0px 0px 0px 10px;
    @media (max-width: 768px) {
      display: none;
    }
  }
  .iconify {
    width: 13px;
    height: 13px;
    // margin-top: 5px;
    @media (max-width: 768px) {
      display: none;
    }
  }
`;
const DropdownImage = styled.div`
  height: 30px;
  width: 30px;
  border-radius: 50%;
  img {
    width: 100%;
    height: 100%;
    border-radius: 50%;
  }
`;
const DropdownList = styled.div`
  position: absolute;
  display: none;
  top: 43px;
  right: 0px;
  width: 220px;
  border-radius: 5px;
  background-color: #ffffff;
  box-shadow: 2px 2px 10px 0 rgba(0, 0, 0, 0.2);
  border-radius: 5px;
  padding-bottom: 10px;
  @media (max-width: 900px) {
    display: block !important;
    top: 27px;
  }
  .menuheading {
    display: flex;
    padding-left: 16px;
    height: 46px;
    width: 100%;
    align-items: center;
    border-radius: 5px 5px 0 0;
    background-color: #f2eff5;
    font-family: Roboto;
    font-size: 16px;
    letter-spacing: 0;
    line-height: 19px;
  }
  li {
    font-family: 'Roboto', sans-serif;
    color: #30333b;
    font-size: 14px;
    line-height: 17px;
    padding: 12px 16px;
    list-style: none;
    cursor: pointer;
    transition: all 0.2s;
    img {
      margin-right: 15px;
      width: 19px;
    }
    &:hover {
      background-color: #cccccf;
      color: #000;
      transition: all 0.2s;
    }
  }
`;

const DropdownContent = styled.div`
  position: absolute;
  display: block;
  top: 43px;
  right: 0px;
  left: 0px;
  border-bottom-left-radius: 5px;
  border-bottom-right-radius: 5px;
  background-color: #f3f5f9;
  // padding-bottom: 10px;
  li {
    font-family: 'Roboto', sans-serif;
    color: #30333b;
    font-size: 14px;
    line-height: 17px;
    padding: 12px 16px;
    list-style: none;
    cursor: pointer;
    transition: all 0.2s;
    img {
      margin-right: 15px;
      width: 19px;
    }
    &:hover {
      background-color: #cccccf;
      color: #000;
      transition: all 0.2s;
    }
  }
  @media (max-width: 500px) {
    display: none !important;
  }
`;

const MenuProfile = styled.div`
  padding: 16px;
  display: flex;
  margin-bottom: 10px;
  align-items: center;
  .image {
    width: 30px;
    height: 30px;
    border-radius: 50%;
    overflow: hidden;
    margin-right: 10px;
    img {
      width: 100%;
      height: 100%;
    }
  }
  span {
    color: #30333b;
    font-family: Roboto;
    font-size: 16px;
    letter-spacing: 0;
    line-height: 19px;
  }
`;

const DropDownButton = styled.div`
  display: flex;
  padding: 16px;
  button {
    padding: 0px 20px;
    font-size: 12px;
  }
`;
