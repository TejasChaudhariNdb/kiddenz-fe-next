/* eslint-disable */
/* eslint-disable camelcase */
// import React from 'react';
/* eslint-disable indent */

import React, { useState, useEffect, useRef } from 'react';
import Router from 'next/router';
import PropTypes from 'prop-types';
import moment from 'moment';
import TimePicker from 'rc-time-picker';
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';
import Geocode from 'react-geocode';
import { MAPS_API_KEY, GAevent } from 'utils/google';
import { CLICK_COUNTS } from 'utils/constants';
import { useDropdownClose, useClickCount } from 'shared/hooks';
import styled from 'styled-components';
import Button from '../Button';

import Flex from '../common/flex';
import Modal from '../Modal';
import RadioButton from '../RadioButton';
import ProfileDropdown from '../ProfileDropdown';
// import { IconCard } from '../BranchRating/styled';
import colors from '../../utils/colors';
import Form from '../../containers/HomePage/Form';
import Input from '../../containers/HomePage/Input';
import profileImage from '../../images/profileImage.jpg';
import kiddenLogo from '../../images/kiddenz-purple logo.svg';
import kiddenLogo1 from '../../images/kiddenz-white logo.svg';
import route from '../../images/toroute.svg';
// import profile from '../../images/profile.jpg';
import profile from '../../images/parents-placeholder.png';
import poweredByGoogle from '../../images/powered_by_google_on_white.png';
function Header({
  type = 'primary',
  placeHolderValue,
  searchBar,
  isModalActive,
  setActiveCallback = () => {},
  activeModalType,
  disableAuthActionButtons = false,
  disableNavItems = false,
  locationSearchMethod,
  setLocationSearchMethod,
  showLocation,
  onLocationClick,
  selectedLocation,
  ...props
}) {
  Geocode.setApiKey(MAPS_API_KEY);
  const locationSearchDropdown = useRef(null);
  const tripLocationSearchDropdown = useRef(null);
  const timePicker = useRef(null);

  const searchDropdown = useRef(null);

  const {
    postCount,
    clickData: {
      location_search: locationSearchClickCount = 0,
      trip_search: tripSearchClickCount = 0,
    } = {},
  } = useClickCount(props);

  const {
    authentication: {
      profile: {
        is_onboarded = true,
        data: { name = '', photo_url: photoUrl } = {},
      } = {},
      isLoggedIn,
    } = {},
  } = props;

  const {
    router: { pathname, query: { form, provider_slug: slug } = {} } = {},
  } = props;

  const [searchstatus, setSearchStatus] = useState(false);
  const [screenWidth, setScreenWidth] = useState(true);
  const [modalActive, setModalActive] = useState(false);
  const [modalType, setModalType] = useState(null);
  const [restrictionType, setRestrictionType] = useState(null);

  // temp
  const [searchType, setSearchType] = useState('location');
  // const [searchBtnDisabled, setSearchBtnDisabledStatus] = useState('location');
  const [locationSearchSting, setLocationSearchSting] = useState('');
  const [locationSearchError, setLocationSearchError] = useState(false);
  const [
    locationSearchDropdownState,
    setLocationSearchDropdownState,
  ] = useState(false);
  const [locationLat, setLocationLat] = useState(0.0);
  const [locationLng, setLocationLng] = useState(0.0);
  const [locationItemClicked, setLocationItemClicked] = useState(false);
  const [locationGeoCodeInProgress, setlocationGeoCodeInProgress] = useState(
    false,
  );

  const [activeTripLocationInput, setActiveTripLocationInput] = useState(null);
  const [fromLocationSearchSting, setFromLocationSearchSting] = useState('');
  const [toLocationSearchSting, setToLocationSearchSting] = useState('');

  const [fromLocationSearchError, setFromLocationSearchError] = useState(false);
  const [toLocationSearchError, setToLocationSearchError] = useState(false);

  const [
    tripLocationSearchDropdownState,
    setTripLocationSearchDropdownState,
  ] = useState(false);

  const [fromLocationLat, setFromLocationLat] = useState(0.0);
  const [toLocationLat, setToLocationLat] = useState(0.0);
  const [fromLocationLng, setFromLocationLng] = useState(0.0);
  const [toLocationLng, setToLocationLng] = useState(0.0);

  const [fromTime, setFromTime] = useState('08:00');
  // const [fromTimeError, setFromTimeError] = useState(false);

  const [authActionSelected, setAuthActionSelected] = useState('login');

  // Articles
  const [articleSearchString, setArticleSearchString] = useState('');

  useDropdownClose(
    locationSearchDropdown,
    setLocationSearchDropdownState,
    false,
    {
      exclude: timePicker,
    },
  );
  useDropdownClose(searchDropdown, () => {
    setSearchStatus(false);
    setArticleSearchString('');
    setLocationSearchSting('');
    setFromLocationSearchSting('');
    setFromLocationSearchError('');
    setToLocationSearchSting('');
    setToLocationSearchError('');
    setFromTime('08:00');
    setSearchType('location');
  });

  useEffect(
    () => {
      setModalActive(isModalActive);
      setModalType(activeModalType);
    },
    [isModalActive, activeModalType],
  );


  useEffect(() => {
    if(props.router.asPath === '/'){
     setTimeout(() => {
      if (!isLoggedIn) {
        setModalType('signup');
        setAuthActionSelected('register');
        setModalActive(true);
      }
    }, 5000);
  }
  }, [isLoggedIn]);

  useEffect(
    () => {
      if (form === 'review_form') {
        if (isLoggedIn) {
          Router.push(`/review?slug=${slug}`).then(() => window.scrollTo(0, 0));
        } else {
          setModalActive(true);
          setModalType('login');
          setAuthActionSelected('login');
        }
      }
    },
    [form],
  );

  /*
    Code block to open login modal in seach result page, if user is not logged in
    NOTE: This Feature is now not Required
  */
  // useEffect(
  //   () => {
  //     if (!isLoggedIn) {
  //       if (pathname === '/daycare/search') {
  //         setModalType('login');
  //         setModalActive(true);
  //       }
  //     }
  //   },
  //   [isLoggedIn],
  // );

  useEffect(
    () => {
      if (global.window && window.innerWidth < 769) {
        setScreenWidth(false);
      } else {
        setScreenWidth(true);
      }
    },
    [global.window && global.window.innerWidth],
  );

  useEffect(
    () => {
      if (modalActive) {
        document.body.style.overflow = 'hidden';
      } else {
        document.body.style.overflow = 'unset';
      }
      return () => {
        document.body.style.overflow = 'unset';
      };
    },
    [modalActive],
  );

  useEffect(
    () => {
      if (isLoggedIn && !is_onboarded) {
        setModalActive(true);
        setModalType('onboarding');
      }
    },
    [is_onboarded],
  );

  const handleSelect = (selected, isDefault = true, type) => {
    setlocationGeoCodeInProgress(true);
    setLocationItemClicked(true);

    // If normal Location search
    if (isDefault) {
      setLocationSearchSting(selected);
      setLocationSearchDropdownState(false);
      setLocationSearchError(false);
    }

    // If Trip search
    if (type === 'from') {
      setFromLocationSearchSting(selected);
      setTripLocationSearchDropdownState(false);
      setFromLocationSearchError(false);
    }
    if (type === 'to') {
      setToLocationSearchSting(selected);
      setTripLocationSearchDropdownState(false);
      setToLocationSearchError(false);
    }

    geocodeByAddress(selected)
      .then(res => getLatLng(res[0]))
      .then(({ lat, lng }) => {
        // If normal Location search
        if (isDefault) {
          setLocationLat(lat);
          setLocationLng(lng);
          if (window.innerWidth <= 768) {
            if (isLoggedIn) {
              /* If user has click location search equal to or more than n times 
                Block the content access
            */
              if (locationSearchClickCount >= CLICK_COUNTS.locationSearch) {
                setModalType('restriction');
                setRestrictionType('location search');
                setModalStatus(true);
              } else {
                postCount('location_search');
                GAevent('Search', 'Location Search Clicked', 'Location Search');
                Router.push(
                  `/daycare/search?search_type=${searchType}&locationString=${selected}&latitude=${lat}&longitude=${lng}&max_age=${Math.floor(
                    maxAge * 12,
                  )}&transportation=0&food=0&type=radius&sort_by=distance`,
                ).then(() => window.scrollTo(0, 0));
              }
            } else {
              GAevent('Search', 'Location Search Clicked', 'Location Search');
              Router.push(
                `/daycare/search?search_type=${searchType}&locationString=${selected}&latitude=${lat}&longitude=${lng}&type=radius&sort_by=distance`,
              ).then(() => window.scrollTo(0, 0));
            }
          }
        }

        // If Trip search
        if (type === 'from') {
          setFromLocationLat(lat);
          setFromLocationLng(lng);
        } else {
          setToLocationLat(lat);
          setToLocationLng(lng);
        }

        setlocationGeoCodeInProgress(false);
      })
      .catch(error => {
        // this.setState({ isGeocoding: false });
      });
  };

  const closeHandler = () => {
    setModalActive(false);
    setActiveCallback(false);
  };

  // Article Search handlers
  const onArticlesSearchInputChange = e => {
    setArticleSearchString(e.target.value);
  };

  const onSearchInputKeyPress = e => {
    if (event.key === 'Enter') {
      e.preventDefault();
      if (articleSearchString) {
        Router.push(
          `/blog/search?key=${encodeURIComponent(
            articleSearchString,
          )}`,
        ).then(() => window.scrollTo(0, 0));
      } else {
        return 0;
      }
    }
  };

  const authActionClick = (actionType = 'login') => {
    if (actionType === 'login') {
      setModalType('login');
      setAuthActionSelected('login');
      // setModalType('onboarding');
      setModalActive(true);
    }
    if (actionType === 'register') {
      setModalType('signup');
      setAuthActionSelected('register');
      setModalActive(true);
    }
  };

  return (
    <>
      <HeaderWrapper className={type}>
        <NavBar className={type}>
          <NavBarItem className="logoLocationWrapper">
            <Logo
              onClick={() =>
                Router.push({
                  pathname: '/',
                }).then(() => window.scrollTo(0, 0))
              }
            >
              <img
                src={type === 'articles' ? kiddenLogo : kiddenLogo1}
                alt=""
              />
            </Logo>
            {showLocation && (
              <LocationPin onClick={onLocationClick}>
                <span>{selectedLocation || 'Select Location'}</span>
                <p>▼</p>
              </LocationPin>
            )}
            {(type === 'secondary' ||
              type === 'ternary' ||
              type === 'articles') &&
              screenWidth &&
              searchBar && (
                <SearchBar
                  className="searchBarMobile"
                  onClick={() => setSearchStatus(!searchstatus)}
                  isArticles={props.isArticles}
                  hasBorder={!props.isArticles}
                >
                  {props.isArticles ? (
                    <label htmlFor="search" style={{ width: '100%' }}>
                      <SearchInput
                        id="search"
                        type="text"
                        placeholder={
                          placeHolderValue || 'Cunningam Rd - Queens Rd'
                        }
                        value={articleSearchString}
                        onChange={onArticlesSearchInputChange}
                        onKeyPress={onSearchInputKeyPress}
                      />
                    </label>
                  ) : (
                    <>
                      <h3>
                        {(placeHolderValue || '').length > 52
                          ? `${placeHolderValue.slice(0, 51)}...`
                          : placeHolderValue}
                      </h3>
                    </>
                  )}
                  <span
                    className="iconify"
                    data-icon="bytesize:search"
                    data-inline="false"
                  />
                </SearchBar>
              )}
          </NavBarItem>
          {!disableNavItems &&
            !screenWidth && (
              <NavBarItem className="mobileOnly">
                {(type === 'secondary' ||
                  type === 'ternary' ||
                  type === 'articles') &&
                  searchBar && (
                    <Button
                      type="links"
                      text={
                        <span
                          className="iconify search"
                          data-icon="bytesize:search"
                          data-inline="false"
                        />
                      }
                      onClick={() => setSearchStatus(!searchstatus)}
                    />
                  )}
                {(type === 'ternary' ||
                  type === 'articles' ||
                  type === 'secondary' ||
                  type === 'primary') && (
                  <ProfileDropdown
                    name={`Hi ${name}`}
                    image={photoUrl || profile}
                    // type="onmobile"
                    isLoggedIn={isLoggedIn}
                    authActionClick={authActionClick}
                    isMobile
                    {...props}
                  />
                )}
              </NavBarItem>
            )}
          {!disableNavItems &&
            screenWidth && (
              <NavBarItem className="mobileMenu">
                <Menu>
                  <MenuItem
                    onClick={() =>
                      Router.push({
                        pathname: '/about-us',
                      }).then(() => window.scrollTo(0, 0))
                    }
                    className={pathname === '/about-us' ? 'active' : ''}
                  >
                    About
                  </MenuItem>
                  <MenuItem
                    onClick={() =>
                      Router.push({
                        pathname: '/blog',
                      }).then(() => window.scrollTo(0, 0))
                    }
                    className={props.isArticles ? 'active' : ''}
                  >
                    Parenting Articles
                  </MenuItem>
                  <MenuItem
                    onClick={() =>
                      Router.push({
                        pathname: '/provider',
                      }).then(() => window.scrollTo(0, 0))
                    }
                    className={pathname === '/provider' ? 'active' : ''}
                  >
                    For Providers
                  </MenuItem>
                </Menu>
                {!disableAuthActionButtons &&
                  !isLoggedIn &&
                  (type === 'primary' ||
                    type === 'secondary' ||
                    type === 'articles') && (
                    <>
                      <Button
                        type="mobile"
                        text="Login"
                        headerButton
                        marginRight="10px"
                        fontWeight="600"
                        onClick={() => {
                          setModalType('login');
                          setAuthActionSelected('login');
                          // setModalType('onboarding');
                          setModalActive(true);
                        }}
                        height="46px"
                      />
                      <Button
                        text="Register"
                        type="outline"
                        fontWeight="600"
                        headerButton
                        onClick={() => {
                          setModalType('signup');
                          setAuthActionSelected('register');
                          setModalActive(true);
                        }}
                        height="46px"
                      />
                    </>
                  )}
                {!disableAuthActionButtons &&
                  isLoggedIn &&
                  (type === 'ternary' ||
                    type === 'articles' ||
                    type === 'secondary' ||
                    type === 'primary') && (
                    <ProfileDropdown
                      name={`Hi ${name}`}
                      image={photoUrl || profile}
                      type="onmobile"
                      isLoggedIn={isLoggedIn}
                      {...props}
                    />
                  )}
              </NavBarItem>
            )}
        </NavBar>
        {!props.isArticles && (
          <div style={{ position: 'relative' }}>
            {searchstatus && (
              <SearchOption
                column
                flexPadding="50px"
                flexBorderRadius="10px"
                whitebg="#fff"
                ref={searchDropdown}
              >
                <Flex flexWidth="100%" flexMargin="0px 0px 20px">
                  <HeaderRadioButton
                    id="location"
                    text="Location"
                    filterRadio="#30333B"
                    filterFont="14px"
                    filterFontWeight="400"
                    filterLineheight="16px"
                    filterPadding="4px 0px 0px 35px"
                    RadioMargin="0px 100px 0px 0px"
                    checked={searchType === 'location'}
                    onClick={() => {
                      setSearchType('location');
                      setFromLocationSearchSting('');
                      setToLocationSearchSting('');
                      setToLocationSearchError(false);
                      setFromLocationSearchError(false);
                    }}
                  />
                  <HeaderRadioButton
                    id="triproute"
                    text="Trip Route"
                    filterRadio="#30333B"
                    filterFont="14px"
                    filterFontWeight="400"
                    filterLineheight="16px"
                    filterPadding="4px 0px 0px 35px"
                    // RadioMargin="0px 0px 0px 112px"
                    checked={searchType === 'trip'}
                    onClick={() => {
                      setSearchType('trip');
                      setLocationSearchSting('');
                      setLocationSearchError(false);
                    }}
                  />
                </Flex>
                {searchType === 'location' ? (
                  <PlacesAutocomplete
                    style={{ position: 'relative' }}
                    value={locationSearchSting}
                    onChange={address => {
                      setLocationItemClicked(false);
                      setLocationSearchSting(address);
                    }}
                    onSelect={address => handleSelect(address)}
                    onError={() => {}}
                    clearItemsOnError
                    shouldFetchSuggestions
                    searchOptions={{
                      componentRestrictions: { country: ['in'] },
                    }}
                  >
                    {({
                      getInputProps,
                      suggestions,
                      getSuggestionItemProps,
                    }) => {
                      if (suggestions.length > 0)
                        setLocationSearchDropdownState(true);
                      else setLocationSearchDropdownState(false);

                      return (
                        <>
                          <label htmlFor="search_box">
                            <div className="searchInputField">
                              <Input
                                {...getInputProps({
                                  onBlur: () => {
                                    if (locationSearchSting)
                                      setLocationSearchError(false);
                                    else setLocationSearchError(true);

                                    if (locationItemClicked)
                                      setLocationSearchError(false);
                                    else setLocationSearchError(true);
                                  },
                                })}
                                id="search_box"
                                type="text"
                                placeholder="Enter your location"
                                value={locationSearchSting}
                                disabled={searchType !== 'location'}
                                autoComplete="off"
                                className="search"
                              />
                            </div>
                          </label>
                          {locationSearchError && (
                            <DestinationError>
                              Location is Required
                            </DestinationError>
                          )}

                          {locationSearchDropdownState &&
                            suggestions.length > 0 && (
                              <SearchOptionHeader
                                column
                                ref={locationSearchDropdown}
                              >
                                {suggestions.map(suggestion => (
                                  <li
                                    {...getSuggestionItemProps(suggestion, {})}
                                  >
                                    {suggestion.description}
                                  </li>
                                ))}
                                <li className="google">
                                  <img
                                    src={poweredByGoogle}
                                    alt="googleImage"
                                  />
                                </li>
                              </SearchOptionHeader>
                            )}
                        </>
                      );
                    }}
                  </PlacesAutocomplete>
                ) : (
                  <>
                    <PlacesAutocomplete
                      value={
                        activeTripLocationInput === 'from'
                          ? fromLocationSearchSting
                          : toLocationSearchSting
                      }
                      onChange={address => {
                        setLocationItemClicked(false);
                        if (activeTripLocationInput === 'from') {
                          setFromLocationSearchSting(address);
                        } else {
                          setToLocationSearchSting(address);
                        }
                      }}
                      onSelect={address =>
                        handleSelect(address, false, activeTripLocationInput)
                      }
                      onError={() => {}}
                      clearItemsOnError
                      shouldFetchSuggestions
                      searchOptions={{
                        componentRestrictions: { country: ['in'] },
                      }}
                    >
                      {({
                        getInputProps,
                        suggestions,
                        getSuggestionItemProps,
                      }) => {
                        if (suggestions.length > 0)
                          setTripLocationSearchDropdownState(true);
                        else setTripLocationSearchDropdownState(false);
                        return (
                          <>
                            <Location
                              flexMargin="27px 0px 0px 0px"
                              style={{ position: 'relative' }}
                            >
                              <LocationFromTo>
                                <LocationFrom>
                                  <div>&nbsp;</div>
                                </LocationFrom>
                                <LocationTo>
                                  <img src={route} alt="" />
                                </LocationTo>
                              </LocationFromTo>
                              <Flex
                                flexWidth="95%"
                                flexMargin="0px 10px 0px 0px"
                                column
                                className="findLocation"
                              >
                                <Flex
                                  style={{
                                    borderBottom: '1px solid #E5E5E5',
                                  }}
                                >
                                  <Destination
                                    {...getInputProps({
                                      onBlur: () => {
                                        if (fromLocationSearchSting)
                                          setFromLocationSearchError(false);
                                        else setFromLocationSearchError(true);

                                        if (locationItemClicked)
                                          setFromLocationSearchError(false);
                                        else setFromLocationSearchError(true);
                                      },
                                    })}
                                    type="text"
                                    placeholder="From"
                                    disabled={searchType !== 'trip'}
                                    onClick={() => {
                                      setActiveTripLocationInput('from');
                                    }}
                                    value={fromLocationSearchSting}
                                  />
                                </Flex>
                                <Flex style={{ position: 'relative' }}>
                                  <Destination
                                    {...getInputProps({
                                      onBlur: () => {
                                        if (fromLocationSearchSting)
                                          setToLocationSearchError(false);
                                        else setToLocationSearchError(true);

                                        if (locationItemClicked)
                                          setToLocationSearchError(false);
                                        else setToLocationSearchError(true);
                                      },
                                    })}
                                    type="text"
                                    placeholder="To"
                                    style={{ marginTop: '15px' }}
                                    disabled={searchType !== 'trip'}
                                    onClick={() => {
                                      setActiveTripLocationInput('to');
                                    }}
                                    value={toLocationSearchSting}
                                  />
                                </Flex>
                              </Flex>

                              <TimeInfo>
                                <Flex row flexPadding="10px 0px 10px 10px">
                                  <Flex
                                    // flexPadding="10px 27px 10px 10px"
                                    alignCenter
                                    className="timeInfo"
                                  >
                                    <span
                                      className="iconify"
                                      data-icon="simple-line-icons:clock"
                                      data-inline="false"
                                    />
                                  </Flex>
                                  <TimePicker
                                    defaultValue={moment()
                                      .hour(8)
                                      .minute(0)}
                                    ref={timePicker}
                                    placeholder="Ideal departure time"
                                    onChange={e => {
                                      // e.stopPropagation();
                                      // eslint-disable-next-line no-underscore-dangle
                                      setFromTime(moment(e._d).format('HH:MM'));
                                    }}
                                    showSecond={false}
                                    minuteStep={5}
                                  />
                                </Flex>
                              </TimeInfo>
                            </Location>

                            {(fromLocationSearchError ||
                              toLocationSearchError) && (
                              <DestinationError>
                                Locations with ideal time is required.
                              </DestinationError>
                            )}
                            {tripLocationSearchDropdownState &&
                              suggestions.length > 0 && (
                                <TripSearchOptionHeader
                                  column
                                  ref={tripLocationSearchDropdown}
                                  top="435px"
                                >
                                  {suggestions.map(suggestion => (
                                    <li
                                      {...getSuggestionItemProps(
                                        suggestion,
                                        {},
                                      )}
                                    >
                                      {suggestion.description}
                                    </li>
                                  ))}
                                  <li className="google">
                                    <img
                                      src={poweredByGoogle}
                                      alt="googleImage"
                                    />
                                  </li>
                                </TripSearchOptionHeader>
                              )}
                          </>
                        );
                      }}
                    </PlacesAutocomplete>
                  </>
                )}

                <Flex flexMargin="20px auto 0px 0px">
                  <Button
                    text="Search"
                    type="secondary"
                    onClick={e => {
                      // amplitude.getInstance().logEvent('SEARCH');
                      e.preventDefault();
                      setSearchStatus(false);
                      if(searchType === 'location') {
                        if (!locationSearchSting)
                        setLocationSearchError(true);
                      else {
                        setLocationSearchError(false);
                        let cleanloc = locationSearchSting
                        cleanloc = cleanloc.replace(/\s/g,'');
                        let urlElements = cleanloc.split(',').reverse();
                        var finalurl='';
                        for(let i=2;i<urlElements.length;i++){
                          finalurl += `/${urlElements[i]}`
                        }
                       // let finalurl = `${urlElements[2]}/${urlElements[3]}/${urlElements[4]}/${urlElements[5]}/${urlElements[6]}/${urlElements[6]}/${urlElements[7]}`
                        console.log("cleanloc:",cleanloc)
                        console.log("urlelemnonrev:",urlElements)
                        console.log("finalurl:",finalurl)
                       // console.log('afterpop:',part1)
                        if (isLoggedIn) {
                          /* If user has click location search equal to or more than n times
                              Block the content access
                          */
                          if (
                            locationSearchClickCount >=
                            CLICK_COUNTS.locationSearch
                          ) {
                            setModalType('restriction');
                            setRestrictionType('location search');
                            setModalStatus(true);
                          } else {
                            postCount('location_search');
                            GAevent(
                              'Search',
                              'Location Search Clicked',
                              'Location Search',
                            );
                            Router.push(
                              `/daycare/search?search_type=${searchType}&locationString=${locationSearchSting}&latitude=${locationLat}&longitude=${locationLng}&transportation=0&food=0&type=radius&sort_by=distance`,
                            ).then(() => window.scrollTo(0, 0));
                          }
                        } else {
                          GAevent(
                            'Search',
                            'Location Search Clicked',
                            'Location Search',
                          );
                          Router.push(
                            `/daycare/search?search_type=${searchType}&locationString=${locationSearchSting}&latitude=${locationLat}&longitude=${locationLng}&type=radius&sort_by=distance`,
                          ).then(() => window.scrollTo(0, 0));
                        }
                      }
                      } else {
                      if (
                        !fromLocationSearchSting ||
                        !toLocationSearchSting
                      ) {
                        if (
                          !fromLocationSearchSting &&
                          !toLocationSearchSting
                        ) {
                          setFromLocationSearchError(true);
                          setToLocationSearchError(true);
                        }

                        if (
                          fromLocationSearchSting &&
                          !toLocationSearchSting
                        ) {
                          setFromLocationSearchError(false);
                          setToLocationSearchError(true);
                        }

                        if (
                          toLocationSearchSting &&
                          !fromLocationSearchSting
                        ) {
                          setFromLocationSearchError(true);
                          setToLocationSearchError(false);
                        }
                        // setFromTimeError(true);
                      } else {
                        setFromLocationSearchError(false);
                        setToLocationSearchError(false);
                        // setFromTimeError(false);

                        if (isLoggedIn) {
                          /* If user has click trip search equal to or more than n times 
                              Block the content access
                          */
                          if (
                            tripSearchClickCount >=
                            CLICK_COUNTS.tripSearch
                          ) {
                            setModalType('restriction');
                            setRestrictionType('trip search');
                            setModalStatus(true);
                          } else {
                            postCount('trip_search');
                            GAevent(
                              'Search',
                              'Trip Search Clicked',
                              'Trip Search',
                            );
                            Router.push(
                              `/daycare/search?search_type=${searchType}&toLocationString=${toLocationSearchSting}&fromLocationString=${fromLocationSearchSting}&fromLat=${fromLocationLat}&toLat=${toLocationLat}&fromLng=${fromLocationLng}&toLng=${toLocationLng}&timehrs=${
                                fromTime.split(':')[0]
                              }&timemins=${
                                fromTime.split(':')[1]
                              }&transportation=0&food=0&type=radius&sort_by=distance`,
                            ).then(() => window.scrollTo(0, 0));
                          }
                        } else {
                          GAevent(
                            'Search',
                            'Trip Search Clicked',
                            'Trip Search',
                          );
                          let newtoLocationSearchSting = toLocationSearchSting
                          let newfromLocationSearchSting = fromLocationSearchSting

                          newtoLocationSearchSting = newtoLocationSearchSting.replace(/\s/g,'');
                          newfromLocationSearchSting = newfromLocationSearchSting.replace(/\s/g,'');
                          
                          let newurl1 = newtoLocationSearchSting.split(',');
                          let newurl2 = newfromLocationSearchSting.split(',');

                          let finalurl1 = newurl1[0].toLowerCase();
                          let finalurl2 = newurl2[0].toLowerCase();

                          console.log(finalurl1,finalurl2);
                          Geocode.fromLatLng(fromLocationLat, toLocationLng).then(
                            (response) => {
                              const address = response.results[0].formatted_address;
                              let city, state, country, cityurl;
                              for (let i = 0; i < response.results[0].address_components.length; i++) {
                                for (let j = 0; j < response.results[0].address_components[i].types.length; j++) {
                                  switch (response.results[0].address_components[i].types[j]) {
                                    case "locality":
                                      city = response.results[0].address_components[i].long_name;
                                      cityurl = city.replace(/\s/g,'').toLowerCase();
                                     
                                      break;
                                    case "administrative_area_level_1":
                                      state = response.results[0].address_components[i].long_name;
                                      break;
                                    case "country":
                                      country = response.results[0].address_components[i].long_name;
                                      break;
                                  }
                                }
                              }
                              Router.push(
                                `/daycare/search?search_type=${searchType}&toLocationString=${toLocationSearchSting}&fromLocationString=${fromLocationSearchSting}&fromLat=${fromLocationLat}&toLat=${toLocationLat}&fromLng=${fromLocationLng}&toLng=${toLocationLng}&timehrs=${
                                  fromTime.split(':')[0]
                                }&timemins=${
                                  fromTime.split(':')[1]
                                }&type=radius&sort_by=distance`,
                                {pathname:`/${cityurl}/${finalurl2}-${finalurl1}`},
                                console.log("Lat-Long",locationLat,locationLng)
                                ).then(() => window.scrollTo(0, 0));
                              console.log("City,State,Country",city, state, country);
                              console.log("Address",address);
                            },
                            (error) => {
                              console.error(error);
                            });
                        }
                      }
                    }
                    }}
                    disabled={locationGeoCodeInProgress}
                  />
                </Flex>
              </SearchOption>
            )}
          </div>
        )}
        {props.isArticles &&
          searchstatus && (
            <MobileSearchArticle ref={searchDropdown}>
              <SearchArticleInput>
                <input
                  type="text"
                  placeholder="Search Article"
                  value={articleSearchString}
                  onChange={onArticlesSearchInputChange}
                  // onKeyPress={onSearchInputKeyPress}
                />
              </SearchArticleInput>
              <Button
                type="secondary"
                text="Search"
                height="44px"
                marginTop="20px"
                onClick={() => {
                  setSearchStatus(false);
                  if (articleSearchString) {
                    Router.push(
                      `/blog/search?key=${encodeURIComponent(
                        articleSearchString,
                      )}`,
                    ).then(() => window.scrollTo(0, 0));
                  } else {
                    return 0;
                  }
                  setArticleSearchString('');
                }}
              />
            </MobileSearchArticle>
          )}
      </HeaderWrapper>
      {modalActive && (
        <Modal
          type={modalType}
          setAuthActionSelected={setAuthActionSelected}
          setActive={setModalActive}
          setModalType={setModalType}
          closeHandler={closeHandler}
          authActionSelected={authActionSelected}
          clickType={restrictionType}
          {...props}
        />
      )}
      {/* {modalActive && <Modal type={'onboarding'} setActive={setModalActive} />} */}
    </>
  );
}
Header.propTypes = {
  type: PropTypes.string,
  placeHolderValue: PropTypes.string,
  searchBar: PropTypes.string,
  authentication: PropTypes.object,
  router: PropTypes.object,
  disableAuthActionButtons: PropTypes.bool,
};
export default Header;

const HeaderWrapper = styled.section`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  z-index: 999999;
  background-color: ${colors.secondary};
  &.articles {
    background-color: ${colors.white};
  }
  &.ternary {
    background-color: ${colors.white};
    box-shadow: 5px 15px 20px 0 rgba(0, 0, 0, 0.1);
  }
  .logoLocationWrapper {
    @media screen and (max-width: 995px) {
      flex-direction: column;
      align-items: flex-start;
    }
  }
  .logoLocationWrapper {
    @media screen and (max-width: 768px) {
      flex-direction: row;
      align-items: center;
    }
  }
`;
const Logo = styled.div`
  // height: 34px;
  // width: 136px;
  cursor: pointer;
  @media (max-width: 995px) {
    width: 118px;
  }
  @media (max-width: 500px) {
    height: 100%;
    width: 80px;
  }
  img {
    width: 100%;
    height: 100%;
    @media (max-width: 500px) {
      transform: scale(1.2);
    }
  }
`;
const LocationPin = styled.div`
  border: 1px solid #0000004a;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0 10px;
  min-height: 35px;
  border-radius: 5px;
  cursor: pointer;
  margin-left: 30px;
  @media screen and (max-width: 995px) {
    margin-left: 8px;
    margin-top: -6px;
    padding: 10px;
    font-size: 12px;
    height: 24px;
  }
  @media screen and (max-width: 768px) {
    padding: 0px 5px;
    height: 25px;
    border-radius: 5px;
    margin-left: 10px;
    margin-top: 0;
  }
  span {
    color: #30333b;
    @media screen and (max-width: 768px) {
      font-size: 12px;
    }
  }
  p {
    color: #30333b;
    margin: 2px 0 0 12px;
    font-size: 12px;
    @media screen and (max-width: 768px) {
      margin: 2px 0 0 2px;
    }
  }
`;
const Menu = styled.ul`
  // width: 100%;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  margin-right: 60px;
  color: ${colors.white};
  @media (max-width: 1024px) {
    margin-right: 20px;
  }
  @media (max-width: 768px) {
    flex-direction: column;
    margin-right: 0px;
    padding: 0px;
  }
`;
const MenuItem = styled.li`
  margin-right: 40px;
  font-family: Quicksand;
  font-size: 16px;
  font-weight: 600;
  letter-spacing: 0;
  line-height: 20px;
  white-space: nowrap;
  cursor: pointer;
  list-style-type: none;
  position: relative;
  @media screen and (max-width: 1085px) {
    margin-right: 20px;
  }
  &:hover,
  &.active {
    color: ${colors.white};
  }
  &::after {
    position: absolute;
    content: '';
    width: 0%;
    left: 0px;
    bottom: -20px;
    height: 3px;
    transition: width 0.3s ease;
    background-color: ${colors.white};
  }
  &:hover::after,
  &.active::after {
    width: 100%;
    transition: width 0.3s ease;
  }

  &:last-child {
    margin-right: 0px;
  }
  @media (max-width: 1024px) {
    margin-right: 20px;
  }
  @media (max-width: 768px) {
    width: 100%;
    margin-right: 0px;
    margin-bottom: 10px;
    text-align: center;
  }
`;
const SearchInput = styled(Input)`
  height: 46px;
`;

const SearchOptionHeader = styled(Flex)`
  position: absolute;
  top: 154px;
  width: 100%;
  max-width: 442px;
  padding: 20px 20px 40px;
  border-radius: 5px;
  background: ${colors.white};
  box-shadow: 0 0 4px 0 rgba(0, 0, 0, 0.3);
  z-index: 1000000;
  .google {
    position: absolute;
    right: 20px;
    bottom: 0px;
    // padding:20px 0px;
  }
  li {
    font-size: 14px;
    line-height: 30px;
    border-bottom: 1px solid rgba(0, 0, 0, 0.1);
    list-style-type: none;
    padding: 5px 0px;
    color: #666c78;
    cursor: pointer;
    &:last-child {
      border-bottom: 0px solid rgba(0, 0, 0, 0.1);
    }
    &:hover {
      color: #000;
    }
  }
`;
const NavBarItem = styled.section`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  &:first-child {
    flex-grow: 1;
  }
  .outline {
    &:hover {
      background-color: #613a95;
      border-color: #fff;
      color: #fff;
    }
  }
  &.mobileOnly {
    display: none;
    margin-left: auto;

    .iconify {
      color: ${colors.white};
      margin-right: 15px;
      font-size: 30px;
      @media (max-width: 500px) {
        margin-right: 0px;
        margin-left: 30px;
      }

      &.search {
        font-size: 22px;
      }

      &:last-child {
        margin-right: 0px;
      }
    }
    @media (max-width: 768px) {
      display: flex;
    }
  }
  &.mobileMenu {
    button {
      font-weight: 600;
    }
    @media (max-width: 768px) {
      position: absolute;
      display: none;
      top: 100%;
      left: 0;
      width: 100%;
      padding-bottom: 20px;
      flex-direction: column;
      justify-content: center;
      background: ${colors.secondary};
      box-shadow: 5px 15px 20px 0 rgba(0, 0, 0, 0.1);
    }
  }
  & .searchBarMobile {
    @media (max-width: 768px) {
      display: none;
    }
  }
`;
const NavBar = styled.section`
  // position: fixed;
  // top: 0;
  // left: 0;
  width: 100%;
//  max-width: 1520px;
max-width: 100%;
  height: 76px;
  margin: 0px auto;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0px 30px;
  background-color: ${colors.secondary};
  z-index: 9999;
  @media (max-width: 995px) {
    &.locationContainer {
      height:115px;
    }
  }
  @media (max-width: 500px) {
  padding: 0px 24px;
  height:52px;
  }
  &.secondary {
    height: 66px;
    @media (max-width: 500px) {
      height:52px;
      }
  }

  &.articles {
    background-color: ${colors.white};
    ${MenuItem}
    {
      &.active ,&:hover {
        color: ${colors.secondary} !important;
      }
      &::after {
        background-color: ${colors.secondary};
      }
    }
    ${Logo} {
      color: ${colors.secondary};
    }
    ${Menu} {
      color: ${colors.darkGrey};
    }
    ${SearchInput} {
      border: 1px solid #dbdee5;
      border-radius: 5px;
    }
    ${NavBarItem} {
      &.mobileMenu {
        @media (max-width: 768px) {
          padding-bottom: 0px;
        }
      }
      .iconify {
        @media (max-width: 768px) {
          color: #30333b;
        }
      }
  }
  &.ternary {
    background-color: ${colors.white};
    box-shadow: 5px 15px 20px 0 rgba(0, 0, 0, 0.1);
    ${MenuItem}
    {
      &.active {
        color: ${colors.secondary};
      }
      &::after {
        background-color: ${colors.secondary};
      }
    }
    ${Logo} {
      color: ${colors.secondary};
    }
    ${Menu} {
      color: ${colors.darkGrey};
    }
    ${SearchInput} {
      border: 1px solid #dbdee5;
      border-radius: 5px;
    }
    ${NavBarItem} {
      &.mobileMenu {
        @media (max-width: 768px) {
          padding-bottom: 0px;
        }
      }
      .iconify {
        @media (max-width: 768px) {
          color: #30333b;
        }
      }
    }
    ${MenuItem} {
      @media (max-width: 768px) {
        color: ${colors.white};
      }
    }
  }
  @media (max-width: 768px) {
    position: relative;
    height: 56px;
    padding: 0px 10px;
  }
  @media (max-width: 500px) {
    height: 52px;
    padding: 0px 10px;
  }
`;
const SearchOption = styled(Flex)`
  position: absolute;
  top: 0;
  left: 14%;
  width: 542px;
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
  z-index: 1000000;
  @media (max-width: 500px) {
    top: 110%;
    left: 0px;
    width: 100%;
    padding: 20px;
  }
  .searchInputField {
    height: 56px;
    width: 100%;
    max-width: 442px;
    display: flex;
    align-items: center;
    padding: 12px 20px;
    outline: none;
    border: 1px solid #a3a9b7;
    border-radius: 5px;
    background-color: ${colors.white};
    @media (max-width: 500px) {
      padding: 12px 12px;
    }
  }
`;
const HeaderRadioButton = styled(RadioButton)`
  & input[type='radio']:not(:checked) + label {
    padding-top: 4px;
    @media (max-width: 414px) {
      margin: 0px 0px 0px 65px;
    }
  }
  & input[type='radio']:checked + label {
    padding-top: 4px;
    @media (max-width: 414px) {
      margin: 0px 0px 0px 65px;
    }
  }
`;

const SearchBar = styled(Form)`
  min-height: 40px;
  position: relative;
  padding-left: 15px;
  max-width: ${({ isArticles }) => {
    return isArticles ? '450px' : '400px';
  }};
  display: flex;
  align-items: center;
  flex-grow: 1;
  margin: 0px;
  margin-left: 55px;
  border: ${({ hasBorder }) => {
    return hasBorder ? '1px solid #dbdee5' : 'none';
  }};
  border-radius: 5px;
  background-color: #ffffff;
  @media (max-width: 1200px) {
    margin-left: 35px;
  }
  h3 {
    color: #4b5256;
    font-size: 14px;
    line-height: 17px;
    font-weight: 400;
    @media (max-width: 1200px) {
      font-size: 12px;
    }
  }

  .iconify {
    position: absolute;
    width: 27px;
    height: 22px;
    color: #4b5256;
    background-color: #fff;
    right: 15px;
    top: 12px;
    @media (max-width: 1200px) {
      width: 18px;
      height: 18px;
      right: 12px;
    }
  }
`;
const DestinationError = styled.span`
  margin: 10px 0px 0px 0px;
  color: red;
  font-size: 12px;
  line-height: 14px;
`;
const LocationFromTo = styled.ul`
  list-style-type: none;
  padding: 0px;
  margin: 0px;
  margin-right: 12px;
  @media (max-width: 500px) {
    margin-top: -10px;
  }
`;
const LocationFrom = styled.li`
  position: relative;
  height: 14px;
  width: 14px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 10px;
  border: 1px solid #63cab9;
  // box-shadow: 0px 0px 5px #63cab9;
  background: ${colors.white};
  border-radius: 50px;
  div {
    height: 8px;
    width: 8px;
    border-radius: 50px;
    background: #63cab9;
    color: #63cab9;
  }
  &:after {
    content: '';
    position: absolute;
    top: 15px;
    left: 5px;
    height: 40px;
    border-left: 1px dashed #bfc4ce;
  }
`;
const Destination = styled.input`
  width: 100%;
  margin-bottom: 20px;
  font-size: 16px;
  border: 0px;
  &:disabled {
    background-color: transparent !important;
  }
  &:focus {
    outline: 0;
    border: 0px;
  }

  @media (max-width: 320px) {
    font-size: 14px;
  }
`;
const LocationTo = styled.li`
  margin-top: 35px;
  list-style-type: none;

  img {
    position: static;
  }
`;

const TimeInfo = styled.div`
  border-radius: 5px;
  background-color: #eff2f4;
  // padding: 5px 10px;
  color: ${colors.inputPlaceholder};
  font-family: 'Quicksand', sans-serif;
  font-size: 14px;
  font-weight: 500;
  line-height: 18px;
  position: absolute;
  right: 9px;
  top: 6px;
  height: fit-content;
  .rc-time-picker {
    width: 100%;
    padding-left: 0px;
  }

  @media (max-width: 375px) {
    font-size: 12px;
  }
  .timeInfo {
    @media (max-width: 500px) {
      padding: 6px 9px 6px 10px;
    }
    @media (max-width: 320px) {
      margin-bottom: 0px;
    }
  }
`;
const Time = styled.span`
  margin-left: 10px;
`;
const Location = styled(Flex)`
  height: 113px;
  width: 442px;
  padding: 12px 14px;
  // margin: 27px 0px 0px 0px;
  border-radius: 5px;
  background: ${colors.white};

  .findLocation {
    input {
      padding-right: 140px;
    }
  }
  @media (max-width: 500px) {
    width: 100%;
    height: 86px;
    padding: 5px 14px;
    .findLocation {
      width: 54% !important;
      & + div {
        width: 34% !important;
      }
      input:disabled {
        background-color: transparent !important;
        margin: 5px 0px;
      }
    }
  }
  @media (max-width: 320px) {
    width: 290px;
    margin: 10px 0px 0px 0px;
  }
`;
const TripSearchOptionHeader = styled(Flex)`
  position: absolute;
  top: 224px;
  width: 100%;
  max-width: 442px;
  padding: 20px 20px 40px;
  border-radius: 5px;
  background: ${colors.white};
  box-shadow: 0 0 4px 0 rgba(0, 0, 0, 0.3);
  z-index: 1000000;
  .google {
    position: absolute;
    right: 20px;
    bottom: 0px;
    // padding:20px 0px;
  }
  li {
    font-size: 14px;
    line-height: 30px;
    border-bottom: 1px solid rgba(0, 0, 0, 0.1);
    list-style-type: none;
    padding: 5px 0px;
    color: #666c78;
    cursor: pointer;
    &:last-child {
      border-bottom: 0px solid rgba(0, 0, 0, 0.1);
    }
    &:hover {
      color: #000;
    }
  }
`;

const MobileSearchArticle = styled.div`
  display: none;
  background: #fff;
  padding: 20px;
  position: absolute;
  width: 100%;
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
  z-index: 1000000;
  // &.active {
  //   @media (max-width: 500px) {
  //     display: block;
  //   }
  // }
  @media (max-width: 768px) {
    display: block;
  }
`;
const SearchArticleInput = styled.div`
  height: 46px;
  width: 100%;
  max-width: 442px;
  display: flex;
  align-items: center;
  padding: 10px 20px;
  outline: none;
  border: 1px solid #a3a9b7;
  border-radius: 5px;
  background-color: #ffffff;
  input {
    border: 0px;
    &:focus {
      outline: 0px;
    }
  }
`;
