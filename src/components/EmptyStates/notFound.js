import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Button from '../Button';
import notFound from '../../images/Kiddenz_Playschools not found.png';

const NotFound = ({ margin }) => (
  <Notfound margin={margin}>
    {' '}
    <img src={notFound} alt="" />
    <p>The daycare or preschool you are looking for could not be found.</p>
    <Button text="Continue search" height="56px" />
    <a href="./notFound.js">Go back Home</a>
  </Notfound>
);
NotFound.propTypes = {
  margin: PropTypes.string,
};
export default NotFound;

const Notfound = styled.div`
  width: 100%;
  max-width: 1200px;
  padding: 0px 20px;
  margin: 100px auto 0px;
  display: flex;
  flex-direction: column;
  align-items: center;
  img {
    margin-bottom: 58px;
    width: 100%;
    @media (max-width: 500px) {
      margin-bottom: 30px;
    }
  }

  p {
    color: #555457;
    font-family: Quicksand;
    font-size: 26px;
    letter-spacing: 0;
    line-height: 34px;
    font-weight: 500;
    margin: 0px 0px 30px;
    @media (max-width: 500px) {
      font-size: 16px;
      line-height: 26px;
      margin: 0px 0px 20px;
      text-align: center;
    }
  }
  a {
    color: #30333b;
    font-family: Quicksand;
    font-size: 15px;
    letter-spacing: 0;
    line-height: 19px;
    text-decoration: none;
    margin-top: 15px;
    @media (max-width: 500px) {
      font-size: 13px;
    }
  }
`;
