import React from 'react';
import PropTypes from 'prop-types';
import Router from 'next/router';
import styled from 'styled-components';
import Button from '../Button';
import thumbsup from '../../images/thumbsup.png';
import Header from '../Header';

const VerifyEmail = ({ margin, ...props }) => (
  <VerifyEmails margin={margin}>
    <Header type="articles" disableAuthActionButtons {...props} />
    <img
      src={thumbsup}
      alt=""
      height={200}
      style={{
        height: 200,
        width: 200,
        marginBottom: 30,
      }}
    />
    <p>Email verified successfully.</p>
    <Button
      text="Continue search"
      onClick={() => Router.push('/daycare/landing')}
      height="56px"
    />
    <a href="/">Go back Home</a>
  </VerifyEmails>
);
VerifyEmail.propTypes = {
  margin: PropTypes.string,
};
export default VerifyEmail;

const VerifyEmails = styled.div`
  width: 100%;
  max-width: 1200px;
  padding: 0px 20px;
  margin: 100px auto 0px;
  display: flex;
  flex-direction: column;
  align-items: center;
  & > img {
    margin-bottom: 58px;
    width: 530px;
    height: 530px;
    @media (max-width: 500px) {
      margin-bottom: 30px;
      width: 300px;
      height: 300px;
    }
  }

  p {
    color: #555457;
    font-family: Quicksand;
    font-size: 26px;
    letter-spacing: 0;
    line-height: 34px;
    font-weight: 500;
    margin: 0px 0px 30px;
    @media (max-width: 500px) {
      font-size: 16px;
      line-height: 26px;
      margin: 0px 0px 20px;
      text-align: center;
    }
  }
  a {
    color: #30333b;
    font-family: Quicksand;
    font-size: 15px;
    letter-spacing: 0;
    line-height: 19px;
    text-decoration: none;
    margin-top: 15px;
    @media (max-width: 500px) {
      font-size: 13px;
    }
  }
`;
