import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Flex from '../common/flex';
import noOpening from '../../images/Kiddenz_No openings.png';

const NoOpening = ({ margin }) => (
  <CareerEmpty margin={margin}>
    <Flex column flexWidth="30%">
      {' '}
      <h3>Careers</h3>
      <p>There are no openings listed as of now.</p>
    </Flex>

    <img src={noOpening} alt="" />
  </CareerEmpty>
);
NoOpening.propTypes = {
  margin: PropTypes.string,
};
export default NoOpening;

const CareerEmpty = styled.div`
  width: 100%;
  max-width: 1200px;
  padding: 0px 20px;
  margin: 100px auto 0px;
  display: flex;
  justify-content: space-between;
  img {
    margin-bottom: 30px;
  }
  h3 {
    color: #30333b;
    font-family: Quicksand;
    font-size: 32px;
    font-weight: 600;
    letter-spacing: 0;
    line-height: 40px;
    margin: 0px 0px 30px;
  }
  p {
    color: #555457;
    font-family: Quicksand;
    font-size: 26px;
    letter-spacing: 0;
    line-height: 34px;
    font-weight: 500;
    margin: 0px;
  }
`;
