import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import error from '../../images/Kiddenz_404.png';
import Header from '../Header';

const Error404 = ({ margin, title, ...props }) => (
  <Error margin={margin}>
    <Header type="articles" disableAuthActionButtons {...props} />
    <img src={error} alt="" />
    <h3>{title}</h3>
  </Error>
);
Error404.propTypes = {
  margin: PropTypes.string,
  title: PropTypes.string,
};
export default Error404;

const Error = styled.div`
  max-width: 1200px;
  padding: 0px 20px;
  margin: ${({ margin }) => margin || '100px auto 0px'};
  display: flex;
  flex-direction: column;
  align-items: center;
  @media (max-width: 500px) {
    margin: 0px auto 0px;
  }
  img {
    margin-bottom: 30px;
    @media (max-width: 900px) {
      transform: scale(0.8);
      margin-bottom: 0px;
    }
    @media (max-width: 500px) {
      transform: scale(0.4);
      margin-bottom: -60px;
    }
  }
  h3 {
    color: #555457;
    font-family: Quicksand;
    font-size: 26px;
    letter-spacing: 0;
    line-height: 33px;
    text-align: center;
    margin: 0px;
    @media (max-width: 900px) {
      font-size: 22px;
      line-height: 30px;
    }
    @media (max-width: 500px) {
      font-size: 18px;
      line-height: 26px;
    }
  }
`;
