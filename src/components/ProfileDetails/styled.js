import styled from 'styled-components';
export const ProfileDetail = styled.div`
  margin: ${props => props.margin || '0px'};

  ul {
    margin: 0x;
    padding: 0px;
  }
  & h4 {
    color: #666c78;
    font-family: 'Roboto', sans-serif;
    font-weight: 400;
    font-size: 12px;
    line-height: 16px;
    margin: 0px;
  }
  & li {
    color: #30333b;
    font-family: 'Roboto', sans-serif;
    font-weight: 400;
    font-size: 14px;
    line-height: 22px;
    list-style-type: none;
    margin-top: 10px;
  }
  @media (max-width: 1280px) {
    margin: 0px 20px 0px 0px;
  }
  @media (max-width: 500px) {
    min-width: fit-content;
  }
  &:last-child {
    @media (max-width: 500px) {
      padding-right: 20px;
    }
  }
`;
