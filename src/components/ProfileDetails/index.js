/* eslint-disable no-nested-ternary */
import React from 'react';
import PropTypes from 'prop-types';
import { ProfileDetail } from './styled';
const ProfileDetails = ({
  heading,
  data1,
  data2,
  margin,
  isPreschool,
  isDaycare,
  isSingle,
}) => (
  <ProfileDetail margin={margin}>
    <ul>
      <h4>{heading}</h4>

      {isSingle ? (
        <>
          <li>{data1} </li>
          <li>{data2}</li>
        </>
      ) : isPreschool ? (
        <li>{data1} </li>
      ) : isDaycare ? (
        <li>{data2} </li>
      ) : (
        ''
      )}
    </ul>
  </ProfileDetail>
);
ProfileDetails.propTypes = {
  heading: PropTypes.string,
  data1: PropTypes.string,
  data2: PropTypes.string,
  margin: PropTypes.string,
  isPreschool: PropTypes.string,
  isDaycare: PropTypes.string,
  isSingle: PropTypes.bool,
};
export default ProfileDetails;
