/* eslint-disable jsx-a11y/mouse-events-have-key-events */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable indent */
/* eslint-disable prettier/prettier */
/* eslint-disable no-unused-expressions */
/* eslint-disable import/no-unresolved */
/* eslint-disable  jsx-a11y/click-events-have-key-events */
/* eslint-disable  jsx-a11y/no-static-element-interactions */
import React from 'react';
import Router from 'next/router';
import PropTypes from 'prop-types';
import moment from 'moment';
import Link from 'next/link';
import styled from 'styled-components';
import VisibilitySensor from 'react-visibility-sensor';
import colors from 'utils/colors';
import Flex from '../common/flex';
import PhotoCard from '../PhotoCard';
import BranchRating from '../BranchRating';
import { Photo } from '../PhotoCard/styled';
import Button from '../Button/index';
import star from '../../images/star.svg';

const statusArr = {
  confirm: {
    suggestion: 'Cancel / Reschedule',
  },
  decline: {
    suggestion:
      'Reason for cancellation: Unfortunately we are closed on that day',
  },
  pending: {
    suggestion: {
      tourId: 'BKS29018',
      date: 'Wednesday, Oct 30th 4:00PM',
    },
  },
};

const WithLink = ({ link = true, href, as, children, onSchoolCardClick }) =>
  link ? (
    <Link href={href} as={as}>
      <a>{children}</a>
    </Link>
  ) : (
    <div onClick={onSchoolCardClick}>{children}</div>
  );

WithLink.propTypes = {
  link: PropTypes.bool,
  href: PropTypes.string,
  as: PropTypes.string,
  children: PropTypes.object,
  onSchoolCardClick: PropTypes.func,
};

const SchoolCard = props => (

  <SchoolWrapper
    onMouseOver={() => props.onFocus && props.onFocus(props.slug)}
    onMouseOut={() => props.onFocus && props.onFocus('')}
    className={props.cardtype}
    schoolWrapperMarTop={props.schoolWrapperMarTop}
    schoolWrapperMarRight={props.schoolWrapperMarRight}
    borderTop={props.borderTop}
    padding={props.padding}
  >
    {props.cardtype === 'schoolCardColumn' && (
      <VisibilitySensor
        onChange={isVisible => {
          props.scrollSensorCallback(isVisible);
        }}
      >
        <WithLink
          href="/[city]/[area]/[daycare-id]"
          as={props.redirectionLink}
          link={props.isLink}
          onSchoolCardClick={props.onSchoolCardClick}
        >
          <Flex column>
            <PhotoCard
              hasBoxShadow={props.hasBoxShadow}
              isWishlisted={props.isWishlisted}
              isLoggedIn={props.isLoggedIn}
              margin="0px"
              image={props.schoolImage}
              height={props.schoolCardHeight}
              width={props.schoolCardWidth}
              minWidth={props.schoolCardMinWidth}
              type={props.photocardType}
              onWishlistIconClick={props.onWishlistIconClick}
            />
            <Flex column flexMargin="10px 0px 0px 0px" className="cardContent">
              <Flex justifyBetween wrap>
                {props.category ? (
                  <CategoryLable>{props.category}</CategoryLable>
                ) : (
                  <div style={{ marginBottom: 20 }} />
                )}
                {props.openingStatus && (
                  <OpeningStatus>{props.openingStatus}</OpeningStatus>
                )}
              </Flex>
              <SchoolName>{props.schoolName} {props.schoolBranch}</SchoolName>
              {props.schoolName.length > 26 && (
                <ToolTipSchoolName> {props.schoolName}</ToolTipSchoolName>
              )}
              {props.schoolRating && (
                <BranchRating
                  branchname={props.schoolBranch}
                  ratingNum={props.schoolRating}
                  starImage={star}
                />
              )}
              <SchoolDescColumn>{props.schoolDesc}</SchoolDescColumn>
            </Flex>
          </Flex>
        </WithLink>
      </VisibilitySensor>
    )}
    {props.cardtype === 'wishList' && (
      <Link href={props.redirectionLink}>
        <a href={props.redirectionLink}>
          <Flex
            className="wishList-flex"
            flexMargin={props.schoolCardMar || '0px'}
            onClick={e => {
              e.preventDefault();
              e.stopPropagation();
              if (props.onCardClick) {
                props.onCardClick();
              } else {
                Router.push(props.redirectionLink);
              }
            }}
          >
            <PhotoCard
              isWishlisted={props.isWishlisted}
              isLoggedIn={props.isLoggedIn}
              image={props.schoolImage}
              height={props.schoolCardHeight}
              width={props.schoolCardWidth}
              margin="0px"
              type={props.photocardType}
              onWishlistIconClick={props.onWishlistIconClick}
            />
            <Flex
              className="wishlistContent"
              column
              flexMargin="0px 0px 0px 20px"
              flexWidth="65%"
            >
              <Flex justifyBetween wrap>
                {props.category && (
                  <CategoryLable>{props.category}</CategoryLable>
                )}
                {props.openingStatus && (
                  <OpeningStatus>{props.openingStatus}</OpeningStatus>
                )}
              </Flex>
              <SchoolName className="name">{props.schoolName}</SchoolName>
              <BranchRating
                branchname={props.schoolBranch}
                ratingNum={props.schoolRating}
                starImage={star}
              />
              {props.tourID && (
                <Flex column flexMargin="24px 0px 0px">
                  <AdditionalInfo margin="0px 0px 5px">
                    Tour ID: <span> {props.tourID}</span>
                  </AdditionalInfo>
                  <AdditionalInfo margin="0px 0px 5px">
                    Scheduled on: <span>{props.schedule}</span>
                  </AdditionalInfo>
                  <AdditionalInfo>
                    Contact: <span>{props.contact}</span>
                  </AdditionalInfo>
                </Flex>
              )}

              {(props.hasCancelReason ||
                props.btnText === 'Schedule change') && (
                <Reason
                  column
                  flexMargin="24px 0px 0px"
                  flexPadding="16px 22px"
                  flexBorderRadius="5px"
                  whitebg="#F2EFF5"
                  onClick={e => {
                    e.preventDefault();
                    e.stopPropagation();
                  }}
                >
                  {props.btnType === 'decline' && <span>{props.reason}</span>}
                  {props.btnType === 'pending' &&
                    (props.hasCancelReason ||
                      props.btnText === 'Schedule change') && (
                      <>
                        <span>{`New schedule suggested from ${
                          props.schoolName
                        }`}</span>
                        <Flex column flexMargin="18px 0px 0px">
                          <h2>
                            Date:
                            <span>{props.scheduledate}</span>
                          </h2>
                          <h2>
                            Tour ID:
                            <span>{props.scheduleID}</span>
                          </h2>
                        </Flex>
                      </>
                    )}
                </Reason>
              )}
              {props.btnSchedule === 'Schedule Tour' ? (
                <Flex>
                  <Button
                    type="subscribe"
                    text="Schedule Tour"
                    marginTop="20px"
                    height="50px"
                    isLoading={props.scheduleCheckLoader}
                    onClick={e => {
                      e.preventDefault();
                      e.stopPropagation();
                      props.scheduleTour(e);
                    }}
                  />
                </Flex>
              ) : null}
              {(props.btnType === 'confirm' ||
                (props.btnType === 'pending' &&
                  !props.hasCancelReason &&
                  props.btnText !== 'Schedule change')) && (
                <Flex>
                  <Button
                    type="viewAll"
                    text={statusArr.confirm.suggestion}
                    marginTop="20px"
                    onClick={e => {
                      e.preventDefault();

                      e.stopPropagation();
                      props.scheduleActions(e, 'option');
                    }}
                  />
                </Flex>
              )}
              {props.btnType === 'pending' &&
                props.btnText === 'Schedule change' && (
                  <Flex>
                    <Button
                      type="links"
                      text="Cancel"
                      marginTop="20px"
                      onClick={e => {
                        e.preventDefault();
                        e.stopPropagation();
                        props.scheduleActions(e, 'cancelSuggestion');
                      }}
                      marginRight="20px"
                    />
                    <Button
                      type="mobile"
                      text="Confirm"
                      marginTop="20px"
                      height={45}
                      onClick={e => {
                        e.preventDefault();
                        e.stopPropagation();
                        props.scheduleActions(e, 'confirmSuggestion');
                      }}
                    />
                  </Flex>
                )}
            </Flex>
            {props.btnType && (
              <ScheduleButton
                className={
                  props.btnText === 'Schedule change'
                    ? 'pending-suggestion'
                    : props.btnType
                }
                onClick={e => {
                  e.preventDefault();
                  e.stopPropagation();
                }}
              >
                <span
                  className="iconify confirm"
                  data-icon="cil:check-alt"
                  data-inline="false"
                />
                <span
                  className="iconify decline"
                  data-icon="ion:close-outline"
                  data-inline="false"
                />
                <span
                  className="iconify schedule"
                  data-icon={
                    props.btnText === 'Pending'
                      ? 'ant-design:clock-circle-outlined'
                      : 'ic:outline-calendar-today'
                  }
                  data-inline="false"
                />
                {props.btnText}
              </ScheduleButton>
            )}
          </Flex>
        </a>
      </Link>
    )}
    {props.cardtype === 'onlineProgram' && (
      <Flex
        className="wishList-flex"
        flexMargin={props.schoolCardMar || '0px'}
        onClick={e => {
          e.preventDefault();
          e.stopPropagation();
          props.onCardClick();
        }}
      >
        <PhotoCard
          isWishlisted={props.isWishlisted}
          isLoggedIn={props.isLoggedIn}
          image={props.schoolImage}
          height={props.schoolCardHeight}
          width={props.schoolCardWidth}
          margin="0px"
          // type={props.photocardType}
        />
        <Flex
          className="wishlistContent"
          column
          flexMargin="0px 0px 0px 20px"
          flexWidth="65%"
        >
          <Flex justifyBetween wrap>
            {props.category && <CategoryLable>{props.category}</CategoryLable>}
            {props.openingStatus && (
              <OpeningStatus>{props.openingStatus}</OpeningStatus>
            )}
          </Flex>
          <ProfileDetail>
            <div className="profile-name">online</div>
          </ProfileDetail>
          <SchoolName className="name">{props.schoolName}</SchoolName>
          <BranchRating
            branchname={props.schoolBranch}
            ratingNum={props.schoolRating}
            starImage={star}
          />
          {props.purchasedBatch && (
            <Flex column flexMargin="10px 0px 0px">
              <AdditionalInfo margin="0px 0px 5px" color="#666C78">
                <span> {props.purchasedBatch.heading_name}</span>
              </AdditionalInfo>
              {props.purchasedBatch.start_date &&
              props.purchasedBatch.end_date ? (
                <AdditionalInfo margin="0px 0px 5px" color="#60B947">
                  <span>
                    {moment(props.purchasedBatch.start_date).format(
                      'DD MMM YY',
                    )}
                  </span>{' '}
                  <span style={{ margin: '0 5px' }}>to</span>
                  <span>
                    {moment(props.purchasedBatch.end_date).format('DD MMM YY')}
                  </span>
                </AdditionalInfo>
              ) : (
                <AdditionalInfo margin="0px 0px 5px" color="#60B947">
                  <span>{props.purchasedBatch.till_date}</span>
                </AdditionalInfo>
              )}
              {/* <AdditionalInfo margin="0px 0px 5px">
                    Scheduled on: <span>{props.schedule}</span>
                  </AdditionalInfo>
                  <AdditionalInfo>
                    Contact: <span>{props.contact}</span>
                  </AdditionalInfo> */}
              {(props.purchasedBatch &&
                props.purchasedBatch.batch_slots &&
                props.purchasedBatch.batch_slots.length) ||
              (props.purchasedBatch &&
                props.purchasedBatch.batch_days &&
                props.purchasedBatch.batch_days.length) ? (
                <SessionInfo onClick={props.onShowSession}>
                  See Batches
                </SessionInfo>
              ) : null}
            </Flex>
          )}

          {(props.hasCancelReason || props.btnText === 'Schedule change') && (
            <Reason
              column
              flexMargin="24px 0px 0px"
              flexPadding="16px 22px"
              flexBorderRadius="5px"
              whitebg="#F2EFF5"
              onClick={e => {
                e.preventDefault();
                e.stopPropagation();
              }}
            >
              {props.btnType === 'decline' && <span>{props.reason}</span>}
              {props.btnType === 'pending' &&
                (props.hasCancelReason ||
                  props.btnText === 'Schedule change') && (
                  <>
                    <span>{`New schedule suggested from ${
                      props.schoolName
                    }`}</span>
                    <Flex column flexMargin="18px 0px 0px">
                      <h2>
                        Date:
                        <span>{props.scheduledate}</span>
                      </h2>
                      <h2>
                        Tour ID:
                        <span>{props.scheduleID}</span>
                      </h2>
                    </Flex>
                  </>
                )}
            </Reason>
          )}
          {props.btnSchedule === 'Schedule Tour' ? (
            <Flex>
              <Button
                type="subscribe"
                text="Schedule Tour"
                marginTop="20px"
                height="50px"
                isLoading={props.scheduleCheckLoader}
                onClick={e => {
                  e.preventDefault();
                  e.stopPropagation();
                  props.scheduleTour(e);
                }}
              />
            </Flex>
          ) : null}
          {(props.btnType === 'confirm' ||
            (props.btnType === 'pending' &&
              !props.hasCancelReason &&
              props.btnText !== 'Schedule change')) && (
            <Flex>
              <Button
                type="viewAll"
                text={statusArr.confirm.suggestion}
                marginTop="20px"
                onClick={e => {
                  e.preventDefault();

                  e.stopPropagation();
                  props.scheduleActions(e, 'option');
                }}
              />
            </Flex>
          )}
          {props.btnType === 'pending' &&
            props.btnText === 'Schedule change' && (
              <Flex>
                <Button
                  type="links"
                  text="Cancel"
                  marginTop="20px"
                  onClick={e => {
                    e.preventDefault();
                    e.stopPropagation();
                    props.scheduleActions(e, 'cancelSuggestion');
                  }}
                  marginRight="20px"
                />
                <Button
                  type="mobile"
                  text="Confirm"
                  marginTop="20px"
                  height={45}
                  onClick={e => {
                    e.preventDefault();
                    e.stopPropagation();
                    props.scheduleActions(e, 'confirmSuggestion');
                  }}
                />
              </Flex>
            )}
        </Flex>
        {props.btnType && (
          <ScheduleButton
            className={
              props.btnText === 'Schedule change'
                ? 'pending-suggestion'
                : props.btnType
            }
            onClick={e => {
              e.preventDefault();
              e.stopPropagation();
            }}
          >
            <span
              className="iconify confirm"
              data-icon="cil:check-alt"
              data-inline="false"
            />
            <span
              className="iconify decline"
              data-icon="ion:close-outline"
              data-inline="false"
            />
            <span
              className="iconify schedule"
              data-icon={
                props.btnText === 'Pending'
                  ? 'ant-design:clock-circle-outlined'
                  : 'ic:outline-calendar-today'
              }
              data-inline="false"
            />
            {props.btnText}
          </ScheduleButton>
        )}
      </Flex>
    )}
    {props.cardtype === 'schoolCardArticle' && (
      <Flex
        column
        flexWidth={props.schoolCardWidth}
        flexboxshadow="0 0 30px 0 rgba(0,0,0,0.05)"
      >
        <PhotoCard
          isWishlisted={props.isWishlisted}
          isLoggedIn={props.isLoggedIn}
          image={props.schoolImage}
          height={props.schoolCardHeight}
          width={props.schoolCardWidth}
          margin="0px"
          type={props.photocardType}
        />
        <Flex column flexPadding="20px 20px 50px 20px">
          <Flex justifyBetween>
            {props.category && <CategoryLable>{props.category}</CategoryLable>}
            {props.openingStatus && (
              <OpeningStatus>{props.openingStatus}</OpeningStatus>
            )}
          </Flex>
          <SchoolName
            style={{
              fontSize: '18px',
              lineHeight: '23px',
              marginTop: '20px',
            }}
          >
            {props.schoolName}
          </SchoolName>
          <HashTag>#health #chiledcare #article</HashTag>
          <SchoolDescColumn>{props.schoolDesc}</SchoolDescColumn>
        </Flex>
      </Flex>
    )}
    {(props.cardtype === 'primary' || props.cardtype === 'small') && (
      <Flex>
        <PhotoCard
          isWishlisted={props.isWishlisted}
          isLoggedIn={props.isLoggedIn}
          image={props.schoolImage}
          height={props.schoolCardHeight}
          width={props.schoolCardWidth}
          margin="0px"
          type={props.photocardType}
        />
        <Flex column flexMargin="0px 0px 0px 20px" className="cardContent">
          {props.category && (
            <Flex>
              <CategoryLable>{props.category}</CategoryLable>{' '}
              <Featured>Featured</Featured>
            </Flex>
          )}
          <SchoolName>{props.schoolName}</SchoolName>
          <BranchRating
            branchname={props.schoolBranch}
            ratingNum={props.schoolRating}
            starImage={star}
          />
          <SchoolDesc>{props.schoolDesc}</SchoolDesc>
        </Flex>
      </Flex>
    )}
    {props.cardtype === 'staff' && (
      <>
        <Flex>
          <PhotoCard
            isWishlisted={props.isWishlisted}
            isLoggedIn={props.isLoggedIn}
            image={props.schoolImage}
            height={props.schoolCardHeight}
            width={props.schoolCardWidth}
            margin="0px"
            type={props.photocardType}
            minWidth="100px"
          />
          <Flex column flexMargin="0px 0px 0px 20px">
            <StaffName>{props.staffName}</StaffName>
            {props.designation ? (
              <Experience>
                Designation:
                <span>{props.designation}</span>
              </Experience>
            ) : null}
            <Experience>
              Experience <span>{props.years}</span>
            </Experience>
          </Flex>
        </Flex>
      </>
    )}

    {props.cardtype === 'mostRead' && (
      <Link href={props.redirectionLink || ''}>
        <a href={props.redirectionLink}>
          <Flex>
            <PhotoCard
              image={props.schoolImage}
              height={props.schoolCardHeight}
              width={props.schoolCardWidth}
              margin="0px"
              type={props.photocardType}
              isBookmarked={props.isBookmarked}
              isLoggedIn={props.isLoggedIn}
              onBookmarkIconClick={props.onBookmarkIconClick}
            />
            <Flex column flexMargin="0px 0px 0px 20px" flexWidth="66%">
              <MostRead>{props.mostRead}</MostRead>
              <Flex flexMargin="8px 0px 0px 0px">
                <Likes>
                  <span
                    className="iconify"
                    data-icon="feather:calendar"
                    data-inline="false"
                  />
                  {props.dateNum}
                </Likes>
                <Likes>
                  <span
                    className="iconify"
                    data-icon="ion:eye-outline"
                    data-inline="false"
                  />
                  {props.viewNum}
                </Likes>
                <Likes>
                  <span
                    className="iconify"
                    data-icon="ant-design:like-outline"
                    data-inline="false"
                  />
                  {props.likesNum}
                </Likes>
              </Flex>
            </Flex>
          </Flex>
        </a>
      </Link>
    )}

    {props.cardtype === 'popularSchool' && (
      <Link href={props.redirectionLink}>
        <a href={props.redirectionLink}>
          <Flex flexWidth="100%" className="popularSchool-list">
            <PhotoCard
              isWishlisted={props.isWishlisted}
              isLoggedIn={props.isLoggedIn}
              image={props.schoolImage}
              height={props.schoolCardHeight}
              width={props.schoolCardWidth}
              margin="0px"
              type={props.photocardType}
            />
            <Flex column flexMargin="0px 0px 0px 20px" className="cardContent">
              {props.category && (
                <Flex>
                  <CategoryLable>{props.category}</CategoryLable>{' '}
                  <Featured>Featured</Featured>
                </Flex>
              )}
              <SchoolName>{props.schoolName}</SchoolName>
              <BranchRating
                branchname={props.schoolBranch}
                ratingNum={props.schoolRating}
                starImage={star}
              />
              <SchoolDesc>{props.schoolDesc}</SchoolDesc>
            </Flex>
          </Flex>
        </a>
      </Link>
    )}
  </SchoolWrapper>
);

SchoolCard.propTypes = {
  hasCancelReason: PropTypes.bool,
  scheduleCheckLoader: PropTypes.bool,
  isLoggedIn: PropTypes.bool,
  isWishlisted: PropTypes.bool,
  isBookmarked: PropTypes.bool,
  category: PropTypes.string,
  openingStatus: PropTypes.string,
  schoolImage: PropTypes.string,
  // schoolId: PropTypes.number,
  schoolName: PropTypes.string,
  schoolBranch: PropTypes.string,
  schoolRating: PropTypes.string,
  schoolDesc: PropTypes.string,
  photocardType: PropTypes.string,
  schoolCardWidth: PropTypes.string,
  schoolCardHeight: PropTypes.string,
  staffName: PropTypes.string,
  designation: PropTypes.string,
  years: PropTypes.string,
  schoolWrapperMarTop: PropTypes.string,
  onFocus: PropTypes.func,
  slug: PropTypes.string,
  schoolWrapperMarRight: PropTypes.string,
  schoolCardMar: PropTypes.string,
  dateNum: PropTypes.string,
  viewNum: PropTypes.string,
  reason: PropTypes.string,
  likesNum: PropTypes.string,
  cardtype: PropTypes.string,
  mostRead: PropTypes.string,
  tourID: PropTypes.string,
  schedule: PropTypes.string,
  contact: PropTypes.string,
  borderTop: PropTypes.string,
  schoolCardMinWidth: PropTypes.string,
  // btnCancel: PropTypes.string,
  // btnSchedule: PropTypes.string,
  // btnScheduleConfirm: PropTypes.string,
  btnType: PropTypes.string,
  btnText: PropTypes.string,
  // reason: PropTypes.string,
  redirectionLink: PropTypes.string,
  padding: PropTypes.string,
  scheduledate: PropTypes.string,
  scheduleID: PropTypes.string,
  btnSchedule: PropTypes.string,
  // onClick: PropTypes.func,
  scheduleTour: PropTypes.func,
  scheduleActions: PropTypes.func,
  onWishlistIconClick: PropTypes.func,
  onBookmarkIconClick: PropTypes.func,

  isLink: PropTypes.bool,
  hasBoxShadow: PropTypes.bool,
  onSchoolCardClick: PropTypes.func,
  scrollSensorCallback: PropTypes.func,
  onCardClick: PropTypes.func,
  purchasedBatch: PropTypes.object,
  onShowSession: PropTypes.func,
};
SchoolCard.defaultProps = {
  schoolWrapperMarRight: '0 !important',
  schoolWrapperMarTop: '70px',
  schoolCardWidth: '205px',
  schoolCardHeight: '128px',
  cardtype: 'primary',
};
export default SchoolCard;

const SchoolName = styled.h2`
  padding-top: 10px;
  padding-bottom: 10px;
  font-family: 'Quicksand', sans-serif;
  font-size: 14px;
  font-weight: bold;
  line-height: 25px;
  margin: 0px;

  @media (max-width: 1366px) {
    font-size: 16px;
    width: 25ch;
  }
  @media (max-width: 1024px) {
    font-size: 14px;
  }
  @media (max-width: 768px) {
    font-size: 18px;
    line-height: 1.2;
    margin: 7px 0;
    padding: 0px;
  }
  @media (max-width: 500px) {
    font-size: 15px;
    width: 30ch;
  }
`;
const ToolTipSchoolName = styled.div`
  display: none;
  transition: opacity 0.3s ease;
  top: 54px;
  left: 0px;
  position: absolute;
  padding: 12px;
  background-color: #fff;
  border: 1px solid #d6d6de;
  border-radius: 5px;
  font-size: 12px;
  font-weight: 400;
  &::after {
    content: '';
    position: absolute;
    left: 140px;
    bottom: 100%;
    width: 10px;
    height: 10px;
    transform: rotate(-45deg);
    top: -6px;
    left: 20px;
    background-color: #fff;
    border-bottom: 0px solid black;
    border-top: 1px solid #c0c8cd;
    border-right: 1px solid #c0c8cd;
    border-left: 0px solid transparent;
  }
`;

const ProfileDetail = styled.div`
  display: flex;
  align-items: center;
  /* margin-bottom: 12px; */
  .profile-name {
    padding: 4px;
    font-family: Quicksand;
    font-weight: 500;
    font-size: 12px;
    line-height: 13px;
    color: #666c78;
    margin-right: 10px;
    text-transform: uppercase;
    border: 1px solid #666c78;
    border-radius: 5px;
  }
  .profile-rating {
    display: flex;
    align-items: center;
    padding-left: 12px;
    border-left: 1px solid #000000;
    line-height: 1;
    font-family: Quicksand;
    font-weight: 500;
    font-size: 12px;
    color: #666c78;
    img {
      margin-right: 12px;
      width: 18px;
    }
  }
`;

const SchoolDesc = styled.div`
  width: 261px;
  margin-top: 10px;
  color: ${colors.lightGrey};
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 18px;
  display: -webkit-box;
  -webkit-line-clamp: 3;
  -webkit-box-orient: vertical;
  overflow: hidden;
  @media (max-width: 1334px) {
    // width: 235px;
    // height: 35px;
    overflow: hidden;
  }
  @media (max-width: 1152px) {
    width: 180px;
  }
  @media (max-width: 375px) {
    height: 30px;
    width: 150px;
    font-size: 12px;
    line-height: 14px;
  }
`;
const CategoryLable = styled.div`
  height: 20px;
  width: fit-content;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 5px;
  border: 1px solid ${colors.lighterGrey};
  border-radius: 3px;
  color: ${colors.lighterGrey};
  font-family: 'Roboto', sans-serif;
  font-size: 11px;
  line-height: 15px;
  box-sizing: border-box;
  text-transform: uppercase;

  width: auto;
  display: table;
  padding-top: 2px;
  padding-bottom: 2px;
  height: auto;
  white-space: pre;
`;
const SchoolWrapper = styled.div`
  // width: 50%;
  margin-bottom: ${props => props.schoolWrapperMarBottom};
  margin-top: ${props => props.schoolWrapperMarTop};
  margin-right: ${props => props.schoolWrapperMarRight};
  border-top: ${props => props.borderTop || '0px solid #DBDFE7'};
  padding: ${props => props.padding || '0px'};
  cursor: pointer;
  a{
    color: #000;
    text-decoration: none;
  }
  &.staff {
    margin-top: 20px;
    margin-bottom: 20px;
  }
  .wishList-flex
  {
    @media (max-width: 500px) {
      flex-direction:column;
      margin-top: 0;
      margin-bottom:20px;
      }
  }
  &.popularSchool
  {
    margin-right:20px;
    .cardContent
    {
      width:min-content;
    }
    @media (max-width: 1400px) {
    margin-right:10px;
    }
    @media (max-width: 1365px) {
    margin-right: 10px;
    max-width: 48%;
    width: 48%;
    ${Photo}
    {
      height: 120px;
    min-width: 160px;
    max-width: 160px;
    }
    .cardContent
    {
      max-width: min-content;
    }

    }

@media (max-width: 800px){
  .cardContent
    {
      margin: 0px 0px 0px 10px;
    }
}
@media (max-width: 500px)
{
  max-width: 100%;
  ${Photo}
  {
    max-width: 100% !important;
}

}
@media (max-width: 1365px)
{
  ${Photo}
  {
    height: 100px;
  min-width: 130px;
  max-width: 130px;
  }
}
    @media (max-width: 768px) {
      width: 100%;
      margin-right: 0px;
      margin-top:20px;
      &>div
      {
        flex-direction:column;
      }
      .cardContent
      {
        margin:20px 0px 20px 0px;
        width: 100%;
      }
      ${Photo}
      {
        width: 100%;
        height: auto;
        // height: 205px;
        min-width: 100%;
        max-width: 100%;
      }
      ${SchoolDesc}
      {
        width:100%;
      }
      .popularSchool-list
      {
          flex-direction:column !important;
      }
    }
  }
  &:hover {
    ${SchoolName} {
      color: ${colors.secondary};
    }
  }
  &.small {
    margin-top: 0px;
    ${SchoolName} {
      margin: 0px 0px 5px 0px;
      font-size: 18px;
    }
    ${SchoolDesc} {
      margin-top: 5px;
      font-size: 13px;
    }
  }
  &.mostRead {
    margin-right: 0px;
    margin-bottom: 30px;
    padding-bottom: 30px;
    border-bottom: 1px solid #dbdfe7;
    &:last-child {
      border-bottom: 0px solid;
    }
    @media (max-width: 1366px) {
      ${Photo} {
        height: 62px;
        width: 100px;
      }
    }
  }
&.wishList
{
  .links
  {
    height:46px;
    padding:0px 40px;
    border-color:#666C78;
  }
  .subscribe
  {
    @media (max-width: 500px) {
      height:46px;
      padding:0px 24px;
      font-size:14px;
    }

  }
}
  @media (max-width: 1366px) {
    margin-right: 10px;
    // ${Photo} {
    //   height: 113px;
    //   width: 152px;
    // }
  }
  @media (max-width: 1152px) {
    margin-right: 0;
  }
  @media (max-width: 1024px) {
    margin-right: 40px;
    // ${Photo} {
    //   height: 103px;
    //   width: 142px;
    // }
  }
  @media (max-width: 768px) {
    margin-right: 0;
    padding: 0;
    &:nth-child(2n) {
      margin-right: 0px;
    }
    // ${Photo} {
    //   height: auto;
    //   width: 100%;
    // }
  }
  @media (max-width: 320px) {
    ${Photo} {
      // height: 110px;
      // width: 120px;
     
    }
  }
  &.schoolCardColumn {
    ${SchoolName} {
      border-radius: 10px;
      position: relative;
      max-width: 90%;
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
      @media screen and (max-width: 1240px) {
        max-width: 100%;
      }
    }
    ${SchoolName}:hover {
      & + ${ToolTipSchoolName} {
        display:block;
        z-index:100;
      }
    }
    // .cardContent:hover 
    // {
    //   ${ToolTipSchoolName} {
    //     opacity: 1;
    //     transition: opacity 0.3s ease;
    //   }
    // }
    &:nth-child(2n) {
      @media (max-width: 500px) {
        margin-right: 0px;
      }
    }
    &:nth-child(3n) {
      @media (max-width: 1450px) {
        margin-right: 0px;
      }
      @media (max-width: 1100px) {
        margin-right: 25px;
      }

      @media (max-width: 500px) {
        // margin-right: 25px;
      }
    }
    &:last-child
    {
      @media (max-width: 1080px) {
        margin-right: 30px;
      }
      @media (max-width: 500px) {
        margin-right: 0px;
      }
    }
    // @media (max-width: 2050px) {
    //   margin-right: 30px;
    //   ${Photo} {
    //     height: 230px;
    //     width: 350px;
    //   }
    // }
    // @media (max-width: 1950px) {
    //   margin-right: 30px;
    //   ${Photo} {
    //     height: 220px;
    //     width: 340px;
    //   }
    // }
    // @media (max-width: 1850px) {
    //   margin-right: 30px;
    //   ${Photo} {
    //     height: 210px;
    //     width: 330px;
    //   }
    // }
    // @media (max-width: 1750px) {
    //   margin-right: 30px;
    //   ${Photo} {
    //     height: 200px;
    //     width: 320px;
    //   }
    // }
    // @media (max-width: 1650px) {
    //   margin-right: 30px;
    //   ${Photo} {
    //     height: 190px;
    //     width: 310px;
    //   }
    // }
    // @media (max-width: 1600px) {
    //   margin-right: 30px;
    //   ${Photo} {
    //     height: 180px;
    //     width: 290px;
    //   }
    // }
    // @media (max-width: 1550px) {
    //   margin-right: 30px;
    //   ${Photo} {
    //     height: 170px;
    //     width: 273px;
    //   }
    // }
    // @media (max-width: 1430px) {
    //   ${Photo} {
    //     width: 250px;
    //   }
    // }
    // @media (max-width: 1280px) {
    //   margin-right: 0px;
    //   ${Photo} {
    //     // height: 170px;
    //     width: 230px;
    //   }
    // }
    // @media (max-width: 1180px) {
    //  width:100% !important; 
    //   margin-right: 0px;
    //   ${Photo} {
    //     height: 150px;
    //     width: 200px;
    //   }
    // }
    // @media (max-width: 1080px) {
    //   width:42%;
    //    margin-right: 0px;
    //    ${Photo} {
    //     //  height: 170px;
    //     //  width: 220px;
    //     height: auto !important;
    //     width: 100% !important;
    //    }
    //  }
    // @media (max-width: 1024px) {
    //   margin-right: 25px;
    //   ${Photo} {
    //     // height: 170px;
    //     width: 240px;
    //   }
    // }
    @media (max-width: 768px) {
      max-width: 33%;
      ${SchoolName} {
        font-size: 14px;
      }
      ${CategoryLable} {
        font-size: 10px;
      }
    }
    @media (max-width: 500px) {
      max-width: 100%;
      width: 100%;
      margin-right: 0px;
      ${Photo} {
        width: 100%;
        height: auto;
        border-radius:10px;
      }
      ${SchoolName} {
        font-size: 20px;
      }
    }
  }
`;

const SchoolDescColumn = styled.div`
  margin-top: 10px;
  color: ${colors.lightGrey};
  font-family: 'Roboto', sans-serif;
  font-size: 13px;
  line-height: 18px;
  @media (max-width: 1366px) {
    ${SchoolDescColumn} {
      max-width: 240px;
    }
  }
  @media (max-width: 1280px) {
    ${SchoolDescColumn} {
      max-width: 230px;
    }
  }
  @media (max-width: 1152px) {
    ${SchoolDescColumn} {
      max-width: 210px;
    }
  }
  @media (max-width: 1024px) {
    ${SchoolDescColumn} {
      max-width: 180px;
    }
  }
  @media (max-width: 500px) {
    max-width: 100%;
  }
`;

const OpeningStatus = styled.div`
  color: #ec716c;
  font-family: 'Roboto', sans-serif;
  font-size: 12px;
  line-height: 14px;
`;
const HashTag = styled.div`
  color: #613a95;
  font-family: 'Roboto', sans-serif;
  font-size: 12px;
  line-height: 16px;
`;
export const Experience = styled.div`
  white-space: pre-line;
  color: #666c78;
  font-family: 'Roboto', sans-serif;
  font-size: 12px;
  margin: 5px 0px 0px;
  span {
    color: #30333b;
  }
`;
export const StaffName = styled.div`
  color: #30333b;
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 19px;
`;
export const Likes = styled.div`
  display: flex;
  align-items: end;
  color: #666c78;
  font-family: 'Roboto', sans-serif;
  font-size: 12px;
  line-height: 19px;
  margin-right: 20px;
  @media screen and (max-width: 1100px) and (min-width: 789px) {
    font-size: 10px;
    line-height: 15px;
    margin-right: 4px;
  }
  @media screen and (max-width: 790px) and (min-width: 766px) {
    margin-right: 3px;
  }
  @media screen and (max-width: 400px) {
    font-size: 10px;
  }
  .iconify {
    width: 18px;
    height: 18px;
    color: #666c78;
    margin-right: 4px;
    @media screen and (max-width: 1100px) {
      width: 16px;
      height: 16px;
    }
    @media screen and (max-width: 790px) and (min-width: 766px) {
      margin-right: 3px;
    }
    @media screen and (max-width: 400px) {
      margin-right: 4px;
    }
  }
`;
export const MostRead = styled.div`
  color: #30333b;
  font-family: 'Quicksand', sans-serif;
  font-size: 14px;
  font-weight: bold;
  line-height: 18px;
`;
export const AdditionalInfo = styled.div`
  color: ${props => props.color || '#666c78'};
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 22px;
  margin: ${props => props.margin || '0px'};
  span {
    color: ${props => props.color || colors.lightBlack}};
  }
`;
export const SessionInfo = styled.div`
  font-family: 'Roboto', sans-serif;
  color: #613a95;
  font-size: 14px;
  margin: 0;
  cursor: pointer;
`;

export const ScheduleButton = styled.div`
  position: absolute;
  right: 0px;
  top: 0px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 40px;
  width: auto;
  border-radius: 23px;
  background-color: #e2ffda;
  color: #60b947;
  font-family: Quicksand;
  font-size: 14px;
  font-weight: 600;
  letter-spacing: 0;
  line-height: 18px;
  padding: 0px 16px;
  border: 1px solid #e2ffda;
  @media (max-width: 500px) {
    top: -48px;
    height: 34px;
  }
  .iconify {
    width: 20px;
    height: 20px;
    color: #fff;
    display: none;
    margin-right: 6px;
  }
  &.confirm {
    .iconify.confirm {
      display: block;
      color: #60b947;
    }
  }
  &.decline {
    background-color: #ff6969;
    color: #fff;
    border: 1px solid #ff6969;

    .iconify.decline {
      display: block;
    }
  }
  &.pending {
    background-color: #ae9cd6;
    color: #fff;
    border: 1px solid #ae9cd6;

    .iconify.schedule {
      display: block;
    }
  }
  &.schedule {
    background-color: #ffce69;
    color: #fff;
    border: 1px solid #ffce69;

    .iconify.schedule {
      display: block;
    }
  }
  &.pending-suggestion {
    background-color: #ffce69;
    color: #fff;
    border: 1px solid #ffce69;

    .iconify.schedule {
      display: block;
    }
  }
`;

const Reason = styled(Flex)`
  & > span {
    font-family: Quicksand;
    font-size: 14px;
    font-weight: 500;
    letter-spacing: 0;
    line-height: 18px;
  }
  h2 {
    color: #30333b;
    font-family: Roboto;
    font-size: 16px;
    letter-spacing: 0;
    line-height: 26px;
    font-weight: 400;
    margin: 0px;
    span {
      font-weight: bold;
      margin-left: 4px;
    }
  }
`;
const Featured = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 20px;
  width: 68px;
  border-radius: 5px;
  background-color: ${colors.secondary};
  color: ${colors.white};
  font-family: 'Roboto', sans-serif;
  font-size: 11px;
  font-weight: 500;
  line-height: 15px;
  text-transform: uppercase;
  box-shadow: 3px 3px 8px 0 rgba(0, 0, 0, 0.2);
  margin-left: 10px;
  @media (max-width: 414px) {
    top: 10px;
    left: 10px;
  }
`;
