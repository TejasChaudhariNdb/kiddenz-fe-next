/* eslint-disable no-nested-ternary */
/* eslint-disable indent */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable no-lonely-if */
/* eslint-disable eqeqeq */
/* eslint-disable import/no-unresolved */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import InputRange from 'react-input-range';
import colors from 'utils/colors';
import Flex from '../common/flex';
import Button from '../Button';
import CheckBox from '../Checkbox';
import RadioButton from '../RadioButton';
// import Button from '../Button';
import Badge from '../Badge';
import Ruppee from '../common/Ruppee';
// import Badge, { BadgeWrapper } from '../Badge';

const Filter = ({
  type,
  onApplyFilter,
  filtersOnChange,
  appliedFilters = {},
  setActive,
  setModalType,
  onSliderChange,
  queryChange,
  resetFilter,
  searchMethod,
  blockSearch,
  filterSectionActive,
  setFilterSectionActive,
}) => {
  const [ageSliderValues, setAgeSliderValues] = useState(0);
  const [amountSliderValues, setAmountSliderValues] = useState({
    min: 500,
    max: 300000,
  });

  const numberFormatter = val => {
    let num;
    if (val >= 10000000) {
      num = `${(val / 10000000).toFixed(2)} Cr`;
    } else if (val >= 100000) {
      num = `${(val / 100000).toFixed(2)} L`;
    } else if (val >= 1000) {
      num = `${(val / 1000).toFixed(2)} K`;
    } else {
      num = val;
    }
    return num;
  };

  useEffect(
    () => {
      setAgeSliderValues(
        appliedFilters.max_age
          ? appliedFilters.max_age === 6
            ? 0
            : appliedFilters.max_age / 12
          : 0,
      );
      setAmountSliderValues({
        min: appliedFilters.min_price ? appliedFilters.min_price : 500,
        max: appliedFilters.max_price ? appliedFilters.max_price : 300000,
      });
    },
    [appliedFilters],
  );

  return (
    <FilterWrapper className={`mobile ${type}`}>
      <FilterBtnWrapper justifyEnd>
        <Button
          type="links"
          text={
            <span
              className="iconify"
              data-icon="feather:filter"
              data-inline="false"
            />
          }
        />
      </FilterBtnWrapper>
      <FilterContent
        flexPadding="15px 30px"
        whitebg="#fff"
        // flexHeight="121px"
        alignCenter
        wrap
        className={filterSectionActive ? 'mobileActive' : ''}
      >
        <FilterHeader
          onClick={() => setFilterSectionActive(!filterSectionActive)}
        >
          <h3>Filter Section</h3>
          <div className={filterSectionActive ? 'hide-icon' : ''}>
            <span
              className="iconify"
              data-icon="eva:arrow-ios-downward-outline"
              data-inline="false"
            />
          </div>
          <div className={!filterSectionActive ? 'hide-icon' : ''}>
            <span
              className="iconify"
              data-icon="eva:arrow-ios-upward-outline"
              data-inline="false"
            />
          </div>
        </FilterHeader>
        <FilterItem>
          <FliterTitle margin="0px 0px 12px">Type of Organisation</FliterTitle>
          <Flex alignCenter>
            <RadioButton
              id="Pre-school"
              text="Pre-school"
              filterRadio="#4B5256"
              filterFont="13px"
              filterFontWeight="400"
              filterLineheight="21px"
              filterPadding="0px 0px 0px 30px"
              RadioMargin="0px 25px 0px 0px"
              onClick={() => filtersOnChange('organisation', 1)}
              checked={appliedFilters.organisation == 1}
            />
            <RadioButton
              id="Day-Care"
              text="Day Care"
              filterRadio="#4B5256"
              filterFont="13px"
              filterFontWeight="400"
              filterLineheight="21px"
              filterPadding="0px 0px 0px 30px"
              RadioMargin="0px 25px 0px 0px"
              onClick={() => filtersOnChange('organisation', 2)}
              checked={appliedFilters.organisation == 2}
            />
            <RadioButton
              id="Both-Org"
              text="Both"
              filterRadio="#4B5256"
              filterFont="13px"
              filterFontWeight="400"
              filterLineheight="21px"
              filterPadding="0px 0px 0px 30px"
              onClick={() => filtersOnChange('organisation', 3)}
              checked={appliedFilters.organisation == 3}
            />
          </Flex>
        </FilterItem>
        <FilterItem className="rangeSlider">
          <Flex justifyBetween flexMargin="0px 0px 12px" alignCenter>
            <FliterTitle>Age</FliterTitle>
            {appliedFilters.max_age !== null && (
              <FliterData>
                {ageSliderValues === 0 ? '6M' : `${ageSliderValues}Y`}
              </FliterData>
            )}
          </Flex>
          <InputRange
            maxValue={10}
            minValue={0}
            value={ageSliderValues}
            formatLabel={() => null}
            onChange={val => setAgeSliderValues(val)}
            onChangeComplete={val =>
              onSliderChange({ max_age: val === 0 ? 6 : val * 12 })
            }
          />
        </FilterItem>
        <FilterItem className="rangeSlider">
          <Flex justifyBetween alignCenter flexMargin="0px 0px 12px">
            <FliterTitle>
              Price/
              {appliedFilters.organisation === 2 ? 'Month' : 'Annum'}
            </FliterTitle>
            <FliterData>
              <Ruppee>₹</Ruppee>
              {numberFormatter(amountSliderValues.min)} - <Ruppee>₹</Ruppee>
              {numberFormatter(amountSliderValues.max)}
            </FliterData>
          </Flex>
          <InputRange
            maxValue={300000}
            minValue={500}
            value={amountSliderValues}
            formatLabel={() => null}
            onChange={val => {
              setAmountSliderValues(val);
              if (appliedFilters.organisation == null)
                filtersOnChange('organisation', 1);
            }}
            onChangeComplete={val =>
              onSliderChange({ min_price: val.min, max_price: val.max })
            }
            allowSameValues={false}
          />
        </FilterItem>
        <FilterItem>
          <FliterTitle margin="0px 0px 12px">
            Select curriculum type
          </FliterTitle>
          <Flex alignCenter>
            <RadioButton
              id="course_montessori"
              text="Montessori"
              filterRadio="#4B5256"
              filterFont="13px"
              filterFontWeight="400"
              filterLineheight="21px"
              filterPadding="0px 0px 0px 30px"
              RadioMargin="0px 25px 0px 0px"
              onClick={() => filtersOnChange('course_type', '1')}
              checked={appliedFilters.course_type == 1}
            />
            <RadioButton
              id="course_any"
              text="Any"
              filterRadio="#4B5256"
              filterFont="13px"
              filterFontWeight="400"
              filterLineheight="21px"
              filterPadding="0px 0px 0px 30px"
              onClick={() => filtersOnChange('course_type', '0')}
              checked={appliedFilters.course_type == 0}
            />
          </Flex>
        </FilterItem>
        <FilterItem>
          <FliterTitle margin="0px 0px 12px">Transportation</FliterTitle>
          <Flex alignCenter>
            <RadioButton
              id="trans_yes"
              text="Yes"
              filterRadio="#4B5256"
              filterFont="13px"
              filterFontWeight="400"
              filterLineheight="21px"
              filterPadding="0px 0px 0px 30px"
              RadioMargin="0px 25px 0px 0px"
              onClick={() => filtersOnChange('transportation', '1')}
              checked={appliedFilters.transportation == 1}
            />
            <RadioButton
              id="trans_no"
              text="No"
              filterRadio="#4B5256"
              filterFont="13px"
              filterFontWeight="400"
              filterLineheight="21px"
              filterPadding="0px 0px 0px 30px"
              onClick={() => filtersOnChange('transportation', '0')}
              checked={appliedFilters.transportation == 0}
            />
          </Flex>
        </FilterItem>
        <FilterItem className="mobileDevice">
          <FliterTitle margin="0px 0px 12px">Fresh Cooked Food</FliterTitle>
          <Flex alignCenter>
            <RadioButton
              id="food_yes"
              text="Yes"
              filterRadio="#4B5256"
              filterFont="13px"
              filterFontWeight="400"
              filterLineheight="21px"
              filterPadding="0px 0px 0px 30px"
              RadioMargin="0px 25px 0px 0px"
              onClick={() => filtersOnChange('food', '1')}
              checked={appliedFilters.food == 1}
            />
            <RadioButton
              id="food_no"
              text="No"
              filterRadio="#4B5256"
              filterFont="13px"
              filterFontWeight="400"
              filterLineheight="21px"
              filterPadding="0px 0px 0px 30px"
              onClick={() => filtersOnChange('food', '0')}
              checked={appliedFilters.food == 0}
            />
          </Flex>
        </FilterItem>
        <FilterItem className="mobileonly">
          {/* {!checkIfFiltersChnaged(appliedFilters) ? (
            <div onClick={resetFilter} className="clearFilter">
              Clear Filters
            </div>
          ) : null} */}
          {type === 'primary' && (
            <Flex justifyCenter>
              <Button
                text="Apply Filters"
                type="filter"
                onClick={e => {
                  e.preventDefault();
                  e.stopPropagation();
                  setFilterSectionActive(false);
                  onApplyFilter(e, appliedFilters, searchMethod);
                  if (!blockSearch) {
                    queryChange(e, appliedFilters);
                  }
                }}
                buttonAcessoryClick={e => {
                  e.preventDefault();
                  e.stopPropagation();
                  resetFilter();
                }}
                displayAcessory={!checkIfFiltersChnaged(appliedFilters)}
              />
            </Flex>
          )}
          <AdditionalFilter
            onClick={() => {
              setActive(true);
              setModalType('default');
            }}
          >
            Additional Filters
          </AdditionalFilter>
        </FilterItem>

        <FilterItem className="smallerDevices">
          <Flex
            whitebg="#F3F5F9"
            flexPadding="20px"
            flexBorderRadius="10px"
            flexMargin="0px 0px 25px 0px"
            flexWidth="100%"
          >
            <Flex column flexMargin="0px 60px 0px 0px">
              <TypeOrg>Type of Organisation</TypeOrg>
              <OrgName>Weekend childcare</OrgName>
            </Flex>
            <RadioButton
              id="yes"
              text="yes"
              filterRadio="#4B5256"
              filterFont="13px"
              filterFontWeight="400"
              filterLineheight="21px"
              filterPadding="0px 0px 0px 30px"
              RadioMargin="0px 65px 0px 0px"
            />
            <RadioButton
              id="no"
              text="No"
              filterRadio="#4B5256"
              filterFont="13px"
              filterFontWeight="400"
              filterLineheight="21px"
              filterPadding="0px 0px 0px 30px"
            />
          </Flex>
          <Flex flexMargin="0px 0px 30px 0px" alignCenter wrap>
            <Title>Type of Organisation</Title>
            <CheckBox
              label="Part Time"
              id="parttime"
              margin="0px 30px 0px 0px"
              filterFont="13px"
              filterChekbox="5px"
              filterLineheight="18px"
            />
            <CheckBox
              label="Full Time"
              id="fulltime"
              margin="0px 30px 0px 0px"
              filterFont="13px"
              filterChekbox="5px"
              filterLineheight="18px"
            />
            <CheckBox
              label="Flexible"
              id="flexible"
              margin="0px"
              filterFont="13px"
              filterChekbox="5px"
              filterLineheight="18px"
            />
          </Flex>
          <Flex flexMargin="0px 0px 30px 0px" alignCenter wrap>
            <Title>Type of Food</Title>
            <CheckBox
              label="Veg"
              id="parttime"
              margin="0px 65px 0px 0px"
              filterFont="13px"
              filterChekbox="5px"
              filterLineheight="18px"
            />
            <CheckBox
              label="Non-veg"
              id="fulltime"
              margin="0px 30px 0px 0px"
              filterFont="13px"
              filterChekbox="5px"
              filterLineheight="18px"
            />
          </Flex>
          <Flex flexMargin="0px 0px 30px 0px" alignCenter wrap>
            <Title>Provides Snack</Title>
            <RadioButton
              id="snack1"
              text="Yes"
              filterRadio="#4B5256"
              filterFont="13px"
              filterFontWeight="400"
              filterLineheight="21px"
              filterPadding="0px 0px 0px 30px"
              RadioMargin="0px 60px 0px 0px"
            />
            <RadioButton
              id="snack1"
              text="No"
              filterRadio="#4B5256"
              filterFont="13px"
              filterFontWeight="400"
              filterLineheight="21px"
              filterPadding="0px 0px 0px 30px"
              RadioMargin="0px"
            />
          </Flex>
          <Flex flexMargin="0px 0px 30px 0px" alignCenter wrap>
            <Title>CCTV</Title>
            <RadioButton
              id="CCTV1"
              text="Yes"
              filterRadio="#4B5256"
              filterFont="13px"
              filterFontWeight="400"
              filterLineheight="21px"
              filterPadding="0px 0px 0px 30px"
              RadioMargin="0px 60px 0px 0px"
            />
            <RadioButton
              id="CCTV2"
              text="No"
              filterRadio="#4B5256"
              filterFont="13px"
              filterFontWeight="400"
              filterLineheight="21px"
              filterPadding="0px 0px 0px 30px"
              RadioMargin="0px"
            />
          </Flex>
          <Flex flexMargin="0px 0px 30px 0px" alignCenter wrap>
            <Title>Potty Training</Title>
            <RadioButton
              id="potty1"
              text="Yes"
              filterRadio="#4B5256"
              filterFont="13px"
              filterFontWeight="400"
              filterLineheight="21px"
              filterPadding="0px 0px 0px 30px"
              RadioMargin="0px 60px 0px 0px"
            />
            <RadioButton
              id="potty2"
              text="No"
              filterRadio="#4B5256"
              filterFont="13px"
              filterFontWeight="400"
              filterLineheight="21px"
              filterPadding="0px 0px 0px 30px"
              RadioMargin="0px"
            />
          </Flex>
          <Flex flexMargin="0px 0px 30px 0px" alignCenter wrap>
            <Title>Extended Hours</Title>
            <RadioButton
              id="hours1"
              text="Yes"
              filterRadio="#4B5256"
              filterFont="13px"
              filterFontWeight="400"
              filterLineheight="21px"
              filterPadding="0px 0px 0px 30px"
              RadioMargin="0px 60px 0px 0px"
            />
            <RadioButton
              id="hours2"
              text="No"
              filterRadio="#4B5256"
              filterFont="13px"
              filterFontWeight="400"
              filterLineheight="21px"
              filterPadding="0px 0px 0px 30px"
              RadioMargin="0px"
            />
          </Flex>
          <Flex flexMargin="0px 0px 30px 0px" alignCenter wrap>
            <Title>Special Care</Title>
            <RadioButton
              id="special1"
              text="Yes"
              filterRadio="#4B5256"
              filterFont="13px"
              filterFontWeight="400"
              filterLineheight="21px"
              filterPadding="0px 0px 0px 30px"
              RadioMargin="0px 60px 0px 0px"
            />
            <RadioButton
              id="special2"
              text="No"
              filterRadio="#4B5256"
              filterFont="13px"
              filterFontWeight="400"
              filterLineheight="21px"
              filterPadding="0px 0px 0px 30px"
              RadioMargin="0px"
            />
          </Flex>
          <Flex flexMargin="0px 0px 30px 0px" alignCenter wrap>
            <Title>Activities</Title>
            <RadioButton
              id="Activities1"
              text="Yes"
              filterRadio="#4B5256"
              filterFont="13px"
              filterFontWeight="400"
              filterLineheight="21px"
              filterPadding="0px 0px 0px 30px"
              RadioMargin="0px 60px 0px 0px"
            />
            <RadioButton
              id="Activities2"
              text="No"
              filterRadio="#4B5256"
              filterFont="13px"
              filterFontWeight="400"
              filterLineheight="21px"
              filterPadding="0px 0px 0px 30px"
              RadioMargin="0px"
            />
          </Flex>
          <Flex flexMargin="0px 0px 30px 0px" alignCenter wrap>
            <Title>Tuition</Title>
            <RadioButton
              id="Tution1"
              text="Yes"
              filterRadio="#4B5256"
              filterFont="13px"
              filterFontWeight="400"
              filterLineheight="21px"
              filterPadding="0px 0px 0px 30px"
              RadioMargin="0px 60px 0px 0px"
            />
            <RadioButton
              id="Tutio2"
              text="No"
              filterRadio="#4B5256"
              filterFont="13px"
              filterFontWeight="400"
              filterLineheight="21px"
              filterPadding="0px 0px 0px 30px"
              RadioMargin="0px"
            />
          </Flex>
          <Flex flexMargin="0px 0px 30px 0px" alignCenter wrap>
            <Title>Tuition</Title>
            <Badge badgeActive text="Mon" marginBottom="10px" />
            <Badge badgeActive text="Tue" marginBottom="10px" />
            <Badge badgeActive text="Wed" marginBottom="10px" />
            <Badge badgeActive text="Thu" marginBottom="10px" />
            <Badge badgeActive text="Fri" marginBottom="10px" />
            <Badge text="Sat" marginBottom="10px" />
            <Badge text="Sun" marginBottom="10px" />
          </Flex>
          <Flex flexMargin="0px 0px 30px 0px" alignCenter wrap>
            <Title>Tuition</Title>
            <Badge
              badgeActive
              text="Montessari"
              marginBottom="10px"
              tooltipText="This comprehensive program developed by physician and educator 
Maria Montessori takes a developmental approach to learning. 
All teachers must have an early childhood undergraduate or graduate 
degree and Montessori certification. The Montessori approach 
emphasizes nature, creativity, and hands-on learning with gentle 
guidance provided by the teachers. The goal of the Montessori method 
is to develop a child's senses, character, practical life skills, 
and academic ability."
            />
            <Badge text="Academic" marginBottom="10px" />
            <Badge text="Play-based" marginBottom="10px" />
            <Badge text="Waldorf" marginBottom="10px" />
            <Badge text="Reggio Emilia" />
            <Badge text="Bank Street" />
            <Badge text="HighScope" />
          </Flex>
          <Flex flexMargin="0px" justifyEnd>
            <Button
              text="Reset Filter"
              marginRight="10px"
              type="subscribe"
              headerButton
            />
            <Button
              text="Apply Filter"
              type="secondary"
              headerButton
              onClick={onApplyFilter}
            />
          </Flex>
        </FilterItem>
      </FilterContent>
    </FilterWrapper>
  );
};

Filter.propTypes = {
  searchMethod: PropTypes.string,
  type: PropTypes.string,
  setModalType: PropTypes.func,
  setActive: PropTypes.func,
  onApplyFilter: PropTypes.func,
  onSliderChange: PropTypes.func,
  filtersOnChange: PropTypes.func,
  resetFilter: PropTypes.func,
  appliedFilters: PropTypes.object,
  queryChange: PropTypes.func,
  blockSearch: PropTypes.bool,
  filterSectionActive: PropTypes.bool,
  setFilterSectionActive: PropTypes.func,
};

Filter.defaultProps = {
  type: 'primary',
};
export default Filter;

const checkIfFiltersChnaged = obj =>
  !Object.entries(obj)
    .map(([key, value]) => {
      if (key === 'schedule_type') {
        if ((value || []).length > 0) return false;
        return true;
        // eslint-disable-next-line no-else-return
      } else {
        if (value === null) return true;
        return false;
      }
    })
    .includes(false);

const AdditionalFilter = styled.div`
  margin-top: 10px;
  color: ${colors.secondary};
  font-size: 14px;
  font-weight: 500;
  line-height: 18px;
  text-align: center;
  cursor: pointer;
  @
`;
const FilterWrapper = styled.section`
  position: fixed;
  top: 0px;
  left: 0px;
  right: 0px;
  padding-top: 64px;
  height: auto;
  width: 100%;
  margin: 0px auto;
  max-width: 2560px;
  z-index: 999;
  background-color: #fff;
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.05);
  @media screen and (max-width: 768px) {
    z-index: 0;
  }
  &.secondary {
    ${AdditionalFilter} {
      text-align: left;
    }
  }

  @media (max-width: 768px) {
    display: block;
    position: relative;
  }
  @media (max-width: 500px) {
    order: 1;
    z-index: 1;
    box-shadow: none;
  }
  .hide-icon {
    display: none;
  }
`;
const FliterTitle = styled.h3`
  margin: ${props => props.margin || '0px'};
  color: ${colors.filterText};
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 19px;
  font-weight: 400;
  @media (max-width: 768px) {
    min-width: 197px;
    margin-bottom: 0px;
  }
  @media (max-width: 500px) {
    font-size: 14px;
    min-width: 100px;
    width: 100%;
    margin-bottom: 20px;
  }
`;
const FliterData = styled.div`
  min-width: 78px;
  width: 78px;
  color: #4b5256;
  font-size: 11px;
  line-height: 13px;
  text-align: right;
  @media (max-width: 500px) {
    margin-top: -20px;
    min-width: fit-content;
    width: fit-content;
  }
`;
const FilterItem = styled.div`
  // flex-grow: 1;
  width: fit-content;
  padding-right: 18px;
  padding-left: 18px;
  border-right: 1px solid ${colors.filterBorder};
  white-space: nowrap;
  @media (max-width: 2000px) and (min-width: 1450px) {
    padding-right: 2%;
  }
  @media (max-width: 1400px) {
    padding-right: 12px;
    padding-left: 12px;
  }
  @media (max-width: 1180px) {
    padding: 0px 30px;
    margin-bottom: 20px;
  }
  @media (max-width: 500px) {
    align-items: flex-start !important;
    flex-direction: column;
    margin-top: 10px;
  }
  .clearFilter {
    color: #30333b;
    font-family: Roboto;
    font-size: 13px;
    letter-spacing: 0;
    line-height: 15px;
    margin-bottom: 10px;
    cursor: pointer;
    &:hover {
      color: #000;
    }
  }
  .filter {
    @media (max-width: 500px) {
      margin-right: 16px;
    }
  }

  &:first-child {
    padding-left: 0px;
    @media (max-width: 1180px) {
      padding-left: 30px;
    }
    @media (max-width: 800px) {
      padding-left: 0px;
    }
    @media (max-width: 500px) {
      padding-left: 0px;
    }
  }
  &:nth-last-child(2) {
    display: flex;
    flex-direction: column;
    align-items: flex-end;
    border-width: 0px;
    padding-right: 0px;
    @media (max-width: 1180px) {
      flex-grow: initial;
    }
  }
  .input-range__slider {
    background: ${colors.secondary};
    border: 5px solid #fff;
    box-shadow: 0px 0px 2px #000;
  }
  .input-range__track--background {
    margin-top: -0.05rem;
    height: 6px;
  }
  .input-range__label--min,
  .input-range__label--max {
    .input-range__label-container {
      display: none;
    }
  }
  &.smallerDevices {
    display: none;
    @media (max-width: 768px) {
      // display: flex;
      flex-direction: column;
      align-items: flex-start;
    }
  }
  &.rangeSlider {
    flex-grow: 1;
    min-width: 11%;
    @media (max-width: 2000px) and (min-width: 1440px) {
      max-width: 14%;
    }
    @media (max-width: 500px) {
      width: 43%;
      margin-right: 7%;
    }
    &:last-child {
      margin-right: 0%;
    }
  }
  @media (max-width: 768px) {
    width: 100%;
    display: flex;
    align-items: center;
    margin-top: 20px;
    padding-right: 0px;
    padding-left: 0px;
    border-width: 0px;
  }
  @media (max-width: 500px) {
    flex-wrap: wrap;
  }
  &.mobileonly {
    @media (max-width: 1320px) {
      justify-content: flex-end;
      flex-direction: row;
      align-items: center;
      padding: 20px 0px 0px 20px;
      ${AdditionalFilter} {
        margin-left: 10px;
      }
    }
    @media (max-width: 500px) {
      justify-content: flex-start;
      padding: 0px;
    }
  }
`;
const FilterBtnWrapper = styled(Flex)`
  display: none;
  padding: 10px 20px;
  background: ${colors.white};
  @media (max-width: 768px) {
    display: flex;
  }
  @media (max-width: 500px) {
    display: none;
  }
`;
const FilterContent = styled(Flex)`
  // max-width: 1440px;
  margin: 0px auto;
  @media (max-width: 2000px) and (min-width: 1440px) {
    justify-content: space-between;
  }
  @media (max-width: 1180px) {
    padding: 15px 30px 15px;
    height: auto;
  }

  @media (max-width: 768px) {
    // height: 100%;
    // height: 90vh;
    overflow: auto;
    width: 100%;
    margin-left: auto;
  }
  @media (max-width: 500px) {
    width: 100%;
    padding: 0px 20px;
    margin-left: 0px;
    max-height: 52px;
    height: 52px;
    overflow: hidden;
    transition: max-height 0.5s ease-in;
  }
  &.mobileActive {
    @media (max-width: 500px) {
      max-height: 700px;
      height: auto;
      transition: max-height 0.5s ease-in;
    }
  }
`;
const TypeOrg = styled.div`
  font-family: 'Roboto', sans-serif;
  color: #7e888e;
  font-family: Roboto;
  font-size: 12px;
  line-height: 16px;
`;
const OrgName = styled.div`
  font-family: 'Roboto', sans-serif;
  color: #30333b;
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 19px;
`;
const Title = styled.div`
  width: 200px;
  color: #7e888e;
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 19px;
  white-space: nowrap;

  @media (max-width: 500px) {
    width: 100%;
    margin-bottom: 20px;
    font-size: 16px;
  }
`;

const FilterHeader = styled.div`
  width: 100%;
  display: none;
  align-items: center;
  justify-content: space-between;
  padding: 10px;
  border: 1px solid #dbdee5;
  border-radius: 5px;
  cursor: pointer;
  @media (max-width: 500px) {
    display: flex;

    h3 {
      color: #30333b;
      font-family: Quicksand;
      font-size: 14px;
      font-weight: 500;
      letter-spacing: 0;
      line-height: 25px;
      margin: 0px;
    }
    .iconify {
      width: 18px;
      height: 18px;
    }
  }
`;
