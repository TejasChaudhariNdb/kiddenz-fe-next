/* eslint-disable indent */
import React from 'react';
import Router from 'next/router';
import Button from 'components/Button';
import kiddenLogo from '../../images/kiddenz-purple logo.svg';
import { PaymentSuccessWrapper, Logo } from './styled';

const PaymentSuccess = () => (
  <PaymentSuccessWrapper>
    <Logo
      onClick={() =>
        Router.push({
          pathname: '/',
        }).then(() => window.scrollTo(0, 0))
      }
    >
      <img src={kiddenLogo} alt="Kiddenz logo" />
    </Logo>
    <div className="success">
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="16"
        height="16"
        fill="currentColor"
        className="bi bi-check-circle hide"
        viewBox="0 0 16 16"
      >
        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
        <path d="M10.97 4.97a.235.235 0 0 0-.02.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-1.071-1.05z" />
      </svg>
      {/* Remove class hide to show/ add class hide to hide the element */}
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="16"
        height="16"
        fill="currentColor"
        className="bi bi-x-circle   red"
        viewBox="0 0 16 16"
      >
        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
        <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
      </svg>
      <h1 className="hide">Congrats !</h1>
      {/* Remove class hide to show/ add class hide to hide the element */}
      <h1>Transaction Failed !</h1>
      <p className="hide">Your Transaction has been completed with kiddenz</p>
      <p>Your Transaction has been Failed with kiddenz</p>
      {/* remove class red for success view */}
      <div className="details reddetails">
        <div className="left">
          <p>Transaction id: 1a3ght34f5</p>
          <p>Mode of payment: Debit card </p>
        </div>
        <div className="right">
          <p>Amount paid: 12,000</p>
          <p>Time: March 6, 2021 9:13 AM</p>
        </div>
      </div>
      <Button text="Go to Homepage" />
    </div>
  </PaymentSuccessWrapper>
);

export default PaymentSuccess;
