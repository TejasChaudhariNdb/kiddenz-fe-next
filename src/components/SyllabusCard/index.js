import React from 'react';
import PropTypes from 'prop-types';
import { SyllabusCard } from './styled';
const Syllabus = ({ text, width }) => (
  <SyllabusCard width={width}>{text}</SyllabusCard>
);
Syllabus.propTypes = {
  text: PropTypes.string,
  width: PropTypes.string,
};
export default Syllabus;
