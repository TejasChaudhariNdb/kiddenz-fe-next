import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import colors from 'utils/colors';

const BadgeWrapper = styled.div`
  position: relative;
  height: 30px;
  min-width: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-right: 10px;
  margin-left: ${props => props.marginLeft};
  margin-bottom: ${props => props.marginBottom};
  padding: 0px 10px;
  border: ${props =>
    props.badgeActive ? '1px solid #613A95' : '1px solid #c0c8cd'};
  border-radius: 5px;
  color: ${props => (props.badgeActive ? '#613A95' : '#4B5256')};
  background-color: ${colors.white};
  font-family: 'Roboto', sans-serif;
  font-size: 13px;
  line-height: 18px;
  cursor: pointer;
  text-transform: Capitalize;
  @media (max-width: 768px) {
    margin-right: 5px;
  }
`;
const Tooltip = styled.div`
  position: absolute;
  left: -30px;
  bottom: 110%;
  width: 432px;
  height: auto;
  padding: 20px;
  border-radius: 10px;
  background-color: #ffffff;
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
  z-index: 0;
  display: none;

  ${BadgeWrapper}:hover & {
    display: block;
  }
  &:after {
    content: '';
    display: block;
    position: absolute;
    bottom: -15px;
    left: 60px;
    border-left: 8px solid transparent;
    border-right: 8px solid transparent;
    border-bottom: 8px solid transparent;
    border-top: 8px solid ${colors.white};
    z-index: 10;
  }
`;
const TooltipContent = styled.div`
  color: ${colors.inputPlaceholder};
  font-family: 'Roboto', sans-serif;
  font-size: 12px;
  line-height: 19px;
`;
const Badge = props => (
  <BadgeWrapper
    badgeActive={props.badgeActive}
    marginBottom={props.marginBottom}
    marginLeft={props.marginLeft}
    onClick={props.onClick}
  >
    {props.text}

    {props.tooltipText && (
      <Tooltip>
        <TooltipContent>{props.tooltipText}</TooltipContent>
      </Tooltip>
    )}
  </BadgeWrapper>
);
Badge.propTypes = {
  text: PropTypes.string,
  badgeActive: PropTypes.bool,
  marginBottom: PropTypes.string,
  marginLeft: PropTypes.string,
  tooltipText: PropTypes.string,
  onClick: PropTypes.func,
};
export default Badge;
