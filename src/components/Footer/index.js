/* eslint react/prop-types: 0 */
import React, { useEffect, useMemo, useState } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Router from 'next/router';
import Link from 'next/link';
import Geocode from 'react-geocode';
// import { FormattedMessage } from 'react-intl';

// import A from 'components/A';
// import LocaleToggle from 'containers/LocaleToggle';
// import Wrapper from './Wrapper';
// import messages from './messages';
import colors from 'utils/colors';
import Flex from '../common/flex';
import Button from '../Button';
import footerBg from '../../images/footer bg (1).png';
// import { NoEmitOnErrorsPlugin } from 'webpack';
function Footer(props) {
  const {
    getData,
    ARTICLES_GET_ALL_CATEGORIES_API_CALL,
    dashboard: { ARTICLES_GET_ALL_CATEGORIES_API } = {},
    authentication: { currentLocation = {} },
    dispatch,
    router: { pathname } = {},
  } = props;

  const { name = '', lat = null, lng = null } = currentLocation;

  const [latitude, setLatitude] = useState(null);
  const [longitude, setLongitude] = useState(null);
  const [locationString, setLocationString] = useState('');

  useEffect(() => {
    ARTICLES_GET_ALL_CATEGORIES_API_CALL({
      filter: ['footerCats'],
    });
  }, []);

  useEffect(
    () => {
      console.log(pathname, lat, lng);
      if (pathname === '/' && !lat && !lng) {
        getLocation();
      }
    },
    [lat, lng],
  );

  const { data: { data = [] } = {} } = useMemo(
    () => getData(ARTICLES_GET_ALL_CATEGORIES_API, {}, false, ['footerCats']),
    [ARTICLES_GET_ALL_CATEGORIES_API],
  );

  const locationRedirection = ({ lati, long, locationName, businessType }) => {
    Router.push(
      `/daycare/search?search_type=location&locationString=${locationName}&latitude=${lati}&longitude=${long}&organisation=${businessType}&type=radius&sort_by=distance`,
    ).then(() => window.scrollTo(0, 0));
  };

  const allTimeDaycare = ({ lati, long, locationName }) => {
    Router.push(
      `/daycare/search?search_type=location&locationString=${locationName}&latitude=${lati}&longitude=${long}&organisation=1&schedule_type=4&type=radius&sort_by=distance`,
    ).then(() => window.scrollTo(0, 0));
  };

  const montesoriSchool = ({ lati, long, locationName }) => {
    Router.push(
      `/daycare/search?search_type=location&locationString=${locationName}&latitude=${lati}&longitude=${long}&course_type=1&type=radius&sort_by=distance`,
    ).then(() => window.scrollTo(0, 0));
  };

  const getLocation = () => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        ({ coords: { latitude: lati, longitude: lngi } = {} }) => {
          setLatitude(lati);
          setLongitude(lngi);
          console.log(lati, lngi);
          Geocode.fromLatLng(
            String(lati.toFixed(5)),
            String(lngi.toFixed(5)),
          ).then(({ results = [] }) => {
            if (results[0]) {
              const locName = results[0].formatted_address;
              setLocationString(locName);
              dispatch({
                type: 'CURRENT_LOCATION_UPATE',
                payload: { name, lat: lati, lng: lngi },
              });
            }
          });
        },
        err => {
          console.log(err);
        },
      );
    }
  };

  return (
    <>
      <FooterWrapper background={props.bgColor} className={props.className}>
        <BgImage>
          <img src={footerBg} alt="" />
        </BgImage>
        <Flex column alignCenter>
          <Join>Reach parents through Kiddenz</Join>
          <Button
            href="/"
            text="Register your school"
            type="btnWhite"
            headerButton
            onClick={() =>
              Router.push({
                pathname: '/provider',
              }).then(() => window.scrollTo(0, 0))
            }
          />
        </Flex>
        <FooterListWrapper flexMargin="60px auto 30px" wrap>
          <FooterList>
            <FooterListTitle>Parents</FooterListTitle>
            <FooterListItem
              onClick={() => {
                console.log(latitude, longitude, lat, lng);
                if ((latitude && longitude) || (lat && lng)) {
                  if (props.filterState) {
                    const filt = { ...props.filterState };
                    filt.organisation = props.filterState.organisation;
                    filt.course_type = props.filterState.course_type;
                    filt.course_type = null;
                    filt.organisation = '1';
                    props.setFilterState(filt);
                  }
                  locationRedirection({
                    lati: latitude || lat,
                    long: longitude || lng,
                    locationName: locationString || name || 'Current location',
                    businessType: 1,
                  });
                } else {
                  Router.push({
                    pathname: '/daycare/landing',
                  }).then(() => window.scrollTo(0, 0));
                }
              }}
            >
              Preschool near me
            </FooterListItem>

            <FooterListItem
              onClick={() => {
                if ((latitude && longitude) || (lat && lng)) {
                  if (props.filterState) {
                    const filt = { ...props.filterState };
                    filt.organisation = props.filterState.organisation;
                    filt.course_type = props.filterState.course_type;
                    filt.course_type = null;
                    filt.organisation = '1';
                    props.setFilterState(filt);
                  }
                  locationRedirection({
                    lati: latitude || lat,
                    long: longitude || lng,
                    locationName: locationString || name || 'Current location',
                    businessType: 1,
                  });
                } else {
                  Router.push({
                    pathname: '/daycare/landing',
                  }).then(() => window.scrollTo(0, 0));
                }
              }}
            >
              Playschool near me
            </FooterListItem>

            <FooterListItem
              onClick={() => {
                if ((latitude && longitude) || (lat && lng)) {
                  if (props.filterState) {
                    const filt = { ...props.filterState };
                    filt.organisation = props.filterState.organisation;
                    filt.course_type = props.filterState.course_type;
                    filt.course_type = null;
                    filt.organisation = '2';
                    props.setFilterState(filt);
                  }
                  locationRedirection({
                    lati: latitude || lat,
                    long: longitude || lng,
                    locationName: locationString || name || 'Current location',
                    businessType: 2,
                  });
                } else {
                  Router.push({
                    pathname: '/daycare/landing',
                  }).then(() => window.scrollTo(0, 0));
                }
              }}
            >
              Daycare near me
            </FooterListItem>

            <FooterListItem
              onClick={() => {
                if ((latitude && longitude) || (lat && lng)) {
                  allTimeDaycare({
                    lati: latitude || lat,
                    long: longitude || lng,
                    locationName: locationString || name || 'Current location',
                  });
                } else {
                  Router.push({
                    pathname: '/daycare/landing',
                  }).then(() => window.scrollTo(0, 0));
                }
              }}
            >
              24-hour daycare near me
            </FooterListItem>

            <FooterListItem
              onClick={() => {
                if ((latitude && longitude) || (lat && lng)) {
                  if (props.filterState) {
                    const filt = { ...props.filterState };
                    filt.course_type = props.filterState.course_type;
                    filt.organisation = props.filterState.organisation;
                    filt.organisation = null;
                    filt.course_type = '1';
                    props.setFilterState(filt);
                  }
                  montesoriSchool({
                    lati: latitude || lat,
                    long: longitude || lng,
                    locationName: locationString || name || 'Current location',
                  });
                } else {
                  Router.push({
                    pathname: '/daycare/landing',
                  }).then(() => window.scrollTo(0, 0));
                }
              }}
            >
              Montessori near me
            </FooterListItem>

            <FooterListItem
              onClick={() => {
                if ((latitude && longitude) || (lat && lng)) {
                  locationRedirection({
                    lati: latitude || lat,
                    long: longitude || lng,
                    locationName: locationString || name || 'Current location',
                    businessType: 2,
                  });
                } else {
                  Router.push({
                    pathname: '/daycare/landing',
                  }).then(() => window.scrollTo(0, 0));
                }
              }}
            >
              Childcare near me
            </FooterListItem>
            <Link href="/all-cities-index">
              <FooterListItem>Browse by city</FooterListItem>
            </Link>
          </FooterList>
          {/* <FooterList>
          <FooterListTitle>Providers</FooterListTitle>
          <FooterListItem>Register with Kiddenz</FooterListItem>
          <FooterListItem>Get Reviews</FooterListItem>
          <FooterListItem>Advertise on Kiddenz</FooterListItem>
          <FooterListItem>More Details</FooterListItem>
          <FooterListItem>Contact</FooterListItem>
        </FooterList> */}
          <FooterList>
            <FooterListTitle>Kiddenz</FooterListTitle>
            <Link href="/about-us">
              <FooterListItem>About Kiddenz</FooterListItem>
            </Link>
            <Link href="/daycare/landing">
              <FooterListItem>Best preschool and daycare</FooterListItem>
            </Link>
            <Link href="/daycare/landing?search_by=location">
              <FooterListItem>Search by location</FooterListItem>
            </Link>
            <Link href="/daycare/landing?search_by=trip">
              <FooterListItem>Search by route</FooterListItem>
            </Link>
            <Link href="/careers">
              <FooterListItem>Career</FooterListItem>
            </Link>
            <Link href="/blog">
              <FooterListItem>Parenting articles</FooterListItem>
            </Link>
          </FooterList>
          <FooterList>
            <FooterListTitle>Preschools</FooterListTitle>
            <Link href="/bengaluru/vidyaranyapura">
              <FooterListItem>Preschool in Vidyaranyapura</FooterListItem>
            </Link>
            <Link href="/bengaluru/kadugodi">
              <FooterListItem>Preschool in Kadugodi</FooterListItem>
            </Link>
            <Link href="/bengaluru/vijaynagar">
              <FooterListItem>Preschool in Vijaynagar</FooterListItem>
            </Link>
            <Link href="/bengaluru/yelahanka">
              <FooterListItem>Preschool in Yelahanka</FooterListItem>
            </Link>
            <Link href="/bengaluru/whitefield">
              <FooterListItem>Preschool in Whitefield</FooterListItem>
            </Link>
            <Link href="/bengaluru/govindraj-nagar">
              <FooterListItem>Preschool in Govindraj nagar</FooterListItem>
            </Link>
            <Link href="/bengaluru/kr-puram">
              <FooterListItem>Preschool in KR Puram</FooterListItem>
            </Link>
            <Link href="/bengaluru/nagarbhavi">
              <FooterListItem>Preschool in Nagarbhavi</FooterListItem>
            </Link>
            <Link href="/bengaluru/arekere">
              <FooterListItem>Preschool in Arekere</FooterListItem>
            </Link>
            <Link href="/daycare/landing?search_by=location">
              <FooterListItem>More preschools...</FooterListItem>
            </Link>
          </FooterList>
          <FooterList>
            <FooterListTitle>Daycare</FooterListTitle>
            <Link href="/bengaluru/koramangala">
              <FooterListItem>Daycare in Koramangala</FooterListItem>
            </Link>
            <Link href="/bengaluru/basavangudi">
              <FooterListItem>Daycare in Basavangudi</FooterListItem>
            </Link>
            <Link href="/bengaluru/gottigere">
              <FooterListItem>Daycare in Gottigere</FooterListItem>
            </Link>
            <Link href="/bengaluru/btm-layout">
              <FooterListItem>Daycare in Btmlayout</FooterListItem>
            </Link>
            <Link href="/bengaluru/ananthnagar">
              <FooterListItem>Daycare in Ananthnagar</FooterListItem>
            </Link>
            <Link href="/bengaluru/bannerghetta">
              <FooterListItem>Daycare in Bannerghetta</FooterListItem>
            </Link>
            <Link href="/bengaluru/jp-nagar">
              <FooterListItem>Daycare in JP Nagar</FooterListItem>
            </Link>
            <Link href="/bengaluru/jayanagar">
              <FooterListItem>Daycare in Jayanagarr</FooterListItem>
            </Link>
            <Link href="/bengaluru/bellandur">
              <FooterListItem>Daycare in Bellandur</FooterListItem>
            </Link>

            <Link href="/daycare/landing?search_by=location">
              <FooterListItem>More daycares...</FooterListItem>
            </Link>
          </FooterList>
          <FooterList>
            <FooterListTitle>For providers</FooterListTitle>
            <Link href="/provider">
              <FooterListItem>Register</FooterListItem>
            </Link>
            <Link href="/faq?section=Providers&question=howGetReview">
              <FooterListItem>Get reviews</FooterListItem>
            </Link>
            <Link href="/faq?section=Providers&question=howGetAdvertise">
              <FooterListItem>Advertise on Kiddenz</FooterListItem>
            </Link>
            <Link href="/faq">
              <FooterListItem>FAQ</FooterListItem>
            </Link>
            <Link href="/provider#contactUs">
              <FooterListItem>Contact us</FooterListItem>
            </Link>
          </FooterList>
        </FooterListWrapper>

        <SocialWrap>
          <IconsWrap>
            <div>
              <a href="https://www.facebook.com/kiddenzcare/" target="_blank">
                <IconsBorder>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="19"
                    height="19"
                    viewBox="0 0 19 19"
                    fill="white"
                  >
                    <path
                      d="M9.5 0C4.25315 0 0 4.27904 0 9.55783C0 14.3281 3.47415 18.2822 8.0161 19V12.32H5.6031V9.55783H8.0161V7.45224C8.0161 5.05704 9.4335 3.73424 11.6042 3.73424C12.6435 3.73424 13.7303 3.92062 13.7303 3.92062V6.27184H12.5333C11.3525 6.27184 10.9848 7.00875 10.9848 7.76478V9.55783H13.6192L13.1983 12.32H10.9848V19C15.5258 18.2832 19 14.3272 19 9.55783C19 4.27904 14.7469 0 9.5 0Z"
                      fill="#FFFFFF"
                    />
                  </svg>
                </IconsBorder>
              </a>
            </div>
            <div>
              <a
                href="https://www.linkedin.com/company/kiddenz/"
                target="_blank"
              >
                <IconsBorder>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="18"
                    height="17"
                    viewBox="0 0 18 17"
                    fill="none"
                  >
                    <path
                      d="M3.6 1.8009C3.59976 2.27829 3.40989 2.73603 3.07216 3.07343C2.73442 3.41083 2.27649 3.60024 1.7991 3.6C1.32171 3.59976 0.863968 3.40989 0.526571 3.07216C0.189175 2.73442 -0.00023847 2.27649 2.25325e-07 1.7991C0.00023892 1.32171 0.19011 0.863968 0.527844 0.526571C0.865579 0.189175 1.32351 -0.00023847 1.8009 2.25325e-07C2.27829 0.00023892 2.73603 0.19011 3.07343 0.527844C3.41083 0.865579 3.60024 1.32351 3.6 1.8009ZM3.654 4.9329H0.0540002V16.2009H3.654V4.9329ZM9.342 4.9329H5.76V16.2009H9.306V10.2879C9.306 6.9939 13.599 6.6879 13.599 10.2879V16.2009H17.154V9.0639C17.154 3.5109 10.8 3.7179 9.306 6.4449L9.342 4.9329Z"
                      fill="#FFFFFF"
                    />
                  </svg>
                </IconsBorder>
              </a>
            </div>
            <div>
              <a href="https://www.instagram.com/kiddenz_care" target="_blank">
                <IconsBorder>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="18"
                    height="18"
                    viewBox="0 0 18 18"
                    fill="none"
                  >
                    <path
                      d="M9 0C11.4453 0 11.7504 0.00899995 12.7098 0.0539999C13.6683 0.0989999 14.3208 0.2493 14.895 0.4725C15.489 0.7011 15.9894 1.0107 16.4898 1.5102C16.9474 1.96011 17.3016 2.50433 17.5275 3.105C17.7498 3.6783 17.901 4.3317 17.946 5.2902C17.9883 6.2496 18 6.5547 18 9C18 11.4453 17.991 11.7504 17.946 12.7098C17.901 13.6683 17.7498 14.3208 17.5275 14.895C17.3022 15.496 16.948 16.0404 16.4898 16.4898C16.0398 16.9473 15.4956 17.3014 14.895 17.5275C14.3217 17.7498 13.6683 17.901 12.7098 17.946C11.7504 17.9883 11.4453 18 9 18C6.5547 18 6.2496 17.991 5.2902 17.946C4.3317 17.901 3.6792 17.7498 3.105 17.5275C2.50409 17.302 1.95977 16.9478 1.5102 16.4898C1.05247 16.04 0.698338 15.4957 0.4725 14.895C0.2493 14.3217 0.0989999 13.6683 0.0539999 12.7098C0.0116999 11.7504 0 11.4453 0 9C0 6.5547 0.00899995 6.2496 0.0539999 5.2902C0.0989999 4.3308 0.2493 3.6792 0.4725 3.105C0.697713 2.50396 1.05192 1.95959 1.5102 1.5102C1.9599 1.05231 2.50419 0.698156 3.105 0.4725C3.6792 0.2493 4.3308 0.0989999 5.2902 0.0539999C6.2496 0.0116999 6.5547 0 9 0ZM9 4.5C7.80653 4.5 6.66193 4.97411 5.81802 5.81802C4.97411 6.66193 4.5 7.80653 4.5 9C4.5 10.1935 4.97411 11.3381 5.81802 12.182C6.66193 13.0259 7.80653 13.5 9 13.5C10.1935 13.5 11.3381 13.0259 12.182 12.182C13.0259 11.3381 13.5 10.1935 13.5 9C13.5 7.80653 13.0259 6.66193 12.182 5.81802C11.3381 4.97411 10.1935 4.5 9 4.5ZM14.85 4.275C14.85 3.97663 14.7315 3.69048 14.5205 3.4795C14.3095 3.26853 14.0234 3.15 13.725 3.15C13.4266 3.15 13.1405 3.26853 12.9295 3.4795C12.7185 3.69048 12.6 3.97663 12.6 4.275C12.6 4.57337 12.7185 4.85952 12.9295 5.0705C13.1405 5.28147 13.4266 5.4 13.725 5.4C14.0234 5.4 14.3095 5.28147 14.5205 5.0705C14.7315 4.85952 14.85 4.57337 14.85 4.275ZM9 6.3C9.71608 6.3 10.4028 6.58446 10.9092 7.09081C11.4155 7.59716 11.7 8.28392 11.7 9C11.7 9.71608 11.4155 10.4028 10.9092 10.9092C10.4028 11.4155 9.71608 11.7 9 11.7C8.28392 11.7 7.59716 11.4155 7.09081 10.9092C6.58446 10.4028 6.3 9.71608 6.3 9C6.3 8.28392 6.58446 7.59716 7.09081 7.09081C7.59716 6.58446 8.28392 6.3 9 6.3Z"
                      fill="#FFFFFF"
                    />
                  </svg>
                </IconsBorder>
              </a>
            </div>
          </IconsWrap>
        </SocialWrap>

        <FooterArticle>
          <h2>Articles</h2>
          <ArticleList>
            {data
              .filter(d => d.name !== 'Uncategorized')
              .map(({ name: title, slug, term_id: categoryId }) => (
                <Link
                  href={`/blog/category/${slug}?id=${categoryId}&name=${title}`}
                >
                  <li>{title}</li>
                </Link>
              ))}
            {/* <li>Education</li>
          <li>Behaviour and habits</li>
          <li>Feeding</li>
          <li>Child activities</li>
          <li>Parenting</li>
          <li>Nutrition</li>
          <li>Dental care</li>
          <li>Technology</li>
          <li>Development</li>
          <li>Potty training</li>
          <li>Travel</li>
          <li>Abuse</li>
          <li>Sleep</li>
          <li>Special children</li>
          <li>Overall Health</li> */}
          </ArticleList>
        </FooterArticle>
        <Flex justifyCenter flexMargin="0px 0px 0px">
          <Copywrite>
            <TermsLink href="/terms-of-use" target="_blank">
              Terms
            </TermsLink>{' '}
            ·{' '}
            <TermsLink href="/privacy-policy" target="_blank">
              Privacy
            </TermsLink>{' '}
            ·{' '}
            <TermsLink href="/copyright" target="_blank">
              Copyright
            </TermsLink>{' '}
            <p>· &copy; 2023 Kiddenz · All rights reserved.</p>
          </Copywrite>
        </Flex>
        <Flex justifyCenter flexMargin="0px 0px 0px">
          <Copywrite>
            <p>Vollgas Eduventures Private Limited</p>
          </Copywrite>
        </Flex>
      </FooterWrapper>
    </>
  );
}

Footer.propTypes = {
  getData: PropTypes.func,
  dispatch: PropTypes.func,
  bgColor: PropTypes.string,
  className: PropTypes.string,
  ARTICLES_GET_ALL_CATEGORIES_API_CALL: PropTypes.func,
  dashboard: PropTypes.object,
  filterState: PropTypes.object,
  setFilterState: PropTypes.func,
  authentication: PropTypes.object,
  router: PropTypes.object,
};

const TermsLink = styled.a`
  color: inherit;
`;
const BgImage = styled.div`
  position: absolute;
  top: -50px;
  right: 0px;
  left: 0;
  bottom: 0px;
  z-index: -1;
  @media (max-width: 768px) {
    right: -350px;
    left: -350px;
    top: -100px;
  }
  @media (max-width: 500px) {
    right: -450px;
    left: -450px;
    // top: -32px;
    top: -82px;
  }
  @media (max-width: 380px) {
    right: -550px;
    left: -550px;
  }
  img {
    width: 100%;
    height: 100%;
  }
`;
const FooterListWrapper = styled(Flex)`
  max-width: 1200px;
  padding: 0px 20px;

  @media (max-width: 1200px) {
    margin: 40px auto 30px;
  }
  @media (max-width: 768px) {
    margin: 30px 0%;
  }
  @media (max-width: 500px) {
    margin: 30px 2% 30px;
    padding: 0;
  }
`;
const FooterList = styled.ul`
  flex-grow: 1;
  color: ${colors.white};
  list-style-type: none;
  padding-left: 0px;
  &:first-child {
    padding-left: 0px;
  }
  &:nth-child(4) {
    max-width: 33%;
    @media (max-width: 500px) {
      max-width: 44%;
    }
  }
  // &:nth-child(2n)
  // {
  //   @media (max-width:500px) {
  //     margin-left:20px;
  //   }
  // }

  @media (max-width: 1200px) {
    width: 16%;
  }

  @media (max-width: 500px) {
    flex-grow: initial;
    width: 44%;
    // max-width: 50%;
    padding: 0px;
    // margin-right: 20px;
    margin: 0 8px 20px;
  }
  @media (max-width: 320px) {
    // width: 100%;
    max-width: 100%;
  }
`;
const FooterListItem = styled.h4`
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 20px;
  font-weight: 400;
  cursor: pointer;
  margin: 0px 0px 3px;
  @media (max-width: 1200px) {
    font-size: 12px;
    line-height: 16px;
  }
  @media (max-width: 500px) {
    font-size: 10px;
  }
`;

const FooterListTitle = styled.h3`
  margin: 0px 0px 20px;
  font-size: 16px;
  font-weight: 400;
  line-height: 18px;
  @media (max-width: 1200px) {
    font-size: 14px;
    line-height: 16px;
  }
`;
const FooterWrapper = styled.section`
  position: relative;
  // height: 565px;
  padding-top: 120px;
  background: ${props => props.background || '#fff'};
  z-index: 10;
  &.straight {
    padding-top: 70px;
    z-index: 1;
    @media (max-width: 500px) {  
      padding-top: 0px;
    }
    ${BgImage} {
      display: none;
    }
    // ${FooterList} {
    //   padding: 0px;
    // }
    // ${FooterListItem} {
    //   font-size: 12px;
    // }
    // ${FooterListTitle} {
    //   font-size: 14px;
    // }
  // }
  }
  @media (max-width: 768px) {
    padding-top: 30px;
    // background: none;
  }
`;

const Copywrite = styled.div`
  color: ${colors.white};
  font-family: 'Roboto', sans-serif;
  font-weight: 400;
  font-size: 14px;
  line-height: 20px;
  margin-bottom: 20px;
  @media screen and (max-width: 768px) {
    margin: 10px 20px;
  }
  @media screen and (max-width: 500px) {
    font-size: 12px;
    text-align: center;
  }
  p {
    font-family: Roboto, sans-serif;
    display: inline;
    margin: 0px;
    @media screen and (max-width: 500px) {
      display: block;
      margin: 10px 0px 0px;
    }
  }
`;
const Join = styled.h4`
  color: #ffffff;
  font-family: Quicksand;
  font-size: 24px;
  font-weight: 600;
  letter-spacing: 0;
  line-height: 30px;
  margin: 0px 0px 30px;
  @media (max-width: 500px) {
    font-size: 20px;
    margin-top: 40px;
  }
  @media (max-width: 500px) {
    font-size: 16px;
  }
`;

const FooterArticle = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  max-width: 1200px;
  padding: 50px 20px;
  margin: 0px auto;
  border-top: 1px solid #ffffff;
  h2 {
    color: #fff;
    font-family: Quicksand;
    font-size: 16px;
    font-weight: 600;
    letter-spacing: 0;
    line-height: 20px;
    margin: 0px 0px 30px;
    @media (max-width: 768px) {
      margin: 0;
    }
  }
  @media (max-width: 768px) {
    padding: 20px 0 0;
  }
`;
const ArticleList = styled.ul`
  display: flex;
  flex-wrap: wrap;
  padding: 0px;
  margin: 0px 0px 10px;
  justify-content: center;
  li {
    display: flex;
    justify-content: center;
    align-items: center;
    margin: 0px 10px 10px 0px;
    height: 50px;
    padding: 0px 32px;
    width: fit-content;
    border: 1px solid #ffffff;
    border-radius: 5px;
    box-shadow: 2px 2px 8px 0 rgba(0, 0, 0, 0.05);
    color: #ffffff;
    font-family: Roboto;
    font-size: 14px;
    letter-spacing: 0;
    line-height: 28px;
    cursor: pointer;
    transition: all 0.3s ease;
    &:hover,
    &.active {
      background-color: #fff;
      color: #55357f;
      transition: all 0.3s ease;
    }
    @media (max-width: 1100px) {
      height: 44px;
      padding: 0px 24px;
      font-size: 13px;
    }
    @media (max-width: 900px) {
      height: 40px;
      padding: 0px 20px;
      font-size: 13px;
    }
    @media (max-width: 500px) {
      height: 36px;
      padding: 0px 14px;
      font-size: 12px;
    }
  }
`;

const SocialWrap = styled.div`
  display: flex;
  justify-content: center;
  margin-bottom: 20px;
`;
const IconsWrap = styled.div`
  display: flex;
  gap: 35px;
`;
const IconsBorder = styled.div`
  align-items: center;
  border: 1px solid white;
  border-radius: 50%;
  cursor: pointer;
  display: flex;
  height: 35px;
  justify-content: center;
  transition: all 0.25s ease;
  width: 35px;
`;

export default Footer;
