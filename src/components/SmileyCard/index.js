import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
// import education from '../../images/articleIcons/education.svg';

const SmileyCard = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  // height: 50px;
  width: fit-content;
  border-radius: 5px;
  background-color: #ffffff;
  padding: 10px 10px;
  box-shadow: 2px 2px 8px 0 rgba(0, 0, 0, 0.05);
  color: #666c78;
  font-family: 'Quicksand', sans-serif;
  font-size: 14px;
  font-weight: 500;
  line-height: 28px;
  cursor: pointer;
  margin: ${props => props.margin || '0px 10px 10px 0px'};
  transition: all 0.3s;
  cursor: pointer;
  border: 0.25px solid #fff;
  & > .iconify {
    width: 22px;
    height: 22px;
    color: ${props => props.iconColor || 'rgba(99, 202, 185, 1)'};
    margin-right: 10px;
  }
  h3 {
    color: #666c78;
    font-family: 'Quicksand', sans-serif;
    font-size: 14px;
    font-weight: 500;
    line-height: 28px;
    margin: 0px;
  }
  &:hover {
    box-shadow: 2px 2px 8px 0 rgba(0, 0, 0, 0.1);
    transform: scale(1.1);
    transition: all 0.3s;
    border-color: #c0c8cd;
  }
  @media (max-width: 1155px) {
    font-size: 12px;
    margin: 0px 5px 10px 0px;
  }
  @media (max-width: 860px) {
    padding: 10px 5px;
    font-size: 11px;
  }
  @media (max-width: 500px) {
    padding: 5px 15px;
    font-size: 13px;
  }
`;
const Smiley = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 30px;
  width: 30px;
  border-radius: 20px;
  background-color: ${props => props.bgcolor || '#63CAB9'};
  margin-right: 10px;
  &.category {
    background-color: #a3a9b7;
  }

  @media (max-width: 1155px) {
    height: 25px;
    width: 25px;
  }
  @media (max-width: 1155px) {
    height: 22px;
    width: 22px;
  }
  .iconify {
    color: #fff;
    width: 20px;
    height: 20px;
    @media (max-width: 1155px) {
      height: 15px;
      width: 15px;
    }
    @media (max-width: 1155px) {
      height: 12px;
      width: 12px;
    }
  }
`;
const Smileycard = ({
  text,
  margin,
  bgcolor,
  type,
  onClick,
  iconName,
  iconColor,
  icon,
}) => (
  <SmileyCard
    margin={margin}
    type={type}
    className="categories"
    onClick={onClick}
    iconColor={iconColor}
  >
    {type === 'landing' && (
      <Smiley bgcolor={bgcolor}>
        {icon ? (
          <img src={icon} alt="" />
        ) : (
          <span
            className="iconify"
            data-icon="emojione-monotone:smiling-face-with-smiling-eyes"
            data-inline="false"
          />
        )}
      </Smiley>
    )}
    {type === 'category' && (
      <Smiley bgcolor={bgcolor} className="category">
        <span
          className="iconify"
          data-icon="mdi:dots-horizontal"
          data-inline="false"
        />
      </Smiley>
    )}
    {type === 'parenting' && (
      <span
        className="iconify"
        data-icon={iconName}
        data-inline="false"
        iconColor={iconColor}
      />
    )}
    <h3>{text}</h3>
  </SmileyCard>
);
Smileycard.propTypes = {
  text: PropTypes.string,
  margin: PropTypes.string,
  bgcolor: PropTypes.string,
  iconName: PropTypes.string,
  type: PropTypes.string,
  iconColor: PropTypes.string,
  icon: PropTypes.string,
  onClick: PropTypes.func,
};
export default Smileycard;
