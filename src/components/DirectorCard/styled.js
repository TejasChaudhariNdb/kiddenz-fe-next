import styled from 'styled-components';
import { Photo } from '../PhotoCard/styled';
export const Comment = styled.div`
  white-space: pre-line;
  position: relative;
  color: #30333b;
  font-family: 'Quicksand', sans-serif;
  font-size: 16px;
  font-weight: 500;
  line-height: 22px;
  //   text-align:justify;
  @media (max-width: 1300px) {
    font-size: 12px;
  }
  @media screen and (max-width: 400px) {
    font-size: 12px;
  }
  @media screen and (max-width: 380px) {
    font-size: 11px;
  }
`;
export const Name = styled.div`
  color: #30333b;
  font-family: 'Quicksand', sans-serif;
  margin-bottom: 5px;
  font-size: 16px;
  font-weight: bold;
  letter-spacing: 0;
  line-height: 28px;
`;

export const DirectorCard = styled.div`
  width: 100%;
  padding: 30px 30px 30px 20px;
  min-height: 183px;
  margin-bottom: 30px;
  border-radius: 10px;
  font-weight: 300;
  background-color: #f2eff5;
  ${Photo} {
    float: left;
    @media screen and (max-width: 380px) {
      margin-right: 10px;
      width: 110px;
      height: 110px;
    }
  }

  @media screen and (max-width: 767px) {
    padding: 20px;
  }
`;

export const CommentIcon = styled.div`
  float: left;
  margin: 2px 15px 15px 0px;
  @media screen and (max-width: 380px) {
    margin: 2px 5px 5px 0px;
  }
  .iconify {
    width: 28px;
    height: 28px;
    color: #60b947;
    @media screen and (max-width: 380px) {
      width: 22px;
      height: 22px;
    }
  }
`;
export const Job = styled.div`
  color: rgba(102, 108, 120, 1);
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  font-weight: 400;
  line-height: 20px;
  margin-bottom: 22px;
  @media (max-width: 1300px) {
    font-size: 12px;
  }
`;
