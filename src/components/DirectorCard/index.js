/* eslint-disable no-unused-vars */
/* eslint-disable react/no-unused-prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import PhotoCard from '../PhotoCard/index';
// import image2 from '../../images/profileImage.jpg';
import image2 from '../../images/center-head-ph.png';
// import profile from '../../images/profile.jpg';
import Flex from '../common/flex';

import { DirectorCard, Name, CommentIcon, Comment, Job } from './styled';

const Director = ({ name, comment, type, job, img }) => (
  <DirectorCard className={type}>
    <PhotoCard
      image={img || image2}
      width="120px"
      height="120px"
      margin="0px 30px 4px 0px"
    />

    {name && name.length ? (
      <Name>{name.charAt(0).toUpperCase() + name.slice(1)}</Name>
    ) : null}
    {job && job.length ? (
      <Job>{job.charAt(0).toUpperCase() + job.slice(1)}</Job>
    ) : null}

    <Comment>
      <CommentIcon>
        <span className="iconify" data-icon="ls:quote" data-inline="false" />
      </CommentIcon>
      {comment}
    </Comment>
  </DirectorCard>
);
Director.propTypes = {
  name: PropTypes.string,
  comment: PropTypes.string,
  type: PropTypes.string,
  job: PropTypes.string,
  img: PropTypes.string,
};
export default Director;
