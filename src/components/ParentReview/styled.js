import styled from 'styled-components';
export const ParentReviews = styled.div`
  margin: ${props => props.margin || '30px 0px 0px;'};
  border-bottom: 1px solid #e6e8ec;
  .commentArea {
    width: 100%;
    padding: 10px;
    border: 1px solid #c0c8cd;
    border-radius: 5px;
    resize: none;
    outline: none;
    margin: 20px 0px;
    &::placeholder {
      opacity: 0.5;
      color: #30333b;
      font-family: 'Roboto', sans-serif;
      font-size: 16px;
      line-height: 21px;
    }
  }
  @media (max-width: 320px) {
    padding-bottom: 15px;
  }
`;
export const ParaGraph = styled.div`
  color: #666c78;
  font-family: 'Roboto', sans-serif;
  font-size: 16px;
  line-height: 24px;
  margin: 20px 0px;
`;
export const Likes = styled.div`
  display: flex;
  align-items: center;
  margin-right: 20px;
  color: #666c78;
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 19px;

  .iconify {
    width: 22px;
    height: 22px;
    olor: #666c78;
    margin-right: 4px;
  }
`;
