import React from 'react';
import PropTypes from 'prop-types';
import Button from 'components/Button';
import { ParentReviews, ParaGraph, Likes } from './styled';
import ParentRating from '../ParentRating/index';
import Flex from '../common/flex';
import CheckBox from '../Checkbox';
const ParentReview = ({
  comment,
  initialRating,
  image,
  name,
  date,
  margin,
  writeComment,
  readonly,
  attach,
  likesNum,
  commentNum,
  reportNum,
  star3,
}) => (
  <ParentReviews margin={margin}>
    <ParentRating
      initialRating={initialRating}
      image={image}
      name={name}
      date={date}
      readonly={readonly}
      star3={star3}
    />
    {writeComment && (
      <textarea rows="3" className="commentArea" placeholder={writeComment} />
    )}
    {comment && <ParaGraph>{comment}</ParaGraph>}
    {attach && (
      <Flex justifyBetween flexMargin="0px 0px 20px 0px">
        <Button text="Attach File" type={attach} />
        <Flex flexMarginXs="0px 0px 0px 10px">
          <CheckBox label="Anonymous" id="id81" margin="0px 30px 0px 0px" />
          <Button text="Post" type="mobile" />
        </Flex>
      </Flex>
    )}
    {likesNum && (
      <Flex flexMargin="0px 0px 20px 0px">
        <Likes>
          <span
            className="iconify"
            data-icon="ant-design:like-outline"
            data-inline="false"
          />
          {likesNum}
        </Likes>
        <Likes>
          <span
            className="iconify"
            data-icon="bx:bx-comment"
            data-inline="false"
          />
          {commentNum}
        </Likes>
        <Likes>
          <span
            className="iconify"
            data-icon="bx:bx-flag"
            data-inline="false"
          />
          {reportNum}
        </Likes>
      </Flex>
    )}
  </ParentReviews>
);
ParentReview.propTypes = {
  name: PropTypes.string,
  date: PropTypes.string,
  initialRating: PropTypes.string,
  image: PropTypes.string,
  comment: PropTypes.string,
  margin: PropTypes.string,
  writeComment: PropTypes.string,
  readonly: PropTypes.string,
  attach: PropTypes.string,
  likesNum: PropTypes.number,
  commentNum: PropTypes.number,
  reportNum: PropTypes.string,
  star3: PropTypes.string,
};
export default ParentReview;
