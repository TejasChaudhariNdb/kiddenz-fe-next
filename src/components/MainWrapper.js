import styled from 'styled-components';

const MainWrapper = styled.div`
  background-color: ${props => props.background || 'transperant'};
  height: 100%;
  display: flex;
  flex-direction: column;
  padding-top: ${props => props.paddingTop || '0px'};
  z-index: 1000;
  &.schoolProfile {
    padding-top: 0px;
  }
  .preCare {
    &:hover {
      color: #55357f;
      background-color: transparent !important;
      border-color: #55357f;
    }
  }
  &.searchResult {
    // padding-top: 0px;
    @media (max-width: 1200px) {
      padding-top: 170px;
    }
    @media (max-width: 900px) {
      padding-top: 10px;
    }
    @media (max-width: 500px) {
      padding-top: 0px;
    }
  }
  @media (max-width: 768px) {
    padding-top: 52px;
    .mobileColumn {
      flex-direction: column;
      button:first-child {
        margin-bottom: 10px !important;
      }
      button {
        padding: 0 30px;
        width: 220px;
        height: auto;
        margin: 0;
        font-weight: bold;
        font-size: 12px;
        line-height: 15px;
        padding: 15px;
      }
    }
  }
  .viewArticle {
    @media screen and (max-width: 768px) {
      margin-bottom: 45px !important;
    }
  }
`;
export default MainWrapper;
