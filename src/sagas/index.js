import { all, fork } from "redux-saga/effects";

import DashboardSaga from "shared/containers/Dashboard/generator";
import AuthenticationSaga from "shared/containers/Authentication/generator";

const SAGA_GENERATOR = [DashboardSaga, AuthenticationSaga];
function* rootSaga() {
  yield all(SAGA_GENERATOR.map(fork));
}

export default rootSaga;
