/**
 * This is axios intercepter which intercepts all the incoming and outgoing requests
 */
import axios from 'axios';
import promise from 'promise';
// import BASE_URL from '../utils/BaseUrl';
import { getCookie } from '../shared/utils/commonHelpers';
// import { SPECIFIC_ERROR_HANDLER } from './errorHandler';
const request = axios;
request.defaults.withCredentials = true;
if (typeof window === 'object') {
  const token = getCookie('x-access-token');
  request.defaults.headers.common.Authorization = token
    ? `Bearer ${token}`
    : '';
}

request.interceptors.request.use(
  config => {
    if (!config.baseURL) {
      // request.defaults.baseURL = BASE_URL;
      // config.baseURL = BASE_URL; // eslint-disable-line no-param-reassign
    }
    return config;
  },
  // SPECIFIC_ERROR_HANDLER([], error);
  error => promise.reject(error),
);

// eslint-disable-next-line arrow-body-style
request.interceptors.response.use(undefined, error => {
  // Handle your common errors here
  // SPECIFIC_ERROR_HANDLER([500, 503], error);
  return Promise.reject(error);
});

export default request;
