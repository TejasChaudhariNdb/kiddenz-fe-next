export const RESTART_ON_REMOUNT = '@@saga-injector/restart-on-remount';
export const DAEMON = '@@saga-injector/daemon';
export const ONCE_TILL_UNMOUNT = '@@saga-injector/once-till-unmount';
export const headers = '';

export const SEO_TAGS = {
  index: {
    title: 'Best Daycare, Preschool and Play school near me - Kiddenz',
    description:
      ' Are you a parent who is looking for the Best Preschool, Daycare, Play school? View photos, virtual tour, curriculum, & reviews & get attractive discounts.',
    keyword: 'Kiddenz - Day Care',
  },
  dayCareLanding: {
    title: 'Find Top Preschool, Playschool, and Daycare - Kiddenz',
    description:
      'Are you a parent who is looking for the Best Preschool, Daycare, Play school for your child? Find the right childcare at your location or on your way to work.',
    keyword: 'Kiddenz - Day Care',
  },
  dayCareSearch: {
    title: 'Kiddenz - Day Care',
    description: 'Kiddenz - Day Care',
    keyword: 'Kiddenz - Day Care',
  },
  dayCareDetail: {
    title: 'Kiddenz - Day Care',
    description: 'Kiddenz - Day Care',
    keyword: 'Kiddenz - Day Care',
  },
  articleList: {
    title: 'Parenting Blogs by experts and parents.',
    description:
      'Discover best articles written by experts and parents. Find answers to all child care related queries and questions on Kiddenz.',
    keyword: 'Kiddenz - Day Care',
  },
  articleDetail: {
    title: 'Kiddenz - Day Care',
    description: 'Kiddenz - Day Care',
    keyword: 'Kiddenz - Day Care',
  },
  categoryList: {
    title: 'Kiddenz - Day Care',
    description: 'Kiddenz - Day Care',
    keyword: 'Kiddenz - Day Care',
  },
  subCategoryList: {
    title: 'Kiddenz - Day Care',
    description: 'Kiddenz - Day Care',
    keyword: 'Kiddenz - Day Care',
  },
  parentingCommunity: {
    title: 'Kiddenz - Day Care',
    description: 'Kiddenz - Day Care',
    keyword: 'Kiddenz - Day Care',
  },
  aboutUs: {
    title: 'Kiddenz – About Us',
    description:
      'Kiddenz is a preschool and daycare search platform that helps parents find high quality daycare, preschool, & playschool. We connect parents with trusted child care providers, offer discounts and counseling support',
    keyword: 'Kiddenz - Day Care',
  },
  copyright: {
    title: 'Kiddenz – Copy Right',
    description: 'Kiddenz – Copy Right',
    keyword: 'Kiddenz – Copy Right',
  },
  terms: {
    title: 'Kiddenz – Terms and Conditions',
    description: 'Kiddenz – Terms and Conditions',
    keyword: 'Kiddenz – Terms and Conditions',
  },
  privacy: {
    title: 'Kiddenz – Privacy Policy',
    description: 'Kiddenz – Privacy Policy',
    keyword: 'Kiddenz – Privacy Policy',
  },
  careers: {
    title: 'Careers at Kiddenz',
    description:
      'Kiddenz is Bengaluru based startup that is revolutionizing parenting. Join Kiddenz to get a chance to do the kind of work that adds up to something meaningful. ',
    keyword: 'Kiddenz - Day Care',
  },
  faq: {
    title: 'Kiddenz - FAQ',
    description:
      'Kiddenz is a preschool and daycare discovery platform designed to help parents find the high quality child care. We enable trust between parents and providers.',
    keyword: 'Kiddenz - Day Care',
  },
  profile: {
    title: 'Kiddenz - Day Care',
    description: 'Kiddenz - Day Care',
    keyword: 'Kiddenz - Day Care',
  },
};

export const CLICK_COUNTS = {
  locationSearch: 200,
  tripSearch: 200,
  filterApplication: 200,
  schoolProfileClick: 200,
};
