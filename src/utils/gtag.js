export const GA_TAG_MANAGER_ID = '';
export const GA_ANALYTICS_ID = '';
export const PHONE_CONVERSION_NUMBER = '';
export const PHONE_CONVERSION_ID = '';
export const FACEBOOK_PIXEL_ID = '309597086791499';
export const LINKEDIN_INSIGHT_ID = '';
export const LEAD_CONVERSION_ID = '';

// https://developers.google.com/analytics/devguides/collection/gtagjs/pages
export const pageview = url => {
  window.gtag('config', GA_TAG_MANAGER_ID, {
    page_path: url,
  });
};

// https://developers.google.com/analytics/devguides/collection/gtagjs/events
export const event = ({ action, category, label, value }) => {
  window.gtag('event', action, {
    event_category: category,
    event_label: label,
    value,
  });
};
