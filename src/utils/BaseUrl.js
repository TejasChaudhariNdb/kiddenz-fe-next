// 360 Url
// export const virtualTourlBaseUrl = 'https://virtualtour.kiddenz.com'; // staging
export const virtualTourlBaseUrl = 'https://virtualtourprod.kiddenz.com'; // kiddenz Prod
export const servertype = 'production'; // kiddenz Prod

// import config from '../../server_env';
// export default 'http://13.233.208.216/';
// export default 'http://13.127.11.28/';
// export default 'https://kidusr.cartoonmango.com/'; // Staging
export default 'https://userprodbe.kiddenz.com/'; // kiddenz Prod
// export default 'https://kiddenzbc.cartoonmango.com/'; // Staging
// export default 'https://userbe.kiddenz.com/'; // Prod // test
