import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { createLogger } from 'redux-logger';
import rootReducer from 'reducers';
import { initialState } from 'constants/initialState';
import rootSaga from 'sagas';

const sagaMiddleware = createSagaMiddleware();

// const stateLogger = createLogger({ stateTransformer: state => state });

export const initStore = (
  preloadState = initialState,
  { isServer, req = null },
) => {
  let composeEnhancers = compose;

  /* istanbul ignore next */
  if (process.env.NODE_ENV !== 'production' && typeof window === 'object') {
    /* eslint-disable no-underscore-dangle */
    if (window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__)
      composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({});

    // NOTE: Uncomment the code below to restore support for Redux Saga
    // Dev Tools once it supports redux-saga version 1.x.x
    // if (window.__SAGA_MONITOR_EXTENSION__)
    //   reduxSagaMonitorOptions = {
    //     sagaMonitor: window.__SAGA_MONITOR_EXTENSION__,
    //   };
    /* eslint-enable */
  }
  // eslint-disable-next-line no-unused-vars
  const stateLogger = createLogger({ stateTransformer: state => state });

  const middlewares = [sagaMiddleware];

  const enhancers = [applyMiddleware(...middlewares)];

  const store = createStore(
    rootReducer,
    preloadState,
    // applyMiddleware(sagaMiddleware, stateLogger)
    composeEnhancers(...enhancers),
  );

  if (req || !isServer) {
    store.runSagaTask = () => {
      store.sagaTask = sagaMiddleware.run(rootSaga);
    };
    // store.sagaTask = sagaMiddleware.run(rootSaga);
    store.runSagaTask();
  }
  return store;
};
