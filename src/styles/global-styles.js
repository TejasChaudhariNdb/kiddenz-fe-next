import { createGlobalStyle } from 'styled-components';
const GlobalStyle = createGlobalStyle`
  html,
  body {
    height: 100%;
    width: 100%;
    line-height: 1.5; 
  }
  *
  {
    -webkit-font-smoothing: antialiased;
  }

  body {
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  .overflowHide {
    overflow: hidden;
    z-index: -1;
  }

  body.fontLoaded {
    font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  #app {
    background-color: #fafafa;
    min-height: 100%;
    min-width: 100%;
  }

  p,
  label {
    font-family: Georgia, Times, 'Times New Roman', serif;
    line-height: 1.5em;
  }
  
  .a-canvas {
    height: 220px !important;
    width: 100% !important;
  }

  .a-enter-ar {
    display: none;
  }
  
  .a-orientation-modal {
    display: none;
  }

  .react-datepicker-wrapper{
    width: 100%;
  }
  input::placeholder {
    overflow: visible;
  }
`;

export default GlobalStyle;

export const SanitizeStyle = createGlobalStyle`
*,
::before,
::after {
 box-sizing: border-box;
}

::before,
::after {
 text-decoration: inherit; /* 1 */
 vertical-align: inherit; /* 2 */
}


html {
 cursor: default; /* 1 */
 line-height: 1.5; /* 2 */
 -moz-tab-size: 4; /* 3 */
 tab-size: 4; /* 3 */
 -webkit-tap-highlight-color: transparent /* 4 */;
 -ms-text-size-adjust: 100%; /* 5 */
 -webkit-text-size-adjust: 100%; /* 5 */
 word-break: break-word; /* 6 */
}

body {
 margin: 0;
}

h1 {
 font-size: 2em;
 margin: 0.67em 0;
}

dl dl,
dl ol,
dl ul,
ol dl,
ul dl {
 margin: 0;
}

ol ol,
ol ul,
ul ol,
ul ul {
 margin: 0;
}

hr {
 height: 0; /* 1 */
 overflow: visible; /* 2 */
}

/**
* Add the correct display in IE.
*/

main {
 display: block;
}

/**
* Remove the list style on navigation lists in all browsers (opinionated).
*/

nav ol,
nav ul {
 list-style: none;
 padding: 0;
}

/**
* 1. Correct the inheritance and scaling of font size in all browsers.
*/

pre {
 font-family: monospace, monospace; /* 1 */
 font-size: 1em; /* 2 */
}

/* Text-level semantics
* ========================================================================== */

/**
* Remove the gray background on active links in IE 10.
*/

a {
 background-color: transparent;
}

/**
* Add the correct text decoration in Edge 18-, IE, and Safari.
*/

abbr[title] {
 text-decoration: underline;
 text-decoration: underline dotted;
}

/**
* Add the correct font weight in Chrome, Edge, and Safari.
*/

b,
strong {
 font-weight: bolder;
}

/**
* 1. Correct the inheritance and scaling of font size in all browsers.
*/

code,
kbd,
samp {
 font-family: monospace, monospace; /* 1 */
 font-size: 1em; /* 2 */
}

/**
* Add the correct font size in all browsers.
*/

small {
 font-size: 80%;
}

/* Embedded content
* ========================================================================== */

/*
* Change the alignment on media elements in all browsers (opinionated).
*/

audio,
canvas,
iframe,
img,
svg,
video {
 vertical-align: middle;
}

 .slick-slide iframe, video {  
  @media screen and (max-width: 1800px) {
    height: 500px !important;
  } 
  @media screen and (max-width: 1172px) {
    height: 403px !important;
  }
  @media screen and (max-width: 1024px) {
    height: 349px !important;
  }
  @media screen and (max-width: 768px) {
    height: 467px !important;
  } 
  @media screen and (max-width: 628px) {
    height: 380px !important;
  }
  @media screen and (max-width: 540px) {
    height: 325px !important;
  }
  @media screen and (max-width: 425px) {
    height: 253px !important;
  }
  @media screen and (max-width: 375px) {
    height: 222px !important; 
  }
  @media screen and (max-width: 320px) {
    height: 187px !important;
  }
  @media screen and (max-width: 280px) {
    height: 162px !important; 
  }
}
 
.razorpay-container {
  iframe {
    height: 90vh !important;
    margin: 2% 0 0 !important;
    @media screen and (max-width: 768px) {
      height: 100vh !important;
      margin: 0 !important;
    }
  }
  .mchild {        
    max-width: 584px;
    width: 100%;
    max-height: 600px;
    height: 100%; 
  }
  #content {
    height: 100% !important;
  }
  #footer, .submit-button {
    position: fixed !important;
  }
  #form #footer {
    z-index: 100;
  }
}

p.endMessage {
  font-family: Quicksand;
  font-weight: 600;
}

#body.sub #footer {
  bottom: 3%;
}

.button, .btn, .submit-button, .loader::after {
  z-index: 99999999999999;
  
}

// .mobile #footer {
//   bottom: 3%;

// }




/**
* Add the correct display in IE 9-.
*/

audio,
video {
 display: inline-block;
}

/**
* Add the correct display in iOS 4-7.
*/

audio:not([controls]) {
 display: none;
 height: 0;
}

/**
* Remove the border on iframes in all browsers (opinionated).
*/

iframe {
 border-style: none;
}

/**
* Remove the border on images within links in IE 10-.
*/

img {
 border-style: none;
}

/**
* Change the fill color to match the text color in all browsers (opinionated).
*/

svg:not([fill]) {
 fill: currentColor;
}

/**
* Hide the overflow in IE.
*/

svg:not(:root) {
 overflow: hidden;
}

/* Tabular data
* ========================================================================== */

/**
* Collapse border spacing in all browsers (opinionated).
*/

table {
 border-collapse: collapse;
}

/* Forms
* ========================================================================== */

/**
* Remove the margin on controls in Safari.
*/

button,
input,
select {
 margin: 0;
}

/**
* 1. Show the overflow in IE.
* 2. Remove the inheritance of text transform in Edge 18-, Firefox, and IE.
*/

button {
 overflow: visible; /* 1 */
 text-transform: none; /* 2 */
}

/**
* Correct the inability to style buttons in iOS and Safari.
*/

button,
[type="button"],
[type="reset"],
[type="submit"] {
 -webkit-appearance: button;
}

/**
* 1. Change the inconsistent appearance in all browsers (opinionated).
* 2. Correct the padding in Firefox.
*/

fieldset {
 border: 1px solid #a0a0a0; /* 1 */
 padding: 0.35em 0.75em 0.625em; /* 2 */
}

/**
* Show the overflow in Edge 18- and IE.
*/

input {
 overflow: visible;
}

/**
* 1. Correct the text wrapping in Edge 18- and IE.
*/

legend {
 color: inherit; /* 2 */
 display: table; /* 1 */
 max-width: 100%; /* 1 */
 white-space: normal; /* 1 */
}

/**
* 1. Add the correct display in Edge 18- and IE.
* 2. Add the correct vertical alignment in Chrome, Edge, and Firefox.
*/

progress {
 display: inline-block; /* 1 */
 vertical-align: baseline; /* 2 */
}

/**
* Remove the inheritance of text transform in Firefox.
*/

select {
 text-transform: none;
}

/**
* 1. Remove the margin in Firefox and Safari.
* 2. Remove the default vertical scrollbar in IE.
* 3. Change the resize direction in all browsers (opinionated).
*/

textarea {
 margin: 0; /* 1 */
 overflow: auto; /* 2 */
 resize: vertical; /* 3 */
}

/**
* Remove the padding in IE 10-.
*/

[type="checkbox"],
[type="radio"] {
 padding: 0;
}

/**
* 1. Correct the odd appearance in Chrome, Edge, and Safari.
* 2. Correct the outline style in Safari.
*/

[type="search"] {
 -webkit-appearance: textfield; /* 1 */
 outline-offset: -2px; /* 2 */
}

/**
* Correct the cursor style of increment and decrement buttons in Safari.
*/

::-webkit-inner-spin-button,
::-webkit-outer-spin-button {
 height: auto;
}

/**
* Correct the text style of placeholders in Chrome, Edge, and Safari.
*/

::-webkit-input-placeholder {
 color: inherit;
 opacity: 0.54;
}

/**
* Remove the inner padding in Chrome, Edge, and Safari on macOS.
*/

::-webkit-search-decoration {
 -webkit-appearance: none;
}

/**
* 1. Correct the inability to style upload buttons in iOS and Safari.
*/

::-webkit-file-upload-button {
 -webkit-appearance: button; /* 1 */
 font: inherit; /* 2 */
}

/**
* Remove the inner border and padding of focus outlines in Firefox.
*/

::-moz-focus-inner {
 border-style: none;
 padding: 0;
}

/**
* Restore the focus outline styles unset by the previous rule in Firefox.
*/

:-moz-focusring {
 outline: 1px dotted ButtonText;
}

/**
* Remove the additional :invalid styles in Firefox.
*/

:-moz-ui-invalid {
 box-shadow: none;
}

/* Interactive
* ========================================================================== */

/*
* Add the correct display in Edge 18- and IE.
*/

details {
 display: block;
}

/*
* Add the correct styles in Edge 18-, IE, and Safari.
*/

dialog {
 background-color: white;
 border: solid;
 color: black;
 display: block;
 height: -moz-fit-content;
 height: -webkit-fit-content;
 height: fit-content;
 left: 0;
 margin: auto;
 padding: 1em;
 position: absolute;
 right: 0;
 width: -moz-fit-content;
 width: -webkit-fit-content;
 width: fit-content;
}

dialog:not([open]) {
 display: none;
}

/*
* Add the correct display in all browsers.
*/

summary {
 display: list-item;
}

/* Scripting
* ========================================================================== */

/**
* Add the correct display in IE 9-.
*/

canvas {
 display: inline-block;
}

/**
* Add the correct display in IE.
*/

template {
 display: none;
}

/* User interaction
* ========================================================================== */

/*
* 1. Remove the tapping delay in IE 10.
* 2. Remove the tapping delay on clickable elements
     in all browsers (opinionated).
*/

a,
area,
button,
input,
label,
select,
summary,
textarea,
[tabindex] {
 -ms-touch-action: manipulation; /* 1 */
 touch-action: manipulation; /* 2 */
}

/**
* Add the correct display in IE 10-.
*/

[hidden] {
 display: none;
}

/* Accessibility
* ========================================================================== */

/**
* Change the cursor on busy elements in all browsers (opinionated).
*/

[aria-busy="true"] {
 cursor: progress;
}

/*
* Change the cursor on control elements in all browsers (opinionated).
*/

[aria-controls] {
 cursor: pointer;
}

/*
* Change the cursor on disabled, not-editable, or otherwise
* inoperable elements in all browsers (opinionated).
*/

[aria-disabled="true"],
[disabled] {
 cursor: not-allowed;
}

/*
* Change the display on visually hidden accessible elements
* in all browsers (opinionated).
*/

[aria-hidden="false"][hidden] {
 display: initial;
}

[aria-hidden="false"][hidden]:not(:focus) {
 clip: rect(0, 0, 0, 0);
 position: absolute;
}


.desktop {
  background-color: #ffffff;
  display: flex;
  flex-direction: row;
  justify-content: center;
  width: 100%;
}

.desktop .div {
  background-color: #ffffff;
  height: 15034px;
  position: relative;
  width: 1440px;
}

.desktop .frame {
  align-items: center;
  display: flex;
  gap: 8px;
  justify-content: center;
  left: 162px;
  padding: 8px;
  position: absolute;
  top: 11718px;
  width: 1116px;
}

.desktop .founders-of-eurokids {
  color: var(--body-text-color);
  font-family: "Quicksand", Helvetica;
  font-size: 24px;
  font-weight: 400;
  letter-spacing: 0;
  line-height: 36px;
  margin-top: -1px;
  position: relative;
  width: 1100px;
}

.desktop .span {
  font-weight: 700;
}

.desktop .text-wrapper-2 {
  font-family: var(--h-3-font-family);
  font-size: var(--h-3-font-size);
  font-style: var(--h-3-font-style);
  font-weight: var(--h-3-font-weight);
  letter-spacing: var(--h-3-letter-spacing);
  line-height: var(--h-3-line-height);
}

.desktop .text-wrapper-3 {
  font-family: var(--h3-h3-reg-font-family);
  font-size: var(--h3-h3-reg-font-size);
  font-style: var(--h3-h3-reg-font-style);
  font-weight: var(--h3-h3-reg-font-weight);
  letter-spacing: var(--h3-h3-reg-letter-spacing);
  line-height: var(--h3-h3-reg-line-height);
}

.desktop .eurokids-is-a-unique-wrapper {
  align-items: center;
  display: flex;
  gap: 8px;
  justify-content: center;
  left: 170px;
  padding: 8px;
  position: absolute;
  top: 12519px;
  width: 1116px;
}

.desktop .eurokids-is-a-unique {
  color: var(--body-text-color);
  font-family: "Quicksand", Helvetica;
  font-size: 18px;
  font-weight: 400;
  letter-spacing: 0.36px;
  line-height: 30px;
  margin-top: -1px;
  position: relative;
  width: 1098px;
}

.desktop .text-wrapper-4 {
  font-family: var(--h-4-font-family);
  font-size: var(--h-4-font-size);
  font-style: var(--h-4-font-style);
  font-weight: var(--h-4-font-weight);
  letter-spacing: var(--h-4-letter-spacing);
  line-height: var(--h-4-line-height);
}

.desktop .text-wrapper-5 {
  font-weight: 600;
}

.desktop .frame-wrapper {
  height: 56px;
  left: 162px;
  position: absolute;
  top: 11646px;
  width: 1116px;
}

.desktop .div-wrapper {
  align-items: center;
  display: inline-flex;
  gap: 8px;
  justify-content: center;
  padding: 8px;
  position: relative;
  top: -16px;
}

.desktop .text-wrapper-6 {
  color: var(--black);
  font-family: var(--h-2-font-family);
  font-size: var(--h-2-font-size);
  font-style: var(--h-2-font-style);
  font-weight: var(--h-2-font-weight);
  letter-spacing: var(--h-2-letter-spacing);
  line-height: var(--h-2-line-height);
  margin-top: -1px;
  position: relative;
  width: fit-content;
}

.desktop .frame-2 {
  height: 56px;
  left: 170px;
  position: absolute;
  top: 12825px;
  width: 1116px;
}

.desktop .frame-3 {
  height: 56px;
  left: 170px;
  position: absolute;
  top: 12463px;
  width: 1116px;
}

.desktop .p {
  color: var(--black);
  font-family: var(--h-3-font-family);
  font-size: var(--h-3-font-size);
  font-style: var(--h-3-font-style);
  font-weight: var(--h-3-font-weight);
  left: 528px;
  letter-spacing: var(--h-3-letter-spacing);
  line-height: var(--h-3-line-height);
  position: absolute;
  top: 14670px;
  white-space: nowrap;
}

.desktop .frame-4 {
  align-items: flex-start;
  border-radius: 2.4px;
  box-shadow: 0px 2.4px 2.4px #00000040;
  display: inline-flex;
  flex-direction: column;
  gap: 4.8px;
  left: 548px;
  overflow: hidden;
  position: absolute;
  top: 14756px;
}

.desktop .rectangle {
  background-color: var(--green);
  border-radius: 2.4px;
  height: 48px;
  position: relative;
  width: 156px;
}

.desktop .frame-5 {
  align-items: center;
  display: inline-flex;
  gap: 9.6px;
  left: 13px;
  position: absolute;
  top: 11px;
}

.desktop .icon-instance-node {
  height: 27px !important;
  position: relative !important;
  width: 27px !important;
}

.desktop .text-wrapper-7 {
  color: var(--white);
  font-family: "Quicksand", Helvetica;
  font-size: 19.2px;
  font-weight: 600;
  letter-spacing: 0;
  line-height: normal;
  position: relative;
  width: fit-content;
}

.desktop .frame-6 {
  align-items: flex-start;
  border-radius: 2.4px;
  box-shadow: 0px 2.4px 2.4px #00000040;
  display: inline-flex;
  flex-direction: column;
  gap: 4.8px;
  left: 735px;
  overflow: hidden;
  position: absolute;
  top: 14756px;
}

.desktop .rectangle-2 {
  background-color: var(--purple);
  border-radius: 2.4px;
  height: 48px;
  position: relative;
  width: 156px;
}

.desktop .frame-7 {
  align-items: center;
  display: inline-flex;
  gap: 9.6px;
  left: 15px;
  position: absolute;
  top: 11px;
}

.desktop .group-3 {
  left: 170px !important;
  position: absolute !important;
  top: 12903px !important;
}

.desktop .group-instance {
  left: 170px !important;
  position: absolute !important;
  top: 13403px !important;
}

.desktop .group-3-instance {
  left: 170px !important;
  position: absolute !important;
  top: 13563px !important;
}

.desktop .design-component-instance-node {
  left: 170px !important;
  position: absolute !important;
  top: 13753px !important;
}

.desktop .group-2 {
  left: 170px !important;
  position: absolute !important;
  top: 14093px !important;
}

.desktop .group-4 {
  left: 170px !important;
  position: absolute !important;
  top: 14283px !important;
}

.desktop .group-5 {
  left: 170px !important;
  position: absolute !important;
  top: 14473px !important;
}

.desktop .group-6 {
  height: 82px;
  left: 170px;
  position: absolute;
  top: 13033px;
  width: 1116px;
}

.desktop .how-can-i-take-2 {
  color: var(--purple);
  font-family: var(--h-3-font-family);
  font-size: var(--h-3-font-size);
  font-style: var(--h-3-font-style);
  font-weight: var(--h-3-font-weight);
  left: 0;
  letter-spacing: var(--h-3-letter-spacing);
  line-height: var(--h-3-line-height);
  position: absolute;
  top: -1px;
  width: 907px;
}

.desktop .you-can-talk-to-the-2 {
  color: #2a2a2ab2;
  font-family: var(--h-4-font-family);
  font-size: var(--h-4-font-size);
  font-style: var(--h-4-font-style);
  font-weight: var(--h-4-font-weight);
  left: 70px;
  letter-spacing: var(--h-4-letter-spacing);
  line-height: var(--h-4-line-height);
  position: absolute;
  top: 51px;
  width: 1000px;
}

.desktop .text-wrapper-8 {
  color: #2a2a2ab2;
  font-family: var(--h-4-font-family);
  font-size: var(--h-4-font-size);
  font-style: var(--h-4-font-style);
  font-weight: var(--h-4-font-weight);
  letter-spacing: var(--h-4-letter-spacing);
  line-height: var(--h-4-line-height);
}

.desktop .text-wrapper-9 {
  font-family: var(--h-4-font-family);
  font-size: var(--h-4-font-size);
  font-style: var(--h-4-font-style);
  font-weight: var(--h-4-font-weight);
  letter-spacing: var(--h-4-letter-spacing);
  line-height: var(--h-4-line-height);
  text-decoration: underline;
}

.desktop .img {
  height: 24px;
  left: 1070px;
  position: absolute;
  top: 6px;
  width: 46px;
}

.desktop .line {
  height: 1px;
  left: 172px;
  position: absolute;
  top: 12422px;
  width: 1097px;
}

.desktop .line-2 {
  height: 1px;
  left: 172px;
  position: absolute;
  top: 12784px;
  width: 1097px;
}

.desktop .frame-8 {
  align-items: flex-start;
  display: flex;
  flex-direction: column;
  gap: 32px;
  height: 599px;
  left: 170px;
  position: absolute;
  top: 1417px;
  width: 1100px;
}

.desktop .frame-9 {
  height: 100px;
  position: relative;
  width: 1100px;
}

.desktop .text-wrapper-10 {
  color: var(--black);
  font-family: var(--h1-font-family);
  font-size: var(--h1-font-size);
  font-style: var(--h1-font-style);
  font-weight: var(--h1-font-weight);
  left: 0;
  letter-spacing: var(--h1-letter-spacing);
  line-height: var(--h1-line-height);
  position: absolute;
  top: -1px;
  width: 1100px;
}

.desktop .eurokids-preschools-wrapper {
  align-self: stretch;
  height: 467px;
  position: relative;
  width: 100%;
}

.desktop .eurokids-preschools {
  color: var(--body-text-color);
  font-family: var(--h-4-font-family);
  font-size: var(--h-4-font-size);
  font-style: var(--h-4-font-style);
  font-weight: var(--h-4-font-weight);
  left: 8px;
  letter-spacing: var(--h-4-letter-spacing);
  line-height: var(--h-4-line-height);
  position: absolute;
  top: 7px;
  width: 1084px;
}

.desktop .additionally-wrapper {
  align-items: center;
  display: flex;
  gap: 8px;
  height: 390px;
  justify-content: center;
  left: 162px;
  overflow: hidden;
  padding: 8px;
  position: absolute;
  top: 3776px;
  width: 1116px;
}

.desktop .additionally {
  color: var(--body-text-color);
  font-family: var(--h-4-font-family);
  font-size: var(--h-4-font-size);
  font-style: var(--h-4-font-style);
  font-weight: var(--h-4-font-weight);
  height: 397px;
  letter-spacing: var(--h-4-letter-spacing);
  line-height: var(--h-4-line-height);
  margin-bottom: -10.5px;
  margin-top: -12.5px;
  position: relative;
  width: 1100px;
}

.desktop .element-play-based {
  color: var(--body-text-color);
  font-family: "Quicksand", Helvetica;
  font-size: 18px;
  font-weight: 400;
  left: 170px;
  letter-spacing: 0.36px;
  line-height: 30px;
  position: absolute;
  top: 4205px;
  width: 1100px;
}

.desktop .text-wrapper-11 {
  color: #2a2a2ae6;
  font-family: "Quicksand", Helvetica;
  font-size: 18px;
  font-weight: 400;
  letter-spacing: 0.36px;
  line-height: 30px;
}

.desktop .frame-10 {
  align-items: center;
  display: flex;
  gap: 8px;
  justify-content: center;
  left: 162px;
  padding: 8px;
  position: absolute;
  top: 5318px;
  width: 1116px;
}

.desktop .text-wrapper-12 {
  color: var(--body-text-color);
  font-family: var(--h-4-font-family);
  font-size: var(--h-4-font-size);
  font-style: var(--h-4-font-style);
  font-weight: var(--h-4-font-weight);
  letter-spacing: var(--h-4-letter-spacing);
  line-height: var(--h-4-line-height);
  margin-top: -1px;
  position: relative;
  width: 1100px;
}

.desktop .frame-11 {
  align-items: center;
  display: flex;
  gap: 8px;
  justify-content: center;
  left: 162px;
  padding: 8px;
  position: absolute;
  top: 6846px;
  width: 1116px;
}

.desktop .daycare-fees-are-wrapper {
  align-items: center;
  display: flex;
  gap: 8px;
  justify-content: center;
  left: 162px;
  padding: 8px;
  position: absolute;
  top: 7116px;
  width: 1116px;
}

.desktop .frame-12 {
  align-items: center;
  display: flex;
  gap: 8px;
  justify-content: center;
  left: 162px;
  padding: 8px;
  position: absolute;
  top: 5550px;
  width: 1116px;
}

.desktop .frame-13 {
  align-items: center;
  display: flex;
  gap: 8px;
  justify-content: center;
  left: 162px;
  padding: 8px;
  position: absolute;
  top: 5782px;
  width: 1116px;
}

.desktop .frame-14 {
  align-items: center;
  display: flex;
  gap: 8px;
  justify-content: center;
  left: 162px;
  padding: 8px;
  position: absolute;
  top: 6224px;
  width: 1116px;
}

.desktop .frame-15 {
  align-items: center;
  display: flex;
  gap: 8px;
  justify-content: center;
  left: 162px;
  padding: 8px;
  position: absolute;
  top: 8840px;
  width: 1116px;
}

.desktop .in-the-bustling-wrapper {
  align-items: center;
  display: flex;
  gap: 8px;
  justify-content: center;
  left: 162px;
  padding: 8px;
  position: absolute;
  top: 10740px;
  width: 1116px;
}

.desktop .in-the-bustling {
  color: var(--body-text-color);
  font-family: "Quicksand", Helvetica;
  font-size: 18px;
  font-weight: 500;
  letter-spacing: 0.06px;
  line-height: 18px;
  margin-top: -1px;
  position: relative;
  width: 1100px;
}

.desktop .frame-16 {
  align-items: center;
  display: flex;
  gap: 8px;
  justify-content: center;
  left: 162px;
  padding: 8px;
  position: absolute;
  top: 9080px;
  width: 1116px;
}

.desktop .frame-17 {
  align-items: center;
  display: flex;
  gap: 8px;
  justify-content: center;
  left: 162px;
  padding: 8px;
  position: absolute;
  top: 9282px;
  width: 1116px;
}

.desktop .to-ensure-a-spacious-wrapper {
  align-items: center;
  display: flex;
  gap: 8px;
  justify-content: center;
  left: 162px;
  padding: 8px;
  position: absolute;
  top: 9514px;
  width: 1116px;
}

.desktop .element-established-brand-wrapper {
  align-items: center;
  display: flex;
  gap: 8px;
  justify-content: center;
  left: 162px;
  padding: 8px;
  position: absolute;
  top: 9808px;
  width: 1116px;
}

.desktop .frame-18 {
  align-items: flex-start;
  display: flex;
  flex-direction: column;
  gap: 32px;
  height: 212px;
  left: 170px;
  overflow: hidden;
  position: absolute;
  top: 2096px;
  width: 1100px;
}

.desktop .frame-19 {
  align-items: center;
  display: inline-flex;
  flex: 0 0 auto;
  gap: 8px;
  justify-content: center;
  padding: 8px;
  position: relative;
}

.desktop .kiddenz-a-renowned-wrapper {
  align-items: center;
  align-self: stretch;
  display: flex;
  flex: 0 0 auto;
  gap: 8px;
  justify-content: center;
  padding: 8px;
  position: relative;
  width: 100%;
}

.desktop .kiddenz-a-renowned {
  color: var(--body-text-color);
  flex: 1;
  font-family: var(--h-4-font-family);
  font-size: var(--h-4-font-size);
  font-style: var(--h-4-font-style);
  font-weight: var(--h-4-font-weight);
  letter-spacing: var(--h-4-letter-spacing);
  line-height: var(--h-4-line-height);
  margin-top: -1px;
  position: relative;
}

.desktop .frame-20 {
  align-items: center;
  display: inline-flex;
  gap: 8px;
  justify-content: center;
  left: 162px;
  padding: 8px;
  position: absolute;
  top: 3704px;
}

.desktop .frame-21 {
  height: 56px;
  left: 162px;
  position: absolute;
  top: 5170px;
  width: 1116px;
}

.desktop .frame-22 {
  align-items: center;
  display: inline-flex;
  gap: 8px;
  justify-content: center;
  padding: 8px;
  position: relative;
}

.desktop .frame-23 {
  height: 56px;
  left: 162px;
  position: absolute;
  top: 6710px;
  width: 1116px;
}

.desktop .frame-24 {
  height: 56px;
  left: 162px;
  position: absolute;
  top: 5254px;
  width: 1116px;
}

.desktop .text-wrapper-13 {
  color: var(--black);
  font-family: var(--h-3-font-family);
  font-size: var(--h-3-font-size);
  font-style: var(--h-3-font-style);
  font-weight: var(--h-3-font-weight);
  letter-spacing: var(--h-3-letter-spacing);
  line-height: var(--h-3-line-height);
  margin-top: -1px;
  position: relative;
  white-space: nowrap;
  width: fit-content;
}

.desktop .frame-25 {
  height: 56px;
  left: 162px;
  position: absolute;
  top: 6782px;
  width: 1116px;
}

.desktop .frame-26 {
  height: 56px;
  left: 162px;
  position: absolute;
  top: 7052px;
  width: 1116px;
}

.desktop .frame-27 {
  height: 56px;
  left: 162px;
  position: absolute;
  top: 5486px;
  width: 1116px;
}

.desktop .frame-28 {
  height: 56px;
  left: 162px;
  position: absolute;
  top: 5718px;
  width: 1116px;
}

.desktop .frame-29 {
  height: 56px;
  left: 162px;
  position: absolute;
  top: 6160px;
  width: 1116px;
}

.desktop .frame-30 {
  height: 56px;
  left: 162px;
  position: absolute;
  top: 8784px;
  width: 1116px;
}

.desktop .frame-31 {
  height: 56px;
  left: 162px;
  position: absolute;
  top: 10684px;
  width: 1116px;
}

.desktop .frame-32 {
  height: 40px;
  left: 162px;
  position: absolute;
  top: 9032px;
  width: 1116px;
}

.desktop .frame-33 {
  height: 40px;
  left: 162px;
  position: absolute;
  top: 9234px;
  width: 1116px;
}

.desktop .frame-34 {
  height: 40px;
  left: 162px;
  position: absolute;
  top: 9466px;
  width: 1116px;
}

.desktop .frame-35 {
  height: 40px;
  left: 162px;
  position: absolute;
  top: 9760px;
  width: 1116px;
}

.desktop .frame-36 {
  height: 56px;
  left: 162px;
  position: absolute;
  top: 8112px;
  width: 1116px;
}

.desktop .frame-37 {
  align-items: flex-start;
  display: flex;
  flex-wrap: wrap;
  gap: -1px 0px;
  height: 480px;
  left: 170px;
  position: absolute;
  top: 8208px;
  width: 1099px;
}

.desktop .rectangle-3 {
  border: 1px solid;
  border-color: #000000;
  height: 80px;
  position: relative;
  width: 367px;
}

.desktop .rectangle-4 {
  border: 1px solid;
  border-color: #000000;
  height: 80px;
  margin-left: -1px;
  position: relative;
  width: 367px;
}

.desktop .text-wrapper-14 {
  color: #000000;
  font-family: var(--h-3-font-family);
  font-size: var(--h-3-font-size);
  font-style: var(--h-3-font-style);
  font-weight: var(--h-3-font-weight);
  left: 842px;
  letter-spacing: var(--h-3-letter-spacing);
  line-height: var(--h-3-line-height);
  position: absolute;
  text-align: center;
  top: 22px;
  white-space: nowrap;
}

.desktop .text-wrapper-15 {
  color: #000000;
  font-family: var(--h-3-font-family);
  font-size: var(--h-3-font-size);
  font-style: var(--h-3-font-style);
  font-weight: var(--h-3-font-weight);
  left: 854px;
  letter-spacing: var(--h-3-letter-spacing);
  line-height: var(--h-3-line-height);
  position: absolute;
  text-align: center;
  top: 102px;
  white-space: nowrap;
}

.desktop .text-wrapper-16 {
  color: #000000;
  font-family: var(--h-3-font-family);
  font-size: var(--h-3-font-size);
  font-style: var(--h-3-font-style);
  font-weight: var(--h-3-font-weight);
  left: 854px;
  letter-spacing: var(--h-3-letter-spacing);
  line-height: var(--h-3-line-height);
  position: absolute;
  text-align: center;
  top: 182px;
  white-space: nowrap;
}

.desktop .text-wrapper-17 {
  color: #000000;
  font-family: var(--h-3-font-family);
  font-size: var(--h-3-font-size);
  font-style: var(--h-3-font-style);
  font-weight: var(--h-3-font-weight);
  left: 854px;
  letter-spacing: var(--h-3-letter-spacing);
  line-height: var(--h-3-line-height);
  position: absolute;
  text-align: center;
  top: 262px;
  white-space: nowrap;
}

.desktop .text-wrapper-18 {
  color: #000000;
  font-family: var(--h-3-font-family);
  font-size: var(--h-3-font-size);
  font-style: var(--h-3-font-style);
  font-weight: var(--h-3-font-weight);
  left: 854px;
  letter-spacing: var(--h-3-letter-spacing);
  line-height: var(--h-3-line-height);
  position: absolute;
  text-align: center;
  top: 342px;
  white-space: nowrap;
}

.desktop .text-wrapper-19 {
  color: #000000;
  font-family: var(--h-3-font-family);
  font-size: var(--h-3-font-size);
  font-style: var(--h-3-font-style);
  font-weight: var(--h-3-font-weight);
  left: 854px;
  letter-spacing: var(--h-3-letter-spacing);
  line-height: var(--h-3-line-height);
  position: absolute;
  text-align: center;
  top: 422px;
  white-space: nowrap;
}

.desktop .text-wrapper-20 {
  color: #000000;
  font-family: var(--h-3-font-family);
  font-size: var(--h-3-font-size);
  font-style: var(--h-3-font-style);
  font-weight: var(--h-3-font-weight);
  left: 479px;
  letter-spacing: var(--h-3-letter-spacing);
  line-height: var(--h-3-line-height);
  position: absolute;
  text-align: center;
  top: 22px;
  white-space: nowrap;
}

.desktop .text-wrapper-21 {
  color: #000000;
  font-family: var(--h-3-font-family);
  font-size: var(--h-3-font-size);
  font-style: var(--h-3-font-style);
  font-weight: var(--h-3-font-weight);
  left: 477px;
  letter-spacing: var(--h-3-letter-spacing);
  line-height: var(--h-3-line-height);
  position: absolute;
  text-align: center;
  top: 102px;
  white-space: nowrap;
}

.desktop .text-wrapper-22 {
  color: #000000;
  font-family: var(--h-3-font-family);
  font-size: var(--h-3-font-size);
  font-style: var(--h-3-font-style);
  font-weight: var(--h-3-font-weight);
  left: 477px;
  letter-spacing: var(--h-3-letter-spacing);
  line-height: var(--h-3-line-height);
  position: absolute;
  text-align: center;
  top: 182px;
  white-space: nowrap;
}

.desktop .text-wrapper-23 {
  color: #000000;
  font-family: var(--h-3-font-family);
  font-size: var(--h-3-font-size);
  font-style: var(--h-3-font-style);
  font-weight: var(--h-3-font-weight);
  left: 477px;
  letter-spacing: var(--h-3-letter-spacing);
  line-height: var(--h-3-line-height);
  position: absolute;
  text-align: center;
  top: 262px;
  white-space: nowrap;
}

.desktop .text-wrapper-24 {
  color: #000000;
  font-family: var(--h-3-font-family);
  font-size: var(--h-3-font-size);
  font-style: var(--h-3-font-style);
  font-weight: var(--h-3-font-weight);
  left: 477px;
  letter-spacing: var(--h-3-letter-spacing);
  line-height: var(--h-3-line-height);
  position: absolute;
  text-align: center;
  top: 342px;
  white-space: nowrap;
}

.desktop .text-wrapper-25 {
  color: #000000;
  font-family: var(--h-3-font-family);
  font-size: var(--h-3-font-size);
  font-style: var(--h-3-font-style);
  font-weight: var(--h-3-font-weight);
  left: 477px;
  letter-spacing: var(--h-3-letter-spacing);
  line-height: var(--h-3-line-height);
  position: absolute;
  text-align: center;
  top: 422px;
  white-space: nowrap;
}

.desktop .text-wrapper-26 {
  color: #000000;
  font-family: var(--h-3-font-family);
  font-size: var(--h-3-font-size);
  font-style: var(--h-3-font-style);
  font-weight: var(--h-3-font-weight);
  left: 119px;
  letter-spacing: var(--h-3-letter-spacing);
  line-height: var(--h-3-line-height);
  position: absolute;
  top: 22px;
  white-space: nowrap;
}

.desktop .text-wrapper-27 {
  color: #000000;
  font-family: var(--h-3-font-family);
  font-size: var(--h-3-font-size);
  font-style: var(--h-3-font-style);
  font-weight: var(--h-3-font-weight);
  left: 119px;
  letter-spacing: var(--h-3-letter-spacing);
  line-height: var(--h-3-line-height);
  position: absolute;
  top: 102px;
  white-space: nowrap;
}

.desktop .text-wrapper-28 {
  color: #000000;
  font-family: var(--h-3-font-family);
  font-size: var(--h-3-font-size);
  font-style: var(--h-3-font-style);
  font-weight: var(--h-3-font-weight);
  left: 119px;
  letter-spacing: var(--h-3-letter-spacing);
  line-height: var(--h-3-line-height);
  position: absolute;
  top: 182px;
  white-space: nowrap;
}

.desktop .text-wrapper-29 {
  color: #000000;
  font-family: var(--h-3-font-family);
  font-size: var(--h-3-font-size);
  font-style: var(--h-3-font-style);
  font-weight: var(--h-3-font-weight);
  left: 119px;
  letter-spacing: var(--h-3-letter-spacing);
  line-height: var(--h-3-line-height);
  position: absolute;
  top: 262px;
  white-space: nowrap;
}

.desktop .text-wrapper-30 {
  color: #000000;
  font-family: var(--h-3-font-family);
  font-size: var(--h-3-font-size);
  font-style: var(--h-3-font-style);
  font-weight: var(--h-3-font-weight);
  left: 119px;
  letter-spacing: var(--h-3-letter-spacing);
  line-height: var(--h-3-line-height);
  position: absolute;
  top: 342px;
  white-space: nowrap;
}

.desktop .text-wrapper-31 {
  color: #000000;
  font-family: var(--h-3-font-family);
  font-size: var(--h-3-font-size);
  font-style: var(--h-3-font-style);
  font-weight: var(--h-3-font-weight);
  left: 119px;
  letter-spacing: var(--h-3-letter-spacing);
  line-height: var(--h-3-line-height);
  position: absolute;
  top: 422px;
  white-space: nowrap;
}

.desktop .line-3 {
  height: 1px;
  left: 172px;
  position: absolute;
  top: 8735px;
  width: 1097px;
}

.desktop .line-4 {
  height: 1px;
  left: 172px;
  position: absolute;
  top: 10643px;
  width: 1097px;
}

.desktop .line-5 {
  height: 1px;
  left: 172px;
  position: absolute;
  top: 11605px;
  width: 1097px;
}

.desktop .line-6 {
  height: 1px;
  left: 172px;
  position: absolute;
  top: 8071px;
  width: 1097px;
}

.desktop .line-7 {
  height: 1px;
  left: 172px;
  position: absolute;
  top: 3671px;
  width: 1097px;
}

.desktop .line-8 {
  height: 1px;
  left: 172px;
  position: absolute;
  top: 5129px;
  width: 1097px;
}

.desktop .line-9 {
  height: 1px;
  left: 172px;
  position: absolute;
  top: 6669px;
  width: 1097px;
}

.desktop .line-10 {
  height: 1px;
  left: 172px;
  position: absolute;
  top: 2055px;
  width: 1097px;
}

.desktop .chevron-left {
  height: 60px;
  left: 1316px;
  position: absolute;
  top: 1101px;
  width: 60px;
}

.desktop .chevron-left-2 {
  height: 60px;
  left: 64px;
  position: absolute;
  top: 1101px;
  width: 60px;
}

.desktop .eurokids {
  color: var(--body-text-color);
  font-family: "Quicksand", Helvetica;
  font-size: 18px;
  font-weight: 500;
  left: 178px;
  letter-spacing: 0.36px;
  line-height: 30px;
  position: absolute;
  text-decoration: underline;
  top: 2435px;
}

.desktop .eurokids-magarpatta {
  color: var(--body-text-color);
  font-family: "Quicksand", Helvetica;
  font-size: 18px;
  font-weight: 500;
  left: 178px;
  letter-spacing: 0.36px;
  line-height: 30px;
  position: absolute;
  text-decoration: underline;
  top: 2683px;
}

.desktop .eurokids-kalyan {
  color: var(--body-text-color);
  font-family: "Quicksand", Helvetica;
  font-size: 18px;
  font-weight: 500;
  left: 178px;
  letter-spacing: 0.36px;
  line-height: 30px;
  position: absolute;
  text-decoration: underline;
  top: 2931px;
}

.desktop .text-wrapper-32 {
  color: #2a2a2ae6;
  font-family: "Quicksand", Helvetica;
  font-size: 18px;
  font-weight: 500;
  letter-spacing: 0.36px;
  line-height: 30px;
  text-decoration: underline;
}

.desktop .text-wrapper-33 {
  color: var(--body-text-color);
  font-family: "Quicksand", Helvetica;
  font-size: 18px;
  font-weight: 500;
  left: 178px;
  letter-spacing: 0.36px;
  line-height: 30px;
  position: absolute;
  text-decoration: underline;
  top: 3593px;
  white-space: nowrap;
}

.desktop .text-wrapper-34 {
  color: var(--body-text-color);
  font-family: "Quicksand", Helvetica;
  font-size: 18px;
  font-weight: 500;
  left: 178px;
  letter-spacing: 0.36px;
  line-height: 30px;
  position: absolute;
  text-decoration: underline;
  top: 3277px;
  white-space: nowrap;
}

.desktop .text-wrapper-35 {
  color: var(--body-text-color);
  font-family: "Quicksand", Helvetica;
  font-size: 18px;
  font-weight: 500;
  left: 178px;
  letter-spacing: 0.36px;
  line-height: 30px;
  position: absolute;
  text-decoration: underline;
  top: 3435px;
  white-space: nowrap;
}

.desktop .text-wrapper-36 {
  color: var(--body-text-color);
  font-family: "Quicksand", Helvetica;
  font-size: 18px;
  font-weight: 500;
  left: 178px;
  letter-spacing: 0.36px;
  line-height: 30px;
  position: absolute;
  text-decoration: underline;
  top: 3119px;
  white-space: nowrap;
}

.desktop .eurokids-jayanagar {
  color: var(--body-text-color);
  font-family: "Quicksand", Helvetica;
  font-size: 18px;
  font-weight: 500;
  left: 613px;
  letter-spacing: 0.36px;
  line-height: 30px;
  position: absolute;
  text-decoration: underline;
  top: 2435px;
}

.desktop .eurokids-kharadi {
  color: var(--body-text-color);
  font-family: "Quicksand", Helvetica;
  font-size: 18px;
  font-weight: 500;
  left: 613px;
  letter-spacing: 0.36px;
  line-height: 30px;
  position: absolute;
  text-decoration: underline;
  top: 2683px;
}

.desktop .eurokids-kalamboli {
  color: var(--body-text-color);
  font-family: "Quicksand", Helvetica;
  font-size: 18px;
  font-weight: 500;
  left: 613px;
  letter-spacing: 0.36px;
  line-height: 30px;
  position: absolute;
  text-decoration: underline;
  top: 2931px;
  white-space: nowrap;
}

.desktop .text-wrapper-37 {
  color: var(--body-text-color);
  font-family: "Quicksand", Helvetica;
  font-size: 18px;
  font-weight: 500;
  left: 613px;
  letter-spacing: 0.36px;
  line-height: 30px;
  position: absolute;
  text-decoration: underline;
  top: 3593px;
  white-space: nowrap;
}

.desktop .eurokids-rmv {
  color: var(--body-text-color);
  font-family: "Quicksand", Helvetica;
  font-size: 18px;
  font-weight: 500;
  left: 1044px;
  letter-spacing: 0.36px;
  line-height: 30px;
  position: absolute;
  text-decoration: underline;
  top: 2435px;
}

.desktop .eurokids-wadgaon {
  color: var(--body-text-color);
  font-family: "Quicksand", Helvetica;
  font-size: 18px;
  font-weight: 500;
  left: 1044px;
  letter-spacing: 0.36px;
  line-height: 30px;
  position: absolute;
  text-decoration: underline;
  top: 2683px;
}

.desktop .text-wrapper-38 {
  color: var(--body-text-color);
  font-family: "Quicksand", Helvetica;
  font-size: 18px;
  font-weight: 500;
  left: 1044px;
  letter-spacing: 0.36px;
  line-height: 30px;
  position: absolute;
  text-decoration: underline;
  top: 2931px;
  white-space: nowrap;
}

.desktop .text-wrapper-39 {
  color: var(--body-text-color);
  font-family: "Quicksand", Helvetica;
  font-size: 18px;
  font-weight: 500;
  left: 1044px;
  letter-spacing: 0.36px;
  line-height: 30px;
  position: absolute;
  text-decoration: underline;
  top: 3593px;
  white-space: nowrap;
}

.desktop .frame-38 {
  height: 48px;
  left: 170px;
  overflow: hidden;
  position: absolute;
  top: 2364px;
  width: 550px;
}

.desktop .frame-39 {
  height: 48px;
  left: 170px;
  overflow: hidden;
  position: absolute;
  top: 2612px;
  width: 550px;
}

.desktop .frame-40 {
  height: 48px;
  left: 170px;
  overflow: hidden;
  position: absolute;
  top: 2860px;
  width: 550px;
}

.desktop .frame-41 {
  height: 48px;
  left: 170px;
  overflow: hidden;
  position: absolute;
  top: 3522px;
  width: 550px;
}

.desktop .frame-42 {
  height: 48px;
  left: 170px;
  overflow: hidden;
  position: absolute;
  top: 3206px;
  width: 550px;
}

.desktop .frame-43 {
  height: 48px;
  left: 170px;
  overflow: hidden;
  position: absolute;
  top: 3364px;
  width: 550px;
}

.desktop .frame-44 {
  height: 48px;
  left: 170px;
  overflow: hidden;
  position: absolute;
  top: 3048px;
  width: 550px;
}

.desktop .overlap {
  height: 615px;
  left: 45px;
  position: absolute;
  top: 117px;
  width: 1350px;
}

.desktop .rectangle-5 {
  height: 467px;
  left: 0;
  position: absolute;
  top: 0;
  width: 1350px;
}

.desktop .rectangle-6 {
  background-color: #ffffff;
  border-radius: 4px;
  box-shadow: 0px 2px 2px #00000014;
  height: 387px;
  left: 57px;
  position: absolute;
  top: 228px;
  width: 1237px;
}

.desktop .text-wrapper-40 {
  color: var(--black);
  font-family: var(--h1-font-family);
  font-size: var(--h1-font-size);
  font-style: var(--h1-font-style);
  font-weight: var(--h1-font-weight);
  left: 126px;
  letter-spacing: var(--h1-letter-spacing);
  line-height: var(--h1-line-height);
  position: absolute;
  top: 273px;
  width: 837px;
}

.desktop .ellipse {
  background-color: var(--grey);
  border-radius: 100px;
  height: 200px;
  left: 97px;
  position: absolute;
  top: 397px;
  width: 200px;
}

.desktop .image {
  height: 111px;
  left: 118px;
  position: absolute;
  top: 441px;
  width: 158px;
}

.desktop .overlap-group-wrapper {
  height: 202px;
  left: 1076px;
  position: absolute;
  top: 228px;
  width: 220px;
}

.desktop .overlap-group {
  background-color: #2a2a2ae6;
  border-radius: 4px;
  height: 202px;
  position: relative;
  width: 218px;
}

.desktop .text-wrapper-41 {
  color: #ffffff;
  font-family: "Quicksand", Helvetica;
  font-size: 22px;
  font-weight: 600;
  left: 43px;
  letter-spacing: 0;
  line-height: normal;
  position: absolute;
  top: 26px;
}

.desktop .image-2 {
  height: 20px;
  left: 38px;
  position: absolute;
  top: 79px;
  width: 25px;
}

.desktop .image-wrapper {
  background-color: #ffffff;
  height: 23px;
  left: 38px;
  position: absolute;
  top: 107px;
  width: 25px;
}

.desktop .image-3 {
  height: 18px;
  left: 3px;
  object-fit: cover;
  position: absolute;
  top: 2px;
  width: 20px;
}

.desktop .playschool-wrapper {
  background-color: #ffffff;
  height: 23px;
  left: 38px;
  position: absolute;
  top: 139px;
  width: 25px;
}

.desktop .playschool {
  height: 21px;
  left: 2px;
  object-fit: cover;
  position: absolute;
  top: 1px;
  width: 21px;
}

.desktop .text-wrapper-42 {
  color: #000000;
  font-family: var(--h3-h3-reg-font-family);
  font-size: var(--h3-h3-reg-font-size);
  font-style: var(--h3-h3-reg-font-style);
  font-weight: var(--h3-h3-reg-font-weight);
  left: 335px;
  letter-spacing: var(--h3-h3-reg-letter-spacing);
  line-height: var(--h3-h3-reg-line-height);
  position: absolute;
  top: 402px;
  width: 685px;
}

.desktop .text-wrapper-43 {
  color: #ffffff;
  font-family: "Quicksand", Helvetica;
  font-size: 20px;
  font-weight: 600;
  left: 1163px;
  letter-spacing: 0;
  line-height: 29.5px;
  position: absolute;
  text-decoration: underline;
  top: 301px;
  white-space: nowrap;
}

.desktop .text-wrapper-44 {
  color: #ffffff;
  font-family: "Quicksand", Helvetica;
  font-size: 20px;
  font-weight: 600;
  left: 1163px;
  letter-spacing: 0;
  line-height: 29.5px;
  position: absolute;
  text-decoration: underline;
  top: 331px;
  white-space: nowrap;
}

.desktop .text-wrapper-45 {
  color: #ffffff;
  font-family: "Quicksand", Helvetica;
  font-size: 20px;
  font-weight: 600;
  left: 1163px;
  letter-spacing: 0;
  line-height: 29.5px;
  position: absolute;
  text-decoration: underline;
  top: 363px;
  white-space: nowrap;
}

.desktop .copy-of-kiddenz-logo {
  height: 83px;
  left: 40px;
  position: absolute;
  top: 12px;
  width: 234px;
}

.desktop .text-wrapper-46 {
  color: #000000;
  font-family: var(--h-4-font-family);
  font-size: var(--h-4-font-size);
  font-style: var(--h-4-font-style);
  font-weight: var(--h-4-font-weight);
  left: 705px;
  letter-spacing: var(--h-4-letter-spacing);
  line-height: var(--h-4-line-height);
  position: absolute;
  top: 37px;
  white-space: nowrap;
}

.desktop .text-wrapper-47 {
  color: #000000;
  font-family: var(--h-4-font-family);
  font-size: var(--h-4-font-size);
  font-style: var(--h-4-font-style);
  font-weight: var(--h-4-font-weight);
  left: 801px;
  letter-spacing: var(--h-4-letter-spacing);
  line-height: var(--h-4-line-height);
  position: absolute;
  top: 37px;
  white-space: nowrap;
}

.desktop .text-wrapper-48 {
  color: #000000;
  font-family: var(--h-4-font-family);
  font-size: var(--h-4-font-size);
  font-style: var(--h-4-font-style);
  font-weight: var(--h-4-font-weight);
  left: 1000px;
  letter-spacing: var(--h-4-letter-spacing);
  line-height: var(--h-4-line-height);
  position: absolute;
  top: 37px;
  white-space: nowrap;
}

.desktop .frame-45 {
  align-items: center;
  background-color: var(--green);
  border-radius: 4px;
  display: inline-flex;
  gap: 8px;
  justify-content: center;
  left: 1163px;
  overflow: hidden;
  padding: 9px 28px;
  position: absolute;
  top: 29px;
}

.desktop .text-wrapper-49 {
  color: var(--white);
  font-family: "Quicksand", Helvetica;
  font-size: 16px;
  font-weight: 500;
  letter-spacing: 0.32px;
  line-height: 30px;
  margin-top: -1px;
  position: relative;
  text-align: center;
  white-space: nowrap;
  width: fit-content;
}

.desktop .frame-46 {
  align-items: center;
  border: 1px solid;
  border-color: var(--purple);
  border-radius: 4px;
  display: inline-flex;
  gap: 8px;
  justify-content: center;
  left: 1274px;
  overflow: hidden;
  padding: 9px 28px;
  position: absolute;
  top: 29px;
}

.desktop .text-wrapper-50 {
  color: var(--purple);
  font-family: "Quicksand", Helvetica;
  font-size: 16px;
  font-weight: 500;
  letter-spacing: 0.32px;
  line-height: 30px;
  margin-top: -1px;
  position: relative;
  text-align: center;
  white-space: nowrap;
  width: fit-content;
}

.desktop .breadcrumbs-instance {
  left: 140px !important;
  position: absolute !important;
  top: 900px !important;
}

.desktop .rectangle-7 {
  height: 326px;
  left: 140px;
  position: absolute;
  top: 968px;
  width: 332px;
}

.desktop .rectangle-8 {
  height: 326px;
  left: 968px;
  object-fit: cover;
  position: absolute;
  top: 968px;
  width: 332px;
}

.desktop .rectangle-9 {
  height: 326px;
  left: 554px;
  object-fit: cover;
  position: absolute;
  top: 968px;
  width: 332px;
}

.desktop .text-wrapper-51 {
  color: #000000;
  font-family: "Quicksand", Helvetica;
  font-size: 18px;
  font-weight: 600;
  left: 220px;
  letter-spacing: 0;
  line-height: normal;
  position: absolute;
  top: 1313px;
}

.desktop .text-wrapper-52 {
  color: #000000;
  font-family: "Quicksand", Helvetica;
  font-size: 18px;
  font-weight: 600;
  left: 685px;
  letter-spacing: 0;
  line-height: normal;
  position: absolute;
  top: 1313px;
}

.desktop .text-wrapper-53 {
  color: #000000;
  font-family: "Quicksand", Helvetica;
  font-size: 18px;
  font-weight: 600;
  left: 1072px;
  letter-spacing: 0;
  line-height: normal;
  position: absolute;
  top: 1313px;
}

.desktop .text-wrapper-54 {
  color: #000000;
  font-family: "Quicksand", Helvetica;
  font-size: 18px;
  font-weight: 500;
  left: 140px;
  letter-spacing: 0.36px;
  line-height: 30px;
  position: absolute;
  text-align: center;
  text-decoration: underline;
  top: 782px;
  white-space: nowrap;
}

.desktop .text-wrapper-55 {
  color: #000000;
  font-family: "Quicksand", Helvetica;
  font-size: 18px;
  font-weight: 500;
  left: 429px;
  letter-spacing: 0.36px;
  line-height: 30px;
  position: absolute;
  text-align: center;
  text-decoration: underline;
  top: 782px;
  white-space: nowrap;
}

.desktop .text-wrapper-56 {
  color: #000000;
  font-family: "Quicksand", Helvetica;
  font-size: 18px;
  font-weight: 500;
  left: 428px;
  letter-spacing: 0.36px;
  line-height: 30px;
  position: absolute;
  text-align: center;
  text-decoration: underline;
  top: 825px;
  white-space: nowrap;
}

.desktop .text-wrapper-57 {
  color: #000000;
  font-family: "Quicksand", Helvetica;
  font-size: 18px;
  font-weight: 500;
  left: 633px;
  letter-spacing: 0.36px;
  line-height: 30px;
  position: absolute;
  text-align: center;
  text-decoration: underline;
  top: 782px;
  white-space: nowrap;
}

.desktop .text-wrapper-58 {
  color: #000000;
  font-family: "Quicksand", Helvetica;
  font-size: 18px;
  font-weight: 500;
  left: 140px;
  letter-spacing: 0.36px;
  line-height: 30px;
  position: absolute;
  text-align: center;
  text-decoration: underline;
  top: 825px;
  white-space: nowrap;
}

.desktop .text-wrapper-59 {
  color: #000000;
  font-family: "Quicksand", Helvetica;
  font-size: 18px;
  font-weight: 500;
  left: 864px;
  letter-spacing: 0.36px;
  line-height: 30px;
  position: absolute;
  text-align: center;
  text-decoration: underline;
  top: 782px;
  white-space: nowrap;
}

.desktop .text-wrapper-60 {
  color: #000000;
  font-family: "Quicksand", Helvetica;
  font-size: 18px;
  font-weight: 500;
  left: 867px;
  letter-spacing: 0.36px;
  line-height: 30px;
  position: absolute;
  text-align: center;
  text-decoration: underline;
  top: 825px;
  white-space: nowrap;
}

.desktop .text-wrapper-61 {
  color: #000000;
  font-family: "Quicksand", Helvetica;
  font-size: 18px;
  font-weight: 500;
  left: 625px;
  letter-spacing: 0.36px;
  line-height: 30px;
  position: absolute;
  text-align: center;
  text-decoration: underline;
  top: 825px;
  white-space: nowrap;
}

.desktop .text-wrapper-62 {
  color: #000000;
  font-family: "Quicksand", Helvetica;
  font-size: 18px;
  font-weight: 500;
  left: 1096px;
  letter-spacing: 0.36px;
  line-height: 30px;
  position: absolute;
  text-align: center;
  text-decoration: underline;
  top: 782px;
  white-space: nowrap;
}

.desktop .text-wrapper-63 {
  color: #000000;
  font-family: "Quicksand", Helvetica;
  font-size: 18px;
  font-weight: 500;
  left: 1201px;
  letter-spacing: 0.36px;
  line-height: 30px;
  position: absolute;
  text-align: center;
  text-decoration: underline;
  top: 825px;
  white-space: nowrap;
}

`;

// export const SanitizeStyle;
