// const express = require("express");
// const next = require("next");
// const bodyParser = require("body-parser");
// // const cors = require("cors");

// const dev = process.env.NODE_ENV !== "production";
// const app = next({ dev });
// const handle = app.getRequestHandler();
// console.log(__dirname);
// const sitemapOptions = {
//   root: __dirname + "/../public/",
//   headers: {
//     "Content-Type": "text/xml;charset=UTF-8",
//   },
// };

// app.prepare().then(() => {
//   const server = express();
//   server.use(bodyParser.json());

//   // serve sitemap
//   server.get("/sitemap.xml", (req, res) =>
//     res.status(200).sendFile("sitemap.xml", sitemapOptions)
//   );

//   server.get("*", (req, res) => {
//     return handle(req, res);
//   });

//   server.listen(3000, (err) => {
//     if (err) throw err;
//     console.log("> Read on http://localhost:3000");
//   });
// });

// const express = require("express");
// const next = require("next");
// const routes = require("../routes");
// const path = require("path");

// const PORT = process.env.PORT || 3000;
// const dev = process.env.NODE_ENV !== "production";
// const app = next({ dir: "./", dev });
// const handler = routes.getRequestHandler(app);
// app
//   .prepare()
//   .then(() => {
//     const server = express();
//     // server.use(express.static(path.join(__dirname, "../public")));
//     server.get("*", (req, res) => handler(req, res));

//     server.listen(PORT, err => {
//       if (err) throw err;
//       console.log(`> Ready on ${PORT}`);
//     });
//   })
//   .catch(ex => {
//     console.error(ex.stack);
//     process.exit(1);
//   });

process.env.NODE_ENV = 'production';
const compression = require('compression');
const express = require('express');
const next = require('next');
const port = parseInt(process.env.PORT, 10) || 3000;
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();

app.prepare().then(() => {
  const server = express();
  if (!dev) {
    server.use(compression());
  }
  server.all('*', (req, res) => handle(req, res));
  server.listen(port, err => {
    if (err) throw err;
  });
});
